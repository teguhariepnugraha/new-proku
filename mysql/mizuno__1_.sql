-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 04 Okt 2023 pada 06.33
-- Versi server: 10.4.21-MariaDB
-- Versi PHP: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mizuno`
--

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `database_schema`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `database_schema` (
`TABLE_CATALOG` varchar(512)
,`TABLE_SCHEMA` varchar(64)
,`TABLE_NAME` varchar(64)
,`COLUMN_NAME` varchar(64)
,`ORDINAL_POSITION` bigint(21) unsigned
,`COLUMN_DEFAULT` longtext
,`IS_NULLABLE` varchar(3)
,`DATA_TYPE` varchar(64)
,`CHARACTER_MAXIMUM_LENGTH` bigint(21) unsigned
,`CHARACTER_OCTET_LENGTH` bigint(21) unsigned
,`NUMERIC_PRECISION` bigint(21) unsigned
,`NUMERIC_SCALE` bigint(21) unsigned
,`DATETIME_PRECISION` bigint(21) unsigned
,`CHARACTER_SET_NAME` varchar(32)
,`COLLATION_NAME` varchar(32)
,`COLUMN_TYPE` longtext
,`COLUMN_KEY` varchar(3)
,`EXTRA` varchar(30)
,`PRIVILEGES` varchar(80)
,`COLUMN_COMMENT` varchar(1024)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pengajuan_insentive_sro`
--

CREATE TABLE `detail_pengajuan_insentive_sro` (
  `iddtlpengajuan` int(11) NOT NULL,
  `idpengajuan` int(11) NOT NULL,
  `lastprogress` varchar(75) DEFAULT NULL,
  `bulan` varchar(75) DEFAULT NULL,
  `tahun` varchar(250) DEFAULT NULL,
  `aprroveby` varchar(250) DEFAULT NULL,
  `aproveddate` date DEFAULT NULL,
  `relateduser` varchar(250) DEFAULT NULL,
  `insert_by` float DEFAULT NULL,
  `insert_time` date DEFAULT NULL,
  `update_by` float DEFAULT NULL,
  `update_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pengajuan_insentive_sro`
--

INSERT INTO `detail_pengajuan_insentive_sro` (`iddtlpengajuan`, `idpengajuan`, `lastprogress`, `bulan`, `tahun`, `aprroveby`, `aproveddate`, `relateduser`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(73, 29, 'SRO', '1', '2022', '04.12.05.010', '2022-04-11', '', 4.12, '2022-04-11', 4.12, '2022-04-11'),
(74, 30, 'SRO', '1', '2022', '04.12.05.010', '2022-04-13', 'DCACJRSRO', 4.12, '2022-04-13', 4.12, '2022-04-13'),
(75, 31, 'SRO', '4', '2022', '01.18.05.001', '2022-05-21', 'DCACWISRO', 1.18, '2022-05-21', 1.18, '2022-05-21'),
(76, 32, 'SRO', '5', '2022', '01.18.05.001', '2022-06-03', 'DCACWISRO', 1.18, '2022-06-03', 1.18, '2022-06-03'),
(77, 33, 'SRO', '5', '2022', '04.12.05.010', '2022-06-13', 'DCACJRSRO', 4.12, '2022-06-13', 4.12, '2022-06-13'),
(78, 34, 'SRO', '6', '2022', '03.21.11.001', '2022-07-15', 'DCACNRSRO', 3.21, '2022-07-15', 3.21, '2022-07-15'),
(79, 35, 'SRO', '7', '2022', '01.18.05.001', '2022-08-10', 'DCACWISRO', 1.18, '2022-08-10', 1.18, '2022-08-10'),
(80, 36, 'SRO', '12', '2022', '04.12.05.010', '2023-01-14', 'DCACJRSRO', 4.12, '2023-01-14', 4.12, '2023-01-14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `districts`
--

CREATE TABLE `districts` (
  `id` char(7) COLLATE utf8_unicode_ci NOT NULL,
  `regency_id` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengajuan_insentive_sro`
--

CREATE TABLE `pengajuan_insentive_sro` (
  `idpengajuan` int(11) NOT NULL,
  `idkaryawan` varchar(50) DEFAULT NULL,
  `cabang` varchar(75) DEFAULT NULL,
  `bulan` varchar(75) DEFAULT NULL,
  `tahun` varchar(250) DEFAULT NULL,
  `destination` varchar(250) DEFAULT NULL,
  `fullname` varchar(250) DEFAULT NULL,
  `content` varchar(250) DEFAULT NULL,
  `relateduser` varchar(250) DEFAULT NULL,
  `insert_by` varchar(50) DEFAULT NULL,
  `insert_time` date DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `update_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengajuan_insentive_sro`
--

INSERT INTO `pengajuan_insentive_sro` (`idpengajuan`, `idkaryawan`, `cabang`, `bulan`, `tahun`, `destination`, `fullname`, `content`, `relateduser`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(29, '04.12.05.010', '641940102', '1', '2022', 'SM', ' R.Puti Kemala Dewi ', 'incentive SRO', '', '04.12.05.010', '2022-04-11', '04.12.05.010', '2022-04-11'),
(30, '04.12.05.010', '641940102', '1', '2022', 'SM', ' R.Puti Kemala Dewi ', 'incentive SRO', 'DCACJRSRO', '04.12.05.010', '2022-04-13', '04.12.05.010', '2022-04-13'),
(31, '01.18.05.001', '641940101', '4', '2022', 'SM', 'Ikrima K K Sari Putri', 'incentive SRO', 'DCACWISRO', '01.18.05.001', '2022-05-21', '01.18.05.001', '2022-05-21'),
(32, '01.18.05.001', '641940101', '5', '2022', 'SM', 'Ikrima K K Sari Putri', 'incentive SRO', 'DCACWISRO', '01.18.05.001', '2022-06-03', '01.18.05.001', '2022-06-03'),
(33, '04.12.05.010', '641940102', '5', '2022', 'SM', ' R.Puti Kemala Dewi ', 'incentive SRO', 'DCACJRSRO', '04.12.05.010', '2022-06-13', '04.12.05.010', '2022-06-13'),
(34, '03.21.11.001', '641940103', '6', '2022', 'SM', 'DINI NOVIA FIRANDANI', 'incentive SRO', 'DCACNRSRO', '03.21.11.001', '2022-07-15', '03.21.11.001', '2022-07-15'),
(35, '01.18.05.001', '641940101', '7', '2022', 'SM', 'Ikrima K K Sari Putri', 'incentive SRO', 'DCACWISRO', '01.18.05.001', '2022-08-10', '01.18.05.001', '2022-08-10'),
(36, '04.12.05.010', '641940102', '12', '2022', 'SM', ' R.Puti Kemala Dewi ', 'incentive SRO', 'DCACJRSRO', '04.12.05.010', '2023-01-14', '04.12.05.010', '2023-01-14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinces`
--

CREATE TABLE `provinces` (
  `id` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `provinces`
--

INSERT INTO `provinces` (`id`, `name`) VALUES
('00', 'bandung');

-- --------------------------------------------------------

--
-- Struktur dari tabel `regencies`
--

CREATE TABLE `regencies` (
  `id` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `province_id` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sysappdashboard`
--

CREATE TABLE `sysappdashboard` (
  `iddashboard` int(11) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `fcode` varchar(10) DEFAULT NULL,
  `nameof` varchar(50) DEFAULT NULL,
  `filename` varchar(50) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `insert_by` float DEFAULT NULL,
  `insert_time` date DEFAULT NULL,
  `update_by` float DEFAULT NULL,
  `update_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sysappmenu`
--

CREATE TABLE `sysappmenu` (
  `idmenu` int(11) NOT NULL,
  `code` varchar(5) DEFAULT NULL,
  `nameof` varchar(50) DEFAULT NULL,
  `page` varchar(50) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `submenu` varchar(5) DEFAULT NULL,
  `insert_by` double DEFAULT NULL,
  `insert_time` date DEFAULT NULL,
  `update_by` double DEFAULT NULL,
  `update_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sysappmenu`
--

INSERT INTO `sysappmenu` (`idmenu`, `code`, `nameof`, `page`, `icon`, `submenu`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(2, '102', 'Master', NULL, 'glyphicon glyphicon-th', 'Yes', NULL, NULL, NULL, '2020-02-17'),
(5, '105', 'Akunting', '', '', 'Yes', NULL, NULL, NULL, '2021-08-19'),
(7, '107', 'Akses System', NULL, NULL, 'Yes', NULL, NULL, NULL, '2020-02-17'),
(11, '108', 'Laporan', NULL, NULL, 'Yes', NULL, NULL, NULL, NULL),
(20, 'DSNM', 'Sales & Marketing', NULL, NULL, 'Yes', 17, '2021-09-22', NULL, NULL),
(22, 'INC-0', 'Incentive', NULL, NULL, NULL, 61, '2022-04-11', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sysappmenuitem`
--

CREATE TABLE `sysappmenuitem` (
  `idmenuitem` int(11) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `fcode` varchar(10) DEFAULT NULL,
  `nameof` varchar(50) DEFAULT NULL,
  `filename` varchar(50) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `ketgroup` varchar(30) DEFAULT NULL,
  `grouplevel` varchar(5) DEFAULT NULL,
  `insert_by` float DEFAULT NULL,
  `insert_time` date DEFAULT NULL,
  `update_by` float DEFAULT NULL,
  `update_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sysappmenuitem`
--

INSERT INTO `sysappmenuitem` (`idmenuitem`, `code`, `fcode`, `nameof`, `filename`, `icon`, `ketgroup`, `grouplevel`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(1, '027', '102', 'Pelajaran', 'cpelajaran/tampil', 'glyphicon glyphicon-star-empty', NULL, NULL, NULL, NULL, 1, '2021-09-01'),
(2, '015', '107', 'Sub Menu', 'csubmenu/tampil', 'glyphicon glyphicon-star-empty', NULL, NULL, NULL, NULL, 1, '2020-02-15'),
(3, '005', '102', 'Akun', 'cakun/tampil', 'glyphicon glyphicon-star-empty', NULL, NULL, NULL, NULL, 1, '2021-09-01'),
(4, '004', '102', 'Jadwal', 'cjadwal/tampil', 'glyphicon glyphicon-star-empty', NULL, NULL, NULL, NULL, 1, '2020-02-05'),
(5, '010', '102', 'Akun Transaksi', 'cakuntransaksi/tampil', 'glyphicon glyphicon-star-empty', NULL, NULL, NULL, NULL, 1, '2021-09-01'),
(6, '006', '105', 'Transaksi', 'ctransaksi/tampil', 'glyphicon glyphicon-star-empty', NULL, NULL, NULL, NULL, 1, '2021-08-19'),
(7, '007', '102', 'List Item Registrasi', 'clistregistrasi/tampil', 'glyphicon glyphicon-star-empty', NULL, NULL, NULL, NULL, 1, '2020-02-05'),
(8, '008', '105', 'Registrasi', 'cregistrasi/tampil', 'glyphicon glyphicon-star-empty', NULL, NULL, NULL, NULL, 1, '2021-08-19'),
(9, '009', '105', 'Angsuran', 'cangsuran/tampil', 'glyphicon glyphicon-star-empty', NULL, NULL, NULL, NULL, 1, '2021-08-19'),
(10, '024', '102', 'Sikap', 'csikap/tampil', 'glyphicon glyphicon-star-empty', NULL, NULL, NULL, NULL, 1, '2021-09-01'),
(11, '011', '102', 'Karyawan', 'ckaryawan/tampil', 'glyphicon glyphicon-star-empty', NULL, NULL, NULL, NULL, 17, '2021-09-21'),
(15, '002', '107', 'Main Menu', 'cmenu/tampil', 'glyphicon glyphicon-star-empty', NULL, NULL, NULL, NULL, 1, '2020-02-15'),
(23, '022', '105', 'Gaji Pokok', 'cgajipokok/tampil', 'glyphicon glyphicon-star-empty', NULL, NULL, 1, '2020-02-05', 1, '2020-02-12'),
(26, '003', '102', 'Jabatan', 'cjabatan/tampil', 'glyphicon glyphicon-star-empty', NULL, NULL, 1, '2020-02-10', 1, '2021-09-01'),
(27, '026', '107', 'Hak Akses', 'chakakses/tampil', 'glyphicon glyphicon-star-empty', NULL, NULL, 1, '2020-02-15', 1, '2020-02-15'),
(28, '025', '107', 'User', 'cuser/tampil', NULL, NULL, NULL, 1, '2020-02-15', 1, '2020-02-17'),
(29, '001', '102', 'Pendidikan', 'cpendidikan/tampil', '', NULL, NULL, 1, '2020-02-19', 1, '2021-09-01'),
(37, '035', '106', 'Angsuran', 'cangsuran/tampil', '', NULL, NULL, NULL, '2020-04-11', 17, '2021-09-16'),
(43, '151', '115', 'Profile', 'admin_personal', '', NULL, NULL, NULL, '2020-05-09', NULL, '2020-05-09'),
(44, '152', '115', 'Ijin', 'cijin_personal/tampil', '', NULL, NULL, NULL, '2020-05-09', NULL, '2020-05-14'),
(51, '040', '102', 'Tugas', 'ctugas/tampil', '', NULL, NULL, NULL, '2021-08-20', NULL, '2021-08-20'),
(52, '041', '102', 'Peserta', 'cpeserta/tampil', '', NULL, NULL, NULL, '2021-09-01', NULL, '2021-09-01'),
(53, '042', '106', 'Nilai Harian Siswa', 'cnilaiharian/tampilnilai_siswa', '', NULL, NULL, NULL, '2021-09-06', 18, '2021-09-15'),
(54, '043', '106', 'Nilai Ujian Siswa', 'cnilaiakhir/tampilnilai_siswa', '', NULL, NULL, NULL, '2021-09-06', 18, '2021-09-16'),
(67, 'DSNM-D', 'DSNM', 'Dashobard', 'dmss/tampil', NULL, NULL, NULL, 17, '2021-09-22', NULL, NULL),
(68, 'DSNM-R', 'DSNM', 'Sales Perfomance', 'csalesperfomance/tampil', NULL, NULL, NULL, 17, '2021-09-22', NULL, NULL),
(69, 'cabang', '102', 'Cabang', 'ccabang/tampil', NULL, NULL, NULL, 17, '2021-09-23', NULL, NULL),
(70, 'DMSSLEW', 'DSNM', 'Evaluasi Wiraniaga', 'cevaluasiWiraniaga/tampil', NULL, NULL, NULL, 61, '2021-10-08', NULL, NULL),
(71, 'DMSSCCSPKD', 'DSNM', 'Chart Control SPK DO', 'cchartControlSPKDO/tampil', NULL, NULL, NULL, 61, '2021-10-08', NULL, NULL),
(73, 'DMSSCO', 'DSNM', 'Control Outlet', 'cpapanControlOutlet/tampil', NULL, NULL, NULL, 61, '2021-10-08', NULL, NULL),
(74, 'DMSSPM', 'DSNM', 'Planing Marketing', 'cplaningMarketing/tampil', NULL, NULL, NULL, 61, '2021-10-09', NULL, NULL),
(77, 'DSNM-targe', 'DSNM', 'Target DO', 'ctargetDO/tampil', NULL, NULL, NULL, 61, '2021-11-19', NULL, NULL),
(78, 'DMNS-targe', 'DSNM', 'Target Unit', 'ctargetunitentry/tampil', NULL, NULL, NULL, 61, '2021-11-22', NULL, NULL),
(79, 'DMSS-D1', 'DSNM', 'Dashboard 1', 'dmss/tampil1', NULL, NULL, NULL, 61, '2021-12-22', NULL, NULL),
(81, 'ISROD', 'INC-0', 'SRO', 'cdetail_incenvite_sro/tampil', NULL, NULL, NULL, 61, '2022-04-11', NULL, NULL),
(82, 'Lincentive', 'INC-0', 'List Incentive SRO', 'clistdataincentivesro/tampil', NULL, NULL, NULL, 61, '2022-04-11', NULL, NULL),
(83, 'INC-Servic', 'INC-0', 'Incentive Service', 'cIncentiveService/tampil', NULL, NULL, NULL, 61, '2023-07-16', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sysuser`
--

CREATE TABLE `sysuser` (
  `iduser` int(11) NOT NULL,
  `idkaryawan` float DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `username` varchar(75) DEFAULT NULL,
  `nama` varchar(75) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `insert_by` float DEFAULT NULL,
  `insert_time` date DEFAULT NULL,
  `update_by` float DEFAULT NULL,
  `update_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sysuser`
--

INSERT INTO `sysuser` (`iduser`, `idkaryawan`, `nik`, `username`, `nama`, `password`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(53, 10, '01245', 'admin', 'Asep Gumilar', 'c3Vuc29uMjAyMA==', 1, '2020-02-17', NULL, '2020-03-01'),
(54, 9, '234', 'Eko', 'Asep', 'Z3Vtcw==', 1, '2020-02-17', NULL, '2020-03-01'),
(56, 1706, 'D13', 'Personalia', 'AGHNIA DYANAH NURFADILAH FACHRUROJI', 'cGVyc29uYWxpYQ==', 53, '2020-03-18', NULL, NULL),
(57, 1671, '388', 'Upah', 'IIS RAHMAYANIS', 'dXBhaDIwMjA=', NULL, '2020-04-14', NULL, NULL),
(58, 1659, '074', '074', 'IKAH KARTIKAH', 'MDc0MjAyMA==', NULL, '2020-04-15', NULL, NULL),
(60, 19, '003', '0003', 'idris', 'bmFuZGhpcmExOQ==', 17, '2021-09-23', NULL, NULL),
(61, 17, '0001', 'nandhira', 'Teguh Ariep', 'bmFuZGhpcmExOQ==', 17, '2021-09-24', NULL, NULL),
(62, 22, '00.12.01.075', 'GM001', 'PAULUS PRIHARTONO', 'bGV2aXRoYTExMDgxOTk4', 61, '2021-11-17', NULL, NULL),
(63, 23, '14.21.04.221', 'ALDO123', 'ALDO YOGASMARA', 'QUxETzEyMw==', 62, '2021-11-17', NULL, NULL),
(64, NULL, NULL, NULL, NULL, NULL, 61, '2021-11-19', NULL, NULL),
(65, 24, '00.12.01.075X', '00.12.01.075X', 'PAULUS PRIHARTONO X', 'MDAuMTIuMDEuMDc1WA==', 61, '2021-12-22', NULL, NULL),
(66, 25, '01.19.08.102', '01.19.08.102', 'RONALD NOVEMBRI W', 'MDEuMTkuMDguMTAy', 61, '2021-12-22', NULL, NULL),
(67, 26, '01.21.09.001', '01.21.09.001', 'Marjoni, SE', 'UEBzc3cwcmQ=', 61, '2021-12-22', NULL, NULL),
(68, 27, '04.09.01.001', '04.09.01.001', 'RUBBY SAPUTRA', 'MDQuMDkuMDEuMDAx', 61, '2021-12-22', NULL, NULL),
(69, 28, '06.21.03.001', '06.21.03.001', 'ARIA BHARATA, S.E', 'MDYuMjEuMDMuMDAx', 61, '2021-12-22', NULL, NULL),
(70, 29, '01.12.01.001', '01.12.01.001', 'ROPIK ARROHMAN', 'MDEuMTIuMDEuMDAx', 61, '2021-12-22', NULL, NULL),
(71, 30, '01.12.01.023', '01.12.01.023', 'REDDY SUWANTO', 'MDEuMTIuMDEuMDIz', 61, '2021-12-22', NULL, NULL),
(72, 31, '01.13.01.054', '01.13.01.054', 'IRFAN KURNIAWAN', 'MDEuMTMuMDEuMDU0', 61, '2021-12-22', NULL, NULL),
(73, 32, '01.18.01.027', '01.18.01.027', 'ZULKARNAEN', 'MDEuMTguMDEuMDI3', 61, '2021-12-22', NULL, NULL),
(74, 33, '01.19.01.015', '01.19.01.015', 'CHRIS ARCHY', 'MDEuMTkuMDEuMDE1', 61, '2021-12-22', NULL, NULL),
(75, 34, '01.19.01.045', '01.19.01.045', 'MUHAMAD DAHLAN', 'MDEuMTkuMDEuMDQ1', 61, '2021-12-22', NULL, NULL),
(76, 35, '01.19.08.102X', '01.19.08.102X', 'RONALD NOVEMBRI W X', 'MDEuMTkuMDguMTAyWA==', 61, '2021-12-22', NULL, NULL),
(77, 36, '03.21.01.001', '03.21.01.001', 'AGUS SULANTIYO', 'MDMuMjEuMDEuMDAx', 61, '2021-12-22', NULL, NULL),
(78, 37, '04.09.01.001X', '04.09.01.001X', 'RUBBY SAPUTRA X', 'MDQuMDkuMDEuMDAxWA==', 61, '2021-12-22', NULL, NULL),
(79, 38, '04.17.01.035', '04.17.01.035', 'SALEH MADANI', 'MDQuMTcuMDEuMDM1', 61, '2021-12-22', NULL, NULL),
(80, 39, '04.17.01.036', '04.17.01.036', 'HENDRIX SANTOSA', 'MDQuMTcuMDEuMDM2', 61, '2021-12-22', NULL, NULL),
(81, 40, '04.19.01.147', '04.19.01.147', 'HADE WIBAWA', 'MDQuMTkuMDEuMTQ3', 61, '2021-12-22', NULL, NULL),
(82, 41, '06.21.03.001X', '06.21.03.001X', 'ARIA BHARATA, S.E X', 'MDYuMjEuMDMuMDAxWA==', 61, '2021-12-22', NULL, NULL),
(83, 42, '14.19.01.097', '14.19.01.097', 'IRVAN ZAENAL MUTAQIN', 'MTQuMTkuMDEuMDk3', 61, '2021-12-22', NULL, NULL),
(84, 43, 'Own-1', 'Vincent', 'Vincent', 'c2FuZHlURDE3Nzc4IQ==', 0, '2021-12-30', NULL, NULL),
(85, 44, 'Own-2', 'Ine', 'Evelyn', 'MTIzNDU2', 0, '2021-12-30', NULL, NULL),
(86, 45, 'Own-3', 'evlyn', 'Ine', 'MTIzNDU2', 0, '2021-12-30', NULL, NULL),
(87, 46, 'KS-001', 'KS-001', 'Ade Anggraeni', 'S1MtMDAx', 61, '2022-01-10', NULL, NULL),
(88, 47, '01.09.02.00.21', 'FAD1', 'SUGANDA CHANDRA ', 'RkFEMQ==', 61, '2022-01-24', NULL, NULL),
(89, 48, '04.12.05.010', '04.12.05.010', ' R.Puti Kemala Dewi ', 'MDQuMTIuMDUuMDEw', 61, '2022-04-11', NULL, NULL),
(90, 49, '01.18.05.001', '01.18.05.001', 'Ikrima K K Sari Putri', 'MDEuMTguMDUuMDAx', 61, '2022-04-11', NULL, NULL),
(91, 50, '03.21.11.001', '03.21.11.001', 'DINI NOVIA FIRANDANI', 'MDMuMjEuMTEuMDAx', 61, '2022-04-11', NULL, NULL),
(92, 51, '01.21.09.002', '01.21.09.002', 'HERI SETIAWAN', 'MDEuMjEuMDkuMDAy', 61, '2022-04-11', NULL, NULL),
(93, 52, '03.18.05.457', '03.18.05.457', 'Kusdiyantoro ST', 'MDMuMTguMDUuNDU3', 61, '2022-04-11', NULL, NULL),
(94, 53, '04.09.05.006', '04.09.05.006', 'Ganda Mulyadi', 'MDQuMDkuMDUuMDA2', 61, '2022-04-11', NULL, NULL),
(95, 54, '01.21.06.001', '01.21.06.001', 'CLARISSA', 'MDEuMjEuMDYuMDAx', 61, '2022-04-11', NULL, NULL),
(96, 55, '03.13.00.034', '03.13.00.034', 'Budi Tanujaya', 'MDMuMTMuMDAuMDM0', 61, '2022-04-11', NULL, NULL),
(97, 56, '00.13.02.005', '00.13.02.005', 'Liana Respati', 'MDAuMTMuMDIuMDA1', 61, '2022-04-11', NULL, NULL),
(98, 57, '03.22.03.001', '03.22.03.001', 'Ety Cia', 'MDMuMjIuMDMuMDAx', 61, '2022-04-12', NULL, NULL),
(101, 59, '04.14.01.022', '04.14.01.022', 'Dedi Sopiyan', 'MDQuMTQuMDEuMDIy', 61, '2023-02-24', NULL, NULL),
(102, 60, '14.22.10.291', '14.22.10.291', 'Lucky Ekaputra', 'MTQuMjIuMTAuMjkx', 61, '2023-05-31', NULL, NULL),
(103, 61, '04.09.05.005', '04.09.05.005', 'agustina wulandari', 'MDQuMDkuMDUuMDA1', 61, '2023-07-16', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sysuseraccess`
--

CREATE TABLE `sysuseraccess` (
  `idjabatan` decimal(10,0) DEFAULT NULL,
  `fcode` varchar(10) DEFAULT NULL,
  `update_by` decimal(10,0) DEFAULT NULL,
  `update_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sysuseraccess`
--

INSERT INTO `sysuseraccess` (`idjabatan`, `fcode`, `update_by`, `update_time`) VALUES
('12', '115', '1659', '2020-05-29'),
('12', '153', '1659', '2020-05-29'),
('12', '151', '1659', '2020-05-29'),
('12', '152', '1659', '2020-05-29'),
('12', '155', '1659', '2020-05-29'),
('12', '154', '1659', '2020-05-29'),
('10', '115', '1659', '2020-05-29'),
('10', '151', '1659', '2020-05-29'),
('10', '155', '1659', '2020-05-29'),
('10', '154', '1659', '2020-05-29'),
('10', '152', '1659', '2020-05-29'),
('10', '153', '1659', '2020-05-29'),
('16', '115', '1659', '2020-06-03'),
('16', '151', '1659', '2020-06-03'),
('16', '153', '1659', '2020-06-03'),
('16', '152', '1659', '2020-06-03'),
('16', '154', '1659', '2020-06-03'),
('16', '155', '1659', '2020-06-03'),
('15', '154', '1659', '2020-06-03'),
('15', '151', '1659', '2020-06-03'),
('15', '155', '1659', '2020-06-03'),
('15', '153', '1659', '2020-06-03'),
('15', '115', '1659', '2020-06-03'),
('15', '152', '1659', '2020-06-03'),
('14', '152', '1659', '2020-06-03'),
('14', '154', '1659', '2020-06-03'),
('14', '155', '1659', '2020-06-03'),
('14', '153', '1659', '2020-06-03'),
('14', '151', '1659', '2020-06-03'),
('14', '115', '1659', '2020-06-03'),
('13', '115', '1659', '2020-06-03'),
('13', '151', '1659', '2020-06-03'),
('13', '152', '1659', '2020-06-03'),
('13', '154', '1659', '2020-06-03'),
('13', '155', '1659', '2020-06-03'),
('13', '153', '1659', '2020-06-03'),
('9', '101', '1659', '2020-12-02'),
('9', '029', '1659', '2020-12-02'),
('9', '001', '1659', '2020-12-02'),
('9', '003', '1659', '2020-12-02'),
('9', '102', '1659', '2020-12-02'),
('9', '004', '1659', '2020-12-02'),
('9', '005', '1659', '2020-12-02'),
('9', '006', '1659', '2020-12-02'),
('9', '007', '1659', '2020-12-02'),
('9', '009', '1659', '2020-12-02'),
('9', '010', '1659', '2020-12-02'),
('9', '008', '1659', '2020-12-02'),
('9', '024', '1659', '2020-12-02'),
('9', '027', '1659', '2020-12-02'),
('9', '030', '1659', '2020-12-02'),
('9', '104', '1659', '2020-12-02'),
('9', '011', '1659', '2020-12-02'),
('9', '012', '1659', '2020-12-02'),
('9', '013', '1659', '2020-12-02'),
('9', '014', '1659', '2020-12-02'),
('9', '016', '1659', '2020-12-02'),
('9', '019', '1659', '2020-12-02'),
('9', '020', '1659', '2020-12-02'),
('9', '021', '1659', '2020-12-02'),
('9', '028', '1659', '2020-12-02'),
('9', '031', '1659', '2020-12-02'),
('9', '032', '1659', '2020-12-02'),
('9', '033', '1659', '2020-12-02'),
('9', '041', '1659', '2020-12-02'),
('9', '040', '1659', '2020-12-02'),
('9', '023', '1659', '2020-12-02'),
('9', '105', '1659', '2020-12-02'),
('9', '022', '1659', '2020-12-02'),
('9', '015', '1659', '2020-12-02'),
('9', '026', '1659', '2020-12-02'),
('9', '107', '1659', '2020-12-02'),
('9', '002', '1659', '2020-12-02'),
('9', '025', '1659', '2020-12-02'),
('9', '035', '1659', '2020-12-02'),
('9', '034', '1659', '2020-12-02'),
('9', '037', '1659', '2020-12-02'),
('9', '038', '1659', '2020-12-02'),
('9', '036', '1659', '2020-12-02'),
('9', '106', '1659', '2020-12-02'),
('9', '039', '1659', '2020-12-02'),
('9', '108', '1659', '2020-12-02'),
('9', '115', '1659', '2020-12-02'),
('9', '151', '1659', '2020-12-02'),
('9', '152', '1659', '2020-12-02'),
('9', '153', '1659', '2020-12-02'),
('9', '154', '1659', '2020-12-02'),
('9', '155', '1659', '2020-12-02'),
('17', '151', '1659', '2121-04-28'),
('17', '155', '1659', '2121-04-28'),
('17', '154', '1659', '2121-04-28'),
('17', '115', '1659', '2121-04-28'),
('17', '153', '1659', '2121-04-28'),
('17', '152', '1659', '2121-04-28'),
('17', '156', '1659', '2121-04-28'),
('0', '0', '0', '2121-09-05'),
('19', '102', NULL, NULL),
('19', '027', NULL, NULL),
('19', '005', NULL, NULL),
('19', '004', NULL, NULL),
('19', '010', NULL, NULL),
('19', '007', NULL, NULL),
('19', '024', NULL, NULL),
('19', '003', NULL, NULL),
('19', '001', NULL, NULL),
('19', '040', NULL, NULL),
('19', '041', NULL, NULL),
('19', '101', NULL, NULL),
('19', '029', NULL, NULL),
('19', '104', NULL, NULL),
('19', '011', NULL, NULL),
('19', '012', NULL, NULL),
('19', '013', NULL, NULL),
('19', '014', NULL, NULL),
('19', '016', NULL, NULL),
('19', '020', NULL, NULL),
('19', '028', NULL, NULL),
('19', '042', NULL, NULL),
('19', '043', NULL, NULL),
('19', '105', NULL, NULL),
('19', '006', NULL, NULL),
('19', '008', NULL, NULL),
('19', '009', NULL, NULL),
('19', '022', NULL, NULL),
('19', '023', NULL, NULL),
('19', '044', NULL, NULL),
('19', '107', NULL, NULL),
('19', '015', NULL, NULL),
('19', '002', NULL, NULL),
('19', '026', NULL, NULL),
('19', '025', NULL, NULL),
('19', '106', NULL, NULL),
('19', '034', NULL, NULL),
('19', '035', NULL, NULL),
('19', '036', NULL, NULL),
('19', '037', NULL, NULL),
('19', '108', NULL, NULL),
('19', '038', NULL, NULL),
('19', '039', NULL, NULL),
('19', '115', NULL, NULL),
('19', '151', NULL, NULL),
('19', '152', NULL, NULL),
('22', 'DSNM-D', NULL, NULL),
('22', 'DSNM-R', NULL, NULL),
('22', 'DMSSLEW', NULL, NULL),
('22', 'DMSSCCSPKD', NULL, NULL),
('22', 'DMSSCO', NULL, NULL),
('22', 'DMSSPM', NULL, NULL),
('22', 'DSNM', NULL, NULL),
('24', '011', NULL, NULL),
('24', '003', NULL, NULL),
('24', '107', NULL, NULL),
('24', '015', NULL, NULL),
('24', '002', NULL, NULL),
('24', '026', NULL, NULL),
('24', '025', NULL, NULL),
('24', 'DSNM', NULL, NULL),
('24', 'DSNM-D', NULL, NULL),
('24', 'DSNM-R', NULL, NULL),
('24', 'DMSSLEW', NULL, NULL),
('24', 'DMSSCCSPKD', NULL, NULL),
('24', 'DMSSCO', NULL, NULL),
('24', 'DMSSPM', NULL, NULL),
('24', 'DSNM-targe', NULL, NULL),
('24', 'DMNS-targe', NULL, NULL),
('24', 'DMSS-D1', NULL, NULL),
('24', '001', NULL, NULL),
('47', 'DMSS-D1', NULL, NULL),
('46', 'DMSS-D1', NULL, NULL),
('45', 'DMSS-D1', NULL, NULL),
('49', 'DMSS-D1', NULL, NULL),
('50', 'DMSS-D1', NULL, NULL),
('51', 'ISROD', NULL, NULL),
('52', 'Lincentive', NULL, NULL),
('54', 'Lincentive', NULL, NULL),
('44', 'DMSS-D1', NULL, NULL),
('44', 'Lincentive', NULL, NULL),
('53', 'Lincentive', NULL, NULL),
('53', 'DSNM-D', NULL, NULL),
('23', '011', NULL, NULL),
('23', '003', NULL, NULL),
('23', '015', NULL, NULL),
('23', '002', NULL, NULL),
('23', '026', NULL, NULL),
('23', '025', NULL, NULL),
('23', '107', NULL, NULL),
('23', '001', NULL, NULL),
('23', 'DSNM', NULL, NULL),
('23', 'DSNM-D', NULL, NULL),
('23', 'DSNM-R', NULL, NULL),
('23', 'DMSSLEW', NULL, NULL),
('23', 'DMSSCCSPKD', NULL, NULL),
('23', 'DMSSCO', NULL, NULL),
('23', 'DMSSPM', NULL, NULL),
('23', 'DSNM-targe', NULL, NULL),
('23', 'DMNS-targe', NULL, NULL),
('23', 'DMSS-D1', NULL, NULL),
('23', 'cabang', NULL, NULL),
('23', 'INC-0', NULL, NULL),
('23', 'ISROD', NULL, NULL),
('23', 'Lincentive', NULL, NULL),
('23', 'INC-Servic', NULL, NULL),
('55', 'ISROD', NULL, NULL),
('55', 'INC-Servic', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sysuseraccess1`
--

CREATE TABLE `sysuseraccess1` (
  `idkaryawan` decimal(10,0) DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `fcode` varchar(10) DEFAULT NULL,
  `update_by` decimal(10,0) DEFAULT NULL,
  `update_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sysuseraccess1`
--

INSERT INTO `sysuseraccess1` (`idkaryawan`, `nik`, `fcode`, `update_by`, `update_time`) VALUES
('1706', 'D13', '004', '0', '2020-04-13'),
('1706', 'D13', '005', '0', '2020-04-13'),
('1706', 'D13', '102', '0', '2020-04-13'),
('1706', 'D13', '007', '0', '2020-04-13'),
('1706', 'D13', '008', '0', '2020-04-13'),
('1706', 'D13', '009', '0', '2020-04-13'),
('1706', 'D13', '010', '0', '2020-04-13'),
('1706', 'D13', '024', '0', '2020-04-13'),
('1706', 'D13', '027', '0', '2020-04-13'),
('1706', 'D13', '030', '0', '2020-04-13'),
('1706', 'D13', '011', '0', '2020-04-13'),
('1706', 'D13', '019', '0', '2020-04-13'),
('1706', 'D13', '020', '0', '2020-04-13'),
('1706', 'D13', '021', '0', '2020-04-13'),
('1706', 'D13', '031', '0', '2020-04-13'),
('1706', 'D13', '032', '0', '2020-04-13'),
('1706', 'D13', '035', '0', '2020-04-13'),
('1706', 'D13', '036', '0', '2020-04-13'),
('1706', 'D13', '037', '0', '2020-04-13'),
('1706', 'D13', '003', '0', '2020-04-13'),
('1706', 'D13', '001', '0', '2020-04-13'),
('1706', 'D13', '006', '0', '2020-04-13'),
('1659', '074', '029', '0', '2020-04-15'),
('1659', '074', '101', '0', '2020-04-15'),
('1659', '074', '031', '0', '2020-04-15'),
('1659', '074', '036', '0', '2020-04-15'),
('1659', '074', '032', '0', '2020-04-15'),
('1659', '074', '013', '0', '2020-04-15'),
('1659', '074', '020', '0', '2020-04-15'),
('1659', '074', '012', '0', '2020-04-15'),
('1659', '074', '019', '0', '2020-04-15'),
('1659', '074', '035', '0', '2020-04-15'),
('1659', '074', '037', '0', '2020-04-15'),
('1671', '388', '024', '0', '2020-04-22'),
('1671', '388', '022', '0', '2020-04-22'),
('1671', '388', '023', '0', '2020-04-22'),
('1671', '388', '013', '0', '2020-04-22'),
('1671', '388', '030', '0', '2020-04-22'),
('1671', '388', '105', '0', '2020-04-22'),
('1671', '388', '034', '0', '2020-04-22'),
('1671', '388', '012', '0', '2020-04-22'),
('1671', '388', '028', '0', '2020-04-22'),
('1671', '388', '035', '0', '2020-04-22'),
('1671', '388', '036', '0', '2020-04-22'),
('1671', '388', '016', '0', '2020-04-22'),
('10', '01245', '005', '0', '2020-04-27'),
('10', '01245', '013', '0', '2020-04-27'),
('10', '01245', '008', '0', '2020-04-27'),
('10', '01245', '004', '0', '2020-04-27'),
('10', '01245', '003', '0', '2020-04-27'),
('10', '01245', '001', '0', '2020-04-27'),
('10', '01245', '012', '0', '2020-04-27'),
('10', '01245', '002', '0', '2020-04-27'),
('10', '01245', '009', '0', '2020-04-27'),
('10', '01245', '010', '0', '2020-04-27'),
('10', '01245', '014', '0', '2020-04-27'),
('10', '01245', '006', '0', '2020-04-27'),
('10', '01245', '011', '0', '2020-04-27'),
('10', '01245', '027', '0', '2020-04-27'),
('10', '01245', '007', '0', '2020-04-27'),
('10', '01245', '024', '0', '2020-04-27'),
('10', '01245', '102', '0', '2020-04-27'),
('10', '01245', '034', '0', '2020-04-27'),
('10', '01245', '016', '0', '2020-04-27'),
('10', '01245', '030', '0', '2020-04-27'),
('10', '01245', '035', '0', '2020-04-27'),
('10', '01245', '036', '0', '2020-04-27'),
('10', '01245', '105', '0', '2020-04-27'),
('10', '01245', '022', '0', '2020-04-27'),
('10', '01245', '023', '0', '2020-04-27'),
('10', '01245', '020', '0', '2020-04-27'),
('10', '01245', '026', '0', '2020-04-27'),
('10', '01245', '015', '0', '2020-04-27'),
('10', '01245', '107', '0', '2020-04-27'),
('10', '01245', '019', '0', '2020-04-27'),
('10', '01245', '025', '0', '2020-04-27'),
('10', '01245', '101', '0', '2020-04-27'),
('10', '01245', '029', '0', '2020-04-27'),
('10', '01245', '031', '0', '2020-04-27'),
('10', '01245', '021', '0', '2020-04-27'),
('10', '01245', '032', '0', '2020-04-27'),
('10', '01245', '033', '0', '2020-04-27'),
('10', '01245', '106', '0', '2020-04-27'),
('10', '01245', '037', '0', '2020-04-27'),
('10', '01245', '028', '0', '2020-04-27'),
('10', '01245', '108', '0', '2020-04-27'),
('10', '01245', '038', '0', '2020-04-27'),
('10', '01245', '040', '0', '2020-04-27'),
('10', '01245', '039', '0', '2020-04-27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabsen`
--

CREATE TABLE `tabsen` (
  `idabsen` int(11) NOT NULL,
  `idkaryawan` int(11) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `masuk` time DEFAULT NULL,
  `keluar` time DEFAULT NULL,
  `jml_jam` int(11) DEFAULT NULL,
  `idjadwal` int(11) DEFAULT NULL,
  `flag_full_absen` varchar(2) DEFAULT NULL COMMENT 'absen masuk dan keluar terisi maka di set 1',
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabsensiswa`
--

CREATE TABLE `tabsensiswa` (
  `idabsensiswa` int(11) NOT NULL,
  `kdabsensiswa` varchar(10) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `idpengajar` int(11) DEFAULT NULL,
  `idkelas` int(11) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabsensiswa`
--

INSERT INTO `tabsensiswa` (`idabsensiswa`, `kdabsensiswa`, `tgl`, `idpengajar`, `idkelas`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(24, '2109003', '2021-09-15', 18, 4, 18, '2021-09-15 19:23:09', 18, '2021-09-15 19:35:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabsensiswadtl`
--

CREATE TABLE `tabsensiswadtl` (
  `idabsensiswadtl` int(11) NOT NULL,
  `idabsensiswa` int(11) DEFAULT NULL,
  `idpeserta` int(11) DEFAULT NULL,
  `absen` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabsensiswadtl`
--

INSERT INTO `tabsensiswadtl` (`idabsensiswadtl`, `idabsensiswa`, `idpeserta`, `absen`) VALUES
(25, 24, 4, 'checked');

-- --------------------------------------------------------

--
-- Struktur dari tabel `takun`
--

CREATE TABLE `takun` (
  `idakun` int(11) NOT NULL,
  `kdakun` varchar(10) DEFAULT NULL,
  `akun` varchar(255) DEFAULT NULL,
  `db` int(11) DEFAULT NULL COMMENT 'debet/kredit',
  `ket` varchar(150) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `takun`
--

INSERT INTO `takun` (`idakun`, `kdakun`, `akun`, `db`, `ket`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(18, '0001', 'Pembelanjaan ATK', 2, 'Pembelanjaan ATK', NULL, NULL, NULL, NULL);

--
-- Trigger `takun`
--
DELIMITER $$
CREATE TRIGGER `takun` AFTER DELETE ON `takun` FOR EACH ROW BEGIN
	
	DELETE FROM takundtl WHERE idakun = OLD.idakun;
	
		
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `takundtl`
--

CREATE TABLE `takundtl` (
  `idakundtl` int(11) NOT NULL,
  `idakun` int(11) DEFAULT NULL,
  `kdakundtl` varchar(10) NOT NULL,
  `akundtl` varchar(150) DEFAULT NULL,
  `satuan` varchar(25) DEFAULT NULL,
  `db` int(11) DEFAULT NULL COMMENT 'debet/kredit',
  `ket` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `takundtl`
--

INSERT INTO `takundtl` (`idakundtl`, `idakun`, `kdakundtl`, `akundtl`, `satuan`, `db`, `ket`) VALUES
(11, 18, '001', 'Beli Penghapus Putih', 'Buah', 2, NULL),
(12, 18, '002', 'Beli Papan Tulis', 'Buah', 2, NULL),
(13, 18, '003', 'Beli Alat Tulis', 'Buah', 2, NULL),
(18, 18, '004', 'Beli Penghapus Papan Tulis', 'Kg', 2, NULL),
(19, 18, '005', 'Dispenser', 'Set', 2, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `takun_transaksi`
--

CREATE TABLE `takun_transaksi` (
  `idakun_transaksi` int(11) NOT NULL,
  `thn_ajaran` varchar(50) NOT NULL,
  `tgl` date DEFAULT NULL,
  `idakun` int(10) DEFAULT NULL,
  `pagu` float DEFAULT NULL,
  `flag` varchar(1) DEFAULT NULL COMMENT 'aktif/tidak',
  `flag_over` varchar(1) DEFAULT NULL COMMENT 'over budget',
  `ket` varchar(150) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `takun_transaksi`
--

INSERT INTO `takun_transaksi` (`idakun_transaksi`, `thn_ajaran`, `tgl`, `idakun`, `pagu`, `flag`, `flag_over`, `ket`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(71, '2021-2022', '2021-08-07', 12, 3500000, '0', NULL, NULL, NULL, NULL, NULL, NULL),
(72, '2021-2022', '2021-08-07', 13, 2450000, '0', NULL, NULL, NULL, NULL, NULL, NULL),
(73, '2019-2020', '2021-08-07', 12, 2500000, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, '2021-2022', '2021-08-07', 11, 1500000, '0', NULL, NULL, NULL, NULL, NULL, NULL),
(75, '2021-2022', '2021-08-07', 19, 250000, '0', NULL, NULL, NULL, NULL, NULL, NULL),
(86, '2022-2023', '2021-08-12', 18, 3500, '1', NULL, NULL, NULL, NULL, NULL, NULL),
(87, '2022-2023', '2021-08-12', 19, 420000, '1', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tangsuran`
--

CREATE TABLE `tangsuran` (
  `idangsuran` int(11) NOT NULL,
  `tgl` date DEFAULT NULL,
  `nota` varchar(15) DEFAULT NULL,
  `idpeserta` int(11) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tangsuran`
--

INSERT INTO `tangsuran` (`idangsuran`, `tgl`, `nota`, `idpeserta`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(88, '2021-08-18', '2108001', 4, NULL, NULL, NULL, NULL),
(89, '2021-08-18', '2108002', 4, NULL, NULL, NULL, NULL),
(90, '2021-08-19', '2108003', 4, NULL, NULL, NULL, NULL),
(93, '2021-08-19', '2108004', 4, NULL, NULL, NULL, NULL);

--
-- Trigger `tangsuran`
--
DELIMITER $$
CREATE TRIGGER `tangsuran` AFTER DELETE ON `tangsuran` FOR EACH ROW BEGIN
	
	DELETE FROM tangsurandtl WHERE idangsuran = OLD.idangsuran;
	
		
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tangsurandtl`
--

CREATE TABLE `tangsurandtl` (
  `idangsurandtl` int(11) NOT NULL,
  `idangsuran` int(11) DEFAULT NULL,
  `idakun` int(11) DEFAULT NULL,
  `angsuran` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tangsurandtl`
--

INSERT INTO `tangsurandtl` (`idangsurandtl`, `idangsuran`, `idakun`, `angsuran`) VALUES
(85, 88, 19, 1100),
(86, 89, 19, 300000),
(87, 90, 18, 500),
(90, 93, 19, 100000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `targetdo`
--

CREATE TABLE `targetdo` (
  `idtargetdo` int(10) NOT NULL,
  `tahun` int(11) DEFAULT NULL,
  `cabang` varchar(50) DEFAULT NULL,
  `tipekendaraan` varchar(50) DEFAULT NULL,
  `modelkendaraan` varchar(50) DEFAULT NULL,
  `jan` int(11) DEFAULT 0,
  `feb` int(11) DEFAULT 0,
  `mar` int(11) DEFAULT 0,
  `apr` int(11) DEFAULT 0,
  `mei` int(11) DEFAULT 0,
  `jun` int(11) DEFAULT 0,
  `jul` int(11) DEFAULT 0,
  `agu` int(11) DEFAULT 0,
  `sep` int(11) DEFAULT 0,
  `okt` int(11) DEFAULT 0,
  `nov` int(11) DEFAULT 0,
  `des` int(11) DEFAULT 0,
  `insert_by` float DEFAULT NULL,
  `insert_time` date DEFAULT NULL,
  `update_by` float DEFAULT NULL,
  `update_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `targetdo`
--

INSERT INTO `targetdo` (`idtargetdo`, `tahun`, `cabang`, `tipekendaraan`, `modelkendaraan`, `jan`, `feb`, `mar`, `apr`, `mei`, `jun`, `jul`, `agu`, `sep`, `okt`, `nov`, `des`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(2, 2021, '641940101', 'NEW CARRY', 'PU FD', 11, 11, 11, 10, 9, 9, 12, 11, 12, 12, 12, 13, 61, '2021-11-22', NULL, NULL),
(3, 2021, '641940101', 'NEW CARRY', 'PU FD AC PS', 9, 9, 8, 9, 8, 8, 10, 10, 10, 10, 11, 10, 61, '2021-11-22', NULL, NULL),
(4, 2021, '641940101', 'NEW CARRY', 'PU FD AC PS-LUX', 1, 1, 0, 1, 0, 0, 1, 0, 2, 2, 1, 2, 61, '2021-11-22', NULL, NULL),
(5, 2021, '641940101', 'NEW CARRY', 'PU WD AC PS', 2, 1, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 61, '2021-11-22', NULL, NULL),
(6, 2021, '641940101', 'NEW CARRY', 'PU WD AC PS-LUX', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 61, '2021-11-22', NULL, NULL),
(7, 2021, '641940101', 'APV-MB', 'GL MT', 0, 0, 2, 2, 0, 0, 1, 2, 2, 0, 0, 0, 61, '2021-11-22', NULL, NULL),
(8, 2021, '641940101', 'APV-MB', 'GX MT', 0, 0, 1, 2, 1, 0, 0, 1, 0, 0, 0, 0, 61, '2021-11-22', NULL, NULL),
(10, 2021, '641940101', 'APV-MB', 'SGX LUX.R15 MT', 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 61, '2021-11-22', NULL, NULL),
(11, 2021, '641940101', 'ALL NEW ERTIGA', 'GL MT', 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 61, '2021-11-22', 61, '2021-11-22'),
(12, 2021, '641940101', 'ALL NEW ERTIGA', 'GL AT', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 61, '2021-11-22', NULL, NULL),
(13, 2021, '641940101', 'ALL NEW ERTIGA', 'GX MT', 2, 2, 1, 2, 2, 1, 2, 1, 2, 2, 2, 2, 61, '2021-11-22', NULL, NULL),
(14, 2021, '641940101', 'ALL NEW ERTIGA', 'GX ESP AT', 1, 1, 1, 1, 0, 1, 2, 2, 1, 2, 1, 2, 61, '2021-11-22', NULL, NULL),
(15, 2021, '641940101', 'XL-7', 'ZETA MT', 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 61, '2021-11-22', NULL, NULL),
(16, 2021, '641940101', 'XL-7', 'ZETA AT', 0, 1, 0, 1, 0, 1, 1, 1, 0, 2, 0, 1, 61, '2021-11-22', NULL, NULL),
(17, 2021, '641940101', 'XL-7', 'BETA MT', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 61, '2021-11-22', NULL, NULL),
(18, 2021, '641940101', 'XL-7', 'BETA AT', 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 61, '2021-11-22', NULL, NULL),
(19, 2021, '641940101', 'XL-7', 'ALPHA MT', 2, 2, 1, 1, 1, 1, 1, 1, 2, 1, 2, 2, 61, '2021-11-22', NULL, NULL),
(20, 2021, '641940101', 'XL-7', 'ALPHA AT', 4, 3, 3, 2, 1, 3, 4, 4, 4, 4, 3, 4, 61, '2021-11-22', NULL, NULL),
(21, 2021, '641940101', 'KARIMUN WAGON R', 'GL MT', 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 61, '2021-11-22', NULL, NULL),
(22, 2021, '641940101', 'KARIMUN WAGON R', 'GS MT', 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 61, '2021-11-22', NULL, NULL),
(23, 2021, '641940101', 'IGNIS', 'GL MT', 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 61, '2021-11-22', NULL, NULL),
(24, 2021, '641940101', 'IGNIS', 'GL AGS', 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 61, '2021-11-22', NULL, NULL),
(25, 2021, '641940101', 'IGNIS', 'GX MT', 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 61, '2021-11-22', NULL, NULL),
(26, 2021, '641940101', 'IGNIS', 'GX AGS', 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 61, '2021-11-22', NULL, NULL),
(27, 2021, '641940101', 'BALENO', 'BALENO MT', 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 61, '2021-11-22', NULL, NULL),
(28, 2021, '641940101', 'BALENO', 'BALENO AT', 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 61, '2021-11-22', NULL, NULL),
(29, 2021, '641940101', 'S-CROSS', 'GLX AT', 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 61, '2021-11-22', NULL, NULL),
(30, 2021, '641940101', 'NEW CARRY', 'PU WD', 0, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 61, '2021-11-22', NULL, NULL),
(31, 2021, '641940101', 'ALL NEW ERTIGA', 'GX AT', 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 61, '2021-11-22', NULL, NULL),
(32, 2021, '641940101', 'ALL NEW ERTIGA', 'GX ESP MT', 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 61, '2021-11-22', NULL, NULL),
(33, 2021, '641940102', 'NEW CARRY', 'PU FD', 26, 24, 26, 28, 28, 28, 26, 30, 30, 25, 32, 35, 61, '2021-11-23', NULL, NULL),
(35, 2021, '641940102', 'NEW CARRY', 'PU WD', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 61, '2021-11-23', NULL, NULL),
(36, 2021, '641940102', 'NEW CARRY', 'PU WD AC PS', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 61, '2021-11-23', NULL, NULL),
(37, 2021, '641940102', 'APV-MB', 'D.VAN PS', 0, 0, 2, 2, 0, 0, 2, 2, 2, 2, 3, 3, 61, '2021-11-23', NULL, NULL),
(38, 2021, '641940102', 'APV-MB', 'GL MT', 0, 0, 1, 1, 0, 0, 2, 2, 2, 2, 2, 2, 61, '2021-11-23', NULL, NULL),
(39, 2021, '641940102', 'APV-MB', 'GX MT', 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 61, '2021-11-23', NULL, NULL),
(40, 2021, '641940102', 'ALL NEW ERTIGA', 'GL MT', 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 2, 2, 61, '2021-11-23', NULL, NULL),
(41, 2021, '641940102', 'ALL NEW ERTIGA', 'GX MT', 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 2, 61, '2021-11-23', NULL, NULL),
(42, 2021, '641940102', 'XL-7', 'ZETA MT', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 61, '2021-11-23', NULL, NULL),
(43, 2021, '641940102', 'XL-7', 'BETA MT', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 61, '2021-11-23', NULL, NULL),
(44, 2021, '641940102', 'XL-7', 'ALPHA MT', 2, 1, 1, 1, 2, 1, 1, 1, 1, 2, 1, 2, 61, '2021-11-23', NULL, NULL),
(45, 2021, '641940102', 'XL-7', 'ALPHA AT', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 61, '2021-11-23', NULL, NULL),
(46, 2021, '641940102', 'KARIMUN WAGON R', 'GL MT', 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 61, '2021-11-23', NULL, NULL),
(47, 2021, '641940102', 'KARIMUN WAGON R', 'GS MT', 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 61, '2021-11-23', NULL, NULL),
(48, 2021, '641940102', 'IGNIS', 'GL MT', 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 61, '2021-11-23', NULL, NULL),
(49, 2021, '641940102', 'IGNIS', 'GL AGS', 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 61, '2021-11-23', NULL, NULL),
(50, 2021, '641940102', 'BALENO', 'BALENO AT', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 61, '2021-11-23', NULL, NULL),
(52, 2021, '641940103', 'NEW CARRY', 'PU FD', 2, 4, 2, 4, 2, 4, 6, 4, 6, 2, 4, 6, 61, '2021-11-23', NULL, NULL),
(53, 2021, '641940103', 'NEW CARRY', 'PU FD AC PS', 4, 2, 4, 2, 2, 2, 4, 4, 5, 4, 4, 2, 61, '2021-11-23', NULL, NULL),
(54, 2021, '641940103', 'NEW CARRY', 'PU FD AC PS-LUX', 2, 0, 0, 0, 0, 0, 1, 2, 0, 2, 0, 0, 61, '2021-11-23', NULL, NULL),
(55, 2021, '641940103', 'NEW CARRY', 'PU WD', 2, 0, 2, 0, 2, 0, 2, 2, 2, 2, 2, 0, 61, '2021-11-23', NULL, NULL),
(56, 2021, '641940103', 'APV-CH', 'CHS BOX', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 61, '2021-11-23', NULL, NULL),
(57, 2021, '641940103', 'APV-MB', 'GL MT', 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 61, '2021-11-23', NULL, NULL),
(58, 2021, '641940103', 'ALL NEW ERTIGA', 'GL MT', 2, 0, 0, 2, 0, 2, 0, 2, 2, 2, 2, 2, 61, '2021-11-23', NULL, NULL),
(59, 2021, '641940103', 'ALL NEW ERTIGA', 'GL AT', 0, 2, 2, 0, 2, 0, 2, 0, 0, 0, 0, 2, 61, '2021-11-23', NULL, NULL),
(60, 2021, '641940103', 'ALL NEW ERTIGA', 'GX MT', 0, 0, 0, 0, 0, 2, 0, 2, 0, 0, 1, 0, 61, '2021-11-23', NULL, NULL),
(62, 2021, '641940103', 'ALL NEW ERTIGA', 'GX ESP MT', 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 61, '2021-11-23', NULL, NULL),
(63, 2021, '641940103', 'ALL NEW ERTIGA', 'GX ESP AT', 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 61, '2021-11-23', NULL, NULL),
(64, 2021, '641940103', 'XL-7', 'ZETA MT', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 61, '2021-11-23', NULL, NULL),
(65, 2021, '641940103', 'XL-7', 'ZETA AT', 0, 2, 0, 2, 0, 0, 2, 2, 2, 2, 2, 2, 61, '2021-11-23', NULL, NULL),
(66, 2021, '641940103', 'XL-7', 'BETA MT', 2, 2, 1, 2, 2, 2, 0, 0, 2, 0, 2, 0, 61, '2021-11-23', NULL, NULL),
(67, 2021, '641940103', 'XL-7', 'BETA AT', 2, 2, 2, 2, 2, 4, 2, 1, 2, 2, 3, 3, 61, '2021-11-23', NULL, NULL),
(68, 2021, '641940103', 'XL-7', 'ALPHA MT', 0, 0, 2, 1, 0, 0, 2, 2, 0, 2, 2, 2, 61, '2021-11-23', NULL, NULL),
(69, 2021, '641940103', 'XL-7', 'ALPHA AT', 4, 3, 4, 4, 4, 4, 2, 4, 3, 4, 4, 6, 61, '2021-11-23', NULL, NULL),
(70, 2021, '641940103', 'KARIMUN WAGON R', 'GS AB MT', 0, 2, 0, 0, 0, 0, 2, 0, 2, 0, 0, 2, 61, '2021-11-23', NULL, NULL),
(71, 2021, '641940103', 'NEW BALENO', 'AT', 1, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 61, '2021-11-23', NULL, NULL),
(72, 2021, '641940103', 'NEW BALENO', 'MT', 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 61, '2021-11-23', NULL, NULL),
(73, 2021, '641940103', 'S-CROSS', 'GLX AT', 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 61, '2021-11-23', NULL, NULL),
(74, 2021, '641940104', 'NEW CARRY', 'PU FD', 4, 4, 4, 4, 4, 4, 4, 5, 4, 4, 4, 5, 61, '2021-11-23', NULL, NULL),
(75, 2021, '641940104', 'NEW CARRY', 'PU FD AC PS', 2, 1, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 61, '2021-11-23', NULL, NULL),
(76, 2021, '641940104', 'NEW CARRY', 'PU FD AC PS-LUX', 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 61, '2021-11-23', NULL, NULL),
(77, 2021, '641940104', 'NEW CARRY', 'PU WD', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 61, '2021-11-23', NULL, NULL),
(78, 2021, '641940104', 'NEW CARRY', 'PU WD AC PS', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 61, '2021-11-23', NULL, NULL),
(79, 2021, '641940104', 'ALL NEW ERTIGA', 'GL MT', 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 61, '2021-11-23', NULL, NULL),
(80, 2021, '641940104', 'ALL NEW ERTIGA', 'GL AT', 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 61, '2021-11-23', NULL, NULL),
(81, 2021, '641940104', 'ALL NEW ERTIGA', 'GX MT', 0, 0, 0, 0, 0, 0, 0, 2, 1, 0, 1, 0, 61, '2021-11-23', NULL, NULL),
(82, 2021, '641940103', 'ALL NEW ERTIGA', 'GA MT', 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 2, 2, 61, '2021-11-23', 61, '2021-12-02'),
(83, 2021, '641940104', 'ALL NEW ERTIGA', 'GX ESP AT', 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 61, '2021-11-23', NULL, NULL),
(84, 2021, '641940104', 'XL-7', 'ZETA AT', 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 61, '2021-11-23', NULL, NULL),
(85, 2021, '641940104', 'XL-7', 'BETA MT', 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 61, '2021-11-23', NULL, NULL),
(86, 2021, '641940104', 'XL-7', 'BETA MT', 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 61, '2021-11-23', NULL, NULL),
(87, 2021, '641940104', 'XL-7', 'ALPHA MT', 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 61, '2021-11-23', NULL, NULL),
(88, 2021, '641940104', 'XL-7', 'ALPHA AT', 1, 1, 1, 1, 2, 1, 2, 2, 1, 1, 1, 2, 61, '2021-11-23', NULL, NULL),
(89, 2021, '641940104', 'KARIMUN WAGON R', 'GL MT', 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 61, '2021-11-23', NULL, NULL),
(90, 2021, '641940104', 'KARIMUN WAGON R', 'GL AB AT', 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 61, '2021-11-23', NULL, NULL),
(91, 2021, '641940104', 'KARIMUN WAGON R', 'GS MT', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 61, '2021-11-23', NULL, NULL),
(92, 2021, '641940104', 'IGNIS', 'GL AGS', 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 61, '2021-11-23', NULL, NULL),
(93, 2021, '641940104', 'IGNIS', 'GX MT', 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 61, '2021-11-23', NULL, NULL),
(94, 2021, '641940104', 'IGNIS', 'GX AGS', 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 61, '2021-11-23', NULL, NULL),
(95, 2021, '641940104', 'NEW BALENO', 'AT', 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 61, '2021-11-23', NULL, NULL),
(96, 2021, '641940104', 'NEW BALENO', 'MT', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 61, '2021-11-23', NULL, NULL),
(97, 2021, '641940104', 'S-CROSS', 'GLX AT', 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 61, '2021-11-23', NULL, NULL),
(98, 2021, '641940102', 'NEW CARRY', 'PU FD AC PS', 6, 6, 6, 8, 8, 6, 8, 8, 8, 6, 8, 8, 61, '2021-11-26', NULL, NULL),
(99, 2021, '641940103', 'NEW CARRY', 'PU WD AC PS', 0, 2, 0, 2, 0, 2, 1, 0, 1, 0, 1, 2, 61, '2021-11-26', NULL, NULL),
(100, 2021, '641940103', 'IGNIS', 'GX MT', 1, 0, 0, 0, 1, 2, 0, 0, 0, 0, 1, 0, 61, '2021-11-27', NULL, NULL),
(101, 2021, '641940103', 'IGNIS', 'GX AGS', 2, 2, 0, 0, 2, 0, 2, 2, 2, 1, 2, 2, 61, '2021-11-27', NULL, NULL),
(103, 2021, '641940103', 'IGNIS', 'GL AGS', 0, 0, 2, 0, 1, 0, 0, 0, 0, 0, 0, 2, 61, '2021-12-02', NULL, NULL),
(104, 2021, '641940104', 'ALL NEW ERTIGA', 'GX AT', 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 61, '2021-12-02', NULL, NULL),
(105, 2021, '641940104', 'ALL NEW ERTIGA', 'GX ESP MT', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 61, '2021-12-02', NULL, NULL),
(106, 2022, '641940101', 'ALL NEW ERTIGA', 'GA MT', 57, 57, 56, 59, 53, 57, 60, 61, 61, 65, 65, 69, 61, '2022-01-05', NULL, NULL),
(107, 2022, '641940102', 'NEW BALENO', 'AT', 58, 55, 62, 63, 56, 59, 57, 61, 58, 60, 65, 66, 61, '2022-01-05', NULL, NULL),
(108, 2022, '641940103', 'NEW CARRY', 'CH - AMB-MTK', 32, 29, 35, 40, 35, 28, 37, 36, 38, 40, 40, 45, 61, '2022-01-05', NULL, NULL),
(109, 2022, '641940104', 'G.VITARA (CBU)', '2.4 AT', 15, 13, 17, 20, 18, 19, 21, 21, 22, 22, 25, 27, 61, '2022-01-05', NULL, NULL),
(110, 2023, '641940101', 'ALL NEW ERTIGA', 'GA MT', 46, 48, 54, 52, 49, 50, 53, 55, 53, 53, 51, 61, 61, '2023-02-08', NULL, NULL),
(111, 2023, '641940102', 'ALL NEW ERTIGA', 'GA MT', 50, 45, 53, 57, 53, 54, 53, 59, 53, 58, 64, 64, 61, '2023-02-08', NULL, NULL),
(112, 2023, '641940103', 'BALENO', 'AT', 30, 31, 34, 33, 34, 34, 33, 36, 32, 35, 32, 35, 61, '2023-02-08', NULL, NULL),
(113, 2023, '641940104', 'BALENO', 'AT', 28, 29, 37, 32, 40, 45, 43, 47, 49, 49, 50, 55, 61, '2023-02-08', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `targetrka`
--

CREATE TABLE `targetrka` (
  `idtargetrka` int(10) NOT NULL,
  `tahun` int(11) DEFAULT NULL,
  `cabang` varchar(50) DEFAULT NULL,
  `tiperka` varchar(50) DEFAULT NULL,
  `jan` int(12) DEFAULT 0,
  `feb` int(12) DEFAULT 0,
  `mar` int(12) DEFAULT 0,
  `apr` int(12) DEFAULT 0,
  `mei` int(12) DEFAULT 0,
  `jun` int(12) DEFAULT 0,
  `jul` int(12) DEFAULT 0,
  `agu` int(12) DEFAULT 0,
  `sep` int(12) DEFAULT 0,
  `okt` int(12) DEFAULT 0,
  `nov` int(12) DEFAULT 0,
  `des` int(12) DEFAULT 0,
  `insert_by` float DEFAULT NULL,
  `insert_time` date DEFAULT NULL,
  `update_by` float DEFAULT NULL,
  `update_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `targetrka`
--

INSERT INTO `targetrka` (`idtargetrka`, `tahun`, `cabang`, `tiperka`, `jan`, `feb`, `mar`, `apr`, `mei`, `jun`, `jul`, `agu`, `sep`, `okt`, `nov`, `des`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(1, 2022, '641940103', 'labour', 128640000, 124800000, 126400000, 131200000, 124800000, 127360000, 128640000, 128640000, 128640000, 128640000, 128640000, 129459200, 0, '0000-00-00', 0, '0000-00-00'),
(3, 2022, '641940103', 'SGP', 124620000, 120900000, 122450000, 127100000, 120900000, 123380000, 124620000, 124620000, 124620000, 124620000, 124620000, 125413600, 0, '0000-00-00', 0, '0000-00-00'),
(4, 2022, '641940103', 'SGO', 84420000, 81900000, 82950000, 86100000, 81900000, 83580000, 84420000, 84420000, 84420000, 84420000, 84420000, 84957600, 0, '0000-00-00', 0, '0000-00-00'),
(5, 2022, '641940103', 'SGA', 12060000, 11700000, 11850000, 12300000, 11700000, 11940000, 12060000, 12060000, 12060000, 12060000, 12060000, 12136800, 0, '0000-00-00', 0, '0000-00-00'),
(6, 2022, '641940102', 'labour', 149600000, 131648000, 155584000, 149600000, 131648000, 149600000, 143616000, 155584000, 155584000, 149600000, 155584000, 161568000, 0, '0000-00-00', 0, '0000-00-00'),
(7, 2022, '641940102', 'SGP', 118250000, 104060000, 122980000, 118250000, 104060000, 118250000, 113520000, 122980000, 122980000, 118250000, 122980000, 127710000, 0, NULL, 0, '0000-00-00'),
(8, 2022, '641940102', 'SGO', 102850000, 90508000, 106964000, 102850000, 90508000, 102850000, 98736000, 106964000, 106964000, 102850000, 106964000, 111078000, 0, '0000-00-00', 0, '0000-00-00'),
(9, 2022, '641940102', 'SGA', 11000000, 9680000, 11440000, 11000000, 9680000, 11000000, 10560000, 11440000, 11440000, 11000000, 11440000, 11880000, 0, '0000-00-00', 0, '0000-00-00'),
(10, 2022, '641940101', 'labour', 145860000, 123420000, 145860000, 140250000, 106590000, 140250000, 145860000, 145860000, 145860000, 145860000, 145860000, 145860000, 0, '0000-00-00', 0, '0000-00-00'),
(11, 2022, '641940101', 'SGP', 123760000, 104720000, 123760000, 119000000, 90440000, 119000000, 123760000, 123760000, 123760000, 123760000, 123760000, 123760000, 0, '0000-00-00', 0, '0000-00-00'),
(12, 2022, '641940101', 'SGO', 110500000, 93500000, 110500000, 106250000, 80750000, 106250000, 110500000, 110500000, 110500000, 110500000, 110500000, 110500000, 0, '0000-00-00', 0, '0000-00-00'),
(13, 2022, '641940101', 'SGA', 17680000, 14960000, 17680000, 17000000, 12920000, 17000000, 17680000, 17680000, 17680000, 17680000, 17680000, 17680000, 0, '0000-00-00', 0, '0000-00-00'),
(14, 2022, '641940104', 'LABOUR', 18375000, 18375000, 18375000, 18375000, 18375000, 18375000, 18375000, 18375000, 18375000, 18375000, 18375000, 18375000, 0, '0000-00-00', 0, '0000-00-00'),
(17, 2022, '641940104', 'SGP', 16275000, 16275000, 16275000, 16275000, 16275000, 16275000, 16275000, 16275000, 16275000, 16275000, 16275000, 16275000, NULL, NULL, NULL, NULL),
(18, 2022, '641940104', 'SGO', 17325000, 17325000, 17325000, 17325000, 17325000, 17325000, 17325000, 17325000, 17325000, 17325000, 17325000, 17325000, NULL, NULL, NULL, NULL),
(19, 2022, '641940104', 'SGA', 525000, 525000, 525000, 525000, 525000, 525000, 525000, 525000, 525000, 525000, 525000, 525000, NULL, NULL, NULL, NULL),
(20, 2023, '641940101', 'LABOUR', 132704000, 117073000, 132704000, 96338000, 127919000, 117073000, 127919000, 132704000, 127919000, 132704000, 132704000, 127919000, 0, '0000-00-00', 0, '0000-00-00'),
(22, 2023, '641940101', 'SGP', 141856000, 125147000, 141856000, 102982000, 136741000, 125147000, 136741000, 141856000, 136741000, 141856000, 141856000, 136741000, 0, '0000-00-00', 0, '0000-00-00'),
(23, 2023, '641940101', 'SGO', 118976000, 104962000, 118976000, 86372000, 114686000, 104962000, 114686000, 118976000, 114686000, 118976000, 118976000, 114686000, 0, '0000-00-00', 0, '0000-00-00'),
(24, 2023, '641940101', 'SGA', 18304000, 16148000, 18304000, 13288000, 17644000, 16148000, 17644000, 18304000, 17644000, 18304000, 18304000, 17644000, 0, '0000-00-00', 0, '0000-00-00'),
(25, 2023, '641940103', 'LABOUR', 109040000, 96280000, 111360000, 95410000, 107880000, 110200000, 108460000, 110200000, 108170000, 109040000, 109330000, 109040000, 0, '0000-00-00', 0, '0000-00-00'),
(26, 2023, '641940103', 'SGP', 120320000, 106240000, 122880000, 105280000, 119040000, 121600000, 119680000, 121600000, 119360000, 120320000, 120640000, 120320000, 0, '0000-00-00', 0, '0000-00-00'),
(27, 2023, '641940103', 'SGO', 99640000, 87980000, 101760000, 87185000, 98580000, 100700000, 99110000, 100700000, 98845000, 99640000, 99905000, 99640000, 0, '0000-00-00', 0, '0000-00-00'),
(28, 2023, '641940103', 'SGA', 16920000, 14940000, 17280000, 14805000, 16740000, 17100000, 16830000, 17100000, 16785000, 16920000, 16965000, 16920000, 0, '0000-00-00', 0, '0000-00-00'),
(29, 2023, '641940102', 'LABOUR', 125840000, 111320000, 125840000, 91960000, 121000000, 111320000, 121000000, 125840000, 121000000, 125840000, 125840000, 121000000, 0, '0000-00-00', 0, '0000-00-00'),
(30, 2023, '641940102', 'SGP', 167024000, 147752000, 167024000, 122056000, 160600000, 147752000, 160600000, 167024000, 160600000, 167024000, 167024000, 160600000, 0, '0000-00-00', 0, '0000-00-00'),
(31, 2023, '641940102', 'SGO', 132704000, 117392000, 132704000, 96976000, 127600000, 117392000, 127600000, 132704000, 127600000, 132704000, 132704000, 127600000, 0, '0000-00-00', 0, '0000-00-00'),
(32, 2023, '641940102', 'SGA', 18304000, 16192000, 18304000, 13376000, 17600000, 16192000, 17600000, 18304000, 17600000, 18304000, 18304000, 17600000, 0, '0000-00-00', 0, '0000-00-00'),
(37, 2023, '641940104', 'labour', 42900000, 36300000, 42900000, 41250000, 44550000, 41250000, 82500000, 85800000, 82500000, 85800000, 85800000, 82500000, NULL, NULL, NULL, NULL),
(38, 2023, '641940104', 'SGP', 33800000, 28600000, 33800000, 32500000, 35100000, 32500000, 65000000, 67600000, 65000000, 67600000, 67600000, 65000000, NULL, NULL, NULL, NULL),
(39, 2023, '641940104', 'SGO', 35100000, 29700000, 35100000, 33750000, 36450000, 33750000, 67500000, 70200000, 67500000, 70200000, 70200000, 67500000, NULL, NULL, NULL, NULL),
(40, 2023, '641940104', 'SGA', 5200000, 4400000, 5200000, 5000000, 5400000, 5000000, 10000000, 10400000, 10000000, 10400000, 10400000, 10000000, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `targetunitentry`
--

CREATE TABLE `targetunitentry` (
  `idtargetunitentry` int(10) NOT NULL,
  `tahun` int(11) DEFAULT NULL,
  `cabang` varchar(50) DEFAULT NULL,
  `jan` int(11) DEFAULT 0,
  `feb` int(11) DEFAULT 0,
  `mar` int(11) DEFAULT 0,
  `apr` int(11) DEFAULT 0,
  `mei` int(11) DEFAULT 0,
  `jun` int(11) DEFAULT 0,
  `jul` int(11) DEFAULT 0,
  `agu` int(11) DEFAULT 0,
  `sep` int(11) DEFAULT 0,
  `okt` int(11) DEFAULT 0,
  `nov` int(11) DEFAULT 0,
  `des` int(11) DEFAULT 0,
  `insert_by` float DEFAULT NULL,
  `insert_time` date DEFAULT NULL,
  `update_by` float DEFAULT NULL,
  `update_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `targetunitentry`
--

INSERT INTO `targetunitentry` (`idtargetunitentry`, `tahun`, `cabang`, `jan`, `feb`, `mar`, `apr`, `mei`, `jun`, `jul`, `agu`, `sep`, `okt`, `nov`, `des`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(1, 2021, '641940101', 375, 345, 375, 375, 330, 375, 390, 360, 390, 375, 390, 390, 62, '2021-11-22', 61, '2021-11-22'),
(2, 2021, '641940102', 489, 450, 508, 489, 430, 489, 508, 469, 508, 469, 508, 508, 61, '2021-11-22', NULL, NULL),
(3, 2021, '641940103', 343, 360, 330, 350, 300, 345, 360, 345, 350, 352, 310, 335, 61, '2021-11-22', NULL, NULL),
(4, 2021, '641940104', 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 61, '2021-11-22', NULL, NULL),
(5, 2022, '641940101', 442, 374, 442, 425, 323, 425, 442, 442, 442, 442, 442, 442, 61, '2022-01-05', NULL, NULL),
(6, 2022, '641940102', 550, 484, 572, 550, 484, 550, 528, 572, 572, 550, 572, 594, 61, '2022-01-05', NULL, NULL),
(7, 2022, '641940103', 420, 390, 395, 410, 390, 398, 402, 402, 402, 402, 402, 405, 61, '2022-01-05', NULL, NULL),
(8, 2022, '641940104', 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 75, 61, '2022-01-05', NULL, NULL),
(9, 2023, '641940101', 468, 432, 450, 342, 432, 432, 450, 468, 450, 468, 468, 45, 61, '2023-02-08', NULL, NULL),
(10, 2023, '641940102', 592, 524, 592, 524, 546, 546, 569, 569, 546, 592, 592, 569, 61, '2023-02-08', NULL, NULL),
(11, 2023, '641940103', 416, 368, 416, 368, 384, 384, 400, 400, 384, 416, 416, 400, 61, '2023-02-08', NULL, NULL),
(12, 2023, '641940104', 140, 150, 197, 281, 231, 254, 263, 300, 316, 381, 412, 447, 61, '2023-02-08', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tcabang`
--

CREATE TABLE `tcabang` (
  `idcabang` int(20) NOT NULL,
  `kdcabang` varchar(50) NOT NULL,
  `nmcabang` varchar(60) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `notlp` varchar(14) NOT NULL,
  `insert_by` double DEFAULT NULL,
  `insert_time` date DEFAULT NULL,
  `update_by` double DEFAULT NULL,
  `update_time` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tcabang`
--

INSERT INTO `tcabang` (`idcabang`, `kdcabang`, `nmcabang`, `alamat`, `notlp`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(5, 'aa', 'teguh', 'bb', 'sss', 17, '2021-09-23', 61, '2021-10-07'),
(6, 'bb', 'ddddeeeeeeeeeeeeeeee', 'eeee', 'ffffff', 61, '2021-09-27', 61, '2021-10-07'),
(7, 'cc', 'ddd', 'ffff', 'ggg', 61, '2021-09-27', NULL, NULL),
(8, 'ddd', 'ccc', 'aaa', 'bbbbbb', 61, '2021-09-27', NULL, NULL),
(9, 'ccc', 'ddd', 'ddd', 'ddd', 61, '2021-10-07', 61, '2021-10-07'),
(10, 'dd', 'aaa', 'ggg', 'ggg', 61, '2021-10-07', NULL, NULL),
(12, 'fff', 'ggg', 'cccc', '33333333333333', 61, '2021-10-07', 61, '2021-10-07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tgaji`
--

CREATE TABLE `tgaji` (
  `idgaji` int(11) NOT NULL,
  `idkaryawan` varchar(10) DEFAULT NULL,
  `gp` float DEFAULT NULL,
  `lembur` float DEFAULT NULL,
  `transport` float DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tgaji`
--

INSERT INTO `tgaji` (`idgaji`, `idkaryawan`, `gp`, `lembur`, `transport`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(117, '11', 35000, 10500, 14500, NULL, NULL, NULL, NULL),
(118, '17', 600, 0, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tjabatan`
--

CREATE TABLE `tjabatan` (
  `idjabatan` int(11) NOT NULL,
  `kdjabatan` varchar(5) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `insert_by` float DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` float DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tjabatan`
--

INSERT INTO `tjabatan` (`idjabatan`, `kdjabatan`, `jabatan`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(20, '002', 'mekanik', NULL, NULL, 0, '2021-10-08 08:33:57'),
(21, '003', 'Akutansi', NULL, NULL, 61, '2021-10-07 11:23:56'),
(22, '004', 'Staff', NULL, '2021-09-05 00:00:00', 61, '2021-10-08 10:53:59'),
(23, '005', 'Administrator', NULL, '2021-09-05 00:00:00', 61, '2021-10-07 11:18:16'),
(24, '006', 'General Manager', NULL, '2021-09-05 00:00:00', 17, '2021-09-22 14:15:16'),
(42, NULL, NULL, 61, '2021-09-24 21:30:52', NULL, NULL),
(44, 'BM-01', 'Branch Manager', 61, '2021-12-22 09:58:32', NULL, NULL),
(45, 'SH-01', 'Sales Head', 61, '2021-12-22 09:58:32', NULL, NULL),
(46, 'OWN-1', 'Owner 1', 61, '2021-12-30 09:04:55', NULL, NULL),
(47, 'OWN-2', 'Owner 2', 61, '2021-12-30 09:04:55', NULL, NULL),
(48, 'OWN-3', 'Owner 3', 61, '2021-12-30 09:04:55', NULL, NULL),
(49, 'kepal', 'Kepala Sales', 61, '2022-01-10 10:35:01', NULL, NULL),
(50, 'HHRD', 'Kepala HRD', 61, '2022-01-24 11:28:42', NULL, NULL),
(51, 'SRO', 'SRO', 61, '2022-04-11 09:16:05', NULL, NULL),
(52, 'SM', 'Service Manager', 61, '2022-04-11 11:01:34', NULL, NULL),
(53, 'FD', 'ADH', 61, '2022-04-11 11:05:02', NULL, NULL),
(54, 'ACCSP', 'Accounting SPV', 61, '2022-04-11 11:05:46', NULL, NULL),
(55, 'SVADM', 'Service Admin', 61, '2023-07-16 12:34:59', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tjadwal`
--

CREATE TABLE `tjadwal` (
  `idjadwal` int(11) NOT NULL,
  `tgl` date DEFAULT NULL,
  `idkaryawan` int(11) DEFAULT NULL,
  `hari` int(5) DEFAULT NULL,
  `masuk` time DEFAULT NULL,
  `keluar` time DEFAULT NULL,
  `aktif` varchar(2) DEFAULT NULL COMMENT 'aktif 1, non aktif 2',
  `ket` varchar(150) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tjadwal`
--

INSERT INTO `tjadwal` (`idjadwal`, `tgl`, `idkaryawan`, `hari`, `masuk`, `keluar`, `aktif`, `ket`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(98, '2021-08-15', 18, 0, '05:00:00', '14:30:00', '1', '', NULL, NULL, NULL, NULL);

--
-- Trigger `tjadwal`
--
DELIMITER $$
CREATE TRIGGER `tjadwal` AFTER DELETE ON `tjadwal` FOR EACH ROW BEGIN
	
	DELETE FROM tjadwaldtl WHERE idjadwal = OLD.idjadwal;
	
		
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tjadwaldtl`
--

CREATE TABLE `tjadwaldtl` (
  `idjadwaldtl` int(11) NOT NULL,
  `idjadwal` int(11) DEFAULT NULL,
  `idkelas` int(11) DEFAULT NULL,
  `masuk` time DEFAULT NULL,
  `keluar` time DEFAULT NULL,
  `jmljam` float DEFAULT NULL,
  `ket` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tjadwaldtl`
--

INSERT INTO `tjadwaldtl` (`idjadwaldtl`, `idjadwal`, `idkelas`, `masuk`, `keluar`, `jmljam`, `ket`) VALUES
(59, 98, 4, '08:30:00', '15:30:00', 7, NULL),
(60, 98, 3, '16:00:00', '23:00:00', 7, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tkaryawan`
--

CREATE TABLE `tkaryawan` (
  `idkaryawan` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `ktp` varchar(50) DEFAULT NULL,
  `tmp_lahir` varchar(50) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `tgl_masuk` date DEFAULT NULL,
  `tgl_keluar` date DEFAULT NULL,
  `nmkaryawan` varchar(200) DEFAULT NULL,
  `jk` varchar(15) DEFAULT NULL,
  `idjabatan` int(2) DEFAULT NULL,
  `idpendidikan` int(11) DEFAULT NULL,
  `tlp` varchar(20) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `idpropinsi` int(11) DEFAULT NULL,
  `idkota` int(11) DEFAULT NULL,
  `idkecamatan` int(11) DEFAULT NULL,
  `iddesa` int(11) DEFAULT NULL,
  `kdpos` varchar(10) DEFAULT NULL,
  `jurusan` varchar(50) DEFAULT NULL,
  `lulusan` varchar(20) DEFAULT NULL,
  `qr` varchar(255) DEFAULT NULL,
  `flag` varchar(1) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `photo` varchar(150) DEFAULT NULL,
  `flag_show` int(11) DEFAULT NULL COMMENT '1 tampil sesuai insert by, 2 semua tampil',
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tkaryawan`
--

INSERT INTO `tkaryawan` (`idkaryawan`, `username`, `password`, `nik`, `ktp`, `tmp_lahir`, `tgl_lahir`, `agama`, `tgl_masuk`, `tgl_keluar`, `nmkaryawan`, `jk`, `idjabatan`, `idpendidikan`, `tlp`, `alamat`, `idpropinsi`, `idkota`, `idkecamatan`, `iddesa`, `kdpos`, `jurusan`, `lulusan`, `qr`, `flag`, `email`, `photo`, `flag_show`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(17, NULL, 'bmFuZGhpcmExOQ==', '0001', '00012', '', NULL, 'Islam', '0000-00-00', '0000-00-00', 'Teguh Ariep', 'L', 23, 22, '085793222681', 'alamat jln. arwinda', 0, 0, 0, 0, '43021', 'T. informatika', 'Universitas Putra In', NULL, NULL, NULL, '/assets/imgkaryawan/WIN_20210921_20_52_30_Pro.jpg', 2, NULL, NULL, 61, '2021-10-07 11:45:05'),
(18, NULL, 'MTIzNDU2', '0002', '0002', NULL, NULL, NULL, NULL, NULL, 'Nissa Annisa', NULL, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(19, NULL, 'MDAz', '003', '003', 'cianjur', NULL, 'Islam', NULL, NULL, 'idris', 'L', 20, 22, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, 17, '2021-09-23 11:04:11', NULL, NULL),
(20, NULL, 'MDAwMTA=', '00010', '45555', 'ddd', NULL, 'Islam', '0000-00-00', '0000-00-00', 'fff', 'L', 20, 21, '', '', 0, 0, 0, 0, '', '', '', NULL, NULL, NULL, '', NULL, 61, '2021-10-07 11:36:22', 61, '2021-10-07 11:37:24'),
(21, NULL, 'MDA3', '007', '933333', 'cianjut', NULL, 'Islam', NULL, NULL, 'esrse', 'L', 20, 22, 'efawefa', NULL, 0, NULL, NULL, NULL, NULL, 'ass', 'ddd', NULL, NULL, NULL, NULL, NULL, 61, '2021-10-07 11:45:55', NULL, NULL),
(22, NULL, 'MDAuMTIuMDEuMDc1', '00.12.01.075', NULL, NULL, NULL, 'Kristen', NULL, NULL, 'PAULUS PRIHARTONO', 'L', 24, 21, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-11-17 10:46:45', NULL, NULL),
(23, NULL, 'MTQuMjEuMDQuMjIx', '14.21.04.221', NULL, NULL, NULL, 'Islam', NULL, NULL, 'ALDO YOGASMARA', 'L', 22, 21, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 62, '2021-11-17 13:42:20', NULL, NULL),
(24, NULL, 'MDAuMTIuMDEuMDc1WA==', '00.12.01.075X', NULL, NULL, NULL, NULL, NULL, NULL, 'PAULUS PRIHARTONO X', NULL, 44, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:00:37', NULL, NULL),
(25, NULL, 'MDEuMTkuMDguMTAy', '01.19.08.102', NULL, NULL, NULL, NULL, NULL, NULL, 'RONALD NOVEMBRI W', NULL, 44, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:01:15', NULL, NULL),
(26, NULL, 'MDEuMjEuMDkuMDAx', '01.21.09.001', NULL, NULL, NULL, NULL, NULL, NULL, 'Marjoni, SE', NULL, 44, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:01:57', NULL, NULL),
(27, NULL, 'MDQuMDkuMDEuMDAx', '04.09.01.001', NULL, NULL, NULL, NULL, NULL, NULL, 'RUBBY SAPUTRA', NULL, 44, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:02:44', NULL, NULL),
(28, NULL, 'MDYuMjEuMDMuMDAx', '06.21.03.001', NULL, NULL, NULL, NULL, NULL, NULL, 'ARIA BHARATA, S.E', NULL, 44, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:03:41', NULL, NULL),
(29, NULL, 'MDEuMTIuMDEuMDAx', '01.12.01.001', NULL, NULL, NULL, NULL, NULL, NULL, 'ROPIK ARROHMAN', NULL, 45, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:04:48', NULL, NULL),
(30, NULL, 'MDEuMTIuMDEuMDIz', '01.12.01.023', NULL, NULL, NULL, NULL, NULL, NULL, 'REDDY SUWANTO', NULL, 45, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:05:38', NULL, NULL),
(31, NULL, 'MDEuMTMuMDEuMDU0', '01.13.01.054', NULL, NULL, NULL, NULL, NULL, NULL, 'IRFAN KURNIAWAN', NULL, 45, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:17:25', NULL, NULL),
(32, NULL, 'MDEuMTguMDEuMDI3', '01.18.01.027', NULL, NULL, NULL, NULL, NULL, NULL, 'ZULKARNAEN', NULL, 45, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:18:15', NULL, NULL),
(33, NULL, 'MDEuMTkuMDEuMDE1', '01.19.01.015', NULL, NULL, NULL, NULL, NULL, NULL, 'CHRIS ARCHY', NULL, 45, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:19:58', NULL, NULL),
(34, NULL, 'MDEuMTkuMDEuMDQ1', '01.19.01.045', NULL, NULL, NULL, NULL, NULL, NULL, 'MUHAMAD DAHLAN', NULL, 45, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:20:38', NULL, NULL),
(35, NULL, 'MDEuMTkuMDguMTAyWA==', '01.19.08.102X', NULL, NULL, NULL, NULL, NULL, NULL, 'RONALD NOVEMBRI W X', NULL, 45, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:21:26', NULL, NULL),
(36, NULL, 'MDMuMjEuMDEuMDAx', '03.21.01.001', NULL, NULL, NULL, NULL, NULL, NULL, 'AGUS SULANTIYO', NULL, 45, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:22:02', NULL, NULL),
(37, NULL, 'MDQuMDkuMDEuMDAxWA==', '04.09.01.001X', NULL, NULL, NULL, NULL, NULL, NULL, 'RUBBY SAPUTRA X', NULL, 45, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:22:50', NULL, NULL),
(38, NULL, 'MDQuMTcuMDEuMDM1', '04.17.01.035', NULL, NULL, NULL, NULL, NULL, NULL, 'SALEH MADANI', NULL, 45, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:23:39', NULL, NULL),
(39, NULL, 'MDQuMTcuMDEuMDM2', '04.17.01.036', NULL, NULL, NULL, NULL, NULL, NULL, 'HENDRIX SANTOSA', NULL, 45, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:24:20', NULL, NULL),
(40, NULL, 'MDQuMTkuMDEuMTQ3', '04.19.01.147', NULL, NULL, NULL, NULL, NULL, NULL, 'HADE WIBAWA', NULL, 45, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:25:15', NULL, NULL),
(41, NULL, 'MDYuMjEuMDMuMDAxWA==', '06.21.03.001X', NULL, NULL, NULL, NULL, NULL, NULL, 'ARIA BHARATA, S.E X', NULL, 45, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:26:04', NULL, NULL),
(42, NULL, 'MTQuMTkuMDEuMDk3', '14.19.01.097', NULL, NULL, NULL, NULL, NULL, NULL, 'IRVAN ZAENAL MUTAQIN', NULL, 45, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-22 10:26:53', NULL, NULL),
(43, NULL, 'T3duLTE=', 'Own-1', NULL, NULL, NULL, NULL, NULL, NULL, 'Vincent', NULL, 46, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-30 09:12:09', NULL, NULL),
(44, NULL, 'T3duLTI=', 'Own-2', NULL, NULL, NULL, NULL, NULL, NULL, 'Ine', NULL, 47, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-30 09:13:09', NULL, NULL),
(45, NULL, 'T3duLTM=', 'Own-3', NULL, NULL, NULL, NULL, NULL, NULL, 'Evlyn', NULL, 47, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2021-12-30 09:14:27', NULL, NULL),
(46, NULL, 'S1MtMDAx', 'KS-001', NULL, NULL, NULL, NULL, NULL, NULL, 'Ade Anggraeni', NULL, 49, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2022-01-10 10:36:48', NULL, NULL),
(47, NULL, 'MDEuMDkuMDIuMDAuMjE=', '01.09.02.00.21', '01.09.02.00.21', NULL, NULL, NULL, NULL, NULL, 'SUGANDA CHANDRA ', NULL, 50, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2022-01-24 11:29:53', NULL, NULL),
(48, NULL, 'MDQuMTIuMDUuMDEw', '04.12.05.010', NULL, NULL, NULL, NULL, NULL, NULL, ' R.Puti Kemala Dewi ', NULL, 51, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2022-04-11 09:17:04', NULL, NULL),
(49, NULL, 'MDEuMTguMDUuMDAx', '01.18.05.001', NULL, NULL, NULL, NULL, NULL, NULL, 'Ikrima K K Sari Putri', NULL, 51, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2022-04-11 10:53:06', NULL, NULL),
(50, NULL, 'MDMuMjEuMTEuMDAx', '03.21.11.001', NULL, NULL, NULL, NULL, NULL, NULL, 'DINI NOVIA FIRANDANI', NULL, 51, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2022-04-11 10:54:14', NULL, NULL),
(51, NULL, 'MDEuMjEuMDkuMDAy', '01.21.09.002', NULL, NULL, NULL, NULL, NULL, NULL, 'HERI SETIAWAN', NULL, 52, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2022-04-11 11:08:09', NULL, NULL),
(52, NULL, 'MDMuMTguMDUuNDU3', '03.18.05.457', NULL, NULL, NULL, NULL, NULL, NULL, 'Kusdiyantoro ST', NULL, 52, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2022-04-11 11:09:31', NULL, NULL),
(53, NULL, 'MDQuMDkuMDUuMDA2', '04.09.05.006', NULL, NULL, NULL, NULL, NULL, NULL, 'Ganda Mulyadi', NULL, 52, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2022-04-11 11:10:44', NULL, NULL),
(54, NULL, 'MDEuMjEuMDYuMDAx', '01.21.06.001', NULL, NULL, NULL, NULL, NULL, NULL, 'CLARISSA', NULL, 53, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2022-04-11 13:14:50', NULL, NULL),
(55, NULL, 'MDMuMTMuMDAuMDM0', '03.13.00.034', NULL, NULL, NULL, NULL, NULL, NULL, 'Budi Tanujaya', NULL, 53, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2022-04-11 13:16:23', NULL, NULL),
(56, NULL, 'MDAuMTMuMDIuMDA1', '00.13.02.005', NULL, NULL, NULL, NULL, NULL, NULL, 'Liana Respati', NULL, 54, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2022-04-11 13:21:30', NULL, NULL),
(57, NULL, 'MDMuMjIuMDMuMDAx', '03.22.03.001', NULL, NULL, NULL, NULL, NULL, NULL, 'Ety Cia', NULL, 53, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2022-04-12 11:21:28', NULL, NULL),
(58, NULL, 'MDQuMTQuMDEuMDIw', '04.14.01.020', NULL, NULL, NULL, NULL, NULL, NULL, 'Dedi Sopiyan', 'L', 45, 22, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, 61, '2023-02-24 08:28:37', NULL, NULL),
(59, NULL, 'MDQuMTQuMDEuMDIy', '04.14.01.022', NULL, NULL, NULL, NULL, NULL, NULL, 'Dedi Sopiyan', NULL, 45, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2023-02-24 08:44:57', NULL, NULL),
(60, NULL, 'MTQuMjIuMTAuMjkx', '14.22.10.291', NULL, NULL, NULL, NULL, NULL, NULL, 'Lucky Ekaputra', NULL, 45, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2023-05-31 14:35:31', NULL, NULL),
(61, NULL, 'MDQuMDkuMDUuMDA1', '04.09.05.005', NULL, NULL, NULL, NULL, NULL, NULL, 'agustina wulandari', NULL, 55, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 61, '2023-07-16 12:35:51', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tkelas`
--

CREATE TABLE `tkelas` (
  `idkelas` int(11) NOT NULL,
  `kelas` varchar(20) NOT NULL,
  `ket` varchar(100) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tkelas`
--

INSERT INTO `tkelas` (`idkelas`, `kelas`, `ket`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(3, 'Kelas A', NULL, NULL, NULL, NULL, NULL),
(4, 'Kelas B', NULL, NULL, NULL, NULL, NULL),
(5, 'Kelas C', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tlembur`
--

CREATE TABLE `tlembur` (
  `idlembur` int(11) NOT NULL,
  `reg` varchar(20) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `masuk` time DEFAULT NULL,
  `keluar` time DEFAULT NULL,
  `jml_jam` int(11) DEFAULT NULL,
  `ket` varchar(150) DEFAULT NULL,
  `acc` varchar(2) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tlembur`
--

INSERT INTO `tlembur` (`idlembur`, `reg`, `tgl`, `masuk`, `keluar`, `jml_jam`, `ket`, `acc`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(5, '2108001', '2021-08-12', '09:00:00', '12:30:00', NULL, 'Meeting Promosi tahun Pelajaran baru', NULL, NULL, NULL, NULL, NULL),
(6, '2108002', '2021-08-12', '11:30:00', '14:30:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Trigger `tlembur`
--
DELIMITER $$
CREATE TRIGGER `tlembur` AFTER DELETE ON `tlembur` FOR EACH ROW BEGIN
	
	DELETE FROM tlemburdtl WHERE idlembur = OLD.idlembur;
	
		
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tlemburdtl`
--

CREATE TABLE `tlemburdtl` (
  `idlemburdtl` int(11) NOT NULL,
  `idlembur` int(11) DEFAULT NULL,
  `idkaryawan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tlemburdtl`
--

INSERT INTO `tlemburdtl` (`idlemburdtl`, `idlembur`, `idkaryawan`) VALUES
(3, 5, 11),
(6, 6, 12);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tlistpembayaran`
--

CREATE TABLE `tlistpembayaran` (
  `idlistpembayaran` int(11) NOT NULL,
  `idakun` int(10) DEFAULT NULL,
  `idpeserta` int(10) DEFAULT NULL,
  `nominal` float DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tlistpembayaran`
--

INSERT INTO `tlistpembayaran` (`idlistpembayaran`, `idakun`, `idpeserta`, `nominal`, `status`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(16, 18, 4, 3500, NULL, NULL, NULL, NULL, NULL),
(17, 19, 4, 420000, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tlistregistrasi`
--

CREATE TABLE `tlistregistrasi` (
  `idlistregistrasi` int(11) NOT NULL,
  `kdlistregistrasi` varchar(10) DEFAULT NULL,
  `thn_ajaran` varchar(15) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `ket` varchar(150) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tlistregistrasi`
--

INSERT INTO `tlistregistrasi` (`idlistregistrasi`, `kdlistregistrasi`, `thn_ajaran`, `tgl`, `ket`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(97, '2108001', '2021-2022', '2021-08-15', '', NULL, NULL, NULL, NULL),
(98, '2108002', '2022-2023', '2021-08-15', '', NULL, NULL, NULL, NULL);

--
-- Trigger `tlistregistrasi`
--
DELIMITER $$
CREATE TRIGGER `tlistregistrasi` AFTER DELETE ON `tlistregistrasi` FOR EACH ROW BEGIN
	
	DELETE FROM tlistregistrasidtl WHERE idlistregistrasi = OLD.idlistregistrasi;
	
		
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tlistregistrasidtl`
--

CREATE TABLE `tlistregistrasidtl` (
  `idlistregistrasidtl` int(11) NOT NULL,
  `idlistregistrasi` int(11) DEFAULT NULL,
  `idakun` int(11) DEFAULT NULL,
  `nominal` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tlistregistrasidtl`
--

INSERT INTO `tlistregistrasidtl` (`idlistregistrasidtl`, `idlistregistrasi`, `idakun`, `nominal`) VALUES
(87, 97, 19, 420000),
(88, 98, 19, 420000),
(90, 98, 18, 3500);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tnilaiakhir`
--

CREATE TABLE `tnilaiakhir` (
  `idnilaiakhir` int(11) NOT NULL,
  `tgl` date DEFAULT NULL,
  `kdnilaiakhir` varchar(10) DEFAULT NULL,
  `idpengajar` int(11) DEFAULT NULL,
  `idkelas` int(11) DEFAULT NULL,
  `idpelajaran` int(11) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tnilaiakhir`
--

INSERT INTO `tnilaiakhir` (`idnilaiakhir`, `tgl`, `kdnilaiakhir`, `idpengajar`, `idkelas`, `idpelajaran`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(119, '2021-08-24', '2108002', 12, 4, 156, NULL, NULL, NULL, NULL),
(120, '2021-09-14', '2109001', 18, 4, 156, 18, '2021-09-14 05:07:10', 18, '2021-09-14 05:16:57');

--
-- Trigger `tnilaiakhir`
--
DELIMITER $$
CREATE TRIGGER `tnilaiakhir` AFTER DELETE ON `tnilaiakhir` FOR EACH ROW BEGIN
	
	DELETE FROM tnilaiakhirdtl WHERE idnilaiakhir = OLD.idnilaiakhir;
	
		
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tnilaiakhirdtl`
--

CREATE TABLE `tnilaiakhirdtl` (
  `idnilaiakhirdtl` int(11) NOT NULL,
  `idnilaiakhir` int(11) DEFAULT NULL,
  `idpeserta` int(11) DEFAULT NULL,
  `mendengar` int(11) DEFAULT NULL,
  `membaca` int(11) DEFAULT NULL,
  `kosakata` int(11) DEFAULT NULL,
  `percakapan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tnilaiakhirdtl`
--

INSERT INTO `tnilaiakhirdtl` (`idnilaiakhirdtl`, `idnilaiakhir`, `idpeserta`, `mendengar`, `membaca`, `kosakata`, `percakapan`) VALUES
(121, 119, 3, 30, 28, 200, 145),
(122, 119, 4, 50, 40, 60, 60),
(123, 120, 4, 10, 20, 30, 40);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tnilaiharian`
--

CREATE TABLE `tnilaiharian` (
  `idnilaiharian` int(11) NOT NULL,
  `tgl` date DEFAULT NULL,
  `kdnilaiharian` varchar(10) DEFAULT NULL,
  `idpengajar` int(11) DEFAULT NULL,
  `idkelas` int(11) DEFAULT NULL,
  `idpelajaran` int(11) DEFAULT NULL,
  `ket` varchar(255) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tnilaiharian`
--

INSERT INTO `tnilaiharian` (`idnilaiharian`, `tgl`, `kdnilaiharian`, `idpengajar`, `idkelas`, `idpelajaran`, `ket`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(114, '2021-08-21', '2108004', 11, 3, 155, NULL, NULL, NULL, NULL, NULL),
(115, '2021-08-24', '2108005', 12, 4, 156, NULL, NULL, NULL, NULL, NULL),
(123, '2021-09-12', '2109001', 18, 3, 155, NULL, 18, '2021-09-12 05:51:07', NULL, NULL);

--
-- Trigger `tnilaiharian`
--
DELIMITER $$
CREATE TRIGGER `tnilaiharian` AFTER DELETE ON `tnilaiharian` FOR EACH ROW BEGIN
	
	DELETE FROM tnilaihariandtl WHERE idnilaiharian = OLD.idnilaiharian;
	
		
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tnilaihariandtl`
--

CREATE TABLE `tnilaihariandtl` (
  `idnilaihariandtl` int(11) NOT NULL,
  `idnilaiharian` int(11) DEFAULT NULL,
  `idpeserta` int(11) DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tnilaihariandtl`
--

INSERT INTO `tnilaihariandtl` (`idnilaihariandtl`, `idnilaiharian`, `idpeserta`, `nilai`) VALUES
(111, 114, 3, 20),
(112, 114, 4, 70),
(113, 115, 3, 30),
(114, 115, 4, 40),
(122, 123, 3, 90);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tnilaisikap`
--

CREATE TABLE `tnilaisikap` (
  `idnilaisikap` int(11) NOT NULL,
  `tgl` date DEFAULT NULL,
  `idpengajar` int(10) DEFAULT NULL,
  `idpeserta` int(10) DEFAULT NULL,
  `idkelas` int(10) DEFAULT NULL,
  `idsikap` int(11) DEFAULT NULL,
  `bobot` int(11) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tnilaisikap`
--

INSERT INTO `tnilaisikap` (`idnilaisikap`, `tgl`, `idpengajar`, `idpeserta`, `idkelas`, `idsikap`, `bobot`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(1, '2021-08-21', NULL, 4, 4, 102, 80, 18, NULL, 18, NULL),
(2, '2021-08-21', NULL, 3, 3, 105, 90, 18, NULL, 18, NULL),
(3, '2021-09-11', 17, 3, 3, 101, 100, 17, '2021-09-11 20:27:20', 17, '2021-09-11 20:27:20');

--
-- Trigger `tnilaisikap`
--
DELIMITER $$
CREATE TRIGGER `tnilaisikap` AFTER DELETE ON `tnilaisikap` FOR EACH ROW BEGIN
	
	DELETE FROM tnilaisikapdtl WHERE idnilaisikap = OLD.idnilaisikap;
	
		
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpekerjaan`
--

CREATE TABLE `tpekerjaan` (
  `idpekerjaan` int(11) NOT NULL,
  `pekerjaan` varchar(150) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tpekerjaan`
--

INSERT INTO `tpekerjaan` (`idpekerjaan`, `pekerjaan`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(158, 'PNS', NULL, NULL, NULL, NULL),
(159, 'Pensiunan PNS', NULL, NULL, NULL, NULL),
(160, 'Petani', NULL, NULL, NULL, NULL),
(161, 'Pedagang', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpelajaran`
--

CREATE TABLE `tpelajaran` (
  `idpelajaran` int(11) NOT NULL,
  `kdpelajaran` varchar(10) DEFAULT NULL,
  `nmpelajaran` varchar(150) DEFAULT NULL,
  `idkelas` int(11) DEFAULT NULL,
  `ket` varchar(150) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tpelajaran`
--

INSERT INTO `tpelajaran` (`idpelajaran`, `kdpelajaran`, `nmpelajaran`, `idkelas`, `ket`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(155, '001', 'Mina 2', 0, NULL, NULL, NULL, 17, '2021-09-16 09:57:58'),
(156, '002', 'MINA 1', 4, NULL, NULL, NULL, NULL, NULL);

--
-- Trigger `tpelajaran`
--
DELIMITER $$
CREATE TRIGGER `tpelajaran` AFTER DELETE ON `tpelajaran` FOR EACH ROW BEGIN
	
	DELETE FROM tpelajarandtl WHERE idpelajaran = OLD.idpelajaran;
	
		
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpelajarandtl`
--

CREATE TABLE `tpelajarandtl` (
  `idpelajarandtl` int(11) NOT NULL,
  `idpelajaran` varchar(11) DEFAULT NULL,
  `bab` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tpelajarandtl`
--

INSERT INTO `tpelajarandtl` (`idpelajarandtl`, `idpelajaran`, `bab`) VALUES
(133, '156', 'BAB 1'),
(134, '156', 'BAB 2'),
(135, '156', 'BAB 3'),
(136, '156', 'BAB 4'),
(137, '156', 'BAB 6'),
(138, '156', 'BAB 7'),
(139, '156', 'BAB 8'),
(140, '156', 'BAB 9'),
(144, '155', 'BAB 12'),
(145, '155', 'BAB 25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpendidikan`
--

CREATE TABLE `tpendidikan` (
  `idpendidikan` int(11) NOT NULL,
  `kdpendidikan` varchar(5) DEFAULT NULL,
  `pendidikan` varchar(50) DEFAULT NULL,
  `ket` varchar(150) DEFAULT NULL,
  `insert_by` float DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` float DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tpendidikan`
--

INSERT INTO `tpendidikan` (`idpendidikan`, `kdpendidikan`, `pendidikan`, `ket`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(21, '001', 'S2', '', NULL, NULL, 61, '2021-10-08 10:56:07'),
(22, '002', 'SI', NULL, NULL, NULL, NULL, NULL),
(23, '003', 'SMA/SMK', NULL, NULL, NULL, NULL, NULL),
(24, '004', 'SMP', NULL, NULL, NULL, NULL, NULL),
(25, '005', 'SD', NULL, NULL, NULL, NULL, NULL),
(26, '006', 'paud', NULL, 61, '2021-10-07 11:34:35', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpeserta`
--

CREATE TABLE `tpeserta` (
  `idpeserta` int(11) NOT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `nama` varchar(128) NOT NULL,
  `jk` varchar(10) DEFAULT NULL,
  `tmp_lahir` varchar(50) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `nik` varchar(50) NOT NULL,
  `agama` varchar(15) DEFAULT NULL,
  `alamat` varchar(70) DEFAULT NULL,
  `rt` varchar(5) DEFAULT NULL,
  `rw` varchar(5) DEFAULT NULL,
  `desa` varchar(50) DEFAULT NULL,
  `kecamatan` varchar(50) DEFAULT NULL,
  `kabupaten` varchar(50) DEFAULT NULL,
  `propinsi` varchar(50) DEFAULT NULL,
  `negara` varchar(50) DEFAULT NULL,
  `kode_pos` varchar(20) DEFAULT NULL,
  `tlp` varchar(20) DEFAULT NULL,
  `idkelas` int(11) DEFAULT NULL,
  `idbiaya` int(11) DEFAULT NULL,
  `nmayah` varchar(100) DEFAULT NULL,
  `tgl_lahir_ayah` date DEFAULT NULL,
  `pendidikan_ayah` int(20) DEFAULT NULL,
  `pekerjaan_ayah` varchar(50) DEFAULT NULL,
  `penghasilan_ayah` varchar(20) DEFAULT NULL,
  `hp_ayah` varchar(20) DEFAULT NULL,
  `hubungan_ayah` varchar(50) DEFAULT NULL,
  `nmibu` varchar(100) DEFAULT NULL,
  `tgl_lahir_ibu` date DEFAULT NULL,
  `pendidikan_ibu` int(20) DEFAULT NULL,
  `pekerjaan_ibu` varchar(50) DEFAULT NULL,
  `penghasilan_ibu` varchar(20) DEFAULT NULL,
  `hp_ibu` varchar(20) DEFAULT NULL,
  `hubungan_ibu` varchar(50) DEFAULT NULL,
  `tinggi_badan` varchar(5) DEFAULT NULL,
  `berat_badan` varchar(5) DEFAULT NULL,
  `jarak_dari_rumah` varchar(5) DEFAULT NULL,
  `status_tempat_tinggal` varchar(50) DEFAULT NULL,
  `status_dalam_keluarga` varchar(50) DEFAULT NULL,
  `jml_saudara_kandung` varchar(5) DEFAULT NULL,
  `penyakit_pernah_diderita` varchar(50) DEFAULT NULL,
  `flag` int(1) DEFAULT NULL COMMENT 'sudah daftar ulang atau belum',
  `flag_kursus` int(11) DEFAULT NULL COMMENT 'pelunasan kursus',
  `flag_pemberangkatan` int(11) DEFAULT NULL COMMENT 'pelunasan pemberangkatan',
  `imgkk` varchar(255) DEFAULT NULL COMMENT 'scan kk',
  `imgktp` varchar(255) DEFAULT NULL COMMENT 'scan ktp',
  `imgijazah` varchar(255) DEFAULT NULL COMMENT 'scan ijazah',
  `imgphoto` varchar(255) DEFAULT NULL COMMENT 'scan photo',
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tpeserta`
--

INSERT INTO `tpeserta` (`idpeserta`, `nip`, `tgl`, `nama`, `jk`, `tmp_lahir`, `tgl_lahir`, `nik`, `agama`, `alamat`, `rt`, `rw`, `desa`, `kecamatan`, `kabupaten`, `propinsi`, `negara`, `kode_pos`, `tlp`, `idkelas`, `idbiaya`, `nmayah`, `tgl_lahir_ayah`, `pendidikan_ayah`, `pekerjaan_ayah`, `penghasilan_ayah`, `hp_ayah`, `hubungan_ayah`, `nmibu`, `tgl_lahir_ibu`, `pendidikan_ibu`, `pekerjaan_ibu`, `penghasilan_ibu`, `hp_ibu`, `hubungan_ibu`, `tinggi_badan`, `berat_badan`, `jarak_dari_rumah`, `status_tempat_tinggal`, `status_dalam_keluarga`, `jml_saudara_kandung`, `penyakit_pernah_diderita`, `flag`, `flag_kursus`, `flag_pemberangkatan`, `imgkk`, `imgktp`, `imgijazah`, `imgphoto`, `password`, `email`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(3, '001', NULL, 'Aqmar Nadhif', 'L', 'Cianjur', '2021-09-23', '10001', 'Islam', 'Kp. Baru', '01', '09', 'Sukataris', 'Karang Tengah', 'Cianjur', 'Jawabarat', NULL, '123', '087820001122', 3, NULL, 'Sobara', '2021-09-22', 25, '161', '12345', '087820001122', NULL, 'Ayi Komara', '2021-09-07', 22, '159', '18000000', '1234', NULL, '185', '85', '10', 'Ngobtrak', 'Cikal', '2', 'Tidak Ada', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MTIzNDU2', NULL, NULL, NULL, NULL, NULL),
(4, '002', NULL, 'Ibrahim', 'L', NULL, NULL, '10002', 'Islam', 'Caringin', '02', '10', 'Caringin', 'Caringin', 'Cianjur', 'Jawabarat', NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MTIzNDU2', NULL, NULL, NULL, NULL, NULL),
(102, NULL, NULL, 'Nissa Annisa', NULL, NULL, NULL, '10003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MTIzNDU2', 'gums2015@gmail.com', 18, NULL, NULL, NULL),
(103, NULL, NULL, 'teguh ariep nugraha', NULL, NULL, NULL, '261185', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bmFuZGhpcmExOQ==', 'teguhariepnugraha@gmail.com', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tplaningmarketing`
--

CREATE TABLE `tplaningmarketing` (
  `idPlaning` int(11) NOT NULL,
  `tglEvent` date DEFAULT NULL,
  `alokasibugetevent` varchar(50) DEFAULT NULL,
  `tempatPameran` varchar(50) DEFAULT NULL,
  `unitDisplay` varchar(50) DEFAULT NULL,
  `qtySales` int(11) DEFAULT NULL,
  `namaSH` varchar(30) DEFAULT NULL,
  `inq` int(11) DEFAULT NULL,
  `hp` int(11) DEFAULT NULL,
  `spk` int(11) DEFAULT NULL,
  `inq_end_month` int(11) DEFAULT NULL,
  `hp_end_month` int(11) DEFAULT NULL,
  `spk_end_month` int(11) DEFAULT NULL,
  `difRealSPKTgt` double DEFAULT NULL,
  `ratioInqSPK` double DEFAULT NULL,
  `difRealSPKTgtPercent` double DEFAULT NULL,
  `totalCost` double DEFAULT NULL,
  `costSPK` double DEFAULT NULL,
  `analisa` varchar(200) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` date DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tplaningmarketing`
--

INSERT INTO `tplaningmarketing` (`idPlaning`, `tglEvent`, `alokasibugetevent`, `tempatPameran`, `unitDisplay`, `qtySales`, `namaSH`, `inq`, `hp`, `spk`, `inq_end_month`, `hp_end_month`, `spk_end_month`, `difRealSPKTgt`, `ratioInqSPK`, `difRealSPKTgtPercent`, `totalCost`, `costSPK`, `analisa`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(29, '2021-10-12', 'Pameran - input di MAOS', 'cianjur super mall12', 'ertiga', 0, 'soleh', 1, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 'kurang minta pembeli', NULL, NULL, NULL, NULL),
(31, '2021-10-11', 'Pameran - input di MAOS', 'r', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL),
(32, '2021-10-11', 'Showroom Event - input di MAOS', 'r', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL),
(33, '2021-10-18', 'Pameran tambahan PT. SIS', 'teguh', 'aaa', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL),
(34, '2021-11-06', 'Pameran - input di MAOS', 'ggg', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL),
(35, '2021-10-12', 'Showroom Event - input di MAOS', 'testing', 'jaringan', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tregistrasi`
--

CREATE TABLE `tregistrasi` (
  `idregistrasi` int(11) NOT NULL,
  `kdregistrasi` varchar(10) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `idakun` int(10) DEFAULT NULL,
  `idpeserta` int(10) DEFAULT NULL,
  `nominal` float DEFAULT NULL,
  `ket` varchar(150) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tregistrasi`
--

INSERT INTO `tregistrasi` (`idregistrasi`, `kdregistrasi`, `tgl`, `idakun`, `idpeserta`, `nominal`, `ket`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(1, '2108001', '2021-08-17', 19, 4, 420000, NULL, NULL, NULL, NULL, NULL),
(2, '2108002', '2021-08-17', 18, 3, 3500, NULL, NULL, NULL, NULL, NULL),
(9, '2108003', '2021-08-18', 19, 4, 420000, NULL, NULL, NULL, NULL, NULL);

--
-- Trigger `tregistrasi`
--
DELIMITER $$
CREATE TRIGGER `tlistpembayaran` AFTER INSERT ON `tregistrasi` FOR EACH ROW BEGIN
	SET @idpeserta = new.idpeserta;
	
	INSERT INTO tlistpembayaran (idakun, idpeserta, nominal)
	SELECT  a.idakun, @idpeserta, a.pagu
	FROM takun_transaksi  AS a   WHERE a.flag = 1;
    END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `tregistrasi` AFTER DELETE ON `tregistrasi` FOR EACH ROW BEGIN
	
	DELETE FROM tlistpembayaran WHERE idpeserta = OLD.idpeserta;
	
		
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tsikap`
--

CREATE TABLE `tsikap` (
  `idsikap` int(11) NOT NULL,
  `kdsikap` varchar(10) DEFAULT NULL,
  `sikap` varchar(150) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tsikap`
--

INSERT INTO `tsikap` (`idsikap`, `kdsikap`, `sikap`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(102, '001', 'Adil', NULL, NULL, NULL, NULL),
(103, '002', 'Sehat', NULL, NULL, NULL, NULL),
(104, '003', 'Sejahtera', NULL, NULL, NULL, NULL),
(107, '2108002', 'Peneakan disiplin', NULL, NULL, NULL, NULL),
(108, '2108003', 'Penilain Prilaku', NULL, NULL, NULL, NULL);

--
-- Trigger `tsikap`
--
DELIMITER $$
CREATE TRIGGER `tsikap` AFTER DELETE ON `tsikap` FOR EACH ROW BEGIN
	
	DELETE FROM tsikapdtl WHERE idsikap = OLD.idsikap;
	
		
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tsikapdtl`
--

CREATE TABLE `tsikapdtl` (
  `idsikapdtl` int(11) NOT NULL,
  `idsikap` int(11) DEFAULT NULL,
  `sikapdtl` varchar(250) DEFAULT NULL,
  `bobot` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tsikapdtl`
--

INSERT INTO `tsikapdtl` (`idsikapdtl`, `idsikap`, `sikapdtl`, `bobot`) VALUES
(99, 102, 'ramah tamah', 75),
(100, 103, 'baik bergaul', 85),
(101, 104, 'akur', 100),
(102, 107, 'Belaku sopan dikelas dan asrama', 80),
(103, 108, 'memebrikan informasi penting sesama anggota ', 80),
(104, 108, 'Membantu sesama anggota', 75),
(105, 108, 'Belaku adil sesama', 90),
(107, 108, 'Memberikan contoh pada peserta lain tentang aturan di asrama', 77);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ttransaksi`
--

CREATE TABLE `ttransaksi` (
  `idtransaksi` int(11) NOT NULL,
  `tgl` date DEFAULT NULL,
  `nota` varchar(25) DEFAULT NULL,
  `supplier` varchar(75) DEFAULT NULL,
  `ket` varchar(150) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ttransaksi`
--

INSERT INTO `ttransaksi` (`idtransaksi`, `tgl`, `nota`, `supplier`, `ket`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(81, '2021-08-09', '2108003', 'CV. Bangun Jaya', 'Pembelian Alat Bangunan Serbaguna', NULL, NULL, NULL, NULL);

--
-- Trigger `ttransaksi`
--
DELIMITER $$
CREATE TRIGGER `ttransaksi` AFTER DELETE ON `ttransaksi` FOR EACH ROW BEGIN
	
	DELETE FROM ttransaksidtl WHERE idtransaksi = OLD.idtransaksi;
	
		
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ttransaksidtl`
--

CREATE TABLE `ttransaksidtl` (
  `idtransaksidtl` int(11) NOT NULL,
  `idtransaksi` int(11) DEFAULT NULL,
  `idakun` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga` float DEFAULT NULL,
  `total` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ttransaksidtl`
--

INSERT INTO `ttransaksidtl` (`idtransaksidtl`, `idtransaksi`, `idakun`, `qty`, `harga`, `total`) VALUES
(78, 81, 12, 16, 2500, 40000),
(79, 81, 13, 11, 3500, 38500),
(81, 82, 18, 20, 15000, 300000),
(82, 83, 19, 20, 1000000, 20000000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ttugas`
--

CREATE TABLE `ttugas` (
  `idtugas` int(11) NOT NULL,
  `kdtugas` varchar(15) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `idkaryawan` int(11) DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `ket` varchar(255) DEFAULT NULL,
  `insert_by` int(11) DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ttugas`
--

INSERT INTO `ttugas` (`idtugas`, `kdtugas`, `tgl`, `idkaryawan`, `tgl_mulai`, `ket`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES
(16, '2109001', '2021-09-01', 12, '2021-09-22', 'Belajajr Berbagi Waktu', NULL, NULL, NULL, NULL);

--
-- Trigger `ttugas`
--
DELIMITER $$
CREATE TRIGGER `ttugas` AFTER DELETE ON `ttugas` FOR EACH ROW BEGIN
	
	DELETE FROM ttugasdtl WHERE idtugas = OLD.idtugas;
	
		
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ttugasdtl`
--

CREATE TABLE `ttugasdtl` (
  `idtugasdtl` int(11) NOT NULL,
  `idtugas` int(11) DEFAULT NULL,
  `idkaryawan` int(11) DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_beres` date DEFAULT NULL,
  `file_tugas` varchar(255) DEFAULT NULL,
  `file_hasil_tugas` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ttugasdtl`
--

INSERT INTO `ttugasdtl` (`idtugasdtl`, `idtugas`, `idkaryawan`, `tgl_mulai`, `tgl_beres`, `file_tugas`, `file_hasil_tugas`) VALUES
(17, 16, 11, '2021-09-22', '2021-09-14', '1.jpg', NULL),
(18, 16, 12, '2021-09-22', '2021-09-29', 'attBackup.mdb', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `villages`
--

CREATE TABLE `villages` (
  `id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `district_id` char(7) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur untuk view `database_schema`
--
DROP TABLE IF EXISTS `database_schema`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `database_schema`  AS SELECT `information_schema`.`columns`.`TABLE_CATALOG` AS `TABLE_CATALOG`, `information_schema`.`columns`.`TABLE_SCHEMA` AS `TABLE_SCHEMA`, `information_schema`.`columns`.`TABLE_NAME` AS `TABLE_NAME`, `information_schema`.`columns`.`COLUMN_NAME` AS `COLUMN_NAME`, `information_schema`.`columns`.`ORDINAL_POSITION` AS `ORDINAL_POSITION`, `information_schema`.`columns`.`COLUMN_DEFAULT` AS `COLUMN_DEFAULT`, `information_schema`.`columns`.`IS_NULLABLE` AS `IS_NULLABLE`, `information_schema`.`columns`.`DATA_TYPE` AS `DATA_TYPE`, `information_schema`.`columns`.`CHARACTER_MAXIMUM_LENGTH` AS `CHARACTER_MAXIMUM_LENGTH`, `information_schema`.`columns`.`CHARACTER_OCTET_LENGTH` AS `CHARACTER_OCTET_LENGTH`, `information_schema`.`columns`.`NUMERIC_PRECISION` AS `NUMERIC_PRECISION`, `information_schema`.`columns`.`NUMERIC_SCALE` AS `NUMERIC_SCALE`, `information_schema`.`columns`.`DATETIME_PRECISION` AS `DATETIME_PRECISION`, `information_schema`.`columns`.`CHARACTER_SET_NAME` AS `CHARACTER_SET_NAME`, `information_schema`.`columns`.`COLLATION_NAME` AS `COLLATION_NAME`, `information_schema`.`columns`.`COLUMN_TYPE` AS `COLUMN_TYPE`, `information_schema`.`columns`.`COLUMN_KEY` AS `COLUMN_KEY`, `information_schema`.`columns`.`EXTRA` AS `EXTRA`, `information_schema`.`columns`.`PRIVILEGES` AS `PRIVILEGES`, `information_schema`.`columns`.`COLUMN_COMMENT` AS `COLUMN_COMMENT` FROM `information_schema`.`columns` WHERE `information_schema`.`columns`.`TABLE_SCHEMA` = 'mizuno' ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `detail_pengajuan_insentive_sro`
--
ALTER TABLE `detail_pengajuan_insentive_sro`
  ADD PRIMARY KEY (`iddtlpengajuan`);

--
-- Indeks untuk tabel `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `districts_id_index` (`regency_id`);

--
-- Indeks untuk tabel `pengajuan_insentive_sro`
--
ALTER TABLE `pengajuan_insentive_sro`
  ADD PRIMARY KEY (`idpengajuan`);

--
-- Indeks untuk tabel `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `regencies`
--
ALTER TABLE `regencies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `regencies_province_id_index` (`province_id`);

--
-- Indeks untuk tabel `sysappdashboard`
--
ALTER TABLE `sysappdashboard`
  ADD PRIMARY KEY (`iddashboard`);

--
-- Indeks untuk tabel `sysappmenu`
--
ALTER TABLE `sysappmenu`
  ADD PRIMARY KEY (`idmenu`);

--
-- Indeks untuk tabel `sysappmenuitem`
--
ALTER TABLE `sysappmenuitem`
  ADD PRIMARY KEY (`idmenuitem`);

--
-- Indeks untuk tabel `sysuser`
--
ALTER TABLE `sysuser`
  ADD PRIMARY KEY (`iduser`);

--
-- Indeks untuk tabel `tabsen`
--
ALTER TABLE `tabsen`
  ADD PRIMARY KEY (`idabsen`),
  ADD KEY `idabsen` (`idabsen`);

--
-- Indeks untuk tabel `tabsensiswa`
--
ALTER TABLE `tabsensiswa`
  ADD PRIMARY KEY (`idabsensiswa`),
  ADD KEY `idkehadiran_siswa` (`idabsensiswa`);

--
-- Indeks untuk tabel `tabsensiswadtl`
--
ALTER TABLE `tabsensiswadtl`
  ADD PRIMARY KEY (`idabsensiswadtl`),
  ADD KEY `idkehadiran_siswadtl` (`idabsensiswadtl`);

--
-- Indeks untuk tabel `takun`
--
ALTER TABLE `takun`
  ADD PRIMARY KEY (`idakun`),
  ADD KEY `idakun` (`idakun`);

--
-- Indeks untuk tabel `takundtl`
--
ALTER TABLE `takundtl`
  ADD PRIMARY KEY (`idakundtl`),
  ADD KEY `idakundtl` (`idakundtl`);

--
-- Indeks untuk tabel `takun_transaksi`
--
ALTER TABLE `takun_transaksi`
  ADD PRIMARY KEY (`idakun_transaksi`),
  ADD KEY `idbiaya` (`idakun_transaksi`);

--
-- Indeks untuk tabel `tangsuran`
--
ALTER TABLE `tangsuran`
  ADD PRIMARY KEY (`idangsuran`);

--
-- Indeks untuk tabel `tangsurandtl`
--
ALTER TABLE `tangsurandtl`
  ADD PRIMARY KEY (`idangsurandtl`);

--
-- Indeks untuk tabel `targetdo`
--
ALTER TABLE `targetdo`
  ADD PRIMARY KEY (`idtargetdo`);

--
-- Indeks untuk tabel `targetrka`
--
ALTER TABLE `targetrka`
  ADD PRIMARY KEY (`idtargetrka`);

--
-- Indeks untuk tabel `targetunitentry`
--
ALTER TABLE `targetunitentry`
  ADD PRIMARY KEY (`idtargetunitentry`);

--
-- Indeks untuk tabel `tcabang`
--
ALTER TABLE `tcabang`
  ADD PRIMARY KEY (`idcabang`);

--
-- Indeks untuk tabel `tgaji`
--
ALTER TABLE `tgaji`
  ADD PRIMARY KEY (`idgaji`),
  ADD KEY `idgaji` (`idgaji`);

--
-- Indeks untuk tabel `tjabatan`
--
ALTER TABLE `tjabatan`
  ADD PRIMARY KEY (`idjabatan`),
  ADD KEY `index_jabatan` (`kdjabatan`,`jabatan`);

--
-- Indeks untuk tabel `tjadwal`
--
ALTER TABLE `tjadwal`
  ADD PRIMARY KEY (`idjadwal`),
  ADD KEY `idjadwal` (`idjadwal`);

--
-- Indeks untuk tabel `tjadwaldtl`
--
ALTER TABLE `tjadwaldtl`
  ADD PRIMARY KEY (`idjadwaldtl`),
  ADD KEY `idjadwaldtl` (`idjadwaldtl`);

--
-- Indeks untuk tabel `tkaryawan`
--
ALTER TABLE `tkaryawan`
  ADD PRIMARY KEY (`idkaryawan`);

--
-- Indeks untuk tabel `tkelas`
--
ALTER TABLE `tkelas`
  ADD PRIMARY KEY (`idkelas`),
  ADD KEY `idkelas` (`idkelas`);

--
-- Indeks untuk tabel `tlembur`
--
ALTER TABLE `tlembur`
  ADD PRIMARY KEY (`idlembur`),
  ADD KEY `idlembur` (`idlembur`);

--
-- Indeks untuk tabel `tlemburdtl`
--
ALTER TABLE `tlemburdtl`
  ADD PRIMARY KEY (`idlemburdtl`),
  ADD KEY `idlemburdtl` (`idlemburdtl`);

--
-- Indeks untuk tabel `tlistpembayaran`
--
ALTER TABLE `tlistpembayaran`
  ADD PRIMARY KEY (`idlistpembayaran`);

--
-- Indeks untuk tabel `tlistregistrasi`
--
ALTER TABLE `tlistregistrasi`
  ADD PRIMARY KEY (`idlistregistrasi`);

--
-- Indeks untuk tabel `tlistregistrasidtl`
--
ALTER TABLE `tlistregistrasidtl`
  ADD PRIMARY KEY (`idlistregistrasidtl`);

--
-- Indeks untuk tabel `tnilaiakhir`
--
ALTER TABLE `tnilaiakhir`
  ADD PRIMARY KEY (`idnilaiakhir`),
  ADD KEY `idnilaiakhir` (`idnilaiakhir`);

--
-- Indeks untuk tabel `tnilaiakhirdtl`
--
ALTER TABLE `tnilaiakhirdtl`
  ADD PRIMARY KEY (`idnilaiakhirdtl`),
  ADD KEY `idnilaiakhirdtl` (`idnilaiakhirdtl`);

--
-- Indeks untuk tabel `tnilaiharian`
--
ALTER TABLE `tnilaiharian`
  ADD PRIMARY KEY (`idnilaiharian`),
  ADD KEY `idnilaiharian` (`idnilaiharian`);

--
-- Indeks untuk tabel `tnilaihariandtl`
--
ALTER TABLE `tnilaihariandtl`
  ADD PRIMARY KEY (`idnilaihariandtl`),
  ADD KEY `idnilaihariandtl` (`idnilaihariandtl`);

--
-- Indeks untuk tabel `tnilaisikap`
--
ALTER TABLE `tnilaisikap`
  ADD PRIMARY KEY (`idnilaisikap`),
  ADD KEY `idnilai_sikap` (`idnilaisikap`);

--
-- Indeks untuk tabel `tpekerjaan`
--
ALTER TABLE `tpekerjaan`
  ADD PRIMARY KEY (`idpekerjaan`),
  ADD KEY `idpekerjaan` (`idpekerjaan`);

--
-- Indeks untuk tabel `tpelajaran`
--
ALTER TABLE `tpelajaran`
  ADD PRIMARY KEY (`idpelajaran`),
  ADD KEY `idpelajaran` (`idpelajaran`);

--
-- Indeks untuk tabel `tpelajarandtl`
--
ALTER TABLE `tpelajarandtl`
  ADD PRIMARY KEY (`idpelajarandtl`);

--
-- Indeks untuk tabel `tpendidikan`
--
ALTER TABLE `tpendidikan`
  ADD PRIMARY KEY (`idpendidikan`),
  ADD KEY `index_pendidikan` (`idpendidikan`,`kdpendidikan`,`pendidikan`);

--
-- Indeks untuk tabel `tpeserta`
--
ALTER TABLE `tpeserta`
  ADD PRIMARY KEY (`idpeserta`),
  ADD KEY `idpeserta` (`idpeserta`);

--
-- Indeks untuk tabel `tplaningmarketing`
--
ALTER TABLE `tplaningmarketing`
  ADD PRIMARY KEY (`idPlaning`);

--
-- Indeks untuk tabel `tregistrasi`
--
ALTER TABLE `tregistrasi`
  ADD PRIMARY KEY (`idregistrasi`);

--
-- Indeks untuk tabel `tsikap`
--
ALTER TABLE `tsikap`
  ADD PRIMARY KEY (`idsikap`),
  ADD KEY `idsikap` (`idsikap`);

--
-- Indeks untuk tabel `tsikapdtl`
--
ALTER TABLE `tsikapdtl`
  ADD PRIMARY KEY (`idsikapdtl`),
  ADD KEY `idsikapdtl` (`idsikapdtl`);

--
-- Indeks untuk tabel `ttransaksi`
--
ALTER TABLE `ttransaksi`
  ADD PRIMARY KEY (`idtransaksi`);

--
-- Indeks untuk tabel `ttransaksidtl`
--
ALTER TABLE `ttransaksidtl`
  ADD PRIMARY KEY (`idtransaksidtl`);

--
-- Indeks untuk tabel `ttugas`
--
ALTER TABLE `ttugas`
  ADD PRIMARY KEY (`idtugas`),
  ADD KEY `idtugas` (`idtugas`);

--
-- Indeks untuk tabel `ttugasdtl`
--
ALTER TABLE `ttugasdtl`
  ADD PRIMARY KEY (`idtugasdtl`),
  ADD KEY `idtugasdtl` (`idtugasdtl`);

--
-- Indeks untuk tabel `villages`
--
ALTER TABLE `villages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `villages_district_id_index` (`district_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `detail_pengajuan_insentive_sro`
--
ALTER TABLE `detail_pengajuan_insentive_sro`
  MODIFY `iddtlpengajuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT untuk tabel `pengajuan_insentive_sro`
--
ALTER TABLE `pengajuan_insentive_sro`
  MODIFY `idpengajuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT untuk tabel `sysappdashboard`
--
ALTER TABLE `sysappdashboard`
  MODIFY `iddashboard` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `sysappmenu`
--
ALTER TABLE `sysappmenu`
  MODIFY `idmenu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `sysappmenuitem`
--
ALTER TABLE `sysappmenuitem`
  MODIFY `idmenuitem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT untuk tabel `sysuser`
--
ALTER TABLE `sysuser`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT untuk tabel `tabsen`
--
ALTER TABLE `tabsen`
  MODIFY `idabsen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tabsensiswa`
--
ALTER TABLE `tabsensiswa`
  MODIFY `idabsensiswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `tabsensiswadtl`
--
ALTER TABLE `tabsensiswadtl`
  MODIFY `idabsensiswadtl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `takun`
--
ALTER TABLE `takun`
  MODIFY `idakun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `takundtl`
--
ALTER TABLE `takundtl`
  MODIFY `idakundtl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `takun_transaksi`
--
ALTER TABLE `takun_transaksi`
  MODIFY `idakun_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT untuk tabel `tangsuran`
--
ALTER TABLE `tangsuran`
  MODIFY `idangsuran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT untuk tabel `tangsurandtl`
--
ALTER TABLE `tangsurandtl`
  MODIFY `idangsurandtl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT untuk tabel `targetdo`
--
ALTER TABLE `targetdo`
  MODIFY `idtargetdo` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT untuk tabel `targetrka`
--
ALTER TABLE `targetrka`
  MODIFY `idtargetrka` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT untuk tabel `targetunitentry`
--
ALTER TABLE `targetunitentry`
  MODIFY `idtargetunitentry` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tcabang`
--
ALTER TABLE `tcabang`
  MODIFY `idcabang` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tgaji`
--
ALTER TABLE `tgaji`
  MODIFY `idgaji` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT untuk tabel `tjabatan`
--
ALTER TABLE `tjabatan`
  MODIFY `idjabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT untuk tabel `tjadwal`
--
ALTER TABLE `tjadwal`
  MODIFY `idjadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT untuk tabel `tjadwaldtl`
--
ALTER TABLE `tjadwaldtl`
  MODIFY `idjadwaldtl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT untuk tabel `tkaryawan`
--
ALTER TABLE `tkaryawan`
  MODIFY `idkaryawan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT untuk tabel `tkelas`
--
ALTER TABLE `tkelas`
  MODIFY `idkelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tlembur`
--
ALTER TABLE `tlembur`
  MODIFY `idlembur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tlemburdtl`
--
ALTER TABLE `tlemburdtl`
  MODIFY `idlemburdtl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tlistpembayaran`
--
ALTER TABLE `tlistpembayaran`
  MODIFY `idlistpembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `tlistregistrasi`
--
ALTER TABLE `tlistregistrasi`
  MODIFY `idlistregistrasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT untuk tabel `tlistregistrasidtl`
--
ALTER TABLE `tlistregistrasidtl`
  MODIFY `idlistregistrasidtl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT untuk tabel `tnilaiakhir`
--
ALTER TABLE `tnilaiakhir`
  MODIFY `idnilaiakhir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT untuk tabel `tnilaiakhirdtl`
--
ALTER TABLE `tnilaiakhirdtl`
  MODIFY `idnilaiakhirdtl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT untuk tabel `tnilaiharian`
--
ALTER TABLE `tnilaiharian`
  MODIFY `idnilaiharian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT untuk tabel `tnilaihariandtl`
--
ALTER TABLE `tnilaihariandtl`
  MODIFY `idnilaihariandtl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT untuk tabel `tnilaisikap`
--
ALTER TABLE `tnilaisikap`
  MODIFY `idnilaisikap` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tpekerjaan`
--
ALTER TABLE `tpekerjaan`
  MODIFY `idpekerjaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT untuk tabel `tpelajaran`
--
ALTER TABLE `tpelajaran`
  MODIFY `idpelajaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;

--
-- AUTO_INCREMENT untuk tabel `tpelajarandtl`
--
ALTER TABLE `tpelajarandtl`
  MODIFY `idpelajarandtl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT untuk tabel `tpendidikan`
--
ALTER TABLE `tpendidikan`
  MODIFY `idpendidikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `tpeserta`
--
ALTER TABLE `tpeserta`
  MODIFY `idpeserta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT untuk tabel `tplaningmarketing`
--
ALTER TABLE `tplaningmarketing`
  MODIFY `idPlaning` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT untuk tabel `tregistrasi`
--
ALTER TABLE `tregistrasi`
  MODIFY `idregistrasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tsikap`
--
ALTER TABLE `tsikap`
  MODIFY `idsikap` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT untuk tabel `tsikapdtl`
--
ALTER TABLE `tsikapdtl`
  MODIFY `idsikapdtl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT untuk tabel `ttransaksi`
--
ALTER TABLE `ttransaksi`
  MODIFY `idtransaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT untuk tabel `ttransaksidtl`
--
ALTER TABLE `ttransaksidtl`
  MODIFY `idtransaksidtl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT untuk tabel `ttugas`
--
ALTER TABLE `ttugas`
  MODIFY `idtugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `ttugasdtl`
--
ALTER TABLE `ttugasdtl`
  MODIFY `idtugasdtl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `districts`
--
ALTER TABLE `districts`
  ADD CONSTRAINT `districts_regency_id_foreign` FOREIGN KEY (`regency_id`) REFERENCES `regencies` (`id`);

--
-- Ketidakleluasaan untuk tabel `regencies`
--
ALTER TABLE `regencies`
  ADD CONSTRAINT `regencies_province_id_foreign` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`);

--
-- Ketidakleluasaan untuk tabel `villages`
--
ALTER TABLE `villages`
  ADD CONSTRAINT `villages_district_id_foreign` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
