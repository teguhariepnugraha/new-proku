var today = new Date();
var currentMonth = today.getMonth();
var currentYear = today.getFullYear();
var selectYear = document.getElementById("year");
var selectMonth = document.getElementById("month");

var months = [];
var selectedDates = [];
var selectedDatesMonth = [];
var selectedDatesMonthReal = [];
var years = [];

// parameters to be set for the datepicker to run accordingly
var minYear = 2011;
var maxYear = 2020;
var startMonth = 0;
var endMonth = 11;
var highlightToday = true;
var dateSeparator = ',';






// constants that would be used in the script
const dictionaryMonth =
    [
        ["Jan", 0],
        ["Feb", 1],
        ["Mar", 2],
        ["Apr", 3],
        ["May", 4],
        ["Jun", 5],
        ["Jul", 6],
        ["Aug", 7],
        ["Sep", 8],
        ["Oct", 9],
        ["Nov", 10],
        ["Dec", 11]
    ];

//this class will add a background to the selected date
$highlightClass = 'bg-primary';

$(window).on('load', function (e) {
    today = new Date("2012-08-01");
    currentMonth = today.getMonth();
    currentYear = today.getFullYear();
    selectYear = document.getElementById("year");
    selectMonth = document.getElementById("month");
    loadControlDate(currentMonth, currentYear);
    loadControlMonth(currentMonth, currentYear);

});

function next() {
    currentYear = currentMonth === 11 ? currentYear + 1 : currentYear;
    currentMonth = currentMonth + 1 % 12;
    loadControl(currentMonth, currentYear);
}

function previous() {
    currentYear = currentMonth === 0 ? currentYear - 1 : currentYear;
    currentMonth = currentMonth === 0 ? 11 : currentMonth - 1;
    loadControl(currentMonth, currentYear);
}

function change() {
    currentYear = parseInt(selectYear.value);
    currentMonth = parseInt(selectMonth.value);
    loadControlDate(currentMonth, currentYear);
}


function loadControlDate(month, year) {

    
    //addMonths(month);
    //addYears(year);

    let firstDay = (new Date(year, month)).getDay();

     // body of the calendar
    var tbl = document.querySelector("#calendarBodyDate");
    // clearing all previous cells
    tbl.innerHTML = "";


    //var monthAndYear = document.getElementById("monthAndYear");
    // filing data about month and in the page via DOM.
    //monthAndYear.innerHTML = months[month] + " " + year;


    ////selectYear.value = year;
    //selectMonth.value = month;
    
    // creating the date cells here
    let date = 1;

    // add the selected dates here to preselect
    //selectedDates.push((month + 1).toString() + '/' + date.toString() + '/' + year.toString());


    
    // there will be maximum 6 rows for any month
    for (let rowIterator = 0; rowIterator < 9; rowIterator++) {

        // creates a new table row and adds it to the table body
        let row = document.createElement("tr");


        //creating individual cells, filing them up with data.
        for (let cellIterated = 0; cellIterated < 4 && date <= daysInMonth(month, year); cellIterated++) {

            // create a table data cell
            cell = document.createElement("td");
            let textNode = "";
            

            // check if this is the valid date for the month
            if (rowIterator !== 0 || cellIterated >= firstDay) {
                cell.id = (month + 1).toString() + '/' + date.toString() + '/' + year.toString();
                cell.class = "clickable";
                textNode = date;

                // this means that highlightToday is set to true and the date being iterated it todays date,
                // in such a scenario we will give it a background color
                

                // set the previous dates to be selected
                // if the selectedDates array has the dates, it means they were selected earlier. 
                // add the background to it.
                if (selectedDates.indexOf((month + 1).toString() + '/' + date.toString() + '/' + year.toString()) >= 0) {
                    cell.classList.add(highlightClass);
                }

                date++;
            }

            cellText = document.createTextNode(textNode);
            cell.appendChild(cellText);
            row.appendChild(cell);
        }

        tbl.appendChild(row); // appending each row into calendar body.
    }

    // this adds the button panel at the bottom of the calendar
    addButtonPanelDate(tbl);

    

    // function when the date cells are clicked
    $("#calendarBodyDate tr td").click(function (e) {
        var id = $(this).attr('id');


        // check the if cell clicked has a date
        // those with an id, have the date
        if (typeof id !== typeof undefined) {
            var classes = $(this).attr('class');
            if (typeof classes === typeof undefined || !classes.includes($highlightClass)) {
                var selectedDate = new Date(id);
                selectedDates.push(( selectedDate.getDate().toString() ));
               
                //alert("www"+selectedDates.index());
                //selectedDates.push((selectedDate.getMonth() + 1).toString() + '/' + selectedDate.getDate().toString() + '/' + selectedDate.getFullYear());
            }
            else {
                var today_id = new Date(id);
                var index = selectedDates.indexOf(today_id.getDate().toString());
                if (index > -1) {
                    selectedDates.splice(index, 1);
                }
                 
            }

            $(this).toggleClass($highlightClass);
        }

        // sort the selected dates array based on the latest date first
        var sortedArray = selectedDates.sort((a, b) => {
            return new Date(a) - new Date(b);
        });

        // update the selectedValuesDate text input
        document.getElementById('selectedValuesDate').value = datesToString(sortedArray);
        //cekInputFilter(document.getElementById("year").value , document.getElementById('selectedValuesMonth').value,document.getElementById('selectedValuesDate').value,$("#select-branch").val());
        console.log("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww");
    });


    
    
}


// check how many days in a month code from https://dzone.com/articles/determining-number-days-month
function daysInMonth(iMonth, iYear) {
    return 32 - new Date(iYear, iMonth, 32).getDate();
}

// adds the months to the dropdown
function addMonths(selectedMonth) {
    var select = document.getElementById("month");

    if (months.length > 0) {
        return;
    }

    for (var month = startMonth; month <= endMonth; month++) {
        var monthInstance = dictionaryMonth[month];
        months.push(monthInstance[0]);
        select.options[select.options.length] = new Option(monthInstance[0], monthInstance[1], parseInt(monthInstance[1]) === parseInt(selectedMonth));
    }
}

// adds the years to the selection dropdown
// by default it is from 1990 to 2030
function addYears(selectedYear) {

    if (years.length > 0) {
        return;
    }

    var select = document.getElementById("year");

    for (var year = minYear; year <= maxYear; year++) {
        years.push(year);
        select.options[select.options.length] = new Option(year, year, parseInt(year) === parseInt(selectedYear));
    }
}

resetCalendar = function () {
    // reset all the selected dates
    selectedDates = [];
    $('#calendarBodyDate tr').each(function () {
        $(this).find('td').each(function () {
            // $(this) will be the current cell
            $(this).removeClass($highlightClass);
            document.getElementById('selectedValuesDate').value ='';
           //alert("testing multdidate js");
        });
    });

};


allCalendar = function() {
    resetCalendar();
    $('#calendarBodyDate tr').each(function () {
        $(this).find('td').each(function () {
            // $(this) will be the current cell
            $(this).toggleClass($highlightClass);
            selectedDates=['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
            document.getElementById('selectedValuesDate').value =selectedDates;
            //selectedDatesMonthReal=['1','2','3','4','5','6','7','8','9','10','11','12'];
            //console.log(document.getElementById("selectedValuesMonth").value);


        });
    });
    //cekInputFilter(document.getElementById("year").value , document.getElementById('selectedValuesMonth').value,document.getElementById('selectedValuesDate').value,$("#select-branch").val());
        //console.log(DO);
};

function datesToString(dates) {
    return dates.join(dateSeparator);
}

// function endSelection() {
//     $('#parentDate').hide();
// }


// to add the button panel at the bottom of the calendar
function addButtonPanelDate(tbl) {
    // after we have looped for all the days and the calendar is complete,
    // we will add a panel that will show the buttons, reset and done
    // let row = document.createElement("tr");
    // row.className = 'buttonPanel';
    // cell = document.createElement("td");
    // cell.colSpan = 7;
    // var parentDiv = document.createElement("div");
    // parentDiv.classList.add('row');
    // parentDiv.classList.add('buttonPanel-row');
    // parentDiv.classList.add('border');


    // let all_button = document.createElement("button");
    // all_button.className = 'btn btn-primary';
    // all_button.type="button";
    // all_button.value = 'All';
    // all_button.onclick=function(){allCalendar();};
    // all_button.innerHTML = '<i  data-toggle="tooltip" title="select all"  data-placement="top">All</i>';
    //var allbuttonText = document.createTextNode("All");
    //all_button.appendChild(allbuttonText);
    //var induk=document.getElementById("induk1");
    //induk.appendChild(all_button);
    

    // var div = document.createElement("div");
    // div.className = 'col-sm';
    // var resetButton = document.createElement("button");
    // resetButton.className = 'btn btn-warning';
    // //resetButton.value = 'Reset'
    // resetButton.type="button";
    // resetButton.onclick = function () { resetCalendar(); };
    // resetButton.innerHTML = '<i class="glyphicon glyphicon-refresh" data-toggle="tooltip" title="Clear"  data-placement="top"></i>';
    //var resetButtonText = document.createTextNode("Reset");
    //resetButton.appendChild(resetButtonText);

    //div.appendChild(resetButton);
    //parentDiv.appendChild(div);
   

    // var div2 = document.createElement("div");
    // div2.className = 'col-sm';
    // var doneButton = document.createElement("button");
    // doneButton.className = 'btn btn-success';
    // //doneButton.value = 'Done';
    // doneButton.type="button";
    // doneButton.onclick = function () { endSelection(); };
    // doneButton.innerHTML = '<i class="fa fa-close" data-toggle="tooltip" title="Close"  data-placement="top"></i>';
    //var doneButtonText = document.createTextNode("Done");
    //doneButton.appendChild(doneButtonText);

    //div2.appendChild(doneButton);
    //parentDiv.appendChild(div2);

    // cell.appendChild(doneButton);
    // cell.appendChild(resetButton);
    // cell.appendChild(all_button);
    // row.appendChild(cell);
    // // appending each row into calendar body.
    // tbl.appendChild(row);
}




function loadControlMonth(month, year) {
    
   
    let firstDay = (new Date(year)).getMonth();

     // body of the calendar
    var tbl = document.querySelector("#calendarBodyMonth");
    // clearing all previous cells
    tbl.innerHTML = "";


    //var monthAndYear = document.getElementById("monthAndYear");
    // filing data about month and in the page via DOM.
    //monthAndYear.innerHTML = months[month] + " " + year;


    ////selectYear.value = year;
    //selectMonth.value = month;
    
    // creating the date cells here
    let date = 1;

    // add the selected dates here to preselect
    //selectedDates.push((month + 1).toString() + '/' + date.toString() + '/' + year.toString());

    // there will be maximum 6 rows for any month
    for (let rowIterator = 0; rowIterator < 4; rowIterator++) {

        // creates a new table row and adds it to the table body
        let row = document.createElement("tr");


        //creating individual cells, filing them up with data.
        for (let cellIterated = 0; cellIterated < 3; cellIterated++) {

            // create a table data cell
            cell = document.createElement("td");
           

            let textNode = "";

            // check if this is the valid date for the month
                
                
                
                 cell.class = "clickable";
                if(date==1){
                        textNode = 'Jan';
                        cell.id = 1;
                }else if(date==2){
                        textNode = 'Feb';
                        cell.id = 2;
                }else if(date==3){
                        textNode = 'Mar';
                        cell.id = 3;
                }else if(date==4){
                        textNode = 'Apr';
                        cell.id = 4;
                }else if(date==5){
                        textNode = 'May';
                        cell.id = 5;
                }else if(date==6){
                        textNode = 'Jun';
                        cell.id = 6;
                }else if(date==7){
                        textNode = 'Jul';
                        cell.id = 7;
                }else if(date==8){
                        textNode = 'Aug';
                        cell.id = 8;
                }else if(date==9){
                        textNode = 'Sep';
                        cell.id = 9;
                }else if(date==10){
                        textNode = 'Oct';
                        cell.id = 10;
                }else if(date==11){
                        textNode = 'Nov';
                        cell.id = 11;
                }else if(date==12){
                        textNode = 'Dec';
                        cell.id = 12;
                }
                date=date+1;
                
                
           

            cellText = document.createTextNode(textNode);
            cell.appendChild(cellText);
            row.appendChild(cell);
        }

        tbl.appendChild(row); // appending each row into calendar body.
    }

    // this adds the button panel at the bottom of the calendar
    addButtonPanelMonth(tbl);

    // function when the date cells are clicked
        $("#calendarBodyMonth tr td").click(function (e) {
        var id = $(this).attr('id');//+"-"+document.getElementById("year").value;
        var realid=$(this).attr('id');
        //alert("lier ah");
      

        // check the if cell clicked has a date
        // those with an id, have the date
        if (typeof id !== typeof undefined) {
            var classes = $(this).attr('class');
            if (typeof classes === typeof undefined || !classes.includes("bg-primary")) {
                var selectedDateMonth = id;
                var selectedDateMonthReal=realid;
                selectedDatesMonth.push(( selectedDateMonth.toString() ));
                selectedDatesMonthReal.push((selectedDateMonthReal.toString() ));
                //alert("www"+selectedDates.index());
                //selectedDates.push((selectedDate.getMonth() + 1).toString() + '/' + selectedDate.getDate().toString() + '/' + selectedDate.getFullYear());
            }
            else {
                var today_id = id;
                var today_id_real=realid;
                var index = selectedDatesMonth.indexOf(today_id.toString());
                if (index > -1) {
                    selectedDatesMonth.splice(index, 1);
                }

                var index1 = selectedDatesMonthReal.indexOf(today_id_real.toString());
                if (index1 > -1) {
                    selectedDatesMonthReal.splice(index1, 1);
                }

                 
            }

            $(this).toggleClass("bg-primary");
        }

        // sort the selected dates array based on the latest date first
        var sortedArrayMonth = selectedDatesMonth.sort((a,b) => {
            return new Date(a) - new Date(b);
        });

        // update the selectedValuesMonth text input
        document.getElementById('selectedValuesMonth').value = datesToString(sortedArrayMonth);
        //console.log("onclick");
        console.log(document.getElementById('selectedValuesMonth').value);

        console.log("hasil murni bulan "+selectedDatesMonthReal);
        // bulan2=[];
        // hitungbulan(1, document.getElementById("year").value , selectedDatesMonthReal, bulan2);
        // hitungbulan(2, document.getElementById("year").value , selectedDatesMonthReal, bulan2);
        // var convertsplitarray=document.getElementById('selectedValuesMonth').value.split(",");
        // for(i=0;i<convertsplitarray.length;i++){
        //      bulan2.push(convertsplitarray[i]);
        // }
        // for(i=0;i<selectedDatesMonthReal.length;i++){
        //      bulan2.push((selectedDatesMonthReal[i])+"-"+(document.getElementById("year").value-1));
        // }
        // bulan2=getUnique(bulan2);
        // var myJsonString = JSON.stringify(bulan2);
        // console.log("sisisisisisis "+myJsonString);
        

        //cekInputFilter(document.getElementById("year").value , document.getElementById('selectedValuesMonth').value,document.getElementById('selectedValuesDate').value,$("#select-branch").val());
        //console.log("klik month"+DO);
        //var DOMulti=DO;

        
    });    

}





resetCalendarMonth = function () {
    // reset all the selected dates
    selectedDatesMonth = [];
    selectedDatesMonthReal=[];
    $('#calendarBodyMonth tr').each(function () {
        $(this).find('td').each(function () {
            // $(this) will be the current cell
            $(this).removeClass($highlightClass);
            document.getElementById('selectedValuesMonth').value ='';
        });
    });
};


allCalendarMonth = function() {

    resetCalendarMonth();
    $('#calendarBodyMonth tr').each(function () {
        $(this).find('td').each(function () {
            // $(this) will be the current cell
            $(this).toggleClass($highlightClass);
            selectedDatesMonth=['1','2','3','4','5','6','7','8','9','10','11','12'];
            document.getElementById('selectedValuesMonth').value =selectedDatesMonth;
            selectedDatesMonthReal=['1','2','3','4','5','6','7','8','9','10','11','12'];
            console.log(document.getElementById("selectedValuesMonth").value);


        });
    });



        // cekInputFilter(document.getElementById("year").value , document.getElementById('selectedValuesMonth').value,document.getElementById('selectedValuesDate').value,$("#select-branch").val());
        //console.log(DO);
};

function datesToString(dates) {
    return dates.join(dateSeparator);
}

// function endSelectionMonth() {
//     alert("clikc me");
// }


// to add the button panel at the bottom of the calendar
function addButtonPanelMonth(tbl) {
    // after we have looped for all the days and the calendar is complete,
    // we will add a panel that will show the buttons, reset and done
    // let row = document.createElement("tr");
    // row.className = 'buttonPanel';
    // cell = document.createElement("td");
    // cell.colSpan = 4;
    //var parentMonthDiv = document.createElement("div");
    ////parentMonthDiv.classList.add('row');
    //parentMonthDiv.classList.add('buttonPanel-row');
    //parentMonthDiv.classList.add('text-right');
    //parentMonthDiv.classList.add('px-5');
    


    // let all_button = document.createElement("button");
    // all_button.className = 'btn btn-primary';
    // //all_button.value = 'All';
    // all_button.type = "button";
    // all_button.onclick=function(){allCalendarMonth();};
    //var allbuttonText = document.createTextNode("All");
    //all_button.innerHTML = '<i  data-toggle="tooltip" title="select all"  data-placement="top">All</i>';
    //all_button.appendChild(allbuttonText);
    //var induk=document.getElementById("indukMonth");
    //induk.appendChild(all_button);
    

    //var div = document.createElement("div");
    // div.className = 'col-sm';
    // var resetButton = document.createElement("button");
    // resetButton.className = 'btn btn-warning';
    //resetButton.value = 'Reset';
    // resetButton.type = 'button';
    // resetButton.onclick = function () { resetCalendarMonth(); };
    //var resetButtonText = document.createTextNode("Reset");
    //resetButton.innerHTML = '<i class="glyphicon glyphicon-refresh" data-toggle="tooltip" title="Clear"  data-placement="top"></i>';
    //resetButton.appendChild(resetButtonText);


    //div.appendChild(resetButton);
    //parentMonthDiv.appendChild(resetButton);

   
   

    // var div2 = document.createElement("div");
    // div2.className = 'col-sm';
    // var doneButton = document.createElement("button");
    // doneButton.className = 'btn btn-success';
    // //doneButton.value = 'Done';
    //  doneButton.type = 'button';
    //  doneButton.innerHTML = '<i class="fa fa-close" data-toggle="tooltip" title="Close"  data-placement="top"></i>';
    // doneButton.onclick = function () { endSelectionMonth(); };
    //var doneButtonText = document.createTextNode("Done");
    //doneButton.appendChild(doneButtonText);

    //div2.appendChild(doneButton);
    //parentMonthDiv.appendChild(doneButton);

    
    
    
    
    // cell.appendChild(doneButton);
    // cell.appendChild(resetButton);
    // cell.appendChild(all_button);
    // row.appendChild(cell);
    // // appending each row into calendar body.
    // tbl.appendChild(row);
}


$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
