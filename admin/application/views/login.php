<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Duta Cendana Adimandiri </title>
  
  <link rel="icon" href="<?php echo base_url() ?>favicon.ico" type="image/jpg">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->

    <!-- Font Awesome -->
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/ionicons/css/ionicons.min.css">
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE1.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/snackjs/dist/js-snackbar.css">
    <script src='<?php echo base_url(); ?>/plugins/snackjs/src/js-snackbar.js'></script>

    <script src="<?php echo base_url(); ?>jquery/jquery.min.js"></script>

    

</head>
<body  class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header">
        <div class="row" style="font-size: x-large;">
            <div class="col-12" style="">
                <a href="login"><img src="" style="width:100%;"></a>
            </div>
        </div>
    </div>
    <div class="card-body">
      <form action="login/do_login" method="post">

       <?php include('alert.php')?>
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Username" name="nik" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-id-card"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" id="password" name="password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <div class="custom-control custom-switch">
            <input onclick="myFunction()" type="checkbox" class="custom-control-input" id="passwi">
            <label class="custom-control-label" for="passwi">Perlihatkan Password</label>
          </div>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <!--<p class="mb-0 text-center">Klik di <a href="signup" class="text-center">SINI</a> untuk membuat akun agar dapat masuk ke langkah pendaftaran kepesertaan.</p>-->
     <!-- <div class="row">
        <div class="col-6" style="align-self: center;">
            Anda Sensei? 
        </div>
        <div class="col-6">
            <a class="btn btn-block btn-primary btn-xs" style="float:right; margin:10px;" href="login/senseilogin">Login</a>
        </div>
    </div>-->
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->

<!-- /.login-box -->

<!-- jQuery -->

<script>
    function myFunction() {
  // Get the checkbox
  var checkBox = document.getElementById("passwi");
  // Get the output text
  var pass = document.getElementById("password");

  // If the checkbox is checked, display the output text
  if (checkBox.checked == true){
    pass.type = "text";
  } else {
    pass.type = "password";
  }
}
</script>
</body>
</html>
