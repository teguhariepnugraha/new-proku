
  <style>
  .text-right{
    text-align:center;
  }
    
  </style>

	
<script type="text/javascript">
 var idpelajaran;
 var idpengajar;
 var idkelas;
 var arrakun = [];

					$(document).ready(function() {
						
							 var hgt;
							 var frm;
							 var wnd;
							
							 $( window ).on( "load", function() { 
								setTimeout(function(){
											
											    idpengajar = "<?php echo $this->session->userdata('userid'); ?>";
												let customerMultiColumn1 = $('#idpengajar').data("kendoMultiColumnComboBox");
												customerMultiColumn1.select(function(dataItem) {
													return dataItem.idpengajar === idpengajar;
												});
												
										},500);
							});
							
							 var taskb = document.documentElement.clientHeight;
							 hgt = taskb -174
							 
							 autonumber('./urlautonumber?link=<?php 
							echo encrypt_url("SELECT CONCAT (
DATE_FORMAT(CURDATE(),'%y'), DATE_FORMAT(CURDATE(),'%m'),  LPAD(IFNULL(MAX(RIGHT(kdabsensiswa,3)), 0)+1 ,3, '00')) AS anmbr FROM 
tabsensiswa WHERE SUBSTRING(kdabsensiswa, 1, 2) = DATE_FORMAT(CURDATE(),'%y') AND SUBSTRING(kdabsensiswa, -5, 2) = DATE_FORMAT(CURDATE(),'%m')") ;?>','kdabsensiswa');
							 
							 $('#groupinput').height(hgt-76);
							 
							 $('#simpan').on('click', function(){
										save();
										setTimeout(function(){
											cekidabsensiswa();
										},1000);
										/*$("#simpan").prop("disabled", true);*/
										
							  });
							 $('#add').on('click', function(){
									tambah();
								});
							
							creategrid();
	
							var d = moment();
							$('#tgl').val(d.format('YYYY-MM-DD'));
							
							function cekidabsensiswa() {
											   var kdabsensiswa = $('#kdabsensiswa').val();
											   
											  
												$.ajax({
													  url: '<?php echo base_url(); ?>cabsensiswa/cekidabsensiswa',
													  type: "get",
													  data:{kdabsensiswa:kdabsensiswa},
													  dataType: "json",
													  success: function(data)
														  {  
															if (data[0].jml > 0)
															{
																 
																simpandtl(data[0].idabsensiswa);
															}														 
														  }
													});
									
								}
								
								
							function simpandtl(idabsensiswa) {
								var arr = [];
								var grid = $("#grid").data("kendoGrid");
								var data = grid.dataSource.data();
								var grid = $("#grid").data("kendoGrid");
								var ds = grid.dataSource.view();
								
								for (var i = 0; i < ds.length; i++) {
										   
										    var row = grid.table.find("tr[data-uid='" + ds[i].uid + "']");
											var checkbox = $(row).find(".checkbox");
											var chk ;
											if (checkbox.is(":checked")) 
											{chk = 'checked';}
											else
											{chk='unchecked';}
											var idpeserta=ds[i].idpeserta;
											
											 if (idabsensiswa != '' && idpeserta != ''  )
											  {
												
													  arr.push({
														    idabsensiswa:idabsensiswa,
														  	idpeserta:idpeserta,
															absen:chk,
														  });
														  
														  
												}
												
								 };
										
									var arr= JSON.stringify(arr);	
													$.ajax({
														  url: '<?php echo base_url(); ?>cabsensiswa/simpandtl',
														  type: 'get',
														  data:{arr:arr},
														  async: false,
														  dataType: "json",
														  success: function(data)
															  {
																												 
															  }
														  
															});
									}
										
									
									$("#idpengajar").kendoMultiColumnComboBox({
										dataTextField: "nama",
										dataValueField: "idpengajar",
										height: 400,
										columns: [
											
											{ field: "nik", title: "NIK", width: 100 },
											{ field: "nama", title: "Nama" },
											
									  		
										],
										footerTemplate: 'Total #: instance.dataSource.total() # items found',
										filter: "contains",
										filterFields: ["nik", "nama"],
										dataSource: {
											
											transport: {
												read: {
														contentType: "application/json; charset=utf-8",
														dataType: "json",
														type: 'post',
														url: '<?php echo base_url(); ?>cabsensiswa/datapengajar',
													}
												
											}
										},
										change: function (e) {
													var dataItem = e.sender.dataItem();
													idpengajar =  dataItem.idpengajar;

												}
									});
										
										
										$("#idkelas").kendoMultiColumnComboBox({
										dataTextField: "kelas",
										dataValueField: "idkelas",
										height: 400,
										columns: [
											
											{ field: "kelas", title: "Kelas" },
											
											
									  		
										],
										footerTemplate: 'Total #: instance.dataSource.total() # items found',
										filter: "contains",
										filterFields: ["kelas"],
										dataSource: {
											
											transport: {
												read: {
														contentType: "application/json; charset=utf-8",
														dataType: "json",
														type: 'post',
														url: '<?php echo base_url(); ?>cabsensiswa/datakelas',
													}
												
											}
										},
										
										change: function (e) {
													var dataItem = e.sender.dataItem();
													idkelas =  dataItem.idkelas;
														
														$("#idpelajaran").kendoMultiColumnComboBox({
														dataTextField: "nmpelajaran",
														dataValueField: "idpelajaran",
														height: 400,
														columns: [
															{ field: "kdpelajaran", title: "Kode",width:65 },
															{ field: "nmpelajaran", title: "Pelajaran" },
															{ field: "kelas", title: "Kelas" },
														],
														footerTemplate: 'Total #: instance.dataSource.total() # items found',
														filter: "contains",
														filterFields: ["kdpelajaran","nmpelajaran"],
														dataSource: {
															
															transport: {
																read: {
																		contentType: "application/json; charset=utf-8",
																		dataType: "json",
																		type: 'get',
																		data:{idkelas:idkelas},
																		url: '<?php echo base_url(); ?>cabsensiswa/datapelajaran',
																	}
															}
														},
														change: function (e) {
																	var dataItem = e.sender.dataItem();
																	idpelajaran =  dataItem.idpelajaran;
																	
																}
													});
													creategrid();
												}
									});
									
									
												
						 });
						  
						
								
					
						
						
						function creategrid() {
							  $("#grid").kendoGrid({
									columns: [
									{ field: "chk",title:"ID",width:40,editable: "false" ,
									  template: "<input name='chk' class='checkbox' type='checkbox' data-bind='checked: chk' #= chk ? checked='checked' : '' #/>" },
									  { field: "idpeserta",title:"idpeserta",hidden:true},
									  { field: "nik",title:"NIK",width:100,editable: true,},
									  { field: "nama", title:"Nama",width:200,editable: true,},
									  
									],
									dataSource: {
													transport: {
														read: {
															dataType: "json",
															type: 'get',
															data:{idkelas:idkelas},
															url: '<?php echo base_url(); ?>cabsensiswa/datapeserta',
														}
													}
												},
									dataBound: function(e) {
   										  	$(".checkbox").bind("change", function(e) {
												var grid = $("#grid").data("kendoGrid");
												var row = $(e.target).closest("tr");
												row.toggleClass("k-state-selected");
												var data = grid.dataItem(row);
												/*alert(data.idakundtl);*/
    											});
  										  },
									selectable: "cell",
									editable: true,
       
								  })
								 var grid = $("#grid").data("kendoGrid");
									
	
						}
						
						  </script>
                           

                <!-- form start -->
                  <div id="content" class="">
            <!-- content starts -->
     <div>

          <ul class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>admin">Home</a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>cabsensiswa/tampil">Absen Siswa</a>
            </li>
            <li>
                <a href="#">Tambah</a>
            </li>
        </ul>
    </div>

    <div class=" row"  style="margin-top:-18px">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-user"></i> Tambah Absen Siswa</h2>

        <div class="box-icon">
            
            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a>
            <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
        </div>
    </div>
    <div class="box-content">
                <!-- form start -->
 <div  id="groupinput" class="form-group" style="overflow:auto; margin:0 0 10px 0;"> 
 
 <!-- batas form spkl-->
  				<div class="box-header well" data-original-title="" style="margin-right:4px">
                        <h2><i class="glyphicon glyphicon-user"></i> Absen Siswa </h2>
                
                        <div class="box-icon">
                            
                            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a>
                            <!--<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                        </div>
                    </div>
                    <div class="box-content">
                   <form id="form2" name="form2" method="" action=""  >
     
    <!-- batas form Ijin-->
                    
                    
                   
                       
                         
   <!-- batas form-->
                         
    					
                      <label for="exampleInputEmail1">Tanggal  </label>
                      <input type="text" class="form-control" name="tgl"  id="tgl" placeholder="Tanggal "  title="date"/>
                      <label for="exampleInputEmail1">No. Reg  </label>
                      <input type="text" class="form-control" name="kdabsensiswa" id="kdabsensiswa"   placeholder="No. Reg"  style="width:100%" readonly="readonly"  />
                
                      <label for="exampleInputEmail1">Pengajar</label>
                     <input type="text" class="form-control" name="idpengajar" id="idpengajar"   placeholder="Pengajar" style="width:100%" readonly="readonly"  />
                     <label for="exampleInputEmail1">Kelas</label>
                     <input type="text" class="form-control" name="idkelas" id="idkelas"   placeholder="Kelas" style="width:100%"  />
                     
     
                      
               
                 
                 
                  </div>
                  
                     <!-- batas form List Karyawan-->
                  <div class="box-header well" data-original-title="" style="margin-right:4px">
                        <h2><i class="glyphicon glyphicon-user"></i> Detail Absen Siswa </h2>
                
                        <div class="box-icon" >
                            
                            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a>
                            <!--<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                        </div>
                    </div>
                    <div class="box-content">
                    	
				
           
		

                    
                    <div id="grid"></div>
                    </div>
                    
                      <!-- batas form List Karyawan-->
                  </div>

                  
                  <input type="hidden" name="id" >
                  <a href="<?php echo base_url(); ?>cabsensiswa/tampil" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Batal</a>
                  <button type="button" name="simpan" id="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                 
<a href="<?php echo base_url(); ?>cabsensiswa/tambah_absensiswa" class="btn btn-danger"><i class="fa fa-retweet"></i> Add</a>
                </form>

               
 </div>

            </div>
        </div>
    </div>
    </div>
 