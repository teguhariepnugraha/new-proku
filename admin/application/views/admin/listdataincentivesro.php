
  <script type="text/javascript" class="init">

    
    $(document).ready(function() {

  
      $('#grid').DataTable( {
      
        scrollY:        '50vh',
        "bFilter": false,
        "bLengthChange": false,
        "aLengthMenu": [
                  [25, 50, 100, 200, -1],
                  [25, 50, 100, 200, "All"]
                ],
        iDisplayLength: -1,
        scrollX: true,
        scrollCollapse: true
      } );
    } );
    
  </script>
 <div id="content" class="">
            <!-- content starts -->
     <div>
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>admin">Home</a>
            </li>
            <li>
                <a href="#">List Pengajuan Insentive SRO</a>
            </li>
        </ul>
    </div>

  <div class=" row"  style="margin-top:-18px">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-user"></i> Pengajuan Insentive SRO</h2>

       
    </div>
    <div class="box-content">
    

                
                  <div class="box-tools" style="float:right">
                     <form id="form2" name="form2" method="POST" action="<?php echo base_url();?>clistdataincentivesro/tampil"  >
      
                    <div class="input-group" style="width: 150px; margin-top:0px; padding-right:-10px">
                      <span class="input-group" style="width: 150px; margin-top:0px; padding-right:-10px">
                      


                      <input type="text" name="table_search"  class="form-control input-sm pull-right" placeholder="Search"  />
                      
                       
                      </span>
                      <div class="input-group-btn" >
                       <button id="btnsrch"  class="btn btn-sm btn-default"><i class="fa fa-search"></i> </button>
                        
                      </div>
                      
                    </div>
                    
                  </div>
                  
              
        <!--<div style="width:100px; margin-top:-20px" >
                  <h3 >
                    <a href="<?php //echo base_url(); ?>cjabatan/tambah_jabatan" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-edit"></i> Tambah</a>
                  </h3>
                   </div>--><!-- /.box-header -->
</form>
                
<table id="grid" class="display nowrap" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>No</th>
        <th style="display:none;">id_pengajuan</th>
        <th style="display:none;">id_karyawan</th>
        <th style="display:none;">relateduser</th>
        <th>cabang</th>
        <th>bulan</th>
        <th>tahun</th>
        <th>Nama</th>
       

        <th><div align="center">Action</div></th>
    </tr>
    </thead>
    <tbody>
    <?php  
       $no = 1;
        foreach ($data as $lihat):
     ?>
    <tr>
      <td><?php echo $no++ ?></td>
      <td style="display:none;"><?php echo $lihat->idpengajuan ?></td>
      <td style="display:none;"><?php echo $lihat->idkaryawan ?></td>
      <td style="display:none;"><?php echo $lihat->relateduser ?></td>
      <td><?php echo $lihat->cabang ?></td>
      <td><?php echo $lihat->bulan ?></td>
      <td><?php echo $lihat->tahun ?></td>
      <td><?php echo $lihat->fullname ?></td>


        <td class="center">

            <div align="right"><a class="btn btn-info" href="<?php echo base_url(); ?>report/cdetail_incentive_sro_report/cetakpdf<?php echo "/".$lihat->bulan."/".$lihat->tahun."/".$lihat->cabang."/".$lihat->idpengajuan?>">
                     Data
                  </a>

              <?php 
                if($lihat->destination=="finish"){
                   echo " <a class='btn btn-danger' href=#>
                     Finish
                             </a> ";
                 // echo "<a class='btn btn-info' href="base_url()."report/cdetail_incentive_sro_report/cetakpdf/".$lihat->bulan."/".$lihat->tahun."/".$lihat->cabang."/".$lihat->idpengajuan."> Finish </a>";
                }else{
                   echo " <a class='btn btn-danger' href=".base_url()."clistdataincentivesro/pengajuanSRO/". $lihat->idpengajuan." >
                     Aprove
                             </a> ";
                }
              ?>
               <!-- <a class="btn btn-danger" href="<?php //echo base_url(); ?>clistdataincentivesro/pengajuanSRO/<?php // echo $lihat->idpengajuan?>" onclick="" >
                     Aprove
                             </a> -->               </div></td>
                
    </tr>
     <?php endforeach ?>
    </tbody>
    </table>
                    

                    


               
    <!--/span-->

<!--/row-->
<!--/row-->
<!-- content ends -->
        </div>



            </div>
        </div>
    </div>
    </div>