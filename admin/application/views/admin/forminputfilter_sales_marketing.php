


<form class="form-horizontal border">



                            <div class="form-group row">
                             <label for="year" class="col-sm-2 col-form-label">Year</label>
                              <div class="col-sm-10">
                                <select id="year" class="form-control"  data-placeholder="Select Year" style="width: 100%;">

                                        <script>
                                             var induk=document.getElementById("year");
                                            var date=new Date();
                                                    var year=date.getFullYear();

                                            for(i=year;i>=2013; i--){
                                              
                                              let option=document.createElement("option");
                                              option.value=i;
                                              option.text=i;
                                              induk.appendChild(option);
                                                                                                
                                            
                                            }

                                          </script>
                                   </select>

                              </div>



                            </div>
                            
                            <div class="form-group row">
                                
                                    <label for="month" class="col-sm-2 col-form-label">Month</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="selectedValuesMonth" class="date-values" style="width:100%;" readonly/>
                                    </div>
                                    <div class="col-sm-10 ">
                                    <div id="parentMonth" class="container border py-1" style="display:none; ">
                                        <div id="indukMonth" class="row header-row text-left">
                                            

                                          
                                            
                                        </div>
                                        <table id="calendar" border="1px" >
                                            
                                            <tbody id="calendarBodyMonth" ></tbody>
                                        </table>
                                    </div>
                                  </div>
                            </div>


                            <div class="form-group row">
                                
                                    <label for="month" class="col-sm-2 col-form-label">Date</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="selectedValuesDate" class="date-values" style="width:100%;" readonly/>
                                    </div>
                                    <div class="col-sm-10">
                                    <div id="parentDate" class="container py-1 border" style="display:none; ">
                                        <div id="induk1" class="row header-row text-left">
                                            

                                          
                                            
                                        </div>
                                        <table id="calendar"  border=1px>
                                            
                                            <tbody id="calendarBodyDate"></tbody>
                                        </table>
                                    </div>
                                  </div>
                            </div>






                            <div class="form-group row">
                                <label for="example-optionClass" class="col-sm-2 col-form-label">Branch</label>
                                <div class="col-sm-10">
                                 <select id="select-branch" class="form-control select2" multiple="multiple" data-placeholder="Select Branch"
                                        style="width: 100%;">
                                  <option>Cianjur</option>
                                  <option>Ciawi</option>
                                  <option>Bekasi</option>
                                  <option>Depok</option>
                                </select>

                                <div class="form-inline">
                                      
                                          <input type="checkbox" class="form-check-input checkbox" id="checkbox-branch" style="width:20px; height:20px ">
                                     
                                     
                                      <label class="col-form-label py-1 px-0.5" style=" height:20px">Select All</label>
                                   
                                  </div>

                              </div>
                            </div>


                            
                            <div class="form-group row">
                                <label for="month1" class="col-sm-2 col-form-label">Sales Head</label>
                                <div class="col-sm-10">
                                <select id="select-seles-head" class="form-control select2" multiple="multiple" data-placeholder="seles head"
                                        style="width: 100%;">
                                  <option value="jan">Jan</option>
                                  <option>Mar</option>
                                  <option>Apr</option>
                                  <option>Mei</option>
                                  <option>Jun</option>
                                  <option>Jul</option>
                                  <option>Aug</option>
                                  <option>Sep</option>
                                  <option>Oct</option>
                                  <option>Nov</option>
                                  <option>Des</option>
                                </select>
                                    <div class="form-inline">
                                      
                                          <input type="checkbox" class="form-check-input checkbox" id="checkbox-sales-head" style="width:20px; height:20px ">
                                     
                                     
                                      <label class="col-form-label py-1 px-0.5" style=" height:20px">Select All</label>
                                    
                                  </div>
                              </div>
                            </div>
                            <div class="form-group row">
                                <label for="month1" class="col-sm-2 col-form-label">Salesman</label>
                                <div class="col-sm-10">
                                <select id="select-selesman" class="form-control select2" multiple="multiple" data-placeholder="Selesman"
                                        style="width: 100%;">
                                  <option>Indri Halimah</option>
                                  <option>Usman</option>
                                  <option>Nandhira</option>
                                  <option>Ujang</option>
                                  
                                </select>

                                  <div class="form-inline">
                                      
                                          <input type="checkbox" class="form-check-input checkbox" id="checkbox-salesman" style="width:20px; height:20px ">
                                     
                                      <label class="col-form-label py-1 px-0.5" style=" height:20px">Select All</label>
                                  </div>

                              </div>
                            </div>
                            <div class="form-group row">
                                <label for="month1" class="col-sm-2 col-form-label">Tipe Kendaraan</label>
                                <div class="col-sm-10">
                                  <select id="select-tipe-kendaraan" class="form-control select2" multiple="multiple" data-placeholder="Tipe kendaraan"
                                          style="width: 100%;">
                                    <option>karimun</option>
                                    <option>ertiga</option>
                                    <option>carry</option>
                                    <option>swift</option>
                                  </select>
                                    <div class="form-inline">
                                      
                                          <input type="checkbox" class="form-check-input checkbox" id="checkbox-tipe-kendaraan" style="width:20px; height:20px ">
                                     
                                     
                                      <label class="col-form-label py-1 px-0.5" style=" height:20px">Select All</label>
                                    
                                  </div>
                              </div>

                            </div>


                            <div class="form-group row">
                                <label for="month1" class="col-sm-2 col-form-label">Model Kendaraan</label>
                                <div class="col-sm-10">
                                  <select id="select-model-kendaraan" class="form-control select2" multiple="multiple" data-placeholder="model"
                                          style="width: 100%;">
                                    <option>karimun</option>
                                    <option>ertiga</option>
                                    <option>carry</option>
                                    <option>swift</option>
                                  </select>
                                    <div class="form-inline">
                                      
                                          <input type="checkbox" class="form-check-input checkbox" id="checkbox-tipe-kendaraan" style="width:20px; height:20px ">
                                     
                                     
                                      <label class="col-form-label py-1 px-0.5" style=" height:20px">Select All</label>
                                    
                                  </div>
                              </div>

                            </div>
                   

                  <div class="text-right"> <!--You can add col-lg-12 if you want -->
                      <button type="button" onclick="buttonreset()" class="btn-warning btn">reset</button>
                      <button type="button" id="btn_submit" class="btn-info btn">Submit</button>
                 </div>
</form>


<script>

$(function () {
    //Initialize Select2 Elements
    $('.select2').select2({
    width: "100%",
    templateSelection: function (data, container) {
      $(container).css("background-color", "#185ADB");
      return data.text;
    },
  }).trigger("change");


    $("#checkbox-sales-head").click(function(){
              if($("#checkbox-sales-head").is(':checked') ){
                  $("#select-seles-head > option").prop("selected","selected");
                  $("#select-seles-head").trigger("change");

              }else{
                  $("#select-seles-head > option").prop("selected","");
                   $("#select-seles-head").trigger("change");
              }
    });


    $("#checkbox-salesman").click(function(){
              if($("#checkbox-salesman").is(':checked') ){
                  $("#select-selesman > option").prop("selected","selected");
                  $("#select-selesman").trigger("change");

              }else{
                  $("#select-selesman > option").prop("selected","");
                   $("#select-selesman").trigger("change");
              }
    });


    $("#checkbox-tipe-kendaraan").click(function(){
              if($("#checkbox-tipe-kendaraan").is(':checked') ){
                  $("#select-tipe-kendaraan > option").prop("selected","selected");
                  $("#select-tipe-kendaraan").trigger("change");

              }else{
                  $("#select-tipe-kendaraan > option").prop("selected","");
                   $("#select-tipe-kendaraan").trigger("change");
              }
    });


    $("#checkbox-branch").click(function(){
              if($("#checkbox-branch").is(':checked') ){
                  $("#select-branch > option").prop("selected","selected");
                  $("#select-branch").trigger("change");

              }else{
                  $("#select-branch > option").prop("selected","");
                   $("#select-branch").trigger("change");
              }
    });


  });

  buttonreset=function(){
      if($("#checkbox-branch").is(':checked') ){
                  $("#select-branch > option").prop("selected","");
                   $("#select-branch").trigger("change");
      }

      if($("#checkbox-tipe-kendaraan").is(':checked') ){
                  $("#select-tipe-kendaraan > option").prop("selected","");
                   $("#select-tipe-kendaraan").trigger("change");
              }

      if($("#checkbox-salesman").is(':checked') ){
                  $("#select-selesman > option").prop("selected","");
                   $("#select-selesman").trigger("change");

              }

      if($("#checkbox-sales-head").is(':checked') ){
                  $("#select-seles-head > option").prop("selected","");
                  $("#select-seles-head").trigger("change");

              }


      document.getElementById('year').value = "";
      document.getElementById('selectedValuesDate').value = "";
      document.getElementById('selectedValuesMonth').value = "";


      $('.checkbox').prop('checked', false);
  }


  </script>

<script>
var $search = $('#selectedValuesDate');
    var $dropBox = $('#parentDate');

    $search.on('blur', function (event) {
        //$dropBox.hide();
    }).on('focus', function () {
        $dropBox.show();
        
    });

    var $search1 = $('#selectedValuesMonth');
    var $dropBox1 = $('#parentMonth');

    $search1.on('blur', function (event) {
        //$dropBox.hide();
    }).on('focus', function () {
        $dropBox1.show();
        
    });
  </script>


  <script>
      function cekInputFilter(tahun, bulan, tgl, cabang){
        var DO = "SELECT   substring(datename(mm,a.inquirydate),0,4) as bulan, year(a.inquirydate) as tahun, COUNT(a.CompanyCode) as jumlah FROM      pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ a.LastProgress in ('DO','DELIVERY') and ";

        var SPK="SELECT   substring(datename(mm,a.inquirydate),0,4) as bulan, year(a.inquirydate) as tahun, COUNT(a.CompanyCode) as jumlah FROM      pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ a.LastProgress in ('SPK') and ";

        var Inquiry="SELECT   substring(datename(mm,a.inquirydate),0,4) as bulan, year(a.inquirydate) as tahun, COUNT(a.CompanyCode) as jumlah FROM      pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ a.LastProgress in ('SPK','DO','DELIVERY') and ";

        var InquiryLost="SELECT   substring(datename(mm,a.inquirydate),0,4) as bulan, year(a.inquirydate) as tahun, COUNT(a.CompanyCode) as jumlah FROM      pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ a.LastProgress in ('LOST') and ";

        var sqlEnd="group by datename(mm,a.inquirydate), year(a.inquirydate) order by datename(month,a.inquirydate), year(a.inquirydate) asc" ;

          if(tahun !isnull() && bulan !Isnull()){

            var bulan=[];
            var bulan1=[];
            var year=document.getElementById("year").value;
            var listBulan=document.getElementById("selectedValuesMonth").value;
               for(var i=0; i<listBulan.length; i++){
                    bulan.push(listBulan[i]);
                    bulan1.push(listBulan[i]);
               }
                
            hitungbulan(2, year, bulan, bulan1);
            hitungbulan(1, year, bulan, bulan1);
            bulan1=getUnique(bulan1);

            $hasil="";
            $i=0;
            $whereMonth="";
            $whereYear="";
       for(var i=0;$i<$bulan1.length;i++){
          hasil=hasil." ".($bulan1[i]);
          hasilExplode=explode("-",bulan1[i]);
          for(j=0;j<count(hasilExplode);j++){
            if(j==0){
            if(i==count($bulan1)-1){
              whereMonth= whereMonth+"month(a.inquiryDate)="+$hasilExplode[0];
            }else{
              $whereMonth= $whereMonth+"month(a.inquiryDate)="+$hasilExplode[0]+" or ";
            }
            }else{

              if(i==count($bulan)-1){
              whereYear= whereYear+"year(a.inquiryDate)="+$hasilExplode[1];
            }else{
              whereYear= whereYear+"year(a.inquiryDate)="+$hasilExplode[1]+" or ";
            }

            }
          }
       }
       whereMonth= "("+whereMonth+") and ("+$whereYear+") ";
       DO=DO+$whereMonth; 
       SPK=SPK+$whereMonth; 
       Inquiry=Inquiry+$whereMonth; 
       InquiryLost=InquiryLost+$whereMonth;    

          }


        if(tgl !isnull()){
          
           tglList1="";
            var listTgl=tgl.split(",");
              for( var i=0; i<count(listTgl);i++){
                  if(i==count(listTgl)-1){
                      tglList1=tglList1."day(a.inquiryDate)= "+listTgl[i]
                  }else{
                      tglList1=tglList1."day(a.inquiryDate)= "+listTgl[i] +" or "
                  }
              }

              DO=DO+$whereMonth+" and ("+tglList1+")";
              SPK=SPK+$whereMonth+" and ("+tglList1+")";
              Inquiry=Inquiry+$whereMonth+" and ("+tglList1+")";
              InquiryLost=InquiryLost+$whereMonth+" and ("+tglList1+")";
        }

        if(cabang !isnull()){
            cabangList1="";
            var cabangList=cabang.split(",");
              for( var i=0; i<count(cabangList);i++){
                  if(i==count(cabangList)-1){
                      cabangList1=cabangList1." a.BranchCode= "+cabangList[i]
                  }else{
                      cabangList1=cabangList1."a.BranchCode= "+cabangList[i] +" or "
                  }
              }

              DO=DO+$whereMonth+" and ("+cabangList1+")";
              SPK=SPK+$whereMonth+" and ("+cabangList1+")";
              Inquiry=Inquiry+$whereMonth+" and ("+cabangList1+")";
              InquiryLost=InquiryLost+$whereMonth+" and ("+cabangList1+")";
        }



              DO=DO+" "+sqlEnd;
              SPK=SPK+" "+sqlEnd;
              Inquiry=Inquiry+" "+sqlEnd;
              InquiryLost=InquiryLost+" "+sqlEnd;

      }//end function
  </script>




  <script>
           $('#btn_submit').on('click',function(){
            var kobar=$('#electedValuesMonth').val();
            var nabar=$('#seletedValueYear').val();
            alert ("function");
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('index.php/admin/daily_monitor_sales/getdataChart')?>",
                dataType : "JSON",
                success: function(data){
                  var html='';
                  var i;
                      $('#message_id').html(data[1]);
                },
                error: function(data){
                //get the status code
                    
                        alert('400 status code! user error');
                  
                   // alert (code);
              },

            });
            return false;
        });
  </script>


