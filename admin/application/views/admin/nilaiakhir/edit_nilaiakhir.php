
  <style>
  .text-right{
    text-align:center;
  }
    
  </style>

	
<script type="text/javascript">
 var idpengajar;
 var idkelas;
 var idpelajaran;
 var arrnilaiakhir = [];

					$(document).ready(function() {
						
							 var hgt;
							 var frm;
							 var wnd;
							
							
							 var taskb = document.documentElement.clientHeight;
							 hgt = taskb -174
							 
														 
							 $('#groupinput').height(hgt-76);
							 
							 $('#simpan').on('click', function(){
										update();
										setTimeout(function(){
											updatedtl();
										},500);
										/*$("#simpan").prop("disabled", true);*/
										
							  });
							 $('#add').on('click', function(){
									tambah();
								});
							
							 $( window ).on( "load", function() {
								 showfield();
								 
								 
								setTimeout(function(){
											creategrid();
											    idpengajar = $("#idpengajar").val();
												let customerMultiColumn1 = $('#idpengajar').data("kendoMultiColumnComboBox");
												customerMultiColumn1.select(function(dataItem) {
													return dataItem.idpengajar === idpengajar;
												});
												idkelas = $("#idkelas").val();
												let customerMultiColumn2 = $('#idkelas').data("kendoMultiColumnComboBox");
												customerMultiColumn2.select(function(dataItem) {
													return dataItem.idkelas === idkelas;
												});
												loadpelajaran(idkelas);
												idpelajaran = $("#idpelajaran").val();
												let customerMultiColumn3 = $('#idpelajaran').data("kendoMultiColumnComboBox");
												customerMultiColumn3.select(function(dataItem) {
													return dataItem.idpelajaran === idpelajaran;
												});
										},500);
							});
							$("#idkelas").attr("readOnly","readOnly");
							$("#idpengajar").kendoMultiColumnComboBox({
										dataTextField: "nama",
										dataValueField: "idpengajar",
										height: 400,
										columns: [
											
											{ field: "nik", title: "NIK", width: 100 },
											{ field: "nama", title: "Nama" },
											
									  		
										],
										footerTemplate: 'Total #: instance.dataSource.total() # items found',
										filter: "contains",
										filterFields: ["nik", "nama"],
										dataSource: {
											
											transport: {
												read: {
														contentType: "application/json; charset=utf-8",
														dataType: "json",
														type: 'post',
														url: '<?php echo base_url(); ?>cnilaiakhir/datapengajar',
													}
												
											}
										},
										change: function (e) {
													var dataItem = e.sender.dataItem();
													idpengajar =  dataItem.idpengajar;

												}
									});
										
										
										$("#idkelas").kendoMultiColumnComboBox({
										dataTextField: "kelas",
										dataValueField: "idkelas",
										height: 400,
										columns: [
											
											{ field: "kelas", title: "Kelas" },
											
											
									  		
										],
										footerTemplate: 'Total #: instance.dataSource.total() # items found',
										filter: "contains",
										filterFields: ["kelas"],
										dataSource: {
											
											transport: {
												read: {
														contentType: "application/json; charset=utf-8",
														dataType: "json",
														type: 'post',
														url: '<?php echo base_url(); ?>cnilaiakhir/datakelas',
													}
												
											}
										},
										change: function (e) {
													var dataItem = e.sender.dataItem();
													idkelas =  dataItem.idkelas;

												}
									});
									
									
						
							function updatedtl(idnilaiakhir) {
								var arr = [];
								var grid = $("#grid").data("kendoGrid");
								var data = grid.dataSource.data();
								
								$.each(data, function (i, row) {
									
										    var idnilaiakhirdtl=data[i]["idnilaiakhirdtl"];
											var idnilaiakhir=data[i]["idnilaiakhir"];
										    var idpeserta=data[i]["idpeserta"];
											var mendengar=data[i]["mendengar"];
											var membaca=data[i]["membaca"];
											var percakapan=data[i]["percakapan"];
											var kosakata=data[i]["kosakata"];
											
											
											 if (parseInt(idnilaiakhirdtl) > 0 )
											  {
													   arr.push({
														    idnilaiakhirdtl:idnilaiakhirdtl,
														  	idnilaiakhir:idnilaiakhir,
															idpeserta:idpeserta,
															mendengar:mendengar,
															membaca:membaca,
															percakapan:percakapan,
															kosakata:kosakata,
															
														  });
												}
											
												
								 });
										
									var arr= JSON.stringify(arr);	
													$.ajax({
														  url: '<?php echo base_url(); ?>cnilaiakhir/updatedtl',
														  type: 'get',
														  data:{arr:arr},
														  async: false,
														  dataType: "json",
														  success: function(data)
															  {
																												 
															  }
														  
															});
									
										
							}
										
										
												
						 });
						  
						
								
					
								
						
						
						function creategrid() {
						  	var datanilaiakhir=JSON.stringify(arrnilaiakhir);
							
							 $("#grid").kendoGrid({
									
									
									dataSource: {
											transport: {
													read: 
														{
															url: '<?php echo base_url(); ?>cnilaiakhir/tampiledit?idnilaiakhir='+idtable,
															contentType: "application/json; charset=utf-8",
															dataType: "json",
															type: 'post'
														},			
												},
						 					schema: {
											data: "data",
											model: {
												id: "idnilaiakhir"	,
												fields: {
														idnilaiakhir: { field: "idnilaiakhir", defaultValue: 1 },
														nilai: { field: "nilai", format: "{0:n}", type: "number"},
														
													}
											}
									}},
									
									columns: [
									  { field: "idnilaiakhirdtl",hidden:true  },
									  { field: "idnilaiakhir",title:"idnilaiakhir",hidden:true},
									  { field: "idpeserta",title:"idpeserta",hidden:true},
									  { field: "nik", title:"NIK",width:100,},
									  { field: "nama", title:"Nama",width:300,},
									  { field: "mendengar",title:"Mendengar",width:100,
									  	headerAttributes: {"class": "table-cell", style: "text-align: center; font-size: 14px"},
										attributes: {"class": "table-cell", style: "text-align: right; font-size: 14px"}},
									  { field: "membaca",title:"Membaca",width:100,
									  	headerAttributes: {"class": "table-cell", style: "text-align: center; font-size: 14px"},
										attributes: {"class": "table-cell", style: "text-align: right; font-size: 14px"}},
									  { field: "kosakata",title:"Kosa Kata",width:100,
									  	headerAttributes: {"class": "table-cell", style: "text-align: center; font-size: 14px"},
										attributes: {"class": "table-cell", style: "text-align: right; font-size: 14px"}},
									  { field: "percakapan",title:"Percakapan",width:100,
									  	headerAttributes: {"class": "table-cell", style: "text-align: center; font-size: 14px"},
										attributes: {"class": "table-cell", style: "text-align: right; font-size: 14px"}},
									  
									 
										  
									],
									
									selectable: "cell",
									
									//filterable: true,
									editable: true,
									
									//navigatable: true,
									//pageable: { pageSizes: false },        
								  });
								  var grid = $("#grid").data("kendoGrid");
									
	
						}
							function loadpelajaran(idkelas)
									{
										$("#idpelajaran").kendoMultiColumnComboBox({
											dataTextField: "nmpelajaran",
											dataValueField: "idpelajaran",
											height: 400,
											columns: [
												
												{ field: "kdpelajaran", title: "Kode",width:65 },
												{ field: "nmpelajaran", title: "Pelajaran" },
												
												
												
											],
											footerTemplate: 'Total #: instance.dataSource.total() # items found',
											filter: "contains",
											filterFields: ["kdpelajaran","nmpelajaran"],
											dataSource: {
												
												transport: {
													read: {
															contentType: "application/json; charset=utf-8",
															dataType: "json",
															type: 'get',
															data:{idkelas:idkelas},
															url: '<?php echo base_url(); ?>cnilaiakhir/datapelajaran',
														}
													
												}
											},
											change: function (e) {
														var dataItem = e.sender.dataItem();
														idpelajaran =  dataItem.idpelajaran;
	
													}
										});
									}
						  </script>
                           

                <!-- form start -->
                  <div id="content" class="">
            <!-- content starts -->
     <div>

          <ul class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>admin">Home</a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>cnilaiakhir/tampil">Nilai Akhir</a>
            </li>
            <li>
                <a href="#">Edit</a>
            </li>
        </ul>
    </div>

    <div class=" row"  style="margin-top:-18px">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-user"></i> Edit Nilai Akhir</h2>

        <div class="box-icon">
            
            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a>
            <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
        </div>
    </div>
    <div class="box-content">
                <!-- form start -->
 <div  id="groupinput" class="form-group" style="overflow:auto; margin:0 0 10px 0;"> 
 
 <!-- batas form spkl-->
  				<div class="box-header well" data-original-title="" style="margin-right:4px">
                        <h2><i class="glyphicon glyphicon-user"></i> Nilai Akhir </h2>
                
                        <div class="box-icon">
                            
                            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a>
                            <!--<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                        </div>
                    </div>
                    <div class="box-content">
                   <form id="form2" name="form2" method="" action=""  >
     
    <!-- batas form Ijin-->
                    
                    
                   
                       
                         
   <!-- batas form-->
                         
    					
                    <label for="exampleInputEmail1">Tanggal  </label>
                      <input type="text" class="form-control" name="tgl"  id="tgl" placeholder="Tanggal "  title="date"/>
                      
                      <input type="text" class="form-control" name="kdnilaiakhir" id="kdnilaiakhir"   placeholder="No. Reg" disabled="disabled" style="display:none" />
                
                      <label for="exampleInputEmail1">Pengajar</label>
                     <input type="text" class="form-control" name="idpengajar" id="idpengajar"   placeholder="Pengajar" style="width:100%"  />
                     <label for="exampleInputEmail1">Kelas</label>
                     <input type="text" class="form-control" name="idkelas" id="idkelas"   placeholder="Kelas" style="width:100%"  />
                     <label for="exampleInputEmail1">Pelajaran</label>
                     <input type="text" class="form-control" name="idpelajaran" id="idpelajaran"   placeholder="Pelajaran" style="width:100%"  />

     
                      
               
                 
                 
                  </div>
                  
                     <!-- batas form List Karyawan-->
                  <div class="box-header well" data-original-title="" style="margin-right:4px">
                        <h2><i class="glyphicon glyphicon-user"></i> Detail Nilai Harian </h2>
                
                        <div class="box-icon" >
                            
                            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a>
                            <!--<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                        </div>
                    </div>
                    <div class="box-content">
                    	
				
           
		

                    
                    <div id="grid"></div>
                    </div>
                    
                      <!-- batas form List Karyawan-->
                  </div>

                  
                  <input type="hidden" name="id" >
                  <a href="<?php echo base_url(); ?>cnilaiakhir/tampil" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Batal</a>
                  <button type="button" name="simpan" id="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                 
<a href="<?php echo base_url(); ?>cnilaiakhir/tambah_nilaiakhir" class="btn btn-danger"><i class="fa fa-retweet"></i> Add</a>
                </form>

               
 </div>

            </div>
        </div>
    </div>
    </div>
 