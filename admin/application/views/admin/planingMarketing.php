

      <script type="text/javascript">

          $(document).ready(function() {
            
               var hgt;
               var frm;
               var taskb = document.documentElement.clientHeight;
               hgt = taskb -174
               $('#groupinput').height(hgt);
              
              
                $('#preview').click(function(){
                
                test1=document.getElementById("table_search").value;
                tglawal=document.getElementById("tglmulai").value;
                tglakhir=document.getElementById("tglakhir").value;
                window.location="<?php echo base_url(); ?>report/cabsen/preview?field=" + test1 + "&tglawal=" + tglawal + "&tglakhir=" + tglakhir;
                
               });



                var _peopleDataSource = new kendo.data.DataSource({
    // transport :{
    //        read: function(options){ 
    //             $.ajax({
    //                 type: "get",
    //                // url:"https://demos.telerik.com/kendo-ui/service/products",
    //                 url:"<?php echo base_url();?>cplaningMarketing/tampil_data",
    //                 dataType: "json",
    //                 success:function(response){
    //                 console.log(response);
    //                 options.success(response); 
                    
    //                 },error:function(data, error){
    //                      console.log("kendo data read gagal "+error);
    //                 }
    //             });
    //        }

    // },
    model: {
              fields: {
                
                idPlaning: { type: "number" },
                //no: { type: "string" },
                tglEvent: { type: "date" },
                alokasibugetevent: { type: "string" },
                tempatPameran: { type: "string" },
                unitDisplay: { type: "string" },
                qtySales: { type: "number" },
                namaSH: { type: "string" },
                inq: { type: "number" },
                hp: { type: "number" },
                spk: { type: "number" },
                inq_end_month : { type: "number" },
                hp_end_month: { type: "number" },
                spk_end_month: { type: "number" },
                difRealSPKTgt: { type: "number" },
                ratioInqSPK: { type: "number" },
                difRealSPKTgtPercent: { type: "number" },
                totalCost: { type: "number" },
                costSPK:{ type: "number" },
                analisa:{ type: "string" },
                editable:false,
              }

          }
});


    tampildata(_peopleDataSource);



 var _roleDataSource = new kendo.data.DataSource({
    data: [
        { id: 1, title: "Pameran - input di MAOS" },
        { id: 2, title: "Showroom Event - input di MAOS" },
        { id: 3, title: "Pameran tambahan PT. SIS" }
    ]
});



function textAreaEditor(container, options) {
$('<textarea class="k-textbox" name="' + options.field + '" style="width:100%;height:100%;" />').appendTo(container);
}

const fields=["idPlaning","tglEvent","alokasibugetevent","tempatPameran","unitDisplay","qtySales","namaSH","inq","hp","spk","inq_end_month","hp_end_month","spk_end_month","difRealSPKTgt","ratioInqSPK","difRealSPKTgtPercent","totalCost","costSPK","analisa"];

var _grid = $("#grid").kendoGrid({
    dataSource: _peopleDataSource,
     toolbar: [ {name:"create", text:"Tambah Data"},"search"
     // { name: "tambahData", text: "Tambah Data", imageClass: "k-create", className: "k-custom-create", iconClass: "k-icon" },
     ],
     height: document.documentElement.clientHeight-180,
    groupable: false,
         scrollable: true,
         sortable: true,
         pageable: {alwaysVisible: true,
                    pageSizes: 10,
                  },
         filterable: false,
         
    columns: [
        {
            field: "idPlaning",
            title: "Id",
            headerAttributes: { "style": "text-align: center; vertical-align: middle;"},
            width: "100px",
            height: "50px",
            hidden:false
           
        },
        // {
        //     title: "#",
        //     template: "#= ++record #",
        //     width: 50,
        //      headerAttributes: { "style": "text-align: center; vertical-align: middle;"},
        //   },
        // {
        //     field: "no",
        //     title: "No",
        //     headerAttributes: { "style": "text-align: center; vertical-align: middle;"},
        //     width: "100px",
        //     height: "50px",

        //     //hidden:"true"
        // },
        {
            field: "tglEvent",
            title: "tanggal event",
            format:"{0:yyyy-MM-dd}",
            headerAttributes: { "style": "text-align: center; vertical-align: middle; white-space: normal;"},
            type:"date",
            width: "150px",
            height: "50px",
            editor: function dateTimeEditor(container, options) {
                    $('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
                            .appendTo(container)
                            .kendoDatePicker({});
                }
        },
        {
            field: "alokasibugetevent",
            title: "Alokasi Bugdet Event",
            headerAttributes: { "style": "text-align: center; vertical-align: middle; white-space: normal;"},
            width: "150px",
            height: "50px",
            //template:  "#= combobox() #",
            editor: function(container, options) {
                $("<input data-bind='value:alokasibugetevent' />")
                    .appendTo(container)
                    .kendoDropDownList({
                        dataSource: _roleDataSource,
                        dataTextField: "title",
                        dataValueField: "title",
                        template: "<span data-id='${data.id}'>${data.title}</span>",
                        select: function(e) {
                            var id = e.item.find("span").attr("data-id");
                            var title1=e.item.find("span").html();
                            combobox(title1);
                            var person =_grid.dataItem($(e.sender.element).closest("tr"));
                            person.no = id;
                            person.alokasibugetevent=title1;
                            $('#grid').data('kendoGrid').refresh(); 

                            setTimeout(function() {
                                $("#log")
                                    .prepend($("<div/>")
                                        .text(
                                            JSON.stringify(_grid.dataSource.data().toJSON())
                                         ).append("<br/><br/>")
                                    );
                                });


                        }
                    });
            }
        },
        {
            field: "tempatPameran",
            title: "Tempat Pameran",
            width: "150px",
            height: "50px",
            
             //template:  "#= kendo.toString(name, '$###.##') #",

             
            
            headerAttributes: { "style": "text-align: center; vertical-align: middle; white-space: normal;"},
        },
        {
            field: "unitDisplay",
            title: "Unit Display",
             width: "100px",
             height: "50px",
            
             //template:  "#= kendo.toString(name, '$###.##') #",

             
            
            headerAttributes: { "style": "text-align: center; vertical-align: middle; white-space: normal;"},
        },
        {
            field: "qtySales",
            title: "Qty Sales",
            width: "100px",
            height: "50px",
            
             //template:  "#= kendo.toString(qtySalese, '$###.##') #",

             
            
            headerAttributes: { "style": "text-align: center; vertical-align: middle; white-space: normal;"},
        },
        {
            field: "namaSH",
            title: "Nama SH",
             width: "100px",
             height: "50px",
            
             //template:  "#= kendo.toString(name, '$###.##') #",

             
            
            headerAttributes: { "style": "text-align: center; vertical-align: middle; white-space: normal;"},
        },
        {
            title:"Target (Planing)",
            width:"200px",
             headerAttributes: { "style": "text-align: center; vertical-align: middle;"},
            columns:[
            {
                field:"inq",
                title:"INQ",
                width:"50px",
                height: "50px",
                //template:  "#= kendo.toString(inq, '$###.##') #",
            },
            {
                field:"hp",
                title:"HP",
                width:"50px",
                height: "50px",
                 //template:  "#= kendo.toString(hp, '$###.##') #",
            },
            {
                field:"spk",
                title:"SPK",
                width:"50px",
                height: "50px",
                //template:  "#= kendo.toString(spk, '$###.##') #",
            },

            ]
        },
        {
            title:"Real (End Of Month)",
            width:"200px",
             headerAttributes: { "style": "text-align: center; vertical-align: middle;"},
            columns:[
            {
                field:"inq_end_month",
                title:"INQ",
                width:"50px",
                height: "50px",
                //template:  "#= kendo.toString(inq_end_month, '$###.##') #",

            },
            {
                field:"hp_end_month",
                title:"HP",
                width:"50px",
                height: "50px",
                //template:  "#= kendo.toString(hp_end_month, '$###.##') #",
            },
            {
                field:"spk_end_month",
                title:"SPK",
                width:"50px",
                height: "50px",
                //template:  "#= kendo.toString(spk_end_month, '$###.##') #",
            },

            ]
        },
         {
            field: "difRealSPKTgt",
            title: "Dif Real SPK & Tgt",
             width: "100px",
             height: "50px",
             //template:  "#= kendo.toString(difRealSPKTgt, '$###.##') #",
                        
            headerAttributes: { "style": "text-align: center; vertical-align: middle; white-space: normal;"},
        },
        {
            field: "ratioInqSPK",
            title: "Ratio Inq - SPK",
             width: "100px",
             height: "50px",
             //template:  "#= kendo.toString(ratioInqSPK, '$###.##') #",
                        
            headerAttributes: { "style": "text-align: center; vertical-align: middle; white-space: normal;"},
        },
        {
            field: "difRealSPKTgtPercent",
            title: "% Dif Real SPK & Tgt",
             width: "100px",
             height: "50px",
             //template:  "#= kendo.toString(difRealSPKTgtPercent, '$###.##') #",
                        
            headerAttributes: { "style": "text-align: center; vertical-align: middle; white-space: normal;"},
        },
        {
            field: "totalCost",
            title: "Total Cost",
             width: "100px",
             height: "50px",
             //template:  "#= kendo.toString(totalCost, '$###.##') #",
                        
            headerAttributes: { "style": "text-align: center; vertical-align: middle; white-space: normal;"},
        },
        {
            field: "costSPK",
            title: "Cost / SPK",
            //template:  "#= kendo.toString(costSPK, '$###.##') #",
                        
            headerAttributes: { "style": "text-align: center; vertical-align: middle;"},
             width: "100px",
             height: "50px",
        },
        {
            field: "analisa",
            title: "Analisa",
                        
            headerAttributes: { "style": "text-align: center; vertical-align: middle;"},
            fieldAttributes: { "style": "text-align: center; vertical-align: middle;"},
             width: "200px",
             
             editor:textAreaEditor, 
        },
        
        { command: [

            { name: "simpan", text: "save", imageClass: "k-save", className: "k-custom-save", iconClass: "k-icon" },
            { name: "hapus", text: "hapus", imageClass: "k-destroy", className: "k-custom-destroy", iconClass: "k-icon" }

         ], title: "Action", width: "300px",
        headerAttributes: { "style": "text-align: center; vertical-align: middle;"},
         }
    ],
    editable: true,
    
}).data("kendoGrid");


$(".k-grid-hapus").bind('click', function (e) {
  alert("testing");
  

});


function combobox(data){
  return kendo.toString(data);
}

//function for get value in td-1



    $("#grid").on("click", "td", function (e) {
        var row = $(this).closest("tr");
        var textVal = "";
       // row.find("td").on("click",function(i, r) {
            //textVal += `Col ${i+1}: ${r.innerText}\n`;
            index=$("td", row).index(this);
           console.log(row.find("td:eq("+index+")").html());
       // });
        //alert(textVal);
    });


    $("#grid").on("click", ".k-grid-simpan", function (e) {
       
        var row = $(this).closest("tr");
        //alert(JSON.stringify(_grid.dataSource.data().toJSON()));
        var textVals = [];
        var textVal="";
        
        row.find("td").each(function(i, r) {
          var key=fields[i];
          if(i<19){

            //textVal = `${fields[i]}:${r.innerText}`;
            textVals[key]=r.innerText;
            
          }
            
        });

     
       //console.log(textVals);
       var myjson=JSON.stringify(Object.assign({},textVals));
       var idPlaning;
       console.log("json stringify "+myjson["idPlaning"]);
       var dataJSON=JSON.parse(myjson,(key,value)=>{
            if(key=='idPlaning'){
              if(value!=""){
                  idPlaning=value;
              }else{
                  idPlaning=0;
              }
            }



          console.log("data idPlaning "+idPlaning) ;
          return value;

       });
       myjson=JSON.stringify(dataJSON);
       console.log("data telah diupdate "+ myjson);

       if(idPlaning==0){

          $.ajax({
                type: "get",
                url:"<?php echo base_url();?>cplaningMarketing/insertdata",
                data:{myjson:myjson},
                dataType: "json",
                success: function(data) {
                  console.log("penyimpanan data berahasil");
                  //window.location.href = "<?php echo base_url();?>cplaningMarketing/tampil";
                    if(data.success==1){
                      SnackBar({
                            message: "data berhasil di simpan",
                            status: "success",
                            position:"bc",
                            width:"600px",

                        });
                      
                    }else{
                      SnackBar({
                            message: "data gagal di simpan",
                            status: "error",
                            position:"bc",
                            width:"600px",

                        });
                    }

                },error:function(data, error){
                    console.log("data errror : "+data.responseText);
                }


          });


       }else{
          $.ajax({
                type: "get",
                url:"<?php echo base_url();?>cplaningMarketing/updatedata",
                data:{id:idPlaning,myjson:myjson},
                dataType: "json",
                success: function(data) {
                 // window.location.href = "<?php echo base_url();?>cplaningMarketing/tampil";
                    console.log("penyimpanan data berhasil coy");
                    SnackBar({
                            message: "data berhasil di update",
                            status: "success",
                            position:"bc",
                            width:"600px",

                        });
               
                },error:function(data, error){
                    console.log("data errror : "+error);
                    SnackBar({
                            message: "data gagal di update",
                            status: "error",
                            position:"bc",
                            width:"600px",

                        });
                }
          });
       }

               
     window.location.href = "<?php echo base_url();?>cplaningMarketing/tampil";
       
    });


    $("#grid").on("click", ".k-grid-hapus", function (e) {
        //  $.ajax({
        //         type: "get",
        //         url:"<?php echo base_url();?>cplaningMarketing/hapusplaning",
        //         data:{id:idPlaning},
        //         dataType: "json",
        //         success:function(data){

        //         }
        // });

    var row = $(this).closest("tr");
        //alert(JSON.stringify(_grid.dataSource.data().toJSON()));
        var textVals = [];
        var textVal="";
        
        row.find("td").each(function(i, r) {
          var key=fields[i];
          if(i<19){

            //textVal = `${fields[i]}:${r.innerText}`;
            textVals[key]=r.innerText;
            
          }
            
        });

        var isiId=textVals["idPlaning"];
           $.confirm({
                    title: 'Konfirmasi !',
                    content: 'Hapus Data?',
                    buttons: {
                        confirm: function () {
                           
                                   $.ajax({
                                          type: "get",
                                          url:"<?php echo base_url();?>cplaningMarketing/hapusplaning",
                                          data:{id:isiId},
                                          dataType: "json",
                                          success:function(data){
                                                if(data.success==1){

                                                     SnackBar({
                                                        message: "data berhasil di Hapus",
                                                        status: "success",
                                                        position:"bc",
                                                        width:"600px",

                                                    });

                                                }else{
                                                    console.log("terjadi error di functio hapus");
                                                }
                                          },error:function(data){
                                                   console.log("fatal error "+data.responseText);
                                          }
                                     });
                        },
                        cancel: function () {
                            $.alert('hapus data dibatalkan');
                        }
                    }
                }); 
           





        });
              
 });
              
               
</script>

 <div id="content" class="">
            <!-- content starts -->
     <div>
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>admin">Home</a>
            </li>
            <li>
                <a href="#">Planing Aktivitas Marketing</a>
            </li>
        </ul>
    </div>

  <div class=" row"  style="margin-top:-18px">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-user"></i> Planing Aktivitas Marketing </h2>

       
    </div>
    <div class="box-content" id="groupinput">
       


<div id="grid"></div>


<script>



    
</script>

         
    </div>         
              
  
       
   </div>



            </div>
        </div>
    </div>
    </div>



      
      <script type="text/javascript">

      $(document).ready(function() {
      var field1='<?php echo $field=$this->input->post('table_search');?>';
      var tglawal='<?php echo $field=$this->input->post('tglmulai');?>';
      var tglakhir='<?php echo $field=$this->input->post('tglakhir');?>';
        var dataTable =  $('#mytable').DataTable( {
          pageResize: true,  // page resize di aktifkan
          /*processing: true,*/
          serverSide: true,

          ajax: {"url": "<?php echo base_url(); ?>report/cabsen/json", "type": "POST","data":{"field":field1,"tglawal":tglawal,"tglakhir":tglakhir},},
                   
          "bFilter": false,
        "bLengthChange": false,
        
        iDisplayLength: 100,
        scrollX: true,
        scrollCollapse: true,
        scrollY:        '50vh',
        });
            
        
      } );


 
    $('input[name="tanggal"]').daterangepicker({
      locale: {
            format: 'DD/MMM/YYYY'
        }

      });

      function clear(){
        $('#select-branch').value="";
        $('#tanggal').value="";
      }
    
      
          
      
    </script>
         
  
       
   </div>



            </div>
        </div>
    </div>
    </div>



      
      <script type="text/javascript">

      $(document).ready(function() {
      var field1='<?php echo $field=$this->input->post('table_search');?>';
      var tglawal='<?php echo $field=$this->input->post('tglmulai');?>';
      var tglakhir='<?php echo $field=$this->input->post('tglakhir');?>';
        var dataTable =  $('#mytable').DataTable( {
          pageResize: true,  // page resize di aktifkan
          /*processing: true,*/
          serverSide: true,

          ajax: {"url": "<?php echo base_url(); ?>report/cabsen/json", "type": "POST","data":{"field":field1,"tglawal":tglawal,"tglakhir":tglakhir},},
                   
          "bFilter": false,
        "bLengthChange": false,
        
        iDisplayLength: 100,
        scrollX: true,
        scrollCollapse: true,
        scrollY:        '50vh',
        });
            
        
      } );


 
    $('input[name="tanggal"]').daterangepicker({
      locale: {
            format: 'DD/MMM/YYYY'
        }

      });

      function clear(){
        $('#select-branch').value="";
        $('#tanggal').value="";
      }
    
      var textEditorInitialize = function(container, options) {
    $('<textarea name="' + options.field + '" style="width: ' + container.width() + 'px;height:' + container.height() + 'px" />')
    .appendTo(container);
};





function tampildata(datasource){
                $.ajax({
                    type: "get",
                   // url:"https://demos.telerik.com/kendo-ui/service/products",
                    url:"<?php echo base_url();?>cplaningMarketing/tampil_data",
                    dataType: "json",
                    success:function(data){
                    
                    //options.success(response[0]); 


                    for(var i=1;i<=Object.keys(data).length;i++){
                        
                        

                      datasource.add(data[i]);
                    }

                    lockrowgrid();

                    
                    },error:function(data, error){
                         console.log("kendo data read gagal "+error);
                    }
                });
            };
          
      
function lockrowgrid(){
  var model = $("#grid").data("kendoGrid");
  console.log(model);
if (model) {
    model.fields["model"].editable = false;
}
}


    </script>