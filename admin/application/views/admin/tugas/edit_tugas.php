<!-- Content Wrapper. Contains page content -->

<script type="text/javascript">
 var idkaryawan;
 var filetugas;
 var file = [];
					$(document).ready(function() {
						 var hgt;
						 var frm;
						 var taskb = document.documentElement.clientHeight;
						 hgt = taskb -174;
						 $( window ).on( "load", function() {
								 showfield();
								 setTimeout(function(){
									 creategrid();
									  idkaryawan = $("#idkaryawan").val();
									  filetugas = $("#file_tugas").val();
									  let customerMultiColumn = $('#idkaryawan').data("kendoMultiColumnComboBox");
									  customerMultiColumn.select(function(dataItem) {
									  return dataItem.idkaryawan === idkaryawan;
									  });
									  		
											if (filetugas !== '')
											{file.push({name: filetugas,  extension: ""} );};
											 $("#file").kendoUpload({
												async: {
													saveUrl: '<?php echo base_url(); ?>ctugas/upload?files=file'  ,
													removeUrl: "remove",
													autoUpload: false
												},
												
												files: file,
												select: onSelect
												
											});
											
											
								  },500);
							});
						
					
						 
							
							function onSelect(e) {
								$.each(e.files, function (index, value) {
									
									$("#file_tugas").val(value.name);
								/*	console.log("Size: " + value.size + " bytes");
									console.log("Extension: " + value.extension);*/
								});
							};
							
							
							
						   $('#groupinput').height(hgt-76);
						   $('#simpan').on('click', function(){
								update();
										setTimeout(function(){
											updatedtl();
										},1000);
							});
							$('#add').on('click', function(){
								window.location = '<?php echo base_url(); ?>ctugas/tambah_tugas';
							});
						  
						  
						  $("#idkaryawan").kendoMultiColumnComboBox({
										dataTextField: "nmkaryawan",
										dataValueField: "idkaryawan",
										height: 400,
										columns: [
											
											{ field: "nik", title: "NIK", width: 100 },
											{ field: "nmkaryawan", title: "Nama" },
											
									  		
										],
										footerTemplate: 'Total #: instance.dataSource.total() # items found',
										filter: "contains",
										filterFields: ["nik", "nmkaryawan"],
										dataSource: {
											
											transport: {
												read: {
														contentType: "application/json; charset=utf-8",
														dataType: "json",
														type: 'post',
														url: '<?php echo base_url(); ?>ctugas/datakaryawan',
													}
												
											}
										},
										change: function (e) {
													var dataItem = e.sender.dataItem();
													idkaryawan =  dataItem.idkaryawan;

												}
									});
								
						  
						 });
						 
						 function updatedtl(idtugas) {
								var arr = [];
								var grid = $("#grid").data("kendoGrid");
								var data = grid.dataSource.data();
								
								var grid = $("#grid").data("kendoGrid")
								var ds = grid.dataSource.view();
								
								for (var i = 0; i < ds.length; i++) {
									var row = grid.table.find("tr[data-uid='" + ds[i].uid + "']");
									var checkbox = $(row).find(".checkbox");
									if (checkbox.is(":checked")) {
										var idkaryawan	=ds[i].idkaryawan;
										var idtugas	=ds[i].idtugas;
										var tgl_beres	=ds[i].tgl_beres;
										var idtugasdtl	=ds[i].idtugasdtl;
										var file_tugas	=ds[i].file_tugas;
										var tgl_mulai=$("#tgl_mulai").val();
										
										if (idkaryawan != '' && tgl_mulai != '' )
											  {
												arr.push({
															tgl_mulai:tgl_mulai,
															tgl_beres:tgl_beres,
															idtugas:idtugas,
															idtugasdtl:idtugasdtl,
															idkaryawan	:idkaryawan,
															file_tugas:file_tugas,
														});
											  }
										
												
									}
								}

									var arr= JSON.stringify(arr);	
									
													$.ajax({
														  url: '<?php echo base_url(); ?>ctugas/updatedtl',
														  type: 'get',
														  data:{arr:arr},
														  async: false,
														  dataType: "json",
														  success: function(data)
															  {
																alert("Data Tersimpan");										 
															  }
														  
															});
									}

						
						function creategrid() {
							  $("#grid").kendoGrid({
								  dataBound: function(e) {
										$("input[type='file']").kendoUpload();
									},
									columns: [
									  { field: "chk",title:"ID",width:40,
									  template: "<input name='chk' class='checkbox' type='checkbox' data-bind='checked: chk' #= chk ? checked='checked' : '' #/>" },
									  { field: "idtugas",title:"idtugas",hidden:true},
									  { field: "idtugasdtl",title:"idtugasdtl",hidden:true},
									  { field: "idkaryawan",title:"idkaryawan",hidden:true},
									  { field: "nik",title:"NIK",width:100,editable: true,width:100,},
									  { field: "nmkaryawan", title:"Nama",width:200,editable: true,},
									  
									{ field: "tgl_beres",title:"Tgl Beres", width:100,format: "{0:MM/dd/yyyy}",
									  editor: function(container, options)
									  { var input = $("<input/>"); 
										input.attr("name",options.field); 
										input.appendTo(container); 
										input.kendoDatePicker({});}
									},
									
									{ field: "file_tugas",title:"File Tugas", width:200,
									  editor: function(container, options)
									  { $('<input type="file" id="files" name="files" />')
										.appendTo(container)
										.kendoUpload({
											async:{
												saveUrl: "<?php echo base_url(); ?>ctugas/upload?files=files",
												autoUpload: true
											},
											select: function onSelect(e) {
													$.each(e.files, function (index, value) {
														/*alert(value.name);*/
														options.model.set("file_tugas", value.name);
													/*	console.log("Size: " + value.size + " bytes");
														console.log("Extension: " + value.extension);*/
													});
												}
										});
									  }
									},
									  /*{ field: "masuk",title:"Masuk"},*/
									  
									  		],
									dataSource: {
													transport: {
														read: {
															dataType: "json",
															data:{idtugas:idtable},
															url: '<?php echo base_url(); ?>ctugas/tugasdtl',
														},
														schema: {data: "data",
																	 model: {
																	  fields: {chk: { type: "boolean",editable: false }}
																	}}, 
															}, 
													
												},
									selectable: "cell",
									editable: true,
       
								  })
								  
						}
								 var grid = $("#grid").data("kendoGrid");
								 
									$("#file").kendoUpload({
												async: {
													saveUrl: '<?php echo base_url(); ?>ctugas/upload?files=file'  ,
													removeUrl: "remove",
													autoUpload: false
												},

												
											});
								

						
						  
						  </script>
                          
 <div id="content" class="">
            <!-- content starts -->
     <div>
         <ul class="breadcrumb">
            <li>
                <a href="<?php echo base_url(); ?>admin">Home</a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>ctugas/tampil">Tugas</a>
            </li>
            <li>
                <a href="#">Tambah</a>
            </li>
        </ul>
    </div>

    <div class=" row"  style="margin-top:-18px">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-user"></i> Edit Tugas</h2>

        <div class="box-icon">
            
            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a>
            <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
        </div>
    </div>
    <div class="box-content" >
                <!-- form start -->
<div id="groupinput" class="form-group" style="overflow:auto; margin:0 0 10px 0;"> 
                <!-- form start -->
    				<div class="box-header well" data-original-title="" style="margin-right:4px">
                        <h2><i class="glyphicon glyphicon-user"></i> Tugas </h2>
                
                        <div class="box-icon">
                            
                            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a>
                            <!--<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                        </div>
                    </div>
                    <div class="box-content">              
                 
                 <form id="form2" name="form2" method="" action=""  >
               
 
                           <label for="exampleInputEmail1">Tanggal </label>
                         <input type="text" class="form-control" name="tgl"  id="tgl" placeholder="Tanggal" title="date"/>
    					
                          <input type="text" class="form-control" name="kdtugas" id="kdtugas"   placeholder="Kode Tugas"  style="display:none"/>
                          
                        <label for="exampleInputEmail1">Karyawan</label>
                          <input type="text" class="form-control" name="idkaryawan" id="idkaryawan"   placeholder="Karyawan" style="width:100%"/>
                        <label for="exampleInputEmail1">Tanggal Mulai</label>
                          <input type="text" class="form-control" name="tgl_mulai" id="tgl_mulai"   placeholder="Tanggal Mulai" title="date"/>
                        
                       <?php /*?><label for="exampleInputEmail1">File Tugas</label>
                          
                            <div class="demo-section k-content">
                                
                                <input name="file" id="file" type="file" />
                            </div><?php */?>
                        
                        <label for="exampleInputEmail1">Ket</label>
                          <input type="text" class="form-control" name="ket" id="ket"   placeholder="Keterangan"/>
                          

                          
     
                        </div>
                  
                     <!-- batas form List Karyawan-->
                  <div class="box-header well" data-original-title="" style="margin-right:4px">
                        <h2><i class="glyphicon glyphicon-user"></i> List Tugas </h2>
                
                        <div class="box-icon" >
                            
                            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a>
                            <!--<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                        </div>
                    </div>
                    <div class="box-content">
                    	
				
           
		

                    
                    <div id="grid"></div>
                    </div>
                    
                      <!-- batas form List Karyawan-->
                  </div>
                        
                      <a href="<?php echo base_url(); ?>ctugas/tampil" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Back</a>
                      <?php /*?><input name="file_tugas" id="file_tugas" type="text"  style="display:none" /><?php */?>
                      <button type="button" name="simpan" id="simpan" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                      <button type="button" name="add" id="add" class="btn btn-danger"><i class="fa fa-retweet"></i> Add</button>
                      
              
              </form>
               
        </div>

            </div>
        </div>
    </div>
    </div>
        
         