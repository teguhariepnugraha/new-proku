
  <style>
  .text-right{
    text-align:center;
  }
    
  </style>

	
<script type="text/javascript">
 var idpeserta;
 var idsikap;
 var flag;
 var arrnilaisikap = [];

					$(document).ready(function() {
						
							 var hgt;
							 var frm;
							 var wnd;
							
							
							 var taskb = document.documentElement.clientHeight;
							 hgt = taskb -174
							 
														 
							 $('#groupinput').height(hgt-76);
							 
							 $('#simpan').on('click', function(){
										update();

							  });
							 $('#add').on('click', function(){
									tambah();
								});
							
							 $( window ).on( "load", function() {
								 showfield();
								 
								setTimeout(function(){
										        idpeserta = $("#idpeserta").val();
												let customerMultiColumn = $('#idpeserta').data("kendoMultiColumnComboBox");
												customerMultiColumn.select(function(dataItem) {
													return dataItem.idpeserta === idpeserta;
												});
												 idsikap = $("#idsikap").val();
												let customerMultiColumn1 = $('#idsikap').data("kendoMultiColumnComboBox");
												customerMultiColumn1.select(function(dataItem) {
													return dataItem.idsikap === idsikap;
												});
										},500);
							});
							
							$("#idpeserta").kendoMultiColumnComboBox({
										dataTextField: "nama",
										dataValueField: "idpeserta",
										height: 400,
										columns: [
											
											{ field: "nik", title: "NIK", width: 100 },
											{ field: "nama", title: "Nama" },
											{ field: "kelas", title: "Kelas", },
									  		
									  		
										],
										footerTemplate: 'Total #: instance.dataSource.total() # items found',
										filter: "contains",
										filterFields: ["nik", "nama","kelas"],
										dataSource: {
											
											transport: {
												read: {
														contentType: "application/json; charset=utf-8",
														dataType: "json",
														type: 'post',
														url: '<?php echo base_url(); ?>cnilaisikap/datapeserta',
													}
												
											}
										},
										change: function (e) {
													var dataItem = e.sender.dataItem();
													kelas =  dataItem.kelas;
													$("#kelas").val(kelas);
													$("#idkelas").val(dataItem.idkelas);

												}
									});
									
									
									$("#idsikap").kendoMultiColumnComboBox({
										dataTextField: "sikap",
										dataValueField: "idsikap",
										height: 400,
										columns: [
											
											{ field: "kategori", title: "Kategori" },
											{ field: "sikap", title: "Sikap" },
											{ field: "bobot", title: "Bobot", width: 55 },
											
										],
										footerTemplate: 'Total #: instance.dataSource.total() # items found',
										filter: "contains",
										filterFields: ["sikap", "bobot"],
										dataSource: {
											
											transport: {
												read: {
														contentType: "application/json; charset=utf-8",
														dataType: "json",
														type: 'post',
														url: '<?php echo base_url(); ?>cnilaisikap/datasikap',
													}
												
											}
										},
										change: function (e) {
													var dataItem = e.sender.dataItem();
													bobot =  dataItem.bobot;
													$("#bobot").val(bobot);

												}
									});
								
						
						});
						  
						
								
					
								
						
						
						
						
						  </script>
                           

                <!-- form start -->
                  <div id="content" class="">
            <!-- content starts -->
     <div>

          <ul class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>admin">Home</a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>cnilaisikap/tampil">Nilai Sikap</a>
            </li>
            <li>
                <a href="#">Edit</a>
            </li>
        </ul>
    </div>

    <div class=" row"  style="margin-top:-18px">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-user"></i> Edit Nilai Sikap</h2>

        <div class="box-icon">
            
            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a>
            <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
        </div>
    </div>
    <div class="box-content">
                <!-- form start -->
 <div  id="groupinput" class="form-group" style="overflow:auto; margin:0 0 10px 0;"> 
 
 <!-- batas form spkl-->
  				<div class="box-header well" data-original-title="" style="margin-right:4px">
                        <h2><i class="glyphicon glyphicon-user"></i> Nilai Sikap </h2>
                
                        <div class="box-icon">
                            
                            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a>
                            <!--<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                        </div>
                    </div>
                    <div class="box-content">
                   <form id="form2" name="form2" method="" action=""  >
     
    <!-- batas form Ijin-->
                    
                    
                   
                       
                         
   <!-- batas form-->
                         
    					
                     <label for="exampleInputEmail1">Tanggal  </label>
                      <input type="text" class="form-control" name="tgl"  id="tgl" placeholder="Tanggal "  title="date"/>
                      <label for="exampleInputEmail1">Pengawas</label>
                      <input type="text" class="form-control" name="nmkaryawan" id="nmkaryawan"   placeholder="Pengawas" disabled="disabled"  />
                
                      <label for="exampleInputEmail1">Peserta</label>
                      <input type="text" class="form-control " style="width:100%" name="idpeserta" id="idpeserta"   placeholder="Peserta" />
                      <label for="exampleInputEmail1">Kelas</label>
                      <input type="text" class="form-control" name="kelas" id="kelas"   placeholder="Kelas" disabled="disabled"/>
     				  <label for="exampleInputEmail1">Sikap</label>
                      <input type="text" class="form-control " style="width:100%" name="idsikap" id="idsikap"   placeholder="Sikap" />
                      <label for="exampleInputEmail1">Nilai</label>
                      <input type="text" class="form-control" name="bobot" id="bobot"   placeholder="Nilai" disabled="disabled"/>
     
                      
               
                 
                 
                  </div>
                  
                     <!-- batas form List Karyawan-->
                
                   
                    
                      <!-- batas form List Karyawan-->
                  </div>

                  
                 <input type="text" name="id" id="idpengajar" style="display:none"  >
                   <input type="text" class="form-control" name="idkelas" id="idkelas"   placeholder="idkelas" disabled="disabled" style="display:none"/>
                  <a href="<?php echo base_url(); ?>cnilaisikap/tampil" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Batal</a>
                  <button type="button" name="simpan" id="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                 
<a href="<?php echo base_url(); ?>cnilaisikap/tambah_nilaisikap" class="btn btn-danger"><i class="fa fa-retweet"></i> Add</a>
                </form>

               
 </div>

            </div>
        </div>
    </div>
    </div>
 