 <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid">
    <!-- Content Header (Page header) -->
    <div class="container" >
     
          <div class="col">
            <h1 class="m-0">DASHBOARD SALES & MARKETING</h1>
          
        </div><!-- /.row -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
<div class="container">
                <!-- /.row -->
        <!-- Main row -->

     <!--   <main class="container border"> -->

                <div class="row">
                  <div class="col col-xs-12 py-2">
                    <div>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Filter
                    </button>
                  </div>
                    <div class="collapse border" id="collapseExample">
                      <div class="card card-body">
                          <!--form input filter-->
                          <?php $this->load->view('admin/forminputfilter_sales_marketing')?>

                      </div>
                    </div>
                  </div>

                </div>

                <br>

                <div class="row" style="background-color:#FFE1AF">
                      <center> Dialy Monitor Sales Head </center>
                </div>

                <div class="row"/>

                    <div class="col-md-6 col-xs-12 py-2">

                                    <div class="card">
                                      <div class="card-header">
                                        <h3 class="card-title">
                                          <i class="fas fa-chart-line mr-1"></i>
                                            DO
                                        </h3>
                                        <div class="card-tools">
                                          
                                        </div>
                                      </div><!-- /.card-header -->
                                      <div class="card-body">
                                        <div class="tab-content p-0">
                                          <!-- Morris chart - Sales -->
                                          <div class="chart tab-pane active" id="revenue-chart"
                                               style="position: relative; height: 250px; width: 100%px;">
                                               <?php $this->load->view('admin/chart/dialy/chart')?>
                                           </div>
                                          
                                        </div>
                                      </div><!-- /.card-body -->
                                    </div>

                          </div>

                          <div class="col-md-6 col-xs-12 py-2">
                                
                                  <div class="card">
                                      <div class="card-header">
                                        <h3 class="card-title">
                                          <i class="fas fa-chart-line mr-1"></i>
                                          SPK
                                        </h3>
                                        <div class="card-tools">
                                          
                                        </div>
                                      </div><!-- /.card-header -->
                                      <div class="card-body">
                                        <div class="tab-content p-0">
                                          <!-- Morris chart - Sales -->
                                          <div class="chart tab-pane active" id="revenue-chart"
                                               style="position: relative; height: 250px; width: 100%;">
                                               <?php $this->load->view('admin/chart/dialy/chart1')?>
                                           </div>
                                          
                                        </div>
                                      </div><!-- /.card-body -->
                                    </div>

                          </div>

                          <div class="col-md-6 col-xs-12 py-2">
                                    
                                    <div class="card">
                                      <div class="card-header">
                                        <h3 class="card-title">
                                          <i class="fas fa-chart-line mr-1"></i>
                                            PROSPEK
                                        </h3>
                                        <div class="card-tools">
                                          
                                        </div>
                                      </div><!-- /.card-header -->
                                      <div class="card-body">
                                        <div class="tab-content p-0">
                                          <!-- Morris chart - Sales -->
                                          <div class="chart tab-pane active" id="revenue-chart"
                                               style="position: relative; height: 250px; width: 100%;">
                                               <?php $this->load->view('admin/chart/dialy/chart3')?>
                                           </div>
                                          
                                        </div>
                                      </div><!-- /.card-body -->
                                    </div>

                          </div>

                          <div class="col-md-6 col-xs-12 py-2">
                                    
                                  <div class="card">
                                      <div class="card-header">
                                        <h3 class="card-title">
                                          <i class="fas fa-chart-line mr-1"></i>
                                            HOT PROSPEK
                                        </h3>
                                        <div class="card-tools">
                                          
                                        </div>
                                      </div><!-- /.card-header -->
                                      <div class="card-body">
                                        <div class="tab-content p-0">
                                          <!-- Morris chart - Sales -->
                                          <div class="chart tab-pane active" id="revenue-chart"
                                               style="position: relative; height: 250px; width: 100%;">
                                               <?php $this->load->view('admin/chart/dialy/chart4')?>
                                           </div>
                                          
                                        </div>
                                      </div><!-- /.card-body -->
                                    </div>


                          </div>

                          <div class="col-md-6 col-xs-12 py-2">

                                    <div class="card">
                                      <div class="card-header">
                                        <h3 class="card-title">
                                          <i class="fas fa-chart-pie mr-1"></i>
                                           Inquiry
                                        </h3>
                                        <div class="card-tools">
                                          
                                        </div>
                                      </div><!-- /.card-header -->
                                      <div class="card-body">
                                        <div class="tab-content p-0">
                                          <!-- Morris chart - Sales -->
                                          <div class="chart tab-pane active" id="revenue-chart"
                                               style="position: relative; height: 250px; width: 100%px;">
                                               <?php $this->load->view('admin/chart/dialy/chart5')?>
                                           </div>
                                          
                                        </div>
                                      </div><!-- /.card-body -->
                                    </div>

                          </div>

                          <div class="col-md-6 col-xs-12 py-2">

                                    <div class="card">
                                      <div class="card-header">
                                        <h3 class="card-title">
                                          <i class="fas fa-chart-line mr-1"></i>
                                            Inquiry LOST
                                        </h3>
                                        <div class="card-tools">
                                          
                                        </div>
                                      </div><!-- /.card-header -->
                                      <div class="card-body">
                                        <div class="tab-content p-0">
                                          <!-- Morris chart - Sales -->
                                          <div class="chart tab-pane active" id="revenue-chart"
                                               style="position: relative; height: 250px; width: 100%px;">
                                               <?php $this->load->view('admin/chart/dialy/chart6')?>
                                           </div>
                                          
                                        </div>
                                      </div><!-- /.card-body -->
                                    </div>

                          </div>





               </div>



              
                <br>
                <div class="row" style="background-color:#FFE1AF">
                      <center> inquiry by Last Progress </center>
                </div>
                <br>
                <div class="row">
                   <div class="col-md-6">
                     
                         <div class="card">
                                      <div class="card-header">
                                        <h3 class="card-title">
                                          <i class="fas fa-chart-line mr-1"></i>
                                          Inquiry M1-M1
                                        </h3>
                                        <div class="card-tools">
                                          
                                        </div>
                                      </div><!-- /.card-header -->
                                      <div class="card-body">
                                        <div class="tab-content p-0">
                                          <!-- Morris chart - Sales -->
                                          <div class="chart tab-pane active" id="revenue-chart"
                                               style="position: relative; height: 250px; width: 100%px;">
                                               <?php $this->load->view('admin/chart/sales&marketing/inquiry_vs_m1-m1')?>
                                           </div>
                                        
                                          
                                        </div>
                                      </div><!-- /.card-body -->
                                    </div>            


                   </div>

                   <div class="col-md-6">
                        <div class="card">
                                      <div class="card-header">
                                        <h3 class="card-title">
                                          <i class="fas fa-chart-line mr-1"></i>
                                          Growth %
                                        </h3>
                                        <div class="card-tools">
                                          
                                        </div>
                                      </div><!-- /.card-header -->
                                      <div class="card-body">
                                        <div class="tab-content p-0">
                                          <!-- Morris chart - Sales -->
                                          <div class="chart tab-pane active" id="revenue-chart"
                                               style="position: relative; height: 250px; width: 100%px;">
                                               <?php $this->load->view('admin/chart/sales&marketing/inquiry_growth')?>
                                           </div>
                                        
                                          
                                        </div>
                                      </div><!-- /.card-body -->
                                    </div>            

                   </div>
                      
                </div> 

                <br>
                <div class="row" style="background-color:#FFE1AF">
                      <center> inquiry by Last Progress </center>
                </div>
                <br>
                <div class="row">
                          

                              <?php $this->load->view('admin/chart/sales&marketing/inquiry_by_last_progress')?>
                                          
                </div> 

                <br>
                <div class="row" style="background-color:#FFE1AF">
                      <center> inquiry by Day </center>
                </div>
                <br>
                <div id="inquiry_day_div" class="row">
                          
                              <?php $this->load->view('admin/chart/sales&marketing/inquiry_day')?>
                                          
                </div> 

                <br>
                <div class="row" style="background-color:#FFE1AF;">
                      <center> Countribution Source </center>
                </div>
                <br>
                <div class="row"  style="height:250px;">
                          
                              <?php $this->load->view('admin/chart/sales&marketing/countribution_source')?>
                                          
                </div> 


                 <br>
                <div class="row" style="background-color:#FFE1AF">
                      <center> Success Ratio </center>
                </div>
                <br>
                <div class="row">
                          

                              <?php $this->load->view('admin/chart/sales&marketing/succesRatio')?>
                                          
                </div> 

                <div class="row" style="background-color:#FFE1AF">
                      <center> Analisa Paymant Leasing </center>
                </div>
                <br>
                <div class="row">
                          

                              <?php $this->load->view('admin/chart/sales&marketing/paymentleasing')?>
                                          
                </div> 



                </div>






          </div>
                              
    <!--   </main> -->





    </div>

      </div>
    

          
       
          
    <!-- /.content -->


