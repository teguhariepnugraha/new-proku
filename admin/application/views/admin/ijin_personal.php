
	<script type="text/javascript" class="init">

		
		$(document).ready(function() {

	
			$('#grid').DataTable( {
			
				scrollY:        '50vh',
				"bFilter": false,
				"bLengthChange": false,
				"aLengthMenu": [
									[25, 50, 100, 200, -1],
									[25, 50, 100, 200, "All"]
								],
				iDisplayLength: -1,
				scrollX: true,
				scrollCollapse: true
			} );
			$('#grid1').DataTable( {
			
				scrollY:        '50vh',
				"bFilter": false,
				"bLengthChange": false,
				"aLengthMenu": [
									[25, 50, 100, 200, -1],
									[25, 50, 100, 200, "All"]
								],
				iDisplayLength: -1,
				scrollX: true,
				scrollCollapse: true
			} );
			
			var idjabatan = '<?php echo $this->session->userdata('idjabatan'); ?>';
			if (idjabatan>16)
			{
				$("#persetujuan_ijin").hide();
				}
			if (idjabatan==16)
			{
				$("table tr th:nth-child(7), table tr td:nth-child(7)").show();
				$("table tr th:nth-child(8), table tr td:nth-child(8)").hide();
				$("table tr th:nth-child(9), table tr td:nth-child(9)").hide();
				$("table tr th:nth-child(10), table tr td:nth-child(10)").hide();
			}
			if (idjabatan==15)
			{
				$("table tr th:nth-child(7), table tr td:nth-child(7)").show();
				$("table tr th:nth-child(8), table tr td:nth-child(8)").show();
				$("table tr th:nth-child(9), table tr td:nth-child(9)").hide();
				$("table tr th:nth-child(10), table tr td:nth-child(10)").hide();
			}
			if (idjabatan==14)
			{
				$("table tr th:nth-child(7), table tr td:nth-child(7)").show();
				$("table tr th:nth-child(8), table tr td:nth-child(8)").show();
				$("table tr th:nth-child(9), table tr td:nth-child(9)").show();
				$("table tr th:nth-child(10), table tr td:nth-child(10)").hide();
			}
			if (idjabatan<=13 )
			{
				$("table tr th:nth-child(7), table tr td:nth-child(7)").show();
				$("table tr th:nth-child(8), table tr td:nth-child(8)").show();
				$("table tr th:nth-child(9), table tr td:nth-child(9)").show();
				$("table tr th:nth-child(10), table tr td:nth-child(10)").show();
			}
			if (idjabatan>=17 )
			{
				$("table tr th:nth-child(7), table tr td:nth-child(7)").hide();
				$("table tr th:nth-child(8), table tr td:nth-child(8)").hide();
				$("table tr th:nth-child(9), table tr td:nth-child(9)").hide();
				$("table tr th:nth-child(10), table tr td:nth-child(10)").hide();
			}
			
		} );
		
	</script>
<?php /*?> <div id="content" class="">
            <!-- content starts -->
     <div>
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>admin_personal">Home</a>
            </li>
            <li>
                <a href="#">Ijin Personal</a>
            </li>
        </ul>
    </div>

 	<div class=" row"  style="margin-top:-18px">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-user"></i> Ijin Personal</h2>

       
    </div>
    <div class="box-content">
		

                
                  <div class="box-tools" style="float:right">
                     <form id="form2" name="form2" method="POST" action="<?php echo base_url();?>cijin_personal/tampil"  >
      
                    <div class="input-group" style="width: 150px; margin-top:0px; padding-right:-10px">
                      <span class="input-group" style="width: 150px; margin-top:0px; padding-right:-10px">
                      


                      <input type="text" name="table_search"  class="form-control input-sm pull-right" placeholder="Search"  />
                      
                       
                      </span>
                      <div class="input-group-btn" >
                       <button id="btnsrch"  class="btn btn-sm btn-default"><i class="fa fa-search"></i> </button>
                        
                      </div>
                      
                    </div>
                    
                  </div>
                  
              
 				<div style="width:100px; margin-top:-20px" >
                  <h3 >
                  	<a href="<?php echo base_url(); ?>cijin_personal/tambah_ijin" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-edit"></i> Tambah</a>
                  </h3>
                   </div><!-- /.box-header -->
</form>
                
<table id="grid2" class="display nowrap" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>No</th>
        <th>Tanggal</th>
  
        <th>Ijin</th>
        <th>Tanggal Keluar</th>
        <th>Tanggal Kembali</th>
        <th>Keterangan</th>
       

        <th><div align="center">Action</div></th>
    </tr>
    </thead>
    <tbody>
    <?php  
       $no = 1;
        foreach ($data as $lihat):
     ?>
    <tr>
      <td><?php echo $no++ ?></td>
      <td><?php echo $lihat->tgl ?></td>

       <td><?php echo $lihat->nama_perijinan ?></td>
      <td><?php echo $lihat->tgl_keluar ?></td>
     <td><?php echo $lihat->tgl_kembali ?></td>
     <td><?php echo $lihat->ket ?></td>


        <td class="center">

            <div align="right"><a class="btn btn-info" href="<?php echo base_url(); ?>cijin_personal/editijin/<?php echo $lihat->idijin?>">
                  <i class="glyphicon glyphicon-edit icon-white"></i>
                  </a>
                <a class="btn btn-danger" href="<?php echo base_url(); ?>cijin_personal/hapusijin/<?php echo $lihat->idijin?>">
                  <i class="glyphicon glyphicon-trash icon-white"></i>
                             </a>                </div></td>
                
    </tr>
     <?php endforeach ?>
    </tbody>
    </table>
                    

                    


               
    <!--/span-->

<!--/row-->
<!--/row-->
<!-- content ends -->
        </div>



            </div>
        </div>
    </div><?php */?>
    
   
     
     
    <!--/span-->
 <div class=" row"  >
<div class="box col-md-12"  >
        <div class="box-inner" >
            <div class="box-header well" data-original-title="" >
                <h2><i class="glyphicon glyphicon-user"></i> Data Ijin</h2>

                <div class="box-icon">
                    
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    
                </div>
            </div>
            <div class="box-content" >
               
        
  
    
    <!--/span-->


           
            <div class="box-content buttons" >
						<div id="wrapper">
                            <div >
                                <div class="row">
                                
                                   
                    
        						<div class="col-sm-0" style="margin-bottom:10px;margin-left:5px; margin-top:-10px; margin-right:5px">
                                        <div class="box-inner" >
                                            <div class="box-header well" data-original-title="">
                                                <h2><i class="glyphicon glyphicon-edit"></i>  List Ijin</h2>
                                
                                                <div class="box-icon">
                                                    
                                                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                                            class="glyphicon glyphicon-chevron-up"></i></a>
                                                   
                                                </div>
                                            </div>
                                           <div class="box-content buttons" >
                                                         
                                                         <div class="col-sm-0" >
                                                            <div class="box-inner"  >
                                                                <div style="margin-right:10px">
                                                                
                                                               <!-- batas ijin-->
                                                                 
   <div class="box-content">
		

                
                  <div class="box-tools" style="float:right">
                     <form id="form2" name="form2" method="POST" action="<?php echo base_url();?>cijin_personal/tampil"  >
      
                    <div class="input-group" style="width: 150px; margin-top:0px; padding-right:-10px">
                      <span class="input-group" style="width: 150px; margin-top:0px; padding-right:-10px">
                      


                      <input type="text" name="table_search"  class="form-control input-sm pull-right" placeholder="Search"  />
                      
                       
                      </span>
                      <div class="input-group-btn" >
                       <button id="btnsrch"  class="btn btn-sm btn-default"><i class="fa fa-search"></i> </button>
                        
                      </div>
                      
                    </div>
                    
                  </div>
                  
              
 				<div style="width:100px; margin-top:-20px" >
                  <h3 >
                  	<a href="<?php echo base_url(); ?>cijin_personal/tambah_ijin" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-edit"></i> Tambah</a>
                  </h3>
                   </div><!-- /.box-header -->
</form>
                
<table id="grid" class="display nowrap" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>No</th>
        <th>Tanggal</th>
  
        <th>Ijin</th>
        <th>Tanggal Keluar</th>
        <th>Tanggal Kembali</th>
        <th>Keterangan</th>
       

        <th><div align="center">Action</div></th>
    </tr>
    </thead>
    <tbody>
    <?php  
       $no = 1;
        foreach ($data as $lihat):
     ?>
    <tr>
      <td><?php echo $no++ ?></td>
      <td><?php echo $lihat->tgl ?></td>

       <td><?php echo $lihat->nama_perijinan ?></td>
      <td><?php echo $lihat->tgl_keluar ?></td>
     <td><?php echo $lihat->tgl_kembali ?></td>
     <td><?php echo $lihat->ket ?></td>


        <td class="center">

            <div align="right"><a class="btn btn-info" href="<?php echo base_url(); ?>cijin_personal/editijin/<?php echo $lihat->idijin?>">
                  <i class="glyphicon glyphicon-edit icon-white"></i>
                  </a>
                <a class="btn btn-danger" href="<?php echo base_url(); ?>cijin_personal/hapusijin/<?php echo $lihat->idijin?>">
                  <i class="glyphicon glyphicon-trash icon-white"></i>
                             </a>                </div></td>
                
    </tr>
     <?php endforeach ?>
    </tbody>
    </table>
                    

                    


               
    <!--/span-->

<!--/row-->
<!--/row-->
<!-- content ends -->
        </div>

 <!-- batas ijin-->

                                                                </div>
                                                            </div>
                                                        </div>
                                             </div>
      
                                        </div>
                                    
                          		</div>
      
                            
                           <!--batas-->
                
            
                        <div class="col-sm-0" id = "persetujuan_ijin"style="margin-bottom:10px;margin-left:5px; margin-top:10px; margin-right:5px">
                                        <div class="box-inner" >
                                            <div class="box-header well" data-original-title="">
                                                <h2><i class="glyphicon glyphicon-edit"></i> Persetujuan Ijin</h2>
                                
                                                <div class="box-icon">
                                                    
                                                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                                            class="glyphicon glyphicon-chevron-up"></i></a>
                                                   
                                                </div>
                                            </div>
                                           <div class="box-content buttons" >
                                                         
                                                         <div class="col-sm-0" >
                                                            <div class="box-inner"  >
                                                            <div class="box-content">
                                                                <div style="height:275px; margin-right:10px">
                                                                 
    <table id="grid1" class="display nowrap" cellspacing="0" width="100%">
                                                                <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Karyawan</th>
                                                                    <th>Jumlah Cuti</th>
                                                                    <th>Tanggal Keluar</th>
                                                                    <th>Tanggal Kembali</th>
                                                                    <th>Jabatan</th>
                                                                    <th><div align="center">Karu</div></th>
                                                                    <th><div align="center">Kaur</div></th>
                                                                     <th><div align="center">Kabag</div></th>
                                                                    <th><div align="center">Kadep</div></th>
                                                                     <th><div align="center">Action</div></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php  
							   										 $no = 1;
																	foreach ($ijinpersonal as $lihat):
																 ?>
                                                                <tr>
                                                                  <td><?php echo $no++ ?></td>
                                                                  <td><?php echo $lihat->nama ?></td>
                                                                  <td><?php echo $lihat->jmlcuti ?></td>
                                                                   <td><?php echo $lihat->tgl_keluar ?></td>
                                                                  <td><?php echo $lihat->tgl_kembali?></td>
                                                                 <td><?php echo $lihat->jabatan ?></td>
                                                            
                                                            
                                                                 <td class="center">
                                                               		 <?php echo $lihat->ket_karu ?>
                                                                 
                                                                  </td>
                                                                   <td class="center">
                                                            		<?php echo $lihat->ket_kaur ?>
                                                                   

                                                                  </td>
                                                                  <td class="center">
                                                            		<?php echo $lihat->ket_kabag ?>
                                                                   
                                                                  </td>
                                                                  <td class="center">
                                                            		<?php echo $lihat->ket_kadep ?>
                                                                   
                                                                  </td>
                                                                   <td class="center">
                                                           
                                                                   <div id="action" align="right"> 
                                                                   <?php
																   $jabatan =  $lihat->jabatan;
																   $idjabatan =  $this->session->userdata('idjabatan');
																   if ($jabatan == 'Pelaksana' and ($idjabatan >=15 and $idjabatan <= 17))
																   { 
																   echo
                                                           			"<a class='btn btn-info' href='accijin/$lihat->idijin?acc=OK&flag='>
                                                                       		<i class='glyphicon glyphicon-ok icon-white'></i>
                                                                       </a>
                                                                       <a class='btn btn-danger' href='accijin/$lihat->idijin?acc=Tolak&flag=2'>
                                                                              <i class='glyphicon glyphicon-trash icon-white'></i>
                                                                        </a>" ;
																   }
																   elseif ($jabatan == 'Pelaksana' and ($idjabatan ==14 || $idjabatan ==13))
																   { 
																   echo
                                                           			"<a class='btn btn-info' href='accijin/$lihat->idijin?acc=OK&flag=1'>
                                                                       		<i class='glyphicon glyphicon-ok icon-white'></i>
                                                                       </a>
                                                                       <a class='btn btn-danger' href='accijin/$lihat->idijin?acc=Tolak&flag=2'>
                                                                              <i class='glyphicon glyphicon-trash icon-white'></i>
                                                                        </a>" ;
																   }
																   elseif ($jabatan == 'Pelaksana' and ($idjabatan ==14))
																   { 
																   echo
                                                           			"<a class='btn btn-info' href='accijin/$lihat->idijin?acc=OK&flag='>
                                                                       		<i class='glyphicon glyphicon-ok icon-white'></i>
                                                                       </a>
                                                                       <a class='btn btn-danger' href='accijin/$lihat->idijin?acc=Tolak&flag=2'>
                                                                              <i class='glyphicon glyphicon-trash icon-white'></i>
                                                                        </a>" ;
																   }
																    elseif ($jabatan == 'Pelaksana' and $idjabatan == 13 )
																   { 
																   echo
                                                           			"<a class='btn btn-info' href='accijin/$lihat->idijin?acc=OK&flag=1'>
                                                                       		<i class='glyphicon glyphicon-ok icon-white'></i>
                                                                       </a>
                                                                       <a class='btn btn-danger' href='accijin/$lihat->idijin?acc=Tolak&flag=2'>
                                                                              <i class='glyphicon glyphicon-trash icon-white'></i>
                                                                        </a>" ;
																   }
																   
																    elseif ($jabatan == 'Kepala Regu' and $idjabatan == 15 )
																   { 
																   echo
                                                           			"<a class='btn btn-info' href='accijin/$lihat->idijin?acc=OK&flag='>
                                                                       		<i class='glyphicon glyphicon-ok icon-white'></i>
                                                                       </a>
                                                                       <a class='btn btn-danger' href='accijin/$lihat->idijin?acc=Tolak&flag=2'>
                                                                              <i class='glyphicon glyphicon-trash icon-white'></i>
                                                                        </a>" ;
																   }
																    elseif ($jabatan == 'Kepala Urusan' and $idjabatan == 14 )
																   { 
																   echo
                                                           			"<a class='btn btn-info' href='accijin/$lihat->idijin?acc=OK&flag='>
                                                                       		<i class='glyphicon glyphicon-ok icon-white'></i>
                                                                       </a>
                                                                       <a class='btn btn-danger' href='accijin/$lihat->idijin?acc=Tolak&flag=2'>
                                                                              <i class='glyphicon glyphicon-trash icon-white'></i>
                                                                        </a>" ;
																   }
																    elseif ($jabatan == 'Kepala Urusan' and $idjabatan == 13 )
																   { 
																   echo
                                                           			"<a class='btn btn-info' href='accijin/$lihat->idijin?acc=OK&flag=1'>
                                                                       		<i class='glyphicon glyphicon-ok icon-white'></i>
                                                                       </a>
                                                                       <a class='btn btn-danger' href='accijin/$lihat->idijin?acc=Tolak&flag=2'>
                                                                              <i class='glyphicon glyphicon-trash icon-white'></i>
                                                                        </a>" ;
																   }
																		?>               
                                                                     </div>
																	 
                                                                  </td>
                                                                            
                                                                </tr>
                                                                 <?php endforeach ?>
                                                                </tbody>
                                                                </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                             </div>
      
                                        </div>
                                        </div>
                                    
                          		</div>
                            </div>
             
                            
                          <!--  batas-->
         
		
    <!--/span-->
</div><!--/row-->
                   
</div>
<!--/row-->
<!--/row-->
<!-- content ends -->
        </div>



            </div>
        </div>
        
        
        
    </div>
    
    </div>
    
                      
  </div>

               


     

      
