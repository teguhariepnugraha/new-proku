
  <style>
  .text-right{
    text-align:center;
  }
    
  </style>

	
<script type="text/javascript">
 var idpeserta;
 var nik1;
 var flag;
 var arrangsuran = [];

					$(document).ready(function() {
						
							 var hgt;
							 var frm;
							 var wnd;
							
							
							 var taskb = document.documentElement.clientHeight;
							 hgt = taskb -174
							 
														 
							 $('#groupinput').height(hgt-76);
							 
							 $('#simpan').on('click', function(){
										update();
										setTimeout(function(){
											updatedtl();
										},500);
										/*$("#simpan").prop("disabled", true);*/
										
							  });
							 $('#add').on('click', function(){
									tambah();
								});
							
							 $( window ).on( "load", function() {
								 showfield();
								 
								 
								setTimeout(function(){
											dataangsuran();
											creategrid();
											    idpeserta = $("#idpeserta").val();
												let customerMultiColumn = $('#idpeserta').data("kendoMultiColumnComboBox");
												customerMultiColumn.select(function(dataItem) {
													return dataItem.idpeserta === idpeserta;
												});
										},500);
							});
							
							$("#idpeserta").kendoMultiColumnComboBox({
										dataTextField: "nama",
										dataValueField: "idpeserta",
										height: 400,
										columns: [
											
											{ field: "nik", title: "NIK", width: 100 },
											{ field: "nama", title: "Nama", width: 250 },
											{ field: "piutang", title: "Piutang", width: 100,
											  headerTemplate:"<div  style='text-align:right; '> Piutang </div> ",
									  template:"<div  style='text-align:right;  '> #:piutang# </div> ",width: "100px", },
									  		{ field: "angsuran", title: "Pelunasan", width: 100,
											  headerTemplate:"<div  style='text-align:right; '> Angsuran </div> ",
									  template:"<div  style='text-align:right;  '> #:angsuran# </div> ",width: "100px", },
									  		{ field: "sisa", title: "Sisa", width: 100,
											  headerTemplate:"<div  style='text-align:right; '> Sisa </div> ",
									  template:"<div  style='text-align:right;  '> #:sisa# </div> ",width: "100px", },
									  		
										],
										footerTemplate: 'Total #: instance.dataSource.total() # items found',
										filter: "contains",
										filterFields: ["nik", "nama"],
										dataSource: {
											
											transport: {
												read: {
														contentType: "application/json; charset=utf-8",
														dataType: "json",
														type: 'post',
														url: '<?php echo base_url(); ?>cangsuran/datapeserta',
													}
												
											}
										},
										change: function (e) {
													var dataItem = e.sender.dataItem();
													idpeserta =  dataItem.idpeserta;

												}
									});


						
							function updatedtl(idangsuran) {
								var arr = [];
								var arr1 = [];
								var grid = $("#grid").data("kendoGrid");
								var data = grid.dataSource.data();
								
								$.each(data, function (i, row) {
									
										    var idangsurandtl=data[i]["idangsurandtl"];
											var idangsuran=data[i]["idangsuran"];
										    var idakun=data[i]["idakun"];
											var angsuran=data[i]["angsuran"];
											
											
											 if (parseInt(idangsurandtl) > 0 )
											  {
													   arr.push({
														    idangsurandtl:idangsurandtl,
														  	idangsuran:idangsuran,
															idakun:idakun,
															angsuran:angsuran,
															
														  });
												}
											else 
											  {
												 
													   arr1.push({
														  	idangsuran:idtable,
															idakun:idakun,
															angsuran:angsuran,
															
														  });
												}
												
								 });
										
									var arr= JSON.stringify(arr);	
													$.ajax({
														  url: '<?php echo base_url(); ?>cangsuran/updatedtl',
														  type: 'get',
														  data:{arr:arr},
														  async: false,
														  dataType: "json",
														  success: function(data)
															  {
																												 
															  }
														  
															});
									if (arr1.length > 0)
									{				
									
										var arr1= JSON.stringify(arr1);	
														$.ajax({
																  url: '<?php echo base_url(); ?>cangsuran/simpandtl',
																  type: 'get',
																  data:{arr:arr1},
																  async: false,
																  dataType: "json",
																  success: function(data)
																  {}
																});
																
										}
										
							}
										
										
												
						 });
						  
						
								
					function dataangsuran() {  
						$.ajax({
								 url: '<?php echo base_url(); ?>cangsuran/dataangsuran',
								 type: 'get',
								 data:{},
								 async: false,
								 dataType: "json",
								 success: function(data)
										{
											$.each(data, function(i, item) {
												
                        							arrangsuran.push({
														idangsuran:data[i].idangsuran,
														angsuran:data[i].angsuran
													});			
                  									 		
												 });	
																								 
										}						  
								});	
						}
								
						
						
						function creategrid() {
						  	var dataangsuran=JSON.stringify(arrangsuran);
							
							 $("#grid").kendoGrid({
									toolbar: [{ name: "create", text: "Tambah" } ],
									
									dataSource: {
											transport: {
													read: 
														{
															url: '<?php echo base_url(); ?>cangsuran/tampiledit?idangsuran='+idtable,
															contentType: "application/json; charset=utf-8",
															dataType: "json",
															type: 'post'
														},			
												},
						 					schema: {
											data: "data",
											model: {
												id: "idakun"	,
												fields: {
														idakun: { field: "idakun", defaultValue: 1 },
														qty: { field: "qty", format: "{0:n}", type: "number"},
														harga: { field: "harga", format: "{0:n}", type: "number"},
														total: { field: "total", format: "{0:n}", type: "number"}	
													}
											}
									}},
									
									columns: [
									  { field: "idangsurandtl",hidden:true  },
									  { field: "idangsuran",title:"idangsuran",hidden:true},
									  { field: "idakun",title:"idakun",hidden:true},
									  { field: "akun", 
										title:"Akun",
										width:400,
										values: dataangsuran,
										editor:function(container,options)
										{
											 $('<input data-text-field="akun" data-value-field="idakun" data-bind="value:' + options.field + '"/>')
										    .appendTo(container)
											.kendoMultiColumnComboBox({
												dataTextField: "akun",
												dataValueField: "idakun",
												scrollable: true,
												value: options.model.idakun, // THIS IS THE CHANGE I MADE
												columns: [
												    
													{ field: "kode", title: "Kode",width:65,},
													{ field: "akun", title: "Akun",width:200,},
													{ field: "piutang", title: "Piutang",width:100,
													  headerTemplate:"<div  style='text-align:right; '> Piutang </div> ",
									  template:"<div  style='text-align:right;  '> #:piutang# </div> ",width: "100px",},
									  				{ field: "angsuran", title: "Angsuran",width:100,
													  headerTemplate:"<div  style='text-align:right; '> Angsuran </div> ",
									  template:"<div  style='text-align:right;  '> #:angsuran# </div> ",width: "100px",},
									  				{ field: "sisa", title: "Sisa", width: 100,
											  headerTemplate:"<div  style='text-align:right; '> Sisa </div> ",
									  template:"<div  style='text-align:right;  '> #:sisa# </div> ",width: "100px", }

												],
												footerTemplate: 'Total #: instance.dataSource.total() # items found',
												filter: "contains",
												filterFields: ["kode","akun",  "satuan"],
												dataSource: {
													transport: {
														read: {
															dataType: "json",
															data:{idpeserta:idpeserta},
															url: '<?php echo base_url(); ?>cangsuran/dataangsuran',
														}
													}
												},
												
												change: function (e) {
													
													var dataItem = e.sender.dataItem()
													var grid = $("#grid").data("kendoGrid");
													var dataRows = grid.items();
													var rowIndex = dataRows.index(grid.select());
													if (rowIndex <= 0)
														{
															rowIndex=0;
														}
													var firstItem = $('#grid').data().kendoGrid.dataSource.data()[rowIndex];

    												firstItem["akun"]=dataItem.akun; 
													options.model.set("idakun", dataItem.idakun);
													
													
													
													
													
													
												}
											});
									 }},
									  /*{ field: "masuk",title:"Masuk"},*/
									  
									  { field: "angsuran",title:"Angsuran",
									    editable: false,format: "{0:n}", type: "number", 
									  	headerAttributes: {"class": "table-cell", style: "text-align: center; font-size: 14px"},
										attributes: {"class": "table-cell", style: "text-align: right; font-size: 14px"}},
									   {  command: [{
													name: "details",
													text:"Delete",
													click: function(e) {
														e.preventDefault();
														var tr = $(e.target).closest("tr"); // get the current table row (tr)
														var data = this.dataItem(tr);
														$.ajax({
																  url: '<?php echo base_url(); ?>cangsuran/deletedtl1',
																  type: 'get',
																  data:{idangsurandtl:data.idangsurandtl},
																  async: false,
																  dataType: "json",
																  success: function(data)
																  {
																	  grid.removeRow(tr);
																	  }
																});

														/*alert("Details for: " + data.kdlemburdtl);*/
														
													}
												  }],
   
										  title: "Action",
            							  width: 100,
									   	  headerAttributes: {"class": "table-cell", style: "text-align: center; font-size: 14px"
										  }}
										  
									],
									
									selectable: "cell",
									
									//filterable: true,
									editable: true,
									
									//navigatable: true,
									//pageable: { pageSizes: false },        
								  });
								  var grid = $("#grid").data("kendoGrid");
									
	
						}
						
						  </script>
                           

                <!-- form start -->
                  <div id="content" class="">
            <!-- content starts -->
     <div>

          <ul class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>admin">Home</a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>cangsuran/tampil">Angsuran</a>
            </li>
            <li>
                <a href="#">Edit</a>
            </li>
        </ul>
    </div>

    <div class=" row"  style="margin-top:-18px">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-user"></i> Edit Angsuran</h2>

        <div class="box-icon">
            
            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a>
            <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
        </div>
    </div>
    <div class="box-content">
                <!-- form start -->
 <div  id="groupinput" class="form-group" style="overflow:auto; margin:0 0 10px 0;"> 
 
 <!-- batas form spkl-->
  				<div class="box-header well" data-original-title="" style="margin-right:4px">
                        <h2><i class="glyphicon glyphicon-user"></i> Angsuran </h2>
                
                        <div class="box-icon">
                            
                            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a>
                            <!--<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                        </div>
                    </div>
                    <div class="box-content">
                   <form id="form2" name="form2" method="" action=""  >
     
    <!-- batas form Ijin-->
                    
                    
                   
                       
                         
   <!-- batas form-->
                         
    					
                     <label for="exampleInputEmail1">Tanggal  </label>
                      <input type="text" class="form-control" name="tgl"  id="tgl" placeholder="Tanggal "  title="date"/>
                      <label for="exampleInputEmail1">No. Reg</label>
                      <input type="text" class="form-control" name="nota" id="nota"   placeholder="No. angsuran" disabled="disabled" />
                
                      <label for="exampleInputEmail1">Peserta</label>
                     <input type="text" class="form-control" name="idpeserta" id="idpeserta"   placeholder="Peserta" style="width:100%"  style="alignment-adjust:middle"/>

     
                      
               
                 
                 
                  </div>
                  
                     <!-- batas form List Karyawan-->
                  <div class="box-header well" data-original-title="" style="margin-right:4px">
                        <h2><i class="glyphicon glyphicon-user"></i> Detail angsuran </h2>
                
                        <div class="box-icon" >
                            
                            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a>
                            <!--<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>-->
                        </div>
                    </div>
                    <div class="box-content">
                    	
				
           
		

                    
                    <div id="grid"></div>
                    </div>
                    
                      <!-- batas form List Karyawan-->
                  </div>

                  
                  <input type="hidden" name="id" >
                  <a href="<?php echo base_url(); ?>cangsuran/tampil" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Batal</a>
                  <button type="button" name="simpan" id="simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                 
<a href="<?php echo base_url(); ?>cangsuran/tambah_angsuran" class="btn btn-danger"><i class="fa fa-retweet"></i> Add</a>
                </form>

               
 </div>

            </div>
        </div>
    </div>
    </div>
 