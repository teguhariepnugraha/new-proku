<canvas id="myChart1" style="height:170px"></canvas>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0-rc/dist/chartjs-plugin-datalabels.min.js" integrity="sha256-xnHIGzvvjYxzGnqAsH3nWhaoONgNZ5oifAsNMi48pYA=" crossorigin="anonymous"></script>

<script>
var ctx = document.getElementById('myChart1').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '',
            data: [12, 19, 3, 5, 15, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1,

        }]
    },
    options: {
        scales: {
            xAxes: [{
            gridLines: {
                color: "rgba(0, 0, 0, 0)",
            }
        }],
        yAxes: [{
            gridLines: {
                color: "rgba(0, 0, 0, 0)",
            }   
        }]
        },
        legend:{
            display:false,
        },
           // "animation": {
           //      "duration": 1,
           //    "onComplete": function() {
           //      var chartInstance = this.chart,
           //        ctx = chartInstance.ctx;
 
           //      ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily, Chart.defaults.global.defaultFontColor);
           //      ctx.textAlign = 'center';
           //      ctx.textBaseline = 'bottom';
           //      ctx.textColor='black';
           //       ctx.fillStyle = "black";
 
           //      this.data.datasets.forEach(function(dataset, i) {
           //        var meta = chartInstance.controller.getDatasetMeta(i);
           //        meta.data.forEach(function(bar, index) {
           //          var data = dataset.data[index];
           //          ctx.fillText(data, bar._model.x, bar._model.y - 5);
           //        });
           //      });
           //    }
           //  },
    },
    plugins: [ChartDataLabels],
  options: {
        color:'black',
        plugins:{
            legend:{
                display:false,
            },
             datalabels:{
          color:'black',

          anchor:'end',
          align:'top',
          offset: 1,
           formatter: function(value, context) {
            //console.log(value+" "+context.dataset.backgroundColor);
            //Object.keys(context.chart.data.datasets[context.index]).forEach((prop)=> console.log(prop));
            //color:'blue',
            ChartDataLabels.defaults.color=context.dataset.backgroundColor;
           // console.log(ChartDataLabels.defaults.color);
            return context.dataset.label +":"+value;

           //return context.chart.datasets.label[contex.dataIndex];

        }
        }
        
  }
});
</script>