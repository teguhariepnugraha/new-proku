
   <script type="text/javascript">
 
								 

							
		$(document).ready(function(){ 
		
			var row = 0;
		     $( window ).on( "load", function() {
				$.ajax({
					 url : "./admin/hideshowdashboard",
					 type: "get",
					 data:{},
					 async: true,
					 dataType: "json",
					 success: function(data)
						 {
						  var i;
						  
							for (i = 0; i < data.length; i++)
								{
									$("#"+data[i].filename).show();
								}
						 }
					 });
									
				
			});
			
			$('#grid').DataTable( {
			
				scrollY:        '50vh',
				"bFilter": false,
				"bLengthChange": false,
				"aLengthMenu": [
									[25, 50, 100, 200, -1],
									[25, 50, 100, 200, "All"]
								],
				iDisplayLength: -1,
				scrollX: true,
				scrollCollapse: true
			} );
			var idjabatan = '<?php echo $this->session->userdata('idjabatan'); ?>';
			if (idjabatan==16)
			{
				$("table tr th:nth-child(7), table tr td:nth-child(7)").show();
				$("table tr th:nth-child(8), table tr td:nth-child(8)").hide();
				$("table tr th:nth-child(9), table tr td:nth-child(9)").hide();
				$("table tr th:nth-child(10), table tr td:nth-child(10)").hide();
			}
			if (idjabatan==15)
			{
				$("table tr th:nth-child(7), table tr td:nth-child(7)").show();
				$("table tr th:nth-child(8), table tr td:nth-child(8)").show();
				$("table tr th:nth-child(9), table tr td:nth-child(9)").hide();
				$("table tr th:nth-child(10), table tr td:nth-child(10)").hide();
			}
			if (idjabatan==14)
			{
				$("table tr th:nth-child(7), table tr td:nth-child(7)").show();
				$("table tr th:nth-child(8), table tr td:nth-child(8)").show();
				$("table tr th:nth-child(9), table tr td:nth-child(9)").show();
				$("table tr th:nth-child(10), table tr td:nth-child(10)").hide();
			}
			if (idjabatan<=13 )
			{
				$("table tr th:nth-child(7), table tr td:nth-child(7)").show();
				$("table tr th:nth-child(8), table tr td:nth-child(8)").show();
				$("table tr th:nth-child(9), table tr td:nth-child(9)").show();
				$("table tr th:nth-child(10), table tr td:nth-child(10)").show();
			}
			if (idjabatan>=18 )
			{
				$("table tr th:nth-child(7), table tr td:nth-child(7)").hide();
				$("table tr th:nth-child(8), table tr td:nth-child(8)").hide();
				$("table tr th:nth-child(9), table tr td:nth-child(9)").show();
				$("table tr th:nth-child(10), table tr td:nth-child(10)").show();
			}
			
	
			
			
		});
</script>

    <!-- topbar starts -->
 <div id="content" class="">
            <!-- content starts -->
     <div>
        <ul class="breadcrumb" >
            <li>
                <a href="#">Home</a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>admin_personal">Dashboard</a>
            </li>
        </ul>
    </div>
<div class=" row"  style="margin-top:-18px">
<div class="box col-md-12"  >
        <div class="box-inner" >
            <div class="box-header well" data-original-title="" >
                <h2><i class="glyphicon glyphicon-user"></i> Persetujuan Pengajuan Cuti Karyawan</h2>

                <div class="box-icon">
                    
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    
                </div>
            </div>
            <div class="box-content" >
               

                                                     
                                                     <div class="col-sm-0" style="margin:-5px" >
                                                        <div class="box-inner"  >
                                                            <div style="margin:10px"> 
                                                             <table id="grid" class="display nowrap" cellspacing="0" width="100%">
                                                                <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Karyawan</th>
                                                                    <th>Jumlah Cuti</th>
                                                                    <th>Tanggal Keluar</th>
                                                                    <th>Tanggal Kembali</th>
                                                                    <th>Jabatan</th>
                                                                    <th><div align="center">Karu</div></th>
                                                                    <th><div align="center">Kaur</div></th>
                                                                     <th><div align="center">Kabag</div></th>
                                                                    <th><div align="center">Kadep</div></th>
                                                                     <th><div align="center">Action</div></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php  
							   										 $no = 1;
																	foreach ($data as $lihat):
																 ?>
                                                                <tr>
                                                                  <td><?php echo $no++ ?></td>
                                                                  <td><?php echo $lihat->nama ?></td>
                                                                  <td><?php echo $lihat->jmlcuti ?></td>
                                                                   <td><?php echo $lihat->tgl_keluar ?></td>
                                                                  <td><?php echo $lihat->tgl_kembali?></td>
                                                                 <td><?php echo $lihat->jabatan ?></td>
                                                            
                                                            
                                                                 <td class="center">
                                                               		 <?php echo $lihat->ket_karu ?>
                                                                 
                                                                  </td>
                                                                   <td class="center">
                                                            		<?php echo $lihat->ket_kaur ?>
                                                                   
                                                                  </td>
                                                                  <td class="center">
                                                            		<?php echo $lihat->ket_kabag ?>
                                                                   
                                                                  </td>
                                                                  <td class="center">
                                                            		<?php echo $lihat->ket_kadep ?>
                                                                   
                                                                  </td>
                                                                   <td class="center">
                                                           
                                                                   <div id="action" align="right"> 
                                                                   <?php
																   $jabatan =  $lihat->jabatan;
																   $idjabatan =  $this->session->userdata('idjabatan');
																   if ($jabatan == 'Pelaksana' and ($idjabatan >=15 and $idjabatan <= 17))
																   { 
																   echo
                                                           			"<a class='btn btn-info' href='accijin/$lihat->idijin?acc=OK&flag='>
                                                                       		<i class='glyphicon glyphicon-ok icon-white'></i>
                                                                       </a>
                                                                       <a class='btn btn-danger' href='accijin/$lihat->idijin?acc=Tolak&flag='>
                                                                              <i class='glyphicon glyphicon-trash icon-white'></i>
                                                                        </a>" ;
																   }
																   elseif ($jabatan == 'Pelaksana' and ($idjabatan ==14 || $idjabatan ==13))
																   { 
																   echo
                                                           			"<a class='btn btn-info' href='accijin/$lihat->idijin?acc=OK&flag=1'>
                                                                       		<i class='glyphicon glyphicon-ok icon-white'></i>
                                                                       </a>
                                                                       <a class='btn btn-danger' href='accijin/$lihat->idijin?acc=Tolak&flag=2'>
                                                                              <i class='glyphicon glyphicon-trash icon-white'></i>
                                                                        </a>" ;
																   }
																   elseif ($jabatan == 'Staff' and ($idjabatan ==14))
																   { 
																   echo
                                                           			"<a class='btn btn-info' href='accijin/$lihat->idijin?acc=OK&flag='>
                                                                       		<i class='glyphicon glyphicon-ok icon-white'></i>
                                                                       </a>
                                                                       <a class='btn btn-danger' href='accijin/$lihat->idijin?acc=Tolak&flag='>
                                                                              <i class='glyphicon glyphicon-trash icon-white'></i>
                                                                        </a>" ;
																   }
																    elseif ($jabatan == 'Staff' and $idjabatan == 13 )
																   { 
																   echo
                                                           			"<a class='btn btn-info' href='accijin/$lihat->idijin?acc=OK&flag=1'>
                                                                       		<i class='glyphicon glyphicon-ok icon-white'></i>
                                                                       </a>
                                                                       <a class='btn btn-danger' href='accijin/$lihat->idijin?acc=Tolak&flag=2'>
                                                                              <i class='glyphicon glyphicon-trash icon-white'></i>
                                                                        </a>" ;
																   }
																   
																    elseif ($jabatan == 'Kepala Regu' and $idjabatan <= 15 )
																   { 
																   echo
                                                           			"<a class='btn btn-info' href='accijin/$lihat->idijin?acc=OK'>
                                                                       		<i class='glyphicon glyphicon-ok icon-white'></i>
                                                                       </a>
                                                                       <a class='btn btn-danger' href='accijin/$lihat->idijin?acc=Tolak'>
                                                                              <i class='glyphicon glyphicon-trash icon-white'></i>
                                                                        </a>" ;
																   }
																    elseif ($jabatan == 'Kepala Urusan' and $idjabatan <= 14 )
																   { 
																   echo
                                                           			"<a class='btn btn-info' href='accijin/$lihat->idijin?acc=OK&flag=1'>
                                                                       		<i class='glyphicon glyphicon-ok icon-white'></i>
                                                                       </a>
                                                                       <a class='btn btn-danger' href='accijin/$lihat->idijin?acc=Tolak&flag=0'>
                                                                              <i class='glyphicon glyphicon-trash icon-white'></i>
                                                                        </a>" ;
																   }
																    elseif ($jabatan == 'Kepala Bagian' and $idjabatan <= 13 )
																   { 
																   echo
                                                           			"<a class='btn btn-info' href='accijin/$lihat->idijin?acc=OK&flag=1'>
                                                                       		<i class='glyphicon glyphicon-ok icon-white'></i>
                                                                       </a>
                                                                       <a class='btn btn-danger' href='accijin/$lihat->idijin?acc=Tolak&flag=0'>
                                                                              <i class='glyphicon glyphicon-trash icon-white'></i>
                                                                        </a>" ;
																   }
																		?>               
                                                                     </div>
																	 
                                                                  </td>
                                                                            
                                                                </tr>
                                                                 <?php endforeach ?>
                                                                </tbody>
                                                                </table>

                                                          </div>
                                                        </div>
                                                	</div>

                          	</div>
               
            </div> 
            
        </div>

    </div>
    

        

     
     
    <!--/span-->

		
    <!--/span-->
</div><!--/row-->
                   
</div>
<!--/row-->
<!--/row-->
<!-- content ends -->
        </div>



            </div>
        </div>
        
        
    </div>
