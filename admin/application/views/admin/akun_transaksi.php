
	<script type="text/javascript" class="init">

		 $(document).ready(function() {
			  var taskb = document.documentElement.clientHeight;
			   hgt = taskb -174
			   $('#groupinput').height(hgt-82);

            $("#grid").kendoGrid({
							dataSource: {
							 	transport: {
												read: 													
														{
															contentType: "application/json; charset=utf-8",
															dataType: "json",
															type: 'post',
															url: "<?php echo base_url(); ?>cakuntransaksi/thnajaran",
															
															}		
											},
										 schema: {data: "data"},
										}, 				
                            pageSize: 6,
                            serverPaging: true,
                            serverSorting: true,
                        sortable: true,
                        pageable: true,
                        detailInit: detailInit,
                        /*dataBound: function() {
                            this.expandRow(this.tbody.find("tr.k-master-row").first());
                        },*/
                        columns: [
						 {field: "idakun_transaksi",hidden:true,},
						 {field: "tgl",title: "Tanggal",width: 100, },
						 {field: "thn_ajaran",title: "Tahun Pelajaran", },
						 {field: "total",title: "Total Belanja",
						 format: "{0:n}", type: "number", 
						 headerAttributes: {"class": "table-cell", style: "text-align: Right; font-size: 14px;font-weight: bold"},
						 attributes: { style: "text-align: Right; font-size: 14px"}},
						 {  command: [{
													name: "details",
													text:"Edit",
													click: function(e) {
														e.preventDefault();
														var tr = $(e.target).closest("tr"); // get the current table row (tr)
														var data = this.dataItem(tr);
														window.location.href= "<?php echo base_url(); ?>cakuntransaksi/editakuntransaksi/" + data.thn_ajaran;
														/*alert("Details for: " + data.kdakundtl);*/	
													}
										}],
   
										  title: "Action",
            							  width: 85,
									   	  headerAttributes: {"class": "table-cell", style: "text-align: center; font-size: 14px;font-weight: bold"
										  }
										  
							},
                        ]
                    });
                });
				function detailInit(e) {
				 var thnajaran = e.data.thn_ajaran;
				
                    $("<div/>").appendTo(e.detailCell).kendoGrid({
					
                       dataSource: {
							 transport: {
										read: 
											{

												url: "<?php echo base_url(); ?>cakuntransaksi/akun",
												contentType: "application/json; charset=utf-8",
												
												type: 'get',
												dataType: "json",
												data:{thnajaran:thnajaran}

												}
											},
										 schema: {data: "data"},
										 serverPaging: true,
										 serverSorting: true,
										 serverFiltering: true,
										 pageSize: 10,
										 filter: { field: "idakun_transaksi", operator: "eq", value: e.data.idakun_transaksi }
										}, 
										
                        scrollable: false,
                        sortable: true,
                        pageable:false,
						detailInit: detailInitdtl,
                        dataBound: function() {
                            /*this.expandRow(this.tbody.find("tr.k-master-row").first());*/
                        },
                        columns: [
							{ field: "idakun", hidden:true,headerAttributes: {style: "font-weight: bold"}},
							{ field: "thn_ajaran",hidden:true,title:"Tahun Pelajaran", headerAttributes: {style: "font-weight: bold"}},
							{ field: "kdakun", title:"Kode", width: "110px",headerAttributes: {style: "font-weight: bold"} },
                            { field: "akun", title:"Akun", headerAttributes: {style: "font-weight: bold"}},
							{field: "pagu",title: "Total Pagu",
							 format: "{0:n}", type: "number", 
							 headerAttributes: {"class": "table-cell", style: "text-align: Right; font-size: 14px;font-weight: bold"},
							 attributes: { style: "text-align: Right; font-size: 14px"}}
                          
                        ]
                    });
					
                }
				function detailInitdtl(e) {
				var idakun = e.data.idakun;
				var thnajaran = e.data.thn_ajaran;
				
                    $("<div/>").appendTo(e.detailCell).kendoGrid({
					
                       dataSource: {
							 transport: {
										read: 
											{

												url: "<?php echo base_url(); ?>cakuntransaksi/akundtl",
												contentType: "application/json; charset=utf-8",
												
												type: 'get',
												dataType: "json",
												data:{thnajaran:thnajaran,idakun:idakun}

												}
											},
										 schema: {data: "data"},
										 serverPaging: true,
										 serverSorting: true,
										 serverFiltering: true,
										 pageSize: 10,
										 filter: { field: "idakun_transaksi", operator: "eq", value: e.data.idakun_transaksi }
										}, 
										
                        scrollable: false,
                        sortable: true,
                        pageable:false,
						 /*detailInit: detailInitmanager,*/
                         dataBound: function() {
                            this.expandRow(this.tbody.find("tr.k-master-row").first());
                        },
                        columns: [
							{ field: "idakundtl", hidden:true,headerAttributes: {style: "font-weight: bold"}},
							{ field: "kdakundtl", title:"Kode", width: "110px",headerAttributes: {style: "font-weight: bold"} },
                            { field: "akundtl", title:"Akun", headerAttributes: {style: "font-weight: bold"}},
							{field: "pagu",title: "Pagu",
							 format: "{0:n}", type: "number", 
							 headerAttributes: {"class": "table-cell", style: "text-align: Right; font-size: 14px;font-weight: bold"},
							 attributes: { style: "text-align: Right; font-size: 14px"}},
							
                          
                        ]
                    });
					
                }
	</script>
 <div id="content" class="">
            <!-- content starts -->
     <div>
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo base_url(); ?>admin">Home</a>
            </li>
            <li>
                <a href="#">Akun Transaksi</a>
            </li>
        </ul>
    </div>

 	<div class=" row"  style="margin-top:-18px">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-user"></i> Akun Transaksi</h2>

       
    </div>
    <div class="box-content">
	 	

                
                  <div class="box-tools" style="float:right">
                     <form id="form2" name="form2" method="post" action="<?php echo base_url();?>cakuntransaksi/tampil"  >
      
                    <div class="input-group" style="width: 150px; margin-top:0px; padding-right:-10px">
                      <span class="input-group" style="width: 150px; margin-top:0px; padding-right:-10px">
                      


                      <input type="text" name="table_search" id="table_search"  class="form-control input-sm pull-right" placeholder="Search"  />
                      
                       
                      </span>
                      <div class="input-group-btn" >
                       <button id="btnsrch"  class="btn btn-sm btn-default"><i class="fa fa-search"></i> </button>
                        
                      </div>
                      
                    </div>
                    
                  </div>
                  
              
 				<div style="width:100px; margin-top:-20px" >
                  <h3 >
                  	<a href="<?php echo base_url(); ?>cakuntransaksi/tambah_akuntransaksi" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-edit"></i> Tambah</a>
                  </h3>
                   </div><!-- /.box-header -->
</form>
          <div  id="groupinput" class="form-group" style="overflow:auto; margin:0 0 10px 0;"> 
                
 <div id="grid"></div>
                    
 </div>        
                    


               
    <!--/span-->

<!--/row-->
<!--/row-->
<!-- content ends -->
        </div>



            </div>
        </div>
    </div>
    </div>