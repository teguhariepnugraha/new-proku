 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header border" >
      <div class="container-fluid">
        
            <h1 class="m-0">DAILY MONITOR SALES HEAD</h1>
         
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
                <!-- /.row -->
        <!-- Main row -->

     <!--   <main class="container border"> -->

                <div class="row">
                  <div class="col col-xs-12 py-2">
                    <div>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Filter
                    </button>
                  </div>
                    <div class="collapse" id="collapseExample">
                      <div class="card card-body">
                          <!--form input filter-->
                          <?php $this->load->view('admin/forminputfilter')?>

                      </div>
                    </div>
                  </div>

                </div>

                <div class="row">
                     
                          <div class="col-md-6 col-xs-12 py-2">

                                    <div class="card">
                                      <div class="card-header">
                                        <h3 class="card-title">
                                          <i class="fas fa-chart-line mr-1"></i>
                                          DO
                                        </h3>
                                        <div class="card-tools">
                                          
                                        </div>
                                      </div><!-- /.card-header -->
                                      <div class="card-body">
                                        <div class="tab-content p-0">
                                          <!-- Morris chart - Sales -->
                                          <div class="chart tab-pane active" id="revenue-chart"
                                               style="position: relative; height: 250px; width: 100%px;">
                                               <?php $this->load->view('admin/chart/chart')?>
                                           </div>
                                          
                                        </div>
                                      </div><!-- /.card-body -->
                                    </div>

                          </div>
                          <div class="col-md-6 col-xs-12 py-2">
                                
                                  <div class="card">
                                      <div class="card-header">
                                        <h3 class="card-title">
                                          <i class="fas fa-chart-line mr-1"></i>
                                          SPK
                                        </h3>
                                        <div class="card-tools">
                                          
                                        </div>
                                      </div><!-- /.card-header -->
                                      <div class="card-body">
                                        <div class="tab-content p-0">
                                          <!-- Morris chart - Sales -->
                                          <div class="chart tab-pane active" id="revenue-chart"
                                               style="position: relative; height: 250px; width: 100%;">
                                               <?php $this->load->view('admin/chart/chart1')?>
                                           </div>
                                          
                                        </div>
                                      </div><!-- /.card-body -->
                                    </div>

                          </div>
                          <div class="col-md-6 col-xs-12 py-2">
                                    
                                    <div class="card">
                                      <div class="card-header">
                                        <h3 class="card-title">
                                          <i class="fas fa-chart-line mr-1"></i>
                                            PROSPEK
                                        </h3>
                                        <div class="card-tools">
                                          
                                        </div>
                                      </div><!-- /.card-header -->
                                      <div class="card-body">
                                        <div class="tab-content p-0">
                                          <!-- Morris chart - Sales -->
                                          <div class="chart tab-pane active" id="revenue-chart"
                                               style="position: relative; height: 250px; width: 100%;">
                                               <?php $this->load->view('admin/chart/chart3')?>
                                           </div>
                                          
                                        </div>
                                      </div><!-- /.card-body -->
                                    </div>

                          </div>
                          <div class="col-md-6 col-xs-12 py-2">
                                    
                                  <div class="card">
                                      <div class="card-header">
                                        <h3 class="card-title">
                                          <i class="fas fa-chart-line mr-1"></i>
                                            HOT PROSPEK
                                        </h3>
                                        <div class="card-tools">
                                          
                                        </div>
                                      </div><!-- /.card-header -->
                                      <div class="card-body">
                                        <div class="tab-content p-0">
                                          <!-- Morris chart - Sales -->
                                          <div class="chart tab-pane active" id="revenue-chart"
                                               style="position: relative; height: 250px; width: 100%;">
                                               <?php $this->load->view('admin/chart/chart4')?>
                                           </div>
                                          
                                        </div>
                                      </div><!-- /.card-body -->
                                    </div>


                          </div>
                          <div class="col-md-6 col-xs-12 py-2">

                                    <div class="card">
                                      <div class="card-header">
                                        <h3 class="card-title">
                                          <i class="fas fa-chart-pie mr-1"></i>
                                          KDP
                                        </h3>
                                        <div class="card-tools">
                                          
                                        </div>
                                      </div><!-- /.card-header -->
                                      <div class="card-body">
                                        <div class="tab-content p-0">
                                          <!-- Morris chart - Sales -->
                                          <div class="chart tab-pane active" id="revenue-chart"
                                               style="position: relative; height: 250px; width: 100%px;">
                                               <?php $this->load->view('admin/chart/chart5')?>
                                           </div>
                                          
                                        </div>
                                      </div><!-- /.card-body -->
                                    </div>

                          </div>

                          <div class="col-md-6 col-xs-12 py-2">

                                    <div class="card">
                                      <div class="card-header">
                                        <h3 class="card-title">
                                          <i class="fas fa-chart-line mr-1"></i>
                                          KDP LOST
                                        </h3>
                                        <div class="card-tools">
                                          
                                        </div>
                                      </div><!-- /.card-header -->
                                      <div class="card-body">
                                        <div class="tab-content p-0">
                                          <!-- Morris chart - Sales -->
                                          <div class="chart tab-pane active" id="revenue-chart"
                                               style="position: relative; height: 250px; width: 100%px;">
                                               <?php $this->load->view('admin/chart/chart6')?>
                                           </div>
                                          
                                        </div>
                                      </div><!-- /.card-body -->
                                    </div>

                          </div>

                            

                      
                </div>
          </div>
                              
    <!--   </main> -->







      </div>
     </section>    
    </div>
          
       
          
    <!-- /.content -->


