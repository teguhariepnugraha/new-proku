<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cnilaiharian extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		// $this->load->helper(array('url','form'));
		 $this->load->model('mnilaiharian');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		
		$a['page']	= "nilaiharian";
		
		$this->load->view('admin/index', $a);
	}
	
	function tampilnilai_siswa(){
		
		$a['page']	= "nilaihariansiswa";
		
		$this->load->view('admin/index', $a);
	}
	
	function json() {
	 $field=$this->input->post('field');
       echo $this->mnilaiharian->json($field);
    }
	
	function json_nilaisiswa() {
	 $field=$this->input->post('field');
       echo $this->mnilaiharian->json_nilaisiswa($field);
    }
	

	function tambah_nilaiharian(){
		
		$a['page']	= "nilaiharian/tambah_nilaiharian";
		$this->load->view('admin/index', $a);
	}

	function insertdata(){
		$table =  'tnilaiharian';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		
		$this->db->insert($table, $myjson );
		redirect('cnilaiharian/tampil','refresh');
	}



	function editnilaiharian($id){
		$a['page']	= "nilaiharian/edit_nilaiharian";
		$this->load->view('admin/index', $a, $id);
	}

	function updatedata(){
		$table =   'tnilaiharian';
		$idtable =  'idnilaiharian';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 


	}

	

	function deletedtl1(){
		$idnilaihariandtl = $this->input->get('idnilaihariandtl');
		
		echo $this->mnilaiharian->deletedtl1($idnilaihariandtl);
		
	}
	
	function hapusnilaiharian($id){
		$this->mnilaiharian->hapusnilaiharian($id);
		redirect('cnilaiharian/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->mnilaiharian->getjson();
    }

	
	function urlcmb()
    {

		echo $this->mnilaiharian->url();
    }
	
	function tampiledit(){
		$field =  $this->input->get('idnilaiharian');	
		echo $this->mnilaiharian->tampiledit($field);
	}
	
	
	
	function datakelas(){	
		echo $this->mnilaiharian->datakelas();
	}
	
	function datapengajar(){	
		echo $this->mnilaiharian->datapengajar();
	}
	function datapelajaran(){	
		$string =  $this->input->get('idkelas');	
		echo $this->mnilaiharian->datapelajaran($string);
	}
	function datapeserta(){	
		$string =  $this->input->get('idkelas');
		echo $this->mnilaiharian->datapeserta($string);
	}
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mnilaiharian->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $this->input->get('fields');	
		echo $this->mnilaiharian->get_datapopup($string);
    }
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mnilaiharian->get_headerpopup($string);
    }
	
	function urlautonumber()
    {
		echo $this->mnilaiharian->urlnumber();
    }
	
	function cekidnilaiharian()
	{
		$kdnilaiharian = $this->input->get('kdnilaiharian');
		echo $this->mnilaiharian->idnilaiharian($kdnilaiharian);
	}
	function simpandtl(){
			$data= $this->input->get('arr');
			echo $this->mnilaiharian->simpandtl($data);
			
		
		}
	function updatedtl(){
			$data= $this->input->get('arr');
			 
			echo $this->mnilaiharian->updatedtl($data);
			
		
		}
}

