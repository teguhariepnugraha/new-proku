	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cjadwal extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		// $this->load->helper(array('url','form'));
		 $this->load->model('mjadwal');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		$field =  $this->input->post('table_search');
		$a['data']	= $this->mjadwal->tampil($field)->result_object();
		$a['page']	= "jadwal";
		
		$this->load->view('admin/index', $a);
	}

	function tambah_jadwal(){
		
		$a['page']	= "jadwal/tambah_jadwal";
		$this->load->view('admin/index', $a);
	}

	function insertdata(){
		$table =  'tjadwal';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong);
		$this->db->insert($table, $myjson );
		redirect('cjadwal/tampil','refresh');
	}



	function editjadwal($id){
		$a['page']	= "jadwal/edit_jadwal";
		$this->load->view('admin/index', $a, $id);
	}

	function updatedata(){
		$table =   'tjadwal';
		$idtable =  'idjadwal';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 


	}

	

	function deletedtl1(){
		$idjadwaldtl = $this->input->get('idjadwaldtl');
		
		echo $this->mjadwal->deletedtl1($idjadwaldtl);
		
	}
	
	function hapusjadwal($id){
		$this->mjadwal->hapusjadwal($id);
		redirect('cjadwal/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->mjadwal->getjson();
    }

	
	function urlcmb()
    {

		echo $this->mjadwal->url();
    }
	
	function tampiledit(){
		$field =  $this->input->get('idjadwal');	
		echo $this->mjadwal->tampiledit($field);
	}
	
	function datakelas(){	
		echo $this->mjadwal->datakelas();
	}
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mjadwal->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $this->input->get('fields');	
		echo $this->mjadwal->get_datapopup($string);
    }
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mjadwal->get_headerpopup($string);
    }
	function cekidjadwal()
	{
		$idkaryawan = $this->input->get('idkaryawan');
		echo $this->mjadwal->idjadwal($idkaryawan);
	}
	function simpandtl(){
			$data= $this->input->get('arr');
			echo $this->mjadwal->simpandtl($data);
			
		
		}
	function updatedtl(){
			$data= $this->input->get('arr');
			 
			echo $this->mjadwal->updatedtl($data);
			
		
		}
}

