<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cValueFilter extends CI_Controller {

	public function __construct() {
        parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
        //$this->load->library('datatables');
        $this->load->model('model_dms/valueFilter');
    }

    public function getAllBranch(){
    	 return print_r(json_encode($this->valueFilter->getAllBranch()));
    }

    public function getAllTipe(){
    	 return print_r(json_encode($this->valueFilter->getAllTipe()));
    }

    public function getAllModel(){
    	 return print_r(json_encode($this->valueFilter->getAllModel()));
    }

    public function getAllModelFilter(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getAllModelFilter($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function getValueOfDo(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getValueOfDO($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function getValueOfSPK(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getValueOfSPK($bulan)));
    	 
    	 //return print_r($whereMonth);
    }


    public function getValueOfProspek(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getValueOfProspek($bulan)));
    	 
    	 //return print_r($whereMonth);
    }


    public function getValueOfHotProspek(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getValueOfHotProspek($bulan)));
    	 
    	 //return print_r($whereMonth);
    }


    public function getValueOfInquiry(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getValueOfInquiry($bulan)));
    	 
    	 //return print_r($whereMonth);
    }


     public function getValueOfInquiryLost(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getValueOfInquiryLost($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function getValueOfContributionSource(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getValueOfContributionSource($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function getValueOfInquiryCaraBayar(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getValueOfInquiryCaraBayar($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function getValueOfInquiryLeasing(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getValueOfInquiryLeasing($bulan)));
    	 
    	 //return print_r($whereMonth);
    }


    public function getValueOfInquiryToSpk(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getValueOfInquiryToSpk($bulan)));
    	 
    	 //return print_r($whereMonth);
    }


    public function getValueOfInquiryToDO(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getValueOfInquiryToDO($bulan)));
    	 
    	 //return print_r($whereMonth);
    }


    public function inquiryToProspek(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->inquiryToProspek($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function inquiryLostToProspek(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->inquiryLostToProspek($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function inquiryLostToDO(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->inquiryLostToDO($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function inquiryLostToSPK(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->inquiryLostToSPK($bulan)));
    	 
    	 //return print_r($whereMonth);
    }


    public function getValueOfGrowth(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getValueOfGrowth($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function getAllSalesmanHead(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getAllSalesmanHead($bulan)));
    	 
    	 //return print_r($whereMonth);
    }


    public function getAllSalesman(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getAllSalesman($bulan)));
    	 
    	 //return print_r($whereMonth);
    }


    public function inquiryToInquiryLost(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->inquiryToInquiryLost($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function DOHarian(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->DOHarian($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function SPKHarian(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->SPKHarian($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function fakturPajak(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->fakturPajak($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function salesmanTerbaik(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->salesmanTerbaik($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

     public function salesheadTerbaik(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->salesheadTerbaik($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

     public function salescounterTerbaik(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->salescounterTerbaik($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function FakturHarian(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->FakturHarian($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function getalldatabranchciawi(){
    	 $bulan = $_GET['branchcode'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getalldatabranchciawi($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function getalldatabranchtargetdociawi(){
         $bulan = $_GET['branchcode'];
         //return print_r($bulan);
         return print_r(json_encode($this->valueFilter->getalldatabranchtargetdociawi($bulan)));
         
         //return print_r($whereMonth);
    }


     public function getalldatabranchtargetdocianjur(){
         $bulan = $_GET['branchcode'];
         //return print_r($bulan);
         return print_r(json_encode($this->valueFilter->getalldatabranchtargetdociawi($bulan)));
         
         //return print_r($whereMonth);
    }

     public function getalldatabranchtargetdocinere(){
         $bulan = $_GET['branchcode'];
         //return print_r($bulan);
         return print_r(json_encode($this->valueFilter->getalldatabranchtargetdociawi($bulan)));
         
         //return print_r($whereMonth);
    }


    public function getalldatabranchtargetdojatiasih(){
         $bulan = $_GET['branchcode'];
         //return print_r($bulan);
         return print_r(json_encode($this->valueFilter->getalldatabranchtargetdociawi($bulan)));
         
         //return print_r($whereMonth);
    }



     public function getalldatabranchcianjur(){
    	 $bulan = $_GET['branchcode'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getalldatabranchcianjur($bulan)));
    	 
    	 //return print_r($whereMonth);
    }

    public function getalldatabranchcinere(){
    	 $bulan = $_GET['branchcode'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getalldatabranchcinere($bulan)));
    	 
    	 //return print_r($whereMonth);
    }


    public function getalldatabranchjatiasih(){
    	 $bulan = $_GET['branchcode'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->valueFilter->getalldatabranchjatiasih($bulan)));
    	 
    	 //return print_r($whereMonth);
    }


    public function DOToSPK(){
         $bulan = $_GET['bulan'];
         //return print_r($bulan);
         return print_r(json_encode($this->valueFilter->DOToSPK($bulan)));
         
         //return print_r($whereMonth);
    }


    public function getstockkendaraan(){
         //return print_r($bulan);
         return print_r(json_encode($this->valueFilter->getstockkendaraan()));
         
         //return print_r($whereMonth);
    }


    public function getInquirySource(){
        $bulan = $_GET['bulan'];
         //return print_r($bulan);
         return print_r(json_encode($this->valueFilter->getInquirySource($bulan)));
         
         //return print_r($whereMonth);
    }





}