<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cakuntransaksi extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		// $this->load->helper(array('url','form'));
		  $this->load->library('session');
		  
		  $this->load->model('makuntransaksi');
		
		 
	}
	public function index()
	{
		
		}


	/* Fungsi Jenis Surat */
	function tampil(){
		
		
		if ($this->input->post('table_search') !== '')
		{
			
		 	$this->session->set_userdata('field',$this->input->post('table_search')); 
			$field =$this->session->userdata('field'); 
			/*echo "<script> alert('$field') ; </script>";*/
			
		}
		
		$a['page']	= "akun_transaksi";
		$this->load->view('admin/index', $a);
		
	}
	
	function thnajaran(){
		
		$field =  $this->session->userdata('field');

		echo $this->makuntransaksi->thnajaran($field);
		$this->session->unset_userdata('field');
	}
	function akun(){
		$thnajaran=$this->input->get('thnajaran');
		echo $this->makuntransaksi->akun($thnajaran);
	}
	function akundtl(){
		$thnajaran=$this->input->get('thnajaran');
		$idakun=$this->input->get('idakun');
		echo $this->makuntransaksi->akundtl($thnajaran,$idakun);
	}
	
	function akundtlshow(){
		$thnajaran=$this->input->get('thnajaran');
		echo $this->makuntransaksi->akundtlshow($thnajaran);
	}
	function akundtlshowedit(){
		$thnajaran = $_GET['thnajaran'];
		echo $this->makuntransaksi->akundtlshowedit($thnajaran);
	}
	
	function tambah_akuntransaksi(){
		
		$a['page']	= "akuntransaksi/tambah_akuntransaksi";
		$this->load->view('admin/index', $a);
	}
	
	function editakuntransaksi($id){
		
		$a['page']	= "akuntransaksi/edit_akuntransaksi";
		$this->load->view('admin/index', $a, $id);
	}

	function insertdata(){
		$table =  'takun_transaksi';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->insert($table, $myjson );
		redirect('cakuntransaksi/tampil','refresh');
	}



	function updatedata(){
		$table =   'takun_transaksi';
		$idtable =  'idakun_transaksi';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 


	}

	
	function simpandtl(){
			$data= $this->input->get('arr');
			$thn_ajaran= $this->input->get('thn_ajaran');
			echo $this->makuntransaksi->simpandtl($data,$thn_ajaran);
		}
	
	function updatedtl(){
			$data= $this->input->get('arr');
			echo $this->makuntransaksi->updatedtl($data);
		}
		
	function deletedtl(){
			$data= $this->input->get('arr');
			echo $this->makuntransaksi->deletedtl($data);
		}
		
	function hapusakuntransaksi($id){
		$this->makuntransaksi->hapusakuntransaksi($id);
		redirect('cakuntransaksi/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->makuntransaksi->getjson();
    }

	
	function urlcmb()
    {

		echo $this->makuntransaksi->url();
    }
	
	
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->makuntransaksi->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $_GET['fields'];
		echo $this->makuntransaksi->get_datapopup($string);
    }
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->makuntransaksi->get_headerpopup($string);
    }


}

