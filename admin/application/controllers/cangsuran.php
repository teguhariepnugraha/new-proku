<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cangsuran extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		// $this->load->helper(array('url','form'));
		 $this->load->model('mangsuran');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		
		$a['page']	= "angsuran";
		
		$this->load->view('admin/index', $a);
	}
	
	function json() {
	 $field=$this->input->post('field');
       echo $this->mangsuran->json($field);
    }
	

	function tambah_angsuran(){
		
		$a['page']	= "angsuran/tambah_angsuran";
		$this->load->view('admin/index', $a);
	}

	function insertdata(){
		$table =  'tangsuran';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		
		$this->db->insert($table, $myjson );
		redirect('cangsuran/tampil','refresh');
	}



	function editangsuran($id){
		$a['page']	= "angsuran/edit_angsuran";
		$this->load->view('admin/index', $a, $id);
	}

	function updatedata(){
		$table =   'tangsuran';
		$idtable =  'idangsuran';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 


	}

	

	function deletedtl1(){
		$idangsurandtl = $this->input->get('idangsurandtl');
		
		echo $this->mangsuran->deletedtl1($idangsurandtl);
		
	}
	
	function hapusangsuran($id){
		$this->mangsuran->hapusangsuran($id);
		redirect('cangsuran/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->mangsuran->getjson();
    }

	
	function urlcmb()
    {

		echo $this->mangsuran->url();
    }
	
	function tampiledit(){
		$field =  $this->input->get('idangsuran');	
		echo $this->mangsuran->tampiledit($field);
	}
	
	function dataangsuran(){	
		$field =  $this->input->get('idpeserta');
		if($field != '')
		{
			echo $this->mangsuran->dataangsuran($field);
		}
		
	}
	
	function datapeserta(){	
		echo $this->mangsuran->datapeserta();
	}
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mangsuran->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $this->input->get('fields');	
		echo $this->mangsuran->get_datapopup($string);
    }
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mangsuran->get_headerpopup($string);
    }
	
	function urlautonumber()
    {
		echo $this->mangsuran->urlnumber();
    }
	
	function cekidangsuran()
	{
		$nota = $this->input->get('nota');
		echo $this->mangsuran->idangsuran($nota);
	}
	function simpandtl(){
			$data= $this->input->get('arr');
			echo $this->mangsuran->simpandtl($data);
			
		
		}
	function updatedtl(){
			$data= $this->input->get('arr');
			 
			echo $this->mangsuran->updatedtl($data);
			
		
		}
}

