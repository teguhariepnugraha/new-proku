<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class csikap extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		// $this->load->helper(array('url','form'));
		 $this->load->model('msikap');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		if ($this->input->post('table_search') !== '')
		{
			
		 	$this->session->set_userdata('field',$this->input->post('table_search')); 
			
			/*echo "<script> alert('$field') ; </script>";*/
			
		}

		$a['page']	= "sikap";
		
		$this->load->view('admin/index', $a);
	}

	function tampiledit(){
		$field =  $this->input->get('idsikap');	
		echo $this->msikap->tampiledit($field);
	}

	function tambah_sikap(){
		
		$a['page']	= "sikap/tambah_sikap";
		$this->load->view('admin/index', $a);
	}
	
	function sikap(){
		$field =  $this->session->userdata('field');

		echo $this->msikap->sikap($field);
		$this->session->unset_userdata('field');
		
	}
	function sikapdtl(){
		$idsikap = $this->input->get('idsikap');
		echo $this->msikap->sikapdtl($idsikap);
	}

	function insertdata(){
		$table =  'tsikap';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		
		$this->db->insert($table, $myjson );
		redirect('csikap/sikap','refresh');
	}



	function editsikap($id){
		$a['page']	= "sikap/edit_sikap";
		$this->load->view('admin/index', $a, $id);
	}

	function updatedata(){
		$table =   'tsikap';
		$idtable =  'idsikap';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 


	}

	

	function deletedtl1(){
		$idsikapdtl = $this->input->get('idsikapdtl');
		
		echo $this->msikap->deletedtl1($idsikapdtl);
		
	}
	
	function hapussikap($id){
		$this->msikap->hapussikap($id);
		redirect('csikap/sikap','refresh');
	}

	function getjsonsample()
    {
		echo $this->msikap->getjson();
    }

	
	function urlcmb()
    {

		echo $this->msikap->url();
    }
	
	function urlautonumber()
    {
		echo $this->msikap->urlnumber();
    }
	
	
	function sikapedit(){
		$field =  $this->input->get('idsikap');	
		echo $this->msikap->sikapedit($field);
	}
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->msikap->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $_GET['fields'];
		echo $this->msikap->get_datapopup($string);
    }
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->msikap->get_headerpopup($string);
    }
	function cekidsikap()
	{
		$kdsikap = $this->input->get('kdsikap');
		echo $this->msikap->idsikap($kdsikap);
	}
	function simpandtl(){
			$data= $this->input->get('arr');
			echo $this->msikap->simpandtl($data);
			
		
		}
	function updatedtl(){
			$data= $this->input->get('arr');
			 
			echo $this->msikap->updatedtl($data);
			
		
		}
}

