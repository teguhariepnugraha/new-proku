<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class clistdataincentivesro extends CI_Controller {

	function __construct(){
		parent::__construct();
		// $this->load->helper(array('url','form'));

		 $this->load->model('mlistdataincentivesro');
	}


    function tampil(){

    	//$field=$this->session->userdata('position');
    	$field =  $this->input->post('table_search');
		$a['data']	= $this->mlistdataincentivesro->tampil($field)->result_object();
		$a['page']	= "listdataincentivesro";
		
		$this->load->view('admin/index', $a);
	}


	public function pengajuanSRO($idpengajuan){
		$result=$this->mlistdataincentivesro->pengajuanSRO($idpengajuan);

		redirect('clistdataincentivesro/tampil','refresh');
		
	}



}