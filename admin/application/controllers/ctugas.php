<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ctugas extends CI_Controller {

	function __construct(){
		parent::__construct();
		// $this->load->helper(array('url','form'));
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		 $this->load->model('mtugas');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		$field =  $this->input->post('table_search');
		$a['data']	= $this->mtugas->tampil($field)->result_object();
		$a['page']	= "tugas";
		
		$this->load->view('admin/index', $a);
	}

	function tambah_tugas(){
		
		$a['page']	= "tugas/tambah_tugas";
		$this->load->view('admin/index', $a);
	}

	function insertdata(){
		$table =  'ttugas';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->insert($table, $myjson );
		redirect('ctugas/tampil','refresh');
	}



	function edittugas($id){
		$a['page']	= "tugas/edit_tugas";
		$this->load->view('admin/index', $a, $id);
	}
	
	
	function upload(){
			$files = $_GET['files'];
			
			$config['upload_path'] = './assets/imgkaryawan';
			$config['allowed_types'] = '*';
			$config['max_size']     = '5000';
			$config['max_width'] = '0';
			$config['max_height'] = '0';
			
			
			
	
			$this->load->library('upload',$config);
				
			$field_name = $files;
			if ( ! $this->upload->do_upload($field_name)) {
				// return the error message and kill the script
				//echo $this->upload->display_errors(); die();
				
			
			} else {
			
			}
		}

	function updatedata(){
		$table =   'ttugas';
		$idtable =  'idtugas';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 


	}

	function updatedtl(){
			$data= $this->input->get('arr');
			 
			echo $this->mtugas->updatedtl($data);
			
		
		}

	function hapustugas($id){
		$this->mtugas->hapustugas($id);
		redirect('ctugas/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->mtugas->getjson();
    }

	function urlautonumber()
    {
		echo $this->mtugas->urlnumber();
    }
	function urlcmb()
    {

		echo $this->mtugas->url();
    }
	
	function datakaryawan(){	
		echo $this->mtugas->datakaryawan();
	}
	function tugasdtl(){	
		$idtugas=$this->input->get('idtugas');
		echo $this->mtugas->tugasdtl($idtugas);
	}
	function simpandtl(){
			$data= $this->input->get('arr');
			echo $this->mtugas->simpandtl($data);
			
		
		}
	
	function cekidtugas()
	{
		$kdtugas = $this->input->get('kdtugas');
		echo $this->mtugas->idtugas($kdtugas);
	}
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mtugas->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mtugas->get_datapopup($string);
    }
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mtugas->get_headerpopup($string);
    }


}

