<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ccabang extends CI_Controller {

	function __construct(){
		parent::__construct();
		// $this->load->helper(array('url','form'));

		 $this->load->model('mcabang');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		$field =  $this->input->post('table_search');
		$a['data']	= $this->mcabang->tampil($field)->result_object();
		$a['page']	= "cabang";
		
		$this->load->view('admin/index', $a);
	}

	function tambah_cabang(){
		
		$a['page']	= "cabang/tambah_cabang";
		$this->load->view('admin/index', $a);
	}

	function insertdata(){
		$table =  'tcabang';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->insert($table, $myjson );
		redirect('ccabang/tampil','refresh');
	}



	function editcabang($id){
		$a['page']	= "cabang/edit_cabang";
		$this->load->view('admin/index', $a, $id);
	}

	function updatedata(){
		$table =   'tcabang';
		$idtable =  'idcabang';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 
		echo ($id);

		


	}

	

	function hapuscabang($id){
		$this->mcabang->hapusjabatan($id);
		redirect('ccabang/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->mcabang->getjson();
    }

	
	function urlcmb()
    {

		echo $this->mcabang->url();
    }
	
	
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mcabang->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mcabang->get_datapopup($string);
    }
    
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mcabang->get_headerpopup($string);
    }

    function search_data()
    {
	
		$string =  $_GET['fields'];
		echo $this->mcabang->search_data($string);
    }


}

