<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ctransaksi extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		// $this->load->helper(array('url','form'));
		 $this->load->model('mtransaksi');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		
		$a['page']	= "transaksi";
		
		$this->load->view('admin/index', $a);
	}
	
	function json() {
	 $field=$this->input->post('field');
       echo $this->mtransaksi->json($field);
    }
	

	function tambah_transaksi(){
		
		$a['page']	= "transaksi/tambah_transaksi";
		$this->load->view('admin/index', $a);
	}

	function insertdata(){
		$table =  'ttransaksi';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		
		$this->db->insert($table, $myjson );
		redirect('ctransaksi/tampil','refresh');
	}



	function edittransaksi($id){
		$a['page']	= "transaksi/edit_transaksi";
		$this->load->view('admin/index', $a, $id);
	}

	function updatedata(){
		$table =   'ttransaksi';
		$idtable =  'idtransaksi';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 


	}

	

	function deletedtl1(){
		$idtransaksidtl = $this->input->get('idtransaksidtl');
		
		echo $this->mtransaksi->deletedtl1($idtransaksidtl);
		
	}
	
	function hapustransaksi($id){
		$this->mtransaksi->hapustransaksi($id);
		redirect('ctransaksi/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->mtransaksi->getjson();
    }

	
	function urlcmb()
    {

		echo $this->mtransaksi->url();
    }
	
	function tampiledit(){
		$field =  $this->input->get('idtransaksi');	
		echo $this->mtransaksi->tampiledit($field);
	}
	
	function datatransaksi(){	
		echo $this->mtransaksi->datatransaksi();
	}
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mtransaksi->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $this->input->get('fields');	
		echo $this->mtransaksi->get_datapopup($string);
    }
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mtransaksi->get_headerpopup($string);
    }
	
	function urlautonumber()
    {
		echo $this->mtransaksi->urlnumber();
    }
	
	function cekidtransaksi()
	{
		$nota = $this->input->get('nota');
		echo $this->mtransaksi->idtransaksi($nota);
	}
	function simpandtl(){
			$data= $this->input->get('arr');
			echo $this->mtransaksi->simpandtl($data);
			
		
		}
	function updatedtl(){
			$data= $this->input->get('arr');
			 
			echo $this->mtransaksi->updatedtl($data);
			
		
		}
}

