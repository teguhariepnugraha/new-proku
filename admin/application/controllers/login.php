<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		// $this->load->helper(array('url','form'));
		 $this->load->model('model_admin');
	}

	public function index()
	{
		/*if($this->session->userdata('admin_valid') == TRUE && $this->session->userdata('user') == 'admin' ){
			
			
			redirect("admin");
		}*/
	/*	if($this->session->userdata('admin_valid') == TRUE && $this->session->userdata('user') == 'personal' ){
			redirect("admin_personal");
		}*/
		$this->load->view('login');
	}

	public function do_login()
	{
		$u = $this->input->post("nik");
		//$u="0001";
		$p = base64_encode($this->input->post("password"));
		//$p="MTIzNDU2";
		//$p = $_COOKIE['src'];
			/*echo "<script> alert('$p') ; </script>";*/
		// echo json_encode('user' => $user, 'pass' => $pass);
		// echo $u."<br>".$p;

		$cari = $this->model_admin->cek_login($u, $p)->row();
		$hitung = $this->model_admin->cek_login($u, $p)->num_rows();
		if(!empty($cari)){
			$caricabang=$this->model_admin->cari_cabang($cari->nik)->row();
			$carirelateduser=$this->model_admin->cari_relateduser($cari->nik)->row();
		}
		
	/*	$cari_personal = $this->model_admin->cek_login_personal($u, $p)->row();
		$personal = $this->model_admin->cek_login_personal($u, $p)->num_rows();*/
	
		if ($hitung > 0) {
			/*$thn_pelajarn = $this->model_admin->thn_pelajarn_aktif()->row();
				
			$flag_show =  $cari->flag_show;
			$flag =  $cari->flag;
			$userid =  $cari->iduser;
			$thn_pelajarn_aktif = $thn_pelajarn->thn_ajaran;
	
			if ($flag == 1 )
			{$and = " and idpeserta = $userid"; }
			else if ($flag == 2 && $flag_show == 2 )
			{$and = ""; }
			else if ($flag == 2 && $flag_show == 1 )
			{$and = ' and a.insert_by =' . $userid; }
			else if ($flag == 2 && $flag_show == '' )
			{$and = ' and a.insert_by =' . $userid; }*/
			
			/*$data = array('userid' => $cari->iduser ,
							'username' => $cari->username, 
							'flag' => $cari->flag, 
							'and' => $and, 
							'thn_pelajarn_aktif' =>$thn_pelajarn_aktif,
							'flag_show' => $cari->flag_show, 
							'idjabatan' => $cari->idjabatan, 
							'admin_valid' => TRUE			
			);*/

			if(empty( $caricabang)){

				$data = array('userid' => $cari->iduser ,
							'username' => $cari->username, 
							'idjabatan' => $cari->idjabatan,
							'password' => $cari->password, 
							'nik' => $cari->nik, 
							'idkaryawan' => $cari->idkaryawan, 
							'admin_nama' =>$cari->nmkaryawan,
							'cabang' => "",
							'jabatan'=>$cari->jabatan,
							'relateduser'=>"",
							'position'=>"",
							'admin_valid' => TRUE			
			);

			}else{
				$data = array('userid' => $cari->iduser ,
							'username' => $cari->username, 
							'idjabatan' => $cari->idjabatan,
							'password' => $cari->password, 
							'nik' => $cari->nik, 
							'idkaryawan' => $cari->idkaryawan, 
							'admin_nama' =>$cari->nmkaryawan,
							'cabang' => $caricabang->BranchCode,
							'jabatan'=>$cari->jabatan,
							'relateduser'=>$carirelateduser->relateduser,
							'position'=>$carirelateduser->position,
							'admin_valid' => TRUE			
			);
			}


			


			
			
			$this->session->set_userdata($data);
			/*$and = $this->session->userdata('and');*/
			/*echo "<script> alert ('  $and  '); </script>";*/
			/*if ($cari->flag == 2)
			{redirect('admin','refresh');}
			else
			{redirect('admin_personal','refresh');}*/
			redirect('admin','refresh');
		}
		/*elseif ($hitung == 0 && $personal > 0) {
			
				
		
			$data = array('admin_id' => $cari_personal->idkaryawan ,
							'admin_user' => $cari_personal->nama, 
							'idkaryawan' => $cari_personal->idkaryawan,
							'admin_nama' => $cari_personal->nama,
							'masuk' => $cari_personal->masuk,	
							'user' => 'personal',	
							'admin_valid' => TRUE			
			);
			
			
			$this->session->set_userdata($data);

			redirect('admin_personal','refresh');
		}*/
		else{
			$this->session->set_flashdata('error', "data tidk ditemukan");
			redirect('./login','refresh');
		}
		
		
		
	}

	public function logout()
	{

		$this->session->sess_destroy();
		redirect('login');
	}
	public function gantilogin()
	{


		redirect('./login/gantilogin');
	}


}

/* End of file login.php */
/* Location: ./application/controllers/login.php */