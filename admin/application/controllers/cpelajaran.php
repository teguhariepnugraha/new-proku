<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cpelajaran extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		// $this->load->helper(array('url','form'));
		 $this->load->model('mpelajaran');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		$field =  $this->input->post('table_search');
		$a['data']	= $this->mpelajaran->tampil($field)->result_object();
		$a['page']	= "pelajaran";
		
		$this->load->view('admin/index', $a);
		$data = array('admin_id' => '12' ,
							'idkaryawan' => '10',
							'admin_valid' => TRUE			
			);
			$this->session->set_userdata($data);
	}

	function tambah_pelajaran(){
		
		$a['page']	= "pelajaran/tambah_pelajaran";
		$this->load->view('admin/index', $a);
	}

	function insertdata(){
		$table =  'tpelajaran';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		
		$this->db->insert($table, $myjson );
		redirect('cpelajaran/tampil','refresh');
	}



	function editpelajaran($id){
		$a['page']	= "pelajaran/edit_pelajaran";
		$this->load->view('admin/index', $a, $id);
	}

	function updatedata(){
		$table =   'tpelajaran';
		$idtable =  'idpelajaran';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 


	}

	

	function deletedtl1(){
		$idpelajarandtl = $this->input->get('idpelajarandtl');
		
		echo $this->mpelajaran->deletedtl1($idpelajarandtl);
		
	}
	
	function hapuspelajaran($id){
		$this->mpelajaran->hapuspelajaran($id);
		redirect('cpelajaran/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->mpelajaran->getjson();
    }

	
	function urlcmb()
    {

		echo $this->mpelajaran->url();
    }
	
	function tampiledit(){
		$field =  $this->input->get('idpelajaran');	
		echo $this->mpelajaran->tampiledit($field);
	}
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mpelajaran->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mpelajaran->get_datapopup($string);
    }
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mpelajaran->get_headerpopup($string);
    }
	function cekidpelajaran()
	{
		$kdpelajaran = $this->input->get('kdpelajaran');
		echo $this->mpelajaran->idpelajaran($kdpelajaran);
	}
	function simpandtl(){
			$data= $this->input->get('arr');
			echo $this->mpelajaran->simpandtl($data);
			
		
		}
	function updatedtl(){
			$data= $this->input->get('arr');
			 
			echo $this->mpelajaran->updatedtl($data);
			
		
		}
}

