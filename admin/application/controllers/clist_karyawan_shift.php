<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class clist_karyawan_shift extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		// $this->load->helper(array('url','form'));
		
		 
		 $this->load->model('mlist_karyawan_shift');
		 
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		$field =   $this->input->post('table_search');
		if ($field != '')
		{
		$this->session->set_userdata('varlist_shift', $field);
		}
		else
		{
			$this->session->unset_userdata('varlist_shift');
			}
		$a['page']	= "list_karyawan_shift";
		$this->load->view('admin/index', $a);
		
	}

	function departemen(){
		$field =   $this->session->userdata('varlist_shift');
		
		echo $this->mlist_karyawan_shift->departemen($field);
		
	}
	function bagian(){
	 	$id=$this->input->get('id');
	    $field =   $this->session->userdata('varlist_shift');
		echo $this->mlist_karyawan_shift->bagian($id,$field);
		
	}
	function karyawan(){
	 	$id=$this->input->get('id');
		$iddivisi=$this->input->get('divisi');
		$field =   $this->session->userdata('varlist_shift');
		echo $this->mlist_karyawan_shift->karyawan($id,$iddivisi,$field);
		
		
	}
	
	
	
	
	
	


}

