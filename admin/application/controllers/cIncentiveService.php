<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require('./application/third_party/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;



class cIncentiveService extends CI_Controller {

	public function __construct() {
        
        parent::__construct();
		if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}
        $this->load->library('datatables');
        $this->load->model('mIncentiveService');
         $this->load->library('pdf');
    }


   public function tampil(){
   			if($this->input->post('submit') != NULL ){

   				 $postData = $this->input->post();
    	  		 $bulan=$postData['bulan'];
    	  		 $tahun=$postData['year'];
    	  		 //$cabang=$postData['cabang'];
    	  		 if( $bulan==null or $tahun==null){
    	  		 	 $this->session->set_flashdata('data_kosong', "kosong");
    	  		 	 redirect('cIncentiveService/tampil','refresh');
			
    	  		 }else{
    	  		 	// $a['data'] =$this->mdetail_incetive_sro->getcreatedBy($bulan,$tahun,$cabang);
    	  		 	$cabang=$this->session->userdata('cabang');
    	  		 	$a['data']=$this->getDataIncentiveService($cabang, $tahun, $bulan);
                    //$cabang='641940104';
                    //$a['data']=$this->getDataIncentiveService($cabang, $tahun, $bulan);
    	  		 	
    	  		 }
   					

   			}

   			$a['page']	= "detail_incentive_service";
		
		    $this->load->view('admin/index', $a);
   		 
    }



    public function getDataIncentiveService($branchcode,$year,$month){


        $monthmin1=$this->getMonthMin1($year,$month);
        $monthmin2=$this->getMonthMin2($year,$month);

        $arrMonthmin1=explode(",",$monthmin1);
        $arrMonthmin2=explode(",",$monthmin2);
    	

    	$arrindux["spooringTotalMargin&Incentive"]=$this->mIncentiveService->getFullIncentiveSpooring($branchcode, $year, $month);
    	$arrindux["chemicalTotalMargin&Incentive"]=$this->mIncentiveService->getfullIncentiveChemical($branchcode,$year,$month);

        $arrindux["getaroutstanding"]=$this->mIncentiveService->getpaymentN($branchcode, $year, $month, $monthmin1, $monthmin2);


        if($branchcode=="641940104"){
                $arrindux["calMSI"]=$this->mIncentiveService->getAllMsiDataCal($branchcode,$year,$month);
        }else{
                $arrindux["calMSI"]=$this->mIncentiveService->getAllMsiDataCal($branchcode,$year,$month);
        }
                
        if($branchcode=="641940104"){
                $arrindux["calMSIN1"]=$this->mIncentiveService->getAllMsiDataCal($branchcode,$arrMonthmin1[1],$arrMonthmin1[0]);
        }else{
               $arrindux["calMSIN1"]=$this->mIncentiveService->getAllMsiDataCal($branchcode,$arrMonthmin1[1],$arrMonthmin1[0]);
         }

        if($branchcode=="641940104"){
                $arrindux["calMSIN2"]=$this->mIncentiveService->getAllMsiDataCal1($branchcode,$arrMonthmin1[1],$arrMonthmin1[0]);
        }else{
            $arrindux["calMSIN2"]=$this->mIncentiveService->getAllMsiDataCal($branchcode,$arrMonthmin1[1],$arrMonthmin1[0]);
        }


         //$arrindux['dataartransaksi'] =$this->mIncentiveService->getformataroutstanding($branchcode, $year, $month, $monthmin1, $monthmin2);
                

        $arrindux["targetRKA"]=$this->mIncentiveService->getTargetRKA($branchcode,$year,$month);
        $arrindux["targetRKAN1"]=$this->mIncentiveService->getTargetRKA($branchcode,$arrMonthmin1[1],(int)$arrMonthmin1[0]);
        $arrindux["targetRKAN2"]=$this->mIncentiveService->getTargetRKA($branchcode,$arrMonthmin2[1],(int)$arrMonthmin2[0]);
        $arrindux["fullRekapBySA"]=$this->mIncentiveService->getfullRekapBySA1($branchcode, $year, $month);
        $arrindux["fullRekapByMechanic"]=$this->mIncentiveService->getfullRekapByMechanic($branchcode, $year, $month);

        



     $arrindux['chemicalIncetive']=$this->calInsentiveChemical($branchcode, $year,$month, $arrindux);

     $arrindux['spooringIncentive']=$this->calIncentiveSpooring($branchcode, $year, $month, $arrindux);

     $arrindux["indukProku"]=$this->indukProku($branchcode, $year, $month, $arrindux);
       
     $arrindux["prokuIncentive"]= $this->calIncentiveMSI($branchcode, $year, $month, $arrindux);
       
        $arrindux["select-bulan"]=$month;

        
        $arrindux["nama_cabang"]=$branchcode;

        $arrindux["rekapIncentive"]=$this->rekapIncentive($branchcode, $year, $month, $arrindux);

        

       

        //logic hanya untuk update data insert ar outstanding saja

        // if($month<12){
        //     if($month==(date("n")-1) && $year==date("Y")){
        //         $hasil=$this->mIncentiveService->insertaroutstanding($branchcode, $year, $month); 
        //      }
        // }else{
        //     if($month==((date("n")+12)-1)  && $year==(date("Y")-1)){
        //         $hasil=$this->mIncentiveService->insertaroutstanding($branchcode, $year, $month);
        //     }
        // }
        

       
        
        

        

    	////$arr1["targetRKA1"]=$this->mIncentiveService->getTargetRKA($branchcode,$year,$month);
    	//$arr1["calMSI1"]=$this->mIncentiveService->getAllMsiDataCal($branchcode,$year,$month);

        //print_r($arrindux);
        //print_r($arrbalik);

    	return $arrindux;

    }



    function hitungjumlahposition($arr,$key,$value){
    	$count=0;
    	    foreach ($arr as $lihat):
    						if($lihat[$key]==$value){
    							$count++;
    						}
    						
    			endforeach;
    	return $count;

    }

    function hitungjumlahpositionsparepart($arr,$key,$value){
        $count=0;
            foreach ($arr as $lihat):
                            if($lihat[$key]==$value || $lihat[$key]=='SPAREPARTS'){
                                $count++;
                            }
                            
                endforeach;
        return $count;

    }

    function calInsentiveChemical($branchcode, $year, $month, $arrindux){

    	//belum untuk PIC stempel nya

    			$insentive=15;
    			$insentiveSA=30;
    			$totalMarginDesinfektan=0;
    			$totalMarginEngineConditioner=0;
    			$totalMarginEngineFlush=0;
    			$totalIncentiveDesinfektan=0;
    			$totalIncentiveEngineConditioner=0;
    			$totalIncentiveEngineFlush=0;
    			$totalIncentiveDesinfektanSA=0;
    			$totalIncentiveEngineConditionerSA=0;
    			$totalIncentiveEngineFlushSA=0;

    			$insentiveCalSM=12;
    			$insentiveCalSA=30;
    			$insentiveCalSparePart=25;
    			$insentiveCalForeman=10;
    			$insentiveCalMechanic=20;
    			$insentiveCalSticker=3;



    			//$arr["spooringTotalMargin&Incentive"]=$this->mIncentiveService->getfullIncentiveChemical($branchcode,$year,$month);
    			foreach ($arrindux["chemicalTotalMargin&Incentive"] as $lihat):
    						if($lihat["partname"]=="DESINFEKTAN"){
    							$totalMarginDesinfektan=$lihat["totalmargin"];
    						}
    						else if($lihat["partname"]=="ENGINE CONDITIONER"){
    							$totalMarginEngineConditioner=$lihat["totalmargin"];
    						}
    						else if($lihat["partname"]=="ENGINE FLUSH"){
    							$totalMarginEngineFlush=$lihat["totalmargin"];
    						}
    			endforeach;

    			//15%
    			$totalIncentiveDesinfektan=$totalMarginDesinfektan*$insentive/100;
    			$totalIncentiveEngineConditioner=$totalMarginEngineConditioner*$insentive/100;
    			$totalIncentiveEngineFlush=$totalMarginEngineFlush*$insentive/100;



    			//30%
    			$totalIncentiveDesinfektanSA=$totalIncentiveDesinfektan*$insentiveSA/100;
    			$totalIncentiveEngineConditionerSA=$totalIncentiveEngineConditioner*$insentiveSA/100;
    			$totalIncentiveEngineFlushSA=$totalIncentiveEngineFlush*$insentiveSA/100;

    			//$arr["listEmployeeChemical"]=$this->mIncentiveService->getEmployeeForIncentChemical($branchcode);

                $arr["listEmployeeChemical"]=$this->getemployeedata($branchcode);

                $arr["listEmployeeChemical1"]=$this->mIncentiveService->getEmployeeForIncentChemical($branchcode);


                $arrayindex=count($arr["listEmployeeChemical"]);
                $arrayindex++;

                // $newdata =  array (
                //   'CompanyCode' => '641940',
                //   //harus diganti
                //   'BranchCode' => $branchcode,
                //   'EmployeeID' => '-',
                //   'EmployeeName'=>'-',
                //   'position'=>'PIC TEMPEL STIKER'
                // );

                //$arr["listEmployeeChemical"]=$newdata;
                //array_push($arr["listEmployeeChemical"],$newdata);

                foreach($arr["listEmployeeChemical1"] as $lihat) :
                      array_push($arr["listEmployeeChemical"],array("EmployeeName"=>$lihat["nama"], "position"=>$lihat['position']));      
                endforeach;

                //print_r($arr["listEmployeeChemical"]);

                //array_push($arr["listEmployeeChemical"],array("nama"=>"teguh", "position"=>"MEKANIK"));

               // print_r($arr["listEmployeeChemical"]);




    			$jmlMechanic=$this->hitungjumlahposition($arr["listEmployeeChemical"],"position","MEKANIK");

    			$jmlSparepart=$this->hitungjumlahposition($arr["listEmployeeChemical"],"position","ADMINISTRASI SPAREPART");

                $jmlForeman=$this->hitungjumlahposition($arr["listEmployeeChemical"],"position","FOREMAN");

    			$incentivePerMechanic=($insentiveCalMechanic/$jmlMechanic);
                if($jmlSparepart==0){
                    $incentivePerSparepart=$insentiveCalSparePart/1;
                }else{
                    $incentivePerSparepart=$insentiveCalSparePart/$jmlSparepart;
                }
    			if($jmlForeman==0){
                    $incentiveForeman=$insentiveCalForeman/1;
                }else{
                    $incentiveForeman=$insentiveCalForeman/$jmlForeman;
                }
                



    			

    			
                 $index=0;
    			foreach ($arr["listEmployeeChemical"] as $lihat):
    						if($lihat["position"]=="SERVICE MANAGER"){

    							$arr["listEmployeeChemical"][$index] += ['scema' =>$insentiveCalSM ];
    							$arr["listEmployeeChemical"][$index] += ['DESINFEKTAN%' =>$insentiveCalSM];
    						    $arr["listEmployeeChemical"][$index] += ['DESINFEKTANRP' =>ceil($totalIncentiveDesinfektan*$insentiveCalSM/100)];


    							$arr["listEmployeeChemical"][$index] += ['ENGINECONDITIONER%' =>$insentiveCalSM];
    							$arr["listEmployeeChemical"][$index] += ['ENGINECONDITIONERRP' =>ceil($totalIncentiveEngineConditioner*$insentiveCalSM/100)];
    							

    							$arr["listEmployeeChemical"][$index] += ['ENGINEFLUSH%' =>$insentiveCalSM];
    							$arr["listEmployeeChemical"][$index] += ['ENGINEFLUSHRP' =>ceil($totalIncentiveEngineFlush*$insentiveCalSM/100)];

    							

    						}
    						else if($lihat["position"]=="SERVICE ADVISOR"){
    						  $arr["listEmployeeChemical"][$index] += ['scema' =>$insentiveCalSA];
    							foreach ($arrindux["chemicalTotalMargin&Incentive"] as $lihat1):

    								if($lihat["EmployeeName"]==$lihat1["employeename"] ){
    									


    										if($lihat1["partname"] == "DESINFEKTAN"){
    											$arr["listEmployeeChemical"][$index] += ['DESINFEKTAN%' =>$lihat1["persentase"]];
    											$arr["listEmployeeChemical"][$index] += ['DESINFEKTANRP' =>ceil($totalIncentiveDesinfektanSA*$lihat1["persentase"]/100)];
    										}

    										else if($lihat1["partname"] == "ENGINE CONDITIONER"){
    											$arr["listEmployeeChemical"][$index] += ['ENGINECONDITIONER%' =>$lihat1["persentase"]];
    											$arr["listEmployeeChemical"][$index] += ['ENGINECONDITIONERRP' =>ceil($totalIncentiveEngineConditionerSA*$lihat1["persentase"]/100)];
    										}
    										else if($lihat1["partname"] == "ENGINE FLUSH"){
    											$arr["listEmployeeChemical"][$index] += ['ENGINEFLUSH%' =>$lihat1["persentase"]];
    											$arr["listEmployeeChemical"][$index] += ['ENGINEFLUSHRP' =>ceil($totalIncentiveEngineFlushSA*$lihat1["persentase"]/100)];
    										}

    								}

    								
		    						
    							endforeach;


    							
    						}
    						else if(($lihat["position"]=="ADMINISTRASI SPAREPART") || ($lihat["position"]=="SPAREPART")){
    							$arr["listEmployeeChemical"][$index] += ['scema' =>$insentiveCalSparePart ];

    							

    							$arr["listEmployeeChemical"][$index] += ['DESINFEKTAN%' =>$incentivePerSparepart];
    							$arr["listEmployeeChemical"][$index] += ['DESINFEKTANRP' =>ceil($totalIncentiveDesinfektan*$incentivePerSparepart/100)];
    							$arr["listEmployeeChemical"][$index] += ['ENGINECONDITIONER%' =>$incentivePerSparepart];
    							$arr["listEmployeeChemical"][$index] += ['ENGINECONDITIONERRP' =>ceil($totalIncentiveEngineConditioner*$incentivePerSparepart/100)];
    							$arr["listEmployeeChemical"][$index] += ['ENGINEFLUSH%' =>$incentivePerSparepart];
    							$arr["listEmployeeChemical"][$index] += ['ENGINEFLUSHRP' =>ceil($totalIncentiveEngineFlush*$incentivePerSparepart/100)];


    						}
    						else if($lihat["position"]=="FOREMAN"){
    							$arr["listEmployeeChemical"][$index] += ['scema' =>$insentiveCalForeman ];

    							$arr["listEmployeeChemical"][$index] += ['DESINFEKTAN%' =>$incentiveForeman];
    							$arr["listEmployeeChemical"][$index] += ['DESINFEKTANRP' =>($totalIncentiveDesinfektan*$insentiveCalForeman/100)];
    							$arr["listEmployeeChemical"][$index] += ['ENGINECONDITIONER%' =>$incentiveForeman];
    							$arr["listEmployeeChemical"][$index] += ['ENGINECONDITIONERRP' =>($totalIncentiveEngineConditioner*$insentiveCalForeman/100)];
    							$arr["listEmployeeChemical"][$index] += ['ENGINEFLUSH%' =>$incentiveForeman];
    							$arr["listEmployeeChemical"][$index] += ['ENGINEFLUSHRP' =>($totalIncentiveEngineFlush*$insentiveCalForeman/100)];

    						}
    						else if($lihat["position"]=="MEKANIK"){
    							$arr["listEmployeeChemical"][$index] += ['scema' =>$insentiveCalMechanic ];

    							$arr["listEmployeeChemical"][$index] += ['DESINFEKTAN%' =>$incentivePerMechanic];
    							$arr["listEmployeeChemical"][$index] += ['DESINFEKTANRP' =>round($totalIncentiveDesinfektan*$incentivePerMechanic/100)];
    							$arr["listEmployeeChemical"][$index] += ['ENGINECONDITIONER%' =>$incentivePerMechanic];
    							$arr["listEmployeeChemical"][$index] += ['ENGINECONDITIONERRP' =>round($totalIncentiveEngineConditioner*$incentivePerMechanic/100)];
    							$arr["listEmployeeChemical"][$index] += ['ENGINEFLUSH%' =>$incentivePerMechanic];
    							$arr["listEmployeeChemical"][$index] += ['ENGINEFLUSHRP' =>round($totalIncentiveEngineFlush*$incentivePerMechanic/100)];

    						}

    						else if($lihat["position"]=="PIC TEMPEL STIKER"){
    							$arr["listEmployeeChemical"][$index] += ['scema' =>$insentiveCalSticker ];

    							$arr["listEmployeeChemical"][$index] += ['DESINFEKTAN%' =>$insentiveCalSticker];
    							$arr["listEmployeeChemical"][$index] += ['DESINFEKTANRP' =>round($totalIncentiveDesinfektan*$insentiveCalSticker/100)];
    							$arr["listEmployeeChemical"][$index] += ['ENGINECONDITIONER%' =>$insentiveCalSticker];
    							$arr["listEmployeeChemical"][$index] += ['ENGINECONDITIONERRP' =>round($totalIncentiveEngineConditioner*$insentiveCalSticker/100)];
    							$arr["listEmployeeChemical"][$index] += ['ENGINEFLUSH%' =>$insentiveCalSticker];
    							$arr["listEmployeeChemical"][$index] += ['ENGINEFLUSHRP' =>round($totalIncentiveEngineFlush*$insentiveCalSticker/100)];

    						}

                            else {
                                $arr["listEmployeeChemical"][$index] += ['scema' =>0 ];

                                $arr["listEmployeeChemical"][$index] += ['DESINFEKTAN%' =>0];
                                $arr["listEmployeeChemical"][$index] += ['DESINFEKTANRP' =>0];
                                $arr["listEmployeeChemical"][$index] += ['ENGINECONDITIONER%' =>0];
                                $arr["listEmployeeChemical"][$index] += ['ENGINECONDITIONERRP' =>0];
                                $arr["listEmployeeChemical"][$index] += ['ENGINEFLUSH%' =>0];
                                $arr["listEmployeeChemical"][$index] += ['ENGINEFLUSHRP' =>0];

                            }

    						$index++;
    			endforeach;

                $calENGINEFLUSHRP=0;
                $calDESINFEKTANRP=0;
                $calENGINECONDITIONERRP=0;

                  $index=0;
            foreach ($arr["listEmployeeChemical"] as $lihat):
                  if(isset($lihat["ENGINEFLUSHRP"])){
                       $calENGINEFLUSHRP=$lihat["ENGINEFLUSHRP"];
                  }else{
                       $calENGINEFLUSHRP=0;
                  }
                  if(isset($lihat["DESINFEKTANRP"])){
                       $calDESINFEKTANRP=$lihat["DESINFEKTANRP"];
                  }else{
                        $calDESINFEKTANRP=0;
                  }
                  if(isset($lihat["ENGINECONDITIONERRP"])){
                      $calENGINECONDITIONERRP=$lihat["ENGINECONDITIONERRP"];
                  }else{
                       $calENGINECONDITIONERRP=0;
                  }
                $arr["listEmployeeChemical"][$index] += ['total' =>$calENGINEFLUSHRP+ $calDESINFEKTANRP+$calENGINECONDITIONERRP];

                $index++;

            endforeach;

    			//print_r ($arrayindex);
    			//print_r ($arr["listEmployeeChemical"]);
    			return ($arr["listEmployeeChemical"]);

    			
    	}

        //perhitungan untuk incentive ac
        function calincentiveAC($branchcode, $year, $month){

            $arr["fullIncentiveAC"]=$this->mIncentiveService->getfullincentiveAc($branchcode, $year, $month);

            $arr["fullRekapBySA"]=$this->mIncentiveService->getfullRekapBySA1($branchcode, $year, $month);

             $arr["employeeAC"]=$this->getemployeedata($branchcode);
             $arrayindex=count($arr["employeeAC"]);
            $arrayindex++;

                //ganti jika employee nya kurang
             $arr["employeeAC1"]=$this->mIncentiveService->getEmployeeForIncentSpooring($branchcode);



            foreach($arr["employeeAC1"] as $lihat) :
                      array_push($arr["employeeAC"],array("EmployeeName"=>$lihat["nama"], "position"=>$lihat["position"]));      
                endforeach;

            $arr['fullRekapByMechanic']=$this->mIncentiveService->getfullRekapByMechanic($branchcode, $year, $month);

            $totalInvoiceAC=0;
            $totalMarginAC=0;
            $totalTargetAC=100;
            $incentiveAC=0;


            foreach ($arr["fullIncentiveAC"] as $lihat):
                            $totalInvoiceAC=$lihat["Total Invoice"];
                            $totalMarginAC=$lihat["totalmargin"];
            endforeach;



            if($totalInvoiceAC>=60 and $totalInvoiceAC<80){
                $incentiveAC=6;
            }else if(($totalInvoiceAC>=80) && ($totalInvoiceAC<100)) {
                $incentiveAC=8;
            }else if($totalInvoice>=100) {
                $incentiveAC=15;
            }else{
                $incentiveAC=0;
            }

            //calculation incentive service ac
            //$totalIncentiveAC=$totalMarginAC*$incentiveAC/100;

            //calculation incentive service ac 50% of margin
            $totalIncentiveAC=($totalMarginAC*50/100)*$incentiveAC/100;

            
            $incentiveSMAC=30;//servicemanager
            $incentiveSAAC=45;//service advisor
            $incentiveSVADMAC=10;//serviceadmin
            $incentiveSROAC=5;//SRO
            $incentiveCEEAC=10;//CCE
            $incentiveKasirAC=0;
            $incentiveOfficeBoyAC=0;
            $incentiveSrmAC=0;
            $incentiveADHAC=0;
            
            $jmlofficeboy=0;

            $jmlSRO=$this->hitungjumlahposition($arr["employeeAC"],"position","SRO");
            $jmlSA=$this->hitungjumlahposition($arr["employeeAC"],"position","SERVICE ADVISOR");
            


            foreach ($arr["employeeAC"] as $lihat):
                if($incentiveAC<>0){
                            if($lihat["position"]=="SERVICE MANAGER"){
                                    $arr["employeeAC"][$index] += ['scema' =>$incentiveSMAC];

                                    $arr["employeeAC"][$index] += ['PERFORMANCE%' =>$incentiveSMAC];
                                $arr["employeeAC"][$index] += ['INCENTIVERP' =>ceil($totalIncentiveAC*$incentiveSMAC/100)];

                               // $arr["employeeAC"][$index] += ['INCENTIVERP' =>ceil($totalIncentiveAC*$incentiveSMAC/100)];


                                

                            }
                            //else if($lihat["position"]=="FD"){
                            // else if($lihat["position"]=="ADH"){
                            //         $arr["employeeAC"][$index] += ['scema' =>$incentiveADHAC];

                            //         $arr["employeeAC"][$index] += ['PERFORMANCE%' =>$incentiveADHAC];
                            //         $arr["employeeAC"][$index] += ['INCENTIVERP' =>ceil($totalIncentiveAC*$incentiveADHAC/100)];


                            // }


                            //else if($lihat["position"]=="SVADM"){
                            else if($lihat["position"]=="SERVICEADMIN"){
                                
                                    $arr["employeeAC"][$index] += ['scema' =>$incentiveSVADMAC];

                                    $arr["employeeAC"][$index] += ['PERFORMANCE%' =>$incentiveSVADMAC];
                                    $arr["employeeAC"][$index] += ['INCENTIVERP' =>ceil($totalIncentiveAC*$incentiveSVADMAC/100)];


                            }


                            // else if($lihat["position"]=="OFFICEBOY"){
                                
                            //         $arr["employeeAC"][$index] += ['scema' =>$incentiveOfficeBoyAC];

                            //         $arr["employeeAC"][$index] += ['PERFORMANCE%' =>100/$jmlofficeboy];
                            //         $arr["employeeAC"][$index] += ['INCENTIVERP' =>ceil(($totalIncentiveAC*$incentiveOfficeAC/100)*(100/$jmlofficeboy)/100)];


                            // }


                            // else if($lihat["position"]=="KASIR"){
                            //         $arr["employeeAC"][$index] += ['scema' =>$incentiveKasirAC];

                            //         $arr["employeeAC"][$index] += ['PERFORMANCE%' =>$incentiveKasirAC];
                            //         $arr["employeeAC"][$index] += ['INCENTIVERP' =>ceil($totalIncentiveAC*$incentiveKasirAC/100)];


                            // }


                            //ini masih perlu konfirmasi
                            else if($lihat["position"]=="SERVICE ADVISOR"){
                                    $arr["employeeAC"][$index] += ['scema' =>$incentiveSASpooring];

                                    foreach ($arr["fullRekapBySA"] as $lihat1):
                                            if($lihat["employeeAC"]==$lihat1["SaName"] ){

                                                $arr["employeeAC"][$index] += ['PERFORMANCE%' =>$lihat1["persentase"]];
                                                $arr["employeeAC"][$index] += ['INCENTIVERP' =>ceil(($totalIncentiveAC*$incentiveSAAC/100)*$lihat1["persentase"]/100)];

                                            }
                                    endforeach;

                                    

                            }
                            // else if($lihat["position"]=="MEKANIK"){

                            //     $arr["employeeAC"][$index] += ['scema' =>0];

                            //     //penambahan logic baru
                                
                               


                            //         foreach ($arr["fullRekapByMechanic"] as $lihat1):
                                        
                            //             if(array_search($lihat["EmployeeName"], array_column($arr["fullRekapByMechanic"], 'mechanicname')) !== False){

                            //                 if($lihat["EmployeeName"]==$lihat1["mechanicname"] ){
                            //                     //echo($lihat["EmployeeName"]."<br>");
                            //                     $arr["employeeAC"][$index] += ['PERFORMANCE%' =>$lihat1["persentase"]];
                            //                     $arr["employeeAC"][$index] += ['INCENTIVERP' =>0];

                            //                 }
                                                    
                            //             }       
                            //             else{
                            //                     $arr["employeeAC"][$index] += ['PERFORMANCE%' =>0];
                            //                     $arr["employeeAC"][$index] += ['INCENTIVERP' =>0];
                            //             }
                            //         endforeach;
                                

                            // }

                            else {
                                    $arr["employeeAC"][$index] += ['scema' =>0];

                                    $arr["employeeAC"][$index] += ['PERFORMANCE%' =>0];
                                    $arr["employeeAC"][$index] += ['INCENTIVERP' =>0];


                            }

                }else{

                            if($lihat["position"]=="SERVICE MANAGER"){
                                    $arr["employeeAC"][$index] += ['scema' =>$incentiveSMAC];

                                    $arr["employeeAC"][$index] += ['PERFORMANCE%' =>$incentiveSMAC];
                                $arr["employeeAC"][$index] += ['INCENTIVERP' =>0];
                                

                            }
                            //else if($lihat["position"]=="FD"){
                            //     else if($lihat["position"]=="ADH"){
                            //         $arr["employeeAC"][$index] += ['scema' =>$incentiveADHAC];

                            //         $arr["employeeAC"][$index] += ['PERFORMANCE%' =>$incentiveADHSpooring];
                            //         $arr["employeeAC"][$index] += ['INCENTIVERP' =>0];


                            // }


                            //else if($lihat["position"]=="SVADM"){
                                else if($lihat["position"]=="SERVICEADMIN"){
                                    $arr["employeeAC"][$index] += ['scema' =>$incentiveSVADMAC];

                                    $arr["employeeAC"][$index] += ['PERFORMANCE%' =>$incentiveSVADMSpooring];
                                    $arr["employeeAC"][$index] += ['INCENTIVERP' =>0];


                            }

                            // else if($lihat["position"]=="OFFICEBOY"){
                                
                            //         $arr["employeeAC"][$index] += ['scema' =>$incentiveOfficeBoyAC];

                            //         $arr["employeeAC"][$index] += ['PERFORMANCE%' =>100/$jmlofficeboy];
                            //         $arr["employeeAC"][$index] += ['INCENTIVERP' =>0];


                            // }

                            // else if($lihat["position"]=="KASIR"){
                            //         $arr["employeeAC"][$index] += ['scema' =>$incentiveKasirAC];

                            //         $arr["employeeAC"][$index] += ['PERFORMANCE%' =>$incentiveKasirSpooring];
                            //         $arr["employeeAC"][$index] += ['INCENTIVERP' =>0];


                            // }

                            //ini perlu konfirmasi dulu
                            else if($lihat["position"]=="SERVICE ADVISOR"){
                                    $arr["employeeAC"][$index] += ['scema' =>$incentiveSAAC];

                                    foreach ($arr["fullRekapBySA"] as $lihat1):
                                            if($lihat["EmployeeName"]==$lihat1["SaName"] ){

                                                $arr["employeeAC"][$index] += ['PERFORMANCE%' =>$lihat1["persentase"]];
                                                $arr["employeeAC"][$index] += ['INCENTIVERP' =>0];

                                            }
                                    endforeach;

                                    

                            }
                            // else if($lihat["position"]=="MEKANIK"){

                            //     $arr["employeeAC"][$index] += ['scema' =>0];

                                

                            //         foreach ($arr["fullRekapByMechanic"] as $lihat1):
                                        
                            //             if(array_search($lihat["EmployeeName"], array_column($arr["fullRekapByMechanic"], 'mechanicnae')) !== False){

                            //                 if($lihat["EmployeeName"]==$lihat1["mechanicname"] ){
                            //                     //echo($lihat["EmployeeName"]."<br>");
                            //                     $arr["employeeAC"][$index] += ['PERFORMANCE%' =>0];
                            //                     $arr["employeeAC"][$index] += ['INCENTIVERP' =>0];

                            //                 }
                                                    
                            //             }       
                            //             else{
                            //                     $arr["employeeAC"][$index] += ['PERFORMANCE%' =>0];
                            //                     $arr["employeeAC"][$index] += ['INCENTIVERP' =>0];
                            //             }
                            //         endforeach;

                                
                            // }

                            else {
                                    $arr["employeeAC"][$index] += ['scema' =>0];

                                    $arr["employeeAC"][$index] += ['PERFORMANCE%' =>0];
                                    $arr["employeeAC"][$index] += ['INCENTIVERP' =>0];


                            }


                }

                $index++;
            endforeach;






        } //end procedure ac


    	function calIncentiveSpooring($branchcode, $year, $month, $arrindux)
    	{
    		//$arr["spooringTotalMargin&Incentive"]=$this->mIncentiveService->getspooringTotalMargin&Incentive($branchcode, $year, $month);
    		// $arr["fullRekapBySA"]=$this->mIncentiveService->getfullRekapBySA1($branchcode, $year, $month);
    		//$arr["employeeSpooring"]=$this->mIncentiveService->getEmployeeForIncentSpooring($branchcode);


            $arr["employeeSpooring"]=$this->getemployeedata($branchcode);

            //print_r($arr["employeeSpooring"]);

                $arr["employeeSpooring1"]=$this->mIncentiveService->getEmployeeForIncentSpooring($branchcode);


                $arrayindex=count($arr["employeeSpooring"]);
                $arrayindex++;

                // $newdata =  array (
                //   'CompanyCode' => '641940',
                //   //harus diganti
                //   'BranchCode' => $branchcode,
                //   'EmployeeID' => '-',
                //   'EmployeeName'=>'-',
                //   'position'=>'PIC TEMPEL STIKER'
                // );

                //$arr["listEmployeeChemical"]=$newdata;
                //array_push($arr["listEmployeeChemical"],$newdata);

                foreach($arr["employeeSpooring1"] as $lihat) :
                      array_push($arr["employeeSpooring"],array("EmployeeName"=>$lihat["nama"], "position"=>$lihat["position"]));      
                endforeach;




    		//$arr['fullRekapByMechanic']=$this->mIncentiveService->getfullRekapByMechanic($branchcode, $year, $month);
    		$totalInvoice=0;
    		$totalMargin=0;
    		$totalTargetSpooring=100;
    		$incentiveSpooring=0;

    		foreach ($arrindux["spooringTotalMargin&Incentive"] as $lihat):
    						$totalInvoice=$lihat["Total Invoice"];
    						$totalMargin=$lihat["totalmargin"];
    		endforeach;

    		//print_r($arr["fullRekapBySA"]);

    		

    		if(round($totalInvoice/$totalTargetSpooring*100)<=100){
    			$incentiveSpooring=0;
    		}else if((round($totalInvoice/$totalTargetSpooring*100)>100) && (round($totalInvoice/$totalTargetSpooring*100)<=150)){
    			$incentiveSpooring=10;
    		}else if(round($totalInvoice/$totalTargetSpooring*100)>150){
    			$incentiveSpooring=15;
    		}

    		

    		//targetnya belum sementara langsung cal
    		$totalIncentiveSpooring=$totalMargin*$incentiveSpooring/100;
    		$index=0;
    		//print_r($totalIncentiveSpooring);

    		$incentiveSrmSpooring=10;
    		$incentiveADHSpooring=5;
    		$incentiveSMSpooring=15;
    		$incentiveSVADMSpooring=10;
    		$incentiveKasirSpooring=10;
    		$incentiveOfficeBoySpooring=10;
    		$incentiveSASpooring=40;
            $jmlofficeboy=0;

            $jmlofficeboy=$this->hitungjumlahposition($arr["employeeSpooring"],"position","OFFICEBOY");


    		foreach ($arr["employeeSpooring"] as $lihat):
    			if($incentiveSpooring<>0){
    						if($lihat["position"]=="SERVICE MANAGER"){
    								$arr["employeeSpooring"][$index] += ['scema' =>$incentiveSMSpooring];

    								$arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>$incentiveSMSpooring];
    							$arr["employeeSpooring"][$index] += ['INCENTIVERP' =>ceil($totalIncentiveSpooring*$incentiveSMSpooring/100)];

                                $arr["employeeSpooring"][$index] += ['INCENTIVERP' =>ceil($totalIncentiveSpooring*$incentiveSMSpooring/100)];


    							

    						}
    						//else if($lihat["position"]=="FD"){
                            else if($lihat["position"]=="ADH"){
    								$arr["employeeSpooring"][$index] += ['scema' =>$incentiveADHSpooring];

    								$arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>$incentiveADHSpooring];
    								$arr["employeeSpooring"][$index] += ['INCENTIVERP' =>ceil($totalIncentiveSpooring*$incentiveADHSpooring/100)];


    						}
    						//else if($lihat["position"]=="SVADM"){
                            else if($lihat["position"]=="SERVICEADMIN"){
                                
    								$arr["employeeSpooring"][$index] += ['scema' =>$incentiveSVADMSpooring];

    								$arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>$incentiveSVADMSpooring];
    								$arr["employeeSpooring"][$index] += ['INCENTIVERP' =>ceil($totalIncentiveSpooring*$incentiveSVADMSpooring/100)];


    						}


                            else if($lihat["position"]=="OFFICEBOY"){
                                
                                    $arr["employeeSpooring"][$index] += ['scema' =>$incentiveOfficeBoySpooring];

                                    $arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>100/$jmlofficeboy];
                                    $arr["employeeSpooring"][$index] += ['INCENTIVERP' =>ceil(($totalIncentiveSpooring*$incentiveOfficeBoySpooring/100)*(100/$jmlofficeboy)/100)];


                            }


    						else if($lihat["position"]=="KASIR"){
    								$arr["employeeSpooring"][$index] += ['scema' =>$incentiveKasirSpooring];

    								$arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>$incentiveKasirSpooring];
    								$arr["employeeSpooring"][$index] += ['INCENTIVERP' =>ceil($totalIncentiveSpooring*$incentiveKasirSpooring/100)];


    						}
    						else if($lihat["position"]=="SERVICE ADVISOR"){
    								$arr["employeeSpooring"][$index] += ['scema' =>$incentiveSASpooring];

    								foreach ($arrindux["fullRekapBySA"] as $lihat1):
    										if($lihat["EmployeeName"]==$lihat1["SaName"] ){

    											$arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>$lihat1["persentase"]];
    											$arr["employeeSpooring"][$index] += ['INCENTIVERP' =>ceil(($totalIncentiveSpooring*$incentiveSASpooring/100)*$lihat1["persentase"]/100)];

    										}
    								endforeach;

    								

    						}
    						else if($lihat["position"]=="MEKANIK"){

    							$arr["employeeSpooring"][$index] += ['scema' =>0];

                                //penambahan logic baru
    							
                               


    								foreach ($arrindux["fullRekapByMechanic"] as $lihat1):
    									
    									if(array_search($lihat["EmployeeName"], array_column($arrindux["fullRekapByMechanic"], 'mechanicname')) !== False){

    										if($lihat["EmployeeName"]==$lihat1["mechanicname"] ){
    											//echo($lihat["EmployeeName"]."<br>");
    											$arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>$lihat1["persentase"]];
    											$arr["employeeSpooring"][$index] += ['INCENTIVERP' =>0];

    										}
    	   	  										
    	   								}		
    									else{
    										    $arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>0];
    											$arr["employeeSpooring"][$index] += ['INCENTIVERP' =>0];
    									}
    								endforeach;
                                

    						}else {
    								$arr["employeeSpooring"][$index] += ['scema' =>0];

    								$arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>0];
    								$arr["employeeSpooring"][$index] += ['INCENTIVERP' =>0];


    						}

    			}else{

    						if($lihat["position"]=="SERVICE MANAGER"){
    								$arr["employeeSpooring"][$index] += ['scema' =>$incentiveSMSpooring];

    								$arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>$incentiveSMSpooring];
    							$arr["employeeSpooring"][$index] += ['INCENTIVERP' =>0];
    							

    						}
    						//else if($lihat["position"]=="FD"){
                                else if($lihat["position"]=="ADH"){
    								$arr["employeeSpooring"][$index] += ['scema' =>$incentiveADHSpooring];

    								$arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>$incentiveADHSpooring];
    								$arr["employeeSpooring"][$index] += ['INCENTIVERP' =>0];


    						}
    						//else if($lihat["position"]=="SVADM"){
                                else if($lihat["position"]=="SERVICEADMIN"){
    								$arr["employeeSpooring"][$index] += ['scema' =>$incentiveSVADMSpooring];

    								$arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>$incentiveSVADMSpooring];
    								$arr["employeeSpooring"][$index] += ['INCENTIVERP' =>0];


    						}

                            else if($lihat["position"]=="OFFICEBOY"){
                                
                                    $arr["employeeSpooring"][$index] += ['scema' =>$incentiveOfficeBoySpooring];

                                    $arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>100/$jmlofficeboy];
                                    $arr["employeeSpooring"][$index] += ['INCENTIVERP' =>0];


                            }

    						else if($lihat["position"]=="KASIR"){
    								$arr["employeeSpooring"][$index] += ['scema' =>$incentiveKasirSpooring];

    								$arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>$incentiveKasirSpooring];
    								$arr["employeeSpooring"][$index] += ['INCENTIVERP' =>0];


    						}
    						else if($lihat["position"]=="SERVICE ADVISOR"){
    								$arr["employeeSpooring"][$index] += ['scema' =>$incentiveSASpooring];

    								foreach ($arrindux["fullRekapBySA"] as $lihat1):
    										if($lihat["EmployeeName"]==$lihat1["SaName"] ){

    											$arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>$lihat1["persentase"]];
    											$arr["employeeSpooring"][$index] += ['INCENTIVERP' =>0];

    										}
    								endforeach;

    								

    						}
    						else if($lihat["position"]=="MEKANIK"){

    							$arr["employeeSpooring"][$index] += ['scema' =>0];

                                

    								foreach ($arrindux["fullRekapByMechanic"] as $lihat1):
    									
    									if(array_search($lihat["EmployeeName"], array_column($arrindux["fullRekapByMechanic"], 'mechanicname')) !== False){

    										if($lihat["EmployeeName"]==$lihat1["mechanicname"] ){
    											//echo($lihat["EmployeeName"]."<br>");
    											$arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>0];
    											$arr["employeeSpooring"][$index] += ['INCENTIVERP' =>0];

    										}
    	   	  										
    	   								}		
    									else{
    										    $arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>0];
    											$arr["employeeSpooring"][$index] += ['INCENTIVERP' =>0];
    									}
    								endforeach;

                                
    						}
    						else {
    								$arr["employeeSpooring"][$index] += ['scema' =>0];

    								$arr["employeeSpooring"][$index] += ['PERFORMANCE%' =>0];
    								$arr["employeeSpooring"][$index] += ['INCENTIVERP' =>0];


    						}


    			}

    			$index++;
    		endforeach;

    		//print_r($totalMargin);

    		//print_r("---------");

    		//print_r($arr["employeeSpooring"]);



    		

    		return ($arr["employeeSpooring"]);





    	}


     public function calIncentiveMSI($branchcode, $year, $month, $arrindux){

 

               
     	  //       $monthmin1=$this->getMonthMin1($year,$month);
     	  //       $monthmin2=$this->getMonthMin2($year,$month);

        //         $arrMonthmin1=explode(",",$monthmin1);
        //         $arrMonthmin2=explode(",",$monthmin2);

        //         //kemungkinan berat di get payment  dan getAllMsiDataCal1

     			// $arr["getaroutstanding"]=$this->mIncentiveService->getpaymentN($branchcode, $year, $month, $monthmin1, $monthmin2);
        //          //print_r("test incentive msi dan target rka");


        //         if($branchcode=="641940104"){
        //              $arr["calMSI"]=$this->mIncentiveService->getAllMsiDataCal($branchcode,$year,$month);
        //         }else{
        //              $arr["calMSI"]=$this->mIncentiveService->getAllMsiDataCal($branchcode,$year,$month);
        //         }
    			
                

        //         if($branchcode=="641940104"){
        //             $arr["calMSIN1"]=$this->mIncentiveService->getAllMsiDataCal($branchcode,$arrMonthmin1[1],$arrMonthmin1[0]);
        //         }else{
        //             $arr["calMSIN1"]=$this->mIncentiveService->getAllMsiDataCal($branchcode,$arrMonthmin1[1],$arrMonthmin1[0]);
        //         }


        //          if($branchcode=="641940104"){
        //             $arr["calMSIN2"]=$this->mIncentiveService->getAllMsiDataCal1($branchcode,$arrMonthmin1[1],$arrMonthmin1[0]);
        //         }else{
        //             $arr["calMSIN2"]=$this->mIncentiveService->getAllMsiDataCal($branchcode,$arrMonthmin1[1],$arrMonthmin1[0]);
        //         }
                
        //        // $arr["calMSIN2"]=$this->mIncentiveService->getAllMsiDataCal($branchcode,$arrMonthmin2[1],$arrMonthmin2[0]);


        //         $arr["targetRKA"]=$this->mIncentiveService->getTargetRKA($branchcode,$year,$month);
        //         $arr["targetRKAN1"]=$this->mIncentiveService->getTargetRKA($branchcode,$arrMonthmin1[1],(int)$arrMonthmin1[0]);
        //         $arr["targetRKAN2"]=$this->mIncentiveService->getTargetRKA($branchcode,$arrMonthmin2[1],(int)$arrMonthmin2[0]);



    			

                
    			
    			//$arr["savearoutstanding"]=

//MSI
    			$labourMSI=0;
    			$lubricantMSI=0;
    			$spareparMSI=0;

                $labourMSIN1=0;
                $lubricantMSIN1=0;
                $spareparMSIN1=0;

                $labourMSIN2=0;
                $lubricantMSIN2=0;
                $spareparMSIN2=0;
//End MSI
//RKA
    			$labourRKA=0;
    			$lubricantRKA=0;
    			$spareparRKA=0;

                $labourRKAN1=0;
                $lubricantRKAN1=0;
                $spareparRKAN1=0;

                $labourRKAN2=0;
                $lubricantRKAN2=0;
                $spareparRKAN2=0;
//end RKA
    			$partsalesrevenueMSI=0;
    			$laborMSIWarantyInternal=0;
    			$lubricantSalesChargle=0;

    			$persentaseJasa=0;
                $persentaseSparepart=0;
                $persentaseOli=0;

                $persentaseJasaN1=0;
                $persentaseSparepartN1=0;
                $persentaseOliN1=0;

                $persentaseJasaN2=0;
                $persentaseSparepartN2=0;
                $persentaseOliN2=0;

    			$totaIncentiveJasa=0;
    			$totaIncentiveSparepart=0;
    			$totaIncentiveOli=0;

    			$labourMSIPersen=0;
                $sparepartMSIPersen=0;
                $lubricantMSIPersen=0;

                $labourPaymentIncentive=0;
                $sparepartPaymentIncentive=0;
                $lubricantPaymentIncentive=0;

                $sparepartpaymentoutstandingOli=0;
                $sparepartpaymentoutstandingPart=0;
                $sparepartpaymentoutstandingJasa=0;

                $hasilPaymentLabour=0;
                $hasilPaymentLabourN1=0;
                $hasilPaymentLabourN2=0;

                $hasilPaymentPart=0;
                $hasilPaymentPartN1=0;
                $hasilPaymentPartN2=0;

                $hasilPaymentOli=0;
                $hasilPaymentOliN1=0;
                $hasilPaymentOliN2=0;

    			foreach ($arrindux["targetRKA"] as $rka):
    				if($rka["asal"]=="labour"){
    					$labourRKA=$rka["total"];
    				}else if($rka["asal"]=="sparepart"){
    					$spareparRKA=$rka["total"];
    				}else if($rka["asal"]=="lubricant"){
    					$lubricantRKA=$rka["total"];
    				}
    			endforeach;

                 //target RKA N1
                foreach ($arrindux["targetRKAN1"] as $rka):
                    if($rka["asal"]=="labour"){
                        $labourRKAN1=$rka["total"];
                    }else if($rka["asal"]=="sparepart"){
                        $spareparRKAN1=$rka["total"];
                    }else if($rka["asal"]=="lubricant"){
                        $lubricantRKAN1=$rka["total"];
                    }
                endforeach;
                //target RKA N2
                foreach ($arrindux["targetRKAN2"] as $rka):
                    if($rka["asal"]=="labour"){
                        $labourRKAN2=$rka["total"];
                    }else if($rka["asal"]=="sparepart"){
                        $spareparRKAN2=$rka["total"];
                    }else if($rka["asal"]=="lubricant"){
                        $lubricantRKAN2=$rka["total"];
                    }
                endforeach;




    			foreach ($arrindux["calMSI"] as $lihat):
    				if($lihat["asal"]=="labour MSI"){
    					$labourMSI=$lihat["total"];
    				}
    				else if($lihat["asal"]=="lubricant MSI"){
    					$lubricantMSI=$lihat["total"];
    				}
    				else if($lihat["asal"]=="sparepart MSI"){
    					$spareparMSI=$lihat["total"];
    				}
    				else if($lihat["asal"]=="Parts Sales + revenue"){
    					$partsalesrevenueMSI=$lihat["total"];
    				}
    				else if($lihat["asal"]=="labour MSI waranty + internal"){
    					$laborMSIWarantyInternal=$lihat["total"];
    				}
    				else if($lihat["asal"]=="Lubricant Sales - Chargeable"){
    					$lubricantSalesChargle=$lihat["total"];
    				}
    						    						
    			endforeach;

                foreach ($arrindux["calMSIN2"] as $lihat):
                    if($lihat["asal"]=="labour MSI"){
                        $labourMSIN2=$lihat["total"];
                    }
                    else if($lihat["asal"]=="lubricant MSI"){
                        $lubricantMSIN2=$lihat["total"];
                    }
                    else if($lihat["asal"]=="sparepart MSI"){
                        $spareparMSIN2=$lihat["total"];
                    }
                    else if($lihat["asal"]=="Parts Sales + revenue"){
                        $partsalesrevenueMSI=$lihat["total"];
                    }
                    else if($lihat["asal"]=="labour MSI waranty + internal"){
                        $laborMSIWarantyInternal=$lihat["total"];
                    }
                    else if($lihat["asal"]=="Lubricant Sales - Chargeable"){
                        $lubricantSalesChargle=$lihat["total"];
                    }
                                                        
                endforeach;


                foreach ($arrindux["calMSIN1"] as $lihat):
                    if($lihat["asal"]=="labour MSI"){
                        $labourMSIN1=$lihat["total"];
                    }
                    else if($lihat["asal"]=="lubricant MSI"){
                        $lubricantMSIN1=$lihat["total"];
                    }
                    else if($lihat["asal"]=="sparepart MSI"){
                        $spareparMSIN1=$lihat["total"];
                    }
                    else if($lihat["asal"]=="Parts Sales + revenue"){
                        $partsalesrevenueMSIN1=$lihat["total"];
                    }
                    else if($lihat["asal"]=="labour MSI waranty + internal"){
                        $laborMSIWarantyInternal=$lihat["total"];
                    }
                    else if($lihat["asal"]=="Lubricant Sales - Chargeable"){
                        $lubricantSalesChargle=$lihat["total"];
                    }
                                                        
                endforeach;

    			
    			//pesentasi dari MSI
    			$labourMSIPersen=round(($labourMSI-$laborMSIWarantyInternal)/$labourMSI*100);
                $sparepartMSIPersen=round($partsalesrevenueMSI/$spareparMSI*100);
                $lubricantMSIPersen=round($lubricantSalesChargle/$lubricantMSI*100);


                $persentaseJasa=$this->getPersenService($labourMSI,$labourRKA);
                $persentaseSparepart=$this->getPersenSparepart($spareparMSI,$spareparRKA);
                $persentaseOli=$this->getPersenOli($lubricantMSI,$lubricantRKA);

                $persentaseJasaN1=$this->getPersenService($labourMSIN1,$labourRKAN1);
                $persentaseSparepartN1=$this->getPersenSparepart($spareparMSIN1, $spareparRKAN1);
                $persentaseOliN1=$this->getPersenOli($lubricantMSIN1,$lubricantRKAN1);

                $persentaseJasaN2=$this->getPersenService($labourMSIN2,$labourRKAN2);
                $persentaseSparepartN2=$this->getPersenSparepart($spareparMSIN2,$spareparRKAN2);
                $persentaseOliN2=$this->getPersenOli($lubricantMSIN2,$lubricantRKAN2);

    			
				//total incentive
				$totaIncentiveJasa=round($persentaseJasa*$labourMSI/100);
    			$totaIncentiveSparepart=round($persentaseSparepart*$spareparMSI/100);
    			$totaIncentiveOli=round($persentaseOli*$lubricantMSI/100);
                 

    		
    			//labourPaymentINcentive
    			$labourPaymentIncentive=round($totaIncentiveJasa*$labourMSIPersen/100);
                $sparepartPaymentIncentive=round($totaIncentiveSparepart*$sparepartMSIPersen/100);
                $lubricantPaymentIncentive=round($totaIncentiveOli*$lubricantMSIPersen/100);
                
                foreach ($arrindux["getaroutstanding"] as $lihat2):
                    if($lihat2["status"]=='N'){
                         $hasilPaymentLabour=$persentaseJasa*$lihat2["labour_payment"]/100;
                         $hasilPaymentPart=$persentaseSparepart*$lihat2["part_payment"]/100;
                         $hasilPaymentOli=$persentaseOli*$lihat2["oli_payment"]/100;
                    }else if($lihat2["status"]=="N-2"){
                         $hasilPaymentLabourN2=$persentaseJasaN2*$lihat2["labour_payment"]/100;
                         $hasilPaymentPartN2=$persentaseSparepartN2*$lihat2["part_payment"]/100;
                         $hasilPaymentOliN2=$persentaseOliN2*$lihat2["oli_payment"]/100;
                    }else if($lihat2["status"]=="N-1"){
                         $hasilPaymentLabourN1=$persentaseJasaN1*$lihat2["labour_payment"]/100;
                         $hasilPaymentPartN1=$persentaseSparepartN1*$lihat2["part_payment"]/100;
                         $hasilPaymentOliN1=$persentaseOliN1*$lihat2["oli_payment"]/100;
                         $hasilpaymentpart12=$lihat2["part_payment"];
                    }
                endforeach;

                
                $labourpaymentoutstandingJasa=$hasilPaymentLabour+$hasilPaymentLabourN2+$hasilPaymentLabourN1;
                $sparepartpaymentoutstandingPart=$hasilPaymentPart+$hasilPaymentPartN2+$hasilPaymentPartN1;
                $sparepartpaymentoutstandingOli= $hasilPaymentOli+ $hasilPaymentOliN2+ $hasilPaymentOliN1;
                
                

               
               
                
                
    			//list incentive untuk proku
    			$SRM=1;
				$SERVICEMANAGER=8;
				$SERVICEADVISOR=14;//15
				$FOREMAN=9;//10
				$MEKANIK=35;
				$CCM=8;
				$ADH=1;
				$SERVICEADMIN=4;
				$SPAREPART=8;
				$WASHER=5;
				$OFFICEBOY=3;
				$KASIR=2;
                $P2K=2;

				
				
				//list array jabatan
				$arr['listjabatan'] = [
						["position"=>"SRM","scema"=>$SRM],
						["position"=>"SERVICEMANAGER","scema"=>$SERVICEMANAGER],
						["position"=>"SERVICEADVISOR","scema"=>$SERVICEADVISOR],
						["position"=>"FOREMAN","scema"=>$FOREMAN],
						["position"=>"MEKANIK","scema"=>$MEKANIK],
						["position"=>"CCM","scema"=>$CCM],
						["position"=>"ADH","scema"=>$ADH],
						["position"=>"SERVICEADMIN","scema"=>$SERVICEADMIN],
						["position"=>"SPAREPART","scema"=>$SPAREPART],
						["position"=>"WASHER","scema"=>$WASHER],
						["position"=>"OFFICEBOY","scema"=>$OFFICEBOY],
						["position"=>"KASIR","scema"=>$KASIR],
                        ["position"=>"P2K","scema"=>$P2K]
			    ];



			    $index=0;

			    foreach ($arr["listjabatan"] as $lihat):
			    	//if($persentaseJasa<>0){
			    		/*$arr["listjabatan"][$index] += ['labour_incentive' =>round($lihat["scema"]*$labourPaymentIncentive/100)];
    				    $arr["listjabatan"][$index] += ['labour_paid' =>round($lihat["scema"]*$labourpaymentoutstandingJasa/100)];
                        */
                        $arr["listjabatan"][$index] += ['labour_incentive' =>round($lihat["scema"]*$totaIncentiveJasa/100)];
                        $arr["listjabatan"][$index] += ['labour_paid' =>round($lihat["scema"]*$labourpaymentoutstandingJasa/100)];
			    	//}else{
			    		//$arr["listjabatan"][$index] += ['labour_incentive' =>0];
    				   // $arr["listjabatan"][$index] += ['labour_paid' =>0];
			    	//}

			    	//if($persentaseSparepart<>0){
			    		/*$arr["listjabatan"][$index] += ['sparepart_incentive' =>round($lihat["scema"]*$sparepartPaymentIncentive/100)];
    				    $arr["listjabatan"][$index] += ['sparepart_paid' =>round($lihat["scema"]*$sparepartpaymentoutstandingPart/100)];
                        */

                        $arr["listjabatan"][$index] += ['sparepart_incentive' =>round($lihat["scema"]*$totaIncentiveSparepart/100)];
                        $arr["listjabatan"][$index] += ['sparepart_paid' =>round($lihat["scema"]*$sparepartpaymentoutstandingPart/100)];
			    	//}else{
			    		//$arr["listjabatan"][$index] += ['sparepart_incentive' =>0];
    				    //$arr["listjabatan"][$index] += ['sparepart_paid' =>0];
			    	//}

			    	//if($persentaseOli<>0){
			    		//$arr["listjabatan"][$index] += ['lubricant_incentive' =>round($lihat["scema"]*$lubricantPaymentIncentive/100)];


                        $arr["listjabatan"][$index] += ['lubricant_incentive' =>round($lihat["scema"]*$totaIncentiveOli/100)];
    				    $arr["listjabatan"][$index] += ['lubricant_paid' =>round($lihat["scema"]*$sparepartpaymentoutstandingOli/100)];
			    	//}else{
			    		//$arr["listjabatan"][$index] += ['sparepart_incentive' =>0];
    				    //$arr["listjabatan"][$index] += ['lubricant_paid' =>0];
			    	//}

			    $index++;
			    endforeach;

               // print_r("rekap cal inventive");



				return ($arr['listjabatan'] );

				//return($arr["listjabatan"]);
				//print_r(("labour :".round($labourMSI/$labourRKA*100). ", labourRKA : ".$labourMSI.", persentase :".round($labourMSI/$labourRKA*100)));
				


    			
    	}

    	public function getbymechanic(){
    		$nama="";
    		$arr["fullmechanic"]=$this->mIncentiveService->getfullRekapByMechanic(641940103,2022,4);
    		foreach ($arr["fullmechanic"] as $lihat):
    				if($lihat["mechanicname"]=="PRABOWO CAHYO ENDRO") {
    					$nama=$lihat["mechanicname"];
    				}
    	    endforeach;

    	   if(array_search("SYAHRIL LICKY SHAHAB1", array_column($arr["fullmechanic"], 'mechanicname')) !== False){
    	   	  echo "ada";
    	   }else{
    	   	  echo "tidak ada";
    	   }

    		
    		//print_r($arr["fullmechanic"]);
    	}

    function getMonthMin1($year,$month){
    	 if(($month)==2){
		     return ("01,".$year);
		   // echo "12".$year-1;
		      
		  }else if ($month==1){
		  	return ("12,".($year-1));
           
		    //echo "11".($year-1)." ";
		  }else{
            if(($month-1)<10){
                
              return ("0".($month-1).",".$year);
            } else{
                
               return (($month-1).",".$year); 
            }
		    
		  }
    }

    function getMonthMin2($year,$month){
    	 if(($month)==2){
		   return ("12,".($year-1));
		      
		  }else if ($month==1){
		    return ("11,".($year-1));
		  }else{
            if(($month-2)<10){
                return("0".($month-2).",".$year);
            }else{
                return (($month-2).",".$year);
            }
		    
		  }
    }


    function getPersenService($MSI,$RKA){
        if(round($MSI/$RKA*100)<=70){
                        return 0;
                }else if((round($MSI/$RKA*100)>70) && (round($MSI/$RKA*100)<=80)){
                         //return 10;
                         return 0;
                }else if((round($MSI/$RKA*100)>=81) && (round($MSI/$RKA*100)<=100)){
                        return 12;
                        //return 10;
                }else if((round($MSI/$RKA*100)>=101) && (round($MSI/$RKA*100)<=120)){
                        return 14;
                }else if((round($MSI/$RKA*100)>=121)){
                        return 15;
                }
    }


    function getPersenSparepart($MSI,$RKA){
        if(round($MSI/$RKA*100)<=70){
                        return 0;
                }else if((round($MSI/$RKA*100)>70) && (round($MSI/$RKA*100)<=80)){
                       //return 4;
                       return 0;
                }else if((round($MSI/$RKA*100)>=81) && (round($MSI/$RKA*100)<=100)){
                        return 5;
                }else if((round($MSI/$RKA*100)>=101) && (round($MSI/$RKA*100)<=120)){
                        return 6;
                }else if((round($MSI/$RKA*100)>=121)){
                        return 7;
                }
    }


    function getPersenOli($MSI,$RKA){
        if(round($MSI/$RKA*100)<=70){
                        return 0;
                }else if((round($MSI/$RKA*100)>70) && (round($MSI/$RKA*100)<=80)){
                        //return 4;
                        return 0;
                }else if((round($MSI/$RKA*100)>=81) && (round($MSI/$RKA*100)<=100)){
                        return 5;
                }else if((round($MSI/$RKA*100)>=101) && (round($MSI/$RKA*100)<=120)){
                        return 6;
                }else if((round($MSI/$RKA*100)>=121)){
                        return 7;
                }
    }


     public function indukProku($branchcode, $year, $month, $arrindux){
       

                

       //          $monthmin1=$this->getMonthMin1($year,$month);
       //          $monthmin2=$this->getMonthMin2($year,$month);

       //          $arrMonthmin1=explode(",",$monthmin1);
       //          $arrMonthmin2=explode(",",$monthmin2);

       //          $arr["getaroutstanding"]=$this->mIncentiveService->getpaymentN($branchcode, $year, $month, $monthmin1, $monthmin2);

       //          print_r(1);

       //          if($branchcode=="641940104"){
       //              $arr["calMSI"]=$this->mIncentiveService->getAllMsiDataCal1($branchcode,$year,$month);

       //          }else{
       //              $arr["calMSI"]=$this->mIncentiveService->getAllMsiDataCal($branchcode,$year,$month);

       //          }

       //          print_r(2);

    		    
       //          if($branchcode=="641940104"){
       //                  $arr["calMSIN1"]=$this->mIncentiveService->getAllMsiDataCal1($branchcode,$arrMonthmin1[1],$arrMonthmin1[0]);
       //          } else{
       //              $arr["calMSIN1"]=$this->mIncentiveService->getAllMsiDataCal($branchcode,$arrMonthmin1[1],$arrMonthmin1[0]);
       //          }

       //          print_r(2);

       //          $arr["calMSIN2"]=$this->mIncentiveService->getAllMsiDataCal($branchcode,$arrMonthmin2[1],$arrMonthmin2[0]);

       //          print_r(2);

    			// $arr["targetRKA"]=$this->mIncentiveService->getTargetRKA($branchcode,$year,$month);
       //          $arr["targetRKAN1"]=$this->mIncentiveService->getTargetRKA($branchcode,$arrMonthmin1[1],(int)$arrMonthmin1[0]);
       //          $arr["targetRKAN2"]=$this->mIncentiveService->getTargetRKA($branchcode,$arrMonthmin2[1],(int)$arrMonthmin2[0]);

    			$labourMSI=0;
    			$lubricantMSI=0;
    			$spareparMSI=0;

                $labourMSIN1=0;
                $lubricantMSIN1=0;
                $spareparMSIN1=0;

                $labourMSIN2=0;
                $lubricantMSIN2=0;
                $spareparMSIN2=0;

                $labourMSI=0;
                $lubricantMSI=0;
                $spareparMSI=0;

    			$labourRKA=0;
    			$lubricantRKA=0;
    			$spareparRKA=0;

                $labourRKAN1=0;
                $lubricantRKAN1=0;
                $spareparRKAN1=0;

                $labourRKAN2=0;
                $lubricantRKAN2=0;
                $spareparRKAN2=0;

    			$partsalesrevenueMSI=0;
    			$laborMSIWarantyInternal=0;
    			$lubricantSalesChargle=0;

    			$persentaseJasa=0;
    			$persentaseSparepart=0;
    			$persentaseOli=0;

                $persentaseJasaN1=0;
                $persentaseSparepartN1=0;
                $persentaseOliN1=0;

                $persentaseJasaN2=0;
                $persentaseSparepartN2=0;
                $persentaseOliN2=0;

    			$totaIncentiveJasa=0;
    			$totaIncentiveSparepart=0;
    			$totaIncentiveOli=0;

    			$labourMSIPersen=0;
                $sparepartMSIPersen=0;
                $lubricantMSIPersen=0;


                $labourPaymentN=0;
                $partPaymentN=0;
                $oliPaymentN=0;

                $labourPaymentN1=0;
                $partPaymentN1=0;
                $oliPaymentN1=0;

                $labourPaymentN2=0;
                $partPaymentN2=0;
                $oliPaymentN2=0;

                $achivmentLabour=0;
                $achivmentsparepart=0;
                $achivmentlubricant=0;

                $achivmentLabourN1=0;
                $achivmentsparepartN1=0;
                $achivmentlubricantN1=0;

                $achivmentLabourN2=0;
                $achivmentsparepartN2=0;
                $achivmentlubricantN2=0;

    			foreach ($arrindux["targetRKA"] as $rka):
    				if($rka["asal"]=="labour"){
    					$labourRKA=$rka["total"];
    				}else if($rka["asal"]=="sparepart"){
    					$spareparRKA=$rka["total"];
    				}else if($rka["asal"]=="lubricant"){
    					$lubricantRKA=$rka["total"];
    				}
    			endforeach;
                //target RKA N1
                foreach ($arrindux["targetRKAN1"] as $rka):
                    if($rka["asal"]=="labour"){
                        $labourRKAN1=$rka["total"];
                    }else if($rka["asal"]=="sparepart"){
                        $spareparRKAN1=$rka["total"];
                    }else if($rka["asal"]=="lubricant"){
                        $lubricantRKAN1=$rka["total"];
                    }
                endforeach;
                //target RKA N2
                foreach ($arrindux["targetRKAN2"] as $rka):
                    if($rka["asal"]=="labour"){
                        $labourRKAN2=$rka["total"];
                    }else if($rka["asal"]=="sparepart"){
                        $spareparRKAN2=$rka["total"];
                    }else if($rka["asal"]=="lubricant"){
                        $lubricantRKAN2=$rka["total"];
                    }
                endforeach;

    			foreach ($arrindux["calMSI"] as $lihat):
    				if($lihat["asal"]=="labour MSI"){
    					$labourMSI=$lihat["total"];
    				}
    				else if($lihat["asal"]=="lubricant MSI"){
    					$lubricantMSI=$lihat["total"];
    				}
    				else if($lihat["asal"]=="sparepart MSI"){
    					$spareparMSI=$lihat["total"];
    				}
    				else if($lihat["asal"]=="Parts Sales + revenue"){
    					$partsalesrevenueMSI=$lihat["total"];
    				}
    				else if($lihat["asal"]=="labour MSI waranty + internal"){
    					$laborMSIWarantyInternal=$lihat["total"];
    				}
    				else if($lihat["asal"]=="Lubricant Sales - Chargeable"){
    					$lubricantSalesChargle=$lihat["total"];
    				}
    						    						
    			endforeach;


                foreach ($arrindux["calMSIN2"] as $lihat):
                    if($lihat["asal"]=="labour MSI"){
                        $labourMSIN2=$lihat["total"];
                    }
                    else if($lihat["asal"]=="lubricant MSI"){
                        $lubricantMSIN2=$lihat["total"];
                    }
                    else if($lihat["asal"]=="sparepart MSI"){
                        $spareparMSIN2=$lihat["total"];
                    }
                   /* else if($lihat["asal"]=="Parts Sales + revenue"){
                        $partsalesrevenueMSI=$lihat["total"];
                    }
                    else if($lihat["asal"]=="labour MSI waranty + internal"){
                        $laborMSIWarantyInternal=$lihat["total"];
                    }
                    else if($lihat["asal"]=="Lubricant Sales - Chargeable"){
                        $lubricantSalesChargle=$lihat["total"];
                    }*/
                                                        
                endforeach;


                foreach ($arrindux["calMSIN1"] as $lihat):
                    if($lihat["asal"]=="labour MSI"){
                        $labourMSIN1=$lihat["total"];
                    }
                    else if($lihat["asal"]=="lubricant MSI"){
                        $lubricantMSIN1=$lihat["total"];
                    }
                    else if($lihat["asal"]=="sparepart MSI"){
                        $spareparMSIN1=$lihat["total"];
                    }
                   /* else if($lihat["asal"]=="Parts Sales + revenue"){
                        $partsalesrevenueMSIN1=$lihat["total"];
                    }
                    else if($lihat["asal"]=="labour MSI waranty + internal"){
                        $laborMSIWarantyInternal=$lihat["total"];
                    }
                    else if($lihat["asal"]=="Lubricant Sales - Chargeable"){
                        $lubricantSalesChargle=$lihat["total"];
                    }*/
                                                        
                endforeach;


                foreach ($arrindux["getaroutstanding"] as $lihat):
                    if($lihat["status"]=="N-1"){
                        $labourPaymentN1=$lihat["labour_payment"];
                        $partPaymentN1=$lihat["part_payment"];
                        $oliPaymentN1=$lihat["oli_payment"];
                    }
                    else if($lihat["status"]=="N-2"){
                        $labourPaymentN2=$lihat["labour_payment"];
                        $partPaymentN2=$lihat["part_payment"];
                        $oliPaymentN2=$lihat["oli_payment"];
                    }
                    else if($lihat["status"]=="N"){
                        $labourPaymentN=$lihat["labour_payment"];
                        $partPaymentN=$lihat["part_payment"];
                        $oliPaymentN=$lihat["oli_payment"];
                    }
                    
                                                        
                endforeach;

                //print_r('induk proku '.$labourPaymentN.' , '.$labourPaymentN1.' , '.$labourPaymentN2);

               
    			
    			//pesentasi dari MSI
    			$labourMSIPersen=round(($labourMSI-$laborMSIWarantyInternal)/$labourMSI*100);
                $sparepartMSIPersen=round($partsalesrevenueMSI/$spareparMSI*100);
                $lubricantMSIPersen=round($lubricantSalesChargle/$lubricantMSI*100);

               // print_r("labourMSI ".$labourMSI." labourRKA".$labourRKA);

                //achivment pembagian RKA & Reveneu
                if($labourRKA<>0 || $labourMSI<>0 ){
                            $achivmentLabour=round($labourMSI/$labourRKA*100);
                }else{
                            $achivmentLabour=0;
                }

                if($spareparRKA<>0 || $spareparMSI<>0 ){
                          $achivmentsparepart=round($spareparMSI/$spareparRKA*100);
                }else{
                          $achivmentsparepart=0;
                }

                if($lubricantRKA<>0 || $lubricantMSI<>0 ){
                          $achivmentlubricant=round($lubricantMSI/$lubricantRKA*100);
                }else{
                         $achivmentlubricant=0;
                }


    			//perhitungan service
                $persentaseJasa=$this->getPersenService($labourMSI,$labourRKA);
                $persentaseSparepart=$this->getPersenSparepart($spareparMSI,$spareparRKA);
                $persentaseOli=$this->getPersenOli($lubricantMSI,$lubricantRKA);

                $persentaseJasaN1=$this->getPersenService($labourMSIN1,$labourRKAN1);
                $persentaseSparepartN1=$this->getPersenSparepart($spareparMSIN1, $spareparRKAN1);
                $persentaseOliN1=$this->getPersenOli($lubricantMSIN1,$lubricantRKAN1);

                $persentaseJasaN2=$this->getPersenService($labourMSIN2,$labourRKAN2);
                $persentaseSparepartN2=$this->getPersenSparepart($spareparMSIN2,$spareparRKAN2);
                $persentaseOliN2=$this->getPersenOli($lubricantMSIN2,$lubricantRKAN2);

                

				//total incentive
				$totaIncentiveJasa=round($persentaseJasa*$labourMSI/100);
    			$totaIncentiveSparepart=round($persentaseSparepart*$spareparMSI/100);
    			$totaIncentiveOli=round($persentaseOli*$lubricantMSI/100);



    			$arr['indukProku'] = [
						["Jenis"=>"Target ","labour"=>$labourRKA, "sparepart"=>$spareparRKA, "lubricant"=>$lubricantRKA],

						["Jenis"=>"Revenue ","labour"=>$labourMSI, "sparepart"=>$spareparMSI, "lubricant"=>$lubricantMSI],

						["Jenis"=>"Achivment ","labour"=>$achivmentLabour, "sparepart"=>$achivmentsparepart, "lubricant"=>$achivmentlubricant],

						["Jenis"=>"Incentive ","labour"=>$totaIncentiveJasa. " (".$persentaseJasa."%)", "sparepart"=>$totaIncentiveSparepart." (".$persentaseSparepart."%)", "lubricant"=>$totaIncentiveOli." (".$persentaseOli."%)"],

						["Jenis"=>"Payment N ","labour"=> $labourPaymentN." (".round($labourPaymentN/$labourMSI*100)."%)" ,"sparepart"=> $partPaymentN." (".round($partPaymentN/$spareparMSI*100)."%)", "lubricant"=>$oliPaymentN." (".round($oliPaymentN/$lubricantMSI*100)."%)" ],

                        ["Jenis"=>"Payment N-1 ","labour"=> $labourPaymentN1.'('.$persentaseJasaN1.'%)' ,"sparepart"=> $partPaymentN1 .'('.$persentaseSparepartN1.'%)', "lubricant"=>$oliPaymentN1.'('.$persentaseOliN1.'%)' ],

                         ["Jenis"=>"Payment N-2 ","labour"=> $labourPaymentN2.'('.$persentaseJasaN2.'%)' ,"sparepart"=> $partPaymentN2 .'('.$persentaseSparepartN2.'%)', "lubricant"=>$oliPaymentN2.'('.$persentaseOliN2.'%)' ],
						
			    ];

               // print_r("induk proku function");

			    // print_r($arr['indukProku'])	;
			   return ($arr['indukProku']);

    	}


        public function pengajuanServiceAwal($bulan,$tahun){
        $result=$this->mIncentiveService->pengajuanServiceAwal($bulan,$tahun);
          if($result=="1"){
                $this->session->set_flashdata('sro_berhasil', "berhasil");
                
          }else if($result=="3"){
                $this->session->set_flashdata('sro_ada', "sro_ada");
                
          }else{
                $this->session->set_flashdata('sro_gagal', "gagal");
          }
            
          redirect('cdetail_incenvite_sro/tampil','refresh');
            
            
        
    }

    public function rekapIncentive($branchcode, $year, $month, $arrindux){

        // $arr["fullRekapBySA"]=$this->mIncentiveService->getfullRekapBySA1($branchcode, $year, $month);
        // $arr["fullRekapByMechanic"]=$this->mIncentiveService->getfullRekapByMechanic($branchcode, $year, $month);

        // $arr["spooringTotalMargin&Incentive"]=$this->mIncentiveService->getspooringTotalMargin&Incentive($branchcode, $year, $month);

        // $arr["chemicalTotalMargin&Incentive"]=$this->mIncentiveService->getfullIncentiveChemical($branchcode, $year, $month);

        //$arr["listincentivechemical"]=$this->calInsentiveChemical($branchcode, $year, $month, $arrindux);

         $arr["fullcalincentiveProku"]=$this->calIncentiveMSI($branchcode, $year, $month, $arrindux);


        //perhitungan spooring
            $totalInvoice=0;
            $totalMargin=0;
            $totalTargetSpooring=100;
            $incentiveSpooring=0;

            $totalIncentiveLabourProku=0;
            $totalIncentivePartProku=0;
            $totalIncentiveOliProku=0;

            foreach ($arrindux["spooringTotalMargin&Incentive"] as $lihat):
                            $totalInvoice=$lihat["Total Invoice"];
                            $totalMargin=$lihat["totalmargin"];
            endforeach;

            foreach($arr["fullcalincentiveProku"] as $fullProku):
                    $totalIncentiveLabourProku=$totalIncentiveLabourProku+$fullProku["labour_paid"];
                    $totalIncentivePartProku=$totalIncentivePartProku+$fullProku['sparepart_paid'];
                    $totalIncentiveOliProku=$totalIncentiveOliProku+$fullProku["lubricant_paid"];

            endforeach;

            //print_r(" dtotalIncentiveLabourProkuta ".$totalIncentiveLabourProku);

            //print_r($arr["fullRekapBySA"]);

            

            if(round($totalInvoice/$totalTargetSpooring*100)<=100){
                $incentiveSpooring=0;
            }else if((round($totalInvoice/$totalTargetSpooring*100)>100) && (round($totalInvoice/$totalTargetSpooring*100)<=150)){
                $incentiveSpooring=10;
            }else if(round($totalInvoice/$totalTargetSpooring*100)>150){
                $incentiveSpooring=15;
            }

            

            //targetnya belum sementara langsung cal
            $totalIncentiveSpooring=$totalMargin*$incentiveSpooring/100;

        //batas perhitungan spooring



        $SRM=1;
        $SERVICEMANAGER=8;
        $FD=1;
        $SERVICEADMIN=4;
        //persentase
        $UMUM=8;
        $SVCCM=0;
        $ADMCCM=0;
        $SRO=0;
        $CRO=0;
        $KASIR=2;
        $SPAREPART=8;
        $SERVICEADVISOR=14;//15
        $FOREMAN=9;//10
        $MEKANIK=35;
        $PIC_STICKER=0;
        $WASHER=5;
        $OFFICEBOY=3;
        $P2K=2;//15;

        //INCENTIVE SPOORING BALANCING
        $SRMSB=10;
        $SERVICEMANAGERSB=15;
        $FDSB=5;
        $SERVICEADMINSB=10;
        //persentase
        $UMUMSB=0;
        $SVCCMSB=0;
        $ADMCCMSB=0;
        $SROSB=0;
        $CROSB=0;
        $KASIRSB=10;
        $SPAREPARTSB=0;
        $SERVICEADVISORSB=40;
        $FOREMANSB=0;
        $MEKANIKSB=0;
        $PIC_STICKERSB=0;
        $WASHERSB=0;
        $OFFICEBOYSB=10;
        $P2KSB=0;

        if($branchcode=="641940103"){

        $arr['listjabatan'] = [
            ["nama"=>"HERI","position"=>"SRM", "scema"=>$SRM, "%MSI"=>100,  "scemaSB"=>$SRMSB, "%sb"=>100],
            ["nama"=>"KUSDIANTORO", "position"=>"SERVICE MANAGER", "scema"=>$SERVICEMANAGER, "%MSI"=>100, "scemaSB"=>$SERVICEMANAGERSB, "%sb"=>100],
            ["nama"=>"ANISSA", "position"=>"ADH", "scema"=>$FD, "%MSI"=>100, "scemaSB"=>$FDSB, "%sb"=>100],
            ["nama"=>"SRI NOPIA LESTARI", "position"=>"SERVICEADMIN", "scema"=>$SERVICEADMIN, "%MSI"=>100, "scemaSB"=>$SERVICEADMINSB, "%sb"=>100],
            ["nama"=>"NAUFAL", "position"=>"SPVCCM", "scema"=>$UMUM, "%MSI"=>5, "scemaSB"=>$UMUMSB,"%sb"=>100],
            ["nama"=>"ASTI", "position"=>"ADMCCM", "scema"=>$UMUM, "%MSI"=>10, "scemaSB"=>$UMUMSB,"%sb"=>100],
            ["nama"=>"DINI NOVIA FIRANDANI", "position"=>"SRO", "scema"=>$UMUM, "%MSI"=>75, "scemaSB"=>$UMUMSB,"%sb"=>100],
            ["nama"=>"RIDHA RIHHADATUL ASYI", "position"=>"CRO", "scema"=>$UMUM, "%MSI"=>10, "scemaSB"=>$UMUMSB, "%sb"=>100],
            ["nama"=>"LUSIA YANTINI", "position"=>"KASIR", "scema"=>$KASIR, "%MSI"=>100, "scemaSB"=>$KASIRSB, "%sb"=>100],
            ["nama"=>"AVISENA ROSADY", "position"=>"ADMINISTRASI SPAREPART", "scema"=>$SPAREPART, "%MSI"=>40, "scemaSB"=>$SPAREPARTSB, "%sb"=>100],
            ["nama"=>"NOVELLIA PENGESTI", "position"=>"ADMINISTRASI SPAREPART", "scema"=>$SPAREPART, "%MSI"=>60, "scemaSB"=>$SPAREPARTSB, "%sb"=>100],
            ["nama"=>"VICKY HANGGARA", "position"=>"FOREMAN", "scema"=>$FOREMAN, "%MSI"=>100, "scemaSB"=>$FOREMANSB, "%sb"=>100],
            ["nama"=>"OPIK", "position"=>"PIC TEMPEL STIKER", "scema"=>$PIC_STICKER, "%MSI"=>0, "scemaSB"=>$PIC_STICKERSB, "%sb"=>100],
            ["nama"=>"SLAMET", "position"=>"WASHER", "scema"=>$WASHER, "%MSI"=>50, "scemaSB"=>$WASHERSB, "%sb"=>100],
            ["nama"=>"USEP", "position"=>"WASHER", "scema"=>$WASHER, "%MSI"=>50, "scemaSB"=>$WASHERSB, "%sb"=>100],
            ["nama"=>"FAUZI", "position"=>"OFFICEBOY", "scema"=>$OFFICEBOY, "%MSI"=>50, "scemaSB"=>$OFFICEBOYSB, "%sb"=>50],
            ["nama"=>"MARWAN","position"=>"OFFICEBOY", "scema"=>$OFFICEBOY, "%MSI"=>50, "scemaSB"=>$OFFICEBOYSB, "%sb"=>50],
            ["nama"=>"P2K","position"=>"P2K", "scema"=>$P2K, "%MSI"=>100, "scemaSB"=>$P2KSB, "%sb"=>100]
                        
        ];

       } else if($branchcode=="641940102"){

        $arr['listjabatan'] = [
            ["nama"=>"","position"=>"SRM", "scema"=>$SRM, "%MSI"=>100,  "scemaSB"=>$SRMSB, "%sb"=>100],
            ["nama"=>"GANDA MULYADI", "position"=>"SERVICE MANAGER", "scema"=>$SERVICEMANAGER, "%MSI"=>100, "scemaSB"=>$SERVICEMANAGERSB, "%sb"=>100],
            ["nama"=>"BUDI TANUJAYA", "position"=>"ADH", "scema"=>$FD, "%MSI"=>100, "scemaSB"=>$FDSB, "%sb"=>100],
            ["nama"=>"AGUSTINA WULANDARI", "position"=>"SERVICEADMIN", "scema"=>$SERVICEADMIN, "%MSI"=>100, "scemaSB"=>$SERVICEADMINSB, "%sb"=>100],
            ["nama"=>"NAUFAL", "position"=>"SPVCCM", "scema"=>$UMUM, "%MSI"=>5, "scemaSB"=>$UMUMSB,"%sb"=>100],
            ["nama"=>"ASTI", "position"=>"ADMCCM", "scema"=>$UMUM, "%MSI"=>10, "scemaSB"=>$UMUMSB,"%sb"=>100],
            ["nama"=>"PUTI", "position"=>"SRO", "scema"=>$UMUM, "%MSI"=>37.5, "scemaSB"=>$UMUMSB,"%sb"=>100],
            ["nama"=>"ANISSA", "position"=>"SRO", "scema"=>$UMUM, "%MSI"=>37.5, "scemaSB"=>$UMUMSB,"%sb"=>100],
            ["nama"=>"NURUL", "position"=>"CRO", "scema"=>$UMUM, "%MSI"=>10, "scemaSB"=>$UMUMSB, "%sb"=>100],
            ["nama"=>"LINDA", "position"=>"KASIR", "scema"=>$KASIR, "%MSI"=>100, "scemaSB"=>$KASIRSB, "%sb"=>100],
            ["nama"=>"TJEN TJEN", "position"=>"ADMINISTRASI SPAREPART", "scema"=>$SPAREPART, "%MSI"=>50, "scemaSB"=>$SPAREPARTSB, "%sb"=>100],
            ["nama"=>"ERNA DEVIANA", "position"=>"ADMINISTRASI SPAREPART", "scema"=>$SPAREPART, "%MSI"=>50, "scemaSB"=>$SPAREPARTSB, "%sb"=>100],
            ["nama"=>"YADI H", "position"=>"FOREMAN", "scema"=>$FOREMAN, "%MSI"=>100, "scemaSB"=>$FOREMANSB, "%sb"=>100],
            ["nama"=>"OPIK", "position"=>"PIC TEMPEL STIKER", "scema"=>$PIC_STICKER, "%MSI"=>0, "scemaSB"=>$PIC_STICKERSB, "%sb"=>100],
            ["nama"=>"HASRI KUSWIRA", "position"=>"WASHER", "scema"=>$WASHER, "%MSI"=>50, "scemaSB"=>$WASHERSB, "%sb"=>100],
            ["nama"=>"ANDI YUSTANDI", "position"=>"WASHER", "scema"=>$WASHER, "%MSI"=>50, "scemaSB"=>$WASHERSB, "%sb"=>100],
            ["nama"=>"YOPI", "position"=>"OFFICEBOY", "scema"=>$OFFICEBOY, "%MSI"=>50, "scemaSB"=>$OFFICEBOYSB, "%sb"=>50],
            ["nama"=>"DEDE","position"=>"OFFICEBOY", "scema"=>$OFFICEBOY, "%MSI"=>50, "scemaSB"=>$OFFICEBOYSB, "%sb"=>50],
            ["nama"=>"P2K","position"=>"P2K", "scema"=>$P2K, "%MSI"=>100, "scemaSB"=>$P2KSB, "%sb"=>100]

        ];

       } else if($branchcode=="641940101"){
  
       $arr['listjabatan'] = [
            ["nama"=>"HERI","position"=>"SRM", "scema"=>$SRM, "%MSI"=>100, "scemaSB"=>$SRMSB, "%sb"=>100],
            ["nama"=>"Heri Setiawan", "position"=>"SERVICE MANAGER", "scema"=>$SERVICEMANAGER, "%MSI"=>100, "scemaSB"=>$SERVICEMANAGERSB, "%sb"=>100],
            ["nama"=>"Clarisa", "position"=>"ADH", "scema"=>$FD, "%MSI"=>100, "scemaSB"=>$FDSB, "%sb"=>100],
            ["nama"=>"Pepen Supendi", "position"=>"SERVICEADMIN", "scema"=>$SERVICEADMIN, "%MSI"=>100, "scemaSB"=>$SERVICEADMINSB, "%sb"=>100],
            ["nama"=>"NAUFAL", "position"=>"SPVCCM", "scema"=>$UMUM, "%MSI"=>5, "scemaSB"=>$UMUMSB,"%sb"=>100],
            ["nama"=>"ASTI", "position"=>"ADMCCM", "scema"=>$UMUM, "%MSI"=>10, "scemaSB"=>$UMUMSB,"%sb"=>100],
            ["nama"=>"IKRIMA", "position"=>"SRO", "scema"=>$UMUM, "%MSI"=>75, "scemaSB"=>$UMUMSB,"%sb"=>100],
            ["nama"=>"Anas Tasya", "position"=>"CRO", "scema"=>$UMUM, "%MSI"=>10, "scemaSB"=>$UMUMSB, "%sb"=>100],
            ["nama"=>"Eric Febrian", "position"=>"KASIR", "scema"=>$KASIR, "%MSI"=>100, "scemaSB"=>$KASIRSB, "%sb"=>100],
            ["nama"=>"Taufik", "position"=>"ADMINISTRASI SPAREPART", "scema"=>$SPAREPART, "%MSI"=>40, "scemaSB"=>$SPAREPARTSB, "%sb"=>100],
            ["nama"=>"Nunik", "position"=>"ADMINISTRASI SPAREPART", "scema"=>$SPAREPART, "%MSI"=>60, "scemaSB"=>$SPAREPARTSB, "%sb"=>100],
            ["nama"=>"JUANDA", "position"=>"FOREMAN", "scema"=>$FOREMAN, "%MSI"=>50, "scemaSB"=>$FOREMANSB, "%sb"=>100],
            ["nama"=>"Tajudin", "position"=>"FOREMAN", "scema"=>$FOREMAN, "%MSI"=>50, "scemaSB"=>$FOREMANSB, "%sb"=>100],
            ["nama"=>"OPIK", "position"=>"PIC TEMPEL STIKER", "scema"=>$PIC_STICKER, "%MSI"=>0, "scemaSB"=>$PIC_STICKERSB, "%sb"=>100],
            ["nama"=>"Fadlan", "position"=>"WASHER", "scema"=>$WASHER, "%MSI"=>50, "scemaSB"=>$WASHERSB, "%sb"=>100],
            ["nama"=>"Satria", "position"=>"WASHER", "scema"=>$WASHER, "%MSI"=>50, "scemaSB"=>$WASHERSB, "%sb"=>100],
            ["nama"=>"Eko", "position"=>"OFFICEBOY", "scema"=>$OFFICEBOY, "%MSI"=>50, "scemaSB"=>$OFFICEBOYSB, "%sb"=>50],
            ["nama"=>"Yudi","position"=>"OFFICEBOY", "scema"=>$OFFICEBOY, "%MSI"=>50, "scemaSB"=>$OFFICEBOYSB, "%sb"=>50],
            ["nama"=>"P2K","position"=>"P2K", "scema"=>$P2K, "%MSI"=>100, "scemaSB"=>$P2KSB, "%sb"=>100]
                        
        ];

       }else if($branchcode=="641940104"){
        //belum dirubah data namanya
       $arr['listjabatan'] = [
            ["nama"=>"HERI","position"=>"SRM", "scema"=>$SRM, "%MSI"=>100, "scemaSB"=>$SRMSB, "%sb"=>100],
            ["nama"=>"Heri Setiawan", "position"=>"SERVICE MANAGER", "scema"=>$SERVICEMANAGER, "%MSI"=>100, "scemaSB"=>$SERVICEMANAGERSB, "%sb"=>100],
            ["nama"=>"Clarisa", "position"=>"ADH", "scema"=>$FD, "%MSI"=>100, "scemaSB"=>$FDSB, "%sb"=>100],
            ["nama"=>"Pepen Supendi", "position"=>"SERVICEADMIN", "scema"=>$SERVICEADMIN, "%MSI"=>100, "scemaSB"=>$SERVICEADMINSB, "%sb"=>100],
            ["nama"=>"NAUFAL", "position"=>"SPVCCM", "scema"=>$UMUM, "%MSI"=>5, "scemaSB"=>$UMUMSB,"%sb"=>100],
            ["nama"=>"ASTI", "position"=>"ADMCCM", "scema"=>$UMUM, "%MSI"=>10, "scemaSB"=>$UMUMSB,"%sb"=>100],
            ["nama"=>"IKRIMA", "position"=>"SRO", "scema"=>$UMUM, "%MSI"=>75, "scemaSB"=>$UMUMSB,"%sb"=>100],
            ["nama"=>"Anas Tasya", "position"=>"CRO", "scema"=>$UMUM, "%MSI"=>10, "scemaSB"=>$UMUMSB, "%sb"=>100],
            ["nama"=>"Eric Febrian", "position"=>"KASIR", "scema"=>$KASIR, "%MSI"=>100, "scemaSB"=>$KASIRSB, "%sb"=>100],
            ["nama"=>"Taufik", "position"=>"ADMINISTRASI SPAREPART", "scema"=>$SPAREPART, "%MSI"=>40, "scemaSB"=>$SPAREPARTSB, "%sb"=>100],
            ["nama"=>"Nunik", "position"=>"ADMINISTRASI SPAREPART", "scema"=>$SPAREPART, "%MSI"=>60, "scemaSB"=>$SPAREPARTSB, "%sb"=>100],
            ["nama"=>"JUANDA", "position"=>"FOREMAN", "scema"=>$FOREMAN, "%MSI"=>50, "scemaSB"=>$FOREMANSB, "%sb"=>100],
            ["nama"=>"Tajudin", "position"=>"FOREMAN", "scema"=>$FOREMAN, "%MSI"=>50, "scemaSB"=>$FOREMANSB, "%sb"=>100],
            ["nama"=>"OPIK", "position"=>"PIC TEMPEL STIKER", "scema"=>$PIC_STICKER, "%MSI"=>0, "scemaSB"=>$PIC_STICKERSB, "%sb"=>100],
            ["nama"=>"Fadlan", "position"=>"WASHER", "scema"=>$WASHER, "%MSI"=>50, "scemaSB"=>$WASHERSB, "%sb"=>100],
            ["nama"=>"Satria", "position"=>"WASHER", "scema"=>$WASHER, "%MSI"=>50, "scemaSB"=>$WASHERSB, "%sb"=>100],
            ["nama"=>"Eko", "position"=>"OFFICEBOY", "scema"=>$OFFICEBOY, "%MSI"=>50, "scemaSB"=>$OFFICEBOYSB, "%sb"=>50],
            ["nama"=>"Yudi","position"=>"OFFICEBOY", "scema"=>$OFFICEBOY, "%MSI"=>50, "scemaSB"=>$OFFICEBOYSB, "%sb"=>50],
            ["nama"=>"P2K","position"=>"P2K", "scema"=>$P2K, "%MSI"=>100, "scemaSB"=>$P2KSB, "%sb"=>100]
                        
        ];

       }



        foreach ($arrindux["fullRekapBySA"] as $lihat):
            if($branchcode=='641940101'){
                if($lihat["SaName"]<>'SYARIFUDIN'){
                    array_push($arr['listjabatan'],["nama"=>$lihat["SaName"],"position"=>"SERVICEADVISOR", "scema"=> $SERVICEADVISOR, "%MSI"=>$lihat["persentase"], "scemaSB"=>$SERVICEADVISORSB, "%sb"=>$lihat["persentase"]]);
                }
                                                         
            }//else if($branchcode=="641940104"){
               // if($lihat["SaName"]=='SYARIFUDIN'){
                    //array_push($arr['listjabatan'],["nama"=>$lihat["SaName"],"position"=>"SERVICEADVISOR", "scema"=> $SERVICEADVISOR, "%MSI"=>$lihat["persentase"], "scemaSB"=>$SERVICEADVISORSB, "%sb"=>$lihat["persentase"]]);
                //}
            //}
            else{
                array_push($arr['listjabatan'],["nama"=>$lihat["SaName"],"position"=>"SERVICEADVISOR", "scema"=> $SERVICEADVISOR, "%MSI"=>$lihat["persentase"], "scemaSB"=>$SERVICEADVISORSB, "%sb"=>$lihat["persentase"]]);
            }
          
        endforeach;

        foreach ($arrindux["fullRekapByMechanic"] as $lihat):
                array_push($arr['listjabatan'],["nama"=>$lihat["mechanicname"],"position"=>"MEKANIK", "scema"=>$MEKANIK, "%MSI"=>$lihat["persentase"], "scemaSB"=>$MEKANIKSB, "%sb"=>100]);                                         
        endforeach;

        //gabung dengan incentive
         $index=0;   
        foreach ($arr["listjabatan"] as $lihat):
            $arr['listjabatan'][$index]+=["incLabour"=>round(($lihat["scema"]*$totalIncentiveLabourProku/100)*$lihat["%MSI"]/100)];

            $arr['listjabatan'][$index]+=["incPart"=>round(($lihat["scema"]*$totalIncentivePartProku/100)*$lihat["%MSI"]/100)];

            $arr['listjabatan'][$index]+=["incOli"=>round(($lihat["scema"]*$totalIncentiveOliProku/100)*$lihat["%MSI"]/100)];

            $arr['listjabatan'][$index]+=["incSB"=>round(($lihat["scemaSB"]*$totalIncentiveSpooring/100)*$lihat["%sb"]/100)];


                 foreach ($arrindux["chemicalIncetive"] as $lihat1) :
                            if($lihat["position"]=="SERVICEADVISOR"){
                              if($lihat1["position"]=="SERVICE ADVISOR"){
                                if($branchcode=='6419401043'){
                                    $arr['listjabatan'][$index]+=["incChemcial"=>$lihat1["total"]];
                                }else{
                                     if($lihat["nama"]==$lihat1["EmployeeName"]){
                                        $arr['listjabatan'][$index]+=["incChemcial"=>$lihat1["total"]];

                                    }
                                }
                               
                              }
                            }else{
                               if($lihat["position"]==$lihat1["position"]) {
                                   $arr['listjabatan'][$index]+=["incChemcial"=>$lihat1["total"]];
                               }
                            }
                endforeach; 

                 $index++;                                      
    endforeach;

        $index=0;
        foreach ($arr["listjabatan"] as $lihat2):
              if(isset($lihat2["incSB"])){
                $arr['listjabatan'][$index]+=["total"=>$lihat2["incLabour"]+$lihat2["incPart"]+$lihat2["incOli"]+$lihat2["incSB"]];
              }else{
                $arr['listjabatan'][$index]+=["total"=>$lihat2["incLabour"]+$lihat2["incPart"]+$lihat2["incOli"]+0];
              }
            $index++;
        endforeach;

          $index=0;
        foreach ($arr["listjabatan"] as $lihat2):
              if(isset($lihat2["incChemcial"])){
                $arr['listjabatan'][$index]["total"]= $arr['listjabatan'][$index]["total"]+$arr['listjabatan'][$index]["incChemcial"];
              }else{
                 $arr['listjabatan'][$index]["total"]= $arr['listjabatan'][$index]["total"]+0;
              }
            $index++;
        endforeach;

    return $arr['listjabatan'];

        //print_r(json_encode($arr['listjabatan']));

    }

    function getemployeedata($branchcode){

            if($branchcode=="641940103"){

        $arr['listjabatan'] = [
            ["EmployeeName"=>"HERI","position"=>"SRM"],
            ["EmployeeName"=>"KUSDIANTORO", "position"=>"SERVICE MANAGER"],
            ["EmployeeName"=>"ANISSA", "position"=>"ADH"],
            ["EmployeeName"=>"SRI NOPIA LESTARI", "position"=>"SERVICEADMIN"],
            ["EmployeeName"=>"NAUFAL", "position"=>"SPVCCM"],
            ["EmployeeName"=>"ASTI", "position"=>"ADMCCM"],
            ["EmployeeName"=>"DINI NOVIA FIRANDANI", "position"=>"SRO"],
            ["EmployeeName"=>"RIDHA RIHHADATUL ASYI","position"=>"CRO"],
            ["EmployeeName"=>"LUSIA YANTINI", "position"=>"KASIR"],
            ["EmployeeName"=>"AVISENA ROSADY", "position"=>"ADMINISTRASI SPAREPART"],
            ["EmployeeName"=>"NOVELLIA PENGESTI", "position"=>"ADMINISTRASI SPAREPART"],
            ["EmployeeName"=>"VICKY HANGGARA", "position"=>"FOREMAN"],
            ["EmployeeName"=>"OPIK", "position"=>"PIC TEMPEL STIKER"],
            ["EmployeeName"=>"SLAMET", "position"=>"WASHER"],
            ["EmployeeName"=>"USEP", "position"=>"WASHER"],
            ["EmployeeName"=>"FAUZI", "position"=>"OFFICEBOY"],
            ["EmployeeName"=>"MARWAN","position"=>"OFFICEBOY"],
            ["EmployeeName"=>"P2K","position"=>"P2K"]
                        
        ];

       } else if($branchcode=="641940102"){

        $arr['listjabatan'] = [
            ["EmployeeName"=>"","position"=>"SRM"],
            ["EmployeeName"=>"GANDA MULYADI", "position"=>"SERVICE MANAGER"],
            ["EmployeeName"=>"BUDI TANUJAYA", "position"=>"ADH"],
            ["EmployeeName"=>"AGUSTINA WULANDARI", "position"=>"SERVICEADMIN"],
            ["EmployeeName"=>"NAUFAL", "position"=>"SPVCCM"],
            ["EmployeeName"=>"ASTI", "position"=>"ADMCCM"],
            ["EmployeeName"=>"PUTI", "position"=>"SRO"],
            ["EmployeeName"=>"ANISSA", "position"=>"SRO"],
            ["EmployeeName"=>"NURUL", "position"=>"CRO"],
            ["EmployeeName"=>"LINDA", "position"=>"KASIR"],
            ["EmployeeName"=>"TJEN TJEN", "position"=>"ADMINISTRASI SPAREPART"],
            ["EmployeeName"=>"ERNA DEVIANA", "position"=>"ADMINISTRASI SPAREPART"],
            ["EmployeeName"=>"YADI H", "position"=>"FOREMAN"],
            ["EmployeeName"=>"OPIK", "position"=>"PIC TEMPEL STIKER"],
            ["EmployeeName"=>"HASRI KUSWIRA", "position"=>"WASHER"],
            ["EmployeeName"=>"ANDI YUSTANDI", "position"=>"WASHER"],
            ["EmployeeName"=>"YOPI", "position"=>"OFFICEBOY"],
            ["EmployeeName"=>"DEDE","position"=>"OFFICEBOY"],
            ["EmployeeName"=>"P2K","position"=>"P2K"]

        ];

       } else if($branchcode=="641940101"){

       $arr['listjabatan'] = [
            ["EmployeeName"=>"HERI","position"=>"SRM"],
            ["EmployeeName"=>"Heri Setiawan", "position"=>"SERVICE MANAGER"],
            ["EmployeeName"=>"Clarisa", "position"=>"ADH"],
            ["EmployeeName"=>"Pepen Supendi", "position"=>"SERVICEADMIN"],
            ["EmployeeName"=>"NAUFAL", "position"=>"SPVCCM"],
            ["EmployeeName"=>"ASTI", "position"=>"ADMCCM"],
            ["EmployeeName"=>"IKRIMA", "position"=>"SRO"],
            ["EmployeeName"=>"Anas Tasya", "position"=>"CRO"],
            ["EmployeeName"=>"Eric Febrian", "position"=>"KASIR"],
            ["EmployeeName"=>"Taufik", "position"=>"ADMINISTRASI SPAREPART"],
            ["EmployeeName"=>"Nunik", "position"=>"ADMINISTRASI SPAREPART"],
            ["EmployeeName"=>"JUANDA", "position"=>"FOREMAN"],
            ["EmployeeName"=>"Tajudin", "position"=>"FOREMAN"],
            ["EmployeeName"=>"OPIK", "position"=>"PIC TEMPEL STIKER"],
            ["EmployeeName"=>"Fadlan", "position"=>"WASHER"],
            ["EmployeeName"=>"Satria", "position"=>"WASHER"],
            ["EmployeeName"=>"Eko", "position"=>"OFFICEBOY"],
            ["EmployeeName"=>"Yudi","position"=>"OFFICEBOY"],
            ["EmployeeName"=>"P2K","position"=>"P2K"]
                        
        ];

       }

        else if($branchcode=="641940104"){
        //belum dirubah data EmployeeNamenya
       $arr['listjabatan'] = [
            ["EmployeeName"=>"HERI","position"=>"SRM"],
            ["EmployeeName"=>"Heri Setiawan", "position"=>"SERVICE MANAGER"],
            ["EmployeeName"=>"Clarisa", "position"=>"ADH"],
            ["EmployeeName"=>"Pepen Supendi", "position"=>"SERVICEADMIN"],
            ["EmployeeName"=>"NAUFAL", "position"=>"SPVCCM"],
            ["EmployeeName"=>"ASTI", "position"=>"ADMCCM"],
            ["EmployeeName"=>"IKRIMA", "position"=>"SRO"],
            ["EmployeeName"=>"Anas Tasya", "position"=>"CRO"],
            ["EmployeeName"=>"Eric Febrian", "position"=>"KASIR"],
            ["EmployeeName"=>"Taufik", "position"=>"ADMINISTRASI SPAREPART"],
            ["EmployeeName"=>"Nunik", "position"=>"ADMINISTRASI SPAREPART"],
            ["EmployeeName"=>"JUANDA", "position"=>"FOREMAN"],
            ["EmployeeName"=>"Tajudin", "position"=>"FOREMAN"],
            ["EmployeeName"=>"OPIK", "position"=>"PIC TEMPEL STIKER"],
            ["EmployeeName"=>"Fadlan", "position"=>"WASHER"],
            ["EmployeeName"=>"Satria", "position"=>"WASHER"],
            ["EmployeeName"=>"Eko", "position"=>"OFFICEBOY"],
            ["EmployeeName"=>"Yudi","position"=>"OFFICEBOY"],
            ["EmployeeName"=>"P2K","position"=>"P2K"]
                        
        ];

       }

       return $arr["listjabatan"];

    }



  }