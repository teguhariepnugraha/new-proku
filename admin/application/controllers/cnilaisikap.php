<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cnilaisikap extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		// $this->load->helper(array('url','form'));
		 $this->load->model('mnilaisikap');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		
		$a['page']	= "nilaisikap";
		
		$this->load->view('admin/index', $a);
	}
	
	function json() {
	 $field=$this->input->post('field');
       echo $this->mnilaisikap->json($field);
    }
	

	function tambah_nilaisikap(){
		
		$a['page']	= "nilaisikap/tambah_nilaisikap";
		$this->load->view('admin/index', $a);
	}

	function insertdata(){
		$table =  'tnilaisikap';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		
		$this->db->insert($table, $myjson );
		redirect('cnilaisikap/tampil','refresh');
	}



	function editnilaisikap($id){
		$a['page']	= "nilaisikap/edit_nilaisikap";
		$this->load->view('admin/index', $a, $id);
	}

	function updatedata(){
		$table =   'tnilaisikap';
		$idtable =  'idnilaisikap';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 


	}

	

	function deletedtl1(){
		$idnilaisikapdtl = $this->input->get('idnilaisikapdtl');
		
		echo $this->mnilaisikap->deletedtl1($idnilaisikapdtl);
		
	}
	
	function hapusnilaisikap($id){
		$this->mnilaisikap->hapusnilaisikap($id);
		redirect('cnilaisikap/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->mnilaisikap->getjson();
    }

	
	function urlcmb()
    {

		echo $this->mnilaisikap->url();
    }
	
	function tampiledit(){
		$field =  $this->input->get('idnilaisikap');	
		echo $this->mnilaisikap->tampiledit($field);
	}
	
	function datanilaisikap(){	
		echo $this->mnilaisikap->datanilaisikap();
	}
	
	function datapeserta(){	
		echo $this->mnilaisikap->datapeserta();
	}
	
	function datasikap(){	
		echo $this->mnilaisikap->datasikap();
	}
	
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mnilaisikap->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $this->input->get('fields');	
		echo $this->mnilaisikap->get_datapopup($string);
    }
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mnilaisikap->get_headerpopup($string);
    }
	
	function urlautonumber()
    {
		echo $this->mnilaisikap->urlnumber();
    }
	
	function cekidnilaisikap()
	{
		$nota = $this->input->get('nota');
		echo $this->mnilaisikap->idnilaisikap($nota);
	}
	function simpandtl(){
			$data= $this->input->get('arr');
			echo $this->mnilaisikap->simpandtl($data);
			
		
		}
	function updatedtl(){
			$data= $this->input->get('arr');
			 
			echo $this->mnilaisikap->updatedtl($data);
			
		
		}
}

