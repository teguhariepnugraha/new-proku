<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cregistrasi extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		// $this->load->helper(array('url','form'));
		 $this->load->model('mregistrasi');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		
		$a['page']	= "registrasi";
		
		$this->load->view('admin/index', $a);
	}
	
	function json() {
	 $field=$this->input->post('field');
       echo $this->mregistrasi->json($field);
    }
	

	function tambah_registrasi(){
		
		$a['page']	= "registrasi/tambah_registrasi";
		$this->load->view('admin/index', $a);
	}

	function insertdata(){
		$table =  'tregistrasi';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		
		$this->db->insert($table, $myjson );
		redirect('cregistrasi/tampil','refresh');
	}



	function editregistrasi($id){
		$a['page']	= "registrasi/edit_registrasi";
		$this->load->view('admin/index', $a);
	}

	function updatedata(){
		$table =   'tregistrasi';
		$idtable =  'idregistrasi';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 


	}

	

	
	function hapusregistrasi($id){
		$this->mregistrasi->hapusregistrasi($id);
		redirect('cregistrasi/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->mregistrasi->getjson();
    }

	
	function urlcmb()
    {

		echo $this->mregistrasi->url();
    }
	
	function tampiledit(){
		$field =  $this->input->get('idregistrasi');	
		echo $this->mregistrasi->tampiledit($field);
	}
	
	function datapeserta(){	
		echo $this->mregistrasi->datapeserta();
	}
	function dataakun(){	
		echo $this->mregistrasi->dataakun();
	}
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mregistrasi->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $this->input->get('fields');	
		echo $this->mregistrasi->get_datapopup($string);
    }
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mregistrasi->get_headerpopup($string);
    }
	
	function urlautonumber()
    {
		echo $this->mregistrasi->urlnumber();
    }
	
	function cekidregistrasi()
	{
		$kdregistrasi = $this->input->get('kdregistrasi');
		echo $this->mregistrasi->idregistrasi($kdregistrasi);
	}
	function simpandtl(){
			$data= $this->input->get('arr');
			echo $this->mregistrasi->simpandtl($data);
			
		
		}
	function updatedtl(){
			$data= $this->input->get('arr');
			 
			echo $this->mregistrasi->updatedtl($data);
			
		
		}
}

