<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class clembur extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		// $this->load->helper(array('url','form'));
		 $this->load->model('mlembur');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		$field =  $this->input->post('table_search');
		$a['data']	= $this->mlembur->tampil($field)->result_object();
		$a['page']	= "lembur";
		
		$this->load->view('admin/index', $a);
	}

	function tambah_lembur(){
		
		$a['page']	= "lembur/tambah_lembur";
		$this->load->view('admin/index', $a);
	}

	function insertdata(){
		$table =  'tlembur';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		
		$this->db->insert($table, $myjson );
		redirect('clembur/tampil','refresh');
	}



	function editlembur($id){
		$a['page']	= "lembur/edit_lembur";
		$this->load->view('admin/index', $a, $id);
	}

	function updatedata(){
		$table =   'tlembur';
		$idtable =  'idlembur';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 


	}

	

	function deletedtl1(){
		$idlemburdtl = $this->input->get('idlemburdtl');
		
		echo $this->mlembur->deletedtl1($idlemburdtl);
		
	}
	
	function hapuslembur($id){
		$this->mlembur->hapuslembur($id);
		redirect('clembur/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->mlembur->getjson();
    }

	
	function urlcmb()
    {

		echo $this->mlembur->url();
    }
	
	function tampiledit(){
		$field =  $this->input->get('idlembur');	
		echo $this->mlembur->tampiledit($field);
	}
	
	function datakaryawan(){	
		echo $this->mlembur->datakaryawan();
	}
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mlembur->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $this->input->get('fields');	
		echo $this->mlembur->get_datapopup($string);
    }
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mlembur->get_headerpopup($string);
    }
	
	function urlautonumber()
    {
		echo $this->mlembur->urlnumber();
    }
	
	function cekidlembur()
	{
		$reg = $this->input->get('reg');
		echo $this->mlembur->idlembur($reg);
	}
	function simpandtl(){
			$data= $this->input->get('arr');
			echo $this->mlembur->simpandtl($data);
			
		
		}
	function updatedtl(){
			$data= $this->input->get('arr');
			 
			echo $this->mlembur->updatedtl($data);
			
		
		}
}

