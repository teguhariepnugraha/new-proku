<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cakun extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		// $this->load->helper(array('url','form'));
		 $this->load->model('makun');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		$field =  $this->input->post('table_search');
		$a['data']	= $this->makun->tampil($field)->result_object();
		$a['page']	= "akun";
		
		$this->load->view('admin/index', $a);
	}

	function tambah_akun(){
		
		$a['page']	= "akun/tambah_akun";
		$this->load->view('admin/index', $a);
	}

	function insertdata(){
		$table =  'takun';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		
		$this->db->insert($table, $myjson );
		redirect('cakun/tampil','refresh');
	}



	function editakun($id){
		$a['page']	= "akun/edit_akun";
		$this->load->view('admin/index', $a, $id);
	}

	function updatedata(){
		$table =   'takun';
		$idtable =  'idakun';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 


	}

	

	function deletedtl1(){
		$idakundtl = $this->input->get('idakundtl');
		
		echo $this->makun->deletedtl1($idakundtl);
		
	}
	
	function hapusakun($id){
		$this->makun->hapusakun($id);
		redirect('cakun/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->makun->getjson();
    }

	
	function urlcmb()
    {

		echo $this->makun->url();
    }
	
	function tampiledit(){
		$field =  $this->input->get('idakun');	
		echo $this->makun->tampiledit($field);
	}
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->makun->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $_GET['fields'];
		echo $this->makun->get_datapopup($string);
    }
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->makun->get_headerpopup($string);
    }
	function cekidakun()
	{
		$kdakun = $this->input->get('kdakun');
		echo $this->makun->idakun($kdakun);
	}
	function simpandtl(){
			$data= $this->input->get('arr');
			echo $this->makun->simpandtl($data);
			
		
		}
	function updatedtl(){
			$data= $this->input->get('arr');
			 
			echo $this->makun->updatedtl($data);
			
		
		}
}

