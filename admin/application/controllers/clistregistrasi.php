<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class clistregistrasi extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		// $this->load->helper(array('url','form'));
		 $this->load->model('mlistregistrasi');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		
		$a['page']	= "listregistrasi";
		
		$this->load->view('admin/index', $a);
	}
	
	function json() {
	 $field=$this->input->post('field');
       echo $this->mlistregistrasi->json($field);
    }
	

	function tambah_listregistrasi(){
		
		$a['page']	= "listregistrasi/tambah_listregistrasi";
		$this->load->view('admin/index', $a);
	}

	function insertdata(){
		$table =  'tlistregistrasi';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		
		$this->db->insert($table, $myjson );
		redirect('clistregistrasi/tampil','refresh');
	}



	function editlistregistrasi($id){
		$a['page']	= "listregistrasi/edit_listregistrasi";
		$this->load->view('admin/index', $a);
	}

	function updatedata(){
		$table =   'tlistregistrasi';
		$idtable =  'idlistregistrasi';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 


	}

	

	function deletedtl1(){
		$idlistregistrasidtl = $this->input->get('idlistregistrasidtl');
		
		echo $this->mlistregistrasi->deletedtl1($idlistregistrasidtl);
		
	}
	
	function hapuslistregistrasi($id){
		$this->mlistregistrasi->hapuslistregistrasi($id);
		redirect('clistregistrasi/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->mlistregistrasi->getjson();
    }

	
	function urlcmb()
    {

		echo $this->mlistregistrasi->url();
    }
	
	function tampiledit(){
		$field =  $this->input->get('idlistregistrasi');	
		echo $this->mlistregistrasi->tampiledit($field);
	}
	
	function datalistregistrasi(){	
		echo $this->mlistregistrasi->datalistregistrasi();
	}
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mlistregistrasi->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $this->input->get('fields');	
		echo $this->mlistregistrasi->get_datapopup($string);
    }
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mlistregistrasi->get_headerpopup($string);
    }
	
	function urlautonumber()
    {
		echo $this->mlistregistrasi->urlnumber();
    }
	
	function cekidlistregistrasi()
	{
		$kdlistregistrasi = $this->input->get('kdlistregistrasi');
		echo $this->mlistregistrasi->idlistregistrasi($kdlistregistrasi);
	}
	function simpandtl(){
			$data= $this->input->get('arr');
			echo $this->mlistregistrasi->simpandtl($data);
			
		
		}
	function updatedtl(){
			$data= $this->input->get('arr');
			 
			echo $this->mlistregistrasi->updatedtl($data);
			
		
		}
}

