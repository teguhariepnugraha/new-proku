<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ctargetunitentry extends CI_Controller {

	function __construct(){
		parent::__construct();
		// $this->load->helper(array('url','form'));

		$this->load->model('mtargetunitentry');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		$field =  $this->input->post('table_search');
		$a['data']	= $this->mtargetunitentry->tampil($field)->result_object();
		$a['page']	= "target_unit_entry";
		
		$this->load->view('admin/index', $a);
	}

	function tambah_targetunitentry(){
		
		$a['page']	= "targetunitentry/tambah_target_unit_entry";
		$this->load->view('admin/index', $a);
	}


	function edit_targetunitentry($id){
		$a['page']	= "targetunitentry/edit_target_unit_entry";
		$this->load->view('admin/index', $a, $id);
	}


	function insertdata(){

		$table =  'targetunitentry';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->insert($table, $myjson );
		redirect('ctargetunitentry/tampil','refresh');
	}



	function updatedata(){
		$table =   'targetunitentry';
		$idtable =  'idtargetunitentry';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 
		echo ($id);

		


	}


	function hapustargetunitentry($id){
		$this->mtargetunitentry->hapustargetunitentry($id);
		
		
		redirect('ctargetDO/tampil','refresh');
	}

    function urlcmb()
    {

		echo $this->mtargetunitentry->url();
    }


    function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mtargetunitentry->mgetjsonshow($id);
    }
	

    function getjsonsample()
    {
		echo $this->mtargetunitentry->getjson();
    }

	

}
