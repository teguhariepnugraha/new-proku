<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cplaningMarketing extends CI_Controller {

	function __construct(){
		parent::__construct();
		// $this->load->helper(array('url','form'));

		 $this->load->model('mplaningMarketing');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		//$field =  $this->input->post('table_search');
		// $a['data']	= json_encode($this->mplaningMarketing->tampil()->result_object());
		//json_encode($this->mplaningMarketing->tampil()->result_object());
		$a['page']	= "planingMarketing";
		
		$this->load->view('admin/index', $a);
	}

	function tampil_data(){
		//$field =  $this->input->post('table_search');
		// $a['data']	= json_encode($this->mplaningMarketing->tampil()->result_object());
		// /print_r(json_encode($this->mplaningMarketing->tampil()->result_object()));

		echo $this->mplaningMarketing->tampil();
		//json_encode($this->mplaningMarketing->tampil()->result_object());

		//$a['page']	= "planingMarketing";
		
		//$this->load->view('admin/index', $a);
	}

	// function tambah_cabang(){
		
	// 	$a['page']	= "cabang/tambah_cabang";
	// 	$this->load->view('admin/index', $a);
	// }

	function insertdata(){
	  try{
			$table =  'tplaningMarketing';
			$bagong = $this->input->get('myjson');
			$myjson =json_decode($bagong,true);
			$this->db->insert($table, $myjson );
			$myObj = new stdClass();
			$myObj->success = 1;
			$myObj->message = "penyimpanan berhasil";
		 echo json_encode($myObj);
		 }catch(Exception $ex){
		 	$myObj = new stdClass();
			$myObj->success = 0;
			$myObj->message = "penyimpanan gagal";
		 echo json_encode($myObj);
		 }

		//redirect('cplaningMarketing/tampil','refresh');
	}



	// function editcabang($id){
	// 	$a['page']	= "cabang/edit_cabang";
	// 	$this->load->view('admin/index', $a, $id);
	// }

	function updatedata(){
		$table =   'tplaningMarketing';
		$idtable =  'idPlaning';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');

		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 
		echo ($id);

	}

	

	function hapusPlaning(){
	  try{
	  	$id = $_GET['id'];
		$this->mplaningMarketing->hapusPlaning($id);
		//redirect('ccabang/tampil','refresh');
		    $myObj = new stdClass();
			$myObj->success = 1;
			$myObj->message = "penghapusan berhasil";
		 echo json_encode($myObj);

		}catch(Exception $ex){
			$myObj = new stdClass();
			$myObj->success = 0;
			$myObj->message = "penghapusan di gagalkan ";
		 echo json_encode($myObj);

		}
	}

	function getjsonsample()
    {
		echo $this->mplaningMarketing->getjson();
    }

	
	function urlcmb()
    {

		echo $this->mplaningMarketing->url();
    }
	
	
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mplaningMarketing->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mplaningMarketing->get_datapopup($string);
    }
    
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mplaningMarketing->get_headerpopup($string);
    }

    function search_data()
    {
	
		$string =  $_GET['fields'];
		echo $this->mplaningMarketing->search_data($string);
    }


}

