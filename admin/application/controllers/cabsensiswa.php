<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cabsensiswa extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		// $this->load->helper(array('url','form'));
		 $this->load->model('mabsensiswa');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		
		$a['page']	= "absensiswa";
		
		$this->load->view('admin/index', $a);
	}
	
	function json() {
	 $field=$this->input->post('field');
       echo $this->mabsensiswa->json($field);
    }
	

	function tambah_absensiswa(){
		
		$a['page']	= "absensiswa/tambah_absensiswa";
		$this->load->view('admin/index', $a);
	}

	function insertdata(){
		$table =  'tabsensiswa';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		
		$this->db->insert($table, $myjson );
		redirect('cabsensiswa/tampil','refresh');
	}



	function editabsensiswa($id){
		$a['page']	= "absensiswa/edit_absensiswa";
		$this->load->view('admin/index', $a, $id);
	}

	function updatedata(){
		$table =   'tabsensiswa';
		$idtable =  'idabsensiswa';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 


	}

	

	function deletedtl1(){
		$idabsensiswadtl = $this->input->get('idabsensiswadtl');
		
		echo $this->mabsensiswa->deletedtl1($idabsensiswadtl);
		
	}
	
	function hapusabsensiswa($id){
		$this->mabsensiswa->hapusabsensiswa($id);
		redirect('cabsensiswa/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->mabsensiswa->getjson();
    }

	
	function urlcmb()
    {

		echo $this->mabsensiswa->url();
    }
	
	function tampiledit(){
		$field =  $this->input->get('idabsensiswa');	
		echo $this->mabsensiswa->tampiledit($field);
	}
	
	
	
	function datakelas(){	
		echo $this->mabsensiswa->datakelas();
	}
	
	function datapengajar(){	
		echo $this->mabsensiswa->datapengajar();
	}
	function datapelajaran(){	
		$string =  $this->input->get('idkelas');	
		echo $this->mabsensiswa->datapelajaran($string);
	}
	function datapeserta(){	
		$string =  $this->input->get('idkelas');
		echo $this->mabsensiswa->datapeserta($string);
	}
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mabsensiswa->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $this->input->get('fields');	
		echo $this->mabsensiswa->get_datapopup($string);
    }
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mabsensiswa->get_headerpopup($string);
    }
	
	function urlautonumber()
    {
		echo $this->mabsensiswa->urlnumber();
    }
	
	function cekidabsensiswa()
	{
		$kdabsensiswa = $this->input->get('kdabsensiswa');
		echo $this->mabsensiswa->idabsensiswa($kdabsensiswa);
	}
	function simpandtl(){
			$data= $this->input->get('arr');
			echo $this->mabsensiswa->simpandtl($data);
			
		
		}
	function updatedtl(){
			$data= $this->input->get('arr');
			 
			echo $this->mabsensiswa->updatedtl($data);
			
		
		}
}

