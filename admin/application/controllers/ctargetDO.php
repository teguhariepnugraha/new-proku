<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ctargetDO extends CI_Controller {

	function __construct(){
		parent::__construct();
		// $this->load->helper(array('url','form'));

		$this->load->model('mtargetdo');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		$field =  $this->input->post('table_search');
		$a['data']	= $this->mtargetdo->tampil($field)->result_object();
		$a['page']	= "target_do";
		
		$this->load->view('admin/index', $a);
	}

	function tambah_targetDO(){
		
		$a['page']	= "targetdo/tambah_targetDO";
		$this->load->view('admin/index', $a);
	}


	function edit_targetDO($id){
		$a['page']	= "targetdo/edit_targetDO";
		$this->load->view('admin/index', $a, $id);
	}


	function hapustargetdo($id){
		$this->mtargetdo->hapustargetdo($id);
		
		
		redirect('ctargetDO/tampil','refresh');
	}


	function insertdata(){

		$table =  'targetdo';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->insert($table, $myjson );
		redirect('ctargetDO/tampil','refresh');
	}



	function updatedata(){
		$table =   'targetdo';
		$idtable =  'idtargetdo';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 
		echo ($id);

		


	}


	public function getAllTipe(){
    	 return print_r(json_encode($this->mtargetdo->getAllTipe()));
    }


    public function getAllModel(){
    	$criteria= $_GET['criteria'];
    	 return print_r(json_encode($this->mtargetdo->getAllModel($criteria)));
    }



    public function getAllModelFilter(){
    	 $bulan = $_GET['bulan'];
    	 //return print_r($bulan);
    	 return print_r(json_encode($this->mtargetdo->getAllModelFilter($bulan)));
    	 
    	 //return print_r($whereMonth);
    }




    function urlcmb()
    {

		echo $this->mtargetdo->url();
    }


    function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mtargetdo->mgetjsonshow($id);
    }
	

    function getjsonsample()
    {
		echo $this->mtargetdo->getjson();
    }

	

}
