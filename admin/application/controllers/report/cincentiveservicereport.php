<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('./application/third_party/vendor/autoload.php');



use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

Include_once(APPPATH.'controllers/cIncentiveService.php');
class cincentiveservicereport extends cIncentiveService  {

	public function __construct() {
        parent::__construct();
		if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}
		 $this->load->library('pdf');
        $this->load->model('mIncentiveService');
       // $this->load->library('/controllers/cIncentiveService');
    }


        function getMonthMin1($year,$month){
    	 if(($month)==2){
		     return ("01,".$year);
		   // echo "12".$year-1;
		      
		  }else if ($month==1){
		  	return ("12,".($year-1));
		    //echo "11".($year-1)." ";
		  }else{
            if(($month-1)<10){
              return ("0".($month-1).",".$year);
            } else{
               return (($month-1).",".$year); 
            }
		    
		  }
    }

    function getMonthMin2($year,$month){
    	 if(($month)==2){
		   return ("12,".($year-1));
		      
		  }else if ($month==1){
		    return ("11,".($year-1));
		  }else{
            if(($month-2)<10){
                return("0".($month-2).",".$year);
            }else{
                return (($month-2).",".$year);
            }
		    
		  }
    }


    	public function cetakpdf($bulan,$tahun,$cabang,$idpengajuan){

    		

                
           $arrindux=$this->getDataIncentiveService($cabang, $tahun, $bulan);
        // $monthmin1=$this->getMonthMin1($tahun,$bulan);
        // $monthmin2=$this->getMonthMin2($tahun,$bulan);
        // $arrMonthmin1=explode(",",$monthmin1);
        // $arrMonthmin2=explode(",",$monthmin2);
	    	 // $arr = array();
    		 // $bulan="12";
    		 // $tahun="2021";
    		 // $cabang="641940102";

    		//pembuatan constanta untuk insentive
    		   //define("jmlIncentive",2000000);
    		   //define("persentasiCon1",60);
    		   //define("persentasiCon2",50);
    			
	    	// $arr['dataartransaksi'] =$this->mIncentiveService->getformataroutstanding($cabang, $tahun, $bulan, $monthmin1, $monthmin2);
	    	 $arr['nama_cabang']=$cabang;
	    	 $arr['select-bulan']=$bulan;
	    	 $arr['select-tahun']=$tahun;
	    	 $arr['bookingsro']=$bulan;


	    	  $arr['rekapIncetive'] =$this->rekapIncentive($cabang, $tahun, $bulan, $arrindux);

	    	  //$arr["fullIncentiveSpooring"]=$this->mIncentiveService->getFullIncentiveSpooring($branchcode, $year, $month);

        	// $arr["listincentivechemical"]=$this->calInsentiveChemical($branchcode, $year, $month);

             //$arr["fullcalincentiveProku"]=$this->calIncentiveMSI($cabang, $tahun, $bulan);

	    	  $arr["PIC"]=$this->mIncentiveService->getPIC($idpengajuan);




	    	 //batas awal excel

	    	 $spreadsheet = new Spreadsheet();
	    	 $spreadsheet->getActiveSheet()->setTitle("AR");
    	     $sheet = $spreadsheet->getActiveSheet();
    	     $sheet->getColumnDimension('A')->setWidth(10);
    	     $sheet->getColumnDimension('B')->setWidth(20);
    	     $sheet->getColumnDimension('C')->setWidth(20);
    	     $sheet->getColumnDimension('D')->setWidth(20);
    	     $sheet->getColumnDimension('E')->setWidth(20);
    	     $sheet->getColumnDimension('F')->setWidth(50);
    	     $sheet->getColumnDimension('G')->setWidth(20);
    	     $sheet->getColumnDimension('H')->setWidth(20);
    	     $sheet->getColumnDimension('I')->setWidth(20);
    	     $sheet->getColumnDimension('J')->setWidth(20);
    	     $sheet->getColumnDimension('K')->setWidth(20);
    	     $sheet->getColumnDimension('L')->setWidth(20);
    	     $sheet->getColumnDimension('M')->setWidth(20);
    	     $sheet->getColumnDimension('N')->setWidth(20);
    	     $sheet->getColumnDimension('O')->setWidth(20);
    	     $sheet->getColumnDimension('P')->setWidth(20);
    	     $sheet->getColumnDimension('Q')->setWidth(20);
    	     $sheet->getColumnDimension('R')->setWidth(20);
    	     $sheet->getColumnDimension('S')->setWidth(20);
    	     $sheet->getColumnDimension('T')->setWidth(20);
    	     $sheet->getColumnDimension('U')->setWidth(20);
    	     $sheet->getColumnDimension('V')->setWidth(20);
    	     $sheet->getColumnDimension('W')->setWidth(200);
    	     $sheet->getColumnDimension('x')->setWidth(20);





    	     

	    	 	if(isset($arr)){
                    if ((array_key_exists("nama_cabang",$arr)) && (array_key_exists("select-bulan",$arr)) && (array_key_exists("select-tahun",$arr))) { 



						    


                    				//$pdf->cell(25,20,$pdf->Image(base_url()."image/tandatangan/suzuki.png",  $pdf->GetX(), $pdf->GetY(),30),0,1,'C');
                    				//$pdf->Image(base_url()."image/tandatangan/suzuki.png" ,10,-5,30,20,'png','');

			                    	//$sheet1->getRowDimension('2')->setRowHeight(100);
									//$sheet1->getColumnDimension('A')->setWidth(100);

                    				$sheet->mergeCells('A2:b2');
                    				$sheet->getRowDimension('2')->setRowHeight(100);

					   				$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
									$drawing->setPath("././image/tandatangan/suzuki1.png"); 
									$drawing->setCoordinates('A2');
									$drawing->setWidth(100);
									$drawing->setHeight(100);

									$drawing->setWorksheet($spreadsheet->getActiveSheet());

                    				$sheet->setCellValue('A1', 'List Data AR ');
		   							$sheet->mergeCells('A1:t1');
		   							$sheet->getStyle('A1:t1')->getAlignment()->setHorizontal('center');
		    						//$sheet->getStyle('A1:k1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('77D970');

                    				//$pdf->Ln(5);

                    				//$pdf->Cell(22,6,'Authorized ',0,0,'l');
                    				//$pdf->SetTextColor(0,0,255);
                    				//$pdf->Cell(14,6,'Suzuki ',0,0,'l');
                    				//$pdf->SetTextColor(0,0,0);
                    				//$pdf->Cell(30,6,'Dealer ',0,1,'l');

                    				// $pdf->Ln(5);


                    				//$pdf->SetFont('Arial','B',10);
                    				if($arr["nama_cabang"]=="641940101"){
                    					//$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Ciawi ",0,1,'L');
                    					$sheet->setCellValue('A3', 'PT Duta Cendana Adimandiri - Ciawi');
                    				}else if($arr["nama_cabang"]=="641940102"){
                    					//$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Cianjur ",0,1,'L');
                    					$sheet->setCellValue('A3', 'PT Duta Cendana Adimandiri - Cianjur');
                    				}else if($arr["nama_cabang"]=="641940103"){
                    					//$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Cinere ",0,1,'L');
                    					$sheet->setCellValue('A3', 'PT Duta Cendana Adimandiri - Cinere');
                    				}else if($arr["nama_cabang"]=="641940104"){
                    					//$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."JatiAsih ",0,1,'L');
                    					$sheet->setCellValue('A3', 'PT Duta Cendana Adimandiri - Jatiasih');
                    				}

                    				 $sheet->mergeCells('A3:b3');

                    				//$pdf->Cell(320,5,'Bulan : '.$arr["select-bulan"],0,1,'C');
                    				//$pdf->Cell(320,5,'Tahun : '.$arr["select-tahun"],0,1,'C');

                    				
							
					}else{
							//$pdf->Cell(70,10,'SRO : ',0,1,'L'); 
					}
			}else{
							//$pdf->Cell(70,10,'SRO : ',0,1,'L'); 
			}



			if(isset($arr)){
                    if (array_key_exists("bookingsro",$arr)){ 
                  		$bulan1='';
							//$pdf->Cell(70,10,'SRO : ' .$arr["bookingsro"][0]["fullname"],0,1,'L'); 
                    		//$pdf->SetFont('Arial','B',10);
							if($arr["bookingsro"]==1){
										//$pdf->Cell(40,6, "Bulan :Jan ".$arr["select-tahun"] ,0,1,'L');
										$sheet->setCellValue('A4', 'Bulan : Jan '.$arr["select-tahun"]);
										$bulan1='Jan '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==2){
										//$pdf->Cell(40,6, "Bulan :Feb ".$arr["select-tahun"],0,1,'L');
										$sheet->setCellValue('A4', 'Bulan : Feb '.$arr["select-tahun"]);
										$bulan1='Feb '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==3){
										//$pdf->Cell(40,6, "Bulan :Mar ".$arr["select-tahun"],0,1,'L');
										$sheet->setCellValue('A4', 'Bulan : Mar '.$arr["select-tahun"]);
										$bulan1='Mar '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==4){
										//$pdf->Cell(40,6, "Bulan :Apr ".$arr["select-tahun"],0,1,'L');
										$sheet->setCellValue('A4', 'Bulan : Apr '.$arr["select-tahun"]);
										$bulan1='Apr '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==5){
										//$pdf->Cell(40,6, "Bulan :Mei ".$arr["select-tahun"],0,1,'L');
										$sheet->setCellValue('A4', 'Bulan : Mei '.$arr["select-tahun"]);
										$bulan1='Mei '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==6){
										//$pdf->Cell(40,6, "Bulan :Jun ".$arr["select-tahun"],0,1,'L');
										$sheet->setCellValue('A4', 'Bulan : Jun '.$arr["select-tahun"]);
										$bulan1='Jun '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==7){
										//$pdf->Cell(40,6, "Bulan :Jul ".$arr["select-tahun"],0,1,'L');
										$sheet->setCellValue('A4', 'Bulan : Jul '.$arr["select-tahun"]);
										$bulan1='Jul '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==8){
										//$pdf->Cell(40,6, "Bulan :Agu ".$arr["select-tahun"],0,1,'L');
										$sheet->setCellValue('A4', 'Bulan : Agu '.$arr["select-tahun"]);
										$bulan1='Agu '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==9){
										//$pdf->Cell(40,6, "Bulan :Sep ".$arr["select-tahun"],0,1,'L');
										$sheet->setCellValue('A4', 'Bulan : Sep '.$arr["select-tahun"]);
										$bulan1='Sep '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==10){
										//$pdf->Cell(40,6, "Bulan :Okt ".$arr["select-tahun"],0,1,'L');
										$sheet->setCellValue('A4', 'Bulan : Okt '.$arr["select-tahun"]);
										$bulan1='Okt '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==11){
										//$pdf->Cell(40,6, "Bulan :Nov ".$arr["select-tahun"],0,1,'L');
										$sheet->setCellValue('A4', 'Bulan : Nov '.$arr["select-tahun"]);
										$bulan1='Nov '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==12){
										//$pdf->Cell(40,6, "Bulan :Des ".$arr["select-tahun"],0,1,'L');
										$sheet->setCellValue('A4', 'Bulan : Des '.$arr["select-tahun"]);
										$bulan1='Des '.$arr["select-tahun"];
									}

									$sheet->mergeCells('A4:b4');
					}else{
							//$pdf->Cell(70,10,'SRO : ',0,1,'L'); 
					}
			}else{
							//$pdf->Cell(70,10,'SRO : ',0,1,'L'); 
			}



			$styleArray = array(
				  'borders' =>array(
				   'outline' =>	array(
				     'borderStyle' =>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				         'color' =>array('argb' =>'00000000'), 
				     ),
				  ),
				);

		
			
			$sheet->setCellValue('A5','No');
			$sheet->mergeCells('A5:A6');
			$sheet->getStyle("A5:A6")->applyFromArray($styleArray);
			$sheet->getStyle('A5:A6')->getAlignment()->setHorizontal('center');
			$sheet->getStyle('A5:A6')->getAlignment()->setvertical('center');

			$sheet->setCellValue('B5','InvoiceNo');
			$sheet->mergeCells('B5:B6');
			$sheet->getStyle('B5:B6')->getAlignment()->setHorizontal('center');
			$sheet->getStyle('B5:B6')->getAlignment()->setvertical('center');
			$sheet->getStyle("B5:B6")->applyFromArray($styleArray);

			$sheet->setCellValue('C5','Invoice Date');
			$sheet->mergeCells('C5:C6');
			$sheet->getStyle('C5:C6')->getAlignment()->setHorizontal('center');
			$sheet->getStyle('C5:C6')->getAlignment()->setvertical('center');
			$sheet->getStyle("C5:C6")->applyFromArray($styleArray);


			$sheet->setCellValue('D5','SPK No');
			$sheet->mergeCells('D5:D6');
			$sheet->getStyle('D5:D6')->getAlignment()->setHorizontal('center');
			$sheet->getStyle('D5:D6')->getAlignment()->setvertical('center');
			$sheet->getStyle("D5:D6")->applyFromArray($styleArray);

			$sheet->setCellValue('E5','SPK Date');
			$sheet->mergeCells('E5:E6');
			$sheet->getStyle('E5:E6')->getAlignment()->setHorizontal('center');
			$sheet->getStyle('E5:E6')->getAlignment()->setvertical('center');
			$sheet->getStyle("E5:E6")->applyFromArray($styleArray);

			$sheet->setCellValue('F5','Customer Name');
			$sheet->mergeCells('F5:F6');
			$sheet->getStyle('F5:F6')->getAlignment()->setHorizontal('center');
			$sheet->getStyle('F5:F6')->getAlignment()->setvertical('center');
			$sheet->getStyle("F5:F6")->applyFromArray($styleArray);

			$sheet->setCellValue('G5','Saldo Awal ');
			$sheet->mergeCells('g5:j5');
			$sheet->getStyle('g5:j5')->getAlignment()->setHorizontal('center');
			$sheet->getStyle('g5:j5')->getAlignment()->setvertical('center');
			$sheet->getStyle("g5:j5")->applyFromArray($styleArray);

			$sheet->setCellValue('G6','Jasa Saldo');
			$sheet->getStyle("G6")->applyFromArray($styleArray);

			$sheet->setCellValue('H6','Part Saldo');
			$sheet->getStyle("H6")->applyFromArray($styleArray);

			$sheet->setCellValue('i6','Oli Saldo');
			$sheet->getStyle("i6")->applyFromArray($styleArray);

			$sheet->setCellValue('J6','Total Saldo');
			$sheet->getStyle("J6")->applyFromArray($styleArray);

			$sheet->setCellValue('K5','DEBET');
			$sheet->mergeCells('K5:N5');
			$sheet->getStyle('K5:N5')->getAlignment()->setHorizontal('center');
			$sheet->getStyle('K5:N5')->getAlignment()->setvertical('center');
			$sheet->getStyle("K5:N5")->applyFromArray($styleArray);


			$sheet->setCellValue('K6','Jasa MSI');
			$sheet->getStyle("K6")->applyFromArray($styleArray);

			$sheet->setCellValue('L6','Part MSI');
			$sheet->getStyle("L6")->applyFromArray($styleArray);

			$sheet->setCellValue('M6','Oli MSI');
			$sheet->getStyle("M6")->applyFromArray($styleArray);

			$sheet->setCellValue('N6','Total Debet');
			$sheet->getStyle("N6")->applyFromArray($styleArray);

			$sheet->setCellValue('o5','Payment');
			$sheet->mergeCells('o5:r5');
			$sheet->getStyle('o5:r5')->getAlignment()->setHorizontal('center');
			$sheet->getStyle('o5:r5')->getAlignment()->setvertical('center');
			$sheet->getStyle("o5:r5")->applyFromArray($styleArray);

			$sheet->setCellValue('o6','Jasa Payment');
			$sheet->getStyle("o6")->applyFromArray($styleArray);

			$sheet->setCellValue('p6','Part Payment');
			$sheet->getStyle("p6")->applyFromArray($styleArray);

			$sheet->setCellValue('q6','Oli Payment');
			$sheet->getStyle("q6")->applyFromArray($styleArray);

			$sheet->setCellValue('r6','Payment Amt');
			$sheet->getStyle("r6")->applyFromArray($styleArray);

			$sheet->setCellValue('s5','Saldo Akhir');
			$sheet->mergeCells('s5:v5');
			$sheet->getStyle('s5:v5')->getAlignment()->setHorizontal('center');
			$sheet->getStyle('s5:v5')->getAlignment()->setvertical('center');
			$sheet->getStyle("s5:v5")->applyFromArray($styleArray);

			$sheet->setCellValue('s6','Jasa');
			$sheet->getStyle("s6")->applyFromArray($styleArray);

			$sheet->setCellValue('t6','Sparepart');
			$sheet->getStyle("t6")->applyFromArray($styleArray);

			$sheet->setCellValue('u6','Oli');
			$sheet->getStyle("u6")->applyFromArray($styleArray);

			$sheet->setCellValue('v6','total');
			$sheet->getStyle("v6")->applyFromArray($styleArray);

			$sheet->setCellValue('w5','Data Bank Kas');
			$sheet->mergeCells('w5:w6');
			$sheet->getStyle('w5:w6')->getAlignment()->setHorizontal('center');
			$sheet->getStyle('w5:w6')->getAlignment()->setvertical('center');
			$sheet->getStyle("w5:w6")->applyFromArray($styleArray);

			$sheet->setCellValue('x5','keterangan');
			$sheet->mergeCells('x5:x6');
			$sheet->getStyle('x5:x6')->getAlignment()->setHorizontal('center');
			$sheet->getStyle('x5:x6')->getAlignment()->setvertical('center');
			$sheet->getStyle("x5:x6")->applyFromArray($styleArray);

			//$sheet->getStyle('A5:x6')->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
			//$sheet->getStyle('A5:x6')->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
			//$sheet->getStyle('A5:x6')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
			//$sheet->getStyle('A5:x6')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

			$sheet->getStyle('A5:x6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('37E2D5');

						if(isset($arr)){
                    if (array_key_exists("dataartransaksi",$arr)){ 
                         $no = 1;
                         $row =6;
                         $Jumlahcount=0;
                         $Jumlahnotcount=0;
                         	foreach ($arr["dataartransaksi"] as $lihat):
                         		 
                         		  $row++;
                         		 	if($lihat["keterangan"]=="not"){
                         		 			//$pdf->SetFillColor(255,0, 0);
                         		 			$Jumlahnotcount++;

                         		 			$sheet->setCellValue('A'.$row,$no++);
                         		 			$sheet->getStyle('A'.$row)->applyFromArray($styleArray);

	                         		 		$sheet->setCellValue('B'.$row,$lihat["invoiceno"]);
	                         		 		$sheet->getStyle('B'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('C'.$row,$lihat["invoicedate"]);
											$sheet->getStyle('C'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('D'.$row,$lihat["joborderno"]);
											$sheet->getStyle('D'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('E'.$row,$lihat["joborderdate"]);
											$sheet->getStyle('E'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('F'.$row,$lihat["customername"]);
											$sheet->getStyle('F'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('G'.$row,$lihat["jasa_awal"]);
											$sheet->getStyle('G'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('H'.$row,$lihat["part_awal"]);
											$sheet->getStyle('H'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('I'.$row,$lihat["oli_awal"]);
											$sheet->getStyle('I'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('J'.$row,$lihat["saldo_awal"]);
											$sheet->getStyle('J'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('K'.$row,$lihat["jasa_msi"]);
											$sheet->getStyle('K'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('l'.$row,$lihat["part_msi"]);
											$sheet->getStyle('l'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('m'.$row,$lihat["oli_msi"]);
											$sheet->getStyle('m'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('n'.$row,$lihat["total_debet"]);
											$sheet->getStyle('n'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('o'.$row,$lihat["jasa_pay"]);
											$sheet->getStyle('o'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('p'.$row,$lihat["part_pay"]);
											$sheet->getStyle('p'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('q'.$row,$lihat["oli_pay"]);
											$sheet->getStyle('q'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('r'.$row,$lihat["paymentamt"]);
											$sheet->getStyle('r'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('s'.$row,$lihat["jasa_saldo"]);
											$sheet->getStyle('s'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('t'.$row,$lihat["part_saldo"]);
											$sheet->getStyle('t'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('u'.$row,$lihat["oli_saldo"]);
											$sheet->getStyle('u'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('v'.$row,$lihat["jasa_saldo"]+$lihat["part_saldo"]+$lihat["oli_saldo"]);
											$sheet->getStyle('v'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('w'.$row,$lihat["databankkas"]);
											$sheet->getStyle('w'.$row,$lihat)->applyFromArray($styleArray);

											$sheet->setCellValue('x'.$row,$lihat["keterangan"]);
											$sheet->getStyle('x'.$row)->applyFromArray($styleArray);


                         		 			$sheet->getStyle('A'.$row.':k'.$row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('F32424');
                         		 	}else{
                         		 			//$pdf->SetFillColor(255,255, 255);
                         		 			$Jumlahcount++;
                         		 			//$pdf->Cell(10,6, $no++,1,0,'C',1);

                         		 	       $sheet->setCellValue('A'.$row,$no++);
                         		 			$sheet->getStyle('A'.$row)->applyFromArray($styleArray);

	                         		 		$sheet->setCellValue('B'.$row,$lihat["invoiceno"]);
	                         		 		$sheet->getStyle('B'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('C'.$row,$lihat["invoicedate"]);
											$sheet->getStyle('C'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('D'.$row,$lihat["joborderno"]);
											$sheet->getStyle('D'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('E'.$row,$lihat["joborderdate"]);
											$sheet->getStyle('E'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('F'.$row,$lihat["customername"]);
											$sheet->getStyle('F'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('G'.$row,$lihat["jasa_awal"]);
											$sheet->getStyle('G'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('H'.$row,$lihat["part_awal"]);
											$sheet->getStyle('H'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('I'.$row,$lihat["oli_awal"]);
											$sheet->getStyle('I'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('J'.$row,$lihat["saldo_awal"]);
											$sheet->getStyle('J'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('K'.$row,$lihat["jasa_msi"]);
											$sheet->getStyle('K'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('l'.$row,$lihat["part_msi"]);
											$sheet->getStyle('l'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('m'.$row,$lihat["oli_msi"]);
											$sheet->getStyle('m'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('n'.$row,$lihat["total_debet"]);
											$sheet->getStyle('n'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('o'.$row,$lihat["jasa_pay"]);
											$sheet->getStyle('o'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('p'.$row,$lihat["part_pay"]);
											$sheet->getStyle('p'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('q'.$row,$lihat["oli_pay"]);
											$sheet->getStyle('q'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('r'.$row,$lihat["paymentamt"]);
											$sheet->getStyle('r'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('s'.$row,$lihat["jasa_saldo"]);
											$sheet->getStyle('s'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('t'.$row,$lihat["part_saldo"]);
											$sheet->getStyle('t'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('u'.$row,$lihat["oli_saldo"]);
											$sheet->getStyle('u'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('v'.$row,$lihat["jasa_saldo"]+$lihat["part_saldo"]+$lihat["oli_saldo"]);
											$sheet->getStyle('v'.$row)->applyFromArray($styleArray);

											$sheet->setCellValue('w'.$row,$lihat["databankkas"]);
											$sheet->getStyle('w'.$row,$lihat)->applyFromArray($styleArray);

											$sheet->setCellValue('x'.$row,$lihat["keterangan"]);
											$sheet->getStyle('x'.$row)->applyFromArray($styleArray);


                         		 	}
                         		 	
                         		 	
                         		

                         	endforeach;
                    	//$pdf->Cell(30,6,'data nya ada',2,1,'C');

                    }

                   // $pdf->ln(5);

                   
                   // $row1=$row+1;
                    //$sheet->setCellValue('A'.$row1,'Valid :'.$Jumlahcount);
                   // $sheet->mergeCells('A'.$row1.':b'.$row1);
                   // $row1=$row1+1;
                   // $sheet->setCellValue('A'.$row1,'Not Valid :'.$Jumlahnotcount);
                   // $sheet->mergeCells('A'.$row1.':b'.$row1);

                   

            }else{
            	
            }

            	$spreadsheet->createSheet();
				// Zero based, so set the second tab as active sheet
				$spreadsheet->setActiveSheetIndex(1);
				$spreadsheet->getActiveSheet()->setTitle('Rekap Incentive');
				$sheet1 = $spreadsheet->getActiveSheet();
				$sheet1->getColumnDimension('A')->setWidth(50);
    	     $sheet1->getColumnDimension('B')->setWidth(30);
    	     $sheet1->getColumnDimension('C')->setWidth(10);
    	     $sheet1->getColumnDimension('D')->setWidth(10);
    	     $sheet1->getColumnDimension('E')->setWidth(10);
    	     $sheet1->getColumnDimension('F')->setWidth(10);
    	     $sheet1->getColumnDimension('G')->setWidth(10);
    	     $sheet1->getColumnDimension('H')->setWidth(10);
    	     $sheet1->getColumnDimension('I')->setWidth(10);
    	     $sheet1->getColumnDimension('J')->setWidth(10);
    	     $sheet1->getColumnDimension('K')->setWidth(10);
    	     $sheet1->getColumnDimension('L')->setWidth(10);
    	     $sheet1->getColumnDimension('M')->setWidth(10);
    	     $sheet1->getColumnDimension('N')->setWidth(10);
    	     $sheet1->getColumnDimension('O')->setWidth(10);


    	      $styleArray = array(
				  'borders' =>
				array( 'outline' =>
				array(
				'borderStyle' =>
				\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				'color' =>
				array('argb' =>
				'00000000'), ),
				  ),
				);


    	     if(isset($arr)){
                    if ((array_key_exists("nama_cabang",$arr)) && (array_key_exists("select-bulan",$arr)) && (array_key_exists("select-tahun",$arr))) { 

                    	$sheet1->setCellValue('A1', 'Rekap Incentive ');
		   					$sheet1->mergeCells('A1:o1');
		   						$sheet1->getStyle('A1:o1')->getAlignment()->setHorizontal('center');
		   				$sheet1->getRowDimension('2')->setRowHeight(100);
						//$sheet1->getColumnDimension('A')->setWidth(100);

		   				$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
						$drawing->setPath("././image/tandatangan/suzuki1.png"); 
						$drawing->setCoordinates('A2');
						$drawing->setWidth(100);
						$drawing->setHeight(100);

						$drawing->setWorksheet($spreadsheet->getActiveSheet());

		   				if($arr["nama_cabang"]=="641940101"){
                    					//$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Ciawi ",0,1,'L');
                    					$sheet1->setCellValue('A3', 'PT Duta Cendana Adimandiri - Ciawi');
                    				}else if($arr["nama_cabang"]=="641940102"){
                    					//$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Cianjur ",0,1,'L');
                    					$sheet1->setCellValue('A3', 'PT Duta Cendana Adimandiri - Cianjur');
                    				}else if($arr["nama_cabang"]=="641940103"){
                    					//$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Cinere ",0,1,'L');
                    					$sheet1->setCellValue('A3', 'PT Duta Cendana Adimandiri - Cinere');
                    				}else if($arr["nama_cabang"]=="641940104"){
                    					//$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."JatiAsih ",0,1,'L');
                    					$sheet1->setCellValue('A3', 'PT Duta Cendana Adimandiri - Jatiasih');
                    				}

                    				 $sheet1->mergeCells('A3:b3');


              }
             }


             if(isset($arr)){
                    if (array_key_exists("bookingsro",$arr)){ 
                  		$bulan1='';
							//$pdf->Cell(70,10,'SRO : ' .$arr["bookingsro"][0]["fullname"],0,1,'L'); 
                    		//$pdf->SetFont('Arial','B',10);
							if($arr["bookingsro"]==1){
										//$pdf->Cell(40,6, "Bulan :Jan ".$arr["select-tahun"] ,0,1,'L');
										$sheet1->setCellValue('A4', 'Bulan : Jan '.$arr["select-tahun"]);
										$bulan1='Jan '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==2){
										//$pdf->Cell(40,6, "Bulan :Feb ".$arr["select-tahun"],0,1,'L');
										$sheet1->setCellValue('A4', 'Bulan : Feb '.$arr["select-tahun"]);
										$bulan1='Feb '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==3){
										//$pdf->Cell(40,6, "Bulan :Mar ".$arr["select-tahun"],0,1,'L');
										$sheet1->setCellValue('A4', 'Bulan : Mar '.$arr["select-tahun"]);
										$bulan1='Mar '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==4){
										//$pdf->Cell(40,6, "Bulan :Apr ".$arr["select-tahun"],0,1,'L');
										$sheet1->setCellValue('A4', 'Bulan : Apr '.$arr["select-tahun"]);
										$bulan1='Apr '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==5){
										//$pdf->Cell(40,6, "Bulan :Mei ".$arr["select-tahun"],0,1,'L');
										$sheet1->setCellValue('A4', 'Bulan : Mei '.$arr["select-tahun"]);
										$bulan1='Mei '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==6){
										//$pdf->Cell(40,6, "Bulan :Jun ".$arr["select-tahun"],0,1,'L');
										$sheet1->setCellValue('A4', 'Bulan : Jun '.$arr["select-tahun"]);
										$bulan1='Jun '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==7){
										//$pdf->Cell(40,6, "Bulan :Jul ".$arr["select-tahun"],0,1,'L');
										$sheet1->setCellValue('A4', 'Bulan : Jul '.$arr["select-tahun"]);
										$bulan1='Jul '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==8){
										//$pdf->Cell(40,6, "Bulan :Agu ".$arr["select-tahun"],0,1,'L');
										$sheet1->setCellValue('A4', 'Bulan : Agu '.$arr["select-tahun"]);
										$bulan1='Agu '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==9){
										//$pdf->Cell(40,6, "Bulan :Sep ".$arr["select-tahun"],0,1,'L');
										$sheet1->setCellValue('A4', 'Bulan : Sep '.$arr["select-tahun"]);
										$bulan1='Sep '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==10){
										//$pdf->Cell(40,6, "Bulan :Okt ".$arr["select-tahun"],0,1,'L');
										$sheet1->setCellValue('A4', 'Bulan : Okt '.$arr["select-tahun"]);
										$bulan1='Okt '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==11){
										//$pdf->Cell(40,6, "Bulan :Nov ".$arr["select-tahun"],0,1,'L');
										$sheet1->setCellValue('A4', 'Bulan : Nov '.$arr["select-tahun"]);
										$bulan1='Nov '.$arr["select-tahun"];
									}else if($arr["bookingsro"]==12){
										//$pdf->Cell(40,6, "Bulan :Des ".$arr["select-tahun"],0,1,'L');
										$sheet1->setCellValue('A4', 'Bulan : Des '.$arr["select-tahun"]);
										$bulan1='Des '.$arr["select-tahun"];
									}

									$sheet1->mergeCells('A4:b4');
					}else{
							//$pdf->Cell(70,10,'SRO : ',0,1,'L'); 
					}
			}else{
							//$pdf->Cell(70,10,'SRO : ',0,1,'L'); 
			}

			$sheet1->setCellValue("A6", "Nama Karyawan");
			$sheet1->mergeCells("A6:A8");
			$sheet1->getStyle('A6:A8')->getAlignment()->setHorizontal('center');
			$sheet1->getStyle('A6:A8')->getAlignment()->setvertical('center');
			$sheet1->getStyle("A6:A8")->applyFromArray($styleArray);

			$sheet1->setCellValue("b6", "Jabatan");
			$sheet1->mergeCells("b6:b8");
			$sheet1->getStyle('b6:b8')->getAlignment()->setHorizontal('center');
			$sheet1->getStyle('b6:b8')->getAlignment()->setvertical('center');
			$sheet1->getStyle("b6:b8")->applyFromArray($styleArray);

			$sheet1->setCellValue("c6", "Scema");
			$sheet1->mergeCells("c6:c8");
			$sheet1->getStyle('c6:c8')->getAlignment()->setHorizontal('center');
			$sheet1->getStyle('c6:c8')->getAlignment()->setvertical('center');
			$sheet1->getStyle("c6:c8")->applyFromArray($styleArray);

			$sheet1->setCellValue("D6", "Incentive");
			$sheet1->mergeCells("d6:N6");
			$sheet1->getStyle('d6:N7')->getAlignment()->setHorizontal('center');
			$sheet1->getStyle('d6:N7')->getAlignment()->setvertical('center');
			$sheet1->getStyle("d6:N7")->applyFromArray($styleArray);

			$sheet1->setCellValue("D7", "Jasa Service");
			$sheet1->mergeCells("D7:E7");
			$sheet1->getStyle('D7:E7')->getAlignment()->setHorizontal('center');
			$sheet1->getStyle('D7:E7')->getAlignment()->setvertical('center');
			$sheet1->getStyle("D7:E7")->applyFromArray($styleArray);

			$sheet1->setCellValue("F7", "Sparepart");
			$sheet1->mergeCells("F7:G7");
			$sheet1->getStyle('F7:G7')->getAlignment()->setHorizontal('center');
			$sheet1->getStyle('F7:G7')->getAlignment()->setvertical('center');
			$sheet1->getStyle("F7:G7")->applyFromArray($styleArray);

			$sheet1->setCellValue("H7", "Oli");
			$sheet1->mergeCells("H7:I7");
			$sheet1->getStyle('H7:I7')->getAlignment()->setHorizontal('center');
			$sheet1->getStyle('H7:I7')->getAlignment()->setvertical('center');
			$sheet1->getStyle("H7:I7")->applyFromArray($styleArray);


			$sheet1->setCellValue("j7", "Scema");
			$sheet1->mergeCells("J7:J8");
			$sheet1->getStyle('J7:J8')->getAlignment()->setHorizontal('center');
			$sheet1->getStyle('J7:J8')->getAlignment()->setvertical('center');
			$sheet1->getStyle("J7:J8")->applyFromArray($styleArray);


			$sheet1->setCellValue("K7", "Spooring & Balancing");
			$sheet1->mergeCells("K7:L7");
			$sheet1->getStyle('K7:L7')->getAlignment()->setHorizontal('center');
			$sheet1->getStyle('K7:L7')->getAlignment()->setvertical('center');
			$sheet1->getStyle("K7:L7")->applyFromArray($styleArray);

			$sheet1->setCellValue("M7", "Chemical");
			$sheet1->mergeCells("M7:M8");
			$sheet1->getStyle('M7:M8')->getAlignment()->setHorizontal('center');
			$sheet1->getStyle('M7:M8')->getAlignment()->setvertical('center');
			$sheet1->getStyle("M7:M8")->applyFromArray($styleArray);

			$sheet1->setCellValue("N7", "total");
			$sheet1->mergeCells("N7:N8");
			$sheet1->getStyle('N7:N8')->getAlignment()->setHorizontal('center');
			$sheet1->getStyle('N7:N8')->getAlignment()->setvertical('center');
			$sheet1->getStyle("N7:N8")->applyFromArray($styleArray);


			$sheet1->setCellValue("D8", "%");
			$sheet1->getStyle("D8")->applyFromArray($styleArray);

			$sheet1->setCellValue("E8", "Rp.");
			$sheet1->getStyle("E8")->applyFromArray($styleArray);

			$sheet1->setCellValue("F8", "%");
			$sheet1->getStyle("F8")->applyFromArray($styleArray);

			$sheet1->setCellValue("G8", "Rp.");
			$sheet1->getStyle("G8")->applyFromArray($styleArray);

			$sheet1->setCellValue("H8", "%");
			$sheet1->getStyle("H8")->applyFromArray($styleArray);

			$sheet1->setCellValue("I8", "Rp.");
			$sheet1->getStyle("I8")->applyFromArray($styleArray);

			$sheet1->setCellValue("k8", "%");
			$sheet1->getStyle("k8")->applyFromArray($styleArray);

			$sheet1->setCellValue("l8", "Rp.");
			$sheet1->getStyle("l8")->applyFromArray($styleArray);

			$sheet1->setCellValue("m8", "%");
			$sheet1->getStyle("m8")->applyFromArray($styleArray);

			$sheet1->setCellValue("n8", "Rp.");
			$sheet1->getStyle("n8")->applyFromArray($styleArray);


			$sheet1->getStyle('A6:n8')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('37E2D5');


			

			//isi data sheet 2 disini

			if(isset($arrdata)){
                    if (array_key_exists("rekapIncetive",$arrdata)){ 
                    	     $positonName="";
                    		 $row1 =9;
                    		 $jumlahSRO=0;
                    		 $jumlahAdminsparepart=0;
                    		 $jumlahWasher=0;
                    		 $jumlahOfficeboy=0;
                    		 $jumlahServieAdvisor=0;
                    		 $jumlahMekanik=0;

                    		 $jumlahSRORow=0;
                    		 $jumlahAdminsparepartRow=0;
                    		 $jumlahWasherRow=0;
                    		 $jumlahOfficeboyRow=0;
                    		 $jumlahServieAdvisorRow=0;
                    		 $jumlahMekanikRow=0;

                    		 foreach ($arr["rekapIncetive"] as $lihat):
                    		 	$sheet1->setCellValue('a'.$row1,$lihat["nama"]);
                    		 	$sheet1->getStyle('a'.$row1)->applyFromArray($styleArray);

                    		 	$sheet1->setCellValue('b'.$row1,$lihat["position"]);
                    		 	$sheet1->getStyle('b'.$row1)->applyFromArray($styleArray);

                    		 	$sheet1->setCellValue('c'.$row1,$lihat["scema"]);
                    		 	$sheet1->getStyle('c'.$row1)->applyFromArray($styleArray);

                    		 	if($lihat["position"]=="SPVCCM" || $lihat["position"]=="ADMCCM" || $lihat["position"]=="SRO" || $lihat["position"]=="CRO"  ){
                    		 		$jumlahSRO++;
                    		 		$jumlahSRORow=$row1;
                    		 	}else if($lihat["position"]=="ADMINISTRASI SPAREPART"){
                    		 		$jumlahAdminsparepart++;
                    		 		$jumlahAdminsparepartRow=$row1;
                    		 	}else if($lihat["position"]=="WASHER"){
									$jumlahWasher++;
									$jumlahWasherRow=$row1;
								}else if($lihat["position"]=="OFFICEBOY"){
									$jumlahOfficeboy++;
									$jumlahOfficeboyRow=$row1;
								}else if($lihat["position"]=="SERVICEADVISOR"){
									$jumlahServieAdvisor++;
									 $jumlahServieAdvisorRow=$row1;
								}else if($lihat["position"]=="MEKANIK"){
									$jumlahMekanik++; 
									$jumlahMekanikRow=$row1;
								}

                    		 	

                    		 	



                    		 	if($lihat["%MSI"]<>100){
                    		 		$sheet1->setCellValue('d'.$row1,$lihat["%MSI"]);
                    		 		$sheet1->getStyle('d'.$row1)->applyFromArray($styleArray);
                    		 	}else{
                    		 		$sheet1->setCellValue('d'.$row1,"");
                    		 		$sheet1->getStyle('d'.$row1)->applyFromArray($styleArray);
                    		 	}
                    		 	
                    		 	$sheet1->setCellValue('E'.$row1,$lihat["incLabour"]);
                    		 	$sheet1->getStyle('e'.$row1)->applyFromArray($styleArray);
                    		 	$sheet1->getStyle('F'.$row1)->applyFromArray($styleArray);

                    		 	$sheet1->setCellValue('g'.$row1,$lihat["incPart"]);
                    		 	$sheet1->getStyle('g'.$row1)->applyFromArray($styleArray);
                    		 	$sheet1->getStyle('H'.$row1)->applyFromArray($styleArray);

                    		 	$sheet1->setCellValue('i'.$row1,$lihat["incOli"]);
                    		 	$sheet1->getStyle('i'.$row1)->applyFromArray($styleArray);
                    		 	
                    		 
                    		 	if($lihat["scemaSB"]<>0){
                    		 			$sheet1->setCellValue('j'.$row1,$lihat["scemaSB"]);
                    		 			$sheet1->getStyle('j'.$row1)->applyFromArray($styleArray);

                    		 	}else{

                    		 		$sheet1->setCellValue('j'.$row1,"");
                    		 		$sheet1->getStyle('j'.$row1)->applyFromArray($styleArray);
                    		 	}
                    		 	
                    		 	if($lihat["%sb"]<>100){
                    		 		$sheet1->setCellValue('K'.$row1,$lihat["%sb"]);
                    		 		$sheet1->getStyle('k'.$row1)->applyFromArray($styleArray);
                    		 	}else{
                    		 		$sheet1->setCellValue('K'.$row1,"");
                    		 		$sheet1->getStyle('k'.$row1)->applyFromArray($styleArray);
                    		 	}

                    		 	if($lihat["scemaSB"]<>0){
                    		 			$sheet1->setCellValue('L'.$row1,$lihat["incSB"]);
                    		 			$sheet1->getStyle('l'.$row1)->applyFromArray($styleArray);

                    		 	}else{

                    		 		$sheet1->setCellValue('L'.$row1,"");
                    		 		$sheet1->getStyle('l'.$row1)->applyFromArray($styleArray);
                    		 	}


                    		 	
                    		 	if(isset($lihat["incChemcial"])){
											 $sheet1->setCellValue('M'.$row1,$lihat["incChemcial"]);
											 $sheet1->getStyle('M'.$row1)->applyFromArray($styleArray);

                    		 	}else{
                    		 				$sheet1->setCellValue('M'.$row1,"");
                    		 				$sheet1->getStyle('M'.$row1)->applyFromArray($styleArray);
	
                    		 	}
                    		 	$sheet1->setCellValue('N'.$row1,$lihat["total"]);
                    		 	$sheet1->getStyle('N'.$row1)->applyFromArray($styleArray);
                    		 	$row1++;
                    		 	

                    		 endforeach;
                    		 $sheet1->mergeCells("C".($jumlahSRORow-($jumlahSRO-1)).':'."C".($jumlahSRORow));

                    		 $sheet1->getStyle("C".($jumlahSRORow-($jumlahSRO-1)).':'."C".($jumlahSRORow))->getAlignment()->setHorizontal('center');
							 $sheet1->getStyle("C".($jumlahSRORow-($jumlahSRO-1)).':'."C".($jumlahSRORow))->getAlignment()->setvertical('center');

                    		 $sheet1->mergeCells("C".($jumlahAdminsparepartRow-($jumlahAdminsparepart-1)).':'."C".($jumlahAdminsparepartRow));

                    		 $sheet1->getStyle("C".($jumlahAdminsparepartRow-($jumlahAdminsparepart-1)).':'."C".($jumlahAdminsparepartRow))->getAlignment()->setHorizontal('center');

							 $sheet1->getStyle("C".($jumlahAdminsparepartRow-($jumlahAdminsparepart-1)).':'."C".($jumlahAdminsparepartRow))->getAlignment()->setvertical('center');

                    		 $sheet1->mergeCells("C".($jumlahWasherRow-($jumlahWasher-1)).':'."C".($jumlahWasherRow));

                    		 $sheet1->getStyle("C".($jumlahWasherRow-($jumlahWasher-1)).':'."C".($jumlahWasherRow))->getAlignment()->setHorizontal('center');
							 $sheet1->getStyle("C".($jumlahWasherRow-($jumlahWasher-1)).':'."C".($jumlahWasherRow))->getAlignment()->setvertical('center');


                    		 $sheet1->mergeCells("C".($jumlahOfficeboyRow-($jumlahOfficeboy-1)).':'."C".($jumlahOfficeboyRow));


                    		 $sheet1->getStyle("C".($jumlahOfficeboyRow-($jumlahOfficeboy-1)).':'."C".($jumlahOfficeboyRow))->getAlignment()->setHorizontal('center');
			                 $sheet1->getStyle("C".($jumlahOfficeboyRow-($jumlahOfficeboy-1)).':'."C".($jumlahOfficeboyRow))->getAlignment()->setvertical('center');



                    		 $sheet1->mergeCells("C".($jumlahServieAdvisorRow-($jumlahServieAdvisor-1)).':'."C".($jumlahServieAdvisorRow));

                    		 $sheet1->getStyle("C".($jumlahServieAdvisorRow-($jumlahServieAdvisor-1)).':'."C".($jumlahServieAdvisorRow))->getAlignment()->setHorizontal('center');
							 $sheet1->getStyle("C".($jumlahServieAdvisorRow-($jumlahServieAdvisor-1)).':'."C".($jumlahServieAdvisorRow))->getAlignment()->setvertical('center');



                    		 $sheet1->mergeCells("C".($jumlahMekanikRow-($jumlahMekanik-1)).':'."C".($jumlahMekanikRow));

                    		 $sheet1->getStyle("C".($jumlahMekanikRow-($jumlahMekanik-1)).':'."C".($jumlahMekanikRow))->getAlignment()->setHorizontal('center');
							 $sheet1->getStyle("C".($jumlahMekanikRow-($jumlahMekanik-1)).':'."C".($jumlahMekanikRow))->getAlignment()->setvertical('center');

                    		  //$sheet1->mergeCells("j".($jumlahWasherRow-($jumlahWasher-1)).':'."j".($jumlahWasherRow));

                    		 $sheet1->mergeCells("j".($jumlahServieAdvisorRow-($jumlahServieAdvisor-1)).':'."j".($jumlahServieAdvisorRow));

                    		 $sheet1->getStyle("j".($jumlahServieAdvisorRow-($jumlahServieAdvisor-1)).':'."j".($jumlahServieAdvisorRow))->getAlignment()->setHorizontal('center');
							 $sheet1->getStyle("j".($jumlahServieAdvisorRow-($jumlahServieAdvisor-1)).':'."j".($jumlahServieAdvisorRow))->getAlignment()->setvertical('center');

                    		

                    		 $sheet1->mergeCells("j".($jumlahOfficeboyRow-($jumlahOfficeboy-1)).':'."j".($jumlahOfficeboyRow));


                    		 $sheet1->getStyle("j".($jumlahOfficeboyRow-($jumlahOfficeboy-1)).':'."j".($jumlahOfficeboyRow))->getAlignment()->setHorizontal('center');
							 $sheet1->getStyle("j".($jumlahOfficeboyRow-($jumlahOfficeboy-1)).':'."j".($jumlahOfficeboyRow))->getAlignment()->setvertical('center');

							 $sheet1->getStyle("C9".':'."C".$row1)->getAlignment()->setHorizontal('center');
							 $sheet1->getStyle("C9".':'."C".$row1)->getAlignment()->setvertical('center');

							  $sheet1->getStyle("j9".':'."j".$row1)->getAlignment()->setHorizontal('center');
							 $sheet1->getStyle("j9".':'."j".$row1)->getAlignment()->setvertical('center');

							  $sheet1->getStyle("d9".':'."d".$row1)->getAlignment()->setHorizontal('center');
							 $sheet1->getStyle("d9".':'."d".$row1)->getAlignment()->setvertical('center');

							  $sheet1->getStyle("k9".':'."k".$row1)->getAlignment()->setHorizontal('center');
							 $sheet1->getStyle("k9".':'."k".$row1)->getAlignment()->setvertical('center');

                    }
            }

            /* $totalIncentiveLabourProku=0;
            $totalIncentivePartProku=0;
            $totalIncentiveOliProku=0;

            if(isset($arr)){
                    if (array_key_exists("fullcalincentiveProku",$arr)){ 

                    	foreach($arr["fullcalincentiveProku"] as $fullProku):
                    			$totalIncentiveLabourProku=$totalIncentiveLabourProku+$fullProku["labour_paid"];
                    			$totalIncentivePartProku=$totalIncentivePartProku+$fullProku['sparepart_paid'];
                    			$totalIncentiveOliProku=$totalIncentiveOliProku+$fullProku["lubricant_paid"];

           			  endforeach;
                    }

                    $sheet1->setCellValue('P9'," Total Incentive Proku");
                    $sheet1->mergeCells('P9:Q9');
                    $sheet1->setCellValue('P10'," Jasa");
                    $sheet1->setCellValue('P11'," Part");
                    $sheet1->setCellValue('P12'," Oli");

                    $sheet1->setCellValue('Q10',$totalIncentiveLabourProku);
                    $sheet1->setCellValue('R11',$totalIncentivePartProku);
                    $sheet1->setCellValue('S12',$totalIncentiveOliProku);
             }

	*/


            

            

            $sheet1->setCellValue('A'.($row1+2)," Dibuat Oleh");
            $sheet1->setCellValue('b'.($row1+2)," Disetujui");
            $sheet1->mergeCells('B'.($row1+2).':'.'c'.($row1+2));

            if(isset($arr)){
                    if (array_key_exists("PIC",$arr)){ 
                    	$i = 0;
                    	$countarray=count($arr["PIC"]);
                    	$sheet1->getRowDimension(($row1+3))->setRowHeight(100);
                           	foreach ($arr["PIC"] as $lihat):
                           		 $res = preg_replace('/[\@\.\;\" "]+/', '', $lihat["aprroveby"]);
                           		 if(++$i === $countarray) {
    								//$pdf->cell(25,20,$pdf->Image(base_url()."image/tandatangan/".$res.".png",  $pdf->GetX(), $pdf->GetY(),25),0,1,'C');

    								$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
									$drawing->setPath("././image/tandatangan/041205010.png");
									$drawing->setCoordinates('A'.($row1+3));
									$drawing->setWidth(100);
									$drawing->setHeight(100);
									$drawing->setWorksheet($spreadsheet->getActiveSheet());

  								 }else{
  								 	//$pdf->cell(25,20,$pdf->Image(base_url()."image/tandatangan/".$res.".png",  $pdf->GetX(), $pdf->GetY(),25),0,0,'C');

  								 	$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
									$drawing->setPath("././image/tandatangan/041205010.png");
									$drawing->setCoordinates('A',($row1+3));
									$drawing->setWidth(100);
									$drawing->setHeight(100);
									$drawing->setWorksheet($spreadsheet->getActiveSheet());
  								 }
                          		  
                       //    		$res =  $lihat->b.aprroveby;
									
               
                     		endforeach;
                    	//$pdf->cell(30,20,print_r($arr["PIC"]),1,0,'C');
                    }else{
                    	//$pdf->Cell(30,6,"Disetujui 111",1,0,'C');
                    }
               }else{
               	//$pdf->Cell(30,6,"Disetujui22222",1,0,'C');
               }
            

            $sheet1->setCellValue('A'.($row1+4)," Admin Service");

           $spreadsheet->setActiveSheetIndex(0);

	    	 //batas akhir excel


// 	    	  $pdf = new FPDF('l','mm', "A4");
// 	    	 //$pdf = new FPDF("landscape","A4");
	    	
// 	        // membuat halaman baru
// 	        $pdf->AddPage();
// 			$pdf->SetMargins(8, 25, 1,1);
// 	    	$pdf->SetFont('Arial','B',11);
// 	    	//$pdf->setAutoPageBreak(true, 5);
// 	        // mencetak string 

// 	    	if(isset($arr)){
//                     if ((array_key_exists("nama_cabang",$arr)) && (array_key_exists("select-bulan",$arr)) && (array_key_exists("select-tahun",$arr))) { 



						    


//                     				//$pdf->cell(25,20,$pdf->Image(base_url()."image/tandatangan/suzuki.png",  $pdf->GetX(), $pdf->GetY(),30),0,1,'C');
//                     				//$pdf->Image(base_url()."image/tandatangan/suzuki.png" ,10,-5,30,20,'png','');

//                     				$pdf->Ln(5);

//                     				$pdf->Cell(22,6,'Authorized ',0,0,'l');
//                     				$pdf->SetTextColor(0,0,255);
//                     				$pdf->Cell(14,6,'Suzuki ',0,0,'l');
//                     				$pdf->SetTextColor(0,0,0);
//                     				$pdf->Cell(30,6,'Dealer ',0,1,'l');

//                     				 $pdf->Ln(5);


//                     				$pdf->SetFont('Arial','B',10);
//                     				if($arr["nama_cabang"]=="641940101"){
//                     					$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Ciawi ",0,1,'L');
//                     				}else if($arr["nama_cabang"]=="641940102"){
//                     					$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Cianjur ",0,1,'L');
//                     				}else if($arr["nama_cabang"]=="641940103"){
//                     					$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Cinere ",0,1,'L');
//                     				}else if($arr["nama_cabang"]=="641940104"){
//                     					$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."JatiAsih ",0,1,'L');
//                     				}

//                     				//$pdf->Cell(320,5,'Bulan : '.$arr["select-bulan"],0,1,'C');
//                     				//$pdf->Cell(320,5,'Tahun : '.$arr["select-tahun"],0,1,'C');

                    				
							
// 					}else{
// 							//$pdf->Cell(70,10,'SRO : ',0,1,'L'); 
// 					}
// 			}else{
// 							//$pdf->Cell(70,10,'SRO : ',0,1,'L'); 
// 			}





	        	
// 				//$pdf->Cell(25,4,'',0,1);

// 			$pdf->SetFont('Arial','',8);

// 			if(isset($arr)){
//                     if (array_key_exists("bookingsro",$arr)){ 
//                   		$bulan1='';
// 							//$pdf->Cell(70,10,'SRO : ' .$arr["bookingsro"][0]["fullname"],0,1,'L'); 
//                     		$pdf->SetFont('Arial','B',10);
// 							if($arr["bookingsro"]==1){
// 										$pdf->Cell(40,6, "Bulan :Jan ".$arr["select-tahun"] ,0,1,'L');
// 										$bulan1='Jan '.$arr["select-tahun"];
// 									}else if($arr["bookingsro"]==2){
// 										$pdf->Cell(40,6, "Bulan :Feb ".$arr["select-tahun"],0,1,'L');
// 										$bulan1='Feb '.$arr["select-tahun"];
// 									}else if($arr["bookingsro"]==3){
// 										$pdf->Cell(40,6, "Bulan :Mar ".$arr["select-tahun"],0,1,'L');
// 										$bulan1='Mar '.$arr["select-tahun"];
// 									}else if($arr["bookingsro"]==4){
// 										$pdf->Cell(40,6, "Bulan :Apr ".$arr["select-tahun"],0,1,'L');
// 										$bulan1='Apr '.$arr["select-tahun"];
// 									}else if($arr["bookingsro"]==5){
// 										$pdf->Cell(40,6, "Bulan :Mei ".$arr["select-tahun"],0,1,'L');
// 										$bulan1='Mei '.$arr["select-tahun"];
// 									}else if($arr["bookingsro"]==6){
// 										$pdf->Cell(40,6, "Bulan :Jun ".$arr["select-tahun"],0,1,'L');
// 										$bulan1='Jun '.$arr["select-tahun"];
// 									}else if($arr["bookingsro"]==7){
// 										$pdf->Cell(40,6, "Bulan :Jul ".$arr["select-tahun"],0,1,'L');
// 										$bulan1='Jul '.$arr["select-tahun"];
// 									}else if($arr["bookingsro"]==8){
// 										$pdf->Cell(40,6, "Bulan :Agu ".$arr["select-tahun"],0,1,'L');
// 										$bulan1='Agu '.$arr["select-tahun"];
// 									}else if($arr["bookingsro"]==9){
// 										$pdf->Cell(40,6, "Bulan :Sep ".$arr["select-tahun"],0,1,'L');
// 										$bulan1='Sep '.$arr["select-tahun"];
// 									}else if($arr["bookingsro"]==10){
// 										$pdf->Cell(40,6, "Bulan :Okt ".$arr["select-tahun"],0,1,'L');
// 										$bulan1='Okt '.$arr["select-tahun"];
// 									}else if($arr["bookingsro"]==11){
// 										$pdf->Cell(40,6, "Bulan :Nov ".$arr["select-tahun"],0,1,'L');
// 										$bulan1='Nov '.$arr["select-tahun"];
// 									}else if($arr["bookingsro"]==12){
// 										$pdf->Cell(40,6, "Bulan :Des ".$arr["select-tahun"],0,1,'L');
// 										$bulan1='Des '.$arr["select-tahun"];
// 									}
// 					}else{
// 							//$pdf->Cell(70,10,'SRO : ',0,1,'L'); 
// 					}
// 			}else{
// 							//$pdf->Cell(70,10,'SRO : ',0,1,'L'); 
// 			}

// 			$pdf->SetFont('Arial','',8);
// 			$pdf->SetFillColor(132, 188, 220);
// 			$pdf->Cell(10,6,'No',1,0,'C',1);;
// 			$pdf->Cell(20,6,'Booking Date',1,0,'C',1);
// 			$pdf->Cell(20,6,'SPK Date',1,0,'C',1);
// 			$pdf->Cell(20,6,'Created Date',1,0,'C',1);
// 			$pdf->Cell(20,6,'Created By',1,0,'C',1);
// 			$pdf->Cell(25,6,'Booking No',1,0,'C',1);
// 			$pdf->Cell(60,6,'Customer ',1,0,'C',1);
// 			$pdf->Cell(20,6,'Police No',1,0,'C',1);
// 			$pdf->Cell(25,6,'SPK NO',1,0,'C',1);
// 			$pdf->Cell(25,6,'Invoice',1,0,'C',1);
// 			$pdf->Cell(20,6,'Ket',1,1,'C',1);
// 			if(isset($arr)){
//                     if (array_key_exists("datadetillistbooking",$arr)){ 
//                          $no = 1;
//                          $Jumlahcount=0;
//                          $Jumlahnotcount=0;
//                          	foreach ($arr["datadetillistbooking"] as $lihat):
                         		 
//                          		 	if($lihat["keterangan"]=="not"){
//                          		 			$pdf->SetFillColor(255,0, 0);
//                          		 			$Jumlahnotcount++;
//                          		 	}else{
//                          		 			$pdf->SetFillColor(255,255, 255);
//                          		 			$Jumlahcount++;
//                          		 	}
                         		 	
//                          		 	$pdf->Cell(10,6, $no++,1,0,'C',1);
// 									$pdf->Cell(20,6, $lihat["bookingdate"],1,0,'C',1);
						
// 									$pdf->Cell(20,6,$lihat["SPK_date"],1,0,'C',1);
// 									$pdf->Cell(20,6,$lihat["CreatedDate"],1,0,'C',1);
// 									$pdf->Cell(20,6,$lihat["createdby"],1,0,'C',1);
// 									$pdf->Cell(25,6,$lihat["BookingNo"],1,0,'C',1);
// 									$pdf->Cell(60,6,$lihat["CustomerName"],1,0,'C',1);
// 									$pdf->Cell(20,6,$lihat["PoliceRegNo"],1,0,'C',1);
// 									$pdf->Cell(25,6,$lihat["SPK_NO"],1,0,'C',1);
// 									$pdf->Cell(25,6,$lihat["invoiceno"],1,0,'C',1);
// 									$pdf->Cell(20,6,$lihat["keterangan"],1,1,'C',1);

									

									   
// 									// $pdf->Cell(20,6, $lihat["bookingdate"],1,0,'C',1);
// 									// $pdf->Cell(20,6, $lihat["SPK_date"],1,0,'C',1);
// 									// $pdf->Cell(20,6, $lihat["PoliceRegNo"],1,0,'C',1);
// 									// $pdf->Cell(30,6, $lihat["SPKNo"],1,0,'C',1);
// 									// $pdf->Cell(30,6, $lihat["invoiceno"],1,0,'C',1);
// 									// $pdf->Cell(40,6, $lihat["invoicedate"],1,0,'C',1);
// 									// $pdf->Cell(30,6, $lihat["status"],1,1,'C',1);
                         		

//                          	endforeach;
//                     	//$pdf->Cell(30,6,'data nya ada',2,1,'C');

//                     }

//                     $pdf->ln(5);

//                     $pdf->SetFillColor(255,0, 0);
//                     $pdf->Cell(35,6,'Valid',1,0,'C',1);

//                     $pdf->SetFillColor(255,255, 255);
//                     $pdf->Cell(35,6,$Jumlahcount,1,1,'C',1);

//                     $pdf->SetFillColor(255,0, 0);
//                     $pdf->Cell(35,6,'Not Valid',1,0,'C',1);

//                     $pdf->SetFillColor(255,255, 255);
//                     $pdf->Cell(35,6,$Jumlahnotcount,1,0,'C',1);

//             }else{
            	
//             }

//    //          $pdf->Cell(25,4,'',0,1);

//    //          if(isset($arr)){
//    //                  if (array_key_exists("bookingsro2",$arr)){ 
// 			// 				$pdf->Cell(70,10,'SRO : ' .$arr["bookingsro2"][0]["fullname"],0,1,'L'); 
// 			// 		}else{
// 			// 				$pdf->Cell(70,10,'SRO : tidak ada ',0,1,'L'); 
// 			// 		} 
// 			// }else{
// 			// 				$pdf->Cell(70,10,'SRO : tidak ada ',0,1,'L'); 
// 			// }

// 			// $pdf->SetFillColor(132, 188, 220);
// 			// $pdf->Cell(10,6,'No',1,0,'C',1);;
// 			// $pdf->Cell(40,6,'Created Date',1,0,'C',1);
// 			// $pdf->Cell(40,6,'Created Date SPK Booking',1,0,'C',1);
// 			// $pdf->Cell(20,6,'Booking Date',1,0,'C',1);
// 			// $pdf->Cell(20,6,'SPK Date',1,0,'C',1);
// 			// $pdf->Cell(20,6,'Police Reg No',1,0,'C',1);
// 			// $pdf->Cell(30,6,'SPK No',1,0,'C',1);
// 			// $pdf->Cell(30,6,'Invoice No',1,0,'C',1);
// 			// $pdf->Cell(40,6,'Invoice Date',1,0,'C',1);
// 			// $pdf->Cell(30,6,'Status',1,1,'C',1);
// 			// if(isset($arr)){
//    //                  if (array_key_exists("bookingsro2",$arr)){ 
//    //                       $no = 1;
//    //                       	foreach ($arr["bookingsro2"] as $lihat):

//    //                       		 if($lihat["status"]=="counted"){
//    //                       		 	$pdf->SetFillColor(255, 0, 0);
//    //                       		 	$pdf->Cell(10,6, $no++,1,0,'C',1);;
// 			// 						$pdf->Cell(40,6, $lihat["createddate"],1,0,'C',1);
// 			// 						$pdf->Cell(40,6, $lihat["createddate_SPK_Booking"],1,0,'C',1);
// 			// 						$pdf->Cell(20,6, $lihat["bookingdate"],1,0,'C',1);
// 			// 						$pdf->Cell(20,6, $lihat["SPK_date"],1,0,'C',1);
// 			// 						$pdf->Cell(20,6, $lihat["PoliceRegNo"],1,0,'C',1);
// 			// 						$pdf->Cell(30,6, $lihat["SPKNo"],1,0,'C',1);
// 			// 						$pdf->Cell(30,6, $lihat["invoiceno"],1,0,'C',1);
// 			// 						$pdf->Cell(40,6, $lihat["invoicedate"],1,0,'C',1);
// 			// 						$pdf->Cell(30,6, $lihat["status"],1,1,'C',1);
//    //                       		 }else{
//    //                       		 	$pdf->Cell(10,6, $no++,1,0,'C');;
// 			// 						$pdf->Cell(40,6, $lihat["createddate"],1,0,'C');
// 			// 						$pdf->Cell(40,6, $lihat["createddate_SPK_Booking"],1,0,'C');
// 			// 						$pdf->Cell(20,6, $lihat["bookingdate"],1,0,'C');
// 			// 						$pdf->Cell(20,6, $lihat["SPK_date"],1,0,'C');
// 			// 						$pdf->Cell(20,6, $lihat["PoliceRegNo"],1,0,'C');
// 			// 						$pdf->Cell(30,6, $lihat["SPKNo"],1,0,'C');
// 			// 						$pdf->Cell(30,6, $lihat["invoiceno"],1,0,'C');
// 			// 						$pdf->Cell(40,6, $lihat["invoicedate"],1,0,'C');
// 			// 						$pdf->Cell(30,6, $lihat["status"],1,1,'C');
//    //                       		 }
                         			
									

//    //                       	endforeach;
//    //                  	//$pdf->Cell(30,6,'data nya ada',2,1,'C');

//    //                  }

//    //          }else{
            	
//    //          }


// //$pdf->cell(30,6,"sro",0,0,'C');


            //$pdf->Output('D','listDetailBooking-'.$bulan1.'.pdf', true);

            
			$cabang1="";
			if($arr["nama_cabang"]=="641940101"){
					$cabang1="Ciawi";
			} else if($arr["nama_cabang"]=="641940102"){
					$cabang1="Cianjur";
			} else if($arr["nama_cabang"]=="641940103"){
					$cabang1="Cinere";
			} else if($arr["nama_cabang"]=="641940104"){
					$cabang1="Jatiasih";
			}

			//$writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
			//$writer->save('output.xlsx');

			$writer = new Xlsx($spreadsheet);

			$filename = 'listDataIncentiveService'.$cabang1.$bulan1;
			ob_end_clean();
			//header('Content-Type: application/vnd.ms-excel');
			 header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"'); 
			header('Cache-Control: max-age=0');

			$writer = new Xlsx($spreadsheet);

			//$filename = 'LisdataBookingsro';
	
	
			$writer->save('php://output');
			
		}


		function cetakRekapProku($bulan,$tahun,$cabang,$idpengajuan){

			$arr2['chemicalIncetive']=$this->calInsentiveChemical($cabang, $tahun,$bulan);
    		$arr2['spooringIncentive']=$this->calIncentiveSpooring($cabang, $tahun, $bulan);

    		$arr2["spooringTotalMargin&Incentive"]=$this->mIncentiveService->getFullIncentiveSpooring($cabang, $tahun, $bulan);
    		$arr2["chemicalTotalMargin&Incentive"]=$this->mIncentiveService->getfullIncentiveChemical($cabang,$tahun,$bulan);

    		$arr2["indukProku"]=$this->indukProku($cabang, $tahun, $bulan, $this->$arr1);
    		$arr2["prokuIncentive"]= $this->calIncentiveMSI($cabang, $tahun, $bulan, $this->$arr1);

        	$arr2["select-bulan"]=$bulan;

        
        	$arr2["nama_cabang"]=$cabang;


        	$styleArray = array(
				  'borders' =>array(
				   'outline' =>	array(
				     'borderStyle' =>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				         'color' =>array('argb' =>'00000000'), 
				     ),
				  ),
				);


        	$spreadsheet = new Spreadsheet();
	    	 $spreadsheet->getActiveSheet()->setTitle("Proku");

	    	  $sheet = $spreadsheet->getActiveSheet();

	    	 $sheet->getColumnDimension('A')->setWidth(30);
    	     $sheet->getColumnDimension('B')->setWidth(30);
    	     $sheet->getColumnDimension('C')->setWidth(30);
    	     $sheet->getColumnDimension('D')->setWidth(30);
    	     $sheet->getColumnDimension('E')->setWidth(30);
    	     $sheet->getColumnDimension('F')->setWidth(30);
    	     $sheet->getColumnDimension('G')->setWidth(30);
    	     $sheet->getColumnDimension('H')->setWidth(30);

	    	  $sheet->setCellValue("A3","Keterangan");
	    	  $sheet->getStyle("a3")->applyFromArray($styleArray);
	    	  $sheet->setCellValue("B3"," Labour");
	    	  $sheet->getStyle("b3")->applyFromArray($styleArray);
	    	  $sheet->setCellValue("C3"," Sparepart");
	    	  $sheet->getStyle("c3")->applyFromArray($styleArray);
	    	  $sheet->setCellValue("D3"," OLI");
	    	  $sheet->getStyle("d3")->applyFromArray($styleArray);
	    	  
	    	  

	    	    if (array_key_exists("indukProku",$arr2)){
	    	    	$row=4;

	    	    	$sheet->getColumnDimension('A')->setWidth(30);
    	     $sheet->getColumnDimension('B')->setWidth(30);
    	     $sheet->getColumnDimension('C')->setWidth(30);
    	     $sheet->getColumnDimension('D')->setWidth(30);

	    	  $sheet->setCellValue("A3","Keterangan");
	    	  $sheet->getStyle("a3")->applyFromArray($styleArray);
	    	  $sheet->setCellValue("B3"," Labour");
	    	  $sheet->getStyle("b3")->applyFromArray($styleArray);
	    	  $sheet->setCellValue("C3"," Sparepart");
	    	  $sheet->getStyle("c3")->applyFromArray($styleArray);
	    	  $sheet->setCellValue("D3"," OLI");
	    	  $sheet->getStyle("d3")->applyFromArray($styleArray);

	    	  $sheet->getStyle('A3:d3')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('37E2D5');

	    	    	foreach($arr2["indukProku"] as $lihat):
	    	    		$sheet->setCellValue("A".$row,$lihat["Jenis"]);
	    	    		 $sheet->getStyle("A".$row)->applyFromArray($styleArray);
	    	    		$sheet->setCellValue("B".$row,$lihat["labour"]);
	    	    		 $sheet->getStyle("B".$row)->applyFromArray($styleArray);
	    	    		$sheet->setCellValue("C".$row,$lihat["sparepart"]);
	    	    		 $sheet->getStyle("C".$row)->applyFromArray($styleArray);
	    	    		$sheet->setCellValue("D".$row,$lihat["lubricant"]);
	    	    		 $sheet->getStyle("D".$row)->applyFromArray($styleArray);
	    	    		$row++;
	    	    	endforeach;
	    	    }



	    	    if (array_key_exists("prokuIncentive",$arr2)){
	    	    	$row1=$row+2;
	    	    	$sheet->setCellValue("A".$row1,"position");
	    	    	$sheet->mergeCells("A".$row1.":"."A".($row1+1));
	    	    	$sheet->getStyle("A".$row1.":"."A".($row1+1))->applyFromArray($styleArray);

	    	 		$sheet->setCellValue("B".$row1,"incentive Scema");
	    	 		$sheet->mergeCells("B".$row1.":"."B".($row1+1));
	    	 		$sheet->getStyle("B".$row1.":"."B".($row1+1))->applyFromArray($styleArray);

	    	 		$sheet->setCellValue("c".$row1, "Labour");
	    	 		$sheet->mergeCells("c".$row1.":"."d".($row1));
	    	 		$sheet->getStyle("c".$row1.":"."d".($row1))->applyFromArray($styleArray);

	    	 		$sheet->setCellValue("e".$row1, "Sparepart");
	    	 		$sheet->mergeCells("E".$row1.":"."f".($row1));
	    	 		$sheet->getStyle("E".$row1.":"."f".($row1))->applyFromArray($styleArray);

	    	 		$sheet->setCellValue("g".$row1, "OLI");
	    	 		$sheet->mergeCells("G".$row1.":"."h".($row1));
	    	 		$sheet->getStyle("G".$row1.":"."h".($row1))->applyFromArray($styleArray);

	    	  		$sheet->setCellValue("c".($row1+1)," incentive");
	    	  		$sheet->getStyle("c".($row1+1))->applyFromArray($styleArray);

	    	  		$sheet->setCellValue("d".($row1+1)," paid");
	    	  		$sheet->getStyle("d".($row1+1))->applyFromArray($styleArray);

	    	  		$sheet->setCellValue("e".($row1+1)," incentive");
	    	  		$sheet->getStyle("e".($row1+1))->applyFromArray($styleArray);

	    	  		$sheet->setCellValue("f".($row1+1)," paid");
	    	  		$sheet->getStyle("f".($row1+1))->applyFromArray($styleArray);

	    	  		$sheet->setCellValue("g".($row1+1)," incentive");
	    	  		$sheet->getStyle("g".($row1+1))->applyFromArray($styleArray);

	    	  		$sheet->setCellValue("h".($row1+1)," paid");
	    	  		$sheet->getStyle("h".($row1+1))->applyFromArray($styleArray);

	    	  		 $sheet->getStyle("A".$row1.':'."h".($row1+1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('37E2D5');


	    	  		$row1=$row1+2;


	    	  		foreach($arr2["prokuIncentive"] as $lihat):
	    	    		$sheet->setCellValue("A".$row1,$lihat["position"]);
	    	    		$sheet->getStyle("A".$row1)->applyFromArray($styleArray);

	    	    		$sheet->setCellValue("B".$row1,$lihat["scema"]);
	    	    		$sheet->getStyle("b".$row1)->applyFromArray($styleArray);

	    	    		$sheet->setCellValue("C".$row1,$lihat["labour_incentive"]);
	    	    		$sheet->getStyle("c".$row1)->applyFromArray($styleArray);

	    	    		$sheet->setCellValue("D".$row1,$lihat["labour_paid"]);
	    	    		$sheet->getStyle("d".$row1)->applyFromArray($styleArray);

	    	    		$sheet->setCellValue("E".$row1,$lihat["sparepart_incentive"]);
	    	    		$sheet->getStyle("E".$row1)->applyFromArray($styleArray);

	    	    		$sheet->setCellValue("F".$row1,$lihat["sparepart_paid"]);
	    	    		$sheet->getStyle("F".$row1)->applyFromArray($styleArray);

	    	    		$sheet->setCellValue("G".$row1,$lihat["lubricant_incentive"]);
	    	    		$sheet->getStyle("G".$row1)->applyFromArray($styleArray);

	    	    		$sheet->setCellValue("H".$row1,$lihat["lubricant_paid"]);
	    	    		$sheet->getStyle("h".($row1+1))->applyFromArray($styleArray);

	    	    		$row1++;
	    	    	endforeach;
	    	    }


	    	    //pembuatan sheet chemical

	    	    $spreadsheet->createSheet();
				// Zero based, so set the second tab as active sheet
				$spreadsheet->setActiveSheetIndex(1);
				$spreadsheet->getActiveSheet()->setTitle('Rekap Chemical');
				$sheet1 = $spreadsheet->getActiveSheet();


				$sheet1 = $spreadsheet->getActiveSheet();

			    	 $sheet1->getColumnDimension('A')->setWidth(30);
		    	     $sheet1->getColumnDimension('B')->setWidth(30);
		    	     $sheet1->getColumnDimension('C')->setWidth(30);
		    	     $sheet1->getColumnDimension('D')->setWidth(30);
		    	     $sheet1->getColumnDimension('E')->setWidth(30);
		    	     $sheet1->getColumnDimension('F')->setWidth(30);
		    	      $sheet1->getColumnDimension('H')->setWidth(30);
		    	       $sheet1->getColumnDimension('I')->setWidth(30);
		    	    
			    	  $sheet1->setCellValue("A3","Employee Name");
			    	  $sheet1->getStyle("A3")->applyFromArray($styleArray);

			    	  $sheet1->setCellValue("B3","Part Name");
			    	  $sheet1->getStyle("B3")->applyFromArray($styleArray);

			    	  $sheet1->setCellValue("C3","Jumlah");
			    	  $sheet1->getStyle("c3")->applyFromArray($styleArray);

			    	  $sheet1->setCellValue("D3","Total");
			    	  $sheet1->getStyle("d3")->applyFromArray($styleArray);

			    	  $sheet1->setCellValue("E3","Total Margin");
			    	  $sheet1->getStyle("e3")->applyFromArray($styleArray);

			    	  $sheet1->setCellValue("F3","Persentase");
			    	  $sheet1->getStyle("f3")->applyFromArray($styleArray);

			    	  $sheet1->getStyle('A3:F3')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('37E2D5');


			    $rows=4;
			    $DESINFEKTANROWS=0;
				$ENGINECONDITIONERROWS=0;
				$ENGINEFLUSHROWS=0;

				$DESINFEKTANSTARTROWS=0;
				$ENGINECONDITIONERSTARTROWS=0;
				$ENGINEFLUSHSTARTROWS=0;

				$DESINFEKTAN15=0;
				$ENGINECONDITIONER15=0;
				$ENGINEFLUSH15=0;

				$DESINFEKTAN30=0;
				$ENGINECONDITIONER30=0;
				$ENGINEFLUSH30=0;



				if(array_key_exists("chemicalTotalMargin&Incentive",$arr2)){

						foreach($arr2["chemicalTotalMargin&Incentive"] as $lihat):
			    	    		$sheet1->setCellValue("A".$rows,$lihat["employeename"]);
			    	    		$sheet1->getStyle("A".$rows,$lihat)->applyFromArray($styleArray);

			    	    		$sheet1->setCellValue("B".$rows,$lihat["partname"]);
			    	    		$sheet1->getStyle("B".$rows,$lihat)->applyFromArray($styleArray);

			    	    		$sheet1->setCellValue("C".$rows,$lihat["jumlah"]);
			    	    		$sheet1->getStyle("C".$rows,$lihat)->applyFromArray($styleArray);

			    	    		$sheet1->setCellValue("D".$rows,$lihat["total"]);
			    	    		$sheet1->getStyle("D".$rows,$lihat)->applyFromArray($styleArray);

			    	    		$sheet1->setCellValue("E".$rows,$lihat["totalmargin"]);
			    	    		$sheet1->getStyle("E".$rows,$lihat)->applyFromArray($styleArray);

			    	    		$sheet1->setCellValue("F".$rows,$lihat["persentase"]);
			    	    		$sheet1->getStyle("F".$rows,$lihat)->applyFromArray($styleArray);

			    	    		    if($lihat["partname"]=="DESINFEKTAN"){
			    	    		    	$DESINFEKTANROWS++;
			    	    		    	$DESINFEKTANSTARTROWS=$rows;
			    	    		    	$DESINFEKTAN15=$lihat["totalmargin"]*15/100;
			    	    		    	$DESINFEKTAN30=$DESINFEKTAN15*30/100;
			    	    		    }else if($lihat["partname"]=="ENGINE CONDITIONER"){
			    	    		    	$ENGINECONDITIONERROWS++;
			    	    		    	$ENGINECONDITIONERSTARTROWS=$rows;
			    	    		    	$ENGINECONDITIONER15=$lihat["totalmargin"]*15/100;
										$ENGINECONDITIONER30=$ENGINECONDITIONER15*30/100;
				
			    	    		    }else if($lihat["partname"]=="ENGINE FLUSH"){
			    	    		    	$ENGINEFLUSHROWS++;
			    	    		    	$ENGINEFLUSHSTARTROWS=$rows;
			    	    		    	$ENGINEFLUSH15=$lihat["totalmargin"]*15/100;
										$ENGINEFLUSH30=$ENGINEFLUSH15*30/100;
			    	    		    }
			    	    		$rows++;
	    	    		endforeach;

	    	    		//print_r('DESINFEKTANROWS '.$DESINFEKTANROWS);
	    	    		//print_r('  DESINFEKTANSTARTROWS '.$DESINFEKTANSTARTROWS);

	    	    		if($DESINFEKTANSTARTROWS>0){

	    	    			 $sheet1->mergeCells("e".($DESINFEKTANSTARTROWS-($DESINFEKTANROWS-1)).':'."e".($DESINFEKTANSTARTROWS));

	    	    			   $sheet1->mergeCells("D".($DESINFEKTANSTARTROWS-($DESINFEKTANROWS-1)).':'."d".($DESINFEKTANSTARTROWS));
	    	    		}

	    	    		if($ENGINECONDITIONERSTARTROWS>0){
	    	    			  $sheet1->mergeCells("e".($ENGINECONDITIONERSTARTROWS-($ENGINECONDITIONERROWS-1)).':'."e".($ENGINECONDITIONERSTARTROWS));

	    	    			  $sheet1->mergeCells("d".($ENGINECONDITIONERSTARTROWS-($ENGINECONDITIONERROWS-1)).':'."d".($ENGINECONDITIONERSTARTROWS));
	    	    		}
	    	    		 
	    	    		if($ENGINEFLUSHSTARTROWS>0){
	    	    			   $sheet1->mergeCells("e".($ENGINEFLUSHSTARTROWS-($ENGINEFLUSHROWS-1)).':'."e".($ENGINEFLUSHSTARTROWS));

	    	    			    $sheet1->mergeCells("d".($ENGINEFLUSHSTARTROWS-($ENGINEFLUSHROWS-1)).':'."d".($ENGINEFLUSHSTARTROWS));
	    	    		}
	    	    		 

	    	    		 


	    	    		 

	    	    		 

	    	    		





	    	    		   $sheet1->setCellValue("H"."3","15%");
	    	    		   $sheet1->mergeCells("H3:I3");
	    	    		   $sheet1->getStyle("H3:I3")->applyFromArray($styleArray);
	    	    		   $sheet1->getStyle('H3:I3')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('37E2D5');

	    	    		   $sheet1->setCellValue("H"."4","DESINFEKTAN");
	    	    		    $sheet1->getStyle("H4")->applyFromArray($styleArray);

	    	    		   $sheet1->setCellValue("H"."5","ENGINE CONDITIONER");
	    	    		    $sheet1->getStyle("H5")->applyFromArray($styleArray);

	    	    		   $sheet1->setCellValue("H"."6","ENGINE FLUSH");
	    	    		    $sheet1->getStyle("H6")->applyFromArray($styleArray);

	    	    		   $sheet1->setCellValue("I"."4",$DESINFEKTAN15);
	    	    		   $sheet1->getStyle("i4")->applyFromArray($styleArray);

	    	    		   $sheet1->setCellValue("I"."5",$ENGINECONDITIONER15);
	    	    		   $sheet1->getStyle("i5")->applyFromArray($styleArray);

	    	    		   $sheet1->setCellValue("I"."6",$ENGINEFLUSH15);
	    	    		   $sheet1->getStyle("i6")->applyFromArray($styleArray);


	    	    		   $sheet1->setCellValue("H"."8","30%");
	    	    		   $sheet1->mergeCells("H8:I8");
	    	    		   $sheet1->getStyle("H8:I8")->applyFromArray($styleArray);

	    	    		   $sheet1->setCellValue("H"."9","DESINFEKTAN");
	    	    		   $sheet1->getStyle("H9")->applyFromArray($styleArray);

	    	    		   $sheet1->setCellValue("H"."10","ENGINE CONDITIONER");
	    	    		   $sheet1->getStyle("H10")->applyFromArray($styleArray);

	    	    		   $sheet1->setCellValue("H"."11","ENGINE FLUSH");
	    	    		   $sheet1->getStyle("H11")->applyFromArray($styleArray);

	    	    		   $sheet1->setCellValue("i"."9",$DESINFEKTAN30);
	    	    		    $sheet1->getStyle("i9")->applyFromArray($styleArray);

	    	    		   $sheet1->setCellValue("i"."10",$ENGINECONDITIONER30);
	    	    		    $sheet1->getStyle("i10")->applyFromArray($styleArray);

	    	    		   $sheet1->setCellValue("i"."11",$ENGINEFLUSH30);
	    	    		    $sheet1->getStyle("i11")->applyFromArray($styleArray);


				}

				$spreadsheet->createSheet();
				// Zero based, so set the second tab as active sheet
				$spreadsheet->setActiveSheetIndex(2);
				$spreadsheet->getActiveSheet()->setTitle('Rekap S&B');
				$sheet2 = $spreadsheet->getActiveSheet();



				     $sheet2->getColumnDimension('A')->setWidth(30);
		    	     $sheet2->getColumnDimension('B')->setWidth(30);
		    	     $sheet2->getColumnDimension('C')->setWidth(30);
		    	     $sheet2->getColumnDimension('D')->setWidth(30);
		    	     $sheet2->getColumnDimension('E')->setWidth(30);

		    	     $sheet2->setCellValue("A3","Total Iinvoice");
			    	  $sheet2->getStyle("A3")->applyFromArray($styleArray);

			    	  $sheet2->setCellValue("B3","Total Margin");
			    	  $sheet2->getStyle("B3")->applyFromArray($styleArray);

			    	  $sheet2->setCellValue("C3","Target");
			    	  $sheet2->getStyle("c3")->applyFromArray($styleArray);

			    	  $sheet2->setCellValue("D3","Pesentase");
			    	  $sheet2->getStyle("d3")->applyFromArray($styleArray);

			    	  $sheet2->setCellValue("E3","Total Incentive");
			    	  $sheet2->getStyle("e3")->applyFromArray($styleArray);


			    	  $sheet2->getStyle('a3:e3')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('37E2D5');


			    	  
			    	  


			      $rowsSb=4;
			      $incentiveSpooringReport=0;

		    	  if(array_key_exists("spooringTotalMargin&Incentive",$arr2)){

		    	  			foreach($arr2["spooringTotalMargin&Incentive"] as $lihat):
		    	  					$sheet2->setCellValue("A".$rowsSb,$lihat["Total Invoice"]);
		    	  					$sheet2->getStyle("A".$rowsSb)->applyFromArray($styleArray);

		    	  					$sheet2->setCellValue("B".$rowsSb,$lihat["totalmargin"]);
		    	  					$sheet2->getStyle("B".$rowsSb)->applyFromArray($styleArray);

		    	  					$sheet2->setCellValue("c".$rowsSb,100);
		    	  					$sheet2->getStyle("c".$rowsSb)->applyFromArray($styleArray);


		    	  					if(round($lihat["Total Invoice"]/100*100)<=100){
    										$incentiveSpooringReport=0;
    								}else if((round($lihat["Total Invoice"]/100*100)>100) && (round($lihat["Total Invoice"]/100*100)<=150)){
    											$incentiveSpooringReport=10;
    			  				    }else if(round($lihat["Total Invoice"]/100*100)>150){
    										$incentiveSpooringReport=15;
    			 					}


		    	  					$sheet2->setCellValue("D".$rowsSb,$incentiveSpooringReport);
		    	  					$sheet2->getStyle("D".$rowsSb)->applyFromArray($styleArray);

		    	  					$sheet2->setCellValue("E".$rowsSb,$lihat["totalmargin"]*$incentiveSpooringReport/100);
		    	  					$sheet2->getStyle("E".$rowsSb)->applyFromArray($styleArray);



		    	  					$rowsSb++;
		    	  			endforeach;
		    	  }
		    	    



	    	    $cabang1="";
			if($cabang=="641940101"){
					$cabang1="Ciawi";
			} else if($cabang=="641940102"){
					$cabang1="Cianjur";
			} else if($cabang=="641940103"){
					$cabang1="Cinere";
			} else if($cabang=="641940104"){
					$cabang1="Jatiasih";
			}

			$bulan1="";
			if($bulan=="1"){
					$bulan1="Jan";
			}else if($bulan=="2"){
					$bulan1="Feb";
			}else if($bulan=="3"){
					$bulan1="Mar";
			}else if($bulan=="4"){
					$bulan1="Apr";
			}else if($bulan=="5"){
					$bulan1="Mei";
			}else if($bulan=="6"){
					$bulan1="Jun";
			}else if($bulan=="7"){
					$bulan1="Jul";
			}else if($bulan=="8"){
					$bulan1="Agu";
			}else if($bulan=="9"){
					$bulan1="Sep";
			}else if($bulan=="10"){
					$bulan1="Okt";
			}else if($bulan=="11"){
					$bulan1="Nov";
			}else if($bulan=="12"){
					$bulan1="Des";
			}

			//$writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
			//$writer->save('output.xlsx');

			$writer = new Xlsx($spreadsheet);

			$filename = 'Proku Incentive'.$cabang1.$bulan1.$tahun;
			ob_end_clean();
			//header('Content-Type: application/vnd.ms-excel');
			 header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"'); 
			header('Cache-Control: max-age=0');

			$writer = new Xlsx($spreadsheet);

			//$filename = 'LisdataBookingsro';
	
	
			$writer->save('php://output');
			

		}


		//cetak PDF untuk rekap user lain isi nya rekap incentive, rekap incentive chem, rekap inctive spooring
		function cetakPdfUserLain($bulan,$tahun,$cabang,$idpengajuan){

			$pdf = new FPDF('l','mm', "A4");
	    	 //$pdf = new FPDF("landscape","A4");
	    	
	        // membuat halaman baru
	        $pdf->AddPage();
			$pdf->SetMargins(8, 25, 1,1);
	    	$pdf->SetFont('Arial','B',11);

	    	$arrPDFChem["chemicalTotalMargin&Incentive"]=$this->mIncentiveService->getfullIncentiveChemical($cabang,$tahun,$bulan);


	    	$pdf->Image(base_url()."image/tandatangan/suzuki.png" ,10,-5,30,20,'png','');
             $pdf->Ln(5);
             	$pdf->Cell(22,6,'Authorized ',0,0,'l');
                    $pdf->SetTextColor(0,0,255);
                    	$pdf->Cell(14,6,'Suzuki ',0,0,'l');
                    		$pdf->SetTextColor(0,0,0);
                    			$pdf->Cell(30,6,'Dealer ',0,1,'l');
                    				 $pdf->Ln(5);

                    		$pdf->SetFont('Arial','B',10);
                    		if($cabang=="641940101"){
                    				$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Ciawi ",0,1,'L');
                    		}else if($cabang=="641940102"){
                    			$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Cianjur ",0,1,'L');
                    		}else if($cabang=="641940103"){
                    			$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Cinere ",0,1,'L');
                    		}else if($cabang=="641940104"){
                    			$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."JatiAsih ",0,1,'L');
                    		}

                    		$pdf->Ln(5);

                    		$pdf->cell(300,6,"Rekap incentive Chemical",0,1,'L');
                    		$pdf->Ln(2);

                    		$pdf->SetFont('Arial','',8);
							$pdf->SetFillColor(132, 188, 220);
							$pdf->Cell(30,6,'Employee Name',1,0,'C',1);;
							$pdf->Cell(30,6,'Part Name',1,0,'C',1);
							$pdf->Cell(20,6,'jumlah',1,0,'C',1);
							$pdf->Cell(20,6,'Total',1,0,'C',1);
							$pdf->Cell(20,6,'Total Margin',1,0,'C',1);
							$pdf->Cell(20,6,'Persentase',1,1,'C',1);
							
							$engineflush15=0;
							$enginecondtion15=0;
							$desinfektan15=0;	

							$engineflush30=0;
							$enginecondtion30=0;
							$desinfektan30=0;	




                    		if(array_key_exists("chemicalTotalMargin&Incentive",$arrPDFChem)){

                    		  foreach ($arrPDFChem["chemicalTotalMargin&Incentive"] as $lihat):
                    			$pdf->Cell(30,6,$lihat["employeename"],1,0,'L');
                    			$pdf->Cell(30,6,$lihat["partname"],1,0,'L');
                    			$pdf->Cell(20,6,$lihat["jumlah"],1,0,'L');
                    			$pdf->Cell(20,6,$lihat["total"],1,0,'L');
                    			$pdf->Cell(20,6,$lihat["totalmargin"],1,0,'L');
                    			$pdf->Cell(20,6,$lihat["persentase"],1,1,'L');
                    					if($lihat["partname"]=='DESINFEKTAN'){
													$desinfektan15=$lihat["totalmargin"]*15/100;
													$desinfektan30=$desinfektan15*30/100;
                    					}else if($lihat["partname"]=='ENGINE CONDITIONER'){
                    								$enginecondtion15 =$lihat["totalmargin"]*15/100;
													$enginecondtion30 =$enginecondtion15*30/100;
                    					}else if($lihat["partname"]=='ENGINE FLUSH'){
                    								$engineflush15 =$lihat["totalmargin"]*15/100;
													$engineflush30 =$engineflush15*30/100;
                    					}
			    	    	  endforeach;



			    	    		

                    		}//end if array key exist chemicalTotalMargin&Incentive


                    		$pdf->Ln(2);	
                    		$pdf->Cell(70,6,"15%",1,0,'C',1);
                    		$pdf->cell(10);
                    		$pdf->Cell(70,6,"30%",1,1,'C',1);

                    		$pdf->Cell(35,6,'DESINFEKTAN',1,0,'L');
                    		$pdf->Cell(35,6,$desinfektan15,1,0,'L');
                    		$pdf->cell(10);
                    		$pdf->Cell(35,6,'DESINFEKTAN',1,0,'L');
                    		$pdf->Cell(35,6,$desinfektan30,1,1,'L');

                    		$pdf->Cell(35,6,'ENGINE CONDITIONER',1,0,'L');
                    		$pdf->Cell(35,6,$enginecondtion15,1,0,'L');
                    		$pdf->cell(10);
                    		$pdf->Cell(35,6,'ENGINE CONDITIONER',1,0,'L');
                    		$pdf->Cell(35,6,$enginecondtion30,1,1,'L');

                    		$pdf->Cell(35,6,'ENGINE FLUSH',1,0,'L');
                    		$pdf->Cell(35,6,$engineflush15,1,0,'L');
                    		$pdf->cell(10);
                    		$pdf->Cell(35,6,'ENGINE FLUSH',1,0,'L');
                    		$pdf->Cell(35,6,$engineflush30,1,1,'L');


                    		







//laporan rekap spooring balancing

            $pdf->AddPage();
			$pdf->SetMargins(8, 25, 1,1);
	    	$pdf->SetFont('Arial','B',11);

	    	

	    	$arrPDFChem["spooringTotalMargin&Incentive"]=$this->mIncentiveService->getFullIncentiveSpooring($cabang, $tahun, $bulan);


	    	$pdf->Image(base_url()."image/tandatangan/suzuki.png" ,10,-5,30,20,'png','');
             $pdf->Ln(5);
             	$pdf->Cell(22,6,'Authorized ',0,0,'l');
                    $pdf->SetTextColor(0,0,255);
                    	$pdf->Cell(14,6,'Suzuki ',0,0,'l');
                    		$pdf->SetTextColor(0,0,0);
                    			$pdf->Cell(30,6,'Dealer ',0,1,'l');
                    				 $pdf->Ln(5);

                    		$pdf->SetFont('Arial','B',10);
                    		if($cabang=="641940101"){
                    				$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Ciawi ",0,1,'L');
                    		}else if($cabang=="641940102"){
                    			$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Cianjur ",0,1,'L');
                    		}else if($cabang=="641940103"){
                    			$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Cinere ",0,1,'L');
                    		}else if($cabang=="641940104"){
                    			$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."JatiAsih ",0,1,'L');
                    		}

                    		$pdf->Ln(5);

                    		$pdf->cell(300,6,"Rekap incentive Spooring & Balancing",0,1,'L');
                    		$pdf->Ln(2);

                    		$pdf->SetFont('Arial','',8);
							$pdf->SetFillColor(132, 188, 220);
							$pdf->Cell(30,6,'Total Iinvoice',1,0,'C',1);;
							$pdf->Cell(30,6,'Total Margin',1,0,'C',1);
							$pdf->Cell(15,6,'Target',1,0,'C',1);
							$pdf->Cell(20,6,'Persentase',1,0,'C',1);
							$pdf->Cell(30,6,'Total Incentive',1,1,'C',1);
							


							



							if(array_key_exists("spooringTotalMargin&Incentive",$arrPDFChem)){

                    		  foreach ($arrPDFChem["spooringTotalMargin&Incentive"] as $lihat):
                    			$pdf->Cell(30,6,$lihat["Total Invoice"],1,0,'L');
                    			$pdf->Cell(30,6,$lihat["totalmargin"],1,0,'L');
                    			$pdf->Cell(15,6,'100',1,0,'L');

                    			if(round($lihat["Total Invoice"]/100*100)<=100){
    										$incentiveSpooringReport=0;
    								}else if((round($lihat["Total Invoice"]/100*100)>100) && (round($lihat["Total Invoice"]/100*100)<=150)){
    											$incentiveSpooringReport=10;
    			  				    }else if(round($lihat["Total Invoice"]/100*100)>150){
    										$incentiveSpooringReport=15;
    			 					}

                    			$pdf->Cell(20,6,$incentiveSpooringReport,1,0,'L');
                    			$pdf->Cell(30,6,($lihat["totalmargin"]*$incentiveSpooringReport/100),1,1,'L');
                    		
			    	    	  endforeach;
			    	    		

                    		//}//end if array key exist spooringTotalMargin&Incentive





//cetak pdf induk proku

            $pdf->AddPage();
			$pdf->SetMargins(8, 25, 1,1);
	    	$pdf->SetFont('Arial','B',11);
	    	$pdf->SetFillColor(132, 188, 220);

	    	//$arrPDFChem["chemicalTotalMargin&Incentive"]=$this->mIncentiveService->getfullIncentiveChemical($cabang,$tahun,$bulan);



	    	$arrPDFChem["indukProku"]=$this->indukProku($cabang, $tahun, $bulan);
    		$arrPDFChem["rekapIncentive"]= $this->rekapIncentive($cabang, $tahun, $bulan);



	    	$pdf->Image(base_url()."image/tandatangan/suzuki.png" ,10,-5,30,20,'png','');
             $pdf->Ln(5);
             	$pdf->Cell(22,6,'Authorized ',0,0,'l');
                    $pdf->SetTextColor(0,0,255);
                    	$pdf->Cell(14,6,'Suzuki ',0,0,'l');
                    		$pdf->SetTextColor(0,0,0);
                    			$pdf->Cell(30,6,'Dealer ',0,1,'l');
                    				 $pdf->Ln(5);

                    		$pdf->SetFont('Arial','B',10);
                    		if($cabang=="641940101"){
                    				$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Ciawi ",0,1,'L');
                    		}else if($cabang=="641940102"){
                    			$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Cianjur ",0,1,'L');
                    		}else if($cabang=="641940103"){
                    			$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Cinere ",0,1,'L');
                    		}else if($cabang=="641940104"){
                    			$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."JatiAsih ",0,1,'L');
                    		}

                    		$pdf->Ln(5);


                    		$pdf->cell(300,6,"Rekap incentive",0,1,'L');
                    		$pdf->Ln(2);


                    		  
//cetak incentive proku


					    	// $pdf->Cell(45,12,"position",1,0,'L');
			    	 	// 	$pdf->Cell(30,12,"incentive Scema",1,0,'L');
			    	 	// 	$pdf->Cell(60,6, "Labour",1,0,'L');
			    	 	// 	$pdf->Cell(60,6, "Sparepart",1,0,'L');
			    	 	// 	$pdf->Cell(60,6, "OLI",1,1,'L');
			    	 	// 	$pdf->cell(75);
			    	  // 		$pdf->Cell(30,6, "incentive",1,0,'L');
			    	  // 		$pdf->Cell(30,6," paid",1,0,'L');
			    	  // 		$pdf->Cell(30,6," incentive",1,0,'L');
			    	  // 		$pdf->Cell(30,6," paid",1,0,'L');
			    	  // 		$pdf->Cell(30,6," incentive",1,0,'L');
			    	  // 		$pdf->Cell(30,6," paid",1,1,'L');
$pdf->SetFont('Arial','',8);


					    	 	$pdf->cell(50,18,"Nama Karyawan",1,0,"C",1);

								$pdf->cell(40,18, "Jabatan",1,0,"C",1);
								$pdf->cell(15,18, "Scema",1,0,"C",1);
								$pdf->cell(175,6, "Incentive",1,1,"C",1);
								$pdf->cell(105);
								$pdf->cell(30,6, "Jasa Service",1,0,"C",1);
								$pdf->cell(30,6, "Sparepart",1,0,"C",1);
								$pdf->cell(30,6, "Oli",1,0,"C",1);
								$pdf->cell(15,12, "Scema",1,0,"C",1);
								$pdf->cell(30,6, "S&B",1,0,"C",1);
								$pdf->cell(20,12, "Chemical",1,0,"C",1);
								$pdf->cell(20,12, "total",1,0,"C",1);
								$pdf->cell(0,6,'' ,0,1,"C");
								$pdf->cell(105);
								$pdf->cell(15,6, "%",1,0,"C",1);
								$pdf->cell(15,6, "Rp.",1,0,"C",1);
								$pdf->cell(15,6, "%",1,0,"C",1);
								$pdf->cell(15,6, "Rp.",1,0,"C",1);
								$pdf->cell(15,6, "%",1,0,"C",1);
								$pdf->cell(15,6, "Rp.",1,0,"C",1);
								$pdf->cell(15);
								$pdf->cell(15,6, "%",1,0,"C",1);
								$pdf->cell(15,6, "Rp.",1,1,"C",1);
								


							


			    	  		foreach($arrPDFChem["rekapIncentive"] as $lihat):
			    	    		$pdf->Cell(50,6,$lihat["nama"],1,0,'L');
			    	    		$pdf->Cell(40,6,$lihat["position"],1,0,'L');
			    	    		$pdf->Cell(15,6,$lihat["scema"],1,0,'L');
			    	    		if($lihat["%MSI"]<>100){
			    	    				$pdf->Cell(15,6,$lihat["%MSI"],1,0,'L');
			    	    		}else{
			    	    				$pdf->Cell(15,6,'',1,0,'L');
			    	    		}
			    	    		
			    	    		$pdf->Cell(15,6,$lihat["incLabour"],1,0,'L');
			    	    		$pdf->Cell(15,6,'',1,0,'L');
			    	    		$pdf->Cell(15,6,$lihat["incPart"],1,0,'L');
			    	    		$pdf->Cell(15,6,'',1,0,'L');
			    	    		$pdf->Cell(15,6,$lihat["incOli"],1,0,'L');
			    	    		
			    	    		if($lihat["scemaSB"]<>0){
			    	    				$pdf->Cell(15,6,$lihat["scemaSB"],1,0,'L');
			    	    		}else{
			    	    				$pdf->Cell(15,6,'',1,0,'L');
			    	    		}

			    	    		if($lihat["%sb"]<>100){
			    	    			$pdf->Cell(15,6,$lihat["%sb"],1,0,'L');
			    	    		}else{
			    	    			$pdf->Cell(15,6,'',1,0,'L');
			    	    		}
			    	    		
			    	    		$pdf->Cell(15,6,$lihat["incSB"],1,0,'L');
			    	    		$pdf->Cell(20,6,$lihat["incChemcial"],1,0,'L');
			    	    		$pdf->Cell(20,6,$lihat["total"],1,1,'L');
			    	    		

			    	    	endforeach;
			    	  		




			    	 $pdf->Output('D','Incentive Service '.'.pdf', true);


		}//end cetak PDF user lain
	
		

}
}
    