<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cdetail_incentive_sro_report extends CI_Controller {

	    public function __construct() {
        parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		 $this->load->library('pdf');
        $this->load->model('report/mdetail_incentive_sro_report');
    }


    	public function cetakpdf($bulan,$tahun,$cabang,$idpengajuan){
	    	 // $arr = array();
    		 // $bulan="12";
    		 // $tahun="2021";
    		 // $cabang="641940102";

    		//pembuatan constanta untuk insentive
    		   define("jmlIncentive",2000000);
    		   define("persentasiCon1",60);
    		   define("persentasiCon2",50);
    			
	    	 $arr =$this->mdetail_incentive_sro_report->getcreatedBy($bulan,$tahun,$cabang,$idpengajuan);

	    	  $pdf = new FPDF('l','mm', "A4");
	    	 //$pdf = new FPDF("landscape","A4");
	    	
	        // membuat halaman baru
	        $pdf->AddPage();
			$pdf->SetMargins(8, 25, 1,1);
	    	$pdf->SetFont('Arial','B',11);
	    	//$pdf->setAutoPageBreak(true, 5);
	        // mencetak string 

	    	if(isset($arr)){
                    if ((array_key_exists("nama_cabang",$arr)) && (array_key_exists("select-bulan",$arr)) && (array_key_exists("select-tahun",$arr))) { 



						    


                    				//$pdf->cell(25,20,$pdf->Image(base_url()."image/tandatangan/suzuki.png",  $pdf->GetX(), $pdf->GetY(),30),0,1,'C');
                    				$pdf->Image(base_url()."image/tandatangan/suzuki.png" ,10,-5,30,20,'png','');

                    				$pdf->Ln(5);

                    				$pdf->Cell(22,6,'Authorized ',0,0,'l');
                    				$pdf->SetTextColor(0,0,255);
                    				$pdf->Cell(14,6,'Suzuki ',0,0,'l');
                    				$pdf->SetTextColor(0,0,0);
                    				$pdf->Cell(30,6,'Dealer ',0,1,'l');

                    				 $pdf->Ln(5);


                    				$pdf->SetFont('Arial','B',10);
                    				if($arr["nama_cabang"]=="641940101"){
                    					$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Ciawi ",0,1,'L');
                    				}else if($arr["nama_cabang"]=="641940102"){
                    					$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Cianjur ",0,1,'L');
                    				}else if($arr["nama_cabang"]=="641940103"){
                    					$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."Cinere ",0,1,'L');
                    				}else if($arr["nama_cabang"]=="641940104"){
                    					$pdf->Cell(320,6,'PT Duta Cendana Adimandiri - '."JatiAsih ",0,1,'L');
                    				}

                    				//$pdf->Cell(320,5,'Bulan : '.$arr["select-bulan"],0,1,'C');
                    				//$pdf->Cell(320,5,'Tahun : '.$arr["select-tahun"],0,1,'C');

                    				
							
					}else{
							//$pdf->Cell(70,10,'SRO : ',0,1,'L'); 
					}
			}else{
							//$pdf->Cell(70,10,'SRO : ',0,1,'L'); 
			}





	        	
				//$pdf->Cell(25,4,'',0,1);

			$pdf->SetFont('Arial','',8);

			if(isset($arr)){
                    if (array_key_exists("bookingsro",$arr)){ 
							//$pdf->Cell(70,10,'SRO : ' .$arr["bookingsro"][0]["fullname"],0,1,'L'); 
                    		$pdf->SetFont('Arial','B',10);
							if($arr["bookingsro"][0]["bulan"]==1){
										$pdf->Cell(40,6, "Bulan :Jan ".$arr["select-tahun"] ,0,1,'L');
									}else if($arr["bookingsro"][0]["bulan"]==2){
										$pdf->Cell(40,6, "Bulan :Feb ".$arr["select-tahun"],0,1,'L');
									}else if($arr["bookingsro"][0]["bulan"]==3){
										$pdf->Cell(40,6, "Bulan :Mar ".$arr["select-tahun"],0,1,'L');
									}else if($arr["bookingsro"][0]["bulan"]==4){
										$pdf->Cell(40,6, "Bulan :Apr ".$arr["select-tahun"],0,1,'L');
									}else if($arr["bookingsro"][0]["bulan"]==5){
										$pdf->Cell(40,6, "Bulan :Mei ".$arr["select-tahun"],0,1,'L');
									}else if($arr["bookingsro"][0]["bulan"]==6){
										$pdf->Cell(40,6, "Bulan :Jun ".$arr["select-tahun"],0,1,'L');
									}else if($arr["bookingsro"][0]["bulan"]==7){
										$pdf->Cell(40,6, "Bulan :Jul ".$arr["select-tahun"],0,1,'L');
									}else if($arr["bookingsro"][0]["bulan"]==8){
										$pdf->Cell(40,6, "Bulan :Agu ".$arr["select-tahun"],0,1,'L');
									}else if($arr["bookingsro"][0]["bulan"]==9){
										$pdf->Cell(40,6, "Bulan :Sep ".$arr["select-tahun"],0,1,'L');
									}else if($arr["bookingsro"][0]["bulan"]==10){
										$pdf->Cell(40,6, "Bulan :Okt ".$arr["select-tahun"],0,1,'L');
									}else if($arr["bookingsro"][0]["bulan"]==11){
										$pdf->Cell(40,6, "Bulan :Nov ".$arr["select-tahun"],0,1,'L');
									}else if($arr["bookingsro"][0]["bulan"]==12){
										$pdf->Cell(40,6, "Bulan :Des ".$arr["select-tahun"],0,1,'L');
									}
					}else{
							//$pdf->Cell(70,10,'SRO : ',0,1,'L'); 
					}
			}else{
							//$pdf->Cell(70,10,'SRO : ',0,1,'L'); 
			}

			$pdf->SetFont('Arial','',8);
			$pdf->SetFillColor(132, 188, 220);
			$pdf->Cell(10,6,'No',1,0,'C',1);;
			$pdf->Cell(30,6,'Nama',1,0,'C',1);
			$pdf->Cell(15,6,'Bulan',1,0,'C',1);
			$pdf->Cell(25,6,'Booking To SPK',1,0,'C',1);
			$pdf->Cell(20,6,'Total Booking',1,0,'C',1);
			$pdf->Cell(25,6,'Target Unit Entry',1,0,'C',1);
			$pdf->Cell(20,6,'Persentasi(%)',1,0,'C',1);
			$pdf->Cell(20,6,'Incentive',1,1,'C',1);
			//$pdf->Cell(40,6,'Invoice Date',1,0,'C',1);
			//$pdf->Cell(30,6,'Status',1,1,'C',1);
			if(isset($arr)){
                    if (array_key_exists("bookingsro",$arr)){ 
                         $no = 1;
                         	foreach ($arr["bookingsro"] as $lihat):
                         		 
                         		 	$pdf->SetFillColor(255,255, 255);
                         		 	$pdf->Cell(10,6, $no++,1,0,'C',1);
									$pdf->Cell(30,6, $lihat["fullname"],1,0,'C',1);
									if($lihat["bulan"]==1){
										$pdf->Cell(15,6, "Jan" ,1,0,'C',1);
									}else if($lihat["bulan"]==2){
										$pdf->Cell(15,6, "Feb",1,0,'C',1);
									}else if($lihat["bulan"]==3){
										$pdf->Cell(15,6, "Mar",1,0,'C',1);
									}else if($lihat["bulan"]==4){
										$pdf->Cell(15,6, "Apr",1,0,'C',1);
									}else if($lihat["bulan"]==5){
										$pdf->Cell(15,6, "Mei",1,0,'C',1);
									}else if($lihat["bulan"]==6){
										$pdf->Cell(15,6, "Jun",1,0,'C',1);
									}else if($lihat["bulan"]==7){
										$pdf->Cell(15,6, "Jul",1,0,'C',1);
									}else if($lihat["bulan"]==8){
										$pdf->Cell(15,6, "Agu",1,0,'C',1);
									}else if($lihat["bulan"]==9){
										$pdf->Cell(15,6, "Sep",1,0,'C',1);
									}else if($lihat["bulan"]==10){
										$pdf->Cell(15,6, "Okt",1,0,'C',1);
									}else if($lihat["bulan"]==11){
										$pdf->Cell(15,6, "Nov",1,0,'C',1);
									}else if($lihat["bulan"]==12){
										$pdf->Cell(15,6, "Des",1,0,'C',1);
									}
									$pdf->Cell(25,6, $lihat["total"],1,0,'C',1);
									$pdf->Cell(20,6,$arr["totalbooking"][0]["total"],1,0,'C',1);
									   if($lihat["Rank"]==1){
									   		//$pdf->Cell(20,6,$arr["bookingsroSUM"][0]["total"],1,0,'C',1);
									   	 $pdf->Cell(25,6,$arr["targetunitentrySum"][0]["jumlah"],1,0,'C',1);
									   	 $pdf->Cell(20,6,ceil(($arr["bookingsroSUM"][0]['total']*100)/$arr["targetunitentrySum"][0]['jumlah']),1,0,'C',1);
									   }else{
									   	    //$pdf->Cell(20,6,$arr["bookingsroSUM"][0]["total"],1,0,'C',1);
									   	    $pdf->Cell(25,6,"",1,0,'C',1);
									   	    $pdf->Cell(20,6,"",1,0,'C',1);
									   }
									   // $pdf->Cell(20,6,$arr["targetunitentrySum"][0]["jumlah"],1,0,'C',1);
									    $persentasi=ceil(($arr["bookingsroSUM"][0]['total']*100)/$arr["targetunitentrySum"][0]['jumlah']);
									    //$pdf->Cell(20,6,$persentasi,1,0,'C',1);
									    if(($arr["nama_cabang"]=="641940101") || ($arr["nama_cabang"]=="641940103")){
									    			if($persentasi>=persentasiCon1){
									    	 				$pdf->Cell(20,6,$persentasi*jmlIncentive/100,1,1,'C',1);
									   				}else{
									    					$pdf->Cell(20,6,"0",1,1,'C',1);
									    			}

									    }else if($arr["nama_cabang"]=="641940102"){
									    			if($persentasi>=persentasiCon2){
									    	 				$pdf->Cell(20,6,$persentasi*jmlIncentive/100,1,1,'C',1);
									    			}else{
									    					$pdf->Cell(20,6,"0",1,1,'C',1);
									   				}

									    }
									    
									   
									// $pdf->Cell(20,6, $lihat["bookingdate"],1,0,'C',1);
									// $pdf->Cell(20,6, $lihat["SPK_date"],1,0,'C',1);
									// $pdf->Cell(20,6, $lihat["PoliceRegNo"],1,0,'C',1);
									// $pdf->Cell(30,6, $lihat["SPKNo"],1,0,'C',1);
									// $pdf->Cell(30,6, $lihat["invoiceno"],1,0,'C',1);
									// $pdf->Cell(40,6, $lihat["invoicedate"],1,0,'C',1);
									// $pdf->Cell(30,6, $lihat["status"],1,1,'C',1);
                         		

                         	endforeach;
                    	//$pdf->Cell(30,6,'data nya ada',2,1,'C');

                    }

            }else{
            	
            }

   //          $pdf->Cell(25,4,'',0,1);

   //          if(isset($arr)){
   //                  if (array_key_exists("bookingsro2",$arr)){ 
			// 				$pdf->Cell(70,10,'SRO : ' .$arr["bookingsro2"][0]["fullname"],0,1,'L'); 
			// 		}else{
			// 				$pdf->Cell(70,10,'SRO : tidak ada ',0,1,'L'); 
			// 		} 
			// }else{
			// 				$pdf->Cell(70,10,'SRO : tidak ada ',0,1,'L'); 
			// }

			// $pdf->SetFillColor(132, 188, 220);
			// $pdf->Cell(10,6,'No',1,0,'C',1);;
			// $pdf->Cell(40,6,'Created Date',1,0,'C',1);
			// $pdf->Cell(40,6,'Created Date SPK Booking',1,0,'C',1);
			// $pdf->Cell(20,6,'Booking Date',1,0,'C',1);
			// $pdf->Cell(20,6,'SPK Date',1,0,'C',1);
			// $pdf->Cell(20,6,'Police Reg No',1,0,'C',1);
			// $pdf->Cell(30,6,'SPK No',1,0,'C',1);
			// $pdf->Cell(30,6,'Invoice No',1,0,'C',1);
			// $pdf->Cell(40,6,'Invoice Date',1,0,'C',1);
			// $pdf->Cell(30,6,'Status',1,1,'C',1);
			// if(isset($arr)){
   //                  if (array_key_exists("bookingsro2",$arr)){ 
   //                       $no = 1;
   //                       	foreach ($arr["bookingsro2"] as $lihat):

   //                       		 if($lihat["status"]=="counted"){
   //                       		 	$pdf->SetFillColor(255, 0, 0);
   //                       		 	$pdf->Cell(10,6, $no++,1,0,'C',1);;
			// 						$pdf->Cell(40,6, $lihat["createddate"],1,0,'C',1);
			// 						$pdf->Cell(40,6, $lihat["createddate_SPK_Booking"],1,0,'C',1);
			// 						$pdf->Cell(20,6, $lihat["bookingdate"],1,0,'C',1);
			// 						$pdf->Cell(20,6, $lihat["SPK_date"],1,0,'C',1);
			// 						$pdf->Cell(20,6, $lihat["PoliceRegNo"],1,0,'C',1);
			// 						$pdf->Cell(30,6, $lihat["SPKNo"],1,0,'C',1);
			// 						$pdf->Cell(30,6, $lihat["invoiceno"],1,0,'C',1);
			// 						$pdf->Cell(40,6, $lihat["invoicedate"],1,0,'C',1);
			// 						$pdf->Cell(30,6, $lihat["status"],1,1,'C',1);
   //                       		 }else{
   //                       		 	$pdf->Cell(10,6, $no++,1,0,'C');;
			// 						$pdf->Cell(40,6, $lihat["createddate"],1,0,'C');
			// 						$pdf->Cell(40,6, $lihat["createddate_SPK_Booking"],1,0,'C');
			// 						$pdf->Cell(20,6, $lihat["bookingdate"],1,0,'C');
			// 						$pdf->Cell(20,6, $lihat["SPK_date"],1,0,'C');
			// 						$pdf->Cell(20,6, $lihat["PoliceRegNo"],1,0,'C');
			// 						$pdf->Cell(30,6, $lihat["SPKNo"],1,0,'C');
			// 						$pdf->Cell(30,6, $lihat["invoiceno"],1,0,'C');
			// 						$pdf->Cell(40,6, $lihat["invoicedate"],1,0,'C');
			// 						$pdf->Cell(30,6, $lihat["status"],1,1,'C');
   //                       		 }
                         			
									

   //                       	endforeach;
   //                  	//$pdf->Cell(30,6,'data nya ada',2,1,'C');

   //                  }

   //          }else{
            	
   //          }


            $pdf->Cell(25,4,'',0,1);
            $pdf->SetFont('Arial','B',10);

            if(isset($arr)){
                     
							$pdf->Cell(70,10,"History Incentive SRO Bulanan",0,1,'L'); 
					
			
			}

			$pdf->SetFont('Arial','',8);
			$pdf->SetFillColor(132, 188, 220);
			$pdf->Cell(10,6,'No',1,0,'C',1);;
			$pdf->Cell(30,6,'Nama',1,0,'C',1);
			$pdf->Cell(15,6,'Bulan',1,0,'C',1);
			$pdf->Cell(25,6,'Booking To SPK',1,0,'C',1);
			$pdf->Cell(20,6,'Total Booking',1,0,'C',1);
			$pdf->Cell(25,6,'Target Unit Entry',1,0,'C',1);
			$pdf->Cell(20,6,'Persentasi(%)',1,0,'C',1);
			$pdf->Cell(20,6,'Incentive',1,1,'C',1);
			//$pdf->Cell(40,6,'Invoice Date',1,0,'C',1);
			//$pdf->Cell(30,6,'Status',1,1,'C',1);
			if(isset($arr)){
                    if (array_key_exists("bookingsroRange",$arr)){ 
                         $no = 1;
                         $index=0; 
                         	foreach ($arr["bookingsroRange"] as $lihat):
                         		
                         		 	$pdf->SetFillColor(255,255, 255);
                         		 	$pdf->Cell(10,6, $no++,1,0,'C',1);
									$pdf->Cell(30,6, $lihat["fullname"],1,0,'C',1);
									if($lihat["bulan"]==1){
										$pdf->Cell(15,6, "Jan" ,1,0,'C',1);
									}else if($lihat["bulan"]==2){
										$pdf->Cell(15,6, "Feb",1,0,'C',1);
									}else if($lihat["bulan"]==3){
										$pdf->Cell(15,6, "Mar",1,0,'C',1);
									}else if($lihat["bulan"]==4){
										$pdf->Cell(15,6, "Apr",1,0,'C',1);
									}else if($lihat["bulan"]==5){
										$pdf->Cell(15,6, "Mei",1,0,'C',1);
									}else if($lihat["bulan"]==6){
										$pdf->Cell(15,6, "Jun",1,0,'C',1);
									}else if($lihat["bulan"]==7){
										$pdf->Cell(15,6, "Jul",1,0,'C',1);
									}else if($lihat["bulan"]==8){
										$pdf->Cell(15,6, "Agu",1,0,'C',1);
									}else if($lihat["bulan"]==9){
										$pdf->Cell(15,6, "Sep",1,0,'C',1);
									}else if($lihat["bulan"]==10){
										$pdf->Cell(15,6, "Okt",1,0,'C',1);
									}else if($lihat["bulan"]==11){
										$pdf->Cell(15,6, "Nov",1,0,'C',1);
									}else if($lihat["bulan"]==12){
										$pdf->Cell(15,6, "Des",1,0,'C',1);
									}
									$pdf->Cell(25,6, $lihat["total"],1,0,'C',1);
									$pdf->Cell(20,6,$arr["totalbookingRange"][$index]['total'],1,0,'C',1);
									   if($lihat["Rank"]==1){
									   		//$pdf->Cell(20,6,$arr["bookingsroRangeSum"][$index]['total'],1,0,'C',1);
									   		 $pdf->Cell(25,6,$arr["targetunitentrySumRange"][0][$lihat["bulan"]],1,0,'C',1);
									   		$persentasi=ceil(($lihat['total']*100)/$arr["targetunitentrySumRange"][0][$lihat["bulan"]]);
									   		$pdf->Cell(20,6,$persentasi,1,0,'C',1);
									   		if(($arr["nama_cabang"]=="641940101") || ($arr["nama_cabang"]=="641940103")){
									   				if($persentasi>=persentasiCon1){
									   						$pdf->Cell(20,6,$persentasi*jmlIncentive/100,1,1,'C',1);
										   			}else{
										   					$pdf->Cell(20,6,"0",1,1,'C',1);
										   			}
									   		}else if($arr["nama_cabang"]=="641940102"){
									   				if($persentasi>=persentasiCon2){
									   						$pdf->Cell(20,6,$persentasi*jmlIncentive/100,1,1,'C',1);
										   			}else{
										   					$pdf->Cell(20,6,"0",1,1,'C',1);
										   			}
									   		}
									   		
									   		$index++;
									   }else{
									   	   //$pdf->Cell(20,6,"",1,0,'C',1);
									   	   $pdf->Cell(20,6,"",1,0,'C',1);
									   	   $pdf->Cell(20,6,"",1,0,'C',1);
									   	   $pdf->Cell(20,6,"",1,1,'C',1);
									   }
									    
									   
									// $pdf->Cell(20,6, $lihat["bookingdate"],1,0,'C',1);
									// $pdf->Cell(20,6, $lihat["SPK_date"],1,0,'C',1);
									// $pdf->Cell(20,6, $lihat["PoliceRegNo"],1,0,'C',1);
									// $pdf->Cell(30,6, $lihat["SPKNo"],1,0,'C',1);
									// $pdf->Cell(30,6, $lihat["invoiceno"],1,0,'C',1);
									// $pdf->Cell(40,6, $lihat["invoicedate"],1,0,'C',1);
									// $pdf->Cell(30,6, $lihat["status"],1,1,'C',1);
                         		

                         	endforeach;
   //                       			//$pdf->Cell(130,6,'Total',1,0,'R');
   //                  				//$pdf->Cell(20,6, $arr["calsro1"][0]["incentive"],1,1,'C');
   //                       	$pdf->Cell(130,6,'Total',1,0,'R');
   //                  				$pdf->Cell(20,6, $total,1,1,'C');
                    	

                 }else{
   //                  				//$pdf->Cell(30,6,'data nya ada',2,1,'C');
               }

   //          }else{
   //          	//$pdf->Cell(30,6,'data nya ada',2,1,'C');
             }


//else disini

   //           $pdf->Cell(25,4,'',0,1);

   //          if(isset($arr)){
   //                  if (array_key_exists("calsro2",$arr)){ 
			// 				$pdf->Cell(70,10,'SRO : ' .$arr["calsro2"][0]["fullname"],0,1,'L'); 
			// 		}else{
			// 				$pdf->Cell(70,10,'SRO : tidak ada',0,1,'L'); 
			// 		}
			// }else{
			// 				$pdf->Cell(70,10,'SRO : tidak ada 1',0,1,'L'); 
			// }

			// $pdf->SetFillColor(132, 188, 220);
			// $pdf->Cell(10,6,'No',1,0,'C',1);;
			// $pdf->Cell(40,6,'Booking Date',1,0,'C',1);
			// $pdf->Cell(40,6,'SPK Date',1,0,'C',1);
			// $pdf->Cell(20,6,'Bulan Invoice',1,0,'C',1);
			// $pdf->Cell(20,6,'Jumlah',1,0,'C',1);
			// $pdf->Cell(20,6,'Incentive',1,1,'C',1);
			
			// if(isset($arr)){
   //                  if (array_key_exists("calsro2",$arr)){ 
   //                       $no = 1;
   //                       	foreach ($arr["calsro2"] as $lihat):
   //                       		 	$pdf->Cell(10,6, $no++,1,0,'C');
			// 						$pdf->Cell(40,6, $lihat["bookingdate"],1,0,'C');
			// 						$pdf->Cell(40,6, $lihat["spkdate"],1,0,'C');
			// 						$pdf->Cell(20,6, $lihat["bulan_invoice"],1,0,'C');
			// 						$pdf->Cell(20,6, $lihat["jumlah"],1,0,'C');
			// 						$pdf->Cell(20,6, $lihat["incentive"],1,1,'C');
									
                         		 
   //                       	endforeach;
   //                       			//$pdf->Cell(130,6,'Total',1,0,'R');
   //                  				//$pdf->Cell(20,6, $arr["calsro2"][0]["incentive"],1,1,'C');
                    	

   //                  }else{
   //                  				//$pdf->Cell(30,6,'data nya ada',2,1,'C');
   //                  }

   //          }else{
   //          	//$pdf->Cell(30,6,'data nya ada',2,1,'C');
   //          }



             $pdf->Cell(25,4,'',0,1);

             $pdf->SetFont('Arial','B',10);

            if(isset($arr)){
                    
							$pdf->Cell(70,10,"Pembagian Incentive",0,1,'L'); 
			}

			$pdf->SetFillColor(132, 188, 220);
			$pdf->SetFont('Arial','',8);
			$pdf->Cell(40,6,'Position',1,0,'C',1);
			$pdf->Cell(40,6,'Nama',1,0,'C',1);
			$pdf->Cell(25,6,'Persentase(%)',1,0,'C',1);
			//$pdf->Cell(30,6,'Persentase/SRO(%)',1,0,'C',1);
			$pdf->Cell(30,6,'Total Incentive',1,1,'C',1);
			
			 if(isset($arr)){
                    if (array_key_exists("bookingsro",$arr)){ 
                         $no = 1;
                         $totalIncetive=0;
                          	foreach ($arr["bookingsro"] as $lihat):
									$pdf->Cell(40,6, "SRO" ,1,0,'C');
								    $pdf->Cell(40,6, $lihat["fullname"],1,0,'C');
			 					// 	$pdf->Cell(40,6, $lihat["totalIncentive"],1,0,'C');

			 						if($lihat["Rank"]==1){
                                                  $pdf->Cell(25,6,"75",1,0,'C');
                                       }else{
                                                  $pdf->Cell(25,6,"",1,0,'C');
                                    }
                                     $persentasiIncentive=ceil(($arr["bookingsroSUM"][0]['total']*100)/$arr["targetunitentrySum"][0]['jumlah']);
                                     $persentasi=($lihat["total"]*100/$arr["bookingsroSUM"][0]['total']);
									 //$pdf->Cell(30,6,$persentasi,1,0,'C');
									 	if(($arr["nama_cabang"]=="641940101") || ($arr["nama_cabang"]=="641940103")){

									 		if($persentasiIncentive>=persentasiCon1 ){
									 			$totalIncetive= (ceil(($arr["bookingsroSUM"][0]['total']*100)/$arr["targetunitentrySum"][0]['jumlah'])*jmlIncentive /100);
                                                   $hasil=$totalIncetive*75/100;
                                                    $hasilIncentive=($lihat["total"]*100/$arr["bookingsroSUM"][0]['total'])*$hasil/100;
                                         		$pdf->Cell(30,6,$hasilIncentive,1,1,'C');
											}else{
									 			$pdf->Cell(30,6,"0",1,1,'C');
									 		}

									 	}else if($arr["nama_cabang"]=="641940102"){

									 		if($persentasiIncentive>=persentasiCon2 ){
									 			$totalIncetive= (ceil(($arr["bookingsroSUM"][0]['total']*100)/$arr["targetunitentrySum"][0]['jumlah'])*jmlIncentive /100);
                                                   $hasil=$totalIncetive*75/100;
                                                    $hasilIncentive=($lihat["total"]*100/$arr["bookingsroSUM"][0]['total'])*$hasil/100;
                                         		$pdf->Cell(30,6,$hasilIncentive,1,1,'C');
											}else{
									 			$pdf->Cell(30,6,"0",1,1,'C');
									 		}

									 	}

									 

                         	endforeach;
   //                  				$pdf->Cell(90,6,'Total',1,0,'R');
   //                  				$pdf->Cell(20,6, $arr["sumsro"][0]["total"],1,1,'C');
                         	$pdf->Cell(40,6,"CCM",1,0,'C');
                         	$pdf->Cell(40,6,"",1,0,'C');
                         	$pdf->Cell(25,6,"10",1,0,'C');
                         	//$pdf->Cell(30,6,"-",1,0,'C');
                         	$pdf->Cell(30,6,(10*$totalIncetive/100),1,1,'C');

                         	$pdf->Cell(40,6,"CCE",1,0,'C');
                         	$pdf->Cell(40,6,"",1,0,'C');
                         	$pdf->Cell(25,6,"15",1,0,'C');
                         	//$pdf->Cell(30,6,"-",1,0,'C');
                         	$pdf->Cell(30,6,(15*$totalIncetive/100),1,1,'C');
                         	

                    }else{
   //                  				//$pdf->Cell(30,6,'data nya ada',2,1,'C');
                    }

            }else{
            	//$pdf->Cell(30,6,'data nya ada',2,1,'C');
            }

   //else disini

//$pdf->Cell(30,6,'eee',2,1,'C');

//chart properties
//position
$chartX=175;
$chartY=64;


//dimension
$chartWidth=115;
$chartHeight=70;

//padding
$chartTopPadding=5;
$chartLeftPadding=10;
$chartBottomPadding=10;
$chartRightPadding=5;

//chart box
$chartBoxX=$chartX+$chartLeftPadding;
$chartBoxY=$chartY+$chartTopPadding;
$chartBoxWidth=$chartWidth-$chartLeftPadding-$chartRightPadding;
//$chartBoxWidth=$chartWidth-50;
$chartBoxHeight=$chartHeight-$chartBottomPadding-$chartTopPadding;

//bar width
$barWidth=7;


//chart data

 

                         $index1=0;
                         $value=0;
                         $color=Array();
						 $data=Array();
                          	foreach ($arr["listbulan"] as $lihat):
                          		if( $lihat==="Jan"){
	  										$color=[255,21,0];
	  										$value=$arr["listvalue"][$index1];
	  						   }else if($lihat==="Feb"){
	    										$color=[255,225,0];
	    										$value=$arr["listvalue"][$index1];
	  						   }else if($lihat==="Mar"){
	    										$color=[50,123,255];
	    										$value=$arr["listvalue"][$index1];
	  						   }else if($lihat==="Apr"){
	    										$color=[255,140,255];
	    										$value=$arr["listvalue"][$index1];
	  						   }else if($lihat==="Mei"){
	    										$colo=[130,255,0];
	    										$value=$arr["listvalue"][$index1];
	  						   }else if($lihat==="Jun"){
	    										$color=[205,225,129];
	    										$value=$arr["listvalue"][$index1];
	  						   }else if($lihat==="Jul"){
	    										$color=[155,225,255];
	    										$value=$arr["listvalue"][$index1];
	  						   }else if($lihat==="Agu"){
	    										$color=[155,145,255];
	    										$value=$arr["listvalue"][$index1];
	  						   }else if($lihat==="Sep"){
	    										$color=[230,50,255];
	    										$value=$arr["listvalue"][$index1];
	  						   }else if($lihat==="Okt"){
	    										$color=[255,125,125];
	    										$value=$arr["listvalue"][$index1];
	  						   }else if($lihat==="Nov"){
	    										$color=[190,180,200];
	    										$value=$arr["listvalue"][$index1];
	  						   }else if($lihat==="Des"){
	    										$color=[220,225,170];
	    										$value=$arr["listvalue"][$index1];
	  						   }


	  						   	$index1++;
								$data[$lihat]=["color"=>$color, "value"=>$value];
								//$data["Feb"]=["color"=>[220,225,170], "value"=>50];

                             endforeach;

                            // print_r($data);


                   




// $data=Array(
//  'lorem'=>[
//   'color'=>[255,21,0],
//   'value'=>100],
//  'ipsum'=>[
//   'color'=>[255,233,0],
//   'value'=>300],
//  'dolor'=>[
//   'color'=>[50,123,255],
//   'value'=>150],
//  'sit'=>[
//   'color'=>[255,140,255],
//   'value'=>50],
//  'amet'=>[
//   'color'=>[130,255,0],
//   'value'=>10],
//   'susu'=>[
//   'color'=>[0,255,150],
//   'value'=>10],
//   'kopi'=>[
//   'color'=>[150,255,150],
//   'value'=>10],
//   'jahe'=>[
//   'color'=>[170,255,120],
//   'value'=>10],
//   'jamu'=>[
//   'color'=>[200,255,150],
//   'value'=>10],
//   'jab'=>[
//   'color'=>[200,255,200],
//   'value'=>10],
//   'data'=>[
//   'color'=>[128,255,200],
//   'value'=>10],
//   'kuda'=>[
//   'color'=>[240,255,190],
//   'value'=>0]
//  );

//$dataMax
$dataMax=0;
foreach($data as $item){
 if($item['value']>$dataMax)$dataMax=$item['value'];
}

//data step
$dataStep=3;


//set font, line width and color
$pdf->SetFont('Arial','',9);
$pdf->SetLineWidth(0.2);
$pdf->SetDrawColor(0);

//chart boundary
$pdf->Rect($chartX,$chartY,$chartWidth,$chartHeight);

//vertical axis line
$pdf->Line(
 $chartBoxX ,
 $chartBoxY , 
 $chartBoxX , 
 ($chartBoxY+$chartBoxHeight)
 );
//horizontal axis line
$pdf->Line(
 $chartBoxX-2 , 
 ($chartBoxY+$chartBoxHeight) , 
 $chartBoxX+($chartBoxWidth) , 
 ($chartBoxY+$chartBoxHeight)
 );

///vertical axis
//calculate chart's y axis scale unit
$yAxisUnits=$chartBoxHeight/$dataMax;

//draw the vertical (y) axis labels
for($i=0 ; $i<=$dataMax ; $i+=$dataStep){
 //y position
 $yAxisPos=$chartBoxY+($yAxisUnits*$i);
 //draw y axis line
 $pdf->Line(
  $chartBoxX-2 ,
  $yAxisPos ,
  $chartBoxX ,
  $yAxisPos
 );
 //set cell position for y axis labels
 $pdf->SetXY($chartBoxX-$chartLeftPadding , $yAxisPos-2);
 //$pdf->Cell($chartLeftPadding-4 , 5 , $dataMax-$i , 1);---------------
 $pdf->Cell($chartLeftPadding-4 , 5 , $dataMax-$i, 0 , 0 , 'R');
}

///horizontal axis
//set cells position
$pdf->SetXY($chartBoxX , $chartBoxY+$chartBoxHeight);

//cell's width
$xLabelWidth=$chartBoxWidth / count($data);

//$pdf->Cell($xLabelWidth , 5 , $itemName , 1 , 0 , 'C');-------------
//loop horizontal axis and draw the bar
$barXPos=0;
foreach($data as $itemName=>$item){
 //print the label
 //$pdf->Cell($xLabelWidth , 5 , $itemName , 1 , 0 , 'C');--------------
 $pdf->Cell($xLabelWidth , 5 , $itemName , 0 , 0 , 'C');
 
 ///drawing the bar
 //bar color
 $pdf->SetFillColor($item['color'][0],$item['color'][1],$item['color'][2]);
 //bar height
 $barHeight=$yAxisUnits*$item['value'];
 //bar x position
 $barX=($xLabelWidth/2)+($xLabelWidth*$barXPos);
 $barX=$barX-($barWidth/2);
 $barX=$barX+$chartBoxX;
 //bar y position
 $barY=$chartBoxHeight-$barHeight;
 $barY=$barY+$chartBoxY;
 //draw the bar
 $pdf->Rect($barX,$barY,$barWidth,$barHeight,'DF');
 //increase x position (next series)
 $barXPos++;
}

//axis labels
$pdf->SetFont('Arial','B',12);
$pdf->SetXY($chartX,$chartY);
//$pdf->Cell(100,10,"%",0);
$pdf->SetXY(($chartWidth/2)-50+$chartX,$chartY+$chartHeight-($chartBottomPadding/2));
//$pdf->Cell(100,5,"Series",0,0,'C');

$pdf->Cell(100,100,"",0,0,'C');










             $pdf->Ln(10);

             $pdf->SetFont('Arial','',8);
             $pdf->Cell(75,6,"Dibuat Oleh",0,0,'C');
             $pdf->Cell(25,6,"Disetujui",0,0,'C');
             $pdf->Cell(25,6,"Diperiksa",0,0,'C');
             $pdf->Cell(50,6,"DiKetahui",0,1,'C');


              if(isset($arr)){
                    if (array_key_exists("PIC",$arr)){ 
                    	$i = 0;
                    	$countarray=count($arr["PIC"]);
                           	foreach ($arr["PIC"] as $lihat):
                           		 $res = preg_replace('/[\@\.\;\" "]+/', '', $lihat["aprroveby"]);
                           		 if(++$i === $countarray) {
    								$pdf->cell(25,20,$pdf->Image(base_url()."image/tandatangan/".$res.".png",  $pdf->GetX(), $pdf->GetY(),25),0,1,'C');
  								 }else{
  								 	$pdf->cell(25,20,$pdf->Image(base_url()."image/tandatangan/".$res.".png",  $pdf->GetX(), $pdf->GetY(),25),0,0,'C');
  								 }
                          		  
                       //    		$res =  $lihat->b.aprroveby;
									
               
                     		endforeach;
                    	//$pdf->cell(30,20,print_r($arr["PIC"]),1,0,'C');
                    }else{
                    	//$pdf->Cell(30,6,"Disetujui 111",1,0,'C');
                    }
               }else{
               	//$pdf->Cell(30,6,"Disetujui22222",1,0,'C');
               }

             // $pdf->cell(30,20,"ttd sro",1,0,'C');
             // $pdf->cell(30,20,"ttd SM",1,0,'C');
             // $pdf->cell(30,20,"ttd ADH",1,0,'C');
             // $pdf->cell(30,20,"ttd Finance",1,0,'C');
             //  $pdf->Cell(30,20,"ttd BM",1,0,'C');
             //  $pdf->Cell(30,20,"ttd FAD",1,0,'C');
             //  $pdf->Cell(30,20,"ttd OM",1,1,'C');


              $pdf->cell(25,6,"sro",0,0,'C');
              $pdf->cell(25,6,"SM",0,0,'C');
              $pdf->cell(25,6,"ADH",0,0,'C');
              $pdf->cell(25,6,"BM",0,0,'C');
              $pdf->Cell(25,6,"Accounting",0,0,'C');
              $pdf->Cell(25,6,"FAD",0,0,'C');
              $pdf->Cell(25,6,"OM",0,1,'C');

             


             //$pdf->Image($image1, 5, 70, 33.78);

   //          if(isset($arr)){
   //                  if (array_key_exists("sumsro2",$arr)){ 
			// 				$pdf->Cell(70,10,'SRO : ' .$arr["sumsro2"][0]["fullname"],0,1,'L'); 
			// 		}else{
			// 				$pdf->Cell(70,10,'SRO : tidak ada',0,1,'L'); 
			// 		}
			// }else{
			// 				$pdf->Cell(70,10,'SRO : tidak ada ',0,1,'L'); 
			// }

			// $pdf->SetFillColor(132, 188, 220);
			// $pdf->Cell(10,6,'No',1,0,'C',1);;
			// $pdf->Cell(40,6,'Position',1,0,'C',1);
			// $pdf->Cell(40,6,'Persentasi',1,0,'C',1);
			// $pdf->Cell(20,6,'Total Incentive',1,1,'C',1);
			
			
			// if(isset($arr)){
   //                  if (array_key_exists("sumsro2",$arr)){ 
   //                       $no = 1;
   //                       	foreach ($arr["sumsro2"] as $lihat):
   //                       		 	$pdf->Cell(10,6, $no++,1,0,'C');
			// 						$pdf->Cell(40,6, $lihat["Position"],1,0,'C');
			// 						$pdf->Cell(40,6, $lihat["total_incentive"],1,0,'C');
			// 						$pdf->Cell(20,6, $lihat["totalIncentive"],1,1,'C');
									
									
                         		 
   //                       	endforeach;
   //                  				$pdf->Cell(90,6,'Total',1,0,'R');
   //                  				$pdf->Cell(20,6, $arr["sumsro2"][0]["total"],1,1,'C');

   //                  }else{
   //                  				//$pdf->Cell(30,6,'data nya ada',2,1,'C');
   //                  }

   //          }else{
   //          	//$pdf->Cell(30,6,'data nya ada',2,1,'C');
   //          }





            //cetak perhitungan sro









	




 












            if(isset($arr)){
                    if (array_key_exists("bookingsro",$arr)){ 
							$nama_pegawai=$arr["bookingsro"][0]["fullname"];
					}
							 
			}else{

			}

//$pdf->cell(30,6,"sro",0,0,'C');


            $pdf->Output('D',$nama_pegawai.'.pdf', true);
			
		}



	
		

}
    