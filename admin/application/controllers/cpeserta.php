<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cpeserta extends CI_Controller {

	    public function __construct() {
        parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
        $this->load->model('mpeserta');
    }
    
    public function parah() {
      

       $this->load->view('admin/peserta');
    }
	
	function tampil(){
		
		$a['page']	= "peserta";

		$this->load->view('admin/index', $a);
	}
	
	
	function json() {
		
	 $field=$this->input->post('field');
	
       echo $this->mpeserta->json($field);
	   
    }

/*    public function json() {
	$field =  $this->input->post('field');
        echo $this->mpeserta->json($field);
    }*/

	function upload(){
			$config['upload_path'] = './assets/imgpeserta';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']     = '5000';
			$config['max_width'] = '0';
			$config['max_height'] = '0';
			
			
			
	
			$this->load->library('upload',$config);
				
			$field_name = "uploadFile";
			if ( ! $this->upload->do_upload($field_name)) {
				// return the error message and kill the script
				//echo $this->upload->display_errors(); die();
				
			
			} else {
			
			}
		}



	/* Fungsi Jenis Surat */
	function getkota(){
        $id=$this->input->get('id');
		echo $this->mpeserta->getkota($id);
    }
	function getkecamatan(){
        $id=$this->input->get('id');
		echo $this->mpeserta->getkecamatan($id);
    }
	function getdesa(){
        $id=$this->input->get('id');
		echo $this->mpeserta->getdesa($id);
    }
	
	

	function tambah_peserta(){
		
		$a['page']	= "peserta/tambah_peserta";
		$this->load->view('admin/index', $a);
	}

	function insertdata(){
		$table =  'tpeserta';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->insert($table, $myjson );
		redirect('cpeserta/tampil','refresh');
	}



	function editpeserta($id){
		$a['page']	= "peserta/edit_peserta";
		$this->load->view('admin/index', $a, $id);
	}

	function updatedata(){
		$table =   'tpeserta';
		$idtable =  'idpeserta';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 


	}

	

	function hapuspeserta($id){
		$this->mpeserta->hapuspeserta($id);
		redirect('cpeserta/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->mpeserta->getjson();
    }

	
	function urlcmb()
    {

		echo $this->mpeserta->url();
    }
	
	
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mpeserta->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mpeserta->get_datapopup($string);
    }
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mpeserta->get_headerpopup($string);
    }


}


