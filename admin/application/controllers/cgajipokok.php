<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cgajipokok extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		// $this->load->helper(array('url','form'));
		
		 
		 $this->load->model('mgajipokok');
		 
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		 $this->session->unset_userdata('cari');
		 $this->session->set_userdata('cari', $this->input->post('table_search'));
		/*$a['data']	= $this->mgajipokok->tampil()->result_object();*/
		$a['page']	= "gajipokok";
		
		$this->load->view('admin/index', $a);
	}

	function tambah_gajipokok(){
		
		$a['page']	= "gajipokok/tambah_gajipokok";
		$this->load->view('admin/index', $a);
	}
	
	
	function savegapok(){
	$jml = 0;
	
		$idkaryawan=$_GET['idkaryawan'];
		$query =   $this->db->query("SELECT  count(*) as jml from tgaji WHERE idkaryawan =  '$idkaryawan'  " );
		foreach ($query->result() as $row)
			{
				$jml= $row->jml;
					   
			}
			if ($jml == 0 )
				{$this->insertdata();}
			if ($jml > 0 )
				{$this->updatedata();}
		
	}
	
	function insertdata(){
		$table =  'tgaji';
		
		$data=array ('idkaryawan' => $_GET['idkaryawan'],
		'gp' =>  $_GET['gp'],
		'lembur' =>  $_GET['lembur'],
		'transport' =>  $_GET['transport']);
		$this->db->insert($table, $data );
		redirect('cgajipokok/tampil','refresh');
	}



	function editgapok($id){
		$a['page']	= "gajipokok/edit_gajipokok";
		$this->load->view('admin/index', $a, $id);
	}

	function updatedata(){
		$idtable='idkaryawan';
		$table =  'tgaji';
		$idkaryawan=$_GET['idkaryawan'];
		$data=array ('idkaryawan' => $_GET['idkaryawan'],
		'gp' => $_GET['gp'],
		'lembur' => $_GET['lembur'],
		'transport' => $_GET['transport']);
		$this->db->where( $idtable, $idkaryawan);
		$this->db->update($table, $data);


	}

	

	function hapusgajipokok($id){
		$this->mgajipokok->hapusgajipokok($id);
		redirect('cgajipokok/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->mgajipokok->getjson();
    }

	
	function urlcmb()
    {

		echo $this->mgajipokok->url();
    }
	
	
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mgajipokok->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	


  	echo $this->mgajipokok->get_datapopup();
    }
	
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mgajipokok->get_headerpopup($string);
    }


}

