<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cnilaiakhir extends CI_Controller {

	function __construct(){
		parent::__construct();
		/*if($this->session->userdata('admin_valid') != TRUE ){
			redirect("login");
		}*/
		// $this->load->helper(array('url','form'));
		 $this->load->model('mnilaiakhir');
	}



	/* Fungsi Jenis Surat */
	function tampil(){
		
		$a['page']	= "nilaiakhirsiswa";
		
		$this->load->view('admin/index', $a);
	}
	function tampilnilai_siswa(){
		
		$a['page']	= "nilaiakhirsiswa";
		
		$this->load->view('admin/index', $a);
	}
	function json_nilaisiswa() {
	 $field=$this->input->post('field');
       echo $this->mnilaiakhir->json_nilaisiswa($field);
    }
	
	function json() {
	 	$field=$this->input->post('field');
       echo $this->mnilaiakhir->json($field);
    }
	function tambah_nilaiakhir(){
		
		$a['page']	= "nilaiakhir/tambah_nilaiakhir";
		$this->load->view('admin/index', $a);
	}

	function insertdata(){
		$table =  'tnilaiakhir';
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		
		$this->db->insert($table, $myjson );
		redirect('cnilaiakhir/tampil','refresh');
	}



	function editnilaiakhir($id){
		$a['page']	= "nilaiakhir/edit_nilaiakhir";
		$this->load->view('admin/index', $a, $id);
	}

	function updatedata(){
		$table =   'tnilaiakhir';
		$idtable =  'idnilaiakhir';
		$id = $_GET['id'];
		$bagong = $this->input->get('myjson');
		$myjson =json_decode($bagong,true);
		$this->db->where( $idtable, $id);
		$this->db->update($table, $myjson); 


	}

	

	function deletedtl1(){
		$idnilaiakhirdtl = $this->input->get('idnilaiakhirdtl');
		
		echo $this->mnilaiakhir->deletedtl1($idnilaiakhirdtl);
		
	}
	
	function hapusnilaiakhir($id){
		$this->mnilaiakhir->hapusnilaiakhir($id);
		redirect('cnilaiakhir/tampil','refresh');
	}

	function getjsonsample()
    {
		echo $this->mnilaiakhir->getjson();
    }

	
	function urlcmb()
    {

		echo $this->mnilaiakhir->url();
    }
	
	function tampiledit(){
		$field =  $this->input->get('idnilaiakhir');	
		echo $this->mnilaiakhir->tampiledit($field);
	}
	
	
	
	function datakelas(){	
		echo $this->mnilaiakhir->datakelas();
	}
	
	function datapengajar(){	
		echo $this->mnilaiakhir->datapengajar();
	}
	function datapelajaran(){	
		$string =  $this->input->get('idkelas');	
		echo $this->mnilaiakhir->datapelajaran($string);
	}
	function datapeserta(){	
		$string =  $this->input->get('idkelas');
		echo $this->mnilaiakhir->datapeserta($string);
	}
	
	function getjsonshow()
    {
	$id = $_GET['id'];
  	echo $this->mnilaiakhir->mgetjsonshow($id);
    }
	
	function getjson_popup()
    {
	
		$string =  $this->input->get('fields');	
		echo $this->mnilaiakhir->get_datapopup($string);
    }
	function getjson_headerpopup()
    {
	
		$string =  $_GET['fields'];
		echo $this->mnilaiakhir->get_headerpopup($string);
    }
	
	function urlautonumber()
    {
		echo $this->mnilaiakhir->urlnumber();
    }
	
	function cekidnilaiakhir()
	{
		$kdnilaiakhir = $this->input->get('kdnilaiakhir');
		echo $this->mnilaiakhir->idnilaiakhir($kdnilaiakhir);
	}
	function simpandtl(){
			$data= $this->input->get('arr');
			echo $this->mnilaiakhir->simpandtl($data);
			
		
		}
	function updatedtl(){
			$data= $this->input->get('arr');
			 
			echo $this->mnilaiakhir->updatedtl($data);
			
		
		}
}

