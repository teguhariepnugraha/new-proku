<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class msikap extends CI_Model {

	
	public function tampil($field)
	 {
		return  $this->db->query("SELECT * from tsikap where (sikap like '" . $field . "%' or kdsikap like '" . $field . "%') limit 1000 ");
  
    }
	
	public function sikap($field)
	 {
  		$arr = array();

		$query = $this->db->query("SELECT *  from tsikap where (kdsikap like '" . $field . "%' or sikap like '" . $field . "%'  )");
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function sikapdtl($idsikap)
	 {
  		$arr = array();

		$query = $this->db->query("SELECT *  from tsikapdtl where idsikap =$idsikap ");

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	
	public function idsikap($kdsikap)
	 {		 
		 $arr = array();
		
		$query = $this->db->query("SELECT count(*) as jml,idsikap from tsikap where kdsikap = '" . $kdsikap . "' group by idsikap");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);

  		 
    }
	
	function tampiledit($field){
		$arr = array();
		
		$query = $this->db->query("SELECT  a.idsikapdtl,a.idsikap,a.sikapdtl,a.bobot FROM  tsikapdtl AS a 
LEFT JOIN tsikap as b ON a.idsikap = b.idsikap   where a.idsikap = '$field'");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
         return  "{\"data\":" .json_encode($arr). "}";
	}
	
	
	
	public function simpandtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->insert_batch('tsikapdtl',$data);
  		 
    }
	public function updatedtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->update_batch('tsikapdtl',$data,'idsikapdtl');
  		 
    }

	public function deletedtl1($id)
	{
		return $this->db->delete('tsikapdtl', array('idsikapdtl' => $id));
	}
	
	public function hapussikap($id)
	{
		return $this->db->delete('tsikap', array('idsikap' => $id));
	}
	
	public function editsikap($id)
	{
		return $this->db->get_where('tsikap',array('idsikap'=>$id));
	}
	
	
	public function get_filterdata($field)
    {
        $arr = array();

		$query = $this->db->query("SELECT * from tsikap as b   where b.nmsikap like '" . $field . "%' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
		public function getjson()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT  column_name, column_type,column_comment FROM database_schema WHERE table_name =  'tsikap' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }

	
	public function mgetjsonshow($id)
    {
        $arr = array();


		$query = $this->db->query("SELECT * from tsikap as a where a.idsikap = '$id'");	
        
		foreach($query->result_object() as $rows )
        {
		foreach ($query->list_fields() as $field)
			{
				$arr[$field] =$rows->$field ;
			}	   	
       }

        return  json_encode($arr);

    }
	
	public function urlnumber()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
	public function url()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
}
