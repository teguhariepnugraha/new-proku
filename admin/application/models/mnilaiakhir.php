<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mnilaiakhir extends CI_Model {

		public function __construct() {
        parent::__construct();
    }
 function json($field)
	 {
		 $and =  $this->session->userdata('and');
		/*return $query = $this->db->query("SELECT a.idnilaiakhir,a.tgl,a.nota,a.supplier,sum(b.total) as total,a.ket from tnilaiakhir as a left join tnilaiakhirdtl as b on a.idnilaiakhir = b.idnilaiakhir where (nota like '" . $field . "%' or supplier like '" . $field . "%' ) group by a.idnilaiakhir ");*/


		
$requestData= $_REQUEST;
$columns = array( 	
  0 => 'idnilaiakhir',
  1 => 'tgl',
  2 => 'pengajar',
  3 => 'kelas',
  4 => 'pelajaran',

);
		$sql = " SELECT a.idnilaiakhir,a.tgl,b.nmkaryawan AS pengajar,c.kelas,d.nmpelajaran AS pelajaran
FROM tnilaiakhir AS a LEFT JOIN tkaryawan AS b ON a.`idpengajar` = b.`idkaryawan`
LEFT JOIN tkelas AS c ON a.`idkelas` = c.`idkelas` LEFT JOIN tpelajaran AS d ON a.idpelajaran = d.idpelajaran left join tnilaiakhirdtl as f on a.idnilaiakhir = f.idnilaiakhir where (b.nmkaryawan like '" . $field . "%' or c.kelas like '" . $field . "%' or d.nmpelajaran like '" . $field . "%' ) " . $and. " group by a.idnilaiakhir  ";

	$query =   $this->db->query($sql);
	$totalData = $query->num_rows();
	$totalFiltered = $totalData;
		
	if( !empty($requestData['search']['value']) ) {
	
	}
	
	$query =   $this->db->query($sql);
	$totalFiltered = $query->num_rows($sql);
		


	//----------------------------------------------------------------------------------
	
	$data = array();
	$x=0;
	 foreach($query->result_object() as $rows )
        {
			$x=$x+1;	  
			
		$nestedData=array(); 
					$nestedData[] = $x;
					$nestedData[] = $rows->tgl;
					$nestedData[] = $rows->pengajar;
					$nestedData[] = $rows->kelas;
					$nestedData[] = $rows->pelajaran;
					$nestedData[] =   "<div align='right'><a class='btn btn-info' href=editnilaiakhir/". $rows->idnilaiakhir ."  >
							  <i class='glyphicon glyphicon-edit icon-white'></i>
							  </a>
							  <a class='btn btn-danger' href=hapusnilaiakhir/". $rows->idnilaiakhir ." >
							  <i class='glyphicon glyphicon-trash icon-white'></i>
							  </a>
							  </div>";
		$data[] = $nestedData;
	}
	//----------------------------------------------------------------------------------
	$json_data = array(
 		"draw"            => intval( $requestData['draw'] ),  
		"recordsTotal"    => intval( $totalData ), 
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data );
	//----------------------------------------------------------------------------------
	return  json_encode($json_data);

    }
	
	public function idnilaiakhir($kdnilaiakhir)
	 {		 
		 $arr = array();
		
		$query = $this->db->query("SELECT count(*) as jml,idnilaiakhir from tnilaiakhir where kdnilaiakhir = '" . $kdnilaiakhir . "' ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);

  		 
    }
	
	
	function json_nilaisiswa($field)
	 {
 $and =  $this->session->userdata('and');
		/*return $query = $this->db->query("SELECT a.idnilaiakhir,a.tgl,a.nota,a.supplier,sum(b.total) as total,a.ket from tnilaiakhir as a left join tnilaiakhirdtl as b on a.idnilaiakhir = b.idnilaiakhir where (nota like '" . $field . "%' or supplier like '" . $field . "%' ) group by a.idnilaiakhir ");*/


		
$requestData= $_REQUEST;
$columns = array( 	
  0 => 'idnilaiakhir',
  1 => 'tgl',
  2 => 'pengajar',
  3 => 'kelas',
  4 => 'pelajaran',
  5 => 'mendengar',
  6 => 'membaca',
  7 => 'kosakata',
  8 => 'percakapan'
  

);
		$sql = " SELECT a.idnilaiakhir,a.tgl,b.nmkaryawan AS pengajar,c.kelas,d.nmpelajaran AS pelajaran,f.mendengar,f.membaca,f.kosakata,f.percakapan 
FROM tnilaiakhir AS a LEFT JOIN tkaryawan AS b ON a.`idpengajar` = b.`idkaryawan`
LEFT JOIN tkelas AS c ON a.`idkelas` = c.`idkelas` LEFT JOIN tpelajaran AS d ON a.idpelajaran = d.idpelajaran left join tnilaiakhirdtl as f on a.idnilaiakhir = f.idnilaiakhir where (b.nmkaryawan like '" . $field . "%' or c.kelas like '" . $field . "%' or d.nmpelajaran like '" . $field . "%' ) " . $and. " group by a.idnilaiakhir  ";

	$query =   $this->db->query($sql);
	$totalData = $query->num_rows();
	$totalFiltered = $totalData;
		
	if( !empty($requestData['search']['value']) ) {
	
	}
	
	$query =   $this->db->query($sql);
	$totalFiltered = $query->num_rows($sql);
		


	//----------------------------------------------------------------------------------
	
	$data = array();
	$x=0;
	 foreach($query->result_object() as $rows )
        {
			$x=$x+1;	  
		$nestedData=array(); 
					$nestedData[] = $x;
					$nestedData[] = $rows->tgl;
					$nestedData[] = $rows->pengajar;
					$nestedData[] = $rows->kelas;
					$nestedData[] = $rows->pelajaran;
					$nestedData[] = $rows->mendengar;
					$nestedData[] = $rows->membaca;
					$nestedData[] = $rows->kosakata;
					$nestedData[] = $rows->percakapan;
					
		$data[] = $nestedData;
	}
	//----------------------------------------------------------------------------------
	$json_data = array(
 		"draw"            => intval( $requestData['draw'] ),  
		"recordsTotal"    => intval( $totalData ), 
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data );
	//----------------------------------------------------------------------------------
	return  json_encode($json_data);

    }
	
	public function idnilaiharian($kdnilaiharian)
	 {		 
		 $arr = array();
		
		$query = $this->db->query("SELECT count(*) as jml,idnilaiharian from tnilaiharian where kdnilaiharian = '" . $kdnilaiharian . "' ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);

  		 
    }
	
	
	function tampiledit($field){
		$arr = array();
		
		$query = $this->db->query("SELECT  a.idnilaiakhirdtl,a.idnilaiakhir,a.idpeserta,a.mendengar,a.membaca,a.kosakata,a.percakapan,b.nama,b.nik FROM  tnilaiakhirdtl AS a LEFT JOIN tnilaiakhir AS c ON a.idnilaiakhir = c.idnilaiakhir left join tpeserta as b on a.idpeserta = b.idpeserta  where c.idnilaiakhir = '$field'");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
         return  "{\"data\":" .json_encode($arr). "}";
	}
	
	
	
	public function simpandtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->insert_batch('tnilaiakhirdtl',$data);
  		 
    }
	public function updatedtl($data)
	 {
		 $data = json_decode( $data, true );
		 $query = $this->db->update_batch('tnilaiakhirdtl',$data,'idnilaiakhirdtl');
  		 return  json_encode($query);
    }

	public function deletedtl1($id)
	{
		return $this->db->delete('tnilaiakhirdtl', array('idnilaiakhirdtl' => $id));
	}
	
	public function hapusnilaiakhir($id)
	{
		return $this->db->delete('tnilaiakhir', array('idnilaiakhir' => $id));
	}
	
	public function editnilaiakhir($id)
	{
		return $this->db->get_where('tnilaiakhir',array('idnilaiakhir'=>$id));
	}
	
	
	public function get_filterdata($field)
    {
        $arr = array();

		$query = $this->db->query("SELECT * from tnilaiakhir as b   where b.nmnilaiakhir like '" . $field . "%' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function urlnumber()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
	
	
	public function datakelas()
    {
        $idkaryawan = $this->session->userdata('userid'); 
        $arr = array();
		
		 $query = $this->db->query(" SELECT c.idkaryawan,.a.idkelas,b.kelas FROM tjadwaldtl AS a 
 LEFT JOIN tjadwal AS c ON a.idjadwal = c.idjadwal LEFT JOIN tkelas AS b ON a.idkelas = b.idkelas where c.idkaryawan = '$idkaryawan' " );
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	public function datapengajar()
    {
       $idkaryawan = $this->session->userdata('userid'); 
        $arr = array();
		 $query = $this->db->query("select idkaryawan as idpengajar,nik,nmkaryawan as nama from tkaryawan where idkaryawan = '$idkaryawan'" );
		 
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	public function datapelajaran($string)
    {
       $idkaryawan = $this->session->userdata('userid'); 
        $arr = array();
		
		 $query = $this->db->query("SELECT d.`idpelajaran`,.a.idkelas,b.kelas,d.`nmpelajaran`,d.`kdpelajaran` FROM tjadwaldtl AS a 
 LEFT JOIN tjadwal AS c ON a.idjadwal = c.idjadwal LEFT JOIN tkelas AS b ON a.idkelas = b.idkelas
 LEFT JOIN tpelajaran AS d ON a.`idkelas` = d.`idkelas` where c.idkaryawan = '$idkaryawan' and a.idkelas='$string'" );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	public function datapeserta($string)
    {
        $arr = array();
		
		$query = $this->db->query("select idpeserta,nik,nama from tpeserta  where idkelas='$string'" );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	
	
		public function getjson()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT  column_name, column_type,column_comment FROM database_schema WHERE table_name =  'tnilaiakhir' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }

	
	public function mgetjsonshow($id)
    {
        $arr = array();


		$query = $this->db->query("SELECT * from tnilaiakhir  where idnilaiakhir = '$id'");	
        
		foreach($query->result_object() as $rows )
        {
		foreach ($query->list_fields() as $field)
			{
				$arr[$field] =$rows->$field ;
			}	   	
       }

        return  json_encode($arr);

    }
	
	public function get_datapopup($field)
    {
       $arr = array();
		
	$query = $this->db->query("SELECT a.idnilaiakhir ,a.nik,a.nmnilaiakhir  FROM  tnilaiakhir AS a  where (a.nmnilaiakhir like '" . $field . "%' or a.nik like '" . $field . "%')   limit 1000 ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function url()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
}
