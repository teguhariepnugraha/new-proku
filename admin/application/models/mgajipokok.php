<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mgajipokok extends CI_Model {

	
	public function tampil()
	 {
		return  $this->db->query("SELECT * from tgapok  ");
  
    }
	public function hapusidgapok($id)
	{
	
		return $this->db->delete('tgapok', array('idgapok' => $id));
	}
	
	public function editgapok($id)
	{
		return $this->db->get_where('tgapok',array('idgapok'=>$id));
	}
	
	
	public function get_filterdata($field)
    {
        $arr = array();

		$query = $this->db->query("SELECT * from tgapok as b   where b.depatemen like '" . $field . "%' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
		public function getjson()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT  column_name, column_type,column_comment FROM database_schema WHERE table_name =  'tgapok' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }

	
	public function mgetjsonshow($id)
    {
        $arr = array();


		$query = $this->db->query("SELECT * from tgapok as a where a.idgapok = '$id'");	
        
		foreach($query->result_object() as $rows )
        {
		foreach ($query->list_fields() as $field)
			{
				$arr[$field] =$rows->$field ;
			}	   	
       }

        return  json_encode($arr);

    }
	
	public function get_datapopup()
    {
	  $field = $this->session->userdata('cari');
       $arr = array();
		
	$query = $this->db->query("SELECT a.idkaryawan,a.nik,a.nmkaryawan,b.idgaji,b.gp,b.lembur,b.transport FROM tkaryawan AS a LEFT JOIN tgaji AS b ON a.idkaryawan = b.idkaryawan  where (a.nmkaryawan like '" . $field . "%' or a.nik like '" . $field . "%')   limit 1000  ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
}
