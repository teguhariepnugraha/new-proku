<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mlembur extends CI_Model {

	
	public function tampil($field)
	 {
		 $and =  $this->session->userdata('and');
		return $query = $this->db->query("SELECT * from tlembur as a where (a.reg like '" . $field . "%'  )" . $and);
  
    }
	public function idlembur($reg)
	 {		 
		 $arr = array();
		
		$query = $this->db->query("SELECT count(*) as jml,idlembur from tlembur where reg = '" . $reg . "' ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);

  		 
    }
	
	function tampiledit($field){
		$arr = array();
		
		$query = $this->db->query("SELECT  a.idlemburdtl,a.idlembur,a.idkaryawan,b.nik,b.nmkaryawan FROM  tlemburdtl AS a 
LEFT JOIN tkaryawan AS b ON a.idkaryawan = b.idkaryawan  where a.idlembur = '$field'");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
         return  "{\"data\":" .json_encode($arr). "}";
	}
	
	
	
	public function simpandtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->insert_batch('tlemburdtl',$data);
  		 
    }
	public function updatedtl($data)
	 {
		 $data = json_decode( $data, true );
		 $query = $this->db->update_batch('tlemburdtl',$data,'idlemburdtl');
  		 return  json_encode($query);
    }

	public function deletedtl1($id)
	{
		return $this->db->delete('tlemburdtl', array('idlemburdtl' => $id));
	}
	
	public function hapuslembur($id)
	{
		return $this->db->delete('tlembur', array('idlembur' => $id));
	}
	
	public function editlembur($id)
	{
		return $this->db->get_where('tlembur',array('idlembur'=>$id));
	}
	
	
	public function get_filterdata($field)
    {
        $arr = array();

		$query = $this->db->query("SELECT * from tlembur as b   where b.nmlembur like '" . $field . "%' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function urlnumber()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
	
	
	public function datakaryawan()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT idkaryawan,nik,nmkaryawan FROM tkaryawan " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	
		public function getjson()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT  column_name, column_type,column_comment FROM database_schema WHERE table_name =  'tlembur' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }

	
	public function mgetjsonshow($id)
    {
        $arr = array();


		$query = $this->db->query("SELECT * from tlembur  where idlembur = '$id'");	
        
		foreach($query->result_object() as $rows )
        {
		foreach ($query->list_fields() as $field)
			{
				$arr[$field] =$rows->$field ;
			}	   	
       }

        return  json_encode($arr);

    }
	
	public function get_datapopup($field)
    {
       $arr = array();
		
	$query = $this->db->query("SELECT a.idkaryawan ,a.nik,a.nmkaryawan  FROM  tkaryawan AS a  where (a.nmkaryawan like '" . $field . "%' or a.nik like '" . $field . "%')   limit 1000 ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function url()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
}
