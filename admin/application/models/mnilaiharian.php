<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mnilaiharian extends CI_Model {

		public function __construct() {
        parent::__construct();
		
    }
	
 function json($field)
	 {
 $and =  $this->session->userdata('and');
		/*return $query = $this->db->query("SELECT a.idnilaiharian,a.tgl,a.nota,a.supplier,sum(b.total) as total,a.ket from tnilaiharian as a left join tnilaihariandtl as b on a.idnilaiharian = b.idnilaiharian where (nota like '" . $field . "%' or supplier like '" . $field . "%' ) group by a.idnilaiharian ");*/
	


$requestData= $_REQUEST;
$columns = array( 	
  0 => 'idnilaiharian',
  1 => 'tgl',
  2 => 'pengajar',
  3 => 'kelas',
  4 => 'pelajaran',

);
		$sql = " SELECT a.idnilaiharian,a.tgl,b.nmkaryawan AS pengajar,c.kelas,d.nmpelajaran AS pelajaran
FROM tnilaiharian AS a LEFT JOIN tkaryawan AS b ON a.`idpengajar` = b.`idkaryawan`
LEFT JOIN tkelas AS c ON a.`idkelas` = c.`idkelas` LEFT JOIN tpelajaran AS d ON a.idpelajaran = d.idpelajaran left join tnilaihariandtl as f on a.idnilaiharian = f.idnilaiharian  where (b.nmkaryawan like '" . $field . "%' or c.kelas like '" . $field . "%' or d.nmpelajaran like '" . $field . "%'  ) " . $and . " group by a.idnilaiharian  ";

	$query =   $this->db->query($sql);
	$totalData = $query->num_rows();
	$totalFiltered = $totalData;
		
	if( !empty($requestData['search']['value']) ) {
	
	}
	
	$query =   $this->db->query($sql);
	$totalFiltered = $query->num_rows($sql);
		


	//----------------------------------------------------------------------------------
	
	$data = array();
	$x=0;
	 foreach($query->result_object() as $rows )
        {
			$x=$x+1;	  
			
		$nestedData=array(); 
					$nestedData[] = $x;
					$nestedData[] = $rows->tgl;
					$nestedData[] = $rows->pengajar;
					$nestedData[] = $rows->kelas;
					$nestedData[] = $rows->pelajaran;
					$nestedData[] =   "<div align='right'><a class='btn btn-info' href=editnilaiharian/". $rows->idnilaiharian ."  >
							  <i class='glyphicon glyphicon-edit icon-white'></i>
							  </a>
							  <a class='btn btn-danger' href=hapusnilaiharian/". $rows->idnilaiharian ." >
							  <i class='glyphicon glyphicon-trash icon-white'></i>
							  </a>
							  </div>";
		$data[] = $nestedData;
	}
	//----------------------------------------------------------------------------------
	$json_data = array(
 		"draw"            => intval( $requestData['draw'] ),  
		"recordsTotal"    => intval( $totalData ), 
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data );
	//----------------------------------------------------------------------------------
	return  json_encode($json_data);

    }
	
	
	
	function json_nilaisiswa($field)
	 {
 $and =  $this->session->userdata('and');
		/*return $query = $this->db->query("SELECT a.idnilaiharian,a.tgl,a.nota,a.supplier,sum(b.total) as total,a.ket from tnilaiharian as a left join tnilaihariandtl as b on a.idnilaiharian = b.idnilaiharian where (nota like '" . $field . "%' or supplier like '" . $field . "%' ) group by a.idnilaiharian ");*/
	


$requestData= $_REQUEST;
$columns = array( 	
  0 => 'idnilaiharian',
  1 => 'tgl',
  2 => 'pengajar',
  3 => 'kelas',
  4 => 'pelajaran',

);
		$sql = " SELECT a.idnilaiharian,a.tgl,b.nmkaryawan AS pengajar,c.kelas,d.nmpelajaran AS pelajaran
FROM tnilaiharian AS a LEFT JOIN tkaryawan AS b ON a.`idpengajar` = b.`idkaryawan`
LEFT JOIN tkelas AS c ON a.`idkelas` = c.`idkelas` LEFT JOIN tpelajaran AS d ON a.idpelajaran = d.idpelajaran left join tnilaihariandtl as f on a.idnilaiharian = f.idnilaiharian  where (b.nmkaryawan like '" . $field . "%' or c.kelas like '" . $field . "%' or d.nmpelajaran like '" . $field . "%'  ) " . $and . " group by a.idnilaiharian  ";

	$query =   $this->db->query($sql);
	$totalData = $query->num_rows();
	$totalFiltered = $totalData;
		
	if( !empty($requestData['search']['value']) ) {
	
	}
	
	$query =   $this->db->query($sql);
	$totalFiltered = $query->num_rows($sql);
		


	//----------------------------------------------------------------------------------
	
	$data = array();
	$x=0;
	 foreach($query->result_object() as $rows )
        {
			$x=$x+1;	  
			
		$nestedData=array(); 
					$nestedData[] = $x;
					$nestedData[] = $rows->tgl;
					$nestedData[] = $rows->pengajar;
					$nestedData[] = $rows->kelas;
					$nestedData[] = $rows->pelajaran;
					
		$data[] = $nestedData;
	}
	//----------------------------------------------------------------------------------
	$json_data = array(
 		"draw"            => intval( $requestData['draw'] ),  
		"recordsTotal"    => intval( $totalData ), 
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data );
	//----------------------------------------------------------------------------------
	return  json_encode($json_data);

    }
	
	public function idnilaiharian($kdnilaiharian)
	 {		 
		 $arr = array();
		
		$query = $this->db->query("SELECT count(*) as jml,idnilaiharian from tnilaiharian where kdnilaiharian = '" . $kdnilaiharian . "' ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);

  		 
    }
	
	function tampiledit($field){
		$arr = array();
		
		$query = $this->db->query("SELECT  a.idnilaihariandtl,a.idnilaiharian,a.idpeserta,a.nilai,b.nama,b.nik FROM  tnilaihariandtl AS a LEFT JOIN tnilaiharian AS c ON a.idnilaiharian = c.idnilaiharian left join tpeserta as b on a.idpeserta = b.idpeserta  where c.idnilaiharian = '$field'");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
         return  "{\"data\":" .json_encode($arr). "}";
	}
	
	
	
	public function simpandtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->insert_batch('tnilaihariandtl',$data);
  		 
    }
	public function updatedtl($data)
	 {
		 $data = json_decode( $data, true );
		 $query = $this->db->update_batch('tnilaihariandtl',$data,'idnilaihariandtl');
  		 return  json_encode($query);
    }

	public function deletedtl1($id)
	{
		return $this->db->delete('tnilaihariandtl', array('idnilaihariandtl' => $id));
	}
	
	public function hapusnilaiharian($id)
	{
		return $this->db->delete('tnilaiharian', array('idnilaiharian' => $id));
	}
	
	public function editnilaiharian($id)
	{
		return $this->db->get_where('tnilaiharian',array('idnilaiharian'=>$id));
	}
	
	
	public function get_filterdata($field)
    {
        $arr = array();

		$query = $this->db->query("SELECT * from tnilaiharian as b   where b.nmnilaiharian like '" . $field . "%' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function urlnumber()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
	
	
	public function datakelas()
    {
		$idkaryawan = $this->session->userdata('userid'); 
        $arr = array();
		
		 $query = $this->db->query(" SELECT c.idkaryawan,.a.idkelas,b.kelas FROM tjadwaldtl AS a 
 LEFT JOIN tjadwal AS c ON a.idjadwal = c.idjadwal LEFT JOIN tkelas AS b ON a.idkelas = b.idkelas where c.idkaryawan = '$idkaryawan' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	public function datapengajar()
    {
		$idkaryawan = $this->session->userdata('userid'); 
        $arr = array();
		
		 $query = $this->db->query("select idkaryawan as idpengajar,nik,nmkaryawan as nama from tkaryawan where idkaryawan = '$idkaryawan'" );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	public function datapelajaran($string)
    {
		$idkaryawan = $this->session->userdata('userid'); 
        $arr = array();
		
		 $query = $this->db->query("SELECT d.`idpelajaran`,.a.idkelas,b.kelas,d.`nmpelajaran`,d.`kdpelajaran` FROM tjadwaldtl AS a 
 LEFT JOIN tjadwal AS c ON a.idjadwal = c.idjadwal LEFT JOIN tkelas AS b ON a.idkelas = b.idkelas
 LEFT JOIN tpelajaran AS d ON a.`idkelas` = d.`idkelas` where c.idkaryawan = '$idkaryawan' and a.idkelas='$string'" );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	public function datapeserta($string)
    {
        $arr = array();
		
		 $query = $this->db->query("select idpeserta,nik,nama from tpeserta  where idkelas='$string'" );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	
	
		public function getjson()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT  column_name, column_type,column_comment FROM database_schema WHERE table_name =  'tnilaiharian' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }

	
	public function mgetjsonshow($id)
    {
        $arr = array();


		$query = $this->db->query("SELECT * from tnilaiharian  where idnilaiharian = '$id'");	
        
		foreach($query->result_object() as $rows )
        {
		foreach ($query->list_fields() as $field)
			{
				$arr[$field] =$rows->$field ;
			}	   	
       }

        return  json_encode($arr);

    }
	
	public function get_datapopup($field)
    {
       $arr = array();
		
	$query = $this->db->query("SELECT a.idnilaiharian ,a.nik,a.nmnilaiharian  FROM  tnilaiharian AS a  where (a.nmnilaiharian like '" . $field . "%' or a.nik like '" . $field . "%')   limit 1000 ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function url()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
}
