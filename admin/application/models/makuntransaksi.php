<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class makuntransaksi extends CI_Model {

	public function tampil()
	 {
		return  $this->db->query("");
  
    }
	public function thnajaran($field)
	 {
  		$arr = array();
		
		$query = $this->db->query("SELECT idakun_transaksi,tgl,thn_ajaran,sum(pagu) as total from takun_transaksi where (thn_ajaran like '" . $field . "%' )group by thn_ajaran  ");

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function akun($thnajaran)
	 {
  		$arr = array();

		$query = $this->db->query("SELECT a.thn_ajaran,c.idakun,c.kdakun,c.akun,sum(a.pagu) as pagu 
FROM takun_transaksi AS a LEFT JOIN takundtl AS b ON b.idakundtl = a.idakun LEFT JOIN takun AS c ON c.idakun = b.`idakun`  where (thn_ajaran like '" . $thnajaran . "%' ) GROUP BY c.idakun,a.thn_ajaran");

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function akundtl($thnajaran,$idakun)
	 {
  		$arr = array();

		$query = $this->db->query("SELECT c.idakun,a.idakun,b.kdakundtl,b.akundtl,b.satuan,a.pagu
FROM takun_transaksi AS a LEFT JOIN takundtl AS b ON b.idakundtl = a.idakun LEFT JOIN takun AS c ON c.idakun = b.`idakun`  where (c.idakun=$idakun and a.thn_ajaran= '" . $thnajaran . "' ) ");

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function simpandtl($data,$thn_ajaran)
	 {
		 
		 $thn_ajaran = json_decode( $thn_ajaran, true );
		 $query = $this->db->query("SELECT * from takun_transaksi where thn_ajaran = '" . $thn_ajaran . "' ");
		 $total = $query->num_rows();
		 if ($total > 0 )
		 {
			
			$data = json_decode( $data, true );
			$this->db->insert_batch('takun_transaksi',$data);
		 }
		 else
		 {
			$this->db->query("update takun_transaksi set flag=0 where flag = '1' ");
			$data = json_decode( $data, true );
			$this->db->insert_batch('takun_transaksi',$data);
		 }
		 
    }
	
	public function updatedtl($data)
	 {
		 $data = json_decode( $data, true );
		return $this->db->update_batch('takun_transaksi',$data,'idakun_transaksi');
    }

	public function deletedtl($data){
		$data = json_decode( $data, true );
        $this->db->where_in('idakun_transaksi', $data);
    	return $this->db->delete('takun_transaksi');
  	}
	
	public function akundtlshow()
	 {
  		$arr = array();

		$query = $this->db->query("SELECT b.idakundtl,c.akun AS kategori,b.kdakundtl,b.akundtl,b.satuan
FROM takundtl AS b   LEFT JOIN takun AS c ON c.idakun = b.`idakun`   ");

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function akundtlshowedit($thnajaran)
	 {
  		$arr = array();

		$query = $this->db->query("SELECT d.idakun_transaksi,c.akun AS kategori,b.kdakundtl,b.akundtl,b.satuan,IF (b.idakundtl>0, 'true', 'false') as chk,d.pagu
FROM takundtl AS b   LEFT JOIN takun AS c ON c.idakun = b.`idakun` left join takun_transaksi as d ON b.idakundtl = d.idakun where d.thn_ajaran like '" . $thnajaran . "'  ");

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function hapusakuntransaksi($id)
	{
	
		return $this->db->delete('takun_transaksi', array('idakun_transaksi' => $id));
	}
	
	public function editakun_transaksi($id)
	{
		return $this->db->get_where('takun_transaksi',array('thn_ajaran'=>$id));
	}
	
	
	public function get_filterdata($field)
    {
        $arr = array();

		$query = $this->db->query("SELECT * from takun_transaksi as b   where b.akundtl like '" . $field . "%' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
		public function getjson()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT  column_name, column_type,column_comment FROM database_schema WHERE table_name =  'takun_transaksi' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }

	
	public function mgetjsonshow($id)
    {
        $arr = array();


		$query = $this->db->query("SELECT * from takun_transaksi as a where a.thn_ajaran = '$id'");	
        
		foreach($query->result_object() as $rows )
        {
		foreach ($query->list_fields() as $field)
			{
				$arr[$field] =$rows->$field ;
			}	   	
       }

        return  json_encode($arr);

    }
	
}
