<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mregistrasi extends CI_Model {

		public function __construct() {
        parent::__construct();
    }
 function json($field)
	 {
		 $and =  $this->session->userdata('and');
		/*return $query = $this->db->query("SELECT a.idregistrasi,a.tgl,a.kdregistrasi,a.supplier,sum(b.total) as total,a.ket from tregistrasi as a left join tregistrasi as b on a.idregistrasi = b.idregistrasi where (kdregistrasi like '" . $field . "%' or supplier like '" . $field . "%' ) group by a.idregistrasi ");*/
		
$requestData= $_REQUEST;
$columns = array( 	
  0 => 'idregistrasi',
  1 => 'tgl',
  2 => 'kdregistrasi',
  3 => 'nik',
  4 => 'nama',
  5 => 'akun',
  6 => 'nominal',


);
		$sql = " SELECT a.idregistrasi,a.tgl,a.kdregistrasi,b.nik,b.nama,c.akundtl AS akun,a.nominal FROM tregistrasi AS a 
LEFT JOIN tpeserta AS b ON a.idpeserta = b.idpeserta LEFT JOIN takundtl AS c ON a.idakun = c.idakundtl where ( a.kdregistrasi like '" . $field . "%' or b.nik like '" . $field . "%' or b.nama like '" . $field . "%' or c.akundtl like '" . $field . "%' ) "  . $and . " group by a.idregistrasi  ";

	$query =   $this->db->query($sql);
	$totalData = $query->num_rows();
	$totalFiltered = $totalData;
		
	if( !empty($requestData['search']['value']) ) {
	
	}
	
	$query =   $this->db->query($sql);
	$totalFiltered = $query->num_rows($sql);
		


	//----------------------------------------------------------------------------------
	
	$data = array();
	$x=0;
	 foreach($query->result_object() as $rows )
        {
			$x=$x+1;	  
		$nestedData=array(); 
					$nestedData[] = $x;
					$nestedData[] = $rows->tgl;
					$nestedData[] = $rows->kdregistrasi;
					$nestedData[] = $rows->nik;
					$nestedData[] = $rows->nama;
					$nestedData[] = $rows->akun;
					$nestedData[] = number_format($rows->nominal, 2);
					$nestedData[] =   "<div align='right'><a class='btn btn-info' href=editregistrasi/". $rows->idregistrasi ."  >
							  <i class='glyphicon glyphicon-edit icon-white'></i>
							  </a>
							  <a class='btn btn-danger' href=hapusregistrasi/". $rows->idregistrasi ." >
							  <i class='glyphicon glyphicon-trash icon-white'></i>
							  </a>
							  </div>";
		$data[] = $nestedData;
	}
	//----------------------------------------------------------------------------------
	$json_data = array(
		"draw"            => intval( $requestData['draw'] ), 
		"recordsTotal"    => intval( $totalData ), 
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data );
	//----------------------------------------------------------------------------------
	return  json_encode($json_data);

    }
	

	
	function tampiledit($field){
		$arr = array();
		
		$query = $this->db->query("SELECT a.idregistrasi,a.tgl,a.kdregistrasi,b.nik,b.nama,c.akundtl AS akun,a.nominal FROM tregistrasi AS a 
LEFT JOIN tpeserta AS b ON a.idpeserta = b.idpeserta LEFT JOIN takundtl AS c ON a.idakun = c.idakundtl where a.idregistrasi = '$field' ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
         return  "{\"data\":" .json_encode($arr). "}";
	}
	
	
	
	public function simpandtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->insert_batch('tregistrasi',$data);
  		 
    }
	public function updatedtl($data)
	 {
		 $data = json_decode( $data, true );
		 $query = $this->db->update_batch('tregistrasi',$data,'idregistrasi');
  		 return  json_encode($query);
    }

	public function deletedtl1($id)
	{
		return $this->db->delete('tregistrasi', array('idregistrasi' => $id));
	}
	
	public function hapusregistrasi($id)
	{
		return $this->db->delete('tregistrasi', array('idregistrasi' => $id));
	}
	
	public function editregistrasi($id)
	{
		return $this->db->get_where('tregistrasi',array('idregistrasi'=>$id));
	}
	
	
	public function get_filterdata($field)
    {
        $arr = array();

		$query = $this->db->query("SELECT * from tregistrasi as b   where b.nmregistrasi like '" . $field . "%' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function urlnumber()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
	
	
	public function datapeserta()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT idpeserta,nik,nama,CONCAT(alamat,' RT ',rt,' RW ',rw,' Desa ',desa,' Kecamatan ',kecamatan,' Kabupaten ',kabupaten,' Propinsi ',propinsi) AS alamat FROM tpeserta" );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	
	public function dataakun()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT a.idakun,b.kdakundtl AS kdakun,b.akundtl AS nmakun,format(pagu,2) as pagu,flag 
FROM takun_transaksi AS a LEFT JOIN takundtl AS b ON a.idakun = b.idakundtl WHERE flag = '1'" );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	
	
		public function getjson()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT  column_name, column_type,column_comment FROM database_schema WHERE table_name =  'tregistrasi' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }

	
	public function mgetjsonshow($id)
    {
        $arr = array();


		$query = $this->db->query("SELECT * from tregistrasi  where idregistrasi = '$id'");	
        
		foreach($query->result_object() as $rows )
        {
		foreach ($query->list_fields() as $field)
			{
				$arr[$field] =$rows->$field ;
			}	   	
       }

        return  json_encode($arr);

    }
	
	public function get_datapopup($field)
    {
       $arr = array();
		
	$query = $this->db->query("SELECT a.idregistrasi ,a.nik,a.nmregistrasi  FROM  tregistrasi AS a  where (a.nmregistrasi like '" . $field . "%' or a.nik like '" . $field . "%')   limit 1000 ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function url()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
}
