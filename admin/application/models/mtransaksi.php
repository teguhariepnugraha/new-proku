<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mtransaksi extends CI_Model {

		public function __construct() {
        parent::__construct();
    }
 function json($field)
	 {
		 $and =  $this->session->userdata('and');
		/*return $query = $this->db->query("SELECT a.idtransaksi,a.tgl,a.nota,a.supplier,sum(b.total) as total,a.ket from ttransaksi as a left join ttransaksidtl as b on a.idtransaksi = b.idtransaksi where (nota like '" . $field . "%' or supplier like '" . $field . "%' ) group by a.idtransaksi ");*/
		
$requestData= $_REQUEST;
$columns = array( 	
  0 => 'idtransaksi',
  1 => 'tgl',
  2 => 'nota',
  3 => 'supplier',
  4 => 'total',
  5 => 'ket',

);
		$sql = " SELECT a.idtransaksi,a.tgl,a.nota,a.supplier,sum(b.total) as total,a.ket from ttransaksi as a left join ttransaksidtl as b on a.idtransaksi = b.idtransaksi where (nota like '" . $field . "%' or supplier like '" . $field . "%' ) "  . $and . " group by a.idtransaksi  ";

	$query =   $this->db->query($sql);
	$totalData = $query->num_rows();
	$totalFiltered = $totalData;
		
	if( !empty($requestData['search']['value']) ) {
	
	}
	
	$query =   $this->db->query($sql);
	$totalFiltered = $query->num_rows($sql);
		


	//----------------------------------------------------------------------------------
	
	$data = array();
	$x=0;
	 foreach($query->result_object() as $rows )
        {
			$x=$x+1;	  
		$nestedData=array(); 
					$nestedData[] = $x;
					$nestedData[] = $rows->tgl;
					$nestedData[] = $rows->nota;
					$nestedData[] = $rows->supplier;
					$nestedData[] = number_format($rows->total, 2);
					$nestedData[] = $rows->ket;
					$nestedData[] =   "<div align='right'><a class='btn btn-info' href=edittransaksi/". $rows->idtransaksi ."  >
							  <i class='glyphicon glyphicon-edit icon-white'></i>
							  </a>
							  <a class='btn btn-danger' href=hapustransaksi/". $rows->idtransaksi ." >
							  <i class='glyphicon glyphicon-trash icon-white'></i>
							  </a>
							  </div>";
		$data[] = $nestedData;
	}
	//----------------------------------------------------------------------------------
	$json_data = array(
 		"draw"            => intval( $requestData['draw'] ),  
		"recordsTotal"    => intval( $totalData ), 
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data );
	//----------------------------------------------------------------------------------
	return  json_encode($json_data);

    }
	
	public function idtransaksi($nota)
	 {		 
		 $arr = array();
		
		$query = $this->db->query("SELECT count(*) as jml,idtransaksi from ttransaksi where nota = '" . $nota . "' ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);

  		 
    }
	
	function tampiledit($field){
		$arr = array();
		
		$query = $this->db->query("SELECT  a.idtransaksidtl,a.idtransaksi,a.idakun,b.akundtl AS akun,b.satuan,a.qty,a.`harga`,a.`total` FROM  ttransaksidtl AS a 
LEFT JOIN ttransaksi AS c ON a.idtransaksi = c.idtransaksi LEFT JOIN takundtl AS b ON a.idakun = b.idakundtl where c.idtransaksi = '$field'");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
         return  "{\"data\":" .json_encode($arr). "}";
	}
	
	
	
	public function simpandtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->insert_batch('ttransaksidtl',$data);
  		 
    }
	public function updatedtl($data)
	 {
		 $data = json_decode( $data, true );
		 $query = $this->db->update_batch('ttransaksidtl',$data,'idtransaksidtl');
  		 return  json_encode($query);
    }

	public function deletedtl1($id)
	{
		return $this->db->delete('ttransaksidtl', array('idtransaksidtl' => $id));
	}
	
	public function hapustransaksi($id)
	{
		return $this->db->delete('ttransaksi', array('idtransaksi' => $id));
	}
	
	public function edittransaksi($id)
	{
		return $this->db->get_where('ttransaksi',array('idtransaksi'=>$id));
	}
	
	
	public function get_filterdata($field)
    {
        $arr = array();

		$query = $this->db->query("SELECT * from ttransaksi as b   where b.nmtransaksi like '" . $field . "%' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function urlnumber()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
	
	
	public function datatransaksi()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT b.kdakundtl as kode,b.akundtl as akun,a.idakun,b.`satuan` FROM takun_transaksi AS a LEFT JOIN takundtl AS b ON a.`idakun` = b.`idakundtl` where flag='1'" );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	
		public function getjson()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT  column_name, column_type,column_comment FROM database_schema WHERE table_name =  'ttransaksi' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }

	
	public function mgetjsonshow($id)
    {
        $arr = array();


		$query = $this->db->query("SELECT * from ttransaksi  where idtransaksi = '$id'");	
        
		foreach($query->result_object() as $rows )
        {
		foreach ($query->list_fields() as $field)
			{
				$arr[$field] =$rows->$field ;
			}	   	
       }

        return  json_encode($arr);

    }
	
	public function get_datapopup($field)
    {
       $arr = array();
		
	$query = $this->db->query("SELECT a.idtransaksi ,a.nik,a.nmtransaksi  FROM  ttransaksi AS a  where (a.nmtransaksi like '" . $field . "%' or a.nik like '" . $field . "%')   limit 1000 ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function url()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
}
