<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class makun extends CI_Model {

	
	public function tampil($field)
	 {
		return  $this->db->query("SELECT * from takun where (akun like '" . $field . "%' or kdakun like '" . $field . "%') limit 1000 ");
  
    }
	public function idakun($kdakun)
	 {		 
		 $arr = array();
		
		$query = $this->db->query("SELECT count(*) as jml,idakun from takun where kdakun = '" . $kdakun . "' group by idakun");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);

  		 
    }
	
	function tampiledit($field){
		$arr = array();
		
		$query = $this->db->query("SELECT  a.idakundtl,a.idakun,a.kdakundtl,a.akundtl,a.db,a.ket FROM  takundtl AS a 
LEFT JOIN takun as b ON a.idakun = b.idakun   where a.idakun = '$field'");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
         return  "{\"data\":" .json_encode($arr). "}";
	}
	
	
	
	public function simpandtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->insert_batch('takundtl',$data);
  		 
    }
	public function updatedtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->update_batch('takundtl',$data,'idakundtl');
  		 
    }

	public function deletedtl1($id)
	{
		return $this->db->delete('takundtl', array('idakundtl' => $id));
	}
	
	public function hapusakun($id)
	{
		return $this->db->delete('takun', array('idakun' => $id));
	}
	
	public function editakun($id)
	{
		return $this->db->get_where('takun',array('idakun'=>$id));
	}
	
	
	public function get_filterdata($field)
    {
        $arr = array();

		$query = $this->db->query("SELECT * from takun as b   where b.nmakun like '" . $field . "%' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
		public function getjson()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT  column_name, column_type,column_comment FROM database_schema WHERE table_name =  'takun' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }

	
	public function mgetjsonshow($id)
    {
        $arr = array();


		$query = $this->db->query("SELECT * from takun as a where a.idakun = '$id'");	
        
		foreach($query->result_object() as $rows )
        {
		foreach ($query->list_fields() as $field)
			{
				$arr[$field] =$rows->$field ;
			}	   	
       }

        return  json_encode($arr);

    }
	
	public function url()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
}
