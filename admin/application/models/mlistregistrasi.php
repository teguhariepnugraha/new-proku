<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mlistregistrasi extends CI_Model {

		public function __construct() {
        parent::__construct();
    }
 function json($field)
	 {
		/*return $query = $this->db->query("SELECT a.idlistregistrasi,a.tgl,a.kdlistregistrasi,a.supplier,sum(b.total) as total,a.ket from tlistregistrasi as a left join tlistregistrasidtl as b on a.idlistregistrasi = b.idlistregistrasi where (kdlistregistrasi like '" . $field . "%' or supplier like '" . $field . "%' ) group by a.idlistregistrasi ");*/
		
$requestData= $_REQUEST;
$columns = array( 	
  0 => 'idlistregistrasi',
  1 => 'tgl',
  2 => 'kdlistregistrasi',
  3 => 'thn_pelajaran',
  4 => 'total',
  5 => 'ket',


);
		$sql = " SELECT a.idlistregistrasi,a.tgl,a.thn_ajaran,SUM(b.nominal) AS total,a.ket 
FROM tlistregistrasidtl AS b LEFT JOIN  tlistregistrasi AS a ON a.idlistregistrasi = b.idlistregistrasi 
LEFT JOIN takundtl AS d ON b.idakun = d.idakundtl LEFT JOIN takun_transaksi AS c ON d.idakun = c.idakun   where ( a.thn_ajaran like '" . $field . "%' ) and c.flag = '1' group by a.idlistregistrasi  ";

	$query =   $this->db->query($sql);
	$totalData = $query->num_rows();
	$totalFiltered = $totalData;
		
	if( !empty($requestData['search']['value']) ) {
	
	}
	
	$query =   $this->db->query($sql);
	$totalFiltered = $query->num_rows($sql);
		


	//----------------------------------------------------------------------------------
	
	$data = array();
	$x=0;
	 foreach($query->result_object() as $rows )
        {
			$x=$x+1;	  
		$nestedData=array(); 
					$nestedData[] = $x;
					$nestedData[] = $rows->tgl;
					

					$nestedData[] = $rows->thn_ajaran;
					$nestedData[] = number_format($rows->total, 2);
					$nestedData[] = $rows->ket;
					$nestedData[] =   "<div align='right'><a class='btn btn-info' href=editlistregistrasi/". $rows->idlistregistrasi ."  >
							  <i class='glyphicon glyphicon-edit icon-white'></i>
							  </a>
							  <a class='btn btn-danger' href=hapuslistregistrasi/". $rows->idlistregistrasi ." >
							  <i class='glyphicon glyphicon-trash icon-white'></i>
							  </a>
							  </div>";
		$data[] = $nestedData;
	}
	//----------------------------------------------------------------------------------
	$json_data = array(
		"draw"            => intval( $requestData['draw'] ), 
		"recordsTotal"    => intval( $totalData ), 
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data );
	//----------------------------------------------------------------------------------
	return  json_encode($json_data);

    }
	
	public function idlistregistrasi($kdlistregistrasi)
	 {		 
		 $arr = array();
		
		$query = $this->db->query("SELECT count(*) as jml,idlistregistrasi from tlistregistrasi where kdlistregistrasi = '" . $kdlistregistrasi . "' ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);

  		 
    }
	
	function tampiledit($field){
		$arr = array();
		
		$query = $this->db->query("SELECT  a.idlistregistrasidtl,a.idlistregistrasi,a.idakun,b.akundtl AS akun,a.nominal FROM  tlistregistrasidtl AS a 
LEFT JOIN takundtl AS b ON a.idakun = b.idakundtl LEFT JOIN takun_transaksi AS c ON b.idakun = c.idakun where a.idlistregistrasi = '$field' and c.flag = '1'");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
         return  "{\"data\":" .json_encode($arr). "}";
	}
	
	
	
	public function simpandtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->insert_batch('tlistregistrasidtl',$data);
  		 
    }
	public function updatedtl($data)
	 {
		 $data = json_decode( $data, true );
		 $query = $this->db->update_batch('tlistregistrasidtl',$data,'idlistregistrasidtl');
  		 return  json_encode($query);
    }

	public function deletedtl1($id)
	{
		return $this->db->delete('tlistregistrasidtl', array('idlistregistrasidtl' => $id));
	}
	
	public function hapuslistregistrasi($id)
	{
		return $this->db->delete('tlistregistrasi', array('idlistregistrasi' => $id));
	}
	
	public function editlistregistrasi($id)
	{
		return $this->db->get_where('tlistregistrasi',array('idlistregistrasi'=>$id));
	}
	
	
	public function get_filterdata($field)
    {
        $arr = array();

		$query = $this->db->query("SELECT * from tlistregistrasi as b   where b.nmlistregistrasi like '" . $field . "%' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function urlnumber()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
	
	
	public function datalistregistrasi()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT b.kdakundtl as kode,b.akundtl as akun,a.idakun,format(a.`pagu`,2) as nominal,a.thn_ajaran FROM takun_transaksi AS a LEFT JOIN takundtl AS b ON a.`idakun` = b.`idakundtl` where flag='1'" );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	
		public function getjson()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT  column_name, column_type,column_comment FROM database_schema WHERE table_name =  'tlistregistrasi' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }

	
	public function mgetjsonshow($id)
    {
        $arr = array();


		$query = $this->db->query("SELECT * from tlistregistrasi  where idlistregistrasi = '$id'");	
        
		foreach($query->result_object() as $rows )
        {
		foreach ($query->list_fields() as $field)
			{
				$arr[$field] =$rows->$field ;
			}	   	
       }

        return  json_encode($arr);

    }
	
	public function get_datapopup($field)
    {
       $arr = array();
		
	$query = $this->db->query("SELECT a.idlistregistrasi ,a.nik,a.nmlistregistrasi  FROM  tlistregistrasi AS a  where (a.nmlistregistrasi like '" . $field . "%' or a.nik like '" . $field . "%')   limit 1000 ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function url()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
}
