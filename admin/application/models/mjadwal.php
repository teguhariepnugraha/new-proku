<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mjadwal extends CI_Model {

	
	public function tampil($field)
	 {
		return  $this->db->query("SELECT a.idjadwal,a.tgl,b.nmkaryawan,TIME_FORMAT(a.masuk, '%H:%i') as masuk,TIME_FORMAT(a.keluar, '%H:%i') as keluar from tjadwal as a left join tkaryawan as b on a.idkaryawan = b.idkaryawan where ( nmkaryawan like '" . $field . "%')  limit 1000 ");
  
    }
	public function idjadwal($idkaryawan)
	 {		 
		 $arr = array();
		
		$query = $this->db->query("SELECT count(*) as jml,idjadwal from tjadwal where idkaryawan =  $idkaryawan group by idjadwal");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);

  		 
    }
	
	function tampiledit($field){
		$arr = array();
		
		$query = $this->db->query("SELECT  a.idjadwaldtl,a.idjadwal,a.idkelas,b.kelas,TIME_FORMAT(a.masuk, '%H:%i') as masuk,TIME_FORMAT(a.keluar, '%H:%i') as keluar,a.jmljam FROM  tjadwaldtl AS a 
LEFT JOIN tjadwal AS c ON a.idjadwal = c.idjadwal LEFT JOIN tkelas AS b ON a.idkelas = b.idkelas   where a.idjadwal = '$field'");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
         return  "{\"data\":" .json_encode($arr). "}";
	}
	
	
	
	public function simpandtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->insert_batch('tjadwaldtl',$data);
  		 
    }
	public function updatedtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->update_batch('tjadwaldtl',$data,'idjadwaldtl');
  		 
    }

	public function deletedtl1($id)
	{
		return $this->db->delete('tjadwaldtl', array('idjadwaldtl' => $id));
	}
	
	public function hapusjadwal($id)
	{
		return $this->db->delete('tjadwal', array('idjadwal' => $id));
	}
	
	public function editjadwal($id)
	{
		return $this->db->get_where('tjadwal',array('idjadwal'=>$id));
	}
	
	
	public function get_filterdata($field)
    {
        $arr = array();

		$query = $this->db->query("SELECT * from tjadwal as b   where b.nmjadwal like '" . $field . "%' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	public function datakelas()
    {
        $arr = array();
		
		 $query = $this->db->query("select kelas,idkelas from tkelas" );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	
		public function getjson()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT  column_name, column_type,column_comment FROM database_schema WHERE table_name =  'tjadwal' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }

	
	public function mgetjsonshow($id)
    {
        $arr = array();


		$query = $this->db->query("SELECT *,TIME_FORMAT(a.masuk, '%H:%i') as masuk,TIME_FORMAT(a.keluar, '%H:%i') as keluar from tjadwal as a left join tkaryawan as b on a.idkaryawan = b.idkaryawan where a.idjadwal = '$id'");	
        
		foreach($query->result_object() as $rows )
        {
		foreach ($query->list_fields() as $field)
			{
				$arr[$field] =$rows->$field ;
			}	   	
       }

        return  json_encode($arr);

    }
	
	public function get_datapopup($field)
    {
       $arr = array();
		
	$query = $this->db->query("SELECT a.idkaryawan ,a.nik,a.nmkaryawan  FROM  tkaryawan AS a  where (a.nmkaryawan like '" . $field . "%' or a.nik like '" . $field . "%')   limit 1000 ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function url()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
}
