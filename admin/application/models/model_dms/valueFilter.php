 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 class valueFilter extends CI_Model {

 	public function __construct () {
 		parent::__construct();
 		$this->dbDms=$this->load->database('dms',true);
 		//$this->load->model('mabsen');
 		
    		
  	} 

  	public function getAllBranch(){
  		  $arr = array();
  		  $sql="SELECT CompanyCode, BranchCode, OutletName
          FROM   pmBranchOutlets";
  		 
  		  $query=$this->dbDms->query($sql);
  		   return $query->result_array();
  		  

  	}


  	public function getAllTipe(){
  		  $arr = array();
  		 // $sql="select distinct(GroupCode) as tipe from pmGroupTypeSeq";

  		  $sql="select distinct groupmodel as tipe from DCAvSalesMstModelGroup where groupmodel not like '%AD%' and groupmodel not like '%02%'
and groupmodel not like '%EU4%' and groupmodel not like '%FE%' and groupmodel not like '%/BOX%' and groupmodel not like '%new jimny%' and groupmodel not like '%new xl-7%'";

  		 
  		  $query=$this->dbDms->query($sql);
  		   return $query->result_array();
  		  

  	}



  	public function getAllModel(){
  		  $arr = array();
  		  $sql="select distinct(TypeCode) as model from gnMstGroupModel";
  		 
  		  $query=$this->dbDms->query($sql);
  		   return $query->result_array();
  		  

  	}


  	public function getAllModelFilter($bulan){

  		 
  		 
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}

  	public function getAllSalesmanHead($bulan){
  		  
  		 
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}

  	public function getAllSalesman($bulan){
  		  
  		 
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}

  	public function getValueOfDO($bulan){
  		  	
  	   // $sql=$sql. $sqlwhere.$sqlEnd;

  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	  	public function getValueOfSPK($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function DOToSPK($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	 


  	public function getValueOfProspek($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}



  	public function getValueOfHotProspek($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}



  	 public function getValueOfInquiry($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}




  	public function getValueOfInquiryLost($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}



  	public function getValueOfGrowth($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function getValueOfContributionSource($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function getValueOfInquiryCaraBayar($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}

  	public function getValueOfInquiryLeasing($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function getValueOfInquiryToSpk($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}

  	public function getValueOfInquiryToDO($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function inquiryToProspek($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function inquiryLostToSPK($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function inquiryLostToDO($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function inquiryLostToProspek($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function inquiryToInquiryLost($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function DOHarian($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}

  	public function SPKHarian($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function FakturHarian($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function fakturPajak($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function salesmanTerbaik($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function salesheadTerbaik($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}

  	public function salescounterTerbaik($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


     public function getInquirySource($bulan){
        
        $query=$this->dbDms->query($bulan);
         return $query->result_array();
        

    }


  	public function getalldatabranchciawi($bulan){
  		$bulanfilter=date('m');
  		$year=date('Y');
  		$day=date('d');

  		$querygetalldatabranch="select id, bulan, tahun, count(distinct inquirynumber) as jumlah,'DO' as jenis_data, 'Ciawi' as cabang from ( ".
"SELECT concat(year(f.DODate),  Right('0'+Convert(varchar(10), month(f.DODate)),2)) as id, substring(datename(mm,f.DODate),0,4) as bulan, year(f.DODate) as tahun, ".
"a.inquirynumber FROM     pmKDP a LEFT JOIN ".
             "gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID LEFT JOIN ".
             "omTrSalesSO c ON c.CompanyCode = a.CompanyCode AND c.BranchCode = a.BranchCode AND c.ProspectNo = a.InquiryNumber LEFT JOIN ".
       "omTrSalesDO as F on c.SoNo=f.SONo /*left join ".
             "omTrSalesInvoice d ON d .CompanyCode = a.CompanyCode AND d .BranchCode = a.BranchCode AND d .SONo = c.SONo LEFT JOIN ".
             "omTrSalesReturn e ON e.CompanyCode = a.CompanyCode AND e.BranchCode = a.BranchCode AND e.InvoiceNo = d .InvoiceNo*/ ".
"WHERE   /*and */ a.LastProgress in ('do','Delivery') and (year(f.dodate)=".$year." and month(f.dodate)=".$bulanfilter." ) and (f.branchcode=".$bulan.")  and (f.status=2) ".
") as table1 group by id, bulan, tahun ". 

"union all ".

"SELECT   concat(year(lastUpdateStatus),  Right('0'+Convert(varchar(10), month(lastupdatestatus)),2)) as id, substring(datename(mm,lastupdatestatus),0,4) as bulan, year(lastupdatestatus) as tahun, ".
 "count(lastUpdateStatus) as jumlah,  'SPK' as jenis_data, 'Ciawi' as cabang from ( ".
 "SELECT ".
   "a.sodate lastupdatestatus, ".
   "b.SpvEmployeeID, ".
   "b.EmployeeID, ".
   "a.status, ".
   "a.branchcode, ".
   "b.tipekendaraan, ".
   "b.variant ".
 "from ".
   "omtrsalesso as a ".
   "left join pmkdp as b ".
   "on a.ProspectNo=b.InquiryNumber ".
 ") as table1 where (year(lastupdatestatus)=".$year." and month(lastupdatestatus)=".$bulanfilter.") and (branchcode=".$bulan.") group by ".
 " concat(year(lastUpdateStatus),  Right('0'+Convert(varchar(10), month(lastupdatestatus)),2)), substring(datename(mm,lastupdatestatus),0,4), year(lastupdatestatus) ".

 "union all ".

 "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'HP' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)) as id, substring(datename(mm,a.lastUpdateStatus),0,4) as bulan, year(a.lastUpdateStatus) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.lastUpdateStatus)=".$bulanfilter." and year(a.lastUpdateStatus)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)),substring(datename(mm,a.lastUpdateStatus),0,4),year(a.lastUpdateStatus), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber where table2.LastProgress in ('HP') group by id, bulan, tahun ".

 "union all ".

  "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'P' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)) as id, substring(datename(mm,a.lastUpdateStatus),0,4) as bulan, year(a.lastUpdateStatus) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.lastUpdateStatus)=".$bulanfilter." and year(a.lastUpdateStatus)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)),substring(datename(mm,a.lastUpdateStatus),0,4),year(a.lastUpdateStatus), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber where table2.LastProgress in ('P') group by id, bulan, tahun ".

"union all ".

  "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'LOST' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)) as id, substring(datename(mm,a.lastUpdateStatus),0,4) as bulan, year(a.lastUpdateStatus) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.lastUpdateStatus)=".$bulanfilter." and year(a.lastUpdateStatus)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)),substring(datename(mm,a.lastUpdateStatus),0,4),year(a.lastUpdateStatus), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber where table2.LastProgress in ('LOST') group by id, bulan, tahun ".

"union all ".

  "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'Inquiry' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.inquirydate),  Right('0'+Convert(varchar(10), month(a.inquirydate)),2)) as id, substring(datename(mm,a.inquirydate),0,4) as bulan, year(a.inquirydate) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.inquirydate)=".$bulanfilter." and year(a.inquirydate)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.inquirydate),  Right('0'+Convert(varchar(10), month(a.inquirydate)),2)),substring(datename(mm,a.inquirydate),0,4),year(a.inquirydate), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber /* where table2.LastProgress in ('LOST')*/ group by id, bulan, tahun ".


"union all ".

  "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'outstanding_spk' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)) as id, substring(datename(mm,a.lastUpdateStatus),0,4) as bulan, year(a.lastUpdateStatus) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.lastUpdateStatus)=".$bulanfilter." and year(a.lastUpdateStatus)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)),substring(datename(mm,a.lastUpdateStatus),0,4),year(a.lastUpdateStatus), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber where table2.LastProgress in ('SPK') group by id, bulan, tahun ".

  "union all ".

  "select concat(year(a.joborderdate),  Right('0'+Convert(varchar(10), month(a.joborderdate)),2)) as id, substring(datename(mm,a.joborderdate),0,4) as bulan, year(a.joborderdate) as tahun, count(a.branchcode) as jumlah, 'service_intake' as jenis_data, 'Ciawi' as cabang from ( Select * from svtrnservice where BranchCode=".$bulan." and (JobOrderDate between  convert(datetime,'".$year.$bulanfilter."01',120) and convert(datetime,'".$year.$bulanfilter.$day."', 120)) and servicetype=2 and ServiceStatus in ( '7', '9') and createdby not like '%BKS%') as a group by concat(year(a.joborderdate),  Right('0'+Convert(varchar(10), month(a.joborderdate)),2)) , substring(datename(mm,a.joborderdate),0,4) , year(a.joborderdate) ";




   			$query=$this->dbDms->query($querygetalldatabranch);
  		   return $query->result_array();
          

  	}


  	public function getalldatabranchcianjur($bulan){
  		$bulanfilter=date('m');
  		$year=date('Y');
  		$day=date('d');
  		

  		$querygetalldatabranch="select id, bulan, tahun, count(distinct inquirynumber) as jumlah,'DO' as jenis_data, 'Ciawi' as cabang from ( ".
"SELECT concat(year(f.DODate),  Right('0'+Convert(varchar(10), month(f.DODate)),2)) as id, substring(datename(mm,f.DODate),0,4) as bulan, year(f.DODate) as tahun, ".
"a.inquirynumber FROM     pmKDP a LEFT JOIN ".
             "gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID LEFT JOIN ".
             "omTrSalesSO c ON c.CompanyCode = a.CompanyCode AND c.BranchCode = a.BranchCode AND c.ProspectNo = a.InquiryNumber LEFT JOIN ".
       "omTrSalesDO as F on c.SoNo=f.SONo /*left join ".
             "omTrSalesInvoice d ON d .CompanyCode = a.CompanyCode AND d .BranchCode = a.BranchCode AND d .SONo = c.SONo LEFT JOIN ".
             "omTrSalesReturn e ON e.CompanyCode = a.CompanyCode AND e.BranchCode = a.BranchCode AND e.InvoiceNo = d .InvoiceNo*/ ".
"WHERE   /*and */ a.LastProgress in ('Delivery') and (year(f.dodate)=".$year." and month(f.dodate)=".$bulanfilter." ) and (f.branchcode=".$bulan.")  and (f.status=2) ".
") as table1 group by id, bulan, tahun ". 

"union all ".

"SELECT   concat(year(lastUpdateStatus),  Right('0'+Convert(varchar(10), month(lastupdatestatus)),2)) as id, substring(datename(mm,lastupdatestatus),0,4) as bulan, year(lastupdatestatus) as tahun, ".
 "count(lastUpdateStatus) as jumlah,  'SPK' as jenis_data, 'Ciawi' as cabang from ( ".
 "SELECT ".
   "a.sodate lastupdatestatus, ".
   "b.SpvEmployeeID, ".
   "b.EmployeeID, ".
   "a.status, ".
   "a.branchcode, ".
   "b.tipekendaraan, ".
   "b.variant ".
 "from ".
   "omtrsalesso as a ".
   "left join pmkdp as b ".
   "on a.ProspectNo=b.InquiryNumber ".
 ") as table1 where (year(lastupdatestatus)=".$year." and month(lastupdatestatus)=".$bulanfilter.") and (branchcode=".$bulan.") group by ".
 " concat(year(lastUpdateStatus),  Right('0'+Convert(varchar(10), month(lastupdatestatus)),2)), substring(datename(mm,lastupdatestatus),0,4), year(lastupdatestatus) ".

 "union all ".

 "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'HP' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)) as id, substring(datename(mm,a.lastUpdateStatus),0,4) as bulan, year(a.lastUpdateStatus) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.lastUpdateStatus)=".$bulanfilter." and year(a.lastUpdateStatus)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)),substring(datename(mm,a.lastUpdateStatus),0,4),year(a.lastUpdateStatus), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber where table2.LastProgress in ('HP') group by id, bulan, tahun ".

 "union all ".

  "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'P' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)) as id, substring(datename(mm,a.lastUpdateStatus),0,4) as bulan, year(a.lastUpdateStatus) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.lastUpdateStatus)=".$bulanfilter." and year(a.lastUpdateStatus)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)),substring(datename(mm,a.lastUpdateStatus),0,4),year(a.lastUpdateStatus), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber where table2.LastProgress in ('P') group by id, bulan, tahun ".

"union all ".

  "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'LOST' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)) as id, substring(datename(mm,a.lastUpdateStatus),0,4) as bulan, year(a.lastUpdateStatus) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.lastUpdateStatus)=".$bulanfilter." and year(a.lastUpdateStatus)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)),substring(datename(mm,a.lastUpdateStatus),0,4),year(a.lastUpdateStatus), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber where table2.LastProgress in ('LOST') group by id, bulan, tahun ".

"union all ".

  "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'Inquiry' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.inquirydate),  Right('0'+Convert(varchar(10), month(a.inquirydate)),2)) as id, substring(datename(mm,a.inquirydate),0,4) as bulan, year(a.inquirydate) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.inquirydate)=".$bulanfilter." and year(a.inquirydate)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.inquirydate),  Right('0'+Convert(varchar(10), month(a.inquirydate)),2)),substring(datename(mm,a.inquirydate),0,4),year(a.inquirydate), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber /* where table2.LastProgress in ('LOST')*/ group by id, bulan, tahun ".

  "union all ".

  "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'outstanding_spk' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)) as id, substring(datename(mm,a.lastUpdateStatus),0,4) as bulan, year(a.lastUpdateStatus) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.lastUpdateStatus)=".$bulanfilter." and year(a.lastUpdateStatus)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)),substring(datename(mm,a.lastUpdateStatus),0,4),year(a.lastUpdateStatus), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber where table2.LastProgress in ('SPK') group by id, bulan, tahun ".

  "union all ".

  "select concat(year(a.joborderdate),  Right('0'+Convert(varchar(10), month(a.joborderdate)),2)) as id, substring(datename(mm,a.joborderdate),0,4) as bulan, year(a.joborderdate) as tahun, count(a.branchcode) as jumlah, 'service_intake' as jenis_data, 'Ciawi' as cabang from ( Select * from svtrnservice where BranchCode=".$bulan." and (JobOrderDate between  convert(datetime,'".$year.$bulanfilter."01',120) and convert(datetime,'".$year.$bulanfilter.$day."', 120)) and servicetype=2 and ServiceStatus in ('7','9')) as a group by concat(year(a.joborderdate),  Right('0'+Convert(varchar(10), month(a.joborderdate)),2)) , substring(datename(mm,a.joborderdate),0,4) , year(a.joborderdate) ";





   			$query=$this->dbDms->query($querygetalldatabranch);
  		   return $query->result_array();
          

  	}


  	public function getalldatabranchcinere($bulan){
  		$bulanfilter=date('m');
  		$year=date('Y');
  		$day=date('d');

  		$querygetalldatabranch="select id, bulan, tahun, count(distinct inquirynumber) as jumlah,'DO' as jenis_data, 'Ciawi' as cabang from ( ".
"SELECT concat(year(f.DODate),  Right('0'+Convert(varchar(10), month(f.DODate)),2)) as id, substring(datename(mm,f.DODate),0,4) as bulan, year(f.DODate) as tahun, ".
"a.inquirynumber FROM     pmKDP a LEFT JOIN ".
             "gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID LEFT JOIN ".
             "omTrSalesSO c ON c.CompanyCode = a.CompanyCode AND c.BranchCode = a.BranchCode AND c.ProspectNo = a.InquiryNumber LEFT JOIN ".
       "omTrSalesDO as F on c.SoNo=f.SONo /*left join ".
             "omTrSalesInvoice d ON d .CompanyCode = a.CompanyCode AND d .BranchCode = a.BranchCode AND d .SONo = c.SONo LEFT JOIN ".
             "omTrSalesReturn e ON e.CompanyCode = a.CompanyCode AND e.BranchCode = a.BranchCode AND e.InvoiceNo = d .InvoiceNo*/ ".
"WHERE   /*and */ a.LastProgress in ('Delivery') and (year(f.dodate)=".$year." and month(f.dodate)=".$bulanfilter." ) and (f.branchcode=".$bulan.")  and (f.status=2) ".
") as table1 group by id, bulan, tahun ". 

"union all ".

"SELECT   concat(year(lastUpdateStatus),  Right('0'+Convert(varchar(10), month(lastupdatestatus)),2)) as id, substring(datename(mm,lastupdatestatus),0,4) as bulan, year(lastupdatestatus) as tahun, ".
 "count(lastUpdateStatus) as jumlah,  'SPK' as jenis_data, 'Ciawi' as cabang from ( ".
 "SELECT ".
   "a.sodate lastupdatestatus, ".
   "b.SpvEmployeeID, ".
   "b.EmployeeID, ".
   "a.status, ".
   "a.branchcode, ".
   "b.tipekendaraan, ".
   "b.variant ".
 "from ".
   "omtrsalesso as a ".
   "left join pmkdp as b ".
   "on a.ProspectNo=b.InquiryNumber ".
 ") as table1 where (year(lastupdatestatus)=".$year." and month(lastupdatestatus)=".$bulanfilter.") and (branchcode=".$bulan.") group by ".
 " concat(year(lastUpdateStatus),  Right('0'+Convert(varchar(10), month(lastupdatestatus)),2)), substring(datename(mm,lastupdatestatus),0,4), year(lastupdatestatus) ".

 "union all ".

 "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'HP' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)) as id, substring(datename(mm,a.lastUpdateStatus),0,4) as bulan, year(a.lastUpdateStatus) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.lastUpdateStatus)=".$bulanfilter." and year(a.lastUpdateStatus)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)),substring(datename(mm,a.lastUpdateStatus),0,4),year(a.lastUpdateStatus), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber where table2.LastProgress in ('HP') group by id, bulan, tahun ".

 "union all ".

  "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'P' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)) as id, substring(datename(mm,a.lastUpdateStatus),0,4) as bulan, year(a.lastUpdateStatus) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.lastUpdateStatus)=".$bulanfilter." and year(a.lastUpdateStatus)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)),substring(datename(mm,a.lastUpdateStatus),0,4),year(a.lastUpdateStatus), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber where table2.LastProgress in ('P') group by id, bulan, tahun ".

"union all ".

  "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'LOST' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)) as id, substring(datename(mm,a.lastUpdateStatus),0,4) as bulan, year(a.lastUpdateStatus) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.lastUpdateStatus)=".$bulanfilter." and year(a.lastUpdateStatus)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)),substring(datename(mm,a.lastUpdateStatus),0,4),year(a.lastUpdateStatus), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber where table2.LastProgress in ('LOST') group by id, bulan, tahun ".

"union all ".

  "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'Inquiry' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.inquirydate),  Right('0'+Convert(varchar(10), month(a.inquirydate)),2)) as id, substring(datename(mm,a.inquirydate),0,4) as bulan, year(a.inquirydate) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.inquirydate)=".$bulanfilter." and year(a.inquirydate)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.inquirydate),  Right('0'+Convert(varchar(10), month(a.inquirydate)),2)),substring(datename(mm,a.inquirydate),0,4),year(a.inquirydate), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber /* where table2.LastProgress in ('LOST')*/ group by id, bulan, tahun ".


  "union all ".

  "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'outstanding_spk' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)) as id, substring(datename(mm,a.lastUpdateStatus),0,4) as bulan, year(a.lastUpdateStatus) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.lastUpdateStatus)=".$bulanfilter." and year(a.lastUpdateStatus)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)),substring(datename(mm,a.lastUpdateStatus),0,4),year(a.lastUpdateStatus), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber where table2.LastProgress in ('SPK') group by id, bulan, tahun ".

  "union all ".

  "select concat(year(a.joborderdate),  Right('0'+Convert(varchar(10), month(a.joborderdate)),2)) as id, substring(datename(mm,a.joborderdate),0,4) as bulan, year(a.joborderdate) as tahun, count(a.branchcode) as jumlah, 'service_intake' as jenis_data, 'Ciawi' as cabang from ( Select * from svtrnservice where BranchCode=".$bulan." and (JobOrderDate between  convert(datetime,'".$year.$bulanfilter."01',120) and convert(datetime,'".$year.$bulanfilter.$day."', 120)) and servicetype=2 and ServiceStatus in ('7', '9') and createdby not like '%BKS%') as a group by concat(year(a.joborderdate),  Right('0'+Convert(varchar(10), month(a.joborderdate)),2)) , substring(datename(mm,a.joborderdate),0,4) , year(a.joborderdate) ";




   			$query=$this->dbDms->query($querygetalldatabranch);
  		   return $query->result_array();
          

  	}


  	public function getalldatabranchjatiasih($bulan){
  		$bulanfilter=date('m');
  		$year=date('Y');
  		$day=date('d');

  		$querygetalldatabranch="select id, bulan, tahun, count(distinct inquirynumber) as jumlah,'DO' as jenis_data, 'Ciawi' as cabang from ( ".
"SELECT concat(year(f.DODate),  Right('0'+Convert(varchar(10), month(f.DODate)),2)) as id, substring(datename(mm,f.DODate),0,4) as bulan, year(f.DODate) as tahun, ".
"a.inquirynumber FROM     pmKDP a LEFT JOIN ".
             "gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID LEFT JOIN ".
             "omTrSalesSO c ON c.CompanyCode = a.CompanyCode AND c.BranchCode = a.BranchCode AND c.ProspectNo = a.InquiryNumber LEFT JOIN ".
       "omTrSalesDO as F on c.SoNo=f.SONo /*left join ".
             "omTrSalesInvoice d ON d .CompanyCode = a.CompanyCode AND d .BranchCode = a.BranchCode AND d .SONo = c.SONo LEFT JOIN ".
             "omTrSalesReturn e ON e.CompanyCode = a.CompanyCode AND e.BranchCode = a.BranchCode AND e.InvoiceNo = d .InvoiceNo*/ ".
"WHERE   /*and */ a.LastProgress in ('Delivery') and (year(f.dodate)=".$year." and month(f.dodate)=".$bulanfilter." ) and (f.branchcode=".$bulan.")  and (f.status=2) ".
") as table1 group by id, bulan, tahun ". 

"union all ".

"SELECT   concat(year(lastUpdateStatus),  Right('0'+Convert(varchar(10), month(lastupdatestatus)),2)) as id, substring(datename(mm,lastupdatestatus),0,4) as bulan, year(lastupdatestatus) as tahun, ".
 "count(lastUpdateStatus) as jumlah,  'SPK' as jenis_data, 'Ciawi' as cabang from ( ".
 "SELECT ".
   "a.sodate lastupdatestatus, ".
   "b.SpvEmployeeID, ".
   "b.EmployeeID, ".
   "a.status, ".
   "a.branchcode, ".
   "b.tipekendaraan, ".
   "b.variant ".
 "from ".
   "omtrsalesso as a ".
   "left join pmkdp as b ".
   "on a.ProspectNo=b.InquiryNumber ".
 ") as table1 where (year(lastupdatestatus)=".$year." and month(lastupdatestatus)=".$bulanfilter.") and (branchcode=".$bulan.") group by ".
 " concat(year(lastUpdateStatus),  Right('0'+Convert(varchar(10), month(lastupdatestatus)),2)), substring(datename(mm,lastupdatestatus),0,4), year(lastupdatestatus) ".

 "union all ".

 "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'HP' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)) as id, substring(datename(mm,a.lastUpdateStatus),0,4) as bulan, year(a.lastUpdateStatus) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.lastUpdateStatus)=".$bulanfilter." and year(a.lastUpdateStatus)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)),substring(datename(mm,a.lastUpdateStatus),0,4),year(a.lastUpdateStatus), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber where table2.LastProgress in ('HP') group by id, bulan, tahun ".

 "union all ".

  "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'P' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)) as id, substring(datename(mm,a.lastUpdateStatus),0,4) as bulan, year(a.lastUpdateStatus) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.lastUpdateStatus)=".$bulanfilter." and year(a.lastUpdateStatus)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)),substring(datename(mm,a.lastUpdateStatus),0,4),year(a.lastUpdateStatus), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber where table2.LastProgress in ('P') group by id, bulan, tahun ".

"union all ".

  "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'LOST' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)) as id, substring(datename(mm,a.lastUpdateStatus),0,4) as bulan, year(a.lastUpdateStatus) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.lastUpdateStatus)=".$bulanfilter." and year(a.lastUpdateStatus)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)),substring(datename(mm,a.lastUpdateStatus),0,4),year(a.lastUpdateStatus), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber where table2.LastProgress in ('LOST') group by id, bulan, tahun ".

"union all ".

  "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'Inquiry' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.inquirydate),  Right('0'+Convert(varchar(10), month(a.inquirydate)),2)) as id, substring(datename(mm,a.inquirydate),0,4) as bulan, year(a.inquirydate) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.inquirydate)=".$bulanfilter." and year(a.inquirydate)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.inquirydate),  Right('0'+Convert(varchar(10), month(a.inquirydate)),2)),substring(datename(mm,a.inquirydate),0,4),year(a.inquirydate), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber /* where table2.LastProgress in ('LOST')*/ group by id, bulan, tahun ".


  "union all ".

  "select id, bulan, tahun, count(distinct table2.inquirynumber) as jumlah, 'outstanding_spk' as jenis_data, 'Ciawi' as cabang from ( SELECT   concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)) as id, substring(datename(mm,a.lastUpdateStatus),0,4) as bulan, year(a.lastUpdateStatus) as tahun, a.inquiryNumber FROM     pmKDP as a left join gnMstEmployee b ON a.CompanyCode = b.CompanyCode AND a.BranchCode = b.BranchCode AND a.EmployeeID = b.EmployeeID WHERE   /*and */ (month(a.lastUpdateStatus)=".$bulanfilter." and year(a.lastUpdateStatus)=".$year." ) and (a.branchcode=".$bulan.") group by concat(year(a.lastUpdateStatus),  Right('0'+Convert(varchar(10), month(a.lastUpdateStatus)),2)),substring(datename(mm,a.lastUpdateStatus),0,4),year(a.lastUpdateStatus), a.inquirynumber ) as table1 inner join pmstatushistory as table2 on table1.inquiryNumber=table2.inquirynumber where table2.LastProgress in ('SPK') group by id, bulan, tahun ".

  "union all ".

  "select concat(year(a.joborderdate),  Right('0'+Convert(varchar(10), month(a.joborderdate)),2)) as id, substring(datename(mm,a.joborderdate),0,4) as bulan, year(a.joborderdate) as tahun, count(a.branchcode) as jumlah, 'service_intake' as jenis_data, 'Ciawi' as cabang from ( Select * from svtrnservice where  (JobOrderDate between  convert(datetime,'".$year.$bulanfilter."01',120) and convert(datetime,'".$year.$bulanfilter.$day."', 120)) and servicetype=2 and ServiceStatus in ('7', '9') and CreatedBy like '%BKS%') as a group by concat(year(a.joborderdate),  Right('0'+Convert(varchar(10), month(a.joborderdate)),2)) , substring(datename(mm,a.joborderdate),0,4) , year(a.joborderdate) ";




   			$query=$this->dbDms->query($querygetalldatabranch);
  		   return $query->result_array();
          

  	}



  	public function getalldatabranchtargetdociawi($bulan){
  		
  		$year=date('Y');
  		if(date('m')==1){
  			$bulanfilter='jan';
  		}else if(date('m')==2){
  			$bulanfilter='feb';
  		}else if(date('m')==3){
  			$bulanfilter='mar';
  		}else if(date('m')==4){
  			$bulanfilter='apr';
  		}else if(date('m')==5){
  			$bulanfilter='mei';
  		}else if(date('m')==6){
  			$bulanfilter='jun';
  		}else if(date('m')==7){
  			$bulanfilter='jul';
  		}else if(date('m')==8){
  			$bulanfilter='agu';
  		}else if(date('m')==9){
  			$bulanfilter='sep';
  		}else if(date('m')==10){
  			$bulanfilter='okt';
  		}else if(date('m')==11){
  			$bulanfilter='nov';
  		}else if (date('m')==12){
  			$bulanfilter='des';
  		}


  		$querygetalldatabranch="select sum(".$bulanfilter.") as jumlah, 'targetdo' as jenisdata from targetdo where tahun='".$year."'"." and cabang='".$bulan."'
union ALL
select sum(".$bulanfilter.") as jumlah, 'targetunitentry' as jenisdata from targetunitentry where tahun='".$year."'"." and cabang='".$bulan."'";

$query=$this->db->query($querygetalldatabranch);
  		   return $query->result_array();


  	}


public function getalldatabranchtargetdojatiasih($bulan){
  		
  		$year=date('Y');
  		if(date('m')==1){
  			$bulanfilter='jan';
  		}else if(date('m')==2){
  			$bulanfilter='feb';
  		}else if(date('m')==3){
  			$bulanfilter='mar';
  		}else if(date('m')==4){
  			$bulanfilter='apr';
  		}else if(date('m')==5){
  			$bulanfilter='mei';
  		}else if(date('m')==6){
  			$bulanfilter='jun';
  		}else if(date('m')==7){
  			$bulanfilter='jul';
  		}else if(date('m')==8){
  			$bulanfilter='agu';
  		}else if(date('m')==9){
  			$bulanfilter='sep';
  		}else if(date('m')==10){
  			$bulanfilter='okt';
  		}else if(date('m')==11){
  			$bulanfilter='nov';
  		}else if (date('m')==12){
  			$bulanfilter='des';
  		}


  		$querygetalldatabranch="select sum(".$bulanfilter.") as jumlah, 'targetdo' as jenisdata from targetdo where tahun='".$year."'"." and cabang='".$bulan."'
union ALL
select sum(".$bulanfilter.") as jumlah, 'targetunitentry' as jenisdata from targetunitentry where tahun='".$year."'"." and cabang='".$bulan."'";

$query=$this->db->query($querygetalldatabranch);
  		   return $query->result_array();


  	}



 public function getalldatabranchtargetdocianjur($bulan){
  		
  		$year=date('Y');
  		if(date('m')==1){
  			$bulanfilter='jan';
  		}else if(date('m')==2){
  			$bulanfilter='feb';
  		}else if(date('m')==3){
  			$bulanfilter='mar';
  		}else if(date('m')==4){
  			$bulanfilter='apr';
  		}else if(date('m')==5){
  			$bulanfilter='mei';
  		}else if(date('m')==6){
  			$bulanfilter='jun';
  		}else if(date('m')==7){
  			$bulanfilter='jul';
  		}else if(date('m')==8){
  			$bulanfilter='agu';
  		}else if(date('m')==9){
  			$bulanfilter='sep';
  		}else if(date('m')==10){
  			$bulanfilter='okt';
  		}else if(date('m')==11){
  			$bulanfilter='nov';
  		}else if (date('m')==12){
  			$bulanfilter='des';
  		}


  		$querygetalldatabranch="select sum(".$bulanfilter.") as jumlah, 'targetdo' as jenisdata from targetdo where tahun='".$year."'"." and cabang='".$bulan."'
union ALL
select sum(".$bulanfilter.") as jumlah, 'targetunitentry' as jenisdata from targetunitentry where tahun='".$year."'"." and cabang='".$bulan."'";

$query=$this->db->query($querygetalldatabranch);
  		   return $query->result_array();


  	}



 public function getalldatabranchtargetdocinere($bulan){
  		
  		$year=date('Y');
  		if(date('m')==1){
  			$bulanfilter='jan';
  		}else if(date('m')==2){
  			$bulanfilter='feb';
  		}else if(date('m')==3){
  			$bulanfilter='mar';
  		}else if(date('m')==4){
  			$bulanfilter='apr';
  		}else if(date('m')==5){
  			$bulanfilter='mei';
  		}else if(date('m')==6){
  			$bulanfilter='jun';
  		}else if(date('m')==7){
  			$bulanfilter='jul';
  		}else if(date('m')==8){
  			$bulanfilter='agu';
  		}else if(date('m')==9){
  			$bulanfilter='sep';
  		}else if(date('m')==10){
  			$bulanfilter='okt';
  		}else if(date('m')==11){
  			$bulanfilter='nov';
  		}else if (date('m')==12){
  			$bulanfilter='des';
  		}


  		$querygetalldatabranch="select sum(".$bulanfilter.") as jumlah, 'targetdo' as jenisdata from targetdo where tahun='".$year."'"." and cabang='".$bulan."'
union ALL
select sum(".$bulanfilter.") as jumlah, 'targetunitentry' as jenisdata from targetunitentry where tahun='".$year."'"." and cabang='".$bulan."'";

$query=$this->db->query($querygetalldatabranch);
  		   return $query->result_array();


  	}




  public function getstockkendaraan(){
  	$bulanfilter=date('m');
  		$year=date('Y');

// 	$sql="select distinct year, month, groupmodel, salesmodeldesc, sum(jumlah) as total from (
// select year, month,GroupModel, salesmodelDesc, sum(endingAV) as jumlah from (
// SELECT CONVERT(Varchar,a.Year) as Year
//      , CASE WHEN a.Month = 1 THEN 'Januari'
//             WHEN a.Month = 2 THEN 'Februari'
//             WHEN a.Month = 3 THEN 'Maret' 
//             WHEN a.Month = 4 THEN 'April' 
//             WHEN a.Month = 5 THEN 'Mei' 
//             WHEN a.Month = 6 THEN 'Juni'
//             WHEN a.Month = 7 THEN 'Juli' 
//             WHEN a.Month = 8 THEN 'Agustus' 
//             WHEN a.Month = 9 THEN 'September'
//             WHEN a.Month = 10 THEN 'Oktober'
//             WHEN a.Month = 11 THEN 'November' 
//             WHEN a.Month = 12 THEN 'Desember'
//         END AS Month
//      , (select top 1 RefferenceDesc1
//           from DCA.dbo.omMstRefference
//          where CompanyCode = a.CompanyCode
//            and RefferenceType = 'WARE'
//            and RefferenceCode = a.WarehouseCode
//          ) as WareHouseName
//      , a.SalesModelCode
// 	 , b.groupcode
// 	 , d.GroupModel
//      , b.SalesModelDesc
//      , CONVERT(Varchar,a.SalesModelYear) as ModelYear
//      , (c.RefferenceCode + ' - ' + c.RefferenceDesc1) as ColourName
//      , a.BeginningAV
//      , a.QtyIn
//      , a.Alocation
//      , a.QtyOut
//      , a.EndingAV
//   FROM DCA.dbo.OmTrInventQtyVehicle a 
//  INNER JOIN DCA.dbo.omMstModel b
//     ON a.CompanyCode = b.CompanyCode
//    AND a.SalesModelCode = b.SalesModelCode 
//  INNER JOIN DCA.dbo.omMstRefference c
//     ON a.CompanyCode = c.CompanyCode  
//    AND a.ColourCode = c.RefferenceCode 
//  inner join DCA.dbo.DCAvSalesMstModelGroup d
//    on b.GroupCode=d.GroupCode
//  where a.year=".$year." and a.month=".$bulanfilter." and a.SalesModelYear=".$year."

// ) as table1 group by year, month, groupmodel, salesmodelDesc
// ) as table2 group by year, month, groupmodel, salesmodelDesc";

$sql="select  year, month, groupmodel, salesmodelcode, salesmodeldesc, sum(endingav) as total from(
select distinct * from (
SELECT CONVERT(Varchar,a.Year) as Year
     , CASE WHEN a.Month = 1 THEN 'Januari'
            WHEN a.Month = 2 THEN 'Februari'
            WHEN a.Month = 3 THEN 'Maret' 
            WHEN a.Month = 4 THEN 'April' 
            WHEN a.Month = 5 THEN 'Mei' 
            WHEN a.Month = 6 THEN 'Juni'
            WHEN a.Month = 7 THEN 'Juli' 
            WHEN a.Month = 8 THEN 'Agustus'
            WHEN a.Month = 9 THEN 'September' 
            WHEN a.Month = 10 THEN 'Oktober'
            WHEN a.Month = 11 THEN 'November' 
            WHEN a.Month = 12 THEN 'Desember' 
        END AS Month
     , (select top 1 RefferenceDesc1
          from DCA.dbo.omMstRefference
         where CompanyCode = a.CompanyCode
           and RefferenceType = 'WARE'
           and RefferenceCode = a.WarehouseCode
         ) as WareHouseName
     , a.SalesModelCode
     , b.SalesModelDesc
     , CONVERT(Varchar,a.SalesModelYear) as ModelYear
     , (c.RefferenceCode + ' - ' + c.RefferenceDesc1) as ColourName
     , a.BeginningAV
     , a.QtyIn
     , a.Alocation
     , a.QtyOut
     , a.EndingAV
   , e.GroupModel
  FROM DCA.dbo.OmTrInventQtyVehicle a 
 INNER JOIN DCA.dbo.omMstModel b
    ON a.CompanyCode = b.CompanyCode
   AND a.SalesModelCode = b.SalesModelCode 
 INNER JOIN DCA.dbo.omMstRefference c
    ON a.CompanyCode = c.CompanyCode  
   AND a.ColourCode = c.RefferenceCode 
 inner join ommstvehicle as d
   on a.salesmodelcode=d.salesmodelcode and a.WarehouseCode=d.WarehouseCode
 inner join DCA.dbo.DCAvSalesMstModelGroup e
   on b.GroupCode=e.GroupCode
WHERE a.year=".$year." and a.month=".$bulanfilter." and d.status=0 
) as table1
) as table2 group by  year, month, groupmodel, salesmodelcode, salesmodeldesc order by groupmodel

";

$query1=$this->dbDms->query($sql);
  		   return $query1->result_array();
}






 }
