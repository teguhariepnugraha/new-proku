<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mangsuran extends CI_Model {

		public function __construct() {
        parent::__construct();
    }
 function json($field)
	 {
		 $and =  $this->session->userdata('and');
		/*return $query = $this->db->query("SELECT a.idangsuran,a.tgl,a.nota,a.supplier,sum(b.total) as total,a.ket from tangsuran as a left join tangsurandtl as b on a.idangsuran = b.idangsuran where (nota like '" . $field . "%' or supplier like '" . $field . "%' ) group by a.idangsuran ");*/
		
$requestData= $_REQUEST;
$columns = array( 	
  0 => 'idangsuran',
  1 => 'tgl',
  2 => 'nota',
  3 => 'nik',
  4 => 'nama',
  5 => 'angsuran',

);
		$sql = " SELECT a.idangsuran,a.tgl,a.nota,c.nik,c.nama, SUM(b.angsuran) AS angsuran
FROM tangsuran AS a LEFT JOIN tangsurandtl AS b ON a.`idangsuran` = b.`idangsuran`
LEFT JOIN tpeserta AS c ON a.`idpeserta` = c.`idpeserta`  where (nota like '" . $field . "%' or nik like '" . $field . "%' or nama like '" . $field . "%' ) "  . $and . " group by a.idangsuran  ";

	$query =   $this->db->query($sql);
	$totalData = $query->num_rows();
	$totalFiltered = $totalData;
		
	if( !empty($requestData['search']['value']) ) {
	
	}
	
	$query =   $this->db->query($sql);
	$totalFiltered = $query->num_rows($sql);
		


	//----------------------------------------------------------------------------------
	
	$data = array();
	$x=0;
	 foreach($query->result_object() as $rows )
        {
			$x=$x+1;	  
		$nestedData=array(); 
					$nestedData[] = $x;
					$nestedData[] = $rows->tgl;
					$nestedData[] = $rows->nota;
					$nestedData[] = $rows->nik;
					$nestedData[] = $rows->nama;
					$nestedData[] = number_format($rows->angsuran, 2);
					$nestedData[] =   "<div align='right'><a class='btn btn-info' href=editangsuran/". $rows->idangsuran ."  >
							  <i class='glyphicon glyphicon-edit icon-white'></i>
							  </a>
							  <a class='btn btn-danger' href=hapusangsuran/". $rows->idangsuran ." >
							  <i class='glyphicon glyphicon-trash icon-white'></i>
							  </a>
							  </div>";
		$data[] = $nestedData;
	}
	//----------------------------------------------------------------------------------
	$json_data = array(
 		"draw"            => intval( $requestData['draw'] ),  
		"recordsTotal"    => intval( $totalData ), 
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data );
	//----------------------------------------------------------------------------------
	return  json_encode($json_data);

    }
	
	public function idangsuran($nota)
	 {		 
		 $arr = array();
		
		$query = $this->db->query("SELECT count(*) as jml,idangsuran from tangsuran where nota = '" . $nota . "' ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);

  		 
    }
	
	function tampiledit($field){
		$arr = array();
		
		$query = $this->db->query("SELECT  a.idangsurandtl,a.idangsuran,a.idakun,b.akundtl AS akun,a.angsuran FROM  tangsurandtl AS a 
LEFT JOIN tangsuran AS c ON a.idangsuran = c.idangsuran LEFT JOIN takundtl AS b ON a.idakun = b.idakundtl where c.idangsuran = '$field'");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
         return  "{\"data\":" .json_encode($arr). "}";
	}
	
	
	
	public function simpandtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->insert_batch('tangsurandtl',$data);
  		 
    }
	public function updatedtl($data)
	 {
		 $data = json_decode( $data, true );
		 $query = $this->db->update_batch('tangsurandtl',$data,'idangsurandtl');
  		 return  json_encode($query);
    }

	public function deletedtl1($id)
	{
		return $this->db->delete('tangsurandtl', array('idangsurandtl' => $id));
	}
	
	public function hapusangsuran($id)
	{
		return $this->db->delete('tangsuran', array('idangsuran' => $id));
	}
	
	public function editangsuran($id)
	{
		return $this->db->get_where('tangsuran',array('idangsuran'=>$id));
	}
	
	
	public function get_filterdata($field)
    {
        $arr = array();

		$query = $this->db->query("SELECT * from tangsuran as b   where b.nmangsuran like '" . $field . "%' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function urlnumber()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
	
	
	public function datapeserta()
    {
        $arr = array();
		
		 $query = $this->db->query("WITH RECURSIVE cte AS
( SELECT a.idpeserta,SUM(b.angsuran) AS angsuran FROM  tangsuran AS a LEFT JOIN tangsurandtl AS b ON a.`idangsuran` = b.`idangsuran` GROUP BY a.idpeserta)
SELECT DISTINCT c.idpeserta,c.nik,c.nama,FORMAT(a.angsuran,2) AS angsuran,FORMAT(SUM(b.nominal),2) AS piutang, FORMAT(SUM(b.nominal) - a.angsuran,2) AS sisa FROM tpeserta  AS c LEFT JOIN cte AS a ON c.`idpeserta` = a.idpeserta LEFT JOIN tlistpembayaran AS b ON a.idpeserta = b.idpeserta" );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	public function dataangsuran($field)
    {
        $arr = array();
		
		 $query = $this->db->query("WITH RECURSIVE cte AS
(
 SELECT b.idakun,a.idpeserta,SUM(b.angsuran) AS angsuran FROM  tangsuran AS a LEFT JOIN tangsurandtl AS b ON a.`idangsuran` = b.`idangsuran` GROUP BY b.idakun,a.`idpeserta`
)
SELECT c.idakun,e.kdakundtl AS kode, e.akundtl AS akun, c.idpeserta,FORMAT(a.angsuran,2) AS angsuran,FORMAT(c.nominal,2) AS piutang ,
FORMAT(c.nominal-a.angsuran,2) AS sisa FROM tlistpembayaran  AS c LEFT JOIN cte AS a ON c.`idakun` = a.idakun 
LEFT JOIN takundtl AS e ON a.`idakun` = e.`idakundtl` WHERE c.idpeserta = $field GROUP BY a.idakun" );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
		public function getjson()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT  column_name, column_type,column_comment FROM database_schema WHERE table_name =  'tangsuran' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }

	
	public function mgetjsonshow($id)
    {
        $arr = array();


		$query = $this->db->query("SELECT * from tangsuran  where idangsuran = '$id'");	
        
		foreach($query->result_object() as $rows )
        {
		foreach ($query->list_fields() as $field)
			{
				$arr[$field] =$rows->$field ;
			}	   	
       }

        return  json_encode($arr);

    }
	
	public function get_datapopup($field)
    {
       $arr = array();
		
	$query = $this->db->query("SELECT a.idangsuran ,a.nik,a.nmangsuran  FROM  tangsuran AS a  where (a.nmangsuran like '" . $field . "%' or a.nik like '" . $field . "%')   limit 1000 ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function url()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
}
