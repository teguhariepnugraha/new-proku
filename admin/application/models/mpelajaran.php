<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mpelajaran extends CI_Model {

	
	public function tampil($field)
	 {
		return  $this->db->query("SELECT * from tpelajaran where (nmpelajaran like '" . $field . "%' or kdpelajaran like '" . $field . "%') limit 1000 ");
  
    }
	public function idpelajaran($kdpelajaran)
	 {		 
		 $arr = array();
		
		$query = $this->db->query("SELECT count(*) as jml,idpelajaran from tpelajaran where kdpelajaran = '" . $kdpelajaran . "' group by idpelajaran");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);

  		 
    }
	
	function tampiledit($field){
		$arr = array();
		
		$query = $this->db->query("SELECT  a.idpelajarandtl,a.bab FROM  tpelajarandtl AS a 
LEFT JOIN tpelajaran as b ON a.idpelajaran = b.idpelajaran   where a.idpelajaran = '$field'");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
	}
	
	
	
	public function simpandtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->insert_batch('tpelajarandtl',$data);
  		 
    }
	public function updatedtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->update_batch('tpelajarandtl',$data,'idpelajarandtl');
  		 
    }

	public function deletedtl1($id)
	{
		return $this->db->delete('tpelajarandtl', array('idpelajarandtl' => $id));
	}
	
	public function hapuspelajaran($id)
	{
		return $this->db->delete('tpelajaran', array('idpelajaran' => $id));
	}
	
	public function editpelajaran($id)
	{
		return $this->db->get_where('tpelajaran',array('idpelajaran'=>$id));
	}
	
	
	public function get_filterdata($field)
    {
        $arr = array();

		$query = $this->db->query("SELECT * from tpelajaran as b   where b.nmpelajaran like '" . $field . "%' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
		public function getjson()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT  column_name, column_type,column_comment FROM database_schema WHERE table_name =  'tpelajaran' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }

	
	public function mgetjsonshow($id)
    {
        $arr = array();


		$query = $this->db->query("SELECT * from tpelajaran as a where a.idpelajaran = '$id'");	
        
		foreach($query->result_object() as $rows )
        {
		foreach ($query->list_fields() as $field)
			{
				$arr[$field] =$rows->$field ;
			}	   	
       }

        return  json_encode($arr);

    }
	
	public function url()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
}
