<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mIncentiveService extends CI_Model {

	public function __construct() {
        parent::__construct();
        $this->dbDms=$this->load->database('dms',true);
    }



	public function getfullIncentiveChemical($branchcode, $year, $month)
	 {
	 	$sql1="";
	 if($branchcode=='641940101'){
	 	$sql1="--chemical

select employeename, partname, jumlah, total,totalmargin, cast(round(((jumlah*100.0)/total),4) as decimal(10,4)) as persentase
from (
select employeename, partname, count(partname) as jumlah,
(
select count(distinct a.invoiceno) as total

from 
svTrninvoice as a 
inner join 
svTrninvItem as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
inner join
svtrnservice as e on a.branchcode=e.branchcode and a.JobOrderNo=e.JobOrderNo
left join gnMstEmployee as f on a.BranchCode=f.BranchCode and e.ForemanID=f.EmployeeID
left join spMstItemInfo as c on a.CompanyCode=c.CompanyCode  and b.PartNo=c.partno
left join gnMstSupplier as d on c.CompanyCode=d.CompanyCode and c.SupplierCode=d.SupplierCode
left join gnMstCustomer as g on a.CustomerCodebill=g.CustomerCode
left join gnmstemployee as emp on e.BranchCode=emp.BranchCode and e.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
where a.branchcode=641940101
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.") 
and c.partname =table1.partname and left(a.invoiceno,3)<>'INI' --And g.customername not like '%DUTA CENDANA ADIMANDIRI%'
and emp.EmployeeName<>'syarifudin'

) as total,

(
select sum(((b.supplyQty*b.RetailPrice)-cast((b.discpct*(b.supplyQty*retailprice)/100) as int))  - (b.supplyQty*b.CostPrice)) as totalmargin

from 
svTrninvoice as a 
inner join 
svTrninvItem as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
inner join
svtrnservice as e on a.branchcode=e.branchcode and a.JobOrderNo=e.JobOrderNo
left join gnMstEmployee as f on a.BranchCode=f.BranchCode and e.ForemanID=f.EmployeeID
left join spMstItemInfo as c on a.CompanyCode=c.CompanyCode  and b.PartNo=c.partno
left join gnMstSupplier as d on c.CompanyCode=d.CompanyCode and c.SupplierCode=d.SupplierCode
left join gnMstCustomer as g on a.CustomerCodeBill=g.CustomerCode
left join gnmstemployee as emp on e.BranchCode=emp.BranchCode and e.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
where a.branchcode=641940101
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")  
and c.partname =table1.partname and left(a.invoiceno,3)<>'INI' --and g.customername not like '%DUTA CENDANA ADIMANDIRI%'
and emp.EmployeeName<>'syarifudin'

) as totalmargin




from(

select a.companycode, a.branchcode, a.ProductType, a.invoiceno, a.invoicedate, a.fpjno, a.fpjdate, a.JobOrderNo, a.JobOrderdate, a.PoliceRegNo,
g.Customername,
e.ForemanID,
f.EmployeeName,
b.PartNo,
c.PartName,
b.RetailPrice,
b.supplyQty,
CAST((b.RetailPrice  * a.PpnPct/100) as int) as ppn,
CAST((b.retailprice + (b.RetailPrice  *a.PpnPct/100)) as int) as total,
b.CostPrice,
(((b.supplyQty*b.RetailPrice)-cast((b.discpct*(b.supplyQty*retailprice)/100) as int))  - (b.supplyQty*b.CostPrice)) as margin,

d.SupplierName

from 
svTrninvoice as a 
inner join 
svTrninvItem as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
inner join
svtrnservice as e on a.branchcode=e.branchcode and a.JobOrderNo=e.JobOrderNo
left join gnMstEmployee as f on a.BranchCode=f.BranchCode and e.ForemanID=f.EmployeeID
left join spMstItemInfo as c on a.CompanyCode=c.CompanyCode  and b.PartNo=c.partno
left join gnMstSupplier as d on c.CompanyCode=d.CompanyCode and c.SupplierCode=d.SupplierCode
left join gnMstCustomer as g on a.CustomerCodeBill=g.CustomerCode
left join gnmstemployee as emp on e.BranchCode=emp.BranchCode and e.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
where a.branchcode=641940101
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")  
and c.partno in ('AL-DESINFEKTAN','AL-ENGINE COND','AL-ENGINE COND.','AL-ENGINE FLUSH','AL-ENGINE FLUSH GM') and left(a.invoiceno,3)<>'INI' --and g.CustomerName not like '%DUTA CENDANA ADIMANDIRI%'
--order by a.joborderno
and emp.EmployeeName<>'syarifudin'
) as table1 group by employeename, partname
) as table2
";

$query=$this->dbDms->query($sql1);
		return $query->result_array();


	 }else if($branchcode=='641940104'){
$sql1="--chemical

select employeename, partname, jumlah, total,totalmargin, cast(round(((jumlah*100.0)/total),4) as decimal(10,4)) as persentase
from (
select employeename, partname, count(partname) as jumlah,
(
select count(distinct a.invoiceno) as total

from 
svTrninvoice as a 
inner join 
svTrninvItem as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
inner join
svtrnservice as e on a.branchcode=e.branchcode and a.JobOrderNo=e.JobOrderNo
left join gnMstEmployee as f on a.BranchCode=f.BranchCode and e.ForemanID=f.EmployeeID
left join spMstItemInfo as c on a.CompanyCode=c.CompanyCode  and b.PartNo=c.partno
left join gnMstSupplier as d on c.CompanyCode=d.CompanyCode and c.SupplierCode=d.SupplierCode
left join gnMstCustomer as g on a.CustomerCodebill=g.CustomerCode
left join gnmstemployee as emp on e.BranchCode=emp.BranchCode and e.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
where a.branchcode=641940104
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.") 
and c.partname =table1.partname and left(a.invoiceno,3)<>'INI' --And g.customername not like '%DUTA CENDANA ADIMANDIRI%'
--and emp.EmployeeName='syarifudin'

) as total,

(
select sum(((b.supplyQty*b.RetailPrice)-cast((b.discpct*(b.supplyQty*retailprice)/100) as int))  - (b.supplyQty*b.CostPrice)) as totalmargin

from 
svTrninvoice as a 
inner join 
svTrninvItem as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
inner join
svtrnservice as e on a.branchcode=e.branchcode and a.JobOrderNo=e.JobOrderNo
left join gnMstEmployee as f on a.BranchCode=f.BranchCode and e.ForemanID=f.EmployeeID
left join spMstItemInfo as c on a.CompanyCode=c.CompanyCode  and b.PartNo=c.partno
left join gnMstSupplier as d on c.CompanyCode=d.CompanyCode and c.SupplierCode=d.SupplierCode
left join gnMstCustomer as g on a.CustomerCodeBill=g.CustomerCode
left join gnmstemployee as emp on e.BranchCode=emp.BranchCode and e.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
where a.branchcode=641940104
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")  
and c.partname =table1.partname and left(a.invoiceno,3)<>'INI' --and g.customername not like '%DUTA CENDANA ADIMANDIRI%'
--and emp.EmployeeName='syarifudin'

) as totalmargin




from(

select a.companycode, a.branchcode, a.ProductType, a.invoiceno, a.invoicedate, a.fpjno, a.fpjdate, a.JobOrderNo, a.JobOrderdate, a.PoliceRegNo,
g.Customername,
e.ForemanID,
f.EmployeeName,
b.PartNo,
c.PartName,
b.RetailPrice,
b.supplyQty,
CAST((b.RetailPrice  * a.PpnPct/100) as int) as ppn,
CAST((b.retailprice + (b.RetailPrice  *a.PpnPct/100)) as int) as total,
b.CostPrice,
(((b.supplyQty*b.RetailPrice)-cast((b.discpct*(b.supplyQty*retailprice)/100) as int))  - (b.supplyQty*b.CostPrice)) as margin,

d.SupplierName

from 
svTrninvoice as a 
inner join 
svTrninvItem as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
inner join
svtrnservice as e on a.branchcode=e.branchcode and a.JobOrderNo=e.JobOrderNo
left join gnMstEmployee as f on a.BranchCode=f.BranchCode and e.ForemanID=f.EmployeeID
left join spMstItemInfo as c on a.CompanyCode=c.CompanyCode  and b.PartNo=c.partno
left join gnMstSupplier as d on c.CompanyCode=d.CompanyCode and c.SupplierCode=d.SupplierCode
left join gnMstCustomer as g on a.CustomerCodeBill=g.CustomerCode
left join gnmstemployee as emp on e.BranchCode=emp.BranchCode and e.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
where a.branchcode=641940104
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")  
and c.partno in ('AL-DESINFEKTAN','AL-ENGINE COND','AL-ENGINE COND.','AL-ENGINE FLUSH','AL-ENGINE FLUSH GM') and left(a.invoiceno,3)<>'INI' --and g.CustomerName not like '%DUTA CENDANA ADIMANDIRI%'
--order by a.joborderno
--and emp.EmployeeName='syarifudin'
) as table1 group by employeename, partname


--- invoice ciawi

union all

select employeename, partname, count(partname) as jumlah,
(
select count(distinct a.invoiceno) as total

from 
svTrninvoice as a 
inner join 
svTrninvItem as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
inner join
svtrnservice as e on a.branchcode=e.branchcode and a.JobOrderNo=e.JobOrderNo
left join gnMstEmployee as f on a.BranchCode=f.BranchCode and e.ForemanID=f.EmployeeID
left join spMstItemInfo as c on a.CompanyCode=c.CompanyCode  and b.PartNo=c.partno
left join gnMstSupplier as d on c.CompanyCode=d.CompanyCode and c.SupplierCode=d.SupplierCode
left join gnMstCustomer as g on a.CustomerCodebill=g.CustomerCode
left join gnmstemployee as emp on e.BranchCode=emp.BranchCode and e.ForemanID=emp.EmployeeID --and emp.PersonnelStatus=1
where a.branchcode=641940101
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.") 
and c.partname =table1.partname and left(a.invoiceno,3)<>'INI' --And g.customername not like '%DUTA CENDANA ADIMANDIRI%'
and emp.EmployeeName='syarifudin'

) as total,

(
select sum(((b.supplyQty*b.RetailPrice)-cast((b.discpct*(b.supplyQty*retailprice)/100) as int))  - (b.supplyQty*b.CostPrice)) as totalmargin

from 
svTrninvoice as a 
inner join 
svTrninvItem as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
inner join
svtrnservice as e on a.branchcode=e.branchcode and a.JobOrderNo=e.JobOrderNo
left join gnMstEmployee as f on a.BranchCode=f.BranchCode and e.ForemanID=f.EmployeeID
left join spMstItemInfo as c on a.CompanyCode=c.CompanyCode  and b.PartNo=c.partno
left join gnMstSupplier as d on c.CompanyCode=d.CompanyCode and c.SupplierCode=d.SupplierCode
left join gnMstCustomer as g on a.CustomerCodeBill=g.CustomerCode
left join gnmstemployee as emp on e.BranchCode=emp.BranchCode and e.ForemanID=emp.EmployeeID --and emp.PersonnelStatus=1
where a.branchcode=641940101
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")  
and c.partname =table1.partname and left(a.invoiceno,3)<>'INI' --and g.customername not like '%DUTA CENDANA ADIMANDIRI%'
and emp.EmployeeName='syarifudin'

) as totalmargin




from(

select a.companycode, a.branchcode, a.ProductType, a.invoiceno, a.invoicedate, a.fpjno, a.fpjdate, a.JobOrderNo, a.JobOrderdate, a.PoliceRegNo,
g.Customername,
e.ForemanID,
f.EmployeeName,
b.PartNo,
c.PartName,
b.RetailPrice,
b.supplyQty,
CAST((b.RetailPrice  * a.PpnPct/100) as int) as ppn,
CAST((b.retailprice + (b.RetailPrice  *a.PpnPct/100)) as int) as total,
b.CostPrice,
(((b.supplyQty*b.RetailPrice)-cast((b.discpct*(b.supplyQty*retailprice)/100) as int))  - (b.supplyQty*b.CostPrice)) as margin,

d.SupplierName

from 
svTrninvoice as a 
inner join 
svTrninvItem as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
inner join
svtrnservice as e on a.branchcode=e.branchcode and a.JobOrderNo=e.JobOrderNo
left join gnMstEmployee as f on a.BranchCode=f.BranchCode and e.ForemanID=f.EmployeeID
left join spMstItemInfo as c on a.CompanyCode=c.CompanyCode  and b.PartNo=c.partno
left join gnMstSupplier as d on c.CompanyCode=d.CompanyCode and c.SupplierCode=d.SupplierCode
left join gnMstCustomer as g on a.CustomerCodeBill=g.CustomerCode
left join gnmstemployee as emp on e.BranchCode=emp.BranchCode and e.ForemanID=emp.EmployeeID --and emp.PersonnelStatus=1
where a.branchcode=641940101
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")  
and c.partno in ('AL-DESINFEKTAN','AL-ENGINE COND','AL-ENGINE COND.','AL-ENGINE FLUSH','AL-ENGINE FLUSH GM') and left(a.invoiceno,3)<>'INI' --and g.CustomerName not like '%DUTA CENDANA ADIMANDIRI%'
--order by a.joborderno
and emp.EmployeeName='syarifudin'
) as table1 group by employeename, partname


--- end invoice ciawi

) as table2
";

$query=$this->dbDms->query($sql1);
		return $query->result_array();

	 }else{

	 

		 $sql1 = "select employeename, partname, jumlah, total,totalmargin, cast(round(((jumlah*100.0)/total),4) as decimal(10,4)) as persentase
from (
select employeename, partname, count(partname) as jumlah,
(
select count(distinct a.invoiceno) as total

from 
svTrninvoice as a 
inner join 
svTrninvItem as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
inner join
svtrnservice as e on a.branchcode=e.branchcode and a.JobOrderNo=e.JobOrderNo
left join gnMstEmployee as f on a.BranchCode=f.BranchCode and e.ForemanID=f.EmployeeID
left join spMstItemInfo as c on a.CompanyCode=c.CompanyCode  and b.PartNo=c.partno
left join gnMstSupplier as d on c.CompanyCode=d.CompanyCode and c.SupplierCode=d.SupplierCode
left join gnMstCustomer as g on a.CustomerCodebill=g.CustomerCode
where a.branchcode=".$branchcode."
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.") 
and c.partname =table1.partname and left(a.invoiceno,3)<>'INI' --And g.customername not like '%DUTA CENDANA ADIMANDIRI%'

) as total,

(
select sum(((b.supplyQty*b.RetailPrice)-cast((b.discpct*(b.supplyQty*retailprice)/100) as int))  - (b.supplyQty*b.CostPrice)) as totalmargin

from 
svTrninvoice as a 
inner join 
svTrninvItem as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
inner join
svtrnservice as e on a.branchcode=e.branchcode and a.JobOrderNo=e.JobOrderNo
left join gnMstEmployee as f on a.BranchCode=f.BranchCode and e.ForemanID=f.EmployeeID
left join spMstItemInfo as c on a.CompanyCode=c.CompanyCode  and b.PartNo=c.partno
left join gnMstSupplier as d on c.CompanyCode=d.CompanyCode and c.SupplierCode=d.SupplierCode
left join gnMstCustomer as g on a.CustomerCodeBill=g.CustomerCode
where a.branchcode=".$branchcode."
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")  
and c.partname =table1.partname and left(a.invoiceno,3)<>'INI'--and g.customername not like '%DUTA CENDANA ADIMANDIRI%'

) as totalmargin




from(

select a.companycode, a.branchcode, a.ProductType, a.invoiceno, a.invoicedate, a.fpjno, a.fpjdate, a.JobOrderNo, a.JobOrderdate, a.PoliceRegNo,
g.Customername,
e.ForemanID,
f.EmployeeName,
b.PartNo,
c.PartName,
b.RetailPrice,
b.supplyQty,
CAST((b.RetailPrice  * a.PpnPct/100) as int) as ppn,
CAST((b.retailprice + (b.RetailPrice  *a.PpnPct/100)) as int) as total,
b.CostPrice,
(((b.supplyQty*b.RetailPrice)-cast((b.discpct*(b.supplyQty*retailprice)/100) as int))  - (b.supplyQty*b.CostPrice)) as margin,

d.SupplierName

from 
svTrninvoice as a 
inner join 
svTrninvItem as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
inner join
svtrnservice as e on a.branchcode=e.branchcode and a.JobOrderNo=e.JobOrderNo
left join gnMstEmployee as f on a.BranchCode=f.BranchCode and e.ForemanID=f.EmployeeID
left join spMstItemInfo as c on a.CompanyCode=c.CompanyCode  and b.PartNo=c.partno
left join gnMstSupplier as d on c.CompanyCode=d.CompanyCode and c.SupplierCode=d.SupplierCode
left join gnMstCustomer as g on a.CustomerCodeBill=g.CustomerCode
where a.branchcode=".$branchcode."
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")  
and c.partno in ('AL-DESINFEKTAN','AL-ENGINE COND','AL-ENGINE COND.','AL-ENGINE FLUSH','AL-ENGINE FLUSH GM') and left(a.invoiceno,3)<>'INI' --and g.CustomerName not like '%DUTA CENDANA ADIMANDIRI%'
--order by a.joborderno
) as table1 group by employeename, partname
) as table2";

//print $sql1;
		
 $query=$this->dbDms->query($sql1);
		return $query->result_array();
// return $sql;
  
    }
}



 public function getfullincentiveAc($branchcode, $Year, $month){
    //if($branchcode=='641940101'){

        $sql="Select count(distinct invoiceno) [Total Invoice], sum(margin) as totalmargin from
(
select distinct a.companycode, a.branchcode, a.ProductType, a.invoiceno, a.invoicedate, a.fpjno, a.fpjdate, a.JobOrderNo, a.JobOrderdate, a.PoliceRegNo, 
d.customername,
b.OperationNo, 
c.Description,
b.OperationCost,
b.SubConPrice,
(b.OperationCost - b.SubConPrice) as margin,
emp.employeename

from 
svTrninvoice as a 
inner join 
svTrninvTask as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
left join svMstTask as c on a.BasicModel=c.BasicModel  and b.OperationNo=c.OperationNo
left join gnMstCustomer as d on a.CustomerCode=d.CustomerCode
left join svtrnservice as sv on a.branchcode=sv.branchcode and a.JobOrderNo=sv.JobOrderNo
left join gnMstEmployee as emp on sv.BranchCode=emp.BranchCode and sv.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
where a.branchcode=641940101
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.") and left(a.invoiceno,3)<>'INI' 
and (c.description like '%AC%' )
--and emp.EmployeeName<>'syarifudin'
--and sv.foremanid<>'061902008'

) as table2";


    //$query=$this->dbDms->query($sql);
        //return $query->result_array();

    //} end if
 }




    public function getFullIncentiveSpooring($branchcode, $year, $month)
    {

    if($branchcode=='641940101'){

    	$sql="Select count(distinct invoiceno) [Total Invoice], sum(margin) as totalmargin from
(
select distinct a.companycode, a.branchcode, a.ProductType, a.invoiceno, a.invoicedate, a.fpjno, a.fpjdate, a.JobOrderNo, a.JobOrderdate, a.PoliceRegNo, 
d.customername,
b.OperationNo, 
c.Description,
b.OperationCost,
b.SubConPrice,
((b.operationhour*b.OperationCost) - b.SubConPrice) as margin,
emp.employeename

from 
svTrninvoice as a 
inner join 
svTrninvTask as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
left join svMstTask as c on a.BasicModel=c.BasicModel  and b.OperationNo=c.OperationNo
left join gnMstCustomer as d on a.CustomerCode=d.CustomerCode
left join svtrnservice as sv on a.branchcode=sv.branchcode and a.JobOrderNo=sv.JobOrderNo
left join gnMstEmployee as emp on sv.BranchCode=emp.BranchCode and sv.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
where a.branchcode=641940101
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.") and left(a.invoiceno,3)<>'INI' 
and (c.description like '%spooring%' or c.description like '%BALANCING%')
--and emp.EmployeeName<>'syarifudin'
and sv.foremanid<>'061902008'

) as table2";

	$query=$this->dbDms->query($sql);
		return $query->result_array();


    }else if($branchcode=='641940104'){

    	$sql="Select count(distinct invoiceno) [Total Invoice], sum(margin) as totalmargin from
(
select a.companycode, a.branchcode, a.ProductType, a.invoiceno, a.invoicedate, a.fpjno, a.fpjdate, a.JobOrderNo, a.JobOrderdate, a.PoliceRegNo, 
d.customername,
b.OperationNo, 
c.Description,
b.OperationCost,
b.SubConPrice,
((b.operationhour*b.OperationCost) - b.SubConPrice) as margin,
emp.employeename

from 
svTrninvoice as a 
inner join 
svTrninvTask as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
left join svMstTask as c on a.BasicModel=c.BasicModel  and b.OperationNo=c.OperationNo
left join gnMstCustomer as d on a.CustomerCode=d.CustomerCode
left join svtrnservice as sv on a.branchcode=sv.branchcode and a.JobOrderNo=sv.JobOrderNo
left join gnMstEmployee as emp on sv.BranchCode=emp.BranchCode and sv.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
where a.branchcode=641940104
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.") and left(a.invoiceno,3)<>'INI' 
and (c.description like '%spooring%' or c.description like '%BALANCING%')
--and emp.EmployeeName='syarifudin'
--and sv.foremanid='061902008'

---- balancing ciawi

union all

select a.companycode, a.branchcode, a.ProductType, a.invoiceno, a.invoicedate, a.fpjno, a.fpjdate, a.JobOrderNo, a.JobOrderdate, a.PoliceRegNo, 
d.customername,
b.OperationNo, 
c.Description,
b.OperationCost,
b.SubConPrice,
((b.operationhour*b.OperationCost) - b.SubConPrice) as margin,
emp.employeename

from 
svTrninvoice as a 
inner join 
svTrninvTask as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
left join svMstTask as c on a.BasicModel=c.BasicModel  and b.OperationNo=c.OperationNo
left join gnMstCustomer as d on a.CustomerCode=d.CustomerCode
left join svtrnservice as sv on a.branchcode=sv.branchcode and a.JobOrderNo=sv.JobOrderNo
left join gnMstEmployee as emp on sv.BranchCode=emp.BranchCode and sv.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
where a.branchcode=641940101
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.") and left(a.invoiceno,3)<>'INI' 
and (c.description like '%spooring%' or c.description like '%BALANCING%')
--and emp.EmployeeName='syarifudin'
and sv.foremanid='061902008'


---- end balancing ciawi

) as table2";

        $query=$this->dbDms->query($sql);
		return $query->result_array();

    }else{
  
    	$sql="Select count(distinct invoiceno) [Total Invoice], sum(margin) as totalmargin from
(
select a.companycode, a.branchcode, a.ProductType, a.invoiceno, a.invoicedate, a.fpjno, a.fpjdate, a.JobOrderNo, a.JobOrderdate, a.PoliceRegNo, 
d.customername,
b.OperationNo, 
c.Description,
b.OperationCost,
b.SubConPrice,
((b.operationhour*b.OperationCost) - b.SubConPrice) as margin



from 
svTrninvoice as a 
inner join 
svTrninvTask as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
left join svMstTask as c on a.BasicModel=c.BasicModel  and b.OperationNo=c.OperationNo
left join gnMstCustomer as d on a.CustomerCode=d.CustomerCode
where a.branchcode=".$branchcode."
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.") and left(a.invoiceno,3)<>'INI' 
and (c.description like '%spooring%' or c.description like '%BALANCING%')

) as table2";

		$query=$this->dbDms->query($sql);
		return $query->result_array();

    }

}


public function getFullRekapByMechanic($branchcode,$year,$month){

    if($branchcode=='641940104'){
        $sql="select branchcode, mechanicid, mechanicname, taskdppamt, total, (100/(


select  count(mechanicid) as jumlah
from (

SELECT a.CompanyCode, a.BranchCode, a.EmployeeID as mechanicid, a.EmployeeName, b.LookUpValueName as position
FROM   dbo.gnMstEmployee AS a
inner join gnMstLookUpDtl as b on a.CompanyCode=b.CompanyCode and b.codeid='titl' and a.TitleCode=b.ParaValue
WHERE (PersonnelStatus = '1')
--AND (TitleCode IN ('2', '3', '8', '9','10','7')) 
AND (TitleCode IN ( '9')) 
and BranchCode=".$branchcode." and employeename not in ('no foreman')
)table1 





)) as persentase
from(

select  branchcode,mechanicid, EmployeeName as mechanicname, --case when position ='SPAREPARTS' then 'ADMINISTRASI SPAREPART' else position end as position
0 as taskdppamt, 0 as total, ROW_NUMBER() OVER (
    ORDER BY mechanicid
   ) row_num
from (

SELECT a.CompanyCode, a.BranchCode, a.EmployeeID as mechanicid, a.EmployeeName, b.LookUpValueName as position
FROM   dbo.gnMstEmployee AS a
inner join gnMstLookUpDtl as b on a.CompanyCode=b.CompanyCode and b.codeid='titl' and a.TitleCode=b.ParaValue
WHERE (PersonnelStatus = '1')
--AND (TitleCode IN ('2', '3', '8', '9','10','7')) 
AND (TitleCode IN ( '9')) 
and BranchCode=".$branchcode." and employeename not in ('no foreman')
)table1 
) table_full";

        $query=$this->dbDms->query($sql);
        return $query->result_array();
}else{



    	$sql="declare @MPMS as bit
set @MPMS = '0'
if (select isnull(ParaValue,0) from gnMstLookUpDtl with(nolock,nowait) 
     where CompanyCode=6419401 and CodeID = 'MPMS') = '1' 
	set @MPMS = '1'


select branchcode, mechanicid, mechanicname, taskdppamt,total, cast(cast(taskdppamt as float)/total*100 as decimal(12,4)) as persentase
from 
(
select branchcode, mechanicid, mechanicname, taskdppamt,
(select sum(taskdppamt) as total

from (

select x.BranchCode, x.MechanicID, x.MechanicName  
     , sum(x.OperationHour) OperationHour  
     , sum(x.TaskAmt) TaskAmt  
     , sum(x.TaskDiscAmt) TaskDiscAmt  
     , sum(x.TaskDppAmt) TaskDppAmt  
     , sum(x.ItemAmtDraft / (case OperationCount when 0 then 1 else OperationCount end)) PartsAmt  
     , sum(x.ItemDiscAmtDraft / (case OperationCount when 0 then 1 else OperationCount end)) PartsDiscAmt  
     , sum((x.ItemAmtDraft - x.ItemDiscAmtDraft) / (case OperationCount when 0 then 1 else OperationCount end)) PartsDppAmt  
  from (
select a.BranchCode
     , a.InvoiceNo  
     , a.InvoiceDate  
     , a.JobOrderNo
     , a.JobType
     , b.OperationNo  
     , b.OperationHour  
     , b.IsSubCon  
     , isnull((  
		  select count(*) from svTrnInvTask with(nolock,nowait)  
		   where CompanyCode = a.CompanyCode  
			 and BranchCode = a.BranchCode  
			 and InvoiceNo = a.InvoiceNo  
			 and IsSubCon = b.IsSubCon
		  ), 0) OperationCount  
     , isnull((  
		  select sum((SupplyQty - ReturnQty) * RetailPrice) from svTrnInvItem with(nolock,nowait)  
		   where CompanyCode = a.CompanyCode  
			 and BranchCode = a.BranchCode  
			 and InvoiceNo = a.InvoiceNo  
			 and IsSubCon = b.IsSubCon
		  ), 0) ItemAmtDraft  
     , isnull((  
		  select convert(money, sum((SupplyQty - ReturnQty) * RetailPrice * DiscPct * 0.01)) from svTrnInvItem with(nolock,nowait)  
		   where CompanyCode = a.CompanyCode  
			 and BranchCode = a.BranchCode  
			 and InvoiceNo = a.InvoiceNo  
			 and IsSubCon = b.IsSubCon
		  ), 0) ItemDiscAmtDraft  
     , isnull((  
		  select Top 1 MechanicID from svTrnInvMechanic with(nolock,nowait)  
		   where CompanyCode = a.CompanyCode  
			 and BranchCode = a.BranchCode  
			 and InvoiceNo = a.InvoiceNo  
			 and OperationNo = b.OperationNo  
		  ), 'SUB') MechanicID  
     , case when @MPMS = '0' then
	  isnull((  
		  select Top 1 y.EmployeeName  
			from svTrnInvMechanic x with(nolock,nowait), gnMstEmployee y with(nolock,nowait)  
		   where y.CompanyCode = x.CompanyCode  
			 and y.BranchCode = x.BranchCode  
			 and y.EmployeeID = x.MechanicID  
			 and x.CompanyCode = a.CompanyCode  
			 and x.BranchCode = a.BranchCode  
			 and x.InvoiceNo = a.InvoiceNo  
			 and x.OperationNo = b.OperationNo  
		  ), 'SUBCON')
		  else
		  isnull((  
		  select Top 1 y.EmployeeName  
			from svTrnInvMechanic x with(nolock,nowait), HrEmployee y with(nolock,nowait)  
		   where y.CompanyCode = x.CompanyCode 
			 and y.EmployeeID = x.MechanicID  
			 and x.CompanyCode = a.CompanyCode  
			 and x.BranchCode = a.BranchCode  
			 and x.InvoiceNo = a.InvoiceNo  
			 and x.OperationNo = b.OperationNo  
		  ), 'SUBCON') end
		   MechanicName  
     , convert(money, b.OperationHour * b.OperationCost) as TaskAmt  
     , convert(money, b.OperationHour * b.OperationCost * b.DiscPct * 0.01) as TaskDiscAmt  
     , convert(money, b.OperationHour * b.OperationCost * (1.0 - (b.DiscPct * 0.01))) as TaskDppAmt  
     , isnull((  
		  select top 1 Description from svMstTask with(nolock,nowait)  
		   where CompanyCode = a.CompanyCode  
			 and ProductType = a.ProductType  
			 and BasicModel = a.BasicModel  
			 and OperationNo = b.OperationNo  
		   order by a.JobType   
		   ), 0) TaskDescription  
  from svTrnInvoice a with(nolock,nowait)  
  left join svTrnInvTask b with(nolock,nowait)  
    on b.CompanyCode = a.CompanyCode  
   and b.BranchCode = a.BranchCode  
   and b.InvoiceNo = a.InvoiceNo  
  left join svTrnService c with(nolock,nowait)  
    on c.CompanyCode = a.CompanyCode  
   and c.BranchCode = a.BranchCode  
   and c.JobOrderNo = a.JobOrderNo  
 where a.CompanyCode = 6419401  
   and a.BranchCode = ".$branchcode."  
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")        
   and isnull(b.OperationNo, '') <> ''  
   and b.IsSubCon = 0
   
union all

-- select invoice yang menjadi subcon
select a.BranchCode
     , a.InvoiceNo  
     , a.InvoiceDate  
     , a.JobOrderNo
     , a.JobType
     , b.OperationNo  
     , b.OperationHour  
     , b.IsSubCon  
     , 1 as OperationCount  
     , 0 as ItemAmtDraft  
     , 0 as ItemDiscAmtDraft  
     , 'SUB' as MechanicID  
     , 'SUBCON' as MechanicName  
     , convert(money, b.OperationHour * b.OperationCost) as TaskAmt  
     , convert(money, b.OperationHour * b.OperationCost * b.DiscPct * 0.01) as TaskDiscAmt  
     , convert(money, b.OperationHour * b.OperationCost * (1.0 - (b.DiscPct * 0.01))) as TaskDppAmt  
     , isnull((  
		  select top 1 Description from svMstTask with(nolock,nowait)  
		   where CompanyCode = a.CompanyCode  
			 and ProductType = a.ProductType  
			 and BasicModel = a.BasicModel  
			 and OperationNo = b.OperationNo  
		   order by a.JobType   
		   ), 0) TaskDescription  
  from svTrnInvoice a with(nolock,nowait)  
  left join svTrnInvTask b with(nolock,nowait)  
    on b.CompanyCode = a.CompanyCode  
   and b.BranchCode = a.BranchCode  
   and b.InvoiceNo = a.InvoiceNo  
  left join svTrnService c with(nolock,nowait)  
    on c.CompanyCode = a.CompanyCode  
   and c.BranchCode = a.BranchCode  
   and c.JobOrderNo = a.JobOrderNo  
 where a.CompanyCode = 6419401  
   and a.BranchCode = ".$branchcode."  
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")        
   and isnull(b.OperationNo, '') <> ''  
   and b.IsSubCon = 1   
   
union all

-- select invoice yang tidak ada task tetapi terdapat partnya
select a.BranchCode, a.InvoiceNo, a.InvoiceDate
     , a.JobOrderNo
     , a.JobType
     , '' as OperationNo
     , 0 as OperationHour 
     , 0 as IsSubcon
     , 1 as OperationCount
     , (b.SupplyQty - b.ReturnQty) * RetailPrice as ItemAmtDraft
     , (b.SupplyQty - b.ReturnQty) * DiscPct * 0.01 * RetailPrice as ItemDiscAmtDraft
     , isnull((  
		select top 1 mec.MechanicID
		  from svTrnSrvMechanic mec with(nolock,nowait)
		  left join svTrnService srv with(nolock,nowait)
			on srv.CompanyCode = mec.CompanyCode
		   and srv.BranchCode = mec.BranchCode
		   and srv.ServiceNo = mec.ServiceNo
		 where srv.CompanyCode = a.CompanyCode
		   and srv.BranchCode = a.BranchCode
		   and srv.JobOrderNo = a.JobOrderNo
		  ), ' ') MechanicID  
     , case when @MPMS = '0' then
		isnull((  
		select top 1 emp.EmployeeName
		  from svTrnSrvMechanic mec with(nolock,nowait)
		  left join gnMstEmployee emp with(nolock,nowait)
		    on emp.CompanyCode = mec.CompanyCode
		   and emp.BranchCode = mec.BranchCode
		   and emp.EmployeeID = mec.MechanicID
		  left join svTrnService srv with(nolock,nowait)
			on srv.CompanyCode = mec.CompanyCode
		   and srv.BranchCode = mec.BranchCode
		   and srv.ServiceNo = mec.ServiceNo
		 where srv.CompanyCode = a.CompanyCode
		   and srv.BranchCode = a.BranchCode
		   and srv.JobOrderNo = a.JobOrderNo
		  ), ' ')
		  else
		  isnull((  
		select top 1 emp.EmployeeName
		  from svTrnSrvMechanic mec with(nolock,nowait)
		  left join HrEmployee emp with(nolock,nowait)
		    on emp.CompanyCode = mec.CompanyCode
		   and emp.EmployeeID = mec.MechanicID
		  left join svTrnService srv with(nolock,nowait)
			on srv.CompanyCode = mec.CompanyCode
		   and srv.BranchCode = mec.BranchCode
		   and srv.ServiceNo = mec.ServiceNo
		 where srv.CompanyCode = a.CompanyCode
		   and srv.BranchCode = a.BranchCode
		   and srv.JobOrderNo = a.JobOrderNo
		  ), ' ') end
		   MechanicName
     , 0 as TaskAmt
     , 0 as TaskDiscAmt
     , 0 as TaskDppAmt
     , '' as TaskDescription
  from svTrnInvoice a with(nolock,nowait)   
 inner join svTrnInvItem b with(nolock,nowait)
    on b.CompanyCode = a.CompanyCode
   and b.BranchCode = a.BranchCode
   and b.InvoiceNo = a.InvoiceNo
 where a.CompanyCode = 6419401  
   and a.BranchCode = ".$branchcode."  
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")      
   and not exists (
	select 1 from svTrnInvTask with(nolock,nowait)
	 where CompanyCode = a.CompanyCode
	   and BranchCode = a.BranchCode
	   and InvoiceNo = a.InvoiceNo
   )
) as x
where isnull(x.MechanicID, '') <> ''  
and x.MechanicName not like '%subcon%'
 group by x.BranchCode, x.MechanicID, x.MechanicName  
 --order by x.MechanicID 
) as table1
) total
from
(
select x.BranchCode, x.MechanicID, x.MechanicName  
     , sum(x.OperationHour) OperationHour  
     , sum(x.TaskAmt) TaskAmt  
     , sum(x.TaskDiscAmt) TaskDiscAmt  
     , sum(x.TaskDppAmt) TaskDppAmt  
     , sum(x.ItemAmtDraft / (case OperationCount when 0 then 1 else OperationCount end)) PartsAmt  
     , sum(x.ItemDiscAmtDraft / (case OperationCount when 0 then 1 else OperationCount end)) PartsDiscAmt  
     , sum((x.ItemAmtDraft - x.ItemDiscAmtDraft) / (case OperationCount when 0 then 1 else OperationCount end)) PartsDppAmt  
  from (
select a.BranchCode
     , a.InvoiceNo  
     , a.InvoiceDate  
     , a.JobOrderNo
     , a.JobType
     , b.OperationNo  
     , b.OperationHour  
     , b.IsSubCon  
     , isnull((  
		  select count(*) from svTrnInvTask with(nolock,nowait)  
		   where CompanyCode = a.CompanyCode  
			 and BranchCode = a.BranchCode  
			 and InvoiceNo = a.InvoiceNo  
			 and IsSubCon = b.IsSubCon
		  ), 0) OperationCount  
     , isnull((  
		  select sum((SupplyQty - ReturnQty) * RetailPrice) from svTrnInvItem with(nolock,nowait)  
		   where CompanyCode = a.CompanyCode  
			 and BranchCode = a.BranchCode  
			 and InvoiceNo = a.InvoiceNo  
			 and IsSubCon = b.IsSubCon
		  ), 0) ItemAmtDraft  
     , isnull((  
		  select convert(money, sum((SupplyQty - ReturnQty) * RetailPrice * DiscPct * 0.01)) from svTrnInvItem with(nolock,nowait)  
		   where CompanyCode = a.CompanyCode  
			 and BranchCode = a.BranchCode  
			 and InvoiceNo = a.InvoiceNo  
			 and IsSubCon = b.IsSubCon
		  ), 0) ItemDiscAmtDraft  
     , isnull((  
		  select Top 1 MechanicID from svTrnInvMechanic with(nolock,nowait)  
		   where CompanyCode = a.CompanyCode  
			 and BranchCode = a.BranchCode  
			 and InvoiceNo = a.InvoiceNo  
			 and OperationNo = b.OperationNo  
		  ), 'SUB') MechanicID  
     , case when @MPMS = '0' then
	  isnull((  
		  select Top 1 y.EmployeeName  
			from svTrnInvMechanic x with(nolock,nowait), gnMstEmployee y with(nolock,nowait)  
		   where y.CompanyCode = x.CompanyCode  
			 and y.BranchCode = x.BranchCode  
			 and y.EmployeeID = x.MechanicID  
			 and x.CompanyCode = a.CompanyCode  
			 and x.BranchCode = a.BranchCode  
			 and x.InvoiceNo = a.InvoiceNo  
			 and x.OperationNo = b.OperationNo  
		  ), 'SUBCON')
		  else
		  isnull((  
		  select Top 1 y.EmployeeName  
			from svTrnInvMechanic x with(nolock,nowait), HrEmployee y with(nolock,nowait)  
		   where y.CompanyCode = x.CompanyCode 
			 and y.EmployeeID = x.MechanicID  
			 and x.CompanyCode = a.CompanyCode  
			 and x.BranchCode = a.BranchCode  
			 and x.InvoiceNo = a.InvoiceNo  
			 and x.OperationNo = b.OperationNo  
		  ), 'SUBCON') end
		   MechanicName  
     , convert(money, b.OperationHour * b.OperationCost) as TaskAmt  
     , convert(money, b.OperationHour * b.OperationCost * b.DiscPct * 0.01) as TaskDiscAmt  
     , convert(money, b.OperationHour * b.OperationCost * (1.0 - (b.DiscPct * 0.01))) as TaskDppAmt  
     , isnull((  
		  select top 1 Description from svMstTask with(nolock,nowait)  
		   where CompanyCode = a.CompanyCode  
			 and ProductType = a.ProductType  
			 and BasicModel = a.BasicModel  
			 and OperationNo = b.OperationNo  
		   order by a.JobType   
		   ), 0) TaskDescription  
  from svTrnInvoice a with(nolock,nowait)  
  left join svTrnInvTask b with(nolock,nowait)  
    on b.CompanyCode = a.CompanyCode  
   and b.BranchCode = a.BranchCode  
   and b.InvoiceNo = a.InvoiceNo  
  left join svTrnService c with(nolock,nowait)  
    on c.CompanyCode = a.CompanyCode  
   and c.BranchCode = a.BranchCode  
   and c.JobOrderNo = a.JobOrderNo  
 where a.CompanyCode = 6419401  
   and a.BranchCode = ".$branchcode."  
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")        
   and isnull(b.OperationNo, '') <> ''  
   and b.IsSubCon = 0
   
union all

-- select invoice yang menjadi subcon
select a.BranchCode
     , a.InvoiceNo  
     , a.InvoiceDate  
     , a.JobOrderNo
     , a.JobType
     , b.OperationNo  
     , b.OperationHour  
     , b.IsSubCon  
     , 1 as OperationCount  
     , 0 as ItemAmtDraft  
     , 0 as ItemDiscAmtDraft  
     , 'SUB' as MechanicID  
     , 'SUBCON' as MechanicName  
     , convert(money, b.OperationHour * b.OperationCost) as TaskAmt  
     , convert(money, b.OperationHour * b.OperationCost * b.DiscPct * 0.01) as TaskDiscAmt  
     , convert(money, b.OperationHour * b.OperationCost * (1.0 - (b.DiscPct * 0.01))) as TaskDppAmt  
     , isnull((  
		  select top 1 Description from svMstTask with(nolock,nowait)  
		   where CompanyCode = a.CompanyCode  
			 and ProductType = a.ProductType  
			 and BasicModel = a.BasicModel  
			 and OperationNo = b.OperationNo  
		   order by a.JobType   
		   ), 0) TaskDescription  
  from svTrnInvoice a with(nolock,nowait)  
  left join svTrnInvTask b with(nolock,nowait)  
    on b.CompanyCode = a.CompanyCode  
   and b.BranchCode = a.BranchCode  
   and b.InvoiceNo = a.InvoiceNo  
  left join svTrnService c with(nolock,nowait)  
    on c.CompanyCode = a.CompanyCode  
   and c.BranchCode = a.BranchCode  
   and c.JobOrderNo = a.JobOrderNo  
 where a.CompanyCode = 6419401  
   and a.BranchCode = ".$branchcode."  
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")       
   and isnull(b.OperationNo, '') <> ''  
   and b.IsSubCon = 1   
   
union all

-- select invoice yang tidak ada task tetapi terdapat partnya
select a.BranchCode, a.InvoiceNo, a.InvoiceDate
     , a.JobOrderNo
     , a.JobType
     , '' as OperationNo
     , 0 as OperationHour 
     , 0 as IsSubcon
     , 1 as OperationCount
     , (b.SupplyQty - b.ReturnQty) * RetailPrice as ItemAmtDraft
     , (b.SupplyQty - b.ReturnQty) * DiscPct * 0.01 * RetailPrice as ItemDiscAmtDraft
     , isnull((  
		select top 1 mec.MechanicID
		  from svTrnSrvMechanic mec with(nolock,nowait)
		  left join svTrnService srv with(nolock,nowait)
			on srv.CompanyCode = mec.CompanyCode
		   and srv.BranchCode = mec.BranchCode
		   and srv.ServiceNo = mec.ServiceNo
		 where srv.CompanyCode = a.CompanyCode
		   and srv.BranchCode = a.BranchCode
		   and srv.JobOrderNo = a.JobOrderNo
		  ), ' ') MechanicID  
     , case when @MPMS = '0' then
		isnull((  
		select top 1 emp.EmployeeName
		  from svTrnSrvMechanic mec with(nolock,nowait)
		  left join gnMstEmployee emp with(nolock,nowait)
		    on emp.CompanyCode = mec.CompanyCode
		   and emp.BranchCode = mec.BranchCode
		   and emp.EmployeeID = mec.MechanicID
		  left join svTrnService srv with(nolock,nowait)
			on srv.CompanyCode = mec.CompanyCode
		   and srv.BranchCode = mec.BranchCode
		   and srv.ServiceNo = mec.ServiceNo
		 where srv.CompanyCode = a.CompanyCode
		   and srv.BranchCode = a.BranchCode
		   and srv.JobOrderNo = a.JobOrderNo
		  ), ' ')
		  else
		  isnull((  
		select top 1 emp.EmployeeName
		  from svTrnSrvMechanic mec with(nolock,nowait)
		  left join HrEmployee emp with(nolock,nowait)
		    on emp.CompanyCode = mec.CompanyCode
		   and emp.EmployeeID = mec.MechanicID
		  left join svTrnService srv with(nolock,nowait)
			on srv.CompanyCode = mec.CompanyCode
		   and srv.BranchCode = mec.BranchCode
		   and srv.ServiceNo = mec.ServiceNo
		 where srv.CompanyCode = a.CompanyCode
		   and srv.BranchCode = a.BranchCode
		   and srv.JobOrderNo = a.JobOrderNo
		  ), ' ') end
		   MechanicName
     , 0 as TaskAmt
     , 0 as TaskDiscAmt
     , 0 as TaskDppAmt
     , '' as TaskDescription
  from svTrnInvoice a with(nolock,nowait)   
 inner join svTrnInvItem b with(nolock,nowait)
    on b.CompanyCode = a.CompanyCode
   and b.BranchCode = a.BranchCode
   and b.InvoiceNo = a.InvoiceNo
 where a.CompanyCode = 6419401  
   and a.BranchCode = ".$branchcode."  
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")     
   and not exists (
	select 1 from svTrnInvTask with(nolock,nowait)
	 where CompanyCode = a.CompanyCode
	   and BranchCode = a.BranchCode
	   and InvoiceNo = a.InvoiceNo
   )
) as x
where isnull(x.MechanicID, '') <> ''  
and x.MechanicName not like '%subcon%'
 group by x.BranchCode, x.MechanicID, x.MechanicName  
 --order by x.MechanicID 
) table3
) as table4";

		$query=$this->dbDms->query($sql);
		return $query->result_array();
    }
}




 public function getAllMsiDataCal($branchcode, $year, $month){

 if($branchcode=='641940101'){
 	$sql="--MSI



--select sum(total) as jumlah_total from (
select 'labour MSI' as asal, sum(lbrdppamt) as total
--,(select distinct docno from arTrnBankKasTerimaInvoice where branchcode=table2.branchcode and invoiceno=table2.invoiceno) as docno
from
(
select table1.* from(
select 
		 sv.foremanid
		 , ceiling(b.OperationHour * b.OperationCost * (100.0 - b.DiscPct) * 0.01) LbrDppAmt
		 , IsSubCon = isnull((
						  select top 1 IsSubCon from svMstTask
						   where CompanyCode = a.CompanyCode
							 and BasicModel = a.BasicModel
							 and OperationNo = b.OperationNo
						  ), 0)
		 , a.JobType		
		
	  from svTrnInvoice a
	 inner join svTrnInvTask b
		on b.CompanyCode = a.CompanyCode
	   and b.BranchCode = a.BranchCode
	   and b.InvoiceNo = a.InvoiceNo
	  left join svMstJob c
		on c.CompanyCode = a.CompanyCode
	   and c.BasicModel = a.BasicModel
	   and c.JobType = a.JobType
	 left join svtrnservice as sv on a.CompanyCode=sv.CompanyCode
	   and a.BranchCode=sv.BranchCode 
	   and a.JobOrderNo=sv.JobOrderNo
	
	 where a.CompanyCode = 6419401
	   and a.BranchCode = 641940101
	   and Year(a.Invoicedate) = ".$year."
	   and Month(a.Invoicedate) = ".$month."
	   and left(a.invoiceno,3)<>'INI'
	   and a.JobType != 'REWORK'
	
)  table1 

where table1.IsSubCon=0 and foremanid<>'061902008'
) as table_induk_labour


union all

select 'lubricant MSI' as asal, sum(spramt) as total  from (	
	select 
		  c.ParaValue as GroupTpgo
		 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
		
		 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
		  , b.typeofgoods
	  from svTrnInvoice a
	 inner join svTrnInvItem b
		on b.CompanyCode = a.CompanyCode
	   and b.BranchCode = a.BranchCode
	   and b.InvoiceNo = a.InvoiceNo
	  left join gnMstLookupDtl c
		on c.CompanyCode = a.CompanyCode
	   and c.CodeID = 'GTGO'
	   and c.LookupValue = b.TypeOfGoods
	  left join gnMstCustomer as e on a.CustomerCodeBill=e.CustomerCode
	  left join svtrnservice as sv on a.CompanyCode=sv.CompanyCode
	   and a.BranchCode=sv.BranchCode 
	   and a.JobOrderNo=sv.JobOrderNo
	  left join gnMstEmployee as emp on sv.ForemanID=emp.EmployeeID and sv.BranchCode=emp.BranchCode and emp.PersonnelStatus=1
	
	 where a.CompanyCode = 6419401
	   and a.BranchCode = 641940101
	   and Year(a.Invoicedate) = ".$year."
	   and Month(a.Invoicedate) = ".$month."
	   and a.JobType != 'REWORK'
	   and left(a.invoiceno,3)<>'INI'
	   and emp.employeeID<>'061902008'
	   --and e.CustomerName not like '%DUTA CENDANA ADIMANDIRI%'
) as table1 where IsSublet = 0 and GroupTpgo != 'SPAREPART'

union all

select 'sparepart MSI' as asal, sum(total) as total
from (
--Total Workshop Parts Sales Revenue
select sum(spramt) as total from (	
	select 
		 c.ParaValue as GroupTpgo
		 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
		 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
		  , b.typeofgoods
	  from svTrnInvoice a
	 inner join svTrnInvItem b
		on b.CompanyCode = a.CompanyCode
	   and b.BranchCode = a.BranchCode
	   and b.InvoiceNo = a.InvoiceNo
	  left join gnMstLookupDtl c
		on c.CompanyCode = a.CompanyCode
	   and c.CodeID = 'GTGO'
	   and c.LookupValue = b.TypeOfGoods
	  left join gnMstCustomer as e on a.CustomerCodeBill=e.CustomerCode
	  left join svtrnservice as sv on 
			a.CompanyCode=sv.CompanyCode
			and a.BranchCode=sv.BranchCode
			and a.JobOrderNo=sv.JobOrderNo
	  left join gnMstEmployee as emp on sv.CompanyCode=emp.CompanyCode
	       and sv.BranchCode=emp.branchcode
		   and sv.ForemanID=emp.EmployeeID
		   and emp.PersonnelStatus=1
	 where a.CompanyCode = 6419401
	   and a.BranchCode = 641940101
	   and Year(a.Invoicedate) = ".$year."
	   and Month(a.Invoicedate) = ".$month."
	   and left(a.invoiceno,3)<>'INI'
	   and a.JobType != 'REWORK'
	   and emp.employeeID<>'061902008'
	   --and e.CustomerName not like '%DUTA CENDANA ADIMANDIRI%'
	   
) as table1 where IsSublet = 0 and GroupTpgo = 'SPAREPART'

union all
-- counter sales part
select sum(totDPPamt) as total 
	              from spTrnSFPJHdr as a
	              left join gnMstCustomer as b on a.customercodebill = b.CustomerCode
	              where 1 = 1
	               and a.CompanyCode = 6419401
	               and BranchCode = 641940101
	               and year(a.FPJDate) = ".$year."
	               and month(a.FPJDate) = ".$month."
	               and a.TypeOfGoods = '0'
                   and b.customername not like('%duta cendana adimandiri%')

union all
-- Total Suzuki Genuine Accessories Sales Revenue - Chargeable to Customer CPUS (External)
select sum(DPPamt) as total from (
select 
					
					ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt
					
				from svTrnInvItem invi	
				left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
				left join gnMstCustomer as e on inv.CustomerCodeBill=e.CustomerCode
				left join svtrnservice as sv on inv.CompanyCode=sv.CompanyCode
				   and inv.BranchCode=sv.BranchCode 
				   and inv.JobOrderNo=sv.JobOrderNo
				left join gnmstemployee as emp on sv.BranchCode=emp.BranchCode
				   and sv.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
				where invi.companycode=6419401
				and invi.branchCode=641940101
				and invi.TypeOfGoods in ('2','5','6','9')
				and year(inv.InvoiceDate)=".$year."
				and MONTH(inv.InvoiceDate)=".$month."
				and left(inv.invoiceno,3)<>'INI'
				and emp.employeeID<>'061902008'
				--and e.customername not like('%duta cendana adimandiri%')
				--group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)

				union all

select 
					
					ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt
				
				from svTrnInvItem invi	
				left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
				left join gnMstCustomer as e on inv.CustomerCodeBill=e.CustomerCode
				left join svtrnservice as sv on inv.CompanyCode=sv.CompanyCode
				   and inv.BranchCode=sv.BranchCode 
				   and inv.JobOrderNo=sv.JobOrderNo
				left join gnmstemployee as emp on sv.BranchCode=emp.BranchCode
				   and sv.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
				where invi.companycode=6419401
				and invi.branchCode=641940101
				and invi.TypeOfGoods in ('7','8')
				and year(inv.InvoiceDate)=".$year."
				and MONTH(inv.InvoiceDate)=".$month."
				and left(inv.invoiceno,3)<>'INI'
				and emp.employeeID<>'061902008'
				--and e.customername not like('%duta cendana adimandiri%')
				
				
				
	) as accesoris	
	
) as table1 

--)table_induk ";

$query=$this->dbDms->query($sql);
		return $query->result_array();


 }else if($branchcode=='641940104'){
 	$sql="--MSI



--select sum(total) as jumlah_total from (
select 'labour MSI' as asal, sum(lbrdppamt) as total
--,(select distinct docno from arTrnBankKasTerimaInvoice where branchcode=table2.branchcode and invoiceno=table2.invoiceno) as docno
from
(
select table1.* from(
select 
		
		sv.foremanid
	   
		
		 , ceiling(b.OperationHour * b.OperationCost * (100.0 - b.DiscPct) * 0.01) LbrDppAmt
		
		 , IsSubCon = isnull((
						  select top 1 IsSubCon from svMstTask
						   where CompanyCode = a.CompanyCode
							 and BasicModel = a.BasicModel
							 and OperationNo = b.OperationNo
						  ), 0)
		 
	  from svTrnInvoice a
	 inner join svTrnInvTask b
		on b.CompanyCode = a.CompanyCode
	   and b.BranchCode = a.BranchCode
	   and b.InvoiceNo = a.InvoiceNo
	  left join svMstJob c
		on c.CompanyCode = a.CompanyCode
	   and c.BasicModel = a.BasicModel
	   and c.JobType = a.JobType
	 left join svtrnservice as sv on a.CompanyCode=sv.CompanyCode
	   and a.BranchCode=sv.BranchCode 
	   and a.JobOrderNo=sv.JobOrderNo
	
	 where a.CompanyCode = 6419401
	   and a.BranchCode = 641940101
	   and Year(a.Invoicedate) = ".$year."
	   and Month(a.Invoicedate) = ".$month."
	   and left(a.invoiceno,3)<>'INI'
	   and a.JobType != 'REWORK'
	
)  table1 

where table1.IsSubCon=0 and foremanid='061902008'
) as table_induk_labour


union all

select 'lubricant MSI' as asal, sum(spramt) as total  from (	
	select 
		  c.ParaValue as GroupTpgo
		 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
		 
		 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
		 
	  from svTrnInvoice a
	 inner join svTrnInvItem b
		on b.CompanyCode = a.CompanyCode
	   and b.BranchCode = a.BranchCode
	   and b.InvoiceNo = a.InvoiceNo
	  left join gnMstLookupDtl c
		on c.CompanyCode = a.CompanyCode
	   and c.CodeID = 'GTGO'
	   and c.LookupValue = b.TypeOfGoods
	  left join gnMstCustomer as e on a.CustomerCodeBill=e.CustomerCode
	  left join svtrnservice as sv on a.CompanyCode=sv.CompanyCode
	   and a.BranchCode=sv.BranchCode 
	   and a.JobOrderNo=sv.JobOrderNo
	  left join gnMstEmployee as emp on sv.ForemanID=emp.EmployeeID and sv.BranchCode=emp.BranchCode and emp.PersonnelStatus=1
	
	 where a.CompanyCode = 6419401
	   and a.BranchCode = 641940101
	   and Year(a.Invoicedate) = ".$year."
	   and Month(a.Invoicedate) = ".$month."
	   and a.JobType != 'REWORK'
	   and left(a.invoiceno,3)<>'INI'
	   and emp.employeeID='061902008'
	   --and e.CustomerName not like '%DUTA CENDANA ADIMANDIRI%'
) as table1 where IsSublet = 0 and GroupTpgo != 'SPAREPART'

union all

select 'sparepart MSI' as asal, sum(total) as total
from (
--Total Workshop Parts Sales Revenue
select sum(spramt) as total from (	
	select 
		 c.ParaValue as GroupTpgo
		 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
		 
		 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
		  
	  from svTrnInvoice a
	 inner join svTrnInvItem b
		on b.CompanyCode = a.CompanyCode
	   and b.BranchCode = a.BranchCode
	   and b.InvoiceNo = a.InvoiceNo
	  left join gnMstLookupDtl c
		on c.CompanyCode = a.CompanyCode
	   and c.CodeID = 'GTGO'
	   and c.LookupValue = b.TypeOfGoods
	  left join gnMstCustomer as e on a.CustomerCodeBill=e.CustomerCode
	  left join svtrnservice as sv on 
			a.CompanyCode=sv.CompanyCode
			and a.BranchCode=sv.BranchCode
			and a.JobOrderNo=sv.JobOrderNo
	  left join gnMstEmployee as emp on sv.CompanyCode=emp.CompanyCode
	       and sv.BranchCode=emp.branchcode
		   and sv.ForemanID=emp.EmployeeID
		   and emp.PersonnelStatus=1
	 where a.CompanyCode = 6419401
	   and a.BranchCode = 641940101
	   and Year(a.Invoicedate) = ".$year."
	   and Month(a.Invoicedate) = ".$month."
	   and left(a.invoiceno,3)<>'INI'
	   and a.JobType != 'REWORK'
	   --and emp.employeeID='061902008'
       and sv.foremanid='061902008'
	   --and e.CustomerName not like '%DUTA CENDANA ADIMANDIRI%'
	   
) as table1 where IsSublet = 0 and GroupTpgo = 'SPAREPART'

--union all
-- counter sales part
--select sum(totDPPamt) as total 
	              --from spTrnSFPJHdr as a
	              --left join gnMstCustomer as b on a.customercodebill = b.CustomerCode
	             --where 1 = 1
	               --and a.CompanyCode = 6419401
	               --and BranchCode = 641940101
	              -- and year(a.FPJDate) = 2022
	               --and month(a.FPJDate) = 7
	               --and a.TypeOfGoods = '0'
                   --and b.customername not like('%duta cendana adimandiri%')

union all
-- Total Suzuki Genuine Accessories Sales Revenue - Chargeable to Customer CPUS (External)
select sum(DPPamt) as total from (
select 
					
					ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt
				from svTrnInvItem invi	
				left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
				left join gnMstCustomer as e on inv.CustomerCodeBill=e.CustomerCode
				left join svtrnservice as sv on inv.CompanyCode=sv.CompanyCode
				   and inv.BranchCode=sv.BranchCode 
				   and inv.JobOrderNo=sv.JobOrderNo
				left join gnmstemployee as emp on sv.BranchCode=emp.BranchCode
				   and sv.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
				where invi.companycode=6419401
				and invi.branchCode=641940101
				and invi.TypeOfGoods in ('2','5','6','9')
				and year(inv.InvoiceDate)=".$year."
				and MONTH(inv.InvoiceDate)=".$month."
				and left(inv.invoiceno,3)<>'INI'
				--and emp.employeeID='061902008'
                and sv.foremanid='061902008'
				--and e.customername not like('%duta cendana adimandiri%')
				--group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)

				union all

select 
					
					ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt
				from svTrnInvItem invi	
				left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
				left join gnMstCustomer as e on inv.CustomerCodeBill=e.CustomerCode
				left join svtrnservice as sv on inv.CompanyCode=sv.CompanyCode
				   and inv.BranchCode=sv.BranchCode 
				   and inv.JobOrderNo=sv.JobOrderNo
				left join gnmstemployee as emp on sv.BranchCode=emp.BranchCode
				   and sv.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
				where invi.companycode=6419401
				and invi.branchCode=641940101
				and invi.TypeOfGoods in ('7','8')
				and year(inv.InvoiceDate)=".$year."
				and MONTH(inv.InvoiceDate)=".$month."
				and left(inv.invoiceno,3)<>'INI'
				--and emp.employeeID='061902008'
                and sv.foremanid='061902008'
				--and e.customername not like('%duta cendana adimandiri%')
				
				
				
	) as accesoris	
	
) as table1 

--)table_induk ";

$query=$this->dbDms->query($sql);
		return $query->result_array();

 }else{
 	$sql="
select 'labour MSI' as asal, sum(lbrdppamt) as total
--,(select distinct docno from arTrnBankKasTerimaInvoice where branchcode=table2.branchcode and invoiceno=table2.invoiceno) as docno
from
(
select * from(
select 
		
		 ceiling(b.OperationHour * b.OperationCost * (100.0 - b.DiscPct) * 0.01) LbrDppAmt
		 
		 , IsSubCon = isnull((
						  select top 1 IsSubCon from svMstTask
						   where CompanyCode = a.CompanyCode
							 and BasicModel = a.BasicModel
							 and OperationNo = b.OperationNo
						  ), 0)
		
	  from svTrnInvoice a
	 inner join svTrnInvTask b
		on b.CompanyCode = a.CompanyCode
	   and b.BranchCode = a.BranchCode
	   and b.InvoiceNo = a.InvoiceNo
	  left join svMstJob c
		on c.CompanyCode = a.CompanyCode
	   and c.BasicModel = a.BasicModel
	   and c.JobType = a.JobType
	 where a.CompanyCode = 6419401
	   and a.BranchCode = ".$branchcode."
	   and Year(a.Invoicedate) = ".$year."
	   and Month(a.Invoicedate) = ".$month."
	   and left(a.invoiceno,3)<>'INI'
	   and a.JobType != 'REWORK'
	
)  table1 where table1.IsSubCon=0
) as table2


union all

select 'lubricant MSI' as asal, sum(spramt) as total  from (	
	select 
		 c.ParaValue as GroupTpgo
		 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
		 
		 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
		 
	  from svTrnInvoice a
	 inner join svTrnInvItem b
		on b.CompanyCode = a.CompanyCode
	   and b.BranchCode = a.BranchCode
	   and b.InvoiceNo = a.InvoiceNo
	  left join gnMstLookupDtl c
		on c.CompanyCode = a.CompanyCode
	   and c.CodeID = 'GTGO'
	   and c.LookupValue = b.TypeOfGoods
	  left join gnMstCustomer as e on a.CustomerCodeBill=e.CustomerCode
	 where a.CompanyCode = 6419401
	   and a.BranchCode = ".$branchcode."
	   and Year(a.Invoicedate) = ".$year."
	   and Month(a.Invoicedate) = ".$month."
	   and a.JobType != 'REWORK'
	   and left(a.invoiceno,3)<>'INI'
	   --and e.CustomerName not like '%DUTA CENDANA ADIMANDIRI%'
) as table1 where IsSublet = 0 and GroupTpgo != 'SPAREPART'

union all

select 'sparepart MSI' as asal, sum(total) as total
from (
--Total Workshop Parts Sales Revenue
select sum(spramt) as total from (	
	select 
		 c.ParaValue as GroupTpgo
		 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
		 
		 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
		 
	  from svTrnInvoice a
	 inner join svTrnInvItem b
		on b.CompanyCode = a.CompanyCode
	   and b.BranchCode = a.BranchCode
	   and b.InvoiceNo = a.InvoiceNo
	  left join gnMstLookupDtl c
		on c.CompanyCode = a.CompanyCode
	   and c.CodeID = 'GTGO'
	   and c.LookupValue = b.TypeOfGoods
	  left join gnMstCustomer as e on a.CustomerCodeBill=e.CustomerCode
	 where a.CompanyCode = 6419401
	   and a.BranchCode = ".$branchcode."
	   and Year(a.Invoicedate) = ".$year."
	   and Month(a.Invoicedate) = ".$month."
	   and left(a.invoiceno,3)<>'INI'
	   and a.JobType != 'REWORK'
	   --and e.CustomerName not like '%DUTA CENDANA ADIMANDIRI%'
	   
) as table1 where IsSublet = 0 and GroupTpgo = 'SPAREPART'

union all
-- counter sales part
select sum(totDPPamt) as total 
	              from spTrnSFPJHdr as a
	              left join gnMstCustomer as b on a.customercodebill = b.CustomerCode
	             where 1 = 1
	               and a.CompanyCode = 6419401
	               and BranchCode = ".$branchcode."
	               and year(a.FPJDate) = ".$year."
	               and month(a.FPJDate) = ".$month."
	               and a.TypeOfGoods = '0'
                   and b.customername not like('%duta cendana adimandiri%')

union all
-- Total Suzuki Genuine Accessories Sales Revenue - Chargeable to Customer CPUS (External)
select sum(DPPamt) as total from (
select 
					
					ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt
					
				from svTrnInvItem invi	
				left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
				left join gnMstCustomer as e on inv.CustomerCodeBill=e.CustomerCode
				where invi.companycode=6419401
				and invi.branchCode=".$branchcode."
				and invi.TypeOfGoods in ('2','5','6','9')
				and year(inv.InvoiceDate)=".$year."
				and MONTH(inv.InvoiceDate)=".$month."
				and left(inv.invoiceno,3)<>'INI'
				--and e.customername not like('%duta cendana adimandiri%')
				--group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)

				union all

select 
					
					ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt
					
				from svTrnInvItem invi	
				left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
				left join gnMstCustomer as e on inv.CustomerCodeBill=e.CustomerCode
				where invi.companycode=6419401
				and invi.branchCode=".$branchcode."
				and invi.TypeOfGoods in ('7','8')
				and year(inv.InvoiceDate)=".$year."
				and MONTH(inv.InvoiceDate)=".$month."
				and left(inv.invoiceno,3)<>'INI'
				--and e.customername not like('%duta cendana adimandiri%')
				
				
				
	) as accesoris	
	
) as table1 

union all

select 'Parts Sales + revenue' as asal, sum([total spramt]) as total
from (
select sum(spramt) as [total spramt] from 
(
select a.InvoiceNo
		 , c.ParaValue as GroupTpgo
		 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
		 , case 
			 when left(a.InvoiceNo, 3) = 'INI' or a.JobType = 'PDI' then 'INT,PDI'
			 when left(a.InvoiceNo, 3) = 'INW' or left(a.InvoiceNo, 3) = 'INF' then 'CLM,FSC'
			 else 'CPUS'
		   end GroupJob
		 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
		  , b.typeofgoods
	  from svTrnInvoice a
	 inner join svTrnInvItem b
		on b.CompanyCode = a.CompanyCode
	   and b.BranchCode = a.BranchCode
	   and b.InvoiceNo = a.InvoiceNo
	  left join gnMstLookupDtl c
		on c.CompanyCode = a.CompanyCode
	   and c.CodeID = 'GTGO'
	   and c.LookupValue = b.TypeOfGoods
	 where a.CompanyCode = 6419401
	   and a.BranchCode = ".$branchcode."
	   and Year(a.Invoicedate) = ".$year."
	   and Month(a.Invoicedate) =".$month."
	   and left(a.invoiceno,3)<>'INI'
	   and a.JobType != 'REWORK'
) table1 where IsSublet = 0 and GroupTpgo = 'SPAREPART' and GroupJob = 'CPUS'

union all

select sum(a.TotDppAmt) as [total spramt]
	              from spTrnSFPJHdr as a
	              left join gnMstCustomer as b on a.customercodebill=b.CustomerCode
	             where 1 = 1
	               and a.CompanyCode = 6419401
	               and a.BranchCode = ".$branchcode."
	               and year(a.FPJDate) = ".$year."
	               and month(a.FPJDate) =".$month."
	               and a.TypeOfGoods = '0'
	               and b.customername not like('%duta cendana adimandiri%')
) tabletambah

union all

select 'labour MSI waranty + internal' as asal, sum(lbrdppamt) as total
--,(select distinct docno from arTrnBankKasTerimaInvoice where branchcode=table2.branchcode and invoiceno=table2.invoiceno) as docno
from
(
select * from(
select 
		a.CompanyCode
	   ,a.BranchCode
		,a.InvoiceNo
	   ,a.invoicedate
	   , a.FPJNo
		, a.InvoiceStatus
	   
		, b.OperationHour
		, b.operationcost
		, b.DiscPct
		, a.joborderdate
		, a.joborderNo
		, a.ProductType
		 , ceiling(b.OperationHour * b.OperationCost * (100.0 - b.DiscPct) * 0.01) LbrDppAmt
		 , a.LaborDppAmt as faktual_labour
		 , IsSubCon = isnull((
						  select top 1 IsSubCon from svMstTask
						   where CompanyCode = a.CompanyCode
							 and BasicModel = a.BasicModel
							 and OperationNo = b.OperationNo
						  ), 0)
		 , a.JobType		
		 --, case 
			--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 1000)  then 'FSC01'
			--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 5000)  then 'FSC02'
			--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 10000) then 'FSC03'
			--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 20000) then 'FSC04'
			--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 30000) then 'FSC05'
			--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 40000) then 'FSC06'
			--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 50000) then 'FSC07'
			--  else ''
		 --  end as KsgType
		 , case 
			  when (c.GroupJobType = 'FSC' and c.WarrantyOdometer = 1000)  then 'FSC01'
			  when (c.GroupJobType = 'FSC' and c.WarrantyOdometer = 5000)  then 'FSC02'
			  when (c.GroupJobType = 'FSC' and c.WarrantyOdometer = 10000) then 'FSC03'
			  when (c.GroupJobType = 'FSC' and c.WarrantyOdometer = 20000) then 'FSC04'
			  when (c.GroupJobType = 'FSC' and c.WarrantyOdometer = 30000) then 'FSC05'
			  when (c.GroupJobType = 'FSC' and c.WarrantyOdometer = 40000) then 'FSC06'
			  when (c.GroupJobType = 'FSC' and c.WarrantyOdometer = 50000) then 'FSC07'
			  else ''
		   end as KsgType
		   ,a.CustomerCode
		   ,a.PoliceRegNo
	  from svTrnInvoice a
	 inner join svTrnInvTask b
		on b.CompanyCode = a.CompanyCode
	   and b.BranchCode = a.BranchCode
	   and b.InvoiceNo = a.InvoiceNo
	  left join svMstJob c
		on c.CompanyCode = a.CompanyCode
	   and c.BasicModel = a.BasicModel
	   and c.JobType = a.JobType
	 where a.CompanyCode = 6419401
	   and a.BranchCode = ".$branchcode."
	   and Year(a.Invoicedate) = ".$year."
	   and Month(a.Invoicedate) =".$month."
	   and a.JobType != 'REWORK'
)  table1 where table1.IsSubCon=0 and (substring(table1.InvoiceNo, 1 ,3) = 'INI'
or substring(table1.InvoiceNo, 1 ,3) = 'INF' or substring(table1.InvoiceNo, 1 ,3) = 'INW') 
and (JobType = 'PDI' or JobType != 'PDI')

) as table2
left join
gnMstCustomer as e on table2.CustomerCode=e.CustomerCode

union all

select 'Lubricant Sales - Chargeable' as asal, sum(spramt) as total from (

select a.InvoiceNo
		 , c.ParaValue as GroupTpgo
		 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
		 , case 
			 when left(a.InvoiceNo, 3) = 'INI' or a.JobType = 'PDI' then 'INT,PDI'
			 when left(a.InvoiceNo, 3) = 'INW' or left(a.InvoiceNo, 3) = 'INF' then 'CLM,FSC'
			 else 'CPUS'
		   end GroupJob
		 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
		  , b.typeofgoods
	  from svTrnInvoice a
	 inner join svTrnInvItem b
		on b.CompanyCode = a.CompanyCode
	   and b.BranchCode = a.BranchCode
	   and b.InvoiceNo = a.InvoiceNo
	  left join gnMstLookupDtl c
		on c.CompanyCode = a.CompanyCode
	   and c.CodeID = 'GTGO'
	   and c.LookupValue = b.TypeOfGoods
	 where a.CompanyCode = 6419401
	   and a.BranchCode = ".$branchcode."
	   and Year(a.Invoicedate) = ".$year."
	   and Month(a.Invoicedate) = ".$month."
	   and a.JobType != 'REWORK'
) table11 where IsSublet = 0 and GroupTpgo != 'SPAREPART' and GroupJob = 'CPUS'";




	$query=$this->dbDms->query($sql);
		return $query->result_array();

 }
 	
 }




 

 public function getfullRekapBySA1($branchcode,$year,$month){

  
//if($branchcode=="641940101" || $branchcode=="641940104"){
if($branchcode=="641940101" ){
     	$sql=" declare @MPMS as bit
set @MPMS = '0'
if (select isnull(ParaValue,0) from gnMstLookUpDtl with (nolock,nowait) where CodeID = 'MPMS') = '1' set @MPMS = '1'


select SaID, SaName,OperationHour, TaskAmt, TaskDiscAmt,TaskDppAmt, PartsAmt, PartsDiscAmt, PartsDppAmt,  totaltaskdppamt,cast(round((cast(TaskDppAmt as float)/totaltaskdppamt*100),4) as decimal(12,4)) as persentase
from (
select SaID, SaName,sum(OperationHour) as OperationHour,sum(TaskAmt) as TaskAmt,sum(TaskDiscAmt) as TaskDiscAmt,sum(TaskDppAmt) as TaskDppAmt,sum(PartsAmt) as PartsAmt,sum(PartsDiscAmt) as PartsDiscAmt,sum(PartsDppAmt) as PartsDppAmt,  totaltaskdppamt
from (
select SaID, replace(SaName,' (SUBCON)','') as SaName,sum(OperationHour) as OperationHour,sum(TaskAmt) as TaskAmt,sum(TaskDiscAmt) as TaskDiscAmt,sum(TaskDppAmt) as TaskDppAmt,sum(PartsAmt) as PartsAmt,sum(PartsDiscAmt) as PartsDiscAmt,sum(PartsDppAmt) as PartsDppAmt, sum(totaltaskdppamt) as totaltaskdppamt
from (
select SaID, SaName,OperationHour,TaskAmt,TaskDiscAmt,TaskDppAmt,PartsAmt,PartsDiscAmt,PartsDppAmt, totaltaskdppamt,
cast(round((cast(TaskDppAmt as float)/totaltaskdppamt*100),4) as decimal(12,4)) as persentase
from
(

select x.SaID, x.SaName
     , sum(case x.TaskPartSeq when 1 then isnull(x.OperationHour,0) else 0 end) OperationHour
     , sum(case x.TaskPartSeq when 1 then x.TaskPartAmt else 0 end) TaskAmt
     , sum(case x.TaskPartSeq when 1 then x.TaskPartDiscAmt else 0 end) TaskDiscAmt
     , sum(case x.TaskPartSeq when 1 then x.TaskPartDppAmt1 else 0 end) TaskDppAmt
     , sum(case x.TaskPartSeq when 2 then x.TaskPartAmt else 0 end) PartsAmt
     , sum(case x.TaskPartSeq when 2 then x.TaskPartDiscAmt else 0 end) PartsDiscAmt
     , sum(case x.TaskPartSeq when 2 then x.TaskPartDppAmt1 else 0 end) PartsDppAmt
     , (
      select 
       sum(case x.TaskPartSeq when 1 then x.TaskPartDppAmt1 else 0 end) totaltaskdppamt
    
 


from 
(

SELECT 
    tx.ForemanID AS SaId
    ,tx.ForemanName AS SaName
    , tx.SaName AS EmployeeName
     , tx.InvoiceNo AS InvoiceNo
     , tx.InvoiceDate AS InvoiceDate
     , '1' AS TaskPartSeq
     , tx.OperationNo
     , tx.OperationHour
     , tx.TaskPartAmt
     , tx.TaskPartDiscAmt
     , tx.TaskPartDppAmt
     , tx.TaskDescription
     , cast((TaskPartAmt - TaskPartDiscAmt) as decimal(18,0)) as taskpartdppamt1
from (
select 
    case when d.MechanicID is null then c.ForemanID
        else 
        
          ( case when @MPMS = '0' then 
            isnull
            ( 
                (select isnull(employeeID,'') from gnMstEmployee with (nolock,nowait) where CompanyCode = 6419401 and BranchCode = 641940101 and employeeid=c.ForemanID and personnelstatus = '1'),
                (select isnull(employeeID,'') from gnMstEmployee with (nolock,nowait) where CompanyCode = 6419401 and BranchCode = 641940101 and employeename like '%'+c.ForemanID+'%'and personnelstatus = '1')
            ) 
            else
            isnull
            ( 
                (select isnull(employeeID,'') from HrEmployee with (nolock,nowait) where CompanyCode = 6419401  and employeeid=c.ForemanID and personnelstatus = '1'),
                (select isnull(employeeID,'') from HrEmployee with (nolock,nowait) where CompanyCode = 6419401  and employeename like '%'+c.ForemanID+'%'and personnelstatus = '1')
            )end)

    end as ForemanID,
    case when d.MechanicID is null then 
    
    (case when @MPMS = '0'then (select employeename + ' (SUBCON)' from gnMstEmployee with (nolock,nowait) where employeeid=c.ForemanID and BranchCode=c.BranchCode) 
    else (select employeename + ' (SUBCON)' from HrEmployee with (nolock,nowait) where employeeid=c.ForemanID)end)

        else 
          ( case when @MPMS = '0' then 
            isnull
            ( 
                (select isnull(employeename,'') from gnMstEmployee with (nolock,nowait) where CompanyCode = 6419401 and BranchCode = 641940101 and employeeid=c.ForemanID and personnelstatus = '1'),
                (select isnull(employeename,'') from gnMstEmployee with (nolock,nowait) where CompanyCode = 6419401 and BranchCode = 641940101 and employeename like '%'+c.ForemanID+'%'and personnelstatus = '1')
            ) 
            else
            isnull
            ( 
                (select isnull(employeename,'') from HrEmployee  with (nolock,nowait) where CompanyCode = 6419401  and employeeid=c.ForemanID and personnelstatus = '1'),
                (select isnull(employeename,'') from HrEmployee with (nolock,nowait) where CompanyCode = 6419401  and employeename like '%'+c.ForemanID+'%'and personnelstatus = '1')
            )end)
    end as ForemanName
     , a.InvoiceDate
     , a.InvoiceNo
     , '1' TaskPartSeq
     , isnull(d.MechanicID, 'SUB') SaID
     , case when @MPMS = '0' then isnull(e.EmployeeName, 'SUBCON') else isnull(f.EmployeeName, 'SUBCON') end SaName
     , c.JobOrderNo
     , b.IsSubCon
     , b.OperationNo
     , b.SharingTask
     , case when (b.SharingTask > 0) then (b.OperationHour / b.SharingTask) else b.OperationHour end OperationHour
     , case when (b.SharingTask > 0) then ((b.OperationHour * b.OperationCost) / b.SharingTask) else (b.OperationHour * b.OperationCost) end TaskPartAmt
     , case when (b.SharingTask > 0) then ((b.OperationHour * b.OperationCost * b.DiscPct * 0.01) / b.SharingTask) else (b.OperationHour * b.OperationCost * b.DiscPct * 0.01) end TaskPartDiscAmt
     , convert(decimal, 0) as TaskPartDppAmt
     , isnull((
        select top 1 Description from svMstTask with (nolock,nowait)
         where CompanyCode = a.CompanyCode
           and ProductType = a.ProductType
           and BasicModel = a.BasicModel
           and OperationNo = b.OperationNo
         order by a.JobType 
         ), 0) TaskDescription
  from svTrnInvoice a with (nolock,nowait)
  left join svTrnInvTask b with (nolock,nowait)
    on b.CompanyCode = a.CompanyCode
   and b.BranchCode = a.BranchCode
   and b.InvoiceNo = a.InvoiceNo
  left join svTrnService c with (nolock,nowait)
    on c.CompanyCode = a.CompanyCode
   and c.BranchCode = a.BranchCode
   and c.JobOrderNo = a.JobOrderNo
   left join svTrnInvMechanic d with (nolock,nowait)
    on d.CompanyCode = a.CompanyCode
   and d.BranchCode = a.BranchCode
   and d.InvoiceNo = a.InvoiceNo   
   and d.OperationNo = b.OperationNo
   left join gnMstEmployee e with (nolock,nowait)
    on e.CompanyCode = a.CompanyCode
   and e.BranchCode = a.BranchCode
   and e.EmployeeID = d.MechanicID  
   and e.personnelstatus = '1' 
 left join HrEmployee f with (nolock,nowait)
    on f.CompanyCode = a.CompanyCode
   and f.EmployeeID = d.MechanicID
   and f.personnelstatus = '1' 
 where 1 = 1
   and a.CompanyCode = 6419401
   and a.BranchCode = 641940101
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")   
   and isnull(b.OperationNo, '') <> '' 
   and isnull(b.IsSubCon, 0) = 0
   and c.servicestatus not in ('6')
) as tx --where ForemanName not like '%syarifudin%'


union 

select * from (
select c.ForemanID as SaId
     , case when @MPMS = '0' then d.EmployeeName else e.EmployeeName end + ' (SUBCON)' as SaName
     , case when @MPMS = '0' then d.EmployeeName else e.EmployeeName end EmployeeName
     , a.InvoiceNo
     , a.InvoiceDate
     , '1' TaskPartSeq
     , b.OperationNo
     , b.OperationHour
     , b.OperationHour * b.OperationCost TaskPartAmt
     , b.OperationHour * b.OperationCost * b.DiscPct * 0.01 TaskPartDiscAmt
     , convert(decimal, 0) as TaskPartDppAmt
     , isnull((
        select top 1 Description from svMstTask with (nolock,nowait)
         where CompanyCode = a.CompanyCode
           and ProductType = a.ProductType
           and BasicModel = a.BasicModel
           and OperationNo = b.OperationNo
         order by a.JobType 
         ), 0) TaskDescription
    ,cast((b.OperationHour * b.OperationCost) - (b.OperationHour * b.OperationCost * b.DiscPct * 0.01) as decimal(18,0)) as TaskPartDppAmt1
  from svTrnInvoice a with (nolock,nowait)
  left join svTrnInvTask b with (nolock,nowait)
    on b.CompanyCode = a.CompanyCode
   and b.BranchCode = a.BranchCode
   and b.InvoiceNo = a.InvoiceNo
  left join svTrnService c with (nolock,nowait)
    on c.CompanyCode = a.CompanyCode
   and c.BranchCode = a.BranchCode
   and c.JobOrderNo = a.JobOrderNo
  left join gnMstEmployee d with (nolock,nowait)
    on d.CompanyCode = a.CompanyCode
   and d.BranchCode = a.BranchCode
   and d.EmployeeID = c.ForemanID
  left join HrEmployee e with (nolock,nowait)
    on e.CompanyCode = a.CompanyCode
   and e.EmployeeID = c.ForemanID
 where 1 = 1
   and a.CompanyCode = 6419401
   and a.BranchCode = 641940101
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")   
   and isnull(b.OperationNo, '') <> ''
   and isnull(b.IsSubCon, 0) = 1
) as table2 --where SaName not like '%syarifudin%'

union

select saID,SaName,EmployeeName,InvoiceNo,InvoiceDate,TaskPartSeq,OperationNo,OperationHour,TaskPartAmt,TaskPartDiscAmt,TaskPartDppAmt,TaskDescription,
cast((TaskPartAmt - TaskPartDiscAmt) as decimal(18,0)) as TaskPartDppAmt1
from (
select c.ForemanID as SaId
     , case when @MPMS = '0' then d.EmployeeName else e.EmployeeName end as SaName
     , case when @MPMS = '0' then d.EmployeeName else e.EmployeeName end EmployeeName
     , a.InvoiceNo
     , a.InvoiceDate
     , '2' TaskPartSeq
     , 'SPAREPART' OperationNo
     , null OperationHour
     , isnull((
        select ceiling(sum((i.SupplyQty - i.ReturnQty) * i.RetailPrice))
          from svTrnInvItem i with (nolock,nowait)
          left join gnMstLookUpDtl j with (nolock,nowait)
            on j.CompanyCode = i.CompanyCode
           and j.CodeID = 'GTGO'
           and j.LookUpValue = i.TypeOfGoods
         where j.CompanyCode is not null
           and i.CompanyCode = a.CompanyCode
           and i.BranchCode  = a.BranchCode
           and i.InvoiceNo   = a.InvoiceNo
        ), 0) TaskPartAmt
     , isnull((
        select ceiling(sum((i.SupplyQty - i.ReturnQty) * i.RetailPrice * isnull(i.DiscPct, 0) * 0.01))
          from svTrnInvItem i with (nolock,nowait)
          left join gnMstLookUpDtl j with (nolock,nowait)
            on j.CompanyCode = i.CompanyCode
           and j.CodeID = 'GTGO'
           and j.ParaValue = 'SPAREPART'
           and j.LookUpValue = i.TypeOfGoods
         where j.CompanyCode is not null
           and i.CompanyCode = a.CompanyCode
           and i.BranchCode  = a.BranchCode
           and i.InvoiceNo   = a.InvoiceNo
        ), 0)
     + isnull((
        select ceiling(sum((i.SupplyQty - i.ReturnQty) * i.RetailPrice * isnull(i.DiscPct, 0) * 0.01))
          from svTrnInvItem i with (nolock,nowait)
          left join gnMstLookUpDtl j with (nolock,nowait)
            on j.CompanyCode = i.CompanyCode
           and j.CodeID = 'GTGO'
           and j.ParaValue <> 'SPAREPART'
           and j.LookUpValue = i.TypeOfGoods
         where j.CompanyCode is not null
           and i.CompanyCode = a.CompanyCode
           and i.BranchCode  = a.BranchCode
           and i.InvoiceNo   = a.InvoiceNo
        ), 0) TaskPartDiscAmt
     , convert(decimal, 0) as TaskPartDppAmt
     , null TaskDescription
  from svTrnInvoice a with (nolock,nowait)
  left join svTrnService c with (nolock,nowait)
    on c.CompanyCode = a.CompanyCode
   and c.BranchCode = a.BranchCode
   and c.JobOrderNo = a.JobOrderNo
  left join gnMstEmployee d with (nolock,nowait)
    on d.CompanyCode = a.CompanyCode
   and d.BranchCode = a.BranchCode
   and d.EmployeeID = c.ForemanID
  left join HrEmployee e with (nolock,nowait)
    on e.CompanyCode = a.CompanyCode
   and e.EmployeeID = c.ForemanID
 where 1 = 1
   and a.CompanyCode = 6419401
   and a.BranchCode = 641940101
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")   
) table3 where SaName not like '%syarifudin%'

) as x
 --where saname not like '%subcon%'




     ) totaltaskdppamt


    
     
 


from 
(

SELECT 
    tx.ForemanID AS SaId
    ,tx.ForemanName AS SaName
    , tx.SaName AS EmployeeName
     , tx.InvoiceNo AS InvoiceNo
     , tx.InvoiceDate AS InvoiceDate
     , '1' AS TaskPartSeq
     , tx.OperationNo
     , tx.OperationHour
     , tx.TaskPartAmt
     , tx.TaskPartDiscAmt
     , tx.TaskPartDppAmt
     , tx.TaskDescription
     , cast((TaskPartAmt - TaskPartDiscAmt) as decimal(18,0)) as taskpartdppamt1
from (
select 
    
    case when d.MechanicID is null then c.ForemanID
        else 
        
          ( case when @MPMS = '0' then 
            isnull
            ( 
                (select isnull(employeeID,'') from gnMstEmployee with (nolock,nowait) where CompanyCode = 6419401 and BranchCode = 641940101 and employeeid=c.ForemanID and personnelstatus = '1'),
                (select isnull(employeeID,'') from gnMstEmployee with (nolock,nowait) where CompanyCode = 6419401 and BranchCode = 641940101 and employeename like '%'+c.ForemanID+'%'and personnelstatus = '1')
            ) 
            else
            isnull
            ( 
                (select isnull(employeeID,'') from HrEmployee with (nolock,nowait) where CompanyCode = 6419401  and employeeid=c.ForemanID and personnelstatus = '1'),
                (select isnull(employeeID,'') from HrEmployee with (nolock,nowait) where CompanyCode = 6419401  and employeename like '%'+c.ForemanID+'%'and personnelstatus = '1')
            )end)

    end as ForemanID,
    case when d.MechanicID is null then 
    
    (case when @MPMS = '0'then (select employeename + ' (SUBCON)' from gnMstEmployee with (nolock,nowait) where employeeid=c.ForemanID and BranchCode=c.BranchCode) 
    else (select employeename + ' (SUBCON)' from HrEmployee with (nolock,nowait) where employeeid=c.ForemanID)end)

        else 
          ( case when @MPMS = '0' then 
            isnull
            ( 
                (select isnull(employeename,'') from gnMstEmployee with (nolock,nowait) where CompanyCode = 6419401 and BranchCode = 641940101 and employeeid=c.ForemanID and personnelstatus = '1'),
                (select isnull(employeename,'') from gnMstEmployee with (nolock,nowait) where CompanyCode = 6419401 and BranchCode = 641940101 and employeename like '%'+c.ForemanID+'%'and personnelstatus = '1')
            ) 
            else
            isnull
            ( 
                (select isnull(employeename,'') from HrEmployee  with (nolock,nowait) where CompanyCode = 6419401  and employeeid=c.ForemanID and personnelstatus = '1'),
                (select isnull(employeename,'') from HrEmployee with (nolock,nowait) where CompanyCode = 6419401  and employeename like '%'+c.ForemanID+'%'and personnelstatus = '1')
            )end)
    end as ForemanName
     , a.InvoiceDate
     , a.InvoiceNo
     , '1' TaskPartSeq
     , isnull(d.MechanicID, 'SUB') SaID
     , case when @MPMS = '0' then isnull(e.EmployeeName, 'SUBCON') else isnull(f.EmployeeName, 'SUBCON') end SaName
     , c.JobOrderNo
     , b.IsSubCon
     , b.OperationNo
     , b.SharingTask
     , case when (b.SharingTask > 0) then (b.OperationHour / b.SharingTask) else b.OperationHour end OperationHour
     , case when (b.SharingTask > 0) then ((b.OperationHour * b.OperationCost) / b.SharingTask) else (b.OperationHour * b.OperationCost) end TaskPartAmt
     , case when (b.SharingTask > 0) then ((b.OperationHour * b.OperationCost * b.DiscPct * 0.01) / b.SharingTask) else (b.OperationHour * b.OperationCost * b.DiscPct * 0.01) end TaskPartDiscAmt
     , convert(decimal, 0) as TaskPartDppAmt
     , isnull((
        select top 1 Description from svMstTask with (nolock,nowait)
         where CompanyCode = a.CompanyCode
           and ProductType = a.ProductType
           and BasicModel = a.BasicModel
           and OperationNo = b.OperationNo
         order by a.JobType 
         ), 0) TaskDescription
  from svTrnInvoice a with (nolock,nowait)
  left join svTrnInvTask b with (nolock,nowait)
    on b.CompanyCode = a.CompanyCode
   and b.BranchCode = a.BranchCode
   and b.InvoiceNo = a.InvoiceNo
  left join svTrnService c with (nolock,nowait)
    on c.CompanyCode = a.CompanyCode
   and c.BranchCode = a.BranchCode
   and c.JobOrderNo = a.JobOrderNo
   left join svTrnInvMechanic d with (nolock,nowait)
    on d.CompanyCode = a.CompanyCode
   and d.BranchCode = a.BranchCode
   and d.InvoiceNo = a.InvoiceNo   
   and d.OperationNo = b.OperationNo
   left join gnMstEmployee e with (nolock,nowait)
    on e.CompanyCode = a.CompanyCode
   and e.BranchCode = a.BranchCode
   and e.EmployeeID = d.MechanicID  
   and e.personnelstatus = '1' 
 left join HrEmployee f with (nolock,nowait)
    on f.CompanyCode = a.CompanyCode
   and f.EmployeeID = d.MechanicID
   and f.personnelstatus = '1' 
 where 1 = 1
   and a.CompanyCode = 6419401
   and a.BranchCode = 641940101
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")   
   and isnull(b.OperationNo, '') <> '' 
   and isnull(b.IsSubCon, 0) = 0
   and c.servicestatus not in ('6')
) as tx --where ForemanName not like '%syarifudin%'


union 

select * from (
select c.ForemanID as SaId
     , case when @MPMS = '0' then d.EmployeeName else e.EmployeeName end + ' (SUBCON)' as SaName
     , case when @MPMS = '0' then d.EmployeeName else e.EmployeeName end EmployeeName
     , a.InvoiceNo
     , a.InvoiceDate
     , '1' TaskPartSeq
     , b.OperationNo
     , b.OperationHour
     , b.OperationHour * b.OperationCost TaskPartAmt
     , b.OperationHour * b.OperationCost * b.DiscPct * 0.01 TaskPartDiscAmt
     , convert(decimal, 0) as TaskPartDppAmt
     , isnull((
        select top 1 Description from svMstTask with (nolock,nowait)
         where CompanyCode = a.CompanyCode
           and ProductType = a.ProductType
           and BasicModel = a.BasicModel
           and OperationNo = b.OperationNo
         order by a.JobType 
         ), 0) TaskDescription
    ,cast((b.OperationHour * b.OperationCost) - (b.OperationHour * b.OperationCost * b.DiscPct * 0.01) as decimal(18,0)) as TaskPartDppAmt1
  from svTrnInvoice a with (nolock,nowait)
  left join svTrnInvTask b with (nolock,nowait)
    on b.CompanyCode = a.CompanyCode
   and b.BranchCode = a.BranchCode
   and b.InvoiceNo = a.InvoiceNo
  left join svTrnService c with (nolock,nowait)
    on c.CompanyCode = a.CompanyCode
   and c.BranchCode = a.BranchCode
   and c.JobOrderNo = a.JobOrderNo
  left join gnMstEmployee d with (nolock,nowait)
    on d.CompanyCode = a.CompanyCode
   and d.BranchCode = a.BranchCode
   and d.EmployeeID = c.ForemanID
  left join HrEmployee e with (nolock,nowait)
    on e.CompanyCode = a.CompanyCode
   and e.EmployeeID = c.ForemanID
 where 1 = 1
   and a.CompanyCode = 6419401
   and a.BranchCode = 641940101
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")   
   and isnull(b.OperationNo, '') <> ''
   and isnull(b.IsSubCon, 0) = 1
) as table2 --where saname not like '%syarifudin%'

union

select saID,SaName,EmployeeName,InvoiceNo,InvoiceDate,TaskPartSeq,OperationNo,OperationHour,TaskPartAmt,TaskPartDiscAmt,TaskPartDppAmt,TaskDescription,
cast((TaskPartAmt - TaskPartDiscAmt) as decimal(18,0)) as TaskPartDppAmt1
from (
select c.ForemanID as SaId
     , case when @MPMS = '0' then d.EmployeeName else e.EmployeeName end as SaName
     , case when @MPMS = '0' then d.EmployeeName else e.EmployeeName end EmployeeName
     , a.InvoiceNo
     , a.InvoiceDate
     , '2' TaskPartSeq
     , 'SPAREPART' OperationNo
     , null OperationHour
     , isnull((
        select ceiling(sum((i.SupplyQty - i.ReturnQty) * i.RetailPrice))
          from svTrnInvItem i with (nolock,nowait)
          left join gnMstLookUpDtl j with (nolock,nowait)
            on j.CompanyCode = i.CompanyCode
           and j.CodeID = 'GTGO'
           and j.LookUpValue = i.TypeOfGoods
         where j.CompanyCode is not null
           and i.CompanyCode = a.CompanyCode
           and i.BranchCode  = a.BranchCode
           and i.InvoiceNo   = a.InvoiceNo
        ), 0) TaskPartAmt
     , isnull((
        select ceiling(sum((i.SupplyQty - i.ReturnQty) * i.RetailPrice * isnull(i.DiscPct, 0) * 0.01))
          from svTrnInvItem i with (nolock,nowait)
          left join gnMstLookUpDtl j with (nolock,nowait)
            on j.CompanyCode = i.CompanyCode
           and j.CodeID = 'GTGO'
           and j.ParaValue = 'SPAREPART'
           and j.LookUpValue = i.TypeOfGoods
         where j.CompanyCode is not null
           and i.CompanyCode = a.CompanyCode
           and i.BranchCode  = a.BranchCode
           and i.InvoiceNo   = a.InvoiceNo
        ), 0)
     + isnull((
        select ceiling(sum((i.SupplyQty - i.ReturnQty) * i.RetailPrice * isnull(i.DiscPct, 0) * 0.01))
          from svTrnInvItem i with (nolock,nowait)
          left join gnMstLookUpDtl j with (nolock,nowait)
            on j.CompanyCode = i.CompanyCode
           and j.CodeID = 'GTGO'
           and j.ParaValue <> 'SPAREPART'
           and j.LookUpValue = i.TypeOfGoods
         where j.CompanyCode is not null
           and i.CompanyCode = a.CompanyCode
           and i.BranchCode  = a.BranchCode
           and i.InvoiceNo   = a.InvoiceNo
        ), 0) TaskPartDiscAmt
     , convert(decimal, 0) as TaskPartDppAmt
     , null TaskDescription
  from svTrnInvoice a with (nolock,nowait)
  left join svTrnService c with (nolock,nowait)
    on c.CompanyCode = a.CompanyCode
   and c.BranchCode = a.BranchCode
   and c.JobOrderNo = a.JobOrderNo
  left join gnMstEmployee d with (nolock,nowait)
    on d.CompanyCode = a.CompanyCode
   and d.BranchCode = a.BranchCode
   and d.EmployeeID = c.ForemanID
  left join HrEmployee e with (nolock,nowait)
    on e.CompanyCode = a.CompanyCode
   and e.EmployeeID = c.ForemanID
 where 1 = 1
   and a.CompanyCode = 6419401
   and a.BranchCode = 641940101
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")   
) table3 --where saname not like '%syarifudin%'

) as x
 --where saname not like '%subcon%'
 group by x.SaID, x.SaName
 ) table_total
 ) as table1 group by said, saname
  ) as table2 group by said, saname,totaltaskdppamt
) table_finish
";


 $query=$this->dbDms->query($sql);
		return $query->result_array();

}else{





 


 	$sql="declare @MPMS as bit
set @MPMS = '0'
if (select isnull(ParaValue,0) from gnMstLookUpDtl with (nolock,nowait) where CodeID = 'MPMS') = '1' set @MPMS = '1'

select SaID, SaName,OperationHour,TaskAmt,TaskDiscAmt,TaskDppAmt,PartsAmt,PartsDiscAmt,PartsDppAmt, totaltaskdppamt,
cast(round((cast(TaskDppAmt as float)/totaltaskdppamt*100),4) as decimal(12,4)) as persentase
from
(

select x.SaID, x.SaName
     , sum(case x.TaskPartSeq when 1 then isnull(x.OperationHour,0) else 0 end) OperationHour
     , sum(case x.TaskPartSeq when 1 then x.TaskPartAmt else 0 end) TaskAmt
     , sum(case x.TaskPartSeq when 1 then x.TaskPartDiscAmt else 0 end) TaskDiscAmt
     , sum(case x.TaskPartSeq when 1 then x.TaskPartDppAmt1 else 0 end) TaskDppAmt
     , sum(case x.TaskPartSeq when 2 then x.TaskPartAmt else 0 end) PartsAmt
     , sum(case x.TaskPartSeq when 2 then x.TaskPartDiscAmt else 0 end) PartsDiscAmt
     , sum(case x.TaskPartSeq when 2 then x.TaskPartDppAmt1 else 0 end) PartsDppAmt
	 , (
	  select 
	   sum(case x.TaskPartSeq when 1 then x.TaskPartDppAmt1 else 0 end) totaltaskdppamt
    
 


from 
(

SELECT 
	tx.ForemanID AS SaId
	,tx.ForemanName AS SaName
	, tx.SaName AS EmployeeName
     , tx.InvoiceNo AS InvoiceNo
     , tx.InvoiceDate AS InvoiceDate
     , '1' AS TaskPartSeq
     , tx.OperationNo
     , tx.OperationHour
     , tx.TaskPartAmt
     , tx.TaskPartDiscAmt
     , tx.TaskPartDppAmt
     , tx.TaskDescription
	 , cast((TaskPartAmt - TaskPartDiscAmt) as decimal(18,0)) as taskpartdppamt1
from (
select 
	case when d.MechanicID is null then c.ForemanID
		else 
		
		  ( case when @MPMS = '0' then 
			isnull
			( 
				(select isnull(employeeID,'') from gnMstEmployee with (nolock,nowait) where CompanyCode = 6419401 and BranchCode = ".$branchcode." and employeeid=c.ForemanID and personnelstatus = '1'),
				(select isnull(employeeID,'') from gnMstEmployee with (nolock,nowait) where CompanyCode = 6419401 and BranchCode = ".$branchcode." and employeename like '%'+c.ForemanID+'%'and personnelstatus = '1')
			) 
			else
			isnull
			( 
				(select isnull(employeeID,'') from HrEmployee with (nolock,nowait) where CompanyCode = 6419401  and employeeid=c.ForemanID and personnelstatus = '1'),
				(select isnull(employeeID,'') from HrEmployee with (nolock,nowait) where CompanyCode = 6419401  and employeename like '%'+c.ForemanID+'%'and personnelstatus = '1')
			)end)

	end as ForemanID,
	case when d.MechanicID is null then 
	
	(case when @MPMS = '0'then (select employeename + ' (SUBCON)' from gnMstEmployee with (nolock,nowait) where employeeid=c.ForemanID and BranchCode=c.BranchCode) 
	else (select employeename + ' (SUBCON)' from HrEmployee with (nolock,nowait) where employeeid=c.ForemanID)end)

		else 
		  ( case when @MPMS = '0' then 
			isnull
			( 
				(select isnull(employeename,'') from gnMstEmployee with (nolock,nowait) where CompanyCode = 6419401 and BranchCode = ".$branchcode." and employeeid=c.ForemanID and personnelstatus = '1'),
				(select isnull(employeename,'') from gnMstEmployee with (nolock,nowait) where CompanyCode = 6419401 and BranchCode = ".$branchcode." and employeename like '%'+c.ForemanID+'%'and personnelstatus = '1')
			) 
			else
			isnull
			( 
				(select isnull(employeename,'') from HrEmployee  with (nolock,nowait) where CompanyCode = 6419401  and employeeid=c.ForemanID and personnelstatus = '1'),
				(select isnull(employeename,'') from HrEmployee with (nolock,nowait) where CompanyCode = 6419401  and employeename like '%'+c.ForemanID+'%'and personnelstatus = '1')
			)end)
	end as ForemanName
     , a.InvoiceDate
	 , a.InvoiceNo
     , '1' TaskPartSeq
     , isnull(d.MechanicID, 'SUB') SaID
     , case when @MPMS = '0' then isnull(e.EmployeeName, 'SUBCON') else isnull(f.EmployeeName, 'SUBCON') end SaName
     , c.JobOrderNo
     , b.IsSubCon
     , b.OperationNo
     , b.SharingTask
     , case when (b.SharingTask > 0) then (b.OperationHour / b.SharingTask) else b.OperationHour end OperationHour
     , case when (b.SharingTask > 0) then ((b.OperationHour * b.OperationCost) / b.SharingTask) else (b.OperationHour * b.OperationCost) end TaskPartAmt
     , case when (b.SharingTask > 0) then ((b.OperationHour * b.OperationCost * b.DiscPct * 0.01) / b.SharingTask) else (b.OperationHour * b.OperationCost * b.DiscPct * 0.01) end TaskPartDiscAmt
     , convert(decimal, 0) as TaskPartDppAmt
     , isnull((
		select top 1 Description from svMstTask with (nolock,nowait)
		 where CompanyCode = a.CompanyCode
		   and ProductType = a.ProductType
		   and BasicModel = a.BasicModel
		   and OperationNo = b.OperationNo
		 order by a.JobType 
		 ), 0) TaskDescription
  from svTrnInvoice a with (nolock,nowait)
  left join svTrnInvTask b with (nolock,nowait)
	on b.CompanyCode = a.CompanyCode
   and b.BranchCode = a.BranchCode
   and b.InvoiceNo = a.InvoiceNo
  left join svTrnService c with (nolock,nowait)
	on c.CompanyCode = a.CompanyCode
   and c.BranchCode = a.BranchCode
   and c.JobOrderNo = a.JobOrderNo
   left join svTrnInvMechanic d with (nolock,nowait)
	on d.CompanyCode = a.CompanyCode
   and d.BranchCode = a.BranchCode
   and d.InvoiceNo = a.InvoiceNo   
   and d.OperationNo = b.OperationNo
   left join gnMstEmployee e with (nolock,nowait)
	on e.CompanyCode = a.CompanyCode
   and e.BranchCode = a.BranchCode
   and e.EmployeeID = d.MechanicID  
   and e.personnelstatus = '1' 
 left join HrEmployee f with (nolock,nowait)
    on f.CompanyCode = a.CompanyCode
   and f.EmployeeID = d.MechanicID
   and f.personnelstatus = '1' 
 where 1 = 1
   and a.CompanyCode = 6419401
   and a.BranchCode = ".$branchcode."
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")   
   and isnull(b.OperationNo, '') <> '' 
   and isnull(b.IsSubCon, 0) = 0
   and c.servicestatus not in ('6')
) as tx


union 


select c.ForemanID as SaId
     , case when @MPMS = '0' then d.EmployeeName else e.EmployeeName end + ' (SUBCON)' as SaName
     , case when @MPMS = '0' then d.EmployeeName else e.EmployeeName end EmployeeName
     , a.InvoiceNo
     , a.InvoiceDate
     , '1' TaskPartSeq
     , b.OperationNo
     , b.OperationHour
     , b.OperationHour * b.OperationCost TaskPartAmt
     , b.OperationHour * b.OperationCost * b.DiscPct * 0.01 TaskPartDiscAmt
     , convert(decimal, 0) as TaskPartDppAmt
     , isnull((
		select top 1 Description from svMstTask with (nolock,nowait)
		 where CompanyCode = a.CompanyCode
		   and ProductType = a.ProductType
		   and BasicModel = a.BasicModel
		   and OperationNo = b.OperationNo
		 order by a.JobType 
		 ), 0) TaskDescription
	,cast((b.OperationHour * b.OperationCost) - (b.OperationHour * b.OperationCost * b.DiscPct * 0.01) as decimal(18,0)) as TaskPartDppAmt1
  from svTrnInvoice a with (nolock,nowait)
  left join svTrnInvTask b with (nolock,nowait)
	on b.CompanyCode = a.CompanyCode
   and b.BranchCode = a.BranchCode
   and b.InvoiceNo = a.InvoiceNo
  left join svTrnService c with (nolock,nowait)
	on c.CompanyCode = a.CompanyCode
   and c.BranchCode = a.BranchCode
   and c.JobOrderNo = a.JobOrderNo
  left join gnMstEmployee d with (nolock,nowait)
	on d.CompanyCode = a.CompanyCode
   and d.BranchCode = a.BranchCode
   and d.EmployeeID = c.ForemanID
  left join HrEmployee e with (nolock,nowait)
    on e.CompanyCode = a.CompanyCode
   and e.EmployeeID = c.ForemanID
 where 1 = 1
   and a.CompanyCode = 6419401
   and a.BranchCode = ".$branchcode."
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")   
   and isnull(b.OperationNo, '') <> ''
   and isnull(b.IsSubCon, 0) = 1

union

select saID,SaName,EmployeeName,InvoiceNo,InvoiceDate,TaskPartSeq,OperationNo,OperationHour,TaskPartAmt,TaskPartDiscAmt,TaskPartDppAmt,TaskDescription,
cast((TaskPartAmt - TaskPartDiscAmt) as decimal(18,0)) as TaskPartDppAmt1
from (
select c.ForemanID as SaId
     , case when @MPMS = '0' then d.EmployeeName else e.EmployeeName end as SaName
     , case when @MPMS = '0' then d.EmployeeName else e.EmployeeName end EmployeeName
     , a.InvoiceNo
     , a.InvoiceDate
     , '2' TaskPartSeq
     , 'SPAREPART' OperationNo
     , null OperationHour
	 , isnull((
		select ceiling(sum((i.SupplyQty - i.ReturnQty) * i.RetailPrice))
		  from svTrnInvItem i with (nolock,nowait)
		  left join gnMstLookUpDtl j with (nolock,nowait)
			on j.CompanyCode = i.CompanyCode
		   and j.CodeID = 'GTGO'
		   and j.LookUpValue = i.TypeOfGoods
		 where j.CompanyCode is not null
  		   and i.CompanyCode = a.CompanyCode
  		   and i.BranchCode  = a.BranchCode
		   and i.InvoiceNo   = a.InvoiceNo
		), 0) TaskPartAmt
	 , isnull((
		select ceiling(sum((i.SupplyQty - i.ReturnQty) * i.RetailPrice * isnull(i.DiscPct, 0) * 0.01))
		  from svTrnInvItem i with (nolock,nowait)
		  left join gnMstLookUpDtl j with (nolock,nowait)
			on j.CompanyCode = i.CompanyCode
		   and j.CodeID = 'GTGO'
		   and j.ParaValue = 'SPAREPART'
		   and j.LookUpValue = i.TypeOfGoods
		 where j.CompanyCode is not null
  		   and i.CompanyCode = a.CompanyCode
  		   and i.BranchCode  = a.BranchCode
		   and i.InvoiceNo   = a.InvoiceNo
		), 0)
	 + isnull((
		select ceiling(sum((i.SupplyQty - i.ReturnQty) * i.RetailPrice * isnull(i.DiscPct, 0) * 0.01))
		  from svTrnInvItem i with (nolock,nowait)
		  left join gnMstLookUpDtl j with (nolock,nowait)
			on j.CompanyCode = i.CompanyCode
		   and j.CodeID = 'GTGO'
		   and j.ParaValue <> 'SPAREPART'
		   and j.LookUpValue = i.TypeOfGoods
		 where j.CompanyCode is not null
  		   and i.CompanyCode = a.CompanyCode
  		   and i.BranchCode  = a.BranchCode
		   and i.InvoiceNo   = a.InvoiceNo
		), 0) TaskPartDiscAmt
     , convert(decimal, 0) as TaskPartDppAmt
     , null TaskDescription
  from svTrnInvoice a with (nolock,nowait)
  left join svTrnService c with (nolock,nowait)
	on c.CompanyCode = a.CompanyCode
   and c.BranchCode = a.BranchCode
   and c.JobOrderNo = a.JobOrderNo
  left join gnMstEmployee d with (nolock,nowait)
	on d.CompanyCode = a.CompanyCode
   and d.BranchCode = a.BranchCode
   and d.EmployeeID = c.ForemanID
  left join HrEmployee e with (nolock,nowait)
    on e.CompanyCode = a.CompanyCode
   and e.EmployeeID = c.ForemanID
 where 1 = 1
   and a.CompanyCode = 6419401
   and a.BranchCode = ".$branchcode."
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")   
) table3

) as x
 where saname not like '%subcon%'




	 ) totaltaskdppamt


	
	 
 


from 
(

SELECT 
	tx.ForemanID AS SaId
	,tx.ForemanName AS SaName
	, tx.SaName AS EmployeeName
     , tx.InvoiceNo AS InvoiceNo
     , tx.InvoiceDate AS InvoiceDate
     , '1' AS TaskPartSeq
     , tx.OperationNo
     , tx.OperationHour
     , tx.TaskPartAmt
     , tx.TaskPartDiscAmt
     , tx.TaskPartDppAmt
     , tx.TaskDescription
	 , cast((TaskPartAmt - TaskPartDiscAmt) as decimal(18,0)) as taskpartdppamt1
from (
select 
	
	case when d.MechanicID is null then c.ForemanID
		else 
		
		  ( case when @MPMS = '0' then 
			isnull
			( 
				(select isnull(employeeID,'') from gnMstEmployee with (nolock,nowait) where CompanyCode = 6419401 and BranchCode = ".$branchcode." and employeeid=c.ForemanID and personnelstatus = '1'),
				(select isnull(employeeID,'') from gnMstEmployee with (nolock,nowait) where CompanyCode = 6419401 and BranchCode = ".$branchcode." and employeename like '%'+c.ForemanID+'%'and personnelstatus = '1')
			) 
			else
			isnull
			( 
				(select isnull(employeeID,'') from HrEmployee with (nolock,nowait) where CompanyCode = 6419401  and employeeid=c.ForemanID and personnelstatus = '1'),
				(select isnull(employeeID,'') from HrEmployee with (nolock,nowait) where CompanyCode = 6419401  and employeename like '%'+c.ForemanID+'%'and personnelstatus = '1')
			)end)

	end as ForemanID,
	case when d.MechanicID is null then 
	
	(case when @MPMS = '0'then (select employeename + ' (SUBCON)' from gnMstEmployee with (nolock,nowait) where employeeid=c.ForemanID and BranchCode=c.BranchCode) 
	else (select employeename + ' (SUBCON)' from HrEmployee with (nolock,nowait) where employeeid=c.ForemanID)end)

		else 
		  ( case when @MPMS = '0' then 
			isnull
			( 
				(select isnull(employeename,'') from gnMstEmployee with (nolock,nowait) where CompanyCode = 6419401 and BranchCode = ".$branchcode." and employeeid=c.ForemanID and personnelstatus = '1'),
				(select isnull(employeename,'') from gnMstEmployee with (nolock,nowait) where CompanyCode = 6419401 and BranchCode = ".$branchcode." and employeename like '%'+c.ForemanID+'%'and personnelstatus = '1')
			) 
			else
			isnull
			( 
				(select isnull(employeename,'') from HrEmployee  with (nolock,nowait) where CompanyCode = 6419401  and employeeid=c.ForemanID and personnelstatus = '1'),
				(select isnull(employeename,'') from HrEmployee with (nolock,nowait) where CompanyCode = 6419401  and employeename like '%'+c.ForemanID+'%'and personnelstatus = '1')
			)end)
	end as ForemanName
     , a.InvoiceDate
	 , a.InvoiceNo
     , '1' TaskPartSeq
     , isnull(d.MechanicID, 'SUB') SaID
     , case when @MPMS = '0' then isnull(e.EmployeeName, 'SUBCON') else isnull(f.EmployeeName, 'SUBCON') end SaName
     , c.JobOrderNo
     , b.IsSubCon
     , b.OperationNo
     , b.SharingTask
     , case when (b.SharingTask > 0) then (b.OperationHour / b.SharingTask) else b.OperationHour end OperationHour
     , case when (b.SharingTask > 0) then ((b.OperationHour * b.OperationCost) / b.SharingTask) else (b.OperationHour * b.OperationCost) end TaskPartAmt
     , case when (b.SharingTask > 0) then ((b.OperationHour * b.OperationCost * b.DiscPct * 0.01) / b.SharingTask) else (b.OperationHour * b.OperationCost * b.DiscPct * 0.01) end TaskPartDiscAmt
     , convert(decimal, 0) as TaskPartDppAmt
     , isnull((
		select top 1 Description from svMstTask with (nolock,nowait)
		 where CompanyCode = a.CompanyCode
		   and ProductType = a.ProductType
		   and BasicModel = a.BasicModel
		   and OperationNo = b.OperationNo
		 order by a.JobType 
		 ), 0) TaskDescription
  from svTrnInvoice a with (nolock,nowait)
  left join svTrnInvTask b with (nolock,nowait)
	on b.CompanyCode = a.CompanyCode
   and b.BranchCode = a.BranchCode
   and b.InvoiceNo = a.InvoiceNo
  left join svTrnService c with (nolock,nowait)
	on c.CompanyCode = a.CompanyCode
   and c.BranchCode = a.BranchCode
   and c.JobOrderNo = a.JobOrderNo
   left join svTrnInvMechanic d with (nolock,nowait)
	on d.CompanyCode = a.CompanyCode
   and d.BranchCode = a.BranchCode
   and d.InvoiceNo = a.InvoiceNo   
   and d.OperationNo = b.OperationNo
   left join gnMstEmployee e with (nolock,nowait)
	on e.CompanyCode = a.CompanyCode
   and e.BranchCode = a.BranchCode
   and e.EmployeeID = d.MechanicID  
   and e.personnelstatus = '1' 
 left join HrEmployee f with (nolock,nowait)
    on f.CompanyCode = a.CompanyCode
   and f.EmployeeID = d.MechanicID
   and f.personnelstatus = '1' 
 where 1 = 1
   and a.CompanyCode = 6419401
   and a.BranchCode = ".$branchcode."
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")   
   and isnull(b.OperationNo, '') <> '' 
   and isnull(b.IsSubCon, 0) = 0
   and c.servicestatus not in ('6')
) as tx


union 


select c.ForemanID as SaId
     , case when @MPMS = '0' then d.EmployeeName else e.EmployeeName end + ' (SUBCON)' as SaName
     , case when @MPMS = '0' then d.EmployeeName else e.EmployeeName end EmployeeName
     , a.InvoiceNo
     , a.InvoiceDate
     , '1' TaskPartSeq
     , b.OperationNo
     , b.OperationHour
     , b.OperationHour * b.OperationCost TaskPartAmt
     , b.OperationHour * b.OperationCost * b.DiscPct * 0.01 TaskPartDiscAmt
     , convert(decimal, 0) as TaskPartDppAmt
     , isnull((
		select top 1 Description from svMstTask with (nolock,nowait)
		 where CompanyCode = a.CompanyCode
		   and ProductType = a.ProductType
		   and BasicModel = a.BasicModel
		   and OperationNo = b.OperationNo
		 order by a.JobType 
		 ), 0) TaskDescription
	,cast((b.OperationHour * b.OperationCost) - (b.OperationHour * b.OperationCost * b.DiscPct * 0.01) as decimal(18,0)) as TaskPartDppAmt1
  from svTrnInvoice a with (nolock,nowait)
  left join svTrnInvTask b with (nolock,nowait)
	on b.CompanyCode = a.CompanyCode
   and b.BranchCode = a.BranchCode
   and b.InvoiceNo = a.InvoiceNo
  left join svTrnService c with (nolock,nowait)
	on c.CompanyCode = a.CompanyCode
   and c.BranchCode = a.BranchCode
   and c.JobOrderNo = a.JobOrderNo
  left join gnMstEmployee d with (nolock,nowait)
	on d.CompanyCode = a.CompanyCode
   and d.BranchCode = a.BranchCode
   and d.EmployeeID = c.ForemanID
  left join HrEmployee e with (nolock,nowait)
    on e.CompanyCode = a.CompanyCode
   and e.EmployeeID = c.ForemanID
 where 1 = 1
   and a.CompanyCode = 6419401
   and a.BranchCode = ".$branchcode."
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")   
   and isnull(b.OperationNo, '') <> ''
   and isnull(b.IsSubCon, 0) = 1

union

select saID,SaName,EmployeeName,InvoiceNo,InvoiceDate,TaskPartSeq,OperationNo,OperationHour,TaskPartAmt,TaskPartDiscAmt,TaskPartDppAmt,TaskDescription,
cast((TaskPartAmt - TaskPartDiscAmt) as decimal(18,0)) as TaskPartDppAmt1
from (
select c.ForemanID as SaId
     , case when @MPMS = '0' then d.EmployeeName else e.EmployeeName end as SaName
     , case when @MPMS = '0' then d.EmployeeName else e.EmployeeName end EmployeeName
     , a.InvoiceNo
     , a.InvoiceDate
     , '2' TaskPartSeq
     , 'SPAREPART' OperationNo
     , null OperationHour
	 , isnull((
		select ceiling(sum((i.SupplyQty - i.ReturnQty) * i.RetailPrice))
		  from svTrnInvItem i with (nolock,nowait)
		  left join gnMstLookUpDtl j with (nolock,nowait)
			on j.CompanyCode = i.CompanyCode
		   and j.CodeID = 'GTGO'
		   and j.LookUpValue = i.TypeOfGoods
		 where j.CompanyCode is not null
  		   and i.CompanyCode = a.CompanyCode
  		   and i.BranchCode  = a.BranchCode
		   and i.InvoiceNo   = a.InvoiceNo
		), 0) TaskPartAmt
	 , isnull((
		select ceiling(sum((i.SupplyQty - i.ReturnQty) * i.RetailPrice * isnull(i.DiscPct, 0) * 0.01))
		  from svTrnInvItem i with (nolock,nowait)
		  left join gnMstLookUpDtl j with (nolock,nowait)
			on j.CompanyCode = i.CompanyCode
		   and j.CodeID = 'GTGO'
		   and j.ParaValue = 'SPAREPART'
		   and j.LookUpValue = i.TypeOfGoods
		 where j.CompanyCode is not null
  		   and i.CompanyCode = a.CompanyCode
  		   and i.BranchCode  = a.BranchCode
		   and i.InvoiceNo   = a.InvoiceNo
		), 0)
	 + isnull((
		select ceiling(sum((i.SupplyQty - i.ReturnQty) * i.RetailPrice * isnull(i.DiscPct, 0) * 0.01))
		  from svTrnInvItem i with (nolock,nowait)
		  left join gnMstLookUpDtl j with (nolock,nowait)
			on j.CompanyCode = i.CompanyCode
		   and j.CodeID = 'GTGO'
		   and j.ParaValue <> 'SPAREPART'
		   and j.LookUpValue = i.TypeOfGoods
		 where j.CompanyCode is not null
  		   and i.CompanyCode = a.CompanyCode
  		   and i.BranchCode  = a.BranchCode
		   and i.InvoiceNo   = a.InvoiceNo
		), 0) TaskPartDiscAmt
     , convert(decimal, 0) as TaskPartDppAmt
     , null TaskDescription
  from svTrnInvoice a with (nolock,nowait)
  left join svTrnService c with (nolock,nowait)
	on c.CompanyCode = a.CompanyCode
   and c.BranchCode = a.BranchCode
   and c.JobOrderNo = a.JobOrderNo
  left join gnMstEmployee d with (nolock,nowait)
	on d.CompanyCode = a.CompanyCode
   and d.BranchCode = a.BranchCode
   and d.EmployeeID = c.ForemanID
  left join HrEmployee e with (nolock,nowait)
    on e.CompanyCode = a.CompanyCode
   and e.EmployeeID = c.ForemanID
 where 1 = 1
   and a.CompanyCode = 6419401
   and a.BranchCode = ".$branchcode."
   and (year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")   
) table3

) as x
 where saname not like '%subcon%'
 group by x.SaID, x.SaName
 ) table_total
";

$query=$this->dbDms->query($sql);
		return $query->result_array();

}







  //print_r(" ".$sql);

 	
//return $sql;

 }


 public function getListSpooring ($branchcode, $year, $month){
$sql ="select a.invoiceno, a.invoicedate, a.JobOrderNo, a.JobOrderdate, a.PoliceRegNo, 
d.customername,
b.OperationNo, 
c.Description,
b.OperationCost,
b.SubConPrice,
(b.OperationCost - b.SubConPrice) as margin

from 
svTrninvoice as a 
inner join 
svTrninvTask as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
left join svMstTask as c on a.BasicModel=c.BasicModel  and b.OperationNo=c.OperationNo
left join gnMstCustomer as d on a.CustomerCode=d.CustomerCode
where a.branchcode=".$branchcode."
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.") --and a.JobOrderNo='SPK/22/000396' 
and (c.description like '%spooring%' or c.description like '%BALANCING%')
--order by a.invoiceno";

$query=$this->dbDms->query($sql);
		return $query->result_array();
 }


 public function getlistchemical($branchcode, $year, $month){
 	$sql="select a.invoiceno, a.invoicedate, a.fpjno, a.fpjdate, a.JobOrderNo, a.JobOrderdate, a.PoliceRegNo,
g.Customername,
f.EmployeeName,
b.PartNo,
c.PartName,
b.RetailPrice,
b.supplyQty,
CAST((b.RetailPrice  * a.PpnPct/100) as int) as ppn,
CAST((b.retailprice + (b.RetailPrice  *a.PpnPct/100)) as int) as total,
b.CostPrice,
(b.retailprice  - b.CostPrice) as margin,

d.SupplierName

from 
svTrninvoice as a 
inner join 
svTrninvItem as b on a.branchcode=b.branchcode and a.invoiceno=b.invoiceno 
inner join
svtrnservice as e on a.branchcode=e.branchcode and a.JobOrderNo=e.JobOrderNo
left join gnMstEmployee as f on a.BranchCode=f.BranchCode and e.ForemanID=f.EmployeeID
left join spMstItemInfo as c on a.CompanyCode=c.CompanyCode  and b.PartNo=c.partno
left join gnMstSupplier as d on c.CompanyCode=d.CompanyCode and c.SupplierCode=d.SupplierCode
left join gnMstCustomer as g on a.CustomerCode=g.CustomerCode
where a.branchcode=".$branchcode."
and
(year(a.invoicedate)=".$year." and month(a.invoicedate)=".$month.")  
and c.partno in ('AL-DESINFEKTAN','AL-ENGINE COND','AL-ENGINE COND.','AL-ENGINE FLUSH','AL-ENGINE FLUSH GM')
--order by a.joborderno";

$query=$this->dbDms->query($sql);
		return $query->result_array();
 }


 public function getTargetRKA($branchcode, $year, $month){
 	$bulan="";
 	 if($month==1){
 	 	$bulan="jan";
 	 }else if($month==2){
 	 	$bulan="feb";
 	 }else if($month==3){
 	 	$bulan="mar";
 	 }else if($month==4){
 	 	$bulan="apr";
 	 }else if($month==5){
 	 	$bulan="mei";
 	 }else if($month==6){
 	 	$bulan="jun";
 	 }else if($month==7){
 	 	$bulan="jul";
 	 }else if($month==8){
 	 	$bulan="agu";
 	 }else if($month==9){
 	 	$bulan="sep";
 	 }else if($month==10){
 	 	$bulan="okt";
 	 }else if($month==11){
 	 	$bulan="nov";
 	 }else if($month==12){
 	 	$bulan="des";
 	 }

 	$sql="select 'labour' as asal, sum(".$bulan.") as total from targetrka where cabang='".$branchcode."' and tahun='".$year."' and tiperka='labour'
union ALL
select 'sparepart' as asal, sum(".$bulan.") as total from targetrka where cabang='".$branchcode."' and tahun='".$year."' and tiperka in ('sgp','sga')
union ALL
select 'lubricant' as asal, sum(".$bulan.") as total from targetrka where cabang='".$branchcode."' and tahun='".$year."' and tiperka in ('sgo')";

	$query=$this->db->query($sql);
	return $query->result_array();
 }


 public function getEmployeeForIncentChemical($branchcode){
  if($branchcode=="641940101"){
  	$sql="select EmployeeName as nama, case when position ='SPAREPARTS' then 'ADMINISTRASI SPAREPART' else position end as position
from (
SELECT a.CompanyCode, a.BranchCode, a.EmployeeID, a.EmployeeName, b.LookUpValueName as position
FROM   dbo.gnMstEmployee AS a
inner join gnMstLookUpDtl as b on a.CompanyCode=b.CompanyCode and b.codeid='titl' and a.TitleCode=b.ParaValue
WHERE (PersonnelStatus = '1')
--AND (TitleCode IN ('2', '3', '8', '9','10','7')) 
AND (TitleCode IN ( '3','9')) 
and BranchCode=".$branchcode." and employeename not in ('no foreman', 'syarifudin') 
) table1";
      $query=$this->dbDms->query($sql);
		return $query->result_array();
  }else{
  	$sql="select  EmployeeName as nama, case when position ='SPAREPARTS' then 'ADMINISTRASI SPAREPART' else position end as position
from (

SELECT a.CompanyCode, a.BranchCode, a.EmployeeID, a.EmployeeName, b.LookUpValueName as position
FROM   dbo.gnMstEmployee AS a
inner join gnMstLookUpDtl as b on a.CompanyCode=b.CompanyCode and b.codeid='titl' and a.TitleCode=b.ParaValue
WHERE (PersonnelStatus = '1')
--AND (TitleCode IN ('2', '3', '8', '9','10','7')) 
AND (TitleCode IN ( '3','9')) 
and BranchCode=".$branchcode." and employeename not in ('no foreman')
)table1";
      $query=$this->dbDms->query($sql);
		return $query->result_array();
  }
 	
 }


 public function getEmployeeForIncentSpooring($branchcode){

 if($branchcode=="641940101"){
 	$sql= "select  EmployeeName as nama, case when position='SPAREPARTS' then 'ADMINISTRASI SPAREPART' else position end as position 
from (
SELECT a.CompanyCode, a.BranchCode, a.EmployeeID, a.EmployeeName, b.LookUpValueName as position
FROM   dbo.gnMstEmployee AS a
inner join gnMstLookUpDtl as b on a.CompanyCode=b.CompanyCode and b.codeid='titl' and a.TitleCode=b.ParaValue
WHERE (PersonnelStatus = '1')
--AND (TitleCode IN ('2', '3', '8', '9', '6','10','7','27'))
AND (TitleCode IN ('3',  '9')) 
and a.BranchCode=".$branchcode."


) as table1 where employeename <>'syarifudin'";

 	 $query=$this->dbDms->query($sql);
		return $query->result_array();
 }else{

 	$sql= "select  EmployeeName as nama, case when position='SPAREPARTS' then 'ADMINISTRASI SPAREPART' else position end as position 
from (
SELECT a.CompanyCode, a.BranchCode, a.EmployeeID, a.EmployeeName, b.LookUpValueName as position
FROM   dbo.gnMstEmployee AS a
inner join gnMstLookUpDtl as b on a.CompanyCode=b.CompanyCode and b.codeid='titl' and a.TitleCode=b.ParaValue
WHERE (PersonnelStatus = '1')
--AND (TitleCode IN ('2', '3', '8', '9', '6','10','7','27')) 
AND (TitleCode IN ( '3', '9')) 
and a.BranchCode=".$branchcode."

) as table1";

 	 $query=$this->dbDms->query($sql);
		return $query->result_array();

 }
 	 

 }



 public function getpaymentN($branchcode, $year, $month, $monthmin1, $monthmin2){
 	$arrMonthmin1=explode(",",$monthmin1);
 	$arrMonthmin2=explode(",",$monthmin2);
 	// if($month<10){
  //       if(($month+1)<10){
  //           $paymentdateend=$year."0".($month+1).'10';
  //           $paymentdateendN=$year."0".($month+1).'10';
  //       }else{
  //           $paymentdateend=$year.($month+1).'10';
  //           $paymentdateendN=$year.($month+1).'10';
  //       }
 	// 	$paymentdatestart=$year."0".$month.'01';
 	// 	$paymentdatestartN=$year."0".$month.'11';
 		
 	// }else{
 	// 	$paymentdatestart=$year.$month.'01';
 	// 	$paymentdateend=$year.($month+1).'10';
 	// 	$paymentdatestartN=$year.$month.'11';
 	// 	$paymentdateendN=$year.($month+1).'10';
 	// }



    if($month<10){
        if(($month+1)>9){
            $paymentdateendN=$year.($month+1).'10';
            $paymentdateend=$year.($month+1).'10';
        }else{
            $paymentdateendN=$year."0".($month+1).'10';
            $paymentdateend=$year."0".($month+1).'10';
        }
        $paymentdatestart=$year."0".$month.'01';
        $paymentdatestartN=$year."0".$month.'11';
    }else{
        if(($month+1)>12){
            $paymentdateend=($year+1).'0'.(($month+1)-12).'10';
            $paymentdateendN=($year+1).'0'.(($month+1)-12).'10';
        }else{
            $paymentdateend=($year).(($month+1)).'10';
            $paymentdateendN=($year).(($month+1)).'10';
        }
        $paymentdatestart=$year.$month.'01';
        $paymentdatestartN=$year.$month.'11';
    }




 if($branchcode=='641940101'){

 	$sql="select status, month(invoicedate) as bulan, 
year(invoicedate) as tahun,sum(jasa_pay) as labour_payment, sum(part_pay) as part_payment, sum(oli_pay) as oli_payment from (

select table_ar.* from (
 select * from (
select  'N' status,
		companycode,
	    branchcode,
		invoiceno,
		invoicedate,
		JobOrderNo,
		JobOrderDate,
		CustomerCodeBill,
		customername,
		0 as jasa_awal,
		0 as part_awal,
		0 as oli_awal,
		jasa_MSI,
		part_msi,
		oli_msi,
		[Total Debet], 
	case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>jasa_msi then jasa_msi else  
		    case when (jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11)) end
		end
	end as jasa_pay
	,case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>(jasa_msi+part_msi) then part_msi else
		     case when (part_msi-((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else part_msi-((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>(jasa_msi+part_msi+oli_msi) then oli_msi else 
			case when (oli_msi-  ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else oli_msi-  ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			end
		end
	end as oli_pay
	

	,case when (table_invoice_full.paymentamt)=0 then jasa_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=jasa_msi then 0 else  jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11))
		end
	end as jasa_saldo
	,case when (table_invoice_full.paymentamt)=0 then part_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=(jasa_msi+part_msi) then 0 else
		     case when (part_msi=0) then 0 else
				((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			 end
		end
	end as part_saldo
	,case when (table_invoice_full.paymentamt)=0 then oli_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=(jasa_msi+part_msi+oli_msi) then 0 else 
			case when (oli_msi=0) then 0 else ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			end
		end
	end as oli_saldo
	
	,(paymentamt) as payemntamt
	--,ceiling(paymentamt/1.11) as asli_payment
	,databankkas
	,case when 
		ceiling(table_invoice_full.paymentamt/1.11)>=table_invoice_full.[total debet] then 'Lunas' else 'belum lunas' 
	end as keterangan 
from (
select table_invoice.*,

(

				select convert(decimal,isnull(SUM(paymentamt),0),0) from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940101 --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=641940101)
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestart."' and '".$paymentdateend."' --tgl ar n
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				) as tableBankKas where tablebankkas.CompanyCode=table_invoice.companycode and tablebankkas.branchcode=table_invoice.branchcode and tablebankkas.invoiceno=table_invoice.invoiceno
			


	  ) as paymentamt,
	  (

				select concat(bankcode,',',bankname,',',docno,',',docdate,',') from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401
					AND a.BranchCode = 641940101
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=641940101)
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestart."' and '".$paymentdateend."'
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				) as tableBankKas where tablebankkas.CompanyCode=table_invoice.companycode and tablebankkas.branchcode=table_invoice.branchcode and tablebankkas.invoiceno=table_invoice.invoiceno
				FOR XML PATH('')


	  ) as databankkas



	   from (
select a.CompanyCode
	  ,a.branchcode
      ,a.invoiceno
	  ,a.InvoiceDate
	  ,a.joborderno
	  ,a.joborderdate
	  ,a.CustomerCodeBill
	  ,b.customername
	  --,a.LaborDppAmt
	  --,a.PartsDppAmt
	  --,a.MaterialDppAmt
	  ,isnull(jasa.labour,0) as jasa_MSI
	  ,isnull(part.part,0) as part_MSI
	  ,isnull(oli.oli,0) as oli_MSI
	  ,(isnull(jasa.labour,0) + isnull(part.part,0)+isnull(oli.oli,0)) as [Total Debet]
	  

from 
	svtrninvoice as a
	left join gnmstcustomer as b on a.CompanyCode=b.CompanyCode and a.CustomerCodeBill=b.CustomerCode
	--start of jasa
	left join (
		select companycode, branchcode,invoiceno, sum(lbrdppamt) as labour from(
				select 
						a.CompanyCode
					   ,a.BranchCode
						,a.InvoiceNo
					   ,a.invoicedate
					   , a.FPJNo
						, a.InvoiceStatus
	   
						, b.OperationHour
						, b.operationcost
						, b.DiscPct
						, a.joborderdate
						, a.joborderNo
						, a.ProductType
						 , ceiling(b.OperationHour * b.OperationCost * (100.0 - b.DiscPct) * 0.01) LbrDppAmt
						 , a.LaborDppAmt as faktual_labour
						 , IsSubCon = isnull((
										  select top 1 IsSubCon from svMstTask
										   where CompanyCode = a.CompanyCode
											 and BasicModel = a.BasicModel
											 and OperationNo = b.OperationNo
										  ), 0)
						 , a.JobType		
						 --, case 
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 1000)  then 'FSC01'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 5000)  then 'FSC02'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 10000) then 'FSC03'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 20000) then 'FSC04'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 30000) then 'FSC05'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 40000) then 'FSC06'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 50000) then 'FSC07'
							--  else ''
						 --  end as KsgType
						   ,a.CustomerCode
						   ,a.PoliceRegNo
					  from svTrnInvoice a
					 inner join svTrnInvTask b
						on b.CompanyCode = a.CompanyCode
					   and b.BranchCode = a.BranchCode
					   and b.InvoiceNo = a.InvoiceNo
					 -- left join svMstJob c
						--on c.CompanyCode = a.CompanyCode
					   --and c.BasicModel = a.BasicModel
					   --and c.JobType = a.JobType
					 where 
						a.JobType != 'REWORK'
					 )  table1 where table1.IsSubCon=0 group by companycode, branchcode,invoiceno
	 ) as jasa on a.CompanyCode=jasa.CompanyCode and a.BranchCode=jasa.BranchCode and a.InvoiceNo=jasa.InvoiceNo
	 --end of jasa

	 --start of oli
	 left join (
			select companycode, branchcode, invoiceno, sum(spramt) as oli  from (	
				select a.*
					 , c.ParaValue as GroupTpgo
					 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
					 , case 
						 when left(a.InvoiceNo, 3) = 'INI' or a.JobType = 'PDI' then 'INT,PDI'
						 when left(a.InvoiceNo, 3) = 'INW' or left(a.InvoiceNo, 3) = 'INF' then 'CLM,FSC'
						 else 'CPUS'
					   end GroupJob
					 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
					  , b.typeofgoods
				  from svTrnInvoice a
				 inner join svTrnInvItem b
					on b.CompanyCode = a.CompanyCode
				   and b.BranchCode = a.BranchCode
				   and b.InvoiceNo = a.InvoiceNo
				  left join gnMstLookupDtl c
					on c.CompanyCode = a.CompanyCode
				   and c.CodeID = 'GTGO'
				   and c.LookupValue = b.TypeOfGoods
			) as table1 where IsSublet = 0 and GroupTpgo != 'SPAREPART' and JobType != 'REWORK' group by companycode, branchcode, invoiceno
     ) as oli on a.CompanyCode=oli.CompanyCode and a.BranchCode=oli.BranchCode and a.InvoiceNo=oli.InvoiceNo
	 --end of oli

	 --sparepart
	 left join (
		select companycode, branchcode, invoiceno, sum(total) as part 
			from (
			--Total Workshop Parts Sales Revenue
			select companycode, branchcode, invoiceno, sum(spramt) as total from (	
				select a.*
					 , c.ParaValue as GroupTpgo
					 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
					 , case 
						 when left(a.InvoiceNo, 3) = 'INI' or a.JobType = 'PDI' then 'INT,PDI'
						 when left(a.InvoiceNo, 3) = 'INW' or left(a.InvoiceNo, 3) = 'INF' then 'CLM,FSC'
						 else 'CPUS'
					   end GroupJob
					 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
					  , b.typeofgoods
				  from svTrnInvoice a
				 inner join svTrnInvItem b
					on b.CompanyCode = a.CompanyCode
				   and b.BranchCode = a.BranchCode
				   and b.InvoiceNo = a.InvoiceNo
				  left join gnMstLookupDtl c
					on c.CompanyCode = a.CompanyCode
				   and c.CodeID = 'GTGO'
				   and c.LookupValue = b.TypeOfGoods
				 where a.JobType != 'REWORK'
			) as table1 where IsSublet = 0 and GroupTpgo = 'SPAREPART' group by companycode, branchcode, invoiceno

			union all
			-- Total Suzuki Genuine Accessories Sales Revenue - Chargeable to Customer CPUS (External)
			select  companycode, branchcode, invoiceno, sum(dppamt) as total from (
			select 
								inv.InvoiceNo,
								inv.InvoiceDate,
								inv.CompanyCode,
								inv.BranchCode,
								year(inv.InvoiceDate) PeriodYear,
								MONTH(inv.InvoiceDate) PeriodMonth,
								ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
								'0' as origin
							from svTrnInvItem invi	
							left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
							where 
							invi.TypeOfGoods in ('2','5','6','9')
							--group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)

							union all

			select 
								inv.InvoiceNo,
								inv.InvoiceDate,
								inv.CompanyCode,
								inv.BranchCode,
								year(inv.InvoiceDate) PeriodYear,
								MONTH(inv.InvoiceDate) PeriodMonth,
								ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
								'1' as origin
							from svTrnInvItem invi	
							left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
							where invi.TypeOfGoods in ('7','8')
							--group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)
				
				) as accesoris	group by companycode, branchcode, invoiceno
	
			) as sparepart group by companycode, branchcode, invoiceno
	 ) as part on a.CompanyCode=part.CompanyCode and a.BranchCode=part.BranchCode and a.InvoiceNo=part.InvoiceNo
	 --end of sparepart

where (month(a.invoicedate)=".$month." and year(a.invoicedate)=".$year.") and a.BranchCode=641940101 and left(a.invoiceno,3)<>'INI'

union all

select a.CompanyCode
	  ,a.BranchCode
	  ,a.fpjno as invoiceno
	  ,a.fpjdate as invoicedate
	  --,a.InvoiceDate
	  ,null as joborderno
	  ,null as joborderdate
	  ,a.CustomerCodeBill
	  ,b.customername
	  --,0 as LaborDppAmt
	  --,a.totdppamt
	  --,0 as MaterialDppAmt
	  ,0 as jasa_MSI
	  ,a.totdppamt as part_MSI
	  ,0 as oli_MSI
	  ,a.totdppamt as [total invoice]
from 
	spTrnSFPJHdr as a
	left join gnmstcustomer as b on a.CompanyCode=b.CompanyCode and a.CustomerCodeBill=b.CustomerCode
where (month(a.fpjdate)=".$month." and year(a.FPJDATE)=".$year.") and a.BranchCode=641940101 and TypeOfGoods = '0' and  customername not like '%DUTA CENDANA ADIMANDIRI%' 
--order by a.FPJNO
) as table_invoice
--order by invoiceno
) as table_Invoice_full
--order by invoiceno
) as table_full --where --keterangan ='belum lunas' --and InvoiceNo='INW/22/000033' 
--order by invoiceno
) as table_ar --order by invoicedate, invoiceno
left join svtrnservice as sv on table_ar.companycode=sv.companycode and table_ar.branchcode=sv.branchcode and table_ar.joborderno=sv.joborderno
where (table_ar.branchcode=641940101  and sv.foremanid<>'061902008') or left(table_ar.invoiceno,3)='FPJ'


union all

select table_aroutstanding.* from (
select 'N-1' as status, 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
,jasa_pay
,part_pay
,oli_pay
,jasa_awal+jasa_msi-jasa_pay as jasa_saldo
,part_awal+part_msi-part_pay as part_saldo
,oli_awal+oli_msi-oli_pay as oli_saldo
,paymentamt
--,paymentamt_asli
,databankkas
,case when ceiling(paymentamt/1.11)>=total_debet then 'lunas' else 'belum lunas' end as keterangan
from (


select
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=jasa_awal then jasa_awal else  jasa_awal-(jasa_awal-ceiling (table_aroutstanding.paymentamt/1.11))
		end
	end as jasa_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal) then part_awal else
		     case when (part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))<0) then 0 else
				part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal+oli_awal) then oli_awal else 
			case when (oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11)))<0  then 0  else oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			end
		end
	end as oli_pay
,paymentamt
,ceiling(paymentamt/1.11) as paymentamt_asli
,databankkas

from (
select 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal1 as jasa_awal
,part_awal1 as part_awal
,oli_awal1 as oli_awal
,0 as jasa_msi
,0 as part_msi
,0 as oli_msi
,jasa_awal1+part_awal1+oli_awal1 as total_debet
,(

				select convert(decimal,isnull(SUM(paymentamt),0),0) from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940101 --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=641940101)
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar n-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
			


	  ) as paymentamt
	  ,(

				select concat(bankcode,',',bankname,',',docno,',',docdate,',') from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940101 --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=641940101)
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
				FOR XML PATH('')


	  ) as databankkas
from aroutstanding where keterangan='belum lunas' --and invoiceno='INW/22/000033'
) as table_aroutstanding
 ) as table_aroustanding_full 
--tambah filter foreman left join
-- order by invoiceno
 ) as table_aroutstanding
left join svtrnservice as sv on table_aroutstanding.companycode=sv.companycode and table_aroutstanding.branchcode=sv.branchcode and table_aroutstanding.joborderno=sv.joborderno

 where table_aroutstanding.branchcode=641940101 and (((month(table_aroutstanding.invoicedate)=".$arrMonthmin1[0]." and year(table_aroutstanding.invoicedate)=".$arrMonthmin1[1].")) and sv.foremanid<>'061902008') or (left(table_aroutstanding.invoiceno,3)='FPJ' and (month(table_aroutstanding.invoicedate)=".$arrMonthmin1[0]." and year(table_aroutstanding.invoicedate)=".$arrMonthmin1[1]."))



 union all

select table_aroutstanding.* from (
 select 'N-2' as status, 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
,jasa_pay
,part_pay
,oli_pay
,jasa_awal+jasa_msi-jasa_pay as jasa_saldo
,part_awal+part_msi-part_pay as part_saldo
,oli_awal+oli_msi-oli_pay as oli_saldo
,paymentamt
--,paymentamt_asli
,databankkas
,case when ceiling(paymentamt/1.11)>=total_debet then 'lunas' else 'belum lunas' end as keterangan
from (


select
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=jasa_awal then jasa_awal else  jasa_awal-(jasa_awal-ceiling (table_aroutstanding.paymentamt/1.11))
		end
	end as jasa_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal) then part_awal else
		     case when (part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))<0) then 0 else
				part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal+oli_awal) then oli_awal else 
			case when (oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11)))<0  then 0  else oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			end
		end
	end as oli_pay
,paymentamt
,ceiling(paymentamt/1.11) as paymentamt_asli
,databankkas

from (
select 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal1 as jasa_awal
,part_awal1 as part_awal
,oli_awal1 as oli_awal
,0 as jasa_msi
,0 as part_msi
,0 as oli_msi
,jasa_awal1+part_awal1+oli_awal1 as total_debet
,(

				select convert(decimal,isnull(SUM(paymentamt),0),0) from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940101 --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=641940101)
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar n-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
			


	  ) as paymentamt
	  ,(

				select concat(bankcode,',',bankname,',',docno,',',docdate,',') from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940101 --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=641940101)
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
				FOR XML PATH('')


	  ) as databankkas
from aroutstanding where keterangan='belum lunas' --and invoiceno='INW/22/000033'
) as table_aroutstanding
 ) as table_aroustanding_full 
--left join
  -- order by invoiceno
 ) as table_aroutstanding
left join svtrnservice as sv on table_aroutstanding.companycode=sv.companycode and table_aroutstanding.branchcode=sv.branchcode and table_aroutstanding.joborderno=sv.joborderno


 where table_aroutstanding.branchcode=641940101 and (((month(table_aroutstanding.invoicedate)=".$arrMonthmin2[0]." and year(table_aroutstanding.invoicedate)=".$arrMonthmin2[1].")  ) and sv.foremanid<>'061902008') or (left(table_aroutstanding.invoiceno,3)='FPJ' and (month(table_aroutstanding.invoicedate)=".$arrMonthmin2[0]." and year(table_aroutstanding.invoicedate)=".$arrMonthmin2[1]."))






) as table_full_ar group by month(invoicedate), year(invoicedate), status
";



$query=$this->dbDms->query($sql);
	return $query->result_array();

 }else if($branchcode=='641940104'){
 	$sql="select status, month(invoicedate) as bulan, 
year(invoicedate) as tahun,sum(jasa_pay) as labour_payment, sum(part_pay) as part_payment, sum(oli_pay) as oli_payment from (


select table_ar.* from (
 select * from (
select  'N' status,
        companycode,
        branchcode,
        invoiceno,
        invoicedate,
        JobOrderNo,
        JobOrderDate,
        CustomerCodeBill,
        customername,
        0 as jasa_awal,
        0 as part_awal,
        0 as oli_awal,
        jasa_MSI,
        part_msi,
        oli_msi,
        [Total Debet], 
    case when (table_invoice_full.paymentamt)=0 then 0
        else 
          case when ceiling (table_invoice_full.paymentamt/1.11)>jasa_msi then jasa_msi else  
            case when (jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11)) end
        end
    end as jasa_pay
    ,case when (table_invoice_full.paymentamt)=0 then 0
        else 
          case when ceiling (table_invoice_full.paymentamt/1.11)>(jasa_msi+part_msi) then part_msi else
             case when (part_msi-((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else part_msi-((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))
             end
        end
    end as part_pay
    ,case when (table_invoice_full.paymentamt)=0 then 0
        else 
          case when ceiling (table_invoice_full.paymentamt/1.11)>(jasa_msi+part_msi+oli_msi) then oli_msi else 
            case when (oli_msi-  ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else oli_msi-  ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))
            end
        end
    end as oli_pay
    

    ,case when (table_invoice_full.paymentamt)=0 then jasa_msi
        else 
          case when ceiling (table_invoice_full.paymentamt/1.11)>=jasa_msi then 0 else  jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11))
        end
    end as jasa_saldo
    ,case when (table_invoice_full.paymentamt)=0 then part_msi
        else 
          case when ceiling (table_invoice_full.paymentamt/1.11)>=(jasa_msi+part_msi) then 0 else
             case when (part_msi=0) then 0 else
                ((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))
             end
        end
    end as part_saldo
    ,case when (table_invoice_full.paymentamt)=0 then oli_msi
        else 
          case when ceiling (table_invoice_full.paymentamt/1.11)>=(jasa_msi+part_msi+oli_msi) then 0 else 
            case when (oli_msi=0) then 0 else ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))
            end
        end
    end as oli_saldo
    
    ,(paymentamt) as payemntamt
    --,ceiling(paymentamt/1.11) as asli_payment
    ,databankkas
    ,case when 
        ceiling(table_invoice_full.paymentamt/1.11)>=table_invoice_full.[total debet] then 'Lunas' else 'belum lunas' 
    end as keterangan 
from (
select table_invoice.*,

(

                select convert(decimal,isnull(SUM(paymentamt),0),0) from(
                    SELECT 'kas' as asal
                    ,a.CompanyCode
                    ,a.BranchCode
                    ,f.LookUpValueName
                    ,a.DocNo
                    ,a.DocDate
                    ,c.InvoiceNo
                    ,c.InvoiceDate
                    ,c.CustomerCode
                    ,a.BankCode
                    ,d.BankName
                    ,a.AccountNo
                    ,a.ReceivAmt as TotalAmt
                    ,b.BankKasType
                    ,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
                    ,c.PaymentAmt
                    ,c.ReceivableAmt
                    ,c.ReceivableOutstand
                    ,c.Description
    

                    ,c.AccountNo as AccountInv
                    ,isnull(i.Description, '') AccountInvDesc
                    ,isnull(j.Description, '') AccountDesc

                FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
                LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
                    AND a.BranchCode = b.BranchCode
                    AND a.DocNo = b.DocNo
                left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
                    AND a.BranchCode = c.BranchCode
                    AND a.DocNo = c.DocNo
                    AND b.CustomerCode = c.CustomerCode
                    AND b.BankKasType = c.BankKasType
 

    
                LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
                    AND a.BranchCode = d.BranchCode
                    AND a.BankCode = d.BankCode
                LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
                    AND b.CustomerCode = e.CustomerCode
                LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
                    AND f.CompanyCode = a.CompanyCode
                    AND f.CodeId = 'PFCN'
                LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
                    AND i.BranchCode = a.BranchCode
                    AND i.AccountNo = c.AccountNo
                LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
                    AND j.BranchCode = a.BranchCode
                    AND j.AccountNo = a.AccountNo   
                WHERE a.Status != '4'
                    --AND a.bType = '1'
                    AND a.CompanyCode = 6419401 --perusahaan
                    AND a.BranchCode = 641940104 --cabang
                    --AND c.ProfitCenterCode like '200'
                    --AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=641940101)
                    --and c.invoiceno in ('INF/22/000700')
                    and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestart."' and '".$paymentdateend."' --tgl ar n
                    AND a.Accountno like '%%'
                    AND c.AccountNo like '%%'
                    --AND c.Description like '%Pencairan%'
                ---order by c.InvoiceNo

                ) as tableBankKas where tablebankkas.CompanyCode=table_invoice.companycode and tablebankkas.branchcode=table_invoice.branchcode and tablebankkas.invoiceno=table_invoice.invoiceno
            


      ) as paymentamt,
      (

                select concat(bankcode,',',bankname,',',docno,',',docdate,',') from(
                    SELECT 'kas' as asal
                    ,a.CompanyCode
                    ,a.BranchCode
                    ,f.LookUpValueName
                    ,a.DocNo
                    ,a.DocDate
                    ,c.InvoiceNo
                    ,c.InvoiceDate
                    ,c.CustomerCode
                    ,a.BankCode
                    ,d.BankName
                    ,a.AccountNo
                    ,a.ReceivAmt as TotalAmt
                    ,b.BankKasType
                    ,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
                    ,c.PaymentAmt
                    ,c.ReceivableAmt
                    ,c.ReceivableOutstand
                    ,c.Description
    

                    ,c.AccountNo as AccountInv
                    ,isnull(i.Description, '') AccountInvDesc
                    ,isnull(j.Description, '') AccountDesc

                FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
                LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
                    AND a.BranchCode = b.BranchCode
                    AND a.DocNo = b.DocNo
                left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
                    AND a.BranchCode = c.BranchCode
                    AND a.DocNo = c.DocNo
                    AND b.CustomerCode = c.CustomerCode
                    AND b.BankKasType = c.BankKasType
 

    
                LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
                    AND a.BranchCode = d.BranchCode
                    AND a.BankCode = d.BankCode
                LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
                    AND b.CustomerCode = e.CustomerCode
                LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
                    AND f.CompanyCode = a.CompanyCode
                    AND f.CodeId = 'PFCN'
                LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
                    AND i.BranchCode = a.BranchCode
                    AND i.AccountNo = c.AccountNo
                LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
                    AND j.BranchCode = a.BranchCode
                    AND j.AccountNo = a.AccountNo   
                WHERE a.Status != '4'
                    --AND a.bType = '1'
                    AND a.CompanyCode = 6419401
                    AND a.BranchCode = 641940104
                    --AND c.ProfitCenterCode like '200'
                    --AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=641940101)
                    --and c.invoiceno in ('INF/22/000700')
                    and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestart."' and '".$paymentdateend."'
                    AND a.Accountno like '%%'
                    AND c.AccountNo like '%%'
                    --AND c.Description like '%Pencairan%'
                ---order by c.InvoiceNo

                ) as tableBankKas where tablebankkas.CompanyCode=table_invoice.companycode and tablebankkas.branchcode=table_invoice.branchcode and tablebankkas.invoiceno=table_invoice.invoiceno
                FOR XML PATH('')


      ) as databankkas



       from (
select a.CompanyCode
      ,a.branchcode
      ,a.invoiceno
      ,a.InvoiceDate
      ,a.joborderno
      ,a.joborderdate
      ,a.CustomerCodeBill
      ,b.customername
      --,a.LaborDppAmt
      --,a.PartsDppAmt
      --,a.MaterialDppAmt
      ,isnull(jasa.labour,0) as jasa_MSI
      ,isnull(part.part,0) as part_MSI
      ,isnull(oli.oli,0) as oli_MSI
      ,(isnull(jasa.labour,0) + isnull(part.part,0)+isnull(oli.oli,0)) as [Total Debet]
      

from 
    svtrninvoice as a
    left join gnmstcustomer as b on a.CompanyCode=b.CompanyCode and a.CustomerCodeBill=b.CustomerCode
    --start of jasa
    left join (
        select companycode, branchcode,invoiceno, sum(lbrdppamt) as labour from(
                select 
                        a.CompanyCode
                       ,a.BranchCode
                        ,a.InvoiceNo
                       ,a.invoicedate
                       , a.FPJNo
                        , a.InvoiceStatus
       
                        , b.OperationHour
                        , b.operationcost
                        , b.DiscPct
                        , a.joborderdate
                        , a.joborderNo
                        , a.ProductType
                         , ceiling(b.OperationHour * b.OperationCost * (100.0 - b.DiscPct) * 0.01) LbrDppAmt
                         , a.LaborDppAmt as faktual_labour
                         , IsSubCon = isnull((
                                          select top 1 IsSubCon from svMstTask
                                           where CompanyCode = a.CompanyCode
                                             and BasicModel = a.BasicModel
                                             and OperationNo = b.OperationNo
                                          ), 0)
                         , a.JobType        
                         --, case 
                            --  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 1000)  then 'FSC01'
                            --  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 5000)  then 'FSC02'
                            --  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 10000) then 'FSC03'
                            --  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 20000) then 'FSC04'
                            --  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 30000) then 'FSC05'
                            --  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 40000) then 'FSC06'
                            --  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 50000) then 'FSC07'
                            --  else ''
                         --  end as KsgType
                           ,a.CustomerCode
                           ,a.PoliceRegNo
                      from svTrnInvoice a
                     inner join svTrnInvTask b
                        on b.CompanyCode = a.CompanyCode
                       and b.BranchCode = a.BranchCode
                       and b.InvoiceNo = a.InvoiceNo
                     -- left join svMstJob c
                        --on c.CompanyCode = a.CompanyCode
                       --and c.BasicModel = a.BasicModel
                       --and c.JobType = a.JobType
                     where 
                        a.JobType != 'REWORK'
                     )  table1 where table1.IsSubCon=0 group by companycode, branchcode,invoiceno
     ) as jasa on a.CompanyCode=jasa.CompanyCode and a.BranchCode=jasa.BranchCode and a.InvoiceNo=jasa.InvoiceNo
     --end of jasa

     --start of oli
     left join (
            select companycode, branchcode, invoiceno, sum(spramt) as oli  from (   
                select a.*
                     , c.ParaValue as GroupTpgo
                     , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
                     , case 
                         when left(a.InvoiceNo, 3) = 'INI' or a.JobType = 'PDI' then 'INT,PDI'
                         when left(a.InvoiceNo, 3) = 'INW' or left(a.InvoiceNo, 3) = 'INF' then 'CLM,FSC'
                         else 'CPUS'
                       end GroupJob
                     , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
                      , b.typeofgoods
                  from svTrnInvoice a
                 inner join svTrnInvItem b
                    on b.CompanyCode = a.CompanyCode
                   and b.BranchCode = a.BranchCode
                   and b.InvoiceNo = a.InvoiceNo
                  left join gnMstLookupDtl c
                    on c.CompanyCode = a.CompanyCode
                   and c.CodeID = 'GTGO'
                   and c.LookupValue = b.TypeOfGoods
            ) as table1 where IsSublet = 0 and GroupTpgo != 'SPAREPART' and JobType != 'REWORK' group by companycode, branchcode, invoiceno
     ) as oli on a.CompanyCode=oli.CompanyCode and a.BranchCode=oli.BranchCode and a.InvoiceNo=oli.InvoiceNo
     --end of oli

     --sparepart
     left join (
        select companycode, branchcode, invoiceno, sum(total) as part 
            from (
            --Total Workshop Parts Sales Revenue
            select companycode, branchcode, invoiceno, sum(spramt) as total from (  
                select a.*
                     , c.ParaValue as GroupTpgo
                     , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
                     , case 
                         when left(a.InvoiceNo, 3) = 'INI' or a.JobType = 'PDI' then 'INT,PDI'
                         when left(a.InvoiceNo, 3) = 'INW' or left(a.InvoiceNo, 3) = 'INF' then 'CLM,FSC'
                         else 'CPUS'
                       end GroupJob
                     , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
                      , b.typeofgoods
                  from svTrnInvoice a
                 inner join svTrnInvItem b
                    on b.CompanyCode = a.CompanyCode
                   and b.BranchCode = a.BranchCode
                   and b.InvoiceNo = a.InvoiceNo
                  left join gnMstLookupDtl c
                    on c.CompanyCode = a.CompanyCode
                   and c.CodeID = 'GTGO'
                   and c.LookupValue = b.TypeOfGoods
                 where a.JobType != 'REWORK'
            ) as table1 where IsSublet = 0 and GroupTpgo = 'SPAREPART' group by companycode, branchcode, invoiceno

            union all
            -- Total Suzuki Genuine Accessories Sales Revenue - Chargeable to Customer CPUS (External)
            select  companycode, branchcode, invoiceno, sum(dppamt) as total from (
            select 
                                inv.InvoiceNo,
                                inv.InvoiceDate,
                                inv.CompanyCode,
                                inv.BranchCode,
                                year(inv.InvoiceDate) PeriodYear,
                                MONTH(inv.InvoiceDate) PeriodMonth,
                                ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
                                '0' as origin
                            from svTrnInvItem invi  
                            left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
                            where 
                            invi.TypeOfGoods in ('2','5','6','9')
                            --group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)

                            union all

            select 
                                inv.InvoiceNo,
                                inv.InvoiceDate,
                                inv.CompanyCode,
                                inv.BranchCode,
                                year(inv.InvoiceDate) PeriodYear,
                                MONTH(inv.InvoiceDate) PeriodMonth,
                                ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
                                '1' as origin
                            from svTrnInvItem invi  
                            left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
                            where invi.TypeOfGoods in ('7','8')
                            --group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)
                
                ) as accesoris  group by companycode, branchcode, invoiceno
    
            ) as sparepart group by companycode, branchcode, invoiceno
     ) as part on a.CompanyCode=part.CompanyCode and a.BranchCode=part.BranchCode and a.InvoiceNo=part.InvoiceNo
     --end of sparepart

where (month(a.invoicedate)=".$month." and year(a.invoicedate)=".$year.") and a.BranchCode=641940104 and left(a.invoiceno,3)<>'INI'

) as table_invoice
--order by invoiceno
) as table_Invoice_full
--order by invoiceno
) as table_full --where --keterangan ='belum lunas' --and InvoiceNo='INW/22/000033' 
--order by invoiceno
) as table_ar --order by invoicedate, invoiceno
--left join svtrnservice as sv on table_ar.companycode=sv.companycode and table_ar.branchcode=sv.branchcode and table_ar.joborderno=sv.joborderno
--where sv.foremanid='061902008'



union all

--- n-1 jataasih & ciawi


select table_aroutstanding.* from (
select 'N-1' as status, 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
,jasa_pay
,part_pay
,oli_pay
,jasa_awal+jasa_msi-jasa_pay as jasa_saldo
,part_awal+part_msi-part_pay as part_saldo
,oli_awal+oli_msi-oli_pay as oli_saldo
,paymentamt
--,paymentamt_asli
,databankkas
,case when ceiling(paymentamt/1.11)>=total_debet then 'lunas' else 'belum lunas' end as keterangan
from (


select
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=jasa_awal then jasa_awal else  jasa_awal-(jasa_awal-ceiling (table_aroutstanding.paymentamt/1.11))
		end
	end as jasa_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal) then part_awal else
		     case when (part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))<0) then 0 else
				part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal+oli_awal) then oli_awal else 
			case when (oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11)))<0  then 0  else oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			end
		end
	end as oli_pay
,paymentamt
,ceiling(paymentamt/1.11) as paymentamt_asli
,databankkas

from (
select 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal1 as jasa_awal
,part_awal1 as part_awal
,oli_awal1 as oli_awal
,0 as jasa_msi
,0 as part_msi
,0 as oli_msi
,jasa_awal1+part_awal1+oli_awal1 as total_debet
,(

				select convert(decimal,isnull(SUM(paymentamt),0),0) from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940101 --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=641940101)
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar n-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
			


	  ) as paymentamt
	  ,(

				select concat(bankcode,',',bankname,',',docno,',',docdate,',') from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940101 --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=641940101)
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
				FOR XML PATH('')


	  ) as databankkas
from aroutstanding where keterangan='belum lunas' --and invoiceno='INW/22/000033'
) as table_aroutstanding
 ) as table_aroustanding_full 
--tambah filter foreman left join
-- order by invoiceno
 ) as table_aroutstanding
left join svtrnservice as sv on table_aroutstanding.companycode=sv.companycode and table_aroutstanding.branchcode=sv.branchcode and table_aroutstanding.joborderno=sv.joborderno

 where table_aroutstanding.branchcode=641940101 and ((month(table_aroutstanding.invoicedate)=".$arrMonthmin1[0]." and year(table_aroutstanding.invoicedate)=".$arrMonthmin1[1].")) and sv.foremanid='061902008'


union all

--- n-1 jati asih


select table_aroutstanding.* from (
select 'N-1' as status, 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
,jasa_pay
,part_pay
,oli_pay
,jasa_awal+jasa_msi-jasa_pay as jasa_saldo
,part_awal+part_msi-part_pay as part_saldo
,oli_awal+oli_msi-oli_pay as oli_saldo
,paymentamt
--,paymentamt_asli
,databankkas
,case when ceiling(paymentamt/1.11)>=total_debet then 'lunas' else 'belum lunas' end as keterangan
from (


select
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=jasa_awal then jasa_awal else  jasa_awal-(jasa_awal-ceiling (table_aroutstanding.paymentamt/1.11))
		end
	end as jasa_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal) then part_awal else
		     case when (part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))<0) then 0 else
				part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal+oli_awal) then oli_awal else 
			case when (oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11)))<0  then 0  else oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			end
		end
	end as oli_pay
,paymentamt
,ceiling(paymentamt/1.11) as paymentamt_asli
,databankkas

from (
select 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal1 as jasa_awal
,part_awal1 as part_awal
,oli_awal1 as oli_awal
,0 as jasa_msi
,0 as part_msi
,0 as oli_msi
,jasa_awal1+part_awal1+oli_awal1 as total_debet
,(

				select convert(decimal,isnull(SUM(paymentamt),0),0) from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940104 --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=641940104)
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar n-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
			


	  ) as paymentamt
	  ,(

				select concat(bankcode,',',bankname,',',docno,',',docdate,',') from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940104 --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=641940104)
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
				FOR XML PATH('')


	  ) as databankkas
from aroutstanding where keterangan='belum lunas' --and invoiceno='INW/22/000033'
) as table_aroutstanding
 ) as table_aroustanding_full 
--tambah filter foreman left join
-- order by invoiceno
 ) as table_aroutstanding
--left join svtrnservice as sv on table_aroutstanding.companycode=sv.companycode and table_aroutstanding.branchcode=sv.branchcode and table_aroutstanding.joborderno=sv.joborderno

 where table_aroutstanding.branchcode=641940104 and ((month(table_aroutstanding.invoicedate)=".$arrMonthmin1[0]." and year(table_aroutstanding.invoicedate)=".$arrMonthmin1[1].")) --and sv.foremanid='061902008'



--- end n-1 jatiasih



--- end n-1 jati asih & ciawi

 union all

select table_aroutstanding.* from (
 select 'N-2' as status, 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
,jasa_pay
,part_pay
,oli_pay
,jasa_awal+jasa_msi-jasa_pay as jasa_saldo
,part_awal+part_msi-part_pay as part_saldo
,oli_awal+oli_msi-oli_pay as oli_saldo
,paymentamt
--,paymentamt_asli
,databankkas
,case when ceiling(paymentamt/1.11)>=total_debet then 'lunas' else 'belum lunas' end as keterangan
from (


select
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=jasa_awal then jasa_awal else  jasa_awal-(jasa_awal-ceiling (table_aroutstanding.paymentamt/1.11))
		end
	end as jasa_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal) then part_awal else
		     case when (part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))<0) then 0 else
				part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal+oli_awal) then oli_awal else 
			case when (oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11)))<0  then 0  else oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			end
		end
	end as oli_pay
,paymentamt
,ceiling(paymentamt/1.11) as paymentamt_asli
,databankkas

from (
select 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal1 as jasa_awal
,part_awal1 as part_awal
,oli_awal1 as oli_awal
,0 as jasa_msi
,0 as part_msi
,0 as oli_msi
,jasa_awal1+part_awal1+oli_awal1 as total_debet
,(

				select convert(decimal,isnull(SUM(paymentamt),0),0) from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940101 --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=641940101)
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar n-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
			


	  ) as paymentamt
	  ,(

				select concat(bankcode,',',bankname,',',docno,',',docdate,',') from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940101 --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=641940101)
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
				FOR XML PATH('')


	  ) as databankkas
from aroutstanding where keterangan='belum lunas' --and invoiceno='INW/22/000033'
) as table_aroutstanding
 ) as table_aroustanding_full 
--left join
  -- order by invoiceno
 ) as table_aroutstanding
left join svtrnservice as sv on table_aroutstanding.companycode=sv.companycode and table_aroutstanding.branchcode=sv.branchcode and table_aroutstanding.joborderno=sv.joborderno


 where table_aroutstanding.branchcode=641940101 and ((month(table_aroutstanding.invoicedate)=".$arrMonthmin2[0]." and year(table_aroutstanding.invoicedate)=".$arrMonthmin2[1].")  ) and sv.foremanid='061902008'






) as table_full_ar group by month(invoicedate), year(invoicedate), status
";

$query=$this->dbDms->query($sql);
	return $query->result_array();


 }else{
 	 	$sql="select status, month(invoicedate) as bulan, 
year(invoicedate) as tahun,sum(jasa_pay) as labour_payment, sum(part_pay) as part_payment, sum(oli_pay) as oli_payment from (

select * from (
 select * from (
select  'N' status,
		companycode,
	    branchcode,
		invoiceno,
		invoicedate,
		JobOrderNo,
		JobOrderDate,
		CustomerCodeBill,
		customername,
		0 as jasa_awal,
		0 as part_awal,
		0 as oli_awal,
		jasa_MSI,
		part_msi,
		oli_msi,
		[Total Debet], 
	case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>jasa_msi then jasa_msi else  
		    case when (jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11)) end
		end
	end as jasa_pay
	,case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>(jasa_msi+part_msi) then part_msi else
		     case when (part_msi-((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else part_msi-((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>(jasa_msi+part_msi+oli_msi) then oli_msi else 
			case when (oli_msi-  ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else oli_msi-  ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			end
		end
	end as oli_pay
	

	,case when (table_invoice_full.paymentamt)=0 then jasa_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=jasa_msi then 0 else  jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11))
		end
	end as jasa_saldo
	,case when (table_invoice_full.paymentamt)=0 then part_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=(jasa_msi+part_msi) then 0 else
		     case when (part_msi=0) then 0 else
				((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			 end
		end
	end as part_saldo
	,case when (table_invoice_full.paymentamt)=0 then oli_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=(jasa_msi+part_msi+oli_msi) then 0 else 
			case when (oli_msi=0) then 0 else ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			end
		end
	end as oli_saldo
	
	,(paymentamt) as payemntamt
	--,ceiling(paymentamt/1.11) as asli_payment
	,databankkas
	,case when 
		ceiling(table_invoice_full.paymentamt/1.11)>=table_invoice_full.[total debet] then 'Lunas' else 'belum lunas' 
	end as keterangan 
from (

select table_invoice.*,
tableBankKas.databankkas,
tablepayment.paymentamt





	   from (
select a.CompanyCode
	  ,a.branchcode
      ,a.invoiceno
	  ,a.InvoiceDate
	  ,a.joborderno
	  ,a.joborderdate
	  ,a.CustomerCodeBill
	  ,b.customername
	  --,a.LaborDppAmt
	  --,a.PartsDppAmt
	  --,a.MaterialDppAmt
	  ,isnull(jasa.labour,0) as jasa_MSI
	  ,isnull(part.part,0) as part_MSI
	  ,isnull(oli.oli,0) as oli_MSI
	  ,(isnull(jasa.labour,0) + isnull(part.part,0)+isnull(oli.oli,0)) as [Total Debet]
	  

from 
	svtrninvoice as a
	left join gnmstcustomer as b on a.CompanyCode=b.CompanyCode and a.CustomerCodeBill=b.CustomerCode
	--start of jasa
	left join (
		select companycode, branchcode,invoiceno, sum(lbrdppamt) as labour from(
				select 
						a.CompanyCode
					   ,a.BranchCode
						,a.InvoiceNo
					   ,a.invoicedate
					   , a.FPJNo
						, a.InvoiceStatus
	   
						, b.OperationHour
						, b.operationcost
						, b.DiscPct
						, a.joborderdate
						, a.joborderNo
						, a.ProductType
						 , ceiling(b.OperationHour * b.OperationCost * (100.0 - b.DiscPct) * 0.01) LbrDppAmt
						 , a.LaborDppAmt as faktual_labour
						 , IsSubCon = isnull((
										  select top 1 IsSubCon from svMstTask
										   where CompanyCode = a.CompanyCode
											 and BasicModel = a.BasicModel
											 and OperationNo = b.OperationNo
										  ), 0)
						 , a.JobType		
						
						   ,a.CustomerCode
						   ,a.PoliceRegNo
					  from svTrnInvoice a
					 inner join svTrnInvTask b
						on b.CompanyCode = a.CompanyCode
					   and b.BranchCode = a.BranchCode
					   and b.InvoiceNo = a.InvoiceNo
					 
					 where 
						a.JobType != 'REWORK'
					 )  table1 where table1.IsSubCon=0 group by companycode, branchcode,invoiceno
	 ) as jasa on a.CompanyCode=jasa.CompanyCode and a.BranchCode=jasa.BranchCode and a.InvoiceNo=jasa.InvoiceNo
	 --end of jasa

	 --start of oli
	 left join (
			select companycode, branchcode, invoiceno, sum(spramt) as oli  from (	
				select a.companycode, a.BranchCode, a.invoiceno, a.JobType
					 , c.ParaValue as GroupTpgo
					 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
					 , case 
						 when left(a.InvoiceNo, 3) = 'INI' or a.JobType = 'PDI' then 'INT,PDI'
						 when left(a.InvoiceNo, 3) = 'INW' or left(a.InvoiceNo, 3) = 'INF' then 'CLM,FSC'
						 else 'CPUS'
					   end GroupJob
					 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
					  , b.typeofgoods
				  from svTrnInvoice a
				 inner join svTrnInvItem b
					on b.CompanyCode = a.CompanyCode
				   and b.BranchCode = a.BranchCode
				   and b.InvoiceNo = a.InvoiceNo
				  left join gnMstLookupDtl c
					on c.CompanyCode = a.CompanyCode
				   and c.CodeID = 'GTGO'
				   and c.LookupValue = b.TypeOfGoods
			) as table1 where IsSublet = 0 and GroupTpgo != 'SPAREPART' and JobType != 'REWORK' group by companycode, branchcode, invoiceno
     ) as oli on a.CompanyCode=oli.CompanyCode and a.BranchCode=oli.BranchCode and a.InvoiceNo=oli.InvoiceNo
	 --end of oli

	 --sparepart
	 left join (
		select companycode, branchcode, invoiceno, sum(total) as part 
			from (
			--Total Workshop Parts Sales Revenue
			select companycode, branchcode, invoiceno, sum(spramt) as total from (	
				select a.companycode, a.branchcode, a.invoiceno, a.JobType
					 , c.ParaValue as GroupTpgo
					 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
					 , case 
						 when left(a.InvoiceNo, 3) = 'INI' or a.JobType = 'PDI' then 'INT,PDI'
						 when left(a.InvoiceNo, 3) = 'INW' or left(a.InvoiceNo, 3) = 'INF' then 'CLM,FSC'
						 else 'CPUS'
					   end GroupJob
					 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
					  , b.typeofgoods
				  from svTrnInvoice a
				 inner join svTrnInvItem b
					on b.CompanyCode = a.CompanyCode
				   and b.BranchCode = a.BranchCode
				   and b.InvoiceNo = a.InvoiceNo
				  left join gnMstLookupDtl c
					on c.CompanyCode = a.CompanyCode
				   and c.CodeID = 'GTGO'
				   and c.LookupValue = b.TypeOfGoods
				 where a.JobType != 'REWORK'
			) as table1 where IsSublet = 0 and GroupTpgo = 'SPAREPART' group by companycode, branchcode, invoiceno

			union all
			-- Total Suzuki Genuine Accessories Sales Revenue - Chargeable to Customer CPUS (External)
			select  companycode, branchcode, invoiceno, sum(dppamt) as total from (
			select 
								inv.InvoiceNo,
								inv.InvoiceDate,
								inv.CompanyCode,
								inv.BranchCode,
								year(inv.InvoiceDate) PeriodYear,
								MONTH(inv.InvoiceDate) PeriodMonth,
								ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
								'0' as origin
							from svTrnInvItem invi	
							left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
							where 
							invi.TypeOfGoods in ('2','5','6','9')
							--group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)

							union all

			select 
								inv.InvoiceNo,
								inv.InvoiceDate,
								inv.CompanyCode,
								inv.BranchCode,
								year(inv.InvoiceDate) PeriodYear,
								MONTH(inv.InvoiceDate) PeriodMonth,
								ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
								'1' as origin
							from svTrnInvItem invi	
							left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
							where invi.TypeOfGoods in ('7','8')
							--group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)
				
				) as accesoris	group by companycode, branchcode, invoiceno
	
			) as sparepart group by companycode, branchcode, invoiceno
	 ) as part on a.CompanyCode=part.CompanyCode and a.BranchCode=part.BranchCode and a.InvoiceNo=part.InvoiceNo
	 --end of sparepart

where (month(a.invoicedate)=".$month." and year(a.invoicedate)=".$year.") and a.BranchCode=".$branchcode." and left(a.invoiceno,3)<>'INI'

union all

select a.CompanyCode
	  ,a.BranchCode
	  ,a.fpjno as invoiceno
	  ,a.fpjdate as invoicedate
	  --,a.InvoiceDate
	  ,null as joborderno
	  ,null as joborderdate
	  ,a.CustomerCodeBill
	  ,b.customername
	  --,0 as LaborDppAmt
	  --,a.totdppamt
	  --,0 as MaterialDppAmt
	  ,0 as jasa_MSI
	  ,a.totdppamt as part_MSI
	  ,0 as oli_MSI
	  ,a.totdppamt as [total invoice]
from 
	spTrnSFPJHdr as a
	left join gnmstcustomer as b on a.CompanyCode=b.CompanyCode and a.CustomerCodeBill=b.CustomerCode
where (month(a.fpjdate)=".$month." and year(a.FPJDATE)=".$year.") and a.BranchCode=".$branchcode." and TypeOfGoods = '0' and  customername not like '%DUTA CENDANA ADIMANDIRI%' 
--order by a.FPJNO
) as table_invoice
left join
(
			select 
companycode,
branchcode,
invoiceno,

stuff((
select 
		table2.bankcode+','+ table2.bankname+','+ table2.docno
from (

SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.PaymentAmt
					
					,a.BankCode
					,d.BankName
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo
) as table2
where table2.branchcode=table1.branchcode and table2.invoiceno=table1.invoiceno for xml path(''),type).value('.','nvarchar(max)'),1,1,'') as databankkas









from (

SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.PaymentAmt
					
					,a.BankCode
					,d.BankName
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo
) as table1
group by companycode,branchcode, invoiceno
				
					

) as tableBankKas on tableBankKas.branchcode=table_invoice.branchcode and tableBankKas.invoiceno=table_invoice.invoiceno 

left  join

	  (

				
					SELECT 
					a.CompanyCode
					,a.BranchCode
					
					
					,c.InvoiceNo
					,sum(c.PaymentAmt) as paymentamt
					
					
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				group by a.CompanyCode
					,a.BranchCode
					,c.InvoiceNo

) as tablepayment on tablepayment.branchcode=table_invoice.branchcode and tablepayment.invoiceno=table_invoice.invoiceno 


--order by invoiceno
) as table_Invoice_full
--order by invoiceno
) as table_full --where --keterangan ='belum lunas' --and InvoiceNo='INW/22/000033' 
--order by invoiceno
) as table_ar --order by invoicedate, invoiceno


union all

select * from (
 select 'N-1' as status, 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
,jasa_pay
,part_pay
,oli_pay
,jasa_awal+jasa_msi-jasa_pay as jasa_saldo
,part_awal+part_msi-part_pay as part_saldo
,oli_awal+oli_msi-oli_pay as oli_saldo
,paymentamt
--,paymentamt_asli
,databankkas
,case when ceiling(paymentamt/1.11)>=total_debet then 'lunas' else 'belum lunas' end as keterangan
from (


select
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=jasa_awal then jasa_awal else  jasa_awal-(jasa_awal-ceiling (table_aroutstanding.paymentamt/1.11))
		end
	end as jasa_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal) then part_awal else
		     case when (part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))<0) then 0 else
				part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal+oli_awal) then oli_awal else 
			case when (oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11)))<0  then 0  else oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			end
		end
	end as oli_pay
,paymentamt
,ceiling(paymentamt/1.11) as paymentamt_asli
,databankkas

from (
select 
aroutstanding.companycode
,aroutstanding.branchcode
,aroutstanding.invoiceno
,aroutstanding.invoicedate
,aroutstanding.joborderno
,aroutstanding.joborderdate
,aroutstanding.customercodebill
,aroutstanding.customername
,jasa_awal1 as jasa_awal
,part_awal1 as part_awal
,oli_awal1 as oli_awal
,0 as jasa_msi
,0 as part_msi
,0 as oli_msi
,jasa_awal1+part_awal1+oli_awal1 as total_debet
,tablepayment.paymentamt
,tableBankKas.databankkas



from aroutstanding

left  join

	  (
			select 
companycode,
branchcode,
invoiceno,

stuff((
select 
		table2.bankcode+','+ table2.bankname+','+ table2.docno
from (

SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.PaymentAmt
					
					,a.BankCode
					,d.BankName
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo
) as table2
where table2.branchcode=table1.branchcode and table2.invoiceno=table1.invoiceno for xml path(''),type).value('.','nvarchar(max)'),1,1,'') as databankkas









from (

SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.PaymentAmt
					
					,a.BankCode
					,d.BankName
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo
) as table1
group by companycode,branchcode, invoiceno
				
					

) as tableBankKas on tableBankKas.branchcode=aroutstanding.branchcode and tableBankKas.invoiceno=aroutstanding.invoiceno 

left  join

	  (

				
					SELECT 
					a.CompanyCode
					,a.BranchCode
					
					
					,c.InvoiceNo
					,sum(c.PaymentAmt) as paymentamt
					
					
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				group by a.CompanyCode
					,a.BranchCode
					,c.InvoiceNo

) as tablepayment on tablepayment.branchcode=aroutstanding.branchcode and tablepayment.invoiceno=aroutstanding.invoiceno 

	  



where keterangan='belum lunas' and aroutstanding.branchcode=".$branchcode." and ((month(aroutstanding.invoicedate)=".$arrMonthmin1[0]." and year(aroutstanding.invoicedate)=".$arrMonthmin1[1].")  ) --and invoiceno='INW/22/000033'
) as table_aroutstanding
 ) as table_aroustanding_full 
-- order by invoiceno
 ) as table_aroutstanding where branchcode=".$branchcode." and ((month(invoicedate)=".$arrMonthmin1[0]." and year(invoicedate)=".$arrMonthmin1[1].")  )--cabang





 union all

select * from (
 select 'N-2' as status, 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
,jasa_pay
,part_pay
,oli_pay
,jasa_awal+jasa_msi-jasa_pay as jasa_saldo
,part_awal+part_msi-part_pay as part_saldo
,oli_awal+oli_msi-oli_pay as oli_saldo
,paymentamt
--,paymentamt_asli
,databankkas
,case when ceiling(paymentamt/1.11)>=total_debet then 'lunas' else 'belum lunas' end as keterangan
from (


select
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=jasa_awal then jasa_awal else  jasa_awal-(jasa_awal-ceiling (table_aroutstanding.paymentamt/1.11))
		end
	end as jasa_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal) then part_awal else
		     case when (part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))<0) then 0 else
				part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal+oli_awal) then oli_awal else 
			case when (oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11)))<0  then 0  else oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			end
		end
	end as oli_pay
,paymentamt
,ceiling(paymentamt/1.11) as paymentamt_asli
,databankkas

from (
select 
aroutstanding.companycode
,aroutstanding.branchcode
,aroutstanding.invoiceno
,aroutstanding.invoicedate
,aroutstanding.joborderno
,aroutstanding.joborderdate
,aroutstanding.customercodebill
,aroutstanding.customername
,jasa_awal1 as jasa_awal
,part_awal1 as part_awal
,oli_awal1 as oli_awal
,0 as jasa_msi
,0 as part_msi
,0 as oli_msi
,jasa_awal1+part_awal1+oli_awal1 as total_debet
,tablepayment.paymentamt
,tableBankKas.databankkas



from aroutstanding

left  join

	  (
			select 
companycode,
branchcode,
invoiceno,

stuff((
select 
		table2.bankcode+','+ table2.bankname+','+ table2.docno
from (

SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.PaymentAmt
					
					,a.BankCode
					,d.BankName
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo
) as table2
where table2.branchcode=table1.branchcode and table2.invoiceno=table1.invoiceno for xml path(''),type).value('.','nvarchar(max)'),1,1,'') as databankkas









from (

SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.PaymentAmt
					
					,a.BankCode
					,d.BankName
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo
) as table1
group by companycode,branchcode, invoiceno
				
					

) as tableBankKas on tableBankKas.branchcode=aroutstanding.branchcode and tableBankKas.invoiceno=aroutstanding.invoiceno 

left join

	  (

				
					SELECT 
					a.CompanyCode
					,a.BranchCode
					
					
					,c.InvoiceNo
					,sum(c.PaymentAmt) as paymentamt
					
					
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				group by a.CompanyCode
					,a.BranchCode
					,c.InvoiceNo

) as tablepayment on tablepayment.branchcode=aroutstanding.branchcode and tablepayment.invoiceno=aroutstanding.invoiceno 

	  



where keterangan='belum lunas' and aroutstanding.branchcode=".$branchcode." and ((month(aroutstanding.invoicedate)=".$arrMonthmin2[0]." and year(aroutstanding.invoicedate)=".$arrMonthmin2[1].")  ) --and invoiceno='INW/22/000033'
) as table_aroutstanding
 ) as table_aroustanding_full 
-- order by invoiceno
 ) as table_aroutstanding where branchcode=".$branchcode." and ((month(invoicedate)=".$arrMonthmin2[0]." and year(invoicedate)=".$arrMonthmin2[1].")  )--cabang






) as table_full_ar group by month(invoicedate), year(invoicedate), status


";

//print_r($sql);

 	$query=$this->dbDms->query($sql);
	return $query->result_array();
 }
 	



 }


 public function getformataroutstanding($branchcode, $year, $month, $monthmin1, $monthmin2){
 	$arrMonthmin1=explode(",",$monthmin1);
 	$arrMonthmin2=explode(",",$monthmin2);
 	//$paymentdatestart=$year.$month.'01';
 	//$paymentdateend=$year.($month+1).'10';
 	//$paymentdatestartN=$year.$month.'11';
 	//$paymentdateendN=$year.($month+1).'10';

 	// if($month<10){
  //       if(($month+1)<10){
  //           $paymentdateend=$year."0".($month+1).'10';
  //           $paymentdateendN=$year."0".($month+1).'10';
  //       }else{
  //           $paymentdateend=$year.($month+1).'10';
  //           $paymentdateendN=$year.($month+1).'10';
  //       }
 	// 	$paymentdatestart=$year."0".$month.'01';
 		
 	// 	$paymentdatestartN=$year."0".$month.'11';
 		
 	// }else{
 	// 	$paymentdatestart=$year.$month.'01';
 	// 	$paymentdateend=$year.($month+1).'10';
 	// 	$paymentdatestartN=$year.$month.'11';
 	// 	$paymentdateendN=$year.($month+1).'10';
 	// }



    if($month<10){
        if(($month+1)>9){
            $paymentdateendN=$year.($month+1).'10';
            $paymentdateend=$year.($month+1).'10';
        }else{
            $paymentdateendN=$year."0".($month+1).'10';
            $paymentdateend=$year."0".($month+1).'10';
        }
        $paymentdatestart=$year."0".$month.'01';
        $paymentdatestartN=$year."0".$month.'11';
    }else{
        if(($month+1)>=12){
            $paymentdateend=($year+1).'0'.(($month+1)-12).'10';
            $paymentdateendN=($year+1).'0'.(($month+1)-12).'10';
        }else{
            $paymentdateend=($year).(($month+1)).'10';
            $paymentdateendN=($year).(($month+1)).'10';
        }
        $paymentdatestart=$year.$month.'01';
        $paymentdatestartN=$year.$month.'11';
    }
    



 	if($branchcode=='641940101'){
 		$sql="select status
  ,invoiceno
  ,convert(date,invoicedate) as invoicedate
  ,joborderno
  ,convert(date,joborderdate) as joborderdate
  ,customername
  ,jasa_awal
  ,part_awal
  ,oli_awal
  ,(jasa_awal+part_awal+oli_awal) as saldo_awal
  ,jasa_msi
  ,part_msi
  ,oli_msi
  ,(jasa_msi+part_msi+oli_msi) as total_debet
  ,jasa_pay
  ,part_pay
  ,oli_pay
  ,jasa_saldo
  ,part_saldo
  ,oli_saldo
  ,paymentamt
  ,databankkas
  ,keterangan
from (

select table_aroutstanding.* from (
select 
 'N-2' as status
,companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
,jasa_pay
,part_pay
,oli_pay
,jasa_awal+jasa_msi-jasa_pay as jasa_saldo
,part_awal+part_msi-part_pay as part_saldo
,oli_awal+oli_msi-oli_pay as oli_saldo
,paymentamt

,databankkas
,case when ceiling(paymentamt/1.11)>=total_debet then 'lunas' else 'belum lunas' end as keterangan
from (


select
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=jasa_awal then jasa_awal else  jasa_awal-(jasa_awal-ceiling (table_aroutstanding.paymentamt/1.11))
		end
	end as jasa_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal) then part_awal else
		     case when (part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))<0) then 0 else
				part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal+oli_awal) then oli_awal else 
			case when (oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11)))<0  then 0  else oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			end
		end
	end as oli_pay
,paymentamt
,ceiling(paymentamt/1.11) as paymentamt_asli
,databankkas

from (
select 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal1 as jasa_awal
,part_awal1 as part_awal
,oli_awal1 as oli_awal
,0 as jasa_msi
,0 as part_msi
,0 as oli_msi
,jasa_awal1+part_awal1+oli_awal1 as total_debet
,(

				select convert(decimal,isnull(SUM(paymentamt),0),0) from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode =641940101
					
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."'
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
			


	  ) as paymentamt
	  ,(

				select concat(bankcode,',',bankname,',',docno,',',docdate,',') from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940101
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' 
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
				FOR XML PATH('')


	  ) as databankkas
from aroutstanding where keterangan='belum lunas' 
) as table_aroutstanding
 ) as table_aroustanding_full 
-- order by invoiceno
 ) as table_aroutstanding 
 left join svtrnservice as sv on table_aroutstanding.companycode=sv.companycode and table_aroutstanding.branchcode=sv.branchcode and table_aroutstanding.joborderno=sv.joborderNo
 where table_aroutstanding.branchcode=641940101 and ((((month(table_aroutstanding.invoicedate)=".$arrMonthmin2[0]." and year(table_aroutstanding.invoicedate)=".$arrMonthmin2[1].")) and sv.foremanid<>'061902008') or (left(table_aroutstanding.invoiceno,3)='FPJ' and (month(table_aroutstanding.invoicedate)=".$arrMonthmin2[0]." and year(table_aroutstanding.invoicedate)=".$arrMonthmin2[1].")))
 

 union all

 select  table_aroutstanding.* from (
select 'N-1' as status
,companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
,jasa_pay
,part_pay
,oli_pay
,jasa_awal+jasa_msi-jasa_pay as jasa_saldo
,part_awal+part_msi-part_pay as part_saldo
,oli_awal+oli_msi-oli_pay as oli_saldo
,paymentamt

,databankkas
,case when ceiling(paymentamt/1.11)>=total_debet then 'lunas' else 'belum lunas' end as keterangan
from (


select
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=jasa_awal then jasa_awal else  jasa_awal-(jasa_awal-ceiling (table_aroutstanding.paymentamt/1.11))
		end
	end as jasa_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal) then part_awal else
		     case when (part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))<0) then 0 else
				part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal+oli_awal) then oli_awal else 
			case when (oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11)))<0  then 0  else oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			end
		end
	end as oli_pay
,paymentamt
,ceiling(paymentamt/1.11) as paymentamt_asli
,databankkas

from (
select 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal1 as jasa_awal
,part_awal1 as part_awal
,oli_awal1 as oli_awal
,0 as jasa_msi
,0 as part_msi
,0 as oli_msi
,jasa_awal1+part_awal1+oli_awal1 as total_debet
,(

				select convert(decimal,isnull(SUM(paymentamt),0),0) from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode =641940101
					
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."'
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
			


	  ) as paymentamt
	  ,(

				select concat(bankcode,',',bankname,',',docno,',',docdate,',') from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940101
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' 
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
				FOR XML PATH('')


	  ) as databankkas
from aroutstanding where keterangan='belum lunas' 
) as table_aroutstanding
 ) as table_aroustanding_full 
-- order by invoiceno
 ) as table_aroutstanding 
left join svtrnservice as sv on table_aroutstanding.companycode=sv.companycode and table_aroutstanding.branchcode=sv.branchcode and table_aroutstanding.joborderno=sv.joborderNo
 where table_aroutstanding.branchcode=641940101 and ((((month(table_aroutstanding.invoicedate)=".$arrMonthmin1[0]." and year(table_aroutstanding.invoicedate)=".$arrMonthmin1[1].")) and sv.foremanid<>'061902008') or (left(table_aroutstanding.invoiceno,3)='FPJ' and (month(table_aroutstanding.invoicedate)=".$arrMonthmin1[0]." and year(table_aroutstanding.invoicedate)=".$arrMonthmin1[1].")))


 

 union all

select table_ar.* from (
 select * from (
select 'N' as status,
        companycode,
	    branchcode,
		invoiceno,
		invoicedate,
		JobOrderNo,
		JobOrderDate,
		CustomerCodeBill,
		customername,
		0 as jasa_awal,
		0 as part_awal,
		0 as oli_awal,
		
		jasa_MSI,
		part_msi,
		oli_msi,
		[Total Debet], 
	case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>jasa_msi then jasa_msi else  
		    case when (jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11)) end
		end
	end as jasa_pay
	,case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>(jasa_msi+part_msi) then part_msi else
		     case when (part_msi-((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else part_msi-((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>(jasa_msi+part_msi+oli_msi) then oli_msi else 
			case when (oli_msi-  ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else oli_msi-  ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			end
		end
	end as oli_pay
	

	,case when (table_invoice_full.paymentamt)=0 then jasa_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=jasa_msi then 0 else  jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11))
		end
	end as jasa_saldo
	,case when (table_invoice_full.paymentamt)=0 then part_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=(jasa_msi+part_msi) then 0 else
		     case when (part_msi=0) then 0 else
				((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			 end
		end
	end as part_saldo
	,case when (table_invoice_full.paymentamt)=0 then oli_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=(jasa_msi+part_msi+oli_msi) then 0 else 
			case when (oli_msi=0) then 0 else ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			end
		end
	end as oli_saldo
	
	,(paymentamt) as payemntamt
	--,ceiling(paymentamt/1.11) as asli_payment
	,databankkas
	,case when 
		ceiling(table_invoice_full.paymentamt/1.11)>=table_invoice_full.[total debet] then 'Lunas' else 'belum lunas' 
	end as keterangan 
from (
select table_invoice.*,

(

				select convert(decimal,isnull(SUM(paymentamt),0),0) from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940101
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestart."' and '".$paymentdateend."'
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'

				) as tableBankKas where tablebankkas.CompanyCode=table_invoice.companycode and tablebankkas.branchcode=table_invoice.branchcode and tablebankkas.invoiceno=table_invoice.invoiceno
			


	  ) as paymentamt,
	  (

				select concat(bankcode,',',bankname,',',docno,',',docdate,',') from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					AND a.CompanyCode = 6419401
					AND a.BranchCode = 641940101
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestart."' and '".$paymentdateend."'
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					

				) as tableBankKas where tablebankkas.CompanyCode=table_invoice.companycode and tablebankkas.branchcode=table_invoice.branchcode and tablebankkas.invoiceno=table_invoice.invoiceno
				FOR XML PATH('')


	  ) as databankkas



	   from (
select a.CompanyCode
	  ,a.branchcode
      ,a.invoiceno
	  ,a.InvoiceDate
	  ,a.joborderno
	  ,a.joborderdate
	  ,a.CustomerCodeBill
	  ,b.customername
	  --,a.LaborDppAmt
	  --,a.PartsDppAmt
	  --,a.MaterialDppAmt
	  ,isnull(jasa.labour,0) as jasa_MSI
	  ,isnull(part.part,0) as part_MSI
	  ,isnull(oli.oli,0) as oli_MSI
	  ,(isnull(jasa.labour,0) + isnull(part.part,0)+isnull(oli.oli,0)) as [Total Debet]
	  

from 
	svtrninvoice as a
	left join gnmstcustomer as b on a.CompanyCode=b.CompanyCode and a.CustomerCodeBill=b.CustomerCode
	--start of jasa
	left join (
		select companycode, branchcode,invoiceno, sum(lbrdppamt) as labour from(
				select 
						a.CompanyCode
					   ,a.BranchCode
						,a.InvoiceNo
					   ,a.invoicedate
					   , a.FPJNo
						, a.InvoiceStatus
	   
						, b.OperationHour
						, b.operationcost
						, b.DiscPct
						, a.joborderdate
						, a.joborderNo
						, a.ProductType
						 , ceiling(b.OperationHour * b.OperationCost * (100.0 - b.DiscPct) * 0.01) LbrDppAmt
						 , a.LaborDppAmt as faktual_labour
						 , IsSubCon = isnull((
										  select top 1 IsSubCon from svMstTask
										   where CompanyCode = a.CompanyCode
											 and BasicModel = a.BasicModel
											 and OperationNo = b.OperationNo
										  ), 0)
						 , a.JobType		
						 --, case 
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 1000)  then 'FSC01'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 5000)  then 'FSC02'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 10000) then 'FSC03'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 20000) then 'FSC04'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 30000) then 'FSC05'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 40000) then 'FSC06'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 50000) then 'FSC07'
							--  else ''
						 --  end as KsgType
						   ,a.CustomerCode
						   ,a.PoliceRegNo
					  from svTrnInvoice a
					 inner join svTrnInvTask b
						on b.CompanyCode = a.CompanyCode
					   and b.BranchCode = a.BranchCode
					   and b.InvoiceNo = a.InvoiceNo
					 -- left join svMstJob c
						--on c.CompanyCode = a.CompanyCode
					   --and c.BasicModel = a.BasicModel
					   --and c.JobType = a.JobType
					 where 
						a.JobType != 'REWORK'
					 )  table1 where table1.IsSubCon=0 group by companycode, branchcode,invoiceno
	 ) as jasa on a.CompanyCode=jasa.CompanyCode and a.BranchCode=jasa.BranchCode and a.InvoiceNo=jasa.InvoiceNo
	 --end of jasa

	 --start of oli
	 left join (
			select companycode, branchcode, invoiceno, sum(spramt) as oli  from (	
				select a.*
					 , c.ParaValue as GroupTpgo
					 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
					 , case 
						 when left(a.InvoiceNo, 3) = 'INI' or a.JobType = 'PDI' then 'INT,PDI'
						 when left(a.InvoiceNo, 3) = 'INW' or left(a.InvoiceNo, 3) = 'INF' then 'CLM,FSC'
						 else 'CPUS'
					   end GroupJob
					 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
					  , b.typeofgoods
				  from svTrnInvoice a
				 inner join svTrnInvItem b
					on b.CompanyCode = a.CompanyCode
				   and b.BranchCode = a.BranchCode
				   and b.InvoiceNo = a.InvoiceNo
				  left join gnMstLookupDtl c
					on c.CompanyCode = a.CompanyCode
				   and c.CodeID = 'GTGO'
				   and c.LookupValue = b.TypeOfGoods
			) as table1 where IsSublet = 0 and GroupTpgo != 'SPAREPART' and JobType != 'REWORK' group by companycode, branchcode, invoiceno
     ) as oli on a.CompanyCode=oli.CompanyCode and a.BranchCode=oli.BranchCode and a.InvoiceNo=oli.InvoiceNo
	 --end of oli

	 --sparepart
	 left join (
		select companycode, branchcode, invoiceno, sum(total) as part 
			from (
			--Total Workshop Parts Sales Revenue
			select companycode, branchcode, invoiceno, sum(spramt) as total from (	
				select a.*
					 , c.ParaValue as GroupTpgo
					 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
					 , case 
						 when left(a.InvoiceNo, 3) = 'INI' or a.JobType = 'PDI' then 'INT,PDI'
						 when left(a.InvoiceNo, 3) = 'INW' or left(a.InvoiceNo, 3) = 'INF' then 'CLM,FSC'
						 else 'CPUS'
					   end GroupJob
					 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
					  , b.typeofgoods
				  from svTrnInvoice a
				 inner join svTrnInvItem b
					on b.CompanyCode = a.CompanyCode
				   and b.BranchCode = a.BranchCode
				   and b.InvoiceNo = a.InvoiceNo
				  left join gnMstLookupDtl c
					on c.CompanyCode = a.CompanyCode
				   and c.CodeID = 'GTGO'
				   and c.LookupValue = b.TypeOfGoods
				 where a.JobType != 'REWORK'
			) as table1 where IsSublet = 0 and GroupTpgo = 'SPAREPART' group by companycode, branchcode, invoiceno

			union all
			-- Total Suzuki Genuine Accessories Sales Revenue - Chargeable to Customer CPUS (External)
			select  companycode, branchcode, invoiceno, sum(dppamt) as total from (
			select 
								inv.InvoiceNo,
								inv.InvoiceDate,
								inv.CompanyCode,
								inv.BranchCode,
								year(inv.InvoiceDate) PeriodYear,
								MONTH(inv.InvoiceDate) PeriodMonth,
								ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
								'0' as origin
							from svTrnInvItem invi	
							left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
							where 
							invi.TypeOfGoods in ('2','5','6','9')
							--group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)

							union all

			select 
								inv.InvoiceNo,
								inv.InvoiceDate,
								inv.CompanyCode,
								inv.BranchCode,
								year(inv.InvoiceDate) PeriodYear,
								MONTH(inv.InvoiceDate) PeriodMonth,
								ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
								'1' as origin
							from svTrnInvItem invi	
							left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
							where invi.TypeOfGoods in ('7','8')
							--group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)
				
				) as accesoris	group by companycode, branchcode, invoiceno
	
			) as sparepart group by companycode, branchcode, invoiceno
	 ) as part on a.CompanyCode=part.CompanyCode and a.BranchCode=part.BranchCode and a.InvoiceNo=part.InvoiceNo
	 --end of sparepart

where (month(a.invoicedate)=".$month." and year(a.invoicedate)=".$year.") and a.BranchCode=641940101 and left(a.invoiceno,3)<>'INI' --and  b.customername not like '%DUTA CENDANA ADIMANDIRI%' 

union all

select a.CompanyCode
	  ,a.BranchCode
	  ,a.fpjno as invoiceno
	  ,a.fpjdate as invoicedate
	 
	  ,null as joborderno
	  ,null as joborderdate
	  ,a.CustomerCodeBill
	  ,b.customername
	 
	  ,0 as jasa_MSI
	  ,a.totdppamt as part_MSI
	  ,0 as oli_MSI
	  ,a.totdppamt as [total invoice]
from 
	spTrnSFPJHdr as a
	left join gnmstcustomer as b on a.CompanyCode=b.CompanyCode and a.CustomerCodeBill=b.CustomerCode
where (month(a.fpjdate)=".$month." and year(a.FPJDATE)=".$year.") and a.BranchCode=641940101 and TypeOfGoods = '0' and  customername not like '%DUTA CENDANA ADIMANDIRI%' 
) as table_invoice
) as table_Invoice_full
) as table_full  
) as table_ar
left join svtrnservice as sv on table_ar.companycode=sv.companycode and table_ar.branchcode=sv.branchcode and table_ar.joborderno=sv.joborderNo
 where (table_ar.branchcode=641940101 and sv.foremanid<>'061902008') or left(table_ar.invoiceno,3)='FPJ'
 
) as table_full_ar order by month(invoicedate), year(invoicedate)
";
//harus di perbaiki oleh logic querynya untuk ciawi maupun jatiasih

$query=$this->dbDms->query($sql);
	return $query->result_array();



 	}else if($branchcode=='641940104'){

 		$sql="select status
  ,invoiceno
  ,convert(date,invoicedate) as invoicedate
  ,joborderno
  ,convert(date,joborderdate) as joborderdate
  ,customername
  ,jasa_awal
  ,part_awal
  ,oli_awal
  ,(jasa_awal+part_awal+oli_awal) as saldo_awal
  ,jasa_msi
  ,part_msi
  ,oli_msi
  ,(jasa_msi+part_msi+oli_msi) as total_debet
  ,jasa_pay
  ,part_pay
  ,oli_pay
  ,jasa_saldo
  ,part_saldo
  ,oli_saldo
  ,paymentamt
  ,databankkas
  ,keterangan
from (

select table_aroutstanding.* from (
select 
 'N-2' as status
,companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
,jasa_pay
,part_pay
,oli_pay
,jasa_awal+jasa_msi-jasa_pay as jasa_saldo
,part_awal+part_msi-part_pay as part_saldo
,oli_awal+oli_msi-oli_pay as oli_saldo
,paymentamt

,databankkas
,case when ceiling(paymentamt/1.11)>=total_debet then 'lunas' else 'belum lunas' end as keterangan
from (


select
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=jasa_awal then jasa_awal else  jasa_awal-(jasa_awal-ceiling (table_aroutstanding.paymentamt/1.11))
		end
	end as jasa_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal) then part_awal else
		     case when (part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))<0) then 0 else
				part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal+oli_awal) then oli_awal else 
			case when (oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11)))<0  then 0  else oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			end
		end
	end as oli_pay
,paymentamt
,ceiling(paymentamt/1.11) as paymentamt_asli
,databankkas

from (
select 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal1 as jasa_awal
,part_awal1 as part_awal
,oli_awal1 as oli_awal
,0 as jasa_msi
,0 as part_msi
,0 as oli_msi
,jasa_awal1+part_awal1+oli_awal1 as total_debet
,(

				select convert(decimal,isnull(SUM(paymentamt),0),0) from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode =641940101
					
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."'
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
			


	  ) as paymentamt
	  ,(

				select concat(bankcode,',',bankname,',',docno,',',docdate,',') from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940101
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' 
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
				FOR XML PATH('')


	  ) as databankkas
from aroutstanding where keterangan='belum lunas' 
) as table_aroutstanding
 ) as table_aroustanding_full 
-- order by invoiceno
 ) as table_aroutstanding 
 left join svtrnservice as sv on table_aroutstanding.companycode=sv.companycode and table_aroutstanding.branchcode=sv.branchcode and table_aroutstanding.joborderno=sv.joborderNo
 where table_aroutstanding.branchcode=641940101 and (((month(table_aroutstanding.invoicedate)=".$arrMonthmin2[0]." and year(table_aroutstanding.invoicedate)=".$arrMonthmin2[1].")) and sv.foremanid='061902008') --or left(table_aroutstanding.invoiceno,3)='FPJ'
 

 union all

 
--- n-1 jati asih & ciawi

 select  table_aroutstanding.* from (
select 'N-1' as status
,companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
,jasa_pay
,part_pay
,oli_pay
,jasa_awal+jasa_msi-jasa_pay as jasa_saldo
,part_awal+part_msi-part_pay as part_saldo
,oli_awal+oli_msi-oli_pay as oli_saldo
,paymentamt

,databankkas
,case when ceiling(paymentamt/1.11)>=total_debet then 'lunas' else 'belum lunas' end as keterangan
from (


select
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=jasa_awal then jasa_awal else  jasa_awal-(jasa_awal-ceiling (table_aroutstanding.paymentamt/1.11))
		end
	end as jasa_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal) then part_awal else
		     case when (part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))<0) then 0 else
				part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal+oli_awal) then oli_awal else 
			case when (oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11)))<0  then 0  else oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			end
		end
	end as oli_pay
,paymentamt
,ceiling(paymentamt/1.11) as paymentamt_asli
,databankkas

from (
select 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal1 as jasa_awal
,part_awal1 as part_awal
,oli_awal1 as oli_awal
,0 as jasa_msi
,0 as part_msi
,0 as oli_msi
,jasa_awal1+part_awal1+oli_awal1 as total_debet
,(

				select convert(decimal,isnull(SUM(paymentamt),0),0) from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode =641940101
					
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."'
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
			


	  ) as paymentamt
	  ,(

				select concat(bankcode,',',bankname,',',docno,',',docdate,',') from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940101
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' 
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
				FOR XML PATH('')


	  ) as databankkas
from aroutstanding where keterangan='belum lunas' 
) as table_aroutstanding
 ) as table_aroustanding_full 
-- order by invoiceno
 ) as table_aroutstanding 
left join svtrnservice as sv on table_aroutstanding.companycode=sv.companycode and table_aroutstanding.branchcode=sv.branchcode and table_aroutstanding.joborderno=sv.joborderNo
 where table_aroutstanding.branchcode=641940101 and (((month(table_aroutstanding.invoicedate)=".$arrMonthmin1[0]." and year(table_aroutstanding.invoicedate)=".$arrMonthmin1[1].")) and sv.foremanid='061902008') --or left(table_aroutstanding.invoiceno,3)='FPJ'
 
 
 union all
 
 --- n-1 jatiasih
 
 
 
 select  table_aroutstanding.* from (
select 'N-1' as status
,companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
,jasa_pay
,part_pay
,oli_pay
,jasa_awal+jasa_msi-jasa_pay as jasa_saldo
,part_awal+part_msi-part_pay as part_saldo
,oli_awal+oli_msi-oli_pay as oli_saldo
,paymentamt

,databankkas
,case when ceiling(paymentamt/1.11)>=total_debet then 'lunas' else 'belum lunas' end as keterangan
from (


select
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=jasa_awal then jasa_awal else  jasa_awal-(jasa_awal-ceiling (table_aroutstanding.paymentamt/1.11))
		end
	end as jasa_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal) then part_awal else
		     case when (part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))<0) then 0 else
				part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal+oli_awal) then oli_awal else 
			case when (oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11)))<0  then 0  else oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			end
		end
	end as oli_pay
,paymentamt
,ceiling(paymentamt/1.11) as paymentamt_asli
,databankkas

from (
select 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal1 as jasa_awal
,part_awal1 as part_awal
,oli_awal1 as oli_awal
,0 as jasa_msi
,0 as part_msi
,0 as oli_msi
,jasa_awal1+part_awal1+oli_awal1 as total_debet
,(

				select convert(decimal,isnull(SUM(paymentamt),0),0) from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode =641940104
					
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."'
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
			


	  ) as paymentamt
	  ,(

				select concat(bankcode,',',bankname,',',docno,',',docdate,',') from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940104
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' 
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
				FOR XML PATH('')


	  ) as databankkas
from aroutstanding where keterangan='belum lunas' 
) as table_aroutstanding
 ) as table_aroustanding_full 
-- order by invoiceno
 ) as table_aroutstanding 
--left join svtrnservice as sv on table_aroutstanding.companycode=sv.companycode and table_aroutstanding.branchcode=sv.branchcode and table_aroutstanding.joborderno=sv.joborderNo
 where table_aroutstanding.branchcode=641940104 and (((month(table_aroutstanding.invoicedate)=".$arrMonthmin1[0]." and year(table_aroutstanding.invoicedate)=".$arrMonthmin1[1]."))) --and sv.foremanid='061902008') --or left(table_aroutstanding.invoiceno,3)='FPJ'
 
 
 --- end n-1 jatiasih
 
 --- end -1 jatiasih & ciawi



 union all


 select table_ar.* from (
 select * from (
select 'N' as status,
        companycode,
	    branchcode,
		invoiceno,
		invoicedate,
		JobOrderNo,
		JobOrderDate,
		CustomerCodeBill,
		customername,
		0 as jasa_awal,
		0 as part_awal,
		0 as oli_awal,
		
		jasa_MSI,
		part_msi,
		oli_msi,
		[Total Debet], 
	case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>jasa_msi then jasa_msi else  
		    case when (jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11)) end
		end
	end as jasa_pay
	,case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>(jasa_msi+part_msi) then part_msi else
		     case when (part_msi-((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else part_msi-((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>(jasa_msi+part_msi+oli_msi) then oli_msi else 
			case when (oli_msi-  ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else oli_msi-  ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			end
		end
	end as oli_pay
	

	,case when (table_invoice_full.paymentamt)=0 then jasa_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=jasa_msi then 0 else  jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11))
		end
	end as jasa_saldo
	,case when (table_invoice_full.paymentamt)=0 then part_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=(jasa_msi+part_msi) then 0 else
		     case when (part_msi=0) then 0 else
				((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			 end
		end
	end as part_saldo
	,case when (table_invoice_full.paymentamt)=0 then oli_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=(jasa_msi+part_msi+oli_msi) then 0 else 
			case when (oli_msi=0) then 0 else ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			end
		end
	end as oli_saldo
	
	,(paymentamt) as payemntamt
	--,ceiling(paymentamt/1.11) as asli_payment
	,databankkas
	,case when 
		ceiling(table_invoice_full.paymentamt/1.11)>=table_invoice_full.[total debet] then 'Lunas' else 'belum lunas' 
	end as keterangan 
from (
select table_invoice.*,

(

				select convert(decimal,isnull(SUM(paymentamt),0),0) from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = 641940104
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestart."' and '".$paymentdateend."'
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'

				) as tableBankKas where tablebankkas.CompanyCode=table_invoice.companycode and tablebankkas.branchcode=table_invoice.branchcode and tablebankkas.invoiceno=table_invoice.invoiceno
			


	  ) as paymentamt,
	  (

				select concat(bankcode,',',bankname,',',docno,',',docdate,',') from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					AND a.CompanyCode = 6419401
					AND a.BranchCode = 641940104
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestart."' and '".$paymentdateend."'
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					

				) as tableBankKas where tablebankkas.CompanyCode=table_invoice.companycode and tablebankkas.branchcode=table_invoice.branchcode and tablebankkas.invoiceno=table_invoice.invoiceno
				FOR XML PATH('')


	  ) as databankkas



	   from (
select a.CompanyCode
	  ,a.branchcode
      ,a.invoiceno
	  ,a.InvoiceDate
	  ,a.joborderno
	  ,a.joborderdate
	  ,a.CustomerCodeBill
	  ,b.customername
	  --,a.LaborDppAmt
	  --,a.PartsDppAmt
	  --,a.MaterialDppAmt
	  ,isnull(jasa.labour,0) as jasa_MSI
	  ,isnull(part.part,0) as part_MSI
	  ,isnull(oli.oli,0) as oli_MSI
	  ,(isnull(jasa.labour,0) + isnull(part.part,0)+isnull(oli.oli,0)) as [Total Debet]
	  

from 
	svtrninvoice as a
	left join gnmstcustomer as b on a.CompanyCode=b.CompanyCode and a.CustomerCodeBill=b.CustomerCode
	--start of jasa
	left join (
		select companycode, branchcode,invoiceno, sum(lbrdppamt) as labour from(
				select 
						a.CompanyCode
					   ,a.BranchCode
						,a.InvoiceNo
					   ,a.invoicedate
					   , a.FPJNo
						, a.InvoiceStatus
	   
						, b.OperationHour
						, b.operationcost
						, b.DiscPct
						, a.joborderdate
						, a.joborderNo
						, a.ProductType
						 , ceiling(b.OperationHour * b.OperationCost * (100.0 - b.DiscPct) * 0.01) LbrDppAmt
						 , a.LaborDppAmt as faktual_labour
						 , IsSubCon = isnull((
										  select top 1 IsSubCon from svMstTask
										   where CompanyCode = a.CompanyCode
											 and BasicModel = a.BasicModel
											 and OperationNo = b.OperationNo
										  ), 0)
						 , a.JobType		
						 --, case 
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 1000)  then 'FSC01'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 5000)  then 'FSC02'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 10000) then 'FSC03'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 20000) then 'FSC04'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 30000) then 'FSC05'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 40000) then 'FSC06'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 50000) then 'FSC07'
							--  else ''
						 --  end as KsgType
						   ,a.CustomerCode
						   ,a.PoliceRegNo
					  from svTrnInvoice a
					 inner join svTrnInvTask b
						on b.CompanyCode = a.CompanyCode
					   and b.BranchCode = a.BranchCode
					   and b.InvoiceNo = a.InvoiceNo
					 -- left join svMstJob c
						--on c.CompanyCode = a.CompanyCode
					   --and c.BasicModel = a.BasicModel
					   --and c.JobType = a.JobType
					 where 
						a.JobType != 'REWORK'
					 )  table1 where table1.IsSubCon=0 group by companycode, branchcode,invoiceno
	 ) as jasa on a.CompanyCode=jasa.CompanyCode and a.BranchCode=jasa.BranchCode and a.InvoiceNo=jasa.InvoiceNo
	 --end of jasa

	 --start of oli
	 left join (
			select companycode, branchcode, invoiceno, sum(spramt) as oli  from (	
				select a.*
					 , c.ParaValue as GroupTpgo
					 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
					 , case 
						 when left(a.InvoiceNo, 3) = 'INI' or a.JobType = 'PDI' then 'INT,PDI'
						 when left(a.InvoiceNo, 3) = 'INW' or left(a.InvoiceNo, 3) = 'INF' then 'CLM,FSC'
						 else 'CPUS'
					   end GroupJob
					 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
					  , b.typeofgoods
				  from svTrnInvoice a
				 inner join svTrnInvItem b
					on b.CompanyCode = a.CompanyCode
				   and b.BranchCode = a.BranchCode
				   and b.InvoiceNo = a.InvoiceNo
				  left join gnMstLookupDtl c
					on c.CompanyCode = a.CompanyCode
				   and c.CodeID = 'GTGO'
				   and c.LookupValue = b.TypeOfGoods
			) as table1 where IsSublet = 0 and GroupTpgo != 'SPAREPART' and JobType != 'REWORK' group by companycode, branchcode, invoiceno
     ) as oli on a.CompanyCode=oli.CompanyCode and a.BranchCode=oli.BranchCode and a.InvoiceNo=oli.InvoiceNo
	 --end of oli

	 --sparepart
	 left join (
		select companycode, branchcode, invoiceno, sum(total) as part 
			from (
			--Total Workshop Parts Sales Revenue
			select companycode, branchcode, invoiceno, sum(spramt) as total from (	
				select a.*
					 , c.ParaValue as GroupTpgo
					 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
					 , case 
						 when left(a.InvoiceNo, 3) = 'INI' or a.JobType = 'PDI' then 'INT,PDI'
						 when left(a.InvoiceNo, 3) = 'INW' or left(a.InvoiceNo, 3) = 'INF' then 'CLM,FSC'
						 else 'CPUS'
					   end GroupJob
					 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
					  , b.typeofgoods
				  from svTrnInvoice a
				 inner join svTrnInvItem b
					on b.CompanyCode = a.CompanyCode
				   and b.BranchCode = a.BranchCode
				   and b.InvoiceNo = a.InvoiceNo
				  left join gnMstLookupDtl c
					on c.CompanyCode = a.CompanyCode
				   and c.CodeID = 'GTGO'
				   and c.LookupValue = b.TypeOfGoods
				 where a.JobType != 'REWORK'
			) as table1 where IsSublet = 0 and GroupTpgo = 'SPAREPART' group by companycode, branchcode, invoiceno

			union all
			-- Total Suzuki Genuine Accessories Sales Revenue - Chargeable to Customer CPUS (External)
			select  companycode, branchcode, invoiceno, sum(dppamt) as total from (
			select 
								inv.InvoiceNo,
								inv.InvoiceDate,
								inv.CompanyCode,
								inv.BranchCode,
								year(inv.InvoiceDate) PeriodYear,
								MONTH(inv.InvoiceDate) PeriodMonth,
								ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
								'0' as origin
							from svTrnInvItem invi	
							left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
							where 
							invi.TypeOfGoods in ('2','5','6','9')
							--group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)

							union all

			select 
								inv.InvoiceNo,
								inv.InvoiceDate,
								inv.CompanyCode,
								inv.BranchCode,
								year(inv.InvoiceDate) PeriodYear,
								MONTH(inv.InvoiceDate) PeriodMonth,
								ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
								'1' as origin
							from svTrnInvItem invi	
							left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
							where invi.TypeOfGoods in ('7','8')
							--group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)
				
				) as accesoris	group by companycode, branchcode, invoiceno
	
			) as sparepart group by companycode, branchcode, invoiceno
	 ) as part on a.CompanyCode=part.CompanyCode and a.BranchCode=part.BranchCode and a.InvoiceNo=part.InvoiceNo
	 --end of sparepart

where (month(a.invoicedate)=".$month." and year(a.invoicedate)=".$year.") and a.BranchCode=641940104 and left(a.invoiceno,3)<>'INI' --and  b.customername not like '%DUTA CENDANA ADIMANDIRI%' 

--union all

--select a.CompanyCode
	  --,a.BranchCode
	  --,a.fpjno as invoiceno
	  --,a.fpjdate as invoicedate
	 
	  --,null as joborderno
	  --,null as joborderdate
	  --,a.CustomerCodeBill
	  --,b.customername
	 
	  --,0 as jasa_MSI
	  --,a.totdppamt as part_MSI
	  --,0 as oli_MSI
	  --,a.totdppamt as [total invoice]
--from 
	--spTrnSFPJHdr as a
	--left join gnmstcustomer as b on a.CompanyCode=b.CompanyCode and a.CustomerCodeBill=b.CustomerCode
--where (month(a.fpjdate)=".$month." and year(a.FPJDATE)=".$year.") and a.BranchCode=641940104 and TypeOfGoods = '0' and  customername not like '%DUTA CENDANA ADIMANDIRI%' 
) as table_invoice
) as table_Invoice_full
) as table_full  
) as table_ar
left join svtrnservice as sv on table_ar.companycode=sv.companycode and table_ar.branchcode=sv.branchcode and table_ar.joborderno=sv.joborderNo
 where (table_ar.branchcode=641940104) --or left(table_ar.invoiceno,3)='FPJ'
 
) as table_full_ar order by month(invoicedate), year(invoicedate)



";

$query=$this->dbDms->query($sql);
	return $query->result_array();

 	}else{

 	
 	

 	$sql="select status
  ,invoiceno
  ,convert(date,invoicedate) as invoicedate
  ,joborderno
  ,convert(date,joborderdate) as joborderdate
  ,customername
  ,jasa_awal
  ,part_awal
  ,oli_awal
  ,(jasa_awal+part_awal+oli_awal) as saldo_awal
  ,jasa_msi
  ,part_msi
  ,oli_msi
  ,(jasa_msi+part_msi+oli_msi) as total_debet
  ,jasa_pay
  ,part_pay
  ,oli_pay
  ,jasa_saldo
  ,part_saldo
  ,oli_saldo
  ,paymentamt
  ,databankkas
  ,keterangan
from (

select * from (
 select 'N-2' as status, 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
,jasa_pay
,part_pay
,oli_pay
,jasa_awal+jasa_msi-jasa_pay as jasa_saldo
,part_awal+part_msi-part_pay as part_saldo
,oli_awal+oli_msi-oli_pay as oli_saldo
,paymentamt
--,paymentamt_asli
,databankkas
,case when ceiling(paymentamt/1.11)>=total_debet then 'lunas' else 'belum lunas' end as keterangan
from (


select
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=jasa_awal then jasa_awal else  jasa_awal-(jasa_awal-ceiling (table_aroutstanding.paymentamt/1.11))
		end
	end as jasa_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal) then part_awal else
		     case when (part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))<0) then 0 else
				part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal+oli_awal) then oli_awal else 
			case when (oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11)))<0  then 0  else oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			end
		end
	end as oli_pay
,paymentamt
,ceiling(paymentamt/1.11) as paymentamt_asli
,databankkas

from (
select 
aroutstanding.companycode
,aroutstanding.branchcode
,aroutstanding.invoiceno
,aroutstanding.invoicedate
,aroutstanding.joborderno
,aroutstanding.joborderdate
,aroutstanding.customercodebill
,aroutstanding.customername
,jasa_awal1 as jasa_awal
,part_awal1 as part_awal
,oli_awal1 as oli_awal
,0 as jasa_msi
,0 as part_msi
,0 as oli_msi
,jasa_awal1+part_awal1+oli_awal1 as total_debet
,convert(decimal,isnull(tablepayment.paymentamt,0),0) as paymentamt
,tableBankKas.databankkas



from aroutstanding

left  join

	  (
			select 
companycode,
branchcode,
invoiceno,

stuff((
select 
		table2.bankcode+','+ table2.bankname+','+ table2.docno
from (

SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.PaymentAmt
					
					,a.BankCode
					,d.BankName
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo
) as table2
where table2.branchcode=table1.branchcode and table2.invoiceno=table1.invoiceno for xml path(''),type).value('.','nvarchar(max)'),1,1,'') as databankkas









from (

SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.PaymentAmt
					
					,a.BankCode
					,d.BankName
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo
) as table1
group by companycode,branchcode, invoiceno
				
					

) as tableBankKas on tableBankKas.branchcode=aroutstanding.branchcode and tableBankKas.invoiceno=aroutstanding.invoiceno 

left join

	  (

				
					SELECT 
					a.CompanyCode
					,a.BranchCode
					
					
					,c.InvoiceNo
					,sum(c.PaymentAmt) as paymentamt
					
					
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				group by a.CompanyCode
					,a.BranchCode
					,c.InvoiceNo

) as tablepayment on tablepayment.branchcode=aroutstanding.branchcode and tablepayment.invoiceno=aroutstanding.invoiceno 

	  



where keterangan='belum lunas' and aroutstanding.branchcode=".$branchcode." and ((month(aroutstanding.invoicedate)=".$arrMonthmin2[0]." and year(aroutstanding.invoicedate)=".$arrMonthmin2[1].")  ) --and invoiceno='INW/22/000033'
) as table_aroutstanding
 ) as table_aroustanding_full 
-- order by invoiceno
 ) as table_aroutstanding where branchcode=".$branchcode." and ((month(invoicedate)=".$arrMonthmin2[0]." and year(invoicedate)=".$arrMonthmin2[1].")  )--cabang



union all

select * from (
 select 'N-1' as status, 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
,jasa_pay
,part_pay
,oli_pay
,jasa_awal+jasa_msi-jasa_pay as jasa_saldo
,part_awal+part_msi-part_pay as part_saldo
,oli_awal+oli_msi-oli_pay as oli_saldo
,paymentamt
--,paymentamt_asli
,databankkas
,case when ceiling(paymentamt/1.11)>=total_debet then 'lunas' else 'belum lunas' end as keterangan
from (


select
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=jasa_awal then jasa_awal else  jasa_awal-(jasa_awal-ceiling (table_aroutstanding.paymentamt/1.11))
		end
	end as jasa_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal) then part_awal else
		     case when (part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))<0) then 0 else
				part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal+oli_awal) then oli_awal else 
			case when (oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11)))<0  then 0  else oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			end
		end
	end as oli_pay
,paymentamt
,ceiling(paymentamt/1.11) as paymentamt_asli
,databankkas

from (
select 
aroutstanding.companycode
,aroutstanding.branchcode
,aroutstanding.invoiceno
,aroutstanding.invoicedate
,aroutstanding.joborderno
,aroutstanding.joborderdate
,aroutstanding.customercodebill
,aroutstanding.customername
,jasa_awal1 as jasa_awal
,part_awal1 as part_awal
,oli_awal1 as oli_awal
,0 as jasa_msi
,0 as part_msi
,0 as oli_msi
,jasa_awal1+part_awal1+oli_awal1 as total_debet
,convert(decimal,isnull(tablepayment.paymentamt,0),0) as paymentamt
,tableBankKas.databankkas



from aroutstanding

left  join

	  (
			select 
companycode,
branchcode,
invoiceno,

stuff((
select 
		table2.bankcode+','+ table2.bankname+','+ table2.docno
from (

SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.PaymentAmt
					
					,a.BankCode
					,d.BankName
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo
) as table2
where table2.branchcode=table1.branchcode and table2.invoiceno=table1.invoiceno for xml path(''),type).value('.','nvarchar(max)'),1,1,'') as databankkas


from (

SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.PaymentAmt
					
					,a.BankCode
					,d.BankName
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo
) as table1
group by companycode,branchcode, invoiceno
				
					

) as tableBankKas on tableBankKas.branchcode=aroutstanding.branchcode and tableBankKas.invoiceno=aroutstanding.invoiceno 

left  join

	  (

				
					SELECT 
					a.CompanyCode
					,a.BranchCode
					
					
					,c.InvoiceNo
					,sum(c.PaymentAmt) as paymentamt
					
					
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				group by a.CompanyCode
					,a.BranchCode
					,c.InvoiceNo

) as tablepayment on tablepayment.branchcode=aroutstanding.branchcode and tablepayment.invoiceno=aroutstanding.invoiceno 

	  



where keterangan='belum lunas' and aroutstanding.branchcode=".$branchcode." and ((month(aroutstanding.invoicedate)=".$arrMonthmin1[0]." and year(aroutstanding.invoicedate)=".$arrMonthmin1[1].")  ) --and invoiceno='INW/22/000033'
) as table_aroutstanding
 ) as table_aroustanding_full 
-- order by invoiceno
 ) as table_aroutstanding where branchcode=".$branchcode." and ((month(invoicedate)=".$arrMonthmin1[0]." and year(invoicedate)=".$arrMonthmin1[1].")  )--cabang


union all


select * from (
 select * from (
select  'N' status,
		companycode,
	    branchcode,
		invoiceno,
		invoicedate,
		JobOrderNo,
		JobOrderDate,
		CustomerCodeBill,
		customername,
		0 as jasa_awal,
		0 as part_awal,
		0 as oli_awal,
		jasa_MSI,
		part_msi,
		oli_msi,
		[Total Debet], 
	case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>jasa_msi then jasa_msi else  
		    case when (jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11)) end
		end
	end as jasa_pay
	,case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>(jasa_msi+part_msi) then part_msi else
		     case when (part_msi-((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else part_msi-((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>(jasa_msi+part_msi+oli_msi) then oli_msi else 
			case when (oli_msi-  ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else oli_msi-  ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			end
		end
	end as oli_pay
	

	,case when (table_invoice_full.paymentamt)=0 then jasa_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=jasa_msi then 0 else  jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11))
		end
	end as jasa_saldo
	,case when (table_invoice_full.paymentamt)=0 then part_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=(jasa_msi+part_msi) then 0 else
		     case when (part_msi=0) then 0 else
				((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			 end
		end
	end as part_saldo
	,case when (table_invoice_full.paymentamt)=0 then oli_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=(jasa_msi+part_msi+oli_msi) then 0 else 
			case when (oli_msi=0) then 0 else ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			end
		end
	end as oli_saldo
	
	,(paymentamt) as payemntamt
	--,ceiling(paymentamt/1.11) as asli_payment
	,databankkas
	,case when 
		ceiling(table_invoice_full.paymentamt/1.11)>=table_invoice_full.[total debet] then 'Lunas' else 'belum lunas' 
	end as keterangan 
from (

select table_invoice.*,
tableBankKas.databankkas,
convert(decimal,isnull(tablepayment.paymentamt,0),0) as paymentamt





	   from (
select a.CompanyCode
	  ,a.branchcode
      ,a.invoiceno
	  ,a.InvoiceDate
	  ,a.joborderno
	  ,a.joborderdate
	  ,a.CustomerCodeBill
	  ,b.customername
	  --,a.LaborDppAmt
	  --,a.PartsDppAmt
	  --,a.MaterialDppAmt
	  ,isnull(jasa.labour,0) as jasa_MSI
	  ,isnull(part.part,0) as part_MSI
	  ,isnull(oli.oli,0) as oli_MSI
	  ,(isnull(jasa.labour,0) + isnull(part.part,0)+isnull(oli.oli,0)) as [Total Debet]
	  

from 
	svtrninvoice as a
	left join gnmstcustomer as b on a.CompanyCode=b.CompanyCode and a.CustomerCodeBill=b.CustomerCode
	--start of jasa
	left join (
		select companycode, branchcode,invoiceno, sum(lbrdppamt) as labour from(
				select 
						a.CompanyCode
					   ,a.BranchCode
						,a.InvoiceNo
					   ,a.invoicedate
					   , a.FPJNo
						, a.InvoiceStatus
	   
						, b.OperationHour
						, b.operationcost
						, b.DiscPct
						, a.joborderdate
						, a.joborderNo
						, a.ProductType
						 , ceiling(b.OperationHour * b.OperationCost * (100.0 - b.DiscPct) * 0.01) LbrDppAmt
						 , a.LaborDppAmt as faktual_labour
						 , IsSubCon = isnull((
										  select top 1 IsSubCon from svMstTask
										   where CompanyCode = a.CompanyCode
											 and BasicModel = a.BasicModel
											 and OperationNo = b.OperationNo
										  ), 0)
						 , a.JobType		
						
						   ,a.CustomerCode
						   ,a.PoliceRegNo
					  from svTrnInvoice a
					 inner join svTrnInvTask b
						on b.CompanyCode = a.CompanyCode
					   and b.BranchCode = a.BranchCode
					   and b.InvoiceNo = a.InvoiceNo
					 
					 where 
						a.JobType != 'REWORK'
					 )  table1 where table1.IsSubCon=0 group by companycode, branchcode,invoiceno
	 ) as jasa on a.CompanyCode=jasa.CompanyCode and a.BranchCode=jasa.BranchCode and a.InvoiceNo=jasa.InvoiceNo
	 --end of jasa

	 --start of oli
	 left join (
			select companycode, branchcode, invoiceno, sum(spramt) as oli  from (	
				select a.companycode, a.BranchCode, a.invoiceno, a.JobType
					 , c.ParaValue as GroupTpgo
					 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
					 , case 
						 when left(a.InvoiceNo, 3) = 'INI' or a.JobType = 'PDI' then 'INT,PDI'
						 when left(a.InvoiceNo, 3) = 'INW' or left(a.InvoiceNo, 3) = 'INF' then 'CLM,FSC'
						 else 'CPUS'
					   end GroupJob
					 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
					  , b.typeofgoods
				  from svTrnInvoice a
				 inner join svTrnInvItem b
					on b.CompanyCode = a.CompanyCode
				   and b.BranchCode = a.BranchCode
				   and b.InvoiceNo = a.InvoiceNo
				  left join gnMstLookupDtl c
					on c.CompanyCode = a.CompanyCode
				   and c.CodeID = 'GTGO'
				   and c.LookupValue = b.TypeOfGoods
			) as table1 where IsSublet = 0 and GroupTpgo != 'SPAREPART' and JobType != 'REWORK' group by companycode, branchcode, invoiceno
     ) as oli on a.CompanyCode=oli.CompanyCode and a.BranchCode=oli.BranchCode and a.InvoiceNo=oli.InvoiceNo
	 --end of oli

	 --sparepart
	 left join (
		select companycode, branchcode, invoiceno, sum(total) as part 
			from (
			--Total Workshop Parts Sales Revenue
			select companycode, branchcode, invoiceno, sum(spramt) as total from (	
				select a.companycode, a.branchcode, a.invoiceno, a.JobType
					 , c.ParaValue as GroupTpgo
					 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
					 , case 
						 when left(a.InvoiceNo, 3) = 'INI' or a.JobType = 'PDI' then 'INT,PDI'
						 when left(a.InvoiceNo, 3) = 'INW' or left(a.InvoiceNo, 3) = 'INF' then 'CLM,FSC'
						 else 'CPUS'
					   end GroupJob
					 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
					  , b.typeofgoods
				  from svTrnInvoice a
				 inner join svTrnInvItem b
					on b.CompanyCode = a.CompanyCode
				   and b.BranchCode = a.BranchCode
				   and b.InvoiceNo = a.InvoiceNo
				  left join gnMstLookupDtl c
					on c.CompanyCode = a.CompanyCode
				   and c.CodeID = 'GTGO'
				   and c.LookupValue = b.TypeOfGoods
				 where a.JobType != 'REWORK'
			) as table1 where IsSublet = 0 and GroupTpgo = 'SPAREPART' group by companycode, branchcode, invoiceno

			union all
			-- Total Suzuki Genuine Accessories Sales Revenue - Chargeable to Customer CPUS (External)
			select  companycode, branchcode, invoiceno, sum(dppamt) as total from (
			select 
								inv.InvoiceNo,
								inv.InvoiceDate,
								inv.CompanyCode,
								inv.BranchCode,
								year(inv.InvoiceDate) PeriodYear,
								MONTH(inv.InvoiceDate) PeriodMonth,
								ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
								'0' as origin
							from svTrnInvItem invi	
							left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
							where 
							invi.TypeOfGoods in ('2','5','6','9')
							--group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)

							union all

			select 
								inv.InvoiceNo,
								inv.InvoiceDate,
								inv.CompanyCode,
								inv.BranchCode,
								year(inv.InvoiceDate) PeriodYear,
								MONTH(inv.InvoiceDate) PeriodMonth,
								ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
								'1' as origin
							from svTrnInvItem invi	
							left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
							where invi.TypeOfGoods in ('7','8')
							--group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)
				
				) as accesoris	group by companycode, branchcode, invoiceno
	
			) as sparepart group by companycode, branchcode, invoiceno
	 ) as part on a.CompanyCode=part.CompanyCode and a.BranchCode=part.BranchCode and a.InvoiceNo=part.InvoiceNo
	 --end of sparepart

where (month(a.invoicedate)=".$month." and year(a.invoicedate)=".$year.") and a.BranchCode=".$branchcode." and left(a.invoiceno,3)<>'INI'

union all

select a.CompanyCode
	  ,a.BranchCode
	  ,a.fpjno as invoiceno
	  ,a.fpjdate as invoicedate
	  --,a.InvoiceDate
	  ,null as joborderno
	  ,null as joborderdate
	  ,a.CustomerCodeBill
	  ,b.customername
	  --,0 as LaborDppAmt
	  --,a.totdppamt
	  --,0 as MaterialDppAmt
	  ,0 as jasa_MSI
	  ,a.totdppamt as part_MSI
	  ,0 as oli_MSI
	  ,a.totdppamt as [total invoice]
from 
	spTrnSFPJHdr as a
	left join gnmstcustomer as b on a.CompanyCode=b.CompanyCode and a.CustomerCodeBill=b.CustomerCode
where (month(a.fpjdate)=".$month." and year(a.FPJDATE)=".$year.") and a.BranchCode=".$branchcode." and TypeOfGoods = '0' and  customername not like '%DUTA CENDANA ADIMANDIRI%' 
--order by a.FPJNO
) as table_invoice
left join
(
			select 
companycode,
branchcode,
invoiceno,

stuff((
select 
		table2.bankcode+','+ table2.bankname+','+ table2.docno
from (

SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.PaymentAmt
					
					,a.BankCode
					,d.BankName
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo
) as table2
where table2.branchcode=table1.branchcode and table2.invoiceno=table1.invoiceno for xml path(''),type).value('.','nvarchar(max)'),1,1,'') as databankkas


from (

SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.PaymentAmt
					
					,a.BankCode
					,d.BankName
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo
) as table1
group by companycode,branchcode, invoiceno
				
					

) as tableBankKas on tableBankKas.branchcode=table_invoice.branchcode and tableBankKas.invoiceno=table_invoice.invoiceno 

left  join

	  (

				
					SELECT 
					a.CompanyCode
					,a.BranchCode
					
					
					,c.InvoiceNo
					,sum(c.PaymentAmt) as paymentamt
					
					
					

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401 --perusahaan
					AND a.BranchCode = ".$branchcode." --cabang
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."' --tgl ar N-2
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				group by a.CompanyCode
					,a.BranchCode
					,c.InvoiceNo

) as tablepayment on tablepayment.branchcode=table_invoice.branchcode and tablepayment.invoiceno=table_invoice.invoiceno 


--order by invoiceno
) as table_Invoice_full
--order by invoiceno
) as table_full --where --keterangan ='belum lunas' --and InvoiceNo='INW/22/000033' 
--order by invoiceno
) as table_ar --order by invoicedate, invoiceno




) as table_full_ar 

";

 	$query=$this->dbDms->query($sql);
	return $query->result_array();
 }
 }



 public function pengajuanServiceAwal($bulan,$tahun){

  		$relateduser=$this->session->userdata('relateduser');
  		$cabang=$this->session->userdata('cabang');


 	 $sql="select idpengajuan, cabang, fullname, relateduser, destination from pengajuan_insentive_service where cabang='".$cabang."' and bulan='".$bulan."' and tahun='".$tahun."' and relateduser='".$relateduser."'";

 	 $query=$this->db->query($sql);
 	 if($query->num_rows()>0){
 	 	//   $sqlupdate="UPDATE `pengajuan_insentive_sro` SET `idkaryawan`='[value-2]',`cabang`='[value-3]',`bulan`='[value-4]',`tahun`='[value-5]',`destination`='[value-6]',`fullname`='[value-7]',`content`='[value-8]',`relateduser`='[value-9]',`insert_by`='[value-10]',`insert_time`='[value-11]',`update_by`='[value-12]',`update_time`='[value-13]' WHERE 1";

 	 	//   $sqlupdate2="UPDATE `detail_pengajuan_insentive_sro` SET `iddtlpengajuan`='[value-1]',`idpengajuan`='[value-2]',`lastprogress`='[value-3]',`bulan`='[value-4]',`tahun`='[value-5]',`aprroveby`='[value-6]',`aproveddate`='[value-7]',`relateduser`='[value-8]',`insert_by`='[value-9]',`insert_time`='[value-10]',`update_by`='[value-11]',`update_time`='[value-12]' WHERE 1";

 	 	//   $this->db->trans_start();
 	 	//   $this->db->query($sqlupdate);
		  // $this->db->query($sqlupdate2);
    //       $this->db->trans_complete();
    //       if ($this->db->trans_status() === FALSE){
      		
		  // }else{

		  // }

 	 	$hasil1="3";
 	 	return $hasil1;

 	 	  
 	 }else{


 	 	  $relateduser=$this->session->userdata('relateduser');
 	 	  $nik=$this->session->userdata('nik');
 	 	  $cabang=$this->session->userdata('cabang');
 	 	  $fullname=$this->session->userdata('admin_nama');
 	 	  


 	 	  
 	 	  $sqlresultpencarian="select idpengajuan from pengajuan_insentive_service where cabang='".$cabang."' and bulan='".$bulan."' and tahun='".$tahun."' and relateduser='".$relateduser."'";

 	 	  $sqlinsert="INSERT INTO `pengajuan_insentive_service`(`idpengajuan`,`idkaryawan`, `cabang`, `bulan`, `tahun`, `destination`, `fullname`, `content`, `relateduser`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES (0,'".$nik."','".$cabang."','".$bulan."','".$tahun."','SM','".$fullname."','incentive Service','".$relateduser."','".$nik."','".date("Y-m-d H:i:s")."','".$nik."','".date("Y-m-d H:i:s")."')";

 	 	 

 	 	  
 	 	  $this->db->trans_begin();
 	 	  $this->db->query($sqlinsert);
		  $idpengajuan=$this->db->query($sqlresultpencarian)->row();

		   $sqlinsert2="INSERT INTO `detail_pengajuan_insentive_service`(`idpengajuan`, `lastprogress`, `bulan`, `tahun`, `aprroveby`, `aproveddate`, `relateduser`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES ('".$idpengajuan->idpengajuan."','SVADM',".$bulan.",'".$tahun."','".$nik."','".date("Y-m-d H:i:s")."','".$relateduser."','".$nik."','".date("Y-m-d H:i:s")."','".$nik."','".date("Y-m-d H:i:s")."')";
		   
           $this->db->query($sqlinsert2);

         
 	 	  if ($this->db->trans_status() === FALSE){
			    $hasil1="0";
		        $this->db->trans_rollback();
        }else{
				$hasil1="1";
		        $this->db->trans_commit();
		}

		return $hasil1;
 	 }

 	 	
 	 

 	}


 public function getPIC($idpengajuan){
	$id=$idpengajuan;
	$sql="SELECT  b.aprroveby, b.iddtlpengajuan, b.relateduser,b.bulan, b.tahun FROM pengajuan_insentive_service as a inner JOIN
detail_pengajuan_insentive_service as b on a.idpengajuan=b.idpengajuan WHERE a.idpengajuan=".$id." order by b.aproveddate";


     $query=$this->db->query($sql);
       return $query->result_array();
}



 public function insertaroutstanding($branchcode, $year, $month){

if($month<10){
        if(($month+1)>9){
            $paymentdateendN=$year.($month+1).'10';
            $paymentdateend=$year.($month+1).'10';
        }else{
            $paymentdateendN=$year."0".($month+1).'10';
            $paymentdateend=$year."0".($month+1).'10';
        }
        $paymentdatestart=$year."0".$month.'01';
        $paymentdatestartN=$year."0".$month.'11';
    }else{
        if(($month+1)>12){
            $paymentdateend=($year+1).'0'.(($month+1)-12).'10';
            $paymentdateendN=($year+1).'0'.(($month+1)-12).'10';
        }else{
            $paymentdateend=($year).(($month+1)).'10';
            $paymentdateendN=($year).(($month+1)).'10';
        }
        $paymentdatestart=$year.$month.'01';
        $paymentdatestartN=$year.$month.'11';
    }



    $sql="--kemungkinan harus diambil juga nomer bktnya
DECLARE @companycode nvarchar(100)
DECLARE @branchcode nvarchar(100)
DECLARE @invoiceno nvarchar(100)
DECLARE @inovino nvarchar(100)
DECLARE @invoicedate datetime
DECLARE @joborderno nvarchar(100)
DECLARE @joborderdate datetime
DECLARE @customercodebill nvarchar(100)
DECLARE @customername nvarchar(100)
DECLARE @jasa_awal bigint
DECLARE @part_awal bigint
DECLARE @oli_awal bigint
DECLARE @jasa_msi bigint
DECLARE @part_msi bigint
DECLARE @oli_msi bigint
DECLARE @total_debet bigint
DECLARE @jasa_pay bigint
DECLARE @part_pay bigint
DECLARE @oli_pay bigint
DECLARE @jasa_saldo bigint
DECLARE @part_saldo bigint
DECLARE @oli_sado bigint
DECLARE @paymentamt bigint
DECLARE @databankkas nvarchar(500)
DECLARE @keterangan nvarchar (15)


DECLARE @getid CURSOR

SET @getid = CURSOR FOR
--source table



--insert into [DCAantariksa].dbo.aroutstanding

select companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
,jasa_pay
,part_pay
,oli_pay
,jasa_awal+jasa_msi-jasa_pay as jasa_saldo
,part_awal+part_msi-part_pay as part_saldo
,oli_awal+oli_msi-oli_pay as oli_saldo
,paymentamt
--,paymentamt_asli
,databankkas
,case when ceiling(paymentamt/1.11)>=total_debet then 'lunas' else 'belum lunas' end as keterangan
from (


select
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal
,part_awal
,oli_awal
,jasa_msi
,part_msi
,oli_msi
,total_debet
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=jasa_awal then jasa_awal else  jasa_awal-(jasa_awal-ceiling (table_aroutstanding.paymentamt/1.11))
		end
	end as jasa_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal) then part_awal else
		     case when (part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))<0) then 0 else
				part_awal-((jasa_awal+part_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_aroutstanding.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_aroutstanding.paymentamt/1.11)>=(jasa_awal+part_awal+oli_awal) then oli_awal else 
			case when (oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11)))<0  then 0  else oli_awal-  ((jasa_awal+part_awal+oli_awal)-ceiling (table_aroutstanding.paymentamt/1.11))
			end
		end
	end as oli_pay
,paymentamt
,ceiling(paymentamt/1.11) as paymentamt_asli
,databankkas

from (
select 
companycode
,branchcode
,invoiceno
,invoicedate
,joborderno
,joborderdate
,customercodebill
,customername
,jasa_awal1 as jasa_awal
,part_awal1 as part_awal
,oli_awal1 as oli_awal
,0 as jasa_msi
,0 as part_msi
,0 as oli_msi
,jasa_awal1+part_awal1+oli_awal1 as total_debet
,(

				select convert(decimal,isnull(SUM(paymentamt),0),0) from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401
					AND a.BranchCode = ".$branchcode."
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."'
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
			


	  ) as paymentamt
	  ,(

				select concat(bankcode,',',bankname,',',docno,',',docdate,',') from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401
					AND a.BranchCode = ".$branchcode."
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestartN."' and '".$paymentdateendN."'
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				) as tableBankKas where tablebankkas.CompanyCode=aroutstanding.companycode and tablebankkas.branchcode=aroutstanding.branchcode and tablebankkas.invoiceno=aroutstanding.invoiceno
				FOR XML PATH('')


	  ) as databankkas
from aroutstanding where keterangan='belum lunas' and branchcode=".$branchcode." --and invoiceno='INW/22/000033'
) as table_aroutstanding
 ) as table_aroustanding_full -- where ceiling(paymentamt/1.11)<total_debet




union all

select * from (
select companycode,
	    branchcode,
		invoiceno,
		invoicedate,
		JobOrderNo,
		JobOrderDate,
		CustomerCodeBill,
		customername,
		0 as jasa_awal,
		0 as part_awal,
		0 as oli_awal,
		
		jasa_MSI,
		part_msi,
		oli_msi,
		[Total Debet], 
	case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>jasa_msi then jasa_msi else  
		    case when (jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11)) end
		end
	end as jasa_pay
	,case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>(jasa_msi+part_msi) then part_msi else
		     case when (part_msi-((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else part_msi-((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			 end
		end
	end as part_pay
	,case when (table_invoice_full.paymentamt)=0 then 0
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>(jasa_msi+part_msi+oli_msi) then oli_msi else 
			case when (oli_msi-  ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))<0) then 0 else oli_msi-  ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			end
		end
	end as oli_pay
	

	,case when (table_invoice_full.paymentamt)=0 then jasa_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=jasa_msi then 0 else  jasa_msi-(jasa_msi-ceiling (table_invoice_full.paymentamt/1.11))
		end
	end as jasa_saldo
	,case when (table_invoice_full.paymentamt)=0 then part_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=(jasa_msi+part_msi) then 0 else
		     case when (part_msi=0) then 0 else
				((jasa_msi+part_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			 end
		end
	end as part_saldo
	,case when (table_invoice_full.paymentamt)=0 then oli_msi
	    else 
	      case when ceiling (table_invoice_full.paymentamt/1.11)>=(jasa_msi+part_msi+oli_msi) then 0 else 
			case when (oli_msi=0) then 0 else ((jasa_msi+part_msi+oli_msi)-ceiling (table_invoice_full.paymentamt/1.11))
			end
		end
	end as oli_saldo
	
	,(paymentamt) as payemntamt
	--,ceiling(paymentamt/1.11) as asli_payment
	,databankkas
	,case when 
		ceiling(table_invoice_full.paymentamt/1.11)>=table_invoice_full.[total debet] then 'Lunas' else 'belum lunas' 
	end as keterangan 
from (
select table_invoice.*,

(

				select convert(decimal,isnull(SUM(paymentamt),0),0) from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401
					AND a.BranchCode = ".$branchcode."
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestart."' and '".$paymentdateend."'
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				) as tableBankKas where tablebankkas.CompanyCode=table_invoice.companycode and tablebankkas.branchcode=table_invoice.branchcode and tablebankkas.invoiceno=table_invoice.invoiceno
			


	  ) as paymentamt,
	  (

				select concat(bankcode,',',bankname,',',docno,',',docdate,',') from(
					SELECT 'kas' as asal
					,a.CompanyCode
					,a.BranchCode
					,f.LookUpValueName
					,a.DocNo
					,a.DocDate
					,c.InvoiceNo
					,c.InvoiceDate
					,c.CustomerCode
					,a.BankCode
					,d.BankName
					,a.AccountNo
					,a.ReceivAmt as TotalAmt
					,b.BankKasType
					,e.CustomerName + ' (' + b.CustomerCode + ')' as pCustomer
					,c.PaymentAmt
					,c.ReceivableAmt
					,c.ReceivableOutstand
					,c.Description
	

					,c.AccountNo as AccountInv
					,isnull(i.Description, '') AccountInvDesc
					,isnull(j.Description, '') AccountDesc

				FROM arTrnBankKasTerimaHdr a with(nolock, nowait)
				LEFT JOIN arTrnBankKasTerimaDtl b with(nolock, nowait) ON a.CompanyCode = b.CompanyCode
					AND a.BranchCode = b.BranchCode
					AND a.DocNo = b.DocNo
				left JOIN arTrnBankKasTerimaInvoice c with(nolock, nowait) ON a.CompanyCode = c.CompanyCode
					AND a.BranchCode = c.BranchCode
					AND a.DocNo = c.DocNo
					AND b.CustomerCode = c.CustomerCode
					AND b.BankKasType = c.BankKasType
 

    
				LEFT JOIN gnMstBankCompany d with(nolock, nowait) ON a.CompanyCode = d.CompanyCode
					AND a.BranchCode = d.BranchCode
					AND a.BankCode = d.BankCode
				LEFT JOIN gnMstCustomer e with(nolock, nowait) ON a.CompanyCode = e.CompanyCode
					AND b.CustomerCode = e.CustomerCode
				LEFT JOIN gnMstLookUpDtl f with(nolock, nowait) ON f.LookUpValue = c.ProfitCenterCode
					AND f.CompanyCode = a.CompanyCode
					AND f.CodeId = 'PFCN'
				LEFT JOIN gnMstAccount i with(nolock, nowait) on i.CompanyCode = a.CompanyCode
					AND i.BranchCode = a.BranchCode
					AND i.AccountNo = c.AccountNo
				LEFT JOIN gnMstAccount j with(nolock, nowait) on j.CompanyCode = a.CompanyCode
					AND j.BranchCode = a.BranchCode
					AND j.AccountNo = a.AccountNo	
				WHERE a.Status != '4'
					--AND a.bType = '1'
					AND a.CompanyCode = 6419401
					AND a.BranchCode = ".$branchcode."
					--AND c.ProfitCenterCode like '200'
					--AND c.InvoiceNo in (select invoiceno from svtrninvoice where (month(invoicedate)=2 and year(invoicedate)=2022) and branchcode=".$branchcode.")
					--and c.invoiceno in ('INF/22/000700')
					and CONVERT(VARCHAR, a.DocDate, 112) between '".$paymentdatestart."' and '".$paymentdateend."'
					AND a.Accountno like '%%'
					AND c.AccountNo like '%%'
					--AND c.Description like '%Pencairan%'
				---order by c.InvoiceNo

				) as tableBankKas where tablebankkas.CompanyCode=table_invoice.companycode and tablebankkas.branchcode=table_invoice.branchcode and tablebankkas.invoiceno=table_invoice.invoiceno
				FOR XML PATH('')


	  ) as databankkas



	   from (
select a.CompanyCode
	  ,a.branchcode
      ,a.invoiceno
	  ,a.InvoiceDate
	  ,a.joborderno
	  ,a.joborderdate
	  ,a.CustomerCodeBill
	  ,b.customername
	  --,a.LaborDppAmt
	  --,a.PartsDppAmt
	  --,a.MaterialDppAmt
	  ,isnull(jasa.labour,0) as jasa_MSI
	  ,isnull(part.part,0) as part_MSI
	  ,isnull(oli.oli,0) as oli_MSI
	  ,(isnull(jasa.labour,0) + isnull(part.part,0)+isnull(oli.oli,0)) as [Total Debet]
	  

from 
	svtrninvoice as a
	left join gnmstcustomer as b on a.CompanyCode=b.CompanyCode and a.CustomerCodeBill=b.CustomerCode
	--start of jasa
	left join (
		select companycode, branchcode,invoiceno, sum(lbrdppamt) as labour from(
				select 
						a.CompanyCode
					   ,a.BranchCode
						,a.InvoiceNo
					   ,a.invoicedate
					   , a.FPJNo
						, a.InvoiceStatus
	   
						, b.OperationHour
						, b.operationcost
						, b.DiscPct
						, a.joborderdate
						, a.joborderNo
						, a.ProductType
						 , ceiling(b.OperationHour * b.OperationCost * (100.0 - b.DiscPct) * 0.01) LbrDppAmt
						 , a.LaborDppAmt as faktual_labour
						 , IsSubCon = isnull((
										  select top 1 IsSubCon from svMstTask
										   where CompanyCode = a.CompanyCode
											 and BasicModel = a.BasicModel
											 and OperationNo = b.OperationNo
										  ), 0)
						 , a.JobType		
						 --, case 
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 1000)  then 'FSC01'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 5000)  then 'FSC02'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 10000) then 'FSC03'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 20000) then 'FSC04'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 30000) then 'FSC05'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 40000) then 'FSC06'
							--  when (c.JobType like 'FS%' and c.GroupJobType = 'FSC' and c.WarrantyOdometer = 50000) then 'FSC07'
							--  else ''
						 --  end as KsgType
						   ,a.CustomerCode
						   ,a.PoliceRegNo
					  from svTrnInvoice a
					 inner join svTrnInvTask b
						on b.CompanyCode = a.CompanyCode
					   and b.BranchCode = a.BranchCode
					   and b.InvoiceNo = a.InvoiceNo
					 -- left join svMstJob c
						--on c.CompanyCode = a.CompanyCode
					   --and c.BasicModel = a.BasicModel
					   --and c.JobType = a.JobType
					 where 
						a.JobType != 'REWORK'
					 )  table1 where table1.IsSubCon=0 group by companycode, branchcode,invoiceno
	 ) as jasa on a.CompanyCode=jasa.CompanyCode and a.BranchCode=jasa.BranchCode and a.InvoiceNo=jasa.InvoiceNo
	 --end of jasa

	 --start of oli
	 left join (
			select companycode, branchcode, invoiceno, sum(spramt) as oli  from (	
				select a.*
					 , c.ParaValue as GroupTpgo
					 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
					 , case 
						 when left(a.InvoiceNo, 3) = 'INI' or a.JobType = 'PDI' then 'INT,PDI'
						 when left(a.InvoiceNo, 3) = 'INW' or left(a.InvoiceNo, 3) = 'INF' then 'CLM,FSC'
						 else 'CPUS'
					   end GroupJob
					 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
					  , b.typeofgoods
				  from svTrnInvoice a
				 inner join svTrnInvItem b
					on b.CompanyCode = a.CompanyCode
				   and b.BranchCode = a.BranchCode
				   and b.InvoiceNo = a.InvoiceNo
				  left join gnMstLookupDtl c
					on c.CompanyCode = a.CompanyCode
				   and c.CodeID = 'GTGO'
				   and c.LookupValue = b.TypeOfGoods
			) as table1 where IsSublet = 0 and GroupTpgo != 'SPAREPART' and JobType != 'REWORK' group by companycode, branchcode, invoiceno
     ) as oli on a.CompanyCode=oli.CompanyCode and a.BranchCode=oli.BranchCode and a.InvoiceNo=oli.InvoiceNo
	 --end of oli

	 --sparepart
	 left join (
		select companycode, branchcode, invoiceno, sum(total) as part 
			from (
			--Total Workshop Parts Sales Revenue
			select companycode, branchcode, invoiceno, sum(spramt) as total from (	
				select a.*
					 , c.ParaValue as GroupTpgo
					 , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
					 , case 
						 when left(a.InvoiceNo, 3) = 'INI' or a.JobType = 'PDI' then 'INT,PDI'
						 when left(a.InvoiceNo, 3) = 'INW' or left(a.InvoiceNo, 3) = 'INF' then 'CLM,FSC'
						 else 'CPUS'
					   end GroupJob
					 , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
					  , b.typeofgoods
				  from svTrnInvoice a
				 inner join svTrnInvItem b
					on b.CompanyCode = a.CompanyCode
				   and b.BranchCode = a.BranchCode
				   and b.InvoiceNo = a.InvoiceNo
				  left join gnMstLookupDtl c
					on c.CompanyCode = a.CompanyCode
				   and c.CodeID = 'GTGO'
				   and c.LookupValue = b.TypeOfGoods
				 where a.JobType != 'REWORK'
			) as table1 where IsSublet = 0 and GroupTpgo = 'SPAREPART' group by companycode, branchcode, invoiceno

			union all
			-- Total Suzuki Genuine Accessories Sales Revenue - Chargeable to Customer CPUS (External)
			select  companycode, branchcode, invoiceno, sum(dppamt) as total from (
			select 
								inv.InvoiceNo,
								inv.InvoiceDate,
								inv.CompanyCode,
								inv.BranchCode,
								year(inv.InvoiceDate) PeriodYear,
								MONTH(inv.InvoiceDate) PeriodMonth,
								ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
								'0' as origin
							from svTrnInvItem invi	
							left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
							where 
							invi.TypeOfGoods in ('2','5','6','9')
							--group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)

							union all

			select 
								inv.InvoiceNo,
								inv.InvoiceDate,
								inv.CompanyCode,
								inv.BranchCode,
								year(inv.InvoiceDate) PeriodYear,
								MONTH(inv.InvoiceDate) PeriodMonth,
								ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
								'1' as origin
							from svTrnInvItem invi	
							left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
							where invi.TypeOfGoods in ('7','8')
							--group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)
				
				) as accesoris	group by companycode, branchcode, invoiceno
	
			) as sparepart group by companycode, branchcode, invoiceno
	 ) as part on a.CompanyCode=part.CompanyCode and a.BranchCode=part.BranchCode and a.InvoiceNo=part.InvoiceNo
	 --end of sparepart

where (month(a.invoicedate)=".$month." and year(a.invoicedate)=".$year.") and a.BranchCode=".$branchcode." and left(a.invoiceno,3)<>'INI'

union all

select a.CompanyCode
	  ,a.BranchCode
	  ,a.fpjno as invoiceno
	  ,a.fpjdate as invoicedate
	  --,a.InvoiceDate
	  ,null as joborderno
	  ,null as joborderdate
	  ,a.CustomerCodeBill
	  ,b.customername
	  --,0 as LaborDppAmt
	  --,a.totdppamt
	  --,0 as MaterialDppAmt
	  ,0 as jasa_MSI
	  ,a.totdppamt as part_MSI
	  ,0 as oli_MSI
	  ,a.totdppamt as [total invoice]
from 
	spTrnSFPJHdr as a
	left join gnmstcustomer as b on a.CompanyCode=b.CompanyCode and a.CustomerCodeBill=b.CustomerCode
where (month(a.fpjdate)=".$month." and year(a.FPJDATE)=".$year.") and a.BranchCode=".$branchcode." and TypeOfGoods = '0' and  customername not like '%DUTA CENDANA ADIMANDIRI%' 
--order by a.FPJNO
) as table_invoice
--order by invoiceno
) as table_Invoice_full
--order by invoiceno
) as table_full where keterangan ='belum lunas' --and InvoiceNo='INW/22/000033'







--end source table

OPEN @getid
FETCH NEXT
FROM @getid INTO
  @companycode
 ,@branchcode 
 ,@invoiceno 
 ,@invoicedate 
 ,@joborderno 
 ,@joborderdate 
 ,@customercodebill 
 ,@customername 
 ,@jasa_awal 
 ,@part_awal 
 ,@oli_awal 
 ,@jasa_msi 
 ,@part_msi 
 ,@oli_msi 
 ,@total_debet 
 ,@jasa_pay 
 ,@part_pay 
 ,@oli_pay 
 ,@jasa_saldo 
 ,@part_saldo 
 ,@oli_sado 
 ,@paymentamt 
 ,@databankkas 
 ,@keterangan 




WHILE @@FETCH_STATUS = 0
BEGIN
	--ceckhing table
    if EXISTS(select * from aroutstanding where invoiceno=@invoiceno and branchcode=@branchcode)
	begin
	 -- update aroutstanding set customername=customername where invoiceno=@invoiceno and branchcode=@branchcode
		 if not EXISTS(select * from aroutstanding where invoiceno=@invoiceno and branchcode=@branchcode and databankkas=@databankkas)
		   begin
		   
					UPDATE [dcaantariksa].[dbo].[aroutstanding]
					  SET [companycode] = @companycode
					  ,[branchcode] = @branchcode
					  ,[invoiceno] = @invoiceno
					  ,[invoicedate] = @invoicedate
					  ,[joborderno] = @joborderno
					  ,[joborderdate] = @joborderdate
					  ,[customercodebill] = @customercodebill
					  ,[customername] = @customername
					  ,[jasa_awal] = @jasa_awal
					  ,[part_awal] = @part_awal
					  ,[oli_awal] = @oli_awal
					  ,[jasa_MSI] = @jasa_msi
					  ,[part_MSI] = @part_msi
					  ,[oli_MSI] = @oli_msi
					  ,[total_debet] = @total_debet
					  ,[jasa_kredit] = @jasa_pay
					  ,[part_kredit] = @part_pay
					  ,[oli_kredit] = @oli_pay
					  ,[jasa_awal1] = @jasa_saldo
					  ,[part_awal1] = @part_saldo
					  ,[oli_awal1] = @oli_sado
					  ,[paymentamt] = @paymentamt
					  ,[databankkas] = @databankkas
					  ,[keterangan] = @keterangan
				 WHERE invoiceno=@invoiceno and branchcode=@branchcode

				 print 'update'+(@invoiceno)
		   end --end if anak update

	end



	--else if utama atas
	else
	begin
		--ganti dengan insert
		INSERT INTO [dcaantariksa].[dbo].[aroutstanding]
           ([companycode]
           ,[branchcode]
           ,[invoiceno]
           ,[invoicedate]
           ,[joborderno]
           ,[joborderdate]
           ,[customercodebill]
           ,[customername]
           ,[jasa_awal]
           ,[part_awal]
           ,[oli_awal]
           ,[jasa_MSI]
           ,[part_MSI]
           ,[oli_MSI]
           ,[total_debet]
           ,[jasa_kredit]
           ,[part_kredit]
           ,[oli_kredit]
           ,[jasa_awal1]
           ,[part_awal1]
           ,[oli_awal1]
           ,[paymentamt]
           ,[databankkas]
           ,[keterangan])
     VALUES
           (
  @companycode
 ,@branchcode 
 ,@invoiceno 
 ,@invoicedate 
 ,@joborderno 
 ,@joborderdate 
 ,@customercodebill 
 ,@customername 
 ,@jasa_awal 
 ,@part_awal 
 ,@oli_awal 
 ,@jasa_msi 
 ,@part_msi 
 ,@oli_msi 
 ,@total_debet 
 ,@jasa_pay 
 ,@part_pay 
 ,@oli_pay 
 ,@jasa_saldo 
 ,@part_saldo 
 ,@oli_sado 
 ,@paymentamt 
 ,@databankkas 
 ,@keterangan 
		   
		   )



	end
    FETCH NEXT
    FROM @getid INTO 
 @companycode
 ,@branchcode 
 ,@invoiceno 
 ,@invoicedate 
 ,@joborderno 
 ,@joborderdate 
 ,@customercodebill 
 ,@customername 
 ,@jasa_awal 
 ,@part_awal 
 ,@oli_awal 
 ,@jasa_msi 
 ,@part_msi 
 ,@oli_msi 
 ,@total_debet 
 ,@jasa_pay 
 ,@part_pay 
 ,@oli_pay 
 ,@jasa_saldo 
 ,@part_saldo 
 ,@oli_sado 
 ,@paymentamt 
 ,@databankkas 
 ,@keterangan 



END

CLOSE @getid
DEALLOCATE @getid

";

if($this->dbDms->query($sql)){
	return "successed";
}else{
	return "not successed";
}
//$query=$this->dbDms->query($sql);
	//return $query->result_array();
 }



 //function sementara untuk cabang jatiasih




 public function getAllMsiDataCal1($branchcode, $year, $month){


 	  $sql="select asal, sum(total) as total from (
select 'labour MSI' as asal, sum(lbrdppamt) as total
--,(select distinct docno from arTrnBankKasTerimaInvoice where branchcode=table2.branchcode and invoiceno=table2.invoiceno) as docno
from
(
select table1.* from(
select 
        
         ceiling(b.OperationHour * b.OperationCost * (100.0 - b.DiscPct) * 0.01) LbrDppAmt
        
         , IsSubCon = isnull((
                          select top 1 IsSubCon from svMstTask
                           where CompanyCode = a.CompanyCode
                             and BasicModel = a.BasicModel
                             and OperationNo = b.OperationNo
                          ), 0)
      from svTrnInvoice a
     inner join svTrnInvTask b
        on b.CompanyCode = a.CompanyCode
       and b.BranchCode = a.BranchCode
       and b.InvoiceNo = a.InvoiceNo
      left join svMstJob c
        on c.CompanyCode = a.CompanyCode
       and c.BasicModel = a.BasicModel
       and c.JobType = a.JobType
     left join svtrnservice as sv on a.CompanyCode=sv.CompanyCode
       and a.BranchCode=sv.BranchCode 
       and a.JobOrderNo=sv.JobOrderNo
    
     where a.CompanyCode = 6419401
       and a.BranchCode = ".$branchcode."
       and Year(a.Invoicedate) = ".$year."
       and Month(a.Invoicedate) = ".$month."
       and left(a.invoiceno,3)<>'INI'
       and a.JobType != 'REWORK'
    
)  table1 

where table1.IsSubCon=0 
) as table_induk_labour

---- labour ciawi

union all



select 'labour MSI' as asal, sum(lbrdppamt) as total
--,(select distinct docno from arTrnBankKasTerimaInvoice where branchcode=table2.branchcode and invoiceno=table2.invoiceno) as docno
from
(
select table1.* from(
select 
        
         ceiling(b.OperationHour * b.OperationCost * (100.0 - b.DiscPct) * 0.01) LbrDppAmt
        
         , IsSubCon = isnull((
                          select top 1 IsSubCon from svMstTask
                           where CompanyCode = a.CompanyCode
                             and BasicModel = a.BasicModel
                             and OperationNo = b.OperationNo
                          ), 0)
        
      from svTrnInvoice a
     inner join svTrnInvTask b
        on b.CompanyCode = a.CompanyCode
       and b.BranchCode = a.BranchCode
       and b.InvoiceNo = a.InvoiceNo
      left join svMstJob c
        on c.CompanyCode = a.CompanyCode
       and c.BasicModel = a.BasicModel
       and c.JobType = a.JobType
     left join svtrnservice as sv on a.CompanyCode=sv.CompanyCode
       and a.BranchCode=sv.BranchCode 
       and a.JobOrderNo=sv.JobOrderNo
    
     where a.CompanyCode = 6419401
       and a.BranchCode = 641940101
       and Year(a.Invoicedate) = ".$year."
       and Month(a.Invoicedate) = ".$month."
       and left(a.invoiceno,3)<>'INI'
       and a.JobType != 'REWORK'
    
)  table1 

where table1.IsSubCon=0 and foremanid='061902008'
) as table_induk_labour



---- end of labour ciawi


union all

select 'lubricant MSI' as asal, sum(spramt) as total  from (    
    select 
          c.ParaValue as GroupTpgo
         , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
         , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
          , b.typeofgoods
      from svTrnInvoice a
     inner join svTrnInvItem b
        on b.CompanyCode = a.CompanyCode
       and b.BranchCode = a.BranchCode
       and b.InvoiceNo = a.InvoiceNo
      left join gnMstLookupDtl c
        on c.CompanyCode = a.CompanyCode
       and c.CodeID = 'GTGO'
       and c.LookupValue = b.TypeOfGoods
      left join gnMstCustomer as e on a.CustomerCodeBill=e.CustomerCode
      left join svtrnservice as sv on a.CompanyCode=sv.CompanyCode
       and a.BranchCode=sv.BranchCode 
       and a.JobOrderNo=sv.JobOrderNo
      left join gnMstEmployee as emp on sv.ForemanID=emp.EmployeeID and sv.BranchCode=emp.BranchCode and emp.PersonnelStatus=1
    
     where a.CompanyCode = 6419401
       and a.BranchCode = ".$branchcode."
       and Year(a.Invoicedate) = ".$year."
       and Month(a.Invoicedate) = ".$month."
       and a.JobType != 'REWORK'
       and left(a.invoiceno,3)<>'INI'
       --and e.CustomerName not like '%DUTA CENDANA ADIMANDIRI%'
) as table1 where IsSublet = 0 and GroupTpgo != 'SPAREPART'


---- lubricant ciawi

union all


select 'lubricant MSI' as asal, sum(spramt) as total  from (    
    select 
    	 c.ParaValue as GroupTpgo
         , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
         , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
          , b.typeofgoods
      from svTrnInvoice a
     inner join svTrnInvItem b
        on b.CompanyCode = a.CompanyCode
       and b.BranchCode = a.BranchCode
       and b.InvoiceNo = a.InvoiceNo
      left join gnMstLookupDtl c
        on c.CompanyCode = a.CompanyCode
       and c.CodeID = 'GTGO'
       and c.LookupValue = b.TypeOfGoods
      left join gnMstCustomer as e on a.CustomerCodeBill=e.CustomerCode
      left join svtrnservice as sv on a.CompanyCode=sv.CompanyCode
       and a.BranchCode=sv.BranchCode 
       and a.JobOrderNo=sv.JobOrderNo
      left join gnMstEmployee as emp on sv.ForemanID=emp.EmployeeID and sv.BranchCode=emp.BranchCode and emp.PersonnelStatus=1
    
     where a.CompanyCode = 6419401
       and a.BranchCode = 641940101
       and Year(a.Invoicedate) = ".$year."
       and Month(a.Invoicedate) = ".$month."
       and a.JobType != 'REWORK'
       and left(a.invoiceno,3)<>'INI'
       and sv.foremanid='061902008'
       --and e.CustomerName not like '%DUTA CENDANA ADIMANDIRI%'
) as table1 where IsSublet = 0 and GroupTpgo != 'SPAREPART'


---- end of lubricant ciawi

union all


select 'sparepart MSI' as asal, sum(total) as total
from (
--Total Workshop Parts Sales Revenue
select sum(spramt) as total from (  
    select 
          c.ParaValue as GroupTpgo
         , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
         , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
          , b.typeofgoods
      from svTrnInvoice a
     inner join svTrnInvItem b
        on b.CompanyCode = a.CompanyCode
       and b.BranchCode = a.BranchCode
       and b.InvoiceNo = a.InvoiceNo
      left join gnMstLookupDtl c with(nolock,nowait)
        on c.CompanyCode = a.CompanyCode
       and c.CodeID = 'GTGO'
       and c.LookupValue = b.TypeOfGoods
      left join gnMstCustomer as e with(nolock,nowait) on a.CustomerCodeBill=e.CustomerCode
      left join svtrnservice as sv on 
            a.CompanyCode=sv.CompanyCode
            and a.BranchCode=sv.BranchCode
            and a.JobOrderNo=sv.JobOrderNo
      left join gnMstEmployee as emp on sv.CompanyCode=emp.CompanyCode
           and sv.BranchCode=emp.branchcode
           and sv.ForemanID=emp.EmployeeID
           and emp.PersonnelStatus=1
     where a.CompanyCode = 6419401
       and a.BranchCode = ".$branchcode."
       and Year(a.Invoicedate) = ".$year."
       and Month(a.Invoicedate) = ".$month."
       and left(a.invoiceno,3)<>'INI'
       and a.JobType != 'REWORK'
       --and e.CustomerName not like '%DUTA CENDANA ADIMANDIRI%'
       
) as table1 where IsSublet = 0 and GroupTpgo = 'SPAREPART'


---- Total Workshop Parts Sales Revenue ciawi

union all

select sum(spramt) as total from (  
    select 
         c.ParaValue as GroupTpgo
         , case when TypeofGoods in ('0', '1') then '0' else '1' end IsSublet
         
         , (ceiling((b.SupplyQty - b.ReturnQty) * b.RetailPrice * (100.0 - b.DiscPct) * 0.01)) SprAmt
          , b.typeofgoods
      from svTrnInvoice a
     inner join svTrnInvItem b
        on b.CompanyCode = a.CompanyCode
       and b.BranchCode = a.BranchCode
       and b.InvoiceNo = a.InvoiceNo
      left join gnMstLookupDtl c
        on c.CompanyCode = a.CompanyCode
       and c.CodeID = 'GTGO'
       and c.LookupValue = b.TypeOfGoods
      left join gnMstCustomer as e on a.CustomerCodeBill=e.CustomerCode
      left join svtrnservice as sv on 
            a.CompanyCode=sv.CompanyCode
            and a.BranchCode=sv.BranchCode
            and a.JobOrderNo=sv.JobOrderNo
      left join gnMstEmployee as emp on sv.CompanyCode=emp.CompanyCode
           and sv.BranchCode=emp.branchcode
           and sv.ForemanID=emp.EmployeeID
           and emp.PersonnelStatus=1
     where a.CompanyCode = 6419401
       and a.BranchCode = 641940101
       and Year(a.Invoicedate) = ".$year."
       and Month(a.Invoicedate) = ".$month."
       and left(a.invoiceno,3)<>'INI'
       and a.JobType != 'REWORK'
       and sv.foremanid='061902008'
       --and e.CustomerName not like '%DUTA CENDANA ADIMANDIRI%'
       
) as table1 where IsSublet = 0 and GroupTpgo = 'SPAREPART'





---- end Total Workshop Parts Sales Revenue ciawi



union all
-- counter sales part
select sum(totDPPamt) as total 
                  from spTrnSFPJHdr as a
                  left join gnMstCustomer as b on a.customercodebill = b.CustomerCode
                  where 1 = 1
                   and a.CompanyCode = 6419401
                   and BranchCode = ".$branchcode."
                   and year(a.FPJDate) = ".$year."
                   and month(a.FPJDate) = ".$month."
                   and a.TypeOfGoods = '0'
                   and b.customername not like('%duta cendana adimandiri%')

union all
-- Total Suzuki Genuine Accessories Sales Revenue - Chargeable to Customer CPUS (External)
select sum(DPPamt) as total from (
select 
                    
                    ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
                    '0' as origin
                from svTrnInvItem invi  
                left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
                left join gnMstCustomer as e on inv.CustomerCodeBill=e.CustomerCode
                left join svtrnservice as sv on inv.CompanyCode=sv.CompanyCode
                   and inv.BranchCode=sv.BranchCode 
                   and inv.JobOrderNo=sv.JobOrderNo
                left join gnmstemployee as emp on sv.BranchCode=emp.BranchCode
                   and sv.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
                where invi.companycode=6419401
                and invi.branchCode=".$branchcode."
                and invi.TypeOfGoods in ('2','5','6','9')
                and year(inv.InvoiceDate)=".$year."
                and MONTH(inv.InvoiceDate)=".$month."
                and left(inv.invoiceno,3)<>'INI'
                
                --and e.customername not like('%duta cendana adimandiri%')
                --group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)

                union all

select 
                    inv.InvoiceNo,
                    inv.InvoiceDate,
                    inv.CompanyCode,
                    inv.BranchCode,
                    year(inv.InvoiceDate) PeriodYear,
                    MONTH(inv.InvoiceDate) PeriodMonth,
                    ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
                    '1' as origin
                from svTrnInvItem invi  
                left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
                left join gnMstCustomer as e on inv.CustomerCodeBill=e.CustomerCode
                left join svtrnservice as sv on inv.CompanyCode=sv.CompanyCode
                   and inv.BranchCode=sv.BranchCode 
                   and inv.JobOrderNo=sv.JobOrderNo
                left join gnmstemployee as emp on sv.BranchCode=emp.BranchCode
                   and sv.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
                where invi.companycode=6419401
                and invi.branchCode=".$branchcode."
                and invi.TypeOfGoods in ('7','8')
                and year(inv.InvoiceDate)=".$year."
                and MONTH(inv.InvoiceDate)=".$month."
                and left(inv.invoiceno,3)<>'INI'
                --and e.customername not like('%duta cendana adimandiri%')
                
                
                
    ) as accesoris  
    

-- Total Suzuki Genuine Accessories Sales Revenue - Chargeable to Customer CPUS (External) ciawi


union all


select sum(DPPamt) as total from (
select 
                   
                    ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
                    '0' as origin
                from svTrnInvItem invi  
                left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
                left join gnMstCustomer as e on inv.CustomerCodeBill=e.CustomerCode
                left join svtrnservice as sv on inv.CompanyCode=sv.CompanyCode
                   and inv.BranchCode=sv.BranchCode 
                   and inv.JobOrderNo=sv.JobOrderNo
                left join gnmstemployee as emp on sv.BranchCode=emp.BranchCode
                   and sv.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
                where invi.companycode=6419401
                and invi.branchCode=641940101
                and invi.TypeOfGoods in ('2','5','6','9')
                and year(inv.InvoiceDate)=".$year."
                and MONTH(inv.InvoiceDate)=".$month."
                and left(inv.invoiceno,3)<>'INI'
                and sv.foremanid='061902008'
                --and e.customername not like('%duta cendana adimandiri%')
                --group by inv.CompanyCode,inv.BranchCode,year(inv.InvoiceDate),MONTH(inv.InvoiceDate)

                union all

select 
                    
                    ceiling((invi.SupplyQty - invi.ReturnQty) * invi.RetailPrice * (100.0 - invi.DiscPct) * 0.01) as DPPAmt,
                    '1' as origin
                from svTrnInvItem invi  
                left join [svTrnInvoice] inv on inv.CompanyCode=invi.CompanyCode and inv.BranchCode=invi.BranchCode and inv.InvoiceNo=invi.InvoiceNo
                left join gnMstCustomer as e on inv.CustomerCodeBill=e.CustomerCode
                left join svtrnservice as sv on inv.CompanyCode=sv.CompanyCode
                   and inv.BranchCode=sv.BranchCode 
                   and inv.JobOrderNo=sv.JobOrderNo
                left join gnmstemployee as emp on sv.BranchCode=emp.BranchCode
                   and sv.ForemanID=emp.EmployeeID and emp.PersonnelStatus=1
                where invi.companycode=6419401
                and invi.branchCode=641940101
                and invi.TypeOfGoods in ('7','8')
                and year(inv.InvoiceDate)=".$year."
                and MONTH(inv.InvoiceDate)=".$month."
                and left(inv.invoiceno,3)<>'INI'
                and sv.foremanid='061902008'
                --and e.customername not like('%duta cendana adimandiri%')
                
                
                
    ) as accesoris  



-- end Total Suzuki Genuine Accessories Sales Revenue - Chargeable to Customer CPUS (External) ciawi
    
) as table1 

)table_induk group by asal ";

	$query=$this->dbDms->query($sql);
		return $query->result_array();

 }
 	
 





 // end function sementara untuk cabang jatiasih


}