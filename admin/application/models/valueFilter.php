 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 class valueFilter extends CI_Model {

 	public function __construct () {
 		parent::__construct();
 		$this->dbDms=$this->load->database('dms',true);
 		//$this->load->model('mabsen');
 		
    		
  	} 

  	public function getAllBranch(){
  		  $arr = array();
  		  $sql="SELECT CompanyCode, BranchCode, OutletName
          FROM   pmBranchOutlets";
  		 
  		  $query=$this->dbDms->query($sql);
  		   return $query->result_array();
  		  

  	}


  	public function getAllTipe(){
  		  $arr = array();
  		  $sql="select distinct(GroupCode) as tipe from gnMstGroupModel";
  		 
  		  $query=$this->dbDms->query($sql);
  		   return $query->result_array();
  		  

  	}



  	public function getAllModel(){
  		  $arr = array();
  		  $sql="select distinct(TypeCode) as model from gnMstGroupModel";
  		 
  		  $query=$this->dbDms->query($sql);
  		   return $query->result_array();
  		  

  	}


  	public function getAllModelFilter($bulan){

  		 
  		 
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}

  	public function getAllSalesmanHead($bulan){
  		  
  		 
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}

  	public function getAllSalesman($bulan){
  		  
  		 
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}

  	public function getValueOfDO($bulan){
  		  	
  	   // $sql=$sql. $sqlwhere.$sqlEnd;

  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	  	public function getValueOfSPK($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function getValueOfProspek($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}



  	public function getValueOfHotProspek($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}



  	 public function getValueOfInquiry($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}




  	public function getValueOfInquiryLost($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}



  	public function getValueOfGrowth($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function getValueOfContributionSource($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function getValueOfInquiryCaraBayar($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}

  	public function getValueOfInquiryLeasing($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function getValueOfInquiryToSpk($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}

  	public function getValueOfInquiryToDO($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function inquiryToProspek($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function inquiryLostToSPK($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function inquiryLostToDO($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function inquiryLostToProspek($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}


  	public function inquiryToInquiryLost($bulan){
  		  
  		  $query=$this->dbDms->query($bulan);
  		   return $query->result_array();
  		  

  	}

    public function getInquirySource($bulan){
        
        $query=$this->dbDms->query($bulan);
         return $query->result_array();
        

    }




 }
