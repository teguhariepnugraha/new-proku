<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mdetail_incentive_sro_report extends CI_Model {

	public function __construct() {
        parent::__construct();
        $this->dbDms=$this->load->database('dms',true);
    }

// public function getcreatedBy($bulan,$tahun,$cabang,$relateduser){
//   		  //  $bulan=$_GET['bulan'];
//  	    // $tahun=$_GET['tahun'];
//  	    // $cabang=$_GET['cabang'];
//  	  //   $sql="";
//   		//   $arr = array();
//   		//   if($cabang=="641940101"){
//   		//   		 $sql="select distinct createdby from svtrnservice as a
// 				// where (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." ) and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy not like '%BKS%'

//  			//   ";
 			  
//   		//   }else  if($cabang=="641940102"){
//   		//   		$sql="select distinct createdby from svtrnservice as a
// 				// where (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." ) and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy like '%sro%'";

//   		//   }else  if($cabang=="641940103"){
//   		//   		$sql="select distinct createdby from svtrnservice as a
// 				// where (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." ) and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy not like '%BKS%'";

//   		//   }else  if($cabang=="641940104"){
//   		//   		$sql="select distinct createdby from svtrnservice as a
// 				// where (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." ) and (a.BranchCode in (641940103,641940101) ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy like '%BKSSRO%'";
//   		//   }

//   		//   //echo "isi querynya adalah :".$sql;
  		 
//   		//   $query=$this->dbDms->query($sql)->result();
//   		// //   while($row = mysql_fetch_array($query)) {
// 	   // //  			array_push($arr, $row);
// 		  // // }
//   		//   	$counter=0;
// 		  // foreach ($query as $key ) {         
//     //  		 //array_push($arr,[$key->createdby]="teguh");
// 		  // 	$counter=$counter+1;
// 		  // 	$arraybooking=array();
// 		  // 	$arraycalsro=array();
// 		  // 	$arraybooking=$this->getlistDataBooking($bulan,$tahun,$cabang, $key->createdby);
// 		  // 	$arraycalsro=$this->getcalculateddatasro($bulan,$tahun,$cabang, $key->createdby);
// 		  // 	$arraysumsro=$this->getcalculateddatasrosumary($bulan,$tahun,$cabang, $key->createdby);
// 		  // 	 //array_push($arr,["bookingsro".$counter=>$arraybooking]);
// 		  // 	 $arr["bookingsro".$counter]=$arraybooking;
// 		  // 	 $arr["calsro".$counter]=$arraycalsro;
// 		  // 	 $arr["sumsro".$counter]=$arraysumsro;
//     //   	  }

// 			//$relateduser=$this->session->userdata('relateduser');
//     		$arraybooking=$this->getlistDataBooking($bulan,$tahun,$cabang, $relateduser);
// 		    $arraycalsro=$this->getcalculateddatasro($bulan,$tahun,$cabang, $relateduser);
// 		    $arraysumsro=$this->getcalculateddatasrosumary($bulan,$tahun,$cabang, $relateduser);
// 		    $arr["bookingsro"]=$arraybooking;
// 		  	 $arr["calsro"]=$arraycalsro;
// 		   	 $arr["sumsro"]=$arraysumsro;
//       	  	$arr["nama_cabang"]=$cabang;
//       	  	    $arr["select-bulan"]=$bulan;
//       	  	    $arr["select-tahun"]=$tahun;
//   		  return $arr;

//   	}


//   	function getlistDataBooking($bulan,$tahun,$cabang,$createdby){
//   	// 	  $bulan=$_GET['bulan'];
//  	 //    $year=$_GET['tahun'];
//  	 //     $cabang=$_GET['cabang'];
//   	$sql="";
//   		  $arr = array();
//   	if($cabang=="641940101"){

//   		$sql="select 
// 	createddate,
// 	createddate_spk as createddate_SPK_Booking,
// 	bookingdate,
// 	joborderdate1 as SPK_date,
// 	PoliceRegNo,
// 	JobOrderNo as SPKNo,
// 	invoiceno,
// 	invoicedate,
// 	createdby,
// 	fullname,
// 	case when convert(date,createddate)<convert(date,bookingdate) and month(invoicedate)=".$bulan." then 'counted' else 'No' end as status
// from (
// select distinct
// 	  a.createddate,
// 	  (select top(1) createddate from SvRegisterSpk where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as createddate_spk,
// 	  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) InvoiceDate from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  a.createdby,
// 	  b.fullname
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
// 	   where (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." ) and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy ='".$createdby."'

// ) as table1 order by bookingdate, createdby";

//   	}else if($cabang=="641940102"){

//   		$sql="select 
// 	createddate,
// 	createddate_spk as createddate_SPK_Booking,
// 	bookingdate,
// 	joborderdate1 as SPK_date,
// 	PoliceRegNo,
// 	JobOrderNo as SPKNo,
// 	invoiceno,
// 	invoicedate,
// 	createdby,
// 	fullname,
// 	case when convert(date,createddate)<convert(date,bookingdate) and month(invoicedate)=".$bulan." then 'counted' else 'No' end as status
// from (
// select distinct
// 	  a.createddate,
// 	  (select top(1) createddate from SvRegisterSpk where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as createddate_spk,
// 	  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) InvoiceDate from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  a.createdby,
// 	  b.fullname
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
// 	   where (a.BranchCode in (".$cabang.") ) and (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." )  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy ='".$createdby."'

// ) as table1 order by bookingdate asc, createdby";

//   	}else if($cabang=="641940103"){

//   		$sql="select 
// 	createddate,
// 	createddate_spk as createddate_SPK_Booking,
// 	bookingdate,
// 	joborderdate1 as SPK_date,
// 	PoliceRegNo,
// 	JobOrderNo as SPKNo,
// 	invoiceno,
// 	invoicedate,
// 	createdby,
// 	fullname,
// 	case when convert(date,createddate)<convert(date,bookingdate) and month(invoicedate)=".$bulan." then 'counted' else 'No' end as status
// from (
// select distinct
// 	  a.createddate,
// 	  (select top(1) createddate from SvRegisterSpk where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as createddate_spk,
// 	  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) InvoiceDate from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  a.createdby,
// 	  b.fullname
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
// 	   where (a.BranchCode in (".$cabang.") ) and (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." )  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy ='".$createdby."'

// ) as table1 order by bookingdate, createdby";

//   	}else if($cabang=="641940104"){
//   			$sql="select 
// 	createddate,
// 	createddate_spk as createddate_SPK_Booking,
// 	bookingdate,
// 	joborderdate1 as SPK_date,
// 	PoliceRegNo,
// 	JobOrderNo as SPKNo,
// 	invoiceno,
// 	invoicedate,
// 	createdby,
// 	fullname,
// 	case when convert(date,createddate)<convert(date,bookingdate) and month(invoicedate)=".$bulan." then 'counted' else 'No' end as status
// from (
// select distinct
// 	  a.createddate,
// 	  (select top(1) createddate from SvRegisterSpk where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as createddate_spk,
// 	  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) InvoiceDate from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  a.createdby,
// 	  b.fullname
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
// 	   where (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." ) and (a.BranchCode in (641940103,641940101) ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy ='".$createdby."'

// ) as table1 order by bookingdate, createdby";
//   	}
  		  
  		 
//   		  $query=$this->dbDms->query($sql);
//   		   return $query->result_array();
  		  

// }

// public function getcalculateddatasro($bulan,$tahun,$cabang,$createdby){
//  	      // $bulan=$_GET['bulan'];
//  	      // $year=$_GET['tahun'];
//  	      // $cabang=$_GET['cabang'];

//  	      $sql="";
//   		  $arr = array();

//   		  if($cabang=="641940101"){

//   		  	 $sql="select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive,fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  a.createddate,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
	 
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
// 	   where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." ) and (a.BranchCode in (".$cabang.")) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
// 	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby = '".$createdby."'



// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname order by bookingdate";




//   		  }else if($cabang=="641940102"){

//   		  		 $sql="select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive,fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  a.createddate,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
	 
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
// 	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." )  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
// 	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby = '".$createdby."'



// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname order by bookingdate";

//   		  }else if($cabang=="641940103"){

//   		  	 $sql="select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive,fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  a.createddate,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
	 
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
// 	   where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." ) and (a.BranchCode in (".$cabang.")) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
// 	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby = '".$createdby."'



// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname order by bookingdate";


//   		  }else if($cabang=="641940104"){

//   		  		 $sql="select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive,fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  a.createddate,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
	 
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
// 	   where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." ) and (a.BranchCode in (641940103,641940101)) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
// 	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby = '".$createdby."'



// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname order by bookingdate";


//   		  }


  		 
//   		  $query=$this->dbDms->query($sql);
//   		   return $query->result_array();
  		  

//   	}


// public function getcalculateddatasrosumary($bulan,$tahun,$cabang,$createdby){

//   		$sql="";
//   		  $arr = array();

//   		if($cabang=="641940101"){
//   			  		$sql="select  'CCE' as Position, '15%' as total_incentive, sum(incentive)*15/100 as totalIncentive, case when incentive=NULL then 0 else sum(incentive) end as total,FullName from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, FullName
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
// 	     where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, FullName 

// ) as table2 group by FullName  

// union all


// select  'CCM' as Position, '10%' as total_incentive, sum(incentive)*10/100 as totalIncentive, sum(incentive) as total,fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive,
// fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	  where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, FullName 

// ) as table2 group by FullName 

// union all

// select  'SRO' as Position, '75%' as total_incentive, sum(incentive)*75/100 as totalIncentive, sum(incentive) as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	    where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 

// ) as table2 group by fullname";


//   		}else if($cabang=="641940102"){

//   		$sql="select  'CCE' as Position, '15%' as total_incentive, sum(incentive)*15/100 as totalIncentive, case when incentive=NULL then 0 else sum(incentive) end as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	     where (a.BranchCode in (".$cabang.") ) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.")  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname

// ) as table2 group by fullname 

// union all


// select  'CCM' as Position, '10%' as total_incentive, sum(incentive)*10/100 as totalIncentive, sum(incentive) as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	  where (a.BranchCode in (".$cabang.") ) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.")  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname

// ) as table2 group by fullname 

// union all

// select  'SRO' as Position, '75%' as total_incentive, sum(incentive)*75/100 as totalIncentive, sum(incentive) as total,fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
// 	    where (a.BranchCode in (".$cabang.") ) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname

// ) as table2 group by fullname";

//   		}else if($cabang=="641940103"){

//   		$sql="select  'CCE' as Position, '15%' as total_incentive, sum(incentive)*15/100 as totalIncentive, case when incentive=NULL then 0 else sum(incentive) end as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	     where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 

// ) as table2 group by fullname 

// union all


// select  'CCM' as Position, '10%' as total_incentive, sum(incentive)*10/100 as totalIncentive, sum(incentive) as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	  where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 

// ) as table2 group by fullname  

// union all

// select  'SRO' as Position, '75%' as total_incentive, sum(incentive)*75/100 as totalIncentive, sum(incentive) as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	    where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname

// ) as table2 group by FullName";

//   		}else if($cabang=="641940104"){

//   		$sql="select  'CCE' as Position, '15%' as total_incentive, sum(incentive)*15/100 as totalIncentive, case when incentive=NULL then 0 else sum(incentive) end as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	     where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (641940103,641940101) ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname

// ) as table2 group by fullname  

// union all


// select  'CCM' as Position, '10%' as total_incentive, sum(incentive)*10/100 as totalIncentive, sum(incentive) as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId


// 	  where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (641940103,641940101) ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 

// ) as table2 group by fullname 

// union all

// select  'SRO' as Position, '75%' as total_incentive, sum(incentive)*75/100 as totalIncentive, sum(incentive) as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	    where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (641940103,641940101) ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname

// ) as table2 group by fullname";

//   		}


//   			$query=$this->dbDms->query($sql);
//   		   return $query->result_array();

//   	}




public function getcreatedBy($bulan,$tahun,$cabang,$idpengajuan){
  		$bulan=$bulan;
 	    $tahun=$tahun;
 	    $idpengajuan=$idpengajuan;
 	    // $cabang=$_GET['cabang'];
	      // $cabang=$this->session->userdata('cabang');
 	         $cabang=$cabang;

 	     $sql="";
  	   $arr = array();
	//---- masih bisa digunakan untuk kedapan nya ---//
  		  if($cabang=="641940101"){
  		  		 $sql="select distinct createdby from svtrnservice as a
				where (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." ) and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy like '%SRO%'

 			  ";
 			  
  		  }else  if($cabang=="641940102"){
  		  		$sql="select distinct createdby from svtrnservice as a
				where (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." ) and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy like '%sro%'";

  		  }else  if($cabang=="641940103"){
  		  		$sql="select distinct createdby from svtrnservice as a
				where (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." ) and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy like '%SRO%'";
		  }
   //-- untuk saat ini sudah tidak digunakan ---//
  		//   }else  if($cabang=="641940104"){
  		//   		$sql="select distinct createdby from svtrnservice as a
				// where (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." ) and (a.BranchCode in (641940103,641940101) ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy like '%BKSSRO%'";
  		//   }

  		  //echo "isi querynya adalah :".$sql;
  //-- masih bisa digunakan untuk ---//
  		 $query=$this->dbDms->query($sql)->result();
  		//   while($row = mysql_fetch_array($query)) {
	   //  			array_push($arr, $row);
		  // }
  		  	$counter=0;
  		  	$createdSRO=array();
  		  	$arraybooking=array();
  		  	$arraytotalbooking=array();
  		  	$arrayBookingSUM=array();
  		  	$arraybookingRange=array();
  		  	$arraytotalbookingRange=array();
  		  	$arraybookingRangeSum=array();
  		  	$arraytargetunitentry=array();
  		  	$arraytargetunitentrySumRange=array();
  		    $arrayPIC=array();
  		  	$listbulan=array();
  		  	$listvalue=array();

		    foreach ($query as $key ) {         
      		 //array_push($arr,[$key->createdby]="teguh");
		  	$counter=$counter+1;
		  	
		  	//$arraycalsro=array();
		  	array_push($createdSRO,$key->createdby);
		  	//$arraybooking=$this->getlistBookingCianjur($bulan,$tahun,$cabang, $key->createdby);
		  // 	$arraycalsro=$this->getcalculatedsroCJR($bulan,$tahun,$cabang, $key->createdby);
		  // 	$arraysumsro=$this->getcalculatedsrosumary($bulan,$tahun,$cabang, $key->createdby);
		  // 	 //array_push($arr,["bookingsro".$counter=>$arraybooking]);
		   	 //$arr["bookingsro".$counter]=$arraybooking;
		  //	 $arr["calsro".$counter]=$arraycalsro;
		  //	 $arr["sumsro".$counter]=$arraysumsro;
      	  }
      	  		$arraybooking=$this->getlistBookingCianjur($bulan,$tahun,$cabang, $createdSRO);
	     $arr["bookingsro"] =$arraybooking;  
	     //logical baru
	     $arraytotalbooking=$this->getlisttotalBookingCianjur($bulan,$tahun,$cabang, $createdSRO);
	     $arr["totalbooking"]=$arraytotalbooking;
	     //end logical baru
	     $arrayBookingSUM=$this->getlistBookingCianjurSUM($bulan,$tahun,$cabang, $createdSRO);
	     $arr["bookingsroSUM"] =$arrayBookingSUM; 

	     $arraybookingRange=$this->getlistBookingCianjurRange($bulan,$tahun,$cabang, $createdSRO);
	     $arr['bookingsroRange'] = $arraybookingRange;
	     //logicl baru total bookingrange
	     $arraytotalbookingRange=$this->getlisttotalBookingCianjurRange($bulan,$tahun,$cabang, $createdSRO);
	     $arr['totalbookingRange']=$arraytotalbookingRange;
	     //end logical baru
	    $arraybookingRangeSum=$this->getlistBookingCianjurSUMRange($bulan,$tahun,$cabang, $createdSRO);
	    $arr['bookingsroRangeSum']=$arraybookingRangeSum;
	    $arraytargetunitentry=$this->getlistTargetEntryCianjurSUM($bulan,$tahun,$cabang, $createdSRO);
	    $arr['targetunitentrySum']=$arraytargetunitentry;
	    $arraytargetunitentrySumRange= $this->getlistTargetEntryCianjurSUMRange($bulan,$tahun,$cabang, $createdSRO);
	    $arr['targetunitentrySumRange']=$arraytargetunitentrySumRange;

	    	$arrayPIC=$this->getPIC($idpengajuan);
	    $arr["PIC"]=$arrayPIC;

	    foreach ($arraybookingRangeSum as $lihat):
	    		if($lihat["bulan"]==1){
	    			array_push($listbulan,"Jan");
	    			
	    		}
	    		if($lihat["bulan"]==2){
	    			array_push($listbulan,"Feb");
	    			
	    		}
	    		if($lihat["bulan"]==3){
	    			array_push($listbulan,"Mar");
	    			
	    		}
	    		if($lihat["bulan"]==4){
	    			array_push($listbulan,"Apr");
	    			
	    		}
	    		if($lihat["bulan"]==5){
	    			array_push($listbulan,"Mei");
	    			
	    		}
	    		if($lihat["bulan"]==6){
	    			array_push($listbulan,"Jun");
	    			
	    		}
	    		if($lihat["bulan"]==7){
	    			array_push($listbulan,"Jul");
	    			
	    		}
	    		if($lihat["bulan"]==8){
	    			array_push($listbulan,"Agu");
	    			
	    		}
	    		if($lihat["bulan"]==9){
	    			array_push($listbulan,"Sep");
	    			
	    		}
	    		if($lihat["bulan"]==10){
	    			array_push($listbulan,"Okt");
	    			
	    		}
	    		if($lihat["bulan"]==11){
	    			array_push($listbulan,"Nov");
	    			
	    		}
	    		if($lihat["bulan"]==12){
	    			array_push($listbulan,"Des");

	    		}
	    		array_push($listvalue,ceil($lihat["total"]*100/$arraytargetunitentrySumRange[0][$lihat["bulan"]]));
	    endforeach;

	   //echo print_r(json_encode($listbulan));
	   echo print_r(json_encode($arrayPIC));
	   //echo implode(" ",$listbulan);
	   //echo implode(" ",$listvalue);

	   $arr["listbulan"]=$listbulan;
	   $arr["listvalue"]=$listvalue;





       

      	  	    //$relateduser=$this->session->userdata('relateduser');
      // 	  	    $arraybooking=$this->getlistBookingCianjur($bulan,$tahun,$cabang, $relateduser);
      // 	  	    $arraycalsro=$this->getcalculatedsroCJR($bulan,$tahun,$cabang, $relateduser);
      // 	  	    $arraysumsro=$this->getcalculatedsrosumary($bulan,$tahun,$cabang, $relateduser);
      // 	  	    $arr["bookingsro"]=$arraybooking;
		    //     $arr["calsro"]=$arraycalsro;
		  		// $arr["sumsro"]=$arraysumsro;
       	  	    $arr["nama_cabang"]=$cabang;
      	  	    $arr["select-bulan"]=$bulan;
       	  	    $arr["select-tahun"]=$tahun;
  		  
  		  return $arr;


   }
      	  






  	

  



 function getlistBookingCianjurSUM($bulan,$tahun,$cabang,$createdby){
  	// 	  $bulan=$_GET['bulan'];
 	 //    $year=$_GET['tahun'];
 	 //     $cabang=$_GET['cabang'];
  	$sql="";
  		  $arr = array();
  	if($cabang=="641940101"){

  		  		$sql="select bulan, sum(total) as total from (
Select fullname as fullname, sum(jumlah) as total, month(bookingdate) as bulan from (
select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah, fullname
from(
select distinct  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.servicetype,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  a.createddate,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  b.fullname
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." )  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby in ('".implode("','",$createdby)."') 



) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 
) as table2 group by fullname, bookingdate 
) as table3 group by bulan order by bulan";

  	}else if($cabang=="641940102"){

  		$sql="select bulan, sum(total) as total from (
Select fullname as fullname, sum(jumlah) as total, month(bookingdate) as bulan from (
select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah, fullname
from(
select distinct  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.servicetype,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  a.createddate,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  b.fullname
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." )  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby in ('".implode("','",$createdby)."') 



) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 
) as table2 group by fullname, bookingdate 
) as table3 group by bulan order by bulan";

  	}else if($cabang=="641940103"){


  		$sql="select bulan, sum(total) as total from (
Select fullname as fullname, sum(jumlah) as total, month(bookingdate) as bulan from (
select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah, fullname
from(
select distinct  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.servicetype,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  a.createddate,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  b.fullname
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." )  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby in ('".implode("','",$createdby)."') 



) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 
) as table2 group by fullname, bookingdate 
) as table3 group by bulan order by bulan";

  	}else if($cabang=="641940104"){
  			$sql="select 
	createddate,
	createddate_spk as createddate_SPK_Booking,
	bookingdate,
	joborderdate1 as SPK_date,
	PoliceRegNo,
	JobOrderNo as SPKNo,
	invoiceno,
	invoicedate,
	createdby,
	fullname,
	case when convert(date,createddate)<convert(date,bookingdate) and month(invoicedate)=".$bulan." then 'counted' else 'No' end as status
from (
select distinct
	  a.createddate,
	  (select top(1) createddate from SvRegisterSpk where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as createddate_spk,
	  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) InvoiceDate from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  a.createdby,
	  b.fullname
	  from svtrnservice as a
	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." ) and (a.BranchCode in (641940103,641940101) ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy ='".$createdby."'

) as table1 order by bookingdate, createdby";
  	}
  		  
  		 
  		  $query=$this->dbDms->query($sql);
  		   return $query->result_array();
  		  

}



function getlistBookingCianjurSUMRange($bulan,$tahun,$cabang,$createdby){
  	// 	  $bulan=$_GET['bulan'];
 	 //    $year=$_GET['tahun'];
 	 //     $cabang=$_GET['cabang'];
  	$sql="";
  		  $arr = array();
  	if($cabang=="641940101"){

  		$sql="select bulan, sum(total) as total from (
Select fullname as fullname, sum(jumlah) as total, month(bookingdate) as bulan from (
select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah, fullname
from(
select distinct  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.servicetype,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  a.createddate,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  b.fullname
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)<=".$bulan." )  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby in ('".implode("','",$createdby)."') 



) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 
) as table2 group by fullname, bookingdate 
) as table3 group by bulan order by bulan";

  	}else if($cabang=="641940102"){

  		$sql="select bulan, sum(total) as total from (
Select fullname as fullname, sum(jumlah) as total, month(bookingdate) as bulan from (
select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah, fullname
from(
select distinct  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.servicetype,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  a.createddate,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  b.fullname
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)<=".$bulan." )  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby in ('".implode("','",$createdby)."') 



) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 
) as table2 group by fullname, bookingdate 
) as table3 group by bulan order by bulan";

  	}else if($cabang=="641940103"){

  		$sql="select bulan, sum(total) as total from (
Select fullname as fullname, sum(jumlah) as total, month(bookingdate) as bulan from (
select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah, fullname
from(
select distinct  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.servicetype,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  a.createddate,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  b.fullname
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)<=".$bulan." )  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby in ('".implode("','",$createdby)."') 



) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 
) as table2 group by fullname, bookingdate 
) as table3 group by bulan order by bulan";


  	}else if($cabang=="641940104"){
  			$sql="select 
	createddate,
	createddate_spk as createddate_SPK_Booking,
	bookingdate,
	joborderdate1 as SPK_date,
	PoliceRegNo,
	JobOrderNo as SPKNo,
	invoiceno,
	invoicedate,
	createdby,
	fullname,
	case when convert(date,createddate)<convert(date,bookingdate) and month(invoicedate)=".$bulan." then 'counted' else 'No' end as status
from (
select distinct
	  a.createddate,
	  (select top(1) createddate from SvRegisterSpk where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as createddate_spk,
	  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) InvoiceDate from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  a.createdby,
	  b.fullname
	  from svtrnservice as a
	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." ) and (a.BranchCode in (641940103,641940101) ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy ='".$createdby."'

) as table1 order by bookingdate, createdby";
  	}
  		  
  		 
  		  $query=$this->dbDms->query($sql);
  		   return $query->result_array();
  		  

}



function getlistBookingCianjur($bulan,$tahun,$cabang,$createdby){
  	// 	  $bulan=$_GET['bulan'];
 	 //    $year=$_GET['tahun'];
 	 //     $cabang=$_GET['cabang'];
  	$sql="";
  		  $arr = array();
  	if($cabang=="641940101"){

$sql="select bulan, fullname, total, count(fullname) OVER(PARTITION BY bulan ORDER BY fullname DESC) Rank from (
  		select bulan, fullname, sum(total) as total from (
Select fullname as fullname, sum(jumlah) as total, month(bookingdate) as bulan from (
select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah, fullname
from(
select distinct  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.servicetype,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  a.createddate,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  b.fullname
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." )  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby in ('".implode("','",$createdby)."') 



) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 
) as table2 group by fullname, bookingdate 
) as table3 group by bulan, fullname 
) as table4 group by bulan, fullname, total order by bulan";


  	}else if($cabang=="641940102"){

  		$sql="select bulan, fullname, total, count(fullname) OVER(PARTITION BY bulan ORDER BY fullname DESC) Rank from (
  		select bulan, fullname, sum(total) as total from (
Select fullname as fullname, sum(jumlah) as total, month(bookingdate) as bulan from (
select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah, fullname
from(
select distinct  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.servicetype,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  a.createddate,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  b.fullname
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." )  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby in ('".implode("','",$createdby)."') 



) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 
) as table2 group by fullname, bookingdate 
) as table3 group by bulan, fullname 
) as table4 group by bulan, fullname, total order by bulan";

  	}else if($cabang=="641940103"){

$sql="select bulan, fullname, total, count(fullname) OVER(PARTITION BY bulan ORDER BY fullname DESC) Rank from (
  		select bulan, fullname, sum(total) as total from (
Select fullname as fullname, sum(jumlah) as total, month(bookingdate) as bulan from (
select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah, fullname
from(
select distinct  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.servicetype,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  a.createddate,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  b.fullname
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." )  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby in ('".implode("','",$createdby)."') 



) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 
) as table2 group by fullname, bookingdate 
) as table3 group by bulan, fullname 
) as table4 group by bulan, fullname, total order by bulan";

  	}else if($cabang=="641940104"){
  			$sql="select 
	createddate,
	createddate_spk as createddate_SPK_Booking,
	bookingdate,
	joborderdate1 as SPK_date,
	PoliceRegNo,
	JobOrderNo as SPKNo,
	invoiceno,
	invoicedate,
	createdby,
	fullname,
	case when convert(date,createddate)<convert(date,bookingdate) and month(invoicedate)=".$bulan." then 'counted' else 'No' end as status
from (
select distinct
	  a.createddate,
	  (select top(1) createddate from SvRegisterSpk where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as createddate_spk,
	  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) InvoiceDate from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  a.createdby,
	  b.fullname
	  from svtrnservice as a
	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." ) and (a.BranchCode in (641940103,641940101) ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy ='".$createdby."'

) as table1 order by bookingdate, createdby";
  	}
  		  
  		 
  		  $query=$this->dbDms->query($sql);
  		   return $query->result_array();
  		  

}



function getlistBookingCianjurRange($bulan,$tahun,$cabang,$createdby){
  	// 	  $bulan=$_GET['bulan'];
 	 //    $year=$_GET['tahun'];
 	 //     $cabang=$_GET['cabang'];
  	$sql="";
  		  $arr = array();
  	if($cabang=="641940101"){

  		$sql="select bulan, fullname, total, count(fullname) OVER(PARTITION BY bulan ORDER BY fullname DESC) Rank from (
  		select bulan, fullname, sum(total) as total from (
Select fullname as fullname, sum(jumlah) as total, month(bookingdate) as bulan from (
select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah, fullname
from(
select distinct  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.servicetype,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  a.createddate,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  b.fullname
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)<=".$bulan." )  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby in ('".implode("','",$createdby)."') 



) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 
) as table2 group by fullname, bookingdate 
) as table3 group by bulan, fullname 
) as table4 group by bulan, fullname, total order by bulan";
  	}else if($cabang=="641940102"){

  		$sql="select bulan, fullname, total, count(fullname) OVER(PARTITION BY bulan ORDER BY fullname DESC) Rank from (
  		select bulan, fullname, sum(total) as total from (
Select fullname as fullname, sum(jumlah) as total, month(bookingdate) as bulan from (
select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah, fullname
from(
select distinct  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.servicetype,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  a.createddate,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  b.fullname
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)<=".$bulan." )  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby in ('".implode("','",$createdby)."') 



) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 
) as table2 group by fullname, bookingdate 
) as table3 group by bulan, fullname 
) as table4 group by bulan, fullname, total order by bulan";

  	}else if($cabang=="641940103"){

  		$sql="select bulan, fullname, total, count(fullname) OVER(PARTITION BY bulan ORDER BY fullname DESC) Rank from (
  		select bulan, fullname, sum(total) as total from (
Select fullname as fullname, sum(jumlah) as total, month(bookingdate) as bulan from (
select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah, fullname
from(
select distinct  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.servicetype,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  a.createddate,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  b.fullname
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)<=".$bulan." )  and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby in ('".implode("','",$createdby)."') 



) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 
) as table2 group by fullname, bookingdate 
) as table3 group by bulan, fullname 
) as table4 group by bulan, fullname, total order by bulan";

  	}else if($cabang=="641940104"){
  			$sql="select 
	createddate,
	createddate_spk as createddate_SPK_Booking,
	bookingdate,
	joborderdate1 as SPK_date,
	PoliceRegNo,
	JobOrderNo as SPKNo,
	invoiceno,
	invoicedate,
	createdby,
	fullname,
	case when convert(date,createddate)<convert(date,bookingdate) and month(invoicedate)=".$bulan." then 'counted' else 'No' end as status
from (
select distinct
	  a.createddate,
	  (select top(1) createddate from SvRegisterSpk where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as createddate_spk,
	  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) InvoiceDate from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  a.createdby,
	  b.fullname
	  from svtrnservice as a
	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." ) and (a.BranchCode in (641940103,641940101) ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy ='".$createdby."'

) as table1 order by bookingdate, createdby";
  	}
  		  
  		 
  		  $query=$this->dbDms->query($sql);
  		   return $query->result_array();
  		  

}


function getlistTargetEntryCianjurSUM($bulan,$tahun,$cabang,$createdby){
	$namabulan="";
  	if($bulan==1){
	 $namabulan="jan";
	}else if($bulan==2){
	 $namabulan="feb";
	}else if($bulan==3){
	 $namabulan="mar";
	}else if($bulan==4){
	 $namabulan="apr";
	}else if($bulan==5){
	 $namabulan="mei";
	}else if($bulan==6){
	 $namabulan="jun";
	}else if($bulan==7){
	 $namabulan="jul";
	}else if($bulan==8){
	 $namabulan="agu";
	}else if($bulan==9){
	 $namabulan="sep";
	}else if($bulan==10){
	 $namabulan="okt";
	}else if($bulan==11){
   	 $namabulan="nov";
	}else if($bulan==12){
   	 $namabulan="des";
	}

  $sql="SELECT sum(".$namabulan.") as jumlah from targetunitentry where tahun='".$tahun."' and cabang='".$cabang."'";
  		  
  		 
  		  $query=$this->db->query($sql);
  		   return $query->result_array();
  		  

}


function getlistTargetEntryCianjurSUMRange($bulan,$tahun,$cabang,$createdby){
	$sqlbulan="";
if($bulan==1){
 $sqlbulan="SELECT sum(jan) '1' ";

}else if($bulan==2){
  $sqlbulan="SELECT
    sum(jan) '1'
   ,sum(feb) '2' ";

}else if($bulan==3){
   $sqlbulan="select sum(jan) '1'
   ,sum(feb) '2'
   ,sum(mar) '3' ";

}else if($bulan==4){
   $sqlbulan="select sum(jan) '1'
   ,sum(feb) '2'
   ,sum(mar) '3'
   ,sum(apr) '4' ";

}else if($bulan==5){
  $sqlbulan="SELECT
    sum(jan) '1'
   ,sum(feb) '2'
   ,sum(mar) '3'
   ,sum(apr) '4'
   ,sum(mei) '5' ";
  
}else if($bulan==6){
  $sqlbulan="SELECT
    sum(jan) '1'
   ,sum(feb) '2'
   ,sum(mar) '3'
   ,sum(apr) '4'
   ,sum(mei) '5'
   ,sum(jun) '6' ";
  
}else if($bulan==7){
   $sqlbulan="SELECT
    sum(jan) '1'
   ,sum(feb) '2'
   ,sum(mar) '3'
   ,sum(apr) '4'
   ,sum(mei) '5'
   ,sum(jun) '6'
   ,sum(jul) '7' ";

}else if($bulan==8){
   $sqlbulan="SELECT
    sum(jan) '1'
   ,sum(feb) '2'
   ,sum(mar) '3'
   ,sum(apr) '4'
   ,sum(mei) '5'
   ,sum(jun) '6'
   ,sum(jul) '7'
   ,sum(agu) '8' ";

}else if($bulan==9){
   $sqlbulan="SELECT
    sum(jan) '1'
   ,sum(feb) '2'
   ,sum(mar) '3'
   ,sum(apr) '4'
   ,sum(mei) '5'
   ,sum(jun) '6'
   ,sum(jul) '7'
   ,sum(agu) '8'
   ,sum(sep) '9' ";

} else if($bulan==10){
   $sqlbulan="SELECT
    sum(jan) '1'
   ,sum(feb) '2'
   ,sum(mar) '3'
   ,sum(apr) '4'
   ,sum(mei) '5'
   ,sum(jun) '6'
   ,sum(jul) '7'
   ,sum(agu) '8'
   ,sum(sep) '9'
   ,sum(okt) '10' ";

} else if($bulan==11){
    $sqlbulan="SELECT
    sum(jan) '1'
   ,sum(feb) '2'
   ,sum(mar) '3'
   ,sum(apr) '4'
   ,sum(mei) '5'
   ,sum(jun) '6'
   ,sum(jul) '7'
   ,sum(agu) '8'
   ,sum(sep) '9'
   ,sum(okt) '10'
   ,sum(nov) '11' ";

} else if($bulan==12){
   $sqlbulan="SELECT
    sum(jan) '1'
   ,sum(feb) '2'
   ,sum(mar) '3'
   ,sum(apr) '4'
   ,sum(mei) '5'
   ,sum(jun) '6'
   ,sum(jul) '7'
   ,sum(agu) '8'
   ,sum(sep) '9'
   ,sum(okt) '10'
   ,sum(nov) '11'
   ,sum(des) '12' ";
}

$sql=$sqlbulan." from targetunitentry where tahun='".$tahun."' and cabang= '".$cabang. "'";  		 
  		  $query=$this->db->query($sql);
  		   return $query->result_array();
  		  

}


function getPIC($idpengajuan){
	$id=$idpengajuan;
	$sql="SELECT  b.aprroveby, b.iddtlpengajuan, b.relateduser,b.bulan, b.tahun FROM pengajuan_insentive_sro as a inner JOIN
detail_pengajuan_insentive_sro as b on a.idpengajuan=b.idpengajuan WHERE a.idpengajuan=".$id." order by b.aproveddate";


     $query=$this->db->query($sql);
       return $query->result_array();
}




//  public function getcalculatedsroCJR($bulan,$tahun,$cabang,$createdby){
//  	      // $bulan=$_GET['bulan'];
//  	      // $year=$_GET['tahun'];
//  	      // $cabang=$_GET['cabang'];

//  	      $sql="";
//   		  $arr = array();

//   		  if($cabang=="641940101"){

//   		  	 $sql="select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive,fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  a.createddate,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
	 
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
// 	   where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." ) and (a.BranchCode in (".$cabang.")) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
// 	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby = '".$createdby."'



// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname order by bookingdate";




//   		  }else if($cabang=="641940102"){

//   		  		 $sql="select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive,fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  a.createddate,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
	 
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
// 	   where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." ) and (a.BranchCode in (".$cabang.")) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
// 	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby = '".$createdby."'



// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname order by bookingdate";

//   		  }else if($cabang=="641940103"){

//   		  	 $sql="select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive,fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  a.createddate,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
	 
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
// 	   where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." ) and (a.BranchCode in (".$cabang.")) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
// 	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby = '".$createdby."'



// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname order by bookingdate";


//   		  }else if($cabang=="641940104"){

//   		  		 $sql="select bookingdate, joborderdate1 as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive,fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  a.createddate,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
	 
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
// 	   where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." ) and (a.BranchCode in (641940103,641940101)) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate))
// 	  and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby = '".$createdby."'



// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname order by bookingdate";


//   		  }


  		 
//   		  $query=$this->dbDms->query($sql);
//   		   return $query->result_array();
  		  

//   	}


//   	public function getcalculatedsrosumary($bulan,$tahun,$cabang,$createdby){

//   		$sql="";
//   		  $arr = array();

//   		if($cabang=="641940101"){
//   			  		$sql="select  'CCE' as Position, '15%' as total_incentive, sum(incentive)*15/100 as totalIncentive, case when incentive=NULL then 0 else sum(incentive) end as total,FullName from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, FullName
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
// 	     where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, FullName 

// ) as table2 group by FullName  

// union all


// select  'CCM' as Position, '10%' as total_incentive, sum(incentive)*10/100 as totalIncentive, sum(incentive) as total,fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive,
// fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	  where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, FullName 

// ) as table2 group by FullName 

// union all

// select  'SRO' as Position, '75%' as total_incentive, sum(incentive)*75/100 as totalIncentive, sum(incentive) as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	    where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 

// ) as table2 group by fullname";


//   		}else if($cabang=="641940102"){

//   		$sql="select  'CCE' as Position, '15%' as total_incentive, sum(incentive)*15/100 as totalIncentive, case when incentive=NULL then 0 else sum(incentive) end as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	     where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname

// ) as table2 group by fullname 

// union all


// select  'CCM' as Position, '10%' as total_incentive, sum(incentive)*10/100 as totalIncentive, sum(incentive) as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	  where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname

// ) as table2 group by fullname 

// union all

// select  'SRO' as Position, '75%' as total_incentive, sum(incentive)*75/100 as totalIncentive, sum(incentive) as total,fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
// 	    where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname

// ) as table2 group by fullname";

//   		}else if($cabang=="641940103"){

//   		$sql="select  'CCE' as Position, '15%' as total_incentive, sum(incentive)*15/100 as totalIncentive, case when incentive=NULL then 0 else sum(incentive) end as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	     where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 

// ) as table2 group by fullname 

// union all


// select  'CCM' as Position, '10%' as total_incentive, sum(incentive)*10/100 as totalIncentive, sum(incentive) as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	  where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 

// ) as table2 group by fullname  

// union all

// select  'SRO' as Position, '75%' as total_incentive, sum(incentive)*75/100 as totalIncentive, sum(incentive) as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	    where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (".$cabang.") ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname

// ) as table2 group by FullName";

//   		}else if($cabang=="641940104"){

//   		$sql="select  'CCE' as Position, '15%' as total_incentive, sum(incentive)*15/100 as totalIncentive, case when incentive=NULL then 0 else sum(incentive) end as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	     where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (641940103,641940101) ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname

// ) as table2 group by fullname  

// union all


// select  'CCM' as Position, '10%' as total_incentive, sum(incentive)*10/100 as totalIncentive, sum(incentive) as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId


// 	  where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (641940103,641940101) ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname 

// ) as table2 group by fullname 

// union all

// select  'SRO' as Position, '75%' as total_incentive, sum(incentive)*75/100 as totalIncentive, sum(incentive) as total, fullname from(
// select convert(varchar,bookingdate,105) bookingdate, convert(varchar,joborderdate1,105) as spkdate, invoicedate as bulan_invoice, count(invoiceno) as jumlah,
// case when count(invoiceno)>=10 and invoicedate=".$bulan." then 100000 else 0 end as incentive, fullname
// from(
// select distinct  convert(date,a.bookingDate) as bookingdate,
// 	  convert(date,a.joborderdate) as joborderdate1,
// 	  a.servicetype,
// 	  a.PoliceRegNo,
// 	  a.JobOrderNo,
// 	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
// 	  (select top(1) month(InvoiceDate) from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
// 	  b.fullname
// 	  --count(b.InvoiceNo) as jumlah
// 	  from svtrnservice as a
// 	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId

// 	    where (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan.") and (a.BranchCode in (641940103,641940101) ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and convert(date,createddate) < convert(date,a.bookingDate) and a.createdby like ('".$createdby."'
// )
// ) as table1 group by bookingdate, joborderdate1, invoicedate, fullname

// ) as table2 group by fullname";

//   		}


//   			$query=$this->dbDms->query($sql);
//   		   return $query->result_array();

//   	}


//get total all booking
function getlisttotalBookingCianjur($bulan,$tahun,$cabang,$createdby){
  	// 	  $bulan=$_GET['bulan'];
 	 //    $year=$_GET['tahun'];
 	 //     $cabang=$_GET['cabang'];
  	$sql="";
  		  $arr = array();
  	if($cabang=="641940101"){

  	$sql="
select bulan, sum(total) as total
from (
 select distinct 
b.fullname as fullname,
month(a.bookingDate) as bulan, 
count(a.bookingDate) as total
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." )
	   
 group by month(a.bookingDate), b.fullname
  ) as table1 group by bulan order by bulan";



  	}else if($cabang=="641940102"){

  		$sql="
select bulan, sum(total) as total
from (
 select distinct 
b.fullname as fullname,
month(a.bookingDate) as bulan, 
count(a.bookingDate) as total
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." )
	   
 group by month(a.bookingDate), b.fullname
  ) as table1 group by bulan order by bulan";

  	}else if($cabang=="641940103"){

  		$sql="
select bulan, sum(total) as total
from (
 select distinct 
b.fullname as fullname,
month(a.bookingDate) as bulan, 
count(a.bookingDate) as total
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)=".$bulan." )
	   
 group by month(a.bookingDate), b.fullname
  ) as table1 group by bulan order by bulan";


  	}else if($cabang=="641940104"){
  			$sql="select 
	createddate,
	createddate_spk as createddate_SPK_Booking,
	bookingdate,
	joborderdate1 as SPK_date,
	PoliceRegNo,
	JobOrderNo as SPKNo,
	invoiceno,
	invoicedate,
	createdby,
	fullname,
	case when convert(date,createddate)<convert(date,bookingdate) and month(invoicedate)=".$bulan." then 'counted' else 'No' end as status
from (
select distinct
	  a.createddate,
	  (select top(1) createddate from SvRegisterSpk where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as createddate_spk,
	  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) InvoiceDate from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  a.createdby,
	  b.fullname
	  from svtrnservice as a
	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." ) and (a.BranchCode in (641940103,641940101) ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy ='".$createdby."'

) as table1 order by bookingdate, createdby";
  	}
  		  
  		 
  		  $query=$this->dbDms->query($sql);
  		   return $query->result_array();
  		  

}


//end



function getlisttotalBookingCianjurRange($bulan,$tahun,$cabang,$createdby){
  	// 	  $bulan=$_GET['bulan'];
 	 //    $year=$_GET['tahun'];
 	 //     $cabang=$_GET['cabang'];
  	$sql="";
  		  $arr = array();
  	if($cabang=="641940101"){

  	$sql="
select bulan, sum(total) as total
from (
 select distinct 
b.fullname as fullname,
month(a.bookingDate) as bulan, 
count(a.bookingDate) as total
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)<=".$bulan." )
	   
 group by month(a.bookingDate), b.fullname
  ) as table1 group by bulan order by bulan";



  	}else if($cabang=="641940102"){

  		$sql="
select bulan, sum(total) as total
from (
 select distinct 
b.fullname as fullname,
month(a.bookingDate) as bulan, 
count(a.bookingDate) as total
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)<=".$bulan." )
	   
 group by month(a.bookingDate), b.fullname
  ) as table1 group by bulan order by bulan";


  	}else if($cabang=="641940103"){

  		$sql="
select bulan, sum(total) as total
from (
 select distinct 
b.fullname as fullname,
month(a.bookingDate) as bulan, 
count(a.bookingDate) as total
	 
	  --count(b.InvoiceNo) as jumlah
	  from svtrnservice as a
	  	inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (a.BranchCode in (".$cabang.")) and (year(a.BookingDate)=".$tahun." and month(a.BookingDate)<=".$bulan." )
	   
 group by month(a.bookingDate), b.fullname
  ) as table1 group by bulan order by bulan";


  	}else if($cabang=="641940104"){
  			$sql="select 
	createddate,
	createddate_spk as createddate_SPK_Booking,
	bookingdate,
	joborderdate1 as SPK_date,
	PoliceRegNo,
	JobOrderNo as SPKNo,
	invoiceno,
	invoicedate,
	createdby,
	fullname,
	case when convert(date,createddate)<convert(date,bookingdate) and month(invoicedate)=".$bulan." then 'counted' else 'No' end as status
from (
select distinct
	  a.createddate,
	  (select top(1) createddate from SvRegisterSpk where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as createddate_spk,
	  convert(date,a.bookingDate) as bookingdate,
	  convert(date,a.joborderdate) as joborderdate1,
	  a.PoliceRegNo,
	  a.JobOrderNo,
	  (select top(1) invoiceNo from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoiceno,
	  (select top(1) InvoiceDate from svTrnInvoice where JobOrderNo=a.JobOrderNo and BranchCode=a.BranchCode) as invoicedate,
	  a.createdby,
	  b.fullname
	  from svtrnservice as a
	  inner join SysUserAllDealer as b on a.CreatedBy=b.UserId
	   where (year(a.BookingDate)=".$tahun." and month(a.bookingdate)=".$bulan." ) and (a.BranchCode in (641940103,641940101) ) and (convert(date,a.JobOrderDate)=convert (date,a.BookingDate)) and a.CreatedBy ='".$createdby."'

) as table1 order by bookingdate, createdby";
  	}
  		  
  		 
  		  $query=$this->dbDms->query($sql);
  		   return $query->result_array();
  		  

}


//end





public function pengajuanSROAwal($bulan,$tahun){

  		$relateduser=$this->session->userdata('relateduser');
  		$cabang=$this->session->userdata('cabang');


 	 $sql="select idpengajuan, cabang, fullname, relateduser, destination from pengajuan_insentive_sro where cabang='".$cabang."' and bulan='".$bulan."' and tahun='".$tahun."' and relateduser='".$relateduser."'";

 	 $query=$this->db->query($sql);
 	 if($query->num_rows()>0){
 	 	//   $sqlupdate="UPDATE `pengajuan_insentive_sro` SET `idkaryawan`='[value-2]',`cabang`='[value-3]',`bulan`='[value-4]',`tahun`='[value-5]',`destination`='[value-6]',`fullname`='[value-7]',`content`='[value-8]',`relateduser`='[value-9]',`insert_by`='[value-10]',`insert_time`='[value-11]',`update_by`='[value-12]',`update_time`='[value-13]' WHERE 1";

 	 	//   $sqlupdate2="UPDATE `detail_pengajuan_insentive_sro` SET `iddtlpengajuan`='[value-1]',`idpengajuan`='[value-2]',`lastprogress`='[value-3]',`bulan`='[value-4]',`tahun`='[value-5]',`aprroveby`='[value-6]',`aproveddate`='[value-7]',`relateduser`='[value-8]',`insert_by`='[value-9]',`insert_time`='[value-10]',`update_by`='[value-11]',`update_time`='[value-12]' WHERE 1";

 	 	//   $this->db->trans_start();
 	 	//   $this->db->query($sqlupdate);
		  // $this->db->query($sqlupdate2);
    //       $this->db->trans_complete();
    //       if ($this->db->trans_status() === FALSE){
      		
		  // }else{

		  // }

 	 	$hasil1="3";
 	 	return $hasil1;

 	 	  
 	 }else{


 	 	  $relateduser=$this->session->userdata('relateduser');
 	 	  $nik=$this->session->userdata('nik');
 	 	  $cabang=$this->session->userdata('cabang');
 	 	  $fullname=$this->session->userdata('admin_nama');
 	 	  


 	 	  
 	 	  $sqlresultpencarian="select idpengajuan from pengajuan_insentive_sro where cabang='".$cabang."' and bulan='".$bulan."' and tahun='".$tahun."' and relateduser='".$relateduser."'";

 	 	  $sqlinsert="INSERT INTO `pengajuan_insentive_sro`(`idpengajuan`,`idkaryawan`, `cabang`, `bulan`, `tahun`, `destination`, `fullname`, `content`, `relateduser`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES (0,'".$nik."','".$cabang."','".$bulan."','".$tahun."','SM','".$fullname."','incentive SRO','".$relateduser."','".$nik."','".date("Y-m-d H:i:s")."','".$nik."','".date("Y-m-d H:i:s")."')";

 	 	 

 	 	  
 	 	  $this->db->trans_begin();
 	 	  $this->db->query($sqlinsert);
		  $idpengajuan=$this->db->query($sqlresultpencarian)->row();

		   $sqlinsert2="INSERT INTO `detail_pengajuan_insentive_sro`(`idpengajuan`, `lastprogress`, `bulan`, `tahun`, `aprroveby`, `aproveddate`, `relateduser`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES ('".$idpengajuan->idpengajuan."','SRO',".$bulan.",'".$tahun."','".$nik."','".date("Y-m-d H:i:s")."','".$relateduser."','".$nik."','".date("Y-m-d H:i:s")."','".$nik."','".date("Y-m-d H:i:s")."')";
		   
           $this->db->query($sqlinsert2);

         
 	 	  if ($this->db->trans_status() === FALSE){
			    $hasil1="0";
		        $this->db->trans_rollback();
        }else{
				$hasil1="1";
		        $this->db->trans_commit();
		}

		return $hasil1;
 	 }

 	 	
 	 

 	}


public function pengajuanSRO($idpengajuan){
 	 

       $nik=$this->session->userdata('nik');
       $position=$this->session->userdata('position');
       $relateduser=$this->session->userdata('relateduser');

       echo "list position ".$position;

       		if($position=="SM"){
       			$position1="FD";
       		}else if($position=="FD"){
       			$position1="BM";
       		}

 	 
 	 	  $sqlupdate="UPDATE `pengajuan_insentive_sro` SET `destination`='".$position1."',`update_by`='".$nik."',`update_time`='".date("Y-m-d H:i:s")."' WHERE idpengajuan = ".$idpengajuan;

 	 	  $sqlupdate2="INSERT INTO `detail_pengajuan_insentive_sro`(`iddtlpengajuan`, `idpengajuan`, `lastprogress`, `bulan`, `tahun`, `aprroveby`, `aproveddate`, `relateduser`, `insert_by`, `insert_time`, `update_by`, `update_time`) VALUES (0,'".$idpengajuan."','Aprroved','','','".$nik."','".date("Y-m-d H:i:s")."','".$relateduser."','".$nik."','".date("Y-m-d H:i:s")."','".$nik."','".date("Y-m-d H:i:s")."')";

 	 	  $this->db->trans_begin();
 	 	  $this->db->query($sqlupdate);
		  $this->db->query($sqlupdate2);
         if ($this->db->trans_status() === FALSE){
			    $hasil1="pengajuan data gagal di koneksi";
		        $this->db->trans_rollback();
        }else{
				$hasil1="data telah di ajukan";
		        $this->db->trans_commit();
		}
 	 	  
 	 	 return $hasil1;
 	 
 }











}
