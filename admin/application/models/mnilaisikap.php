<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mnilaisikap extends CI_Model {

		public function __construct() {
        parent::__construct();
    }
 function json($field)
	 {
		 $and =  $this->session->userdata('and');
		/*return $query = $this->db->query("SELECT a.idnilaisikap,a.tgl,a.nota,a.supplier,sum(b.total) as total,a.ket from tnilaisikap as a left join tnilaisikapdtl as b on a.idnilaisikap = b.idnilaisikap where (nota like '" . $field . "%' or supplier like '" . $field . "%' ) group by a.idnilaisikap ");*/
		
$requestData= $_REQUEST;
$columns = array( 	
  0 => 'idnilaisikap',
  1 => 'tgl',
  2 => 'nmkaryawan',
  3 => 'nama',
  4 => 'kelas',
  5 => 'sikap',
  6 => 'bobot',

);
		$sql = " SELECT a.idnilaisikap,a.tgl,b.nmkaryawan,c.nama,d.kelas,e.sikapdtl as sikap,a.bobot FROM 
tnilaisikap AS a LEFT JOIN tkaryawan AS b ON a.idpengajar = b.idkaryawan LEFT JOIN tpeserta AS c ON a.idpeserta = c.idpeserta
 LEFT JOIN tkelas AS d ON a.idkelas = d.idkelas LEFT JOIN tsikapdtl AS e ON a.idsikap = e.idsikapdtl where (b.nmkaryawan like '" . $field . "%' or c.nama like '" . $field . "%' or e.sikapdtl like '" . $field . "%' or d.kelas like '" . $field . "%' )" . $and;

	$query =   $this->db->query($sql);
	$totalData = $query->num_rows();
	$totalFiltered = $totalData;
		
	if( !empty($requestData['search']['value']) ) {
	
	}
	
	$query =   $this->db->query($sql);
	$totalFiltered = $query->num_rows($sql);
		


	//----------------------------------------------------------------------------------
	
	$data = array();
	$x=0;
	 foreach($query->result_object() as $rows )
        {
			$x=$x+1;	  
		$nestedData=array(); 
					$nestedData[] = $x;
					$nestedData[] = $rows->tgl;
					$nestedData[] = $rows->nmkaryawan;
					$nestedData[] = $rows->nama;
					$nestedData[] = $rows->kelas;
					$nestedData[] = $rows->sikap;
					$nestedData[] = number_format($rows->bobot, 2);
					
					$nestedData[] =   "<div align='right'><a class='btn btn-info' href=editnilaisikap/". $rows->idnilaisikap ."  >
							  <i class='glyphicon glyphicon-edit icon-white'></i>
							  </a>
							  <a class='btn btn-danger' href=hapusnilaisikap/". $rows->idnilaisikap ." >
							  <i class='glyphicon glyphicon-trash icon-white'></i>
							  </a>
							  </div>";
		$data[] = $nestedData;
	}
	//----------------------------------------------------------------------------------
	$json_data = array(
 		
		"recordsTotal"    => intval( $totalData ), 
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data );
	//----------------------------------------------------------------------------------
	return  json_encode($json_data);

    }
	
	public function idnilaisikap($nota)
	 {		 
		 $arr = array();
		
		$query = $this->db->query("SELECT count(*) as jml,idnilaisikap from tnilaisikap where nota = '" . $nota . "' ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);

  		 
    }
	
	function tampiledit($field){
		$arr = array();
		
		$query = $this->db->query("SELECT  a.idnilaisikapdtl,a.idnilaisikap,a.idakun,b.akundtl AS akun,b.satuan,a.qty,a.`harga`,a.`total` FROM  tnilaisikapdtl AS a 
LEFT JOIN tnilaisikap AS c ON a.idnilaisikap = c.idnilaisikap LEFT JOIN takundtl AS b ON a.idakun = b.idakundtl where c.idnilaisikap = '$field'");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
         return  "{\"data\":" .json_encode($arr). "}";
	}
	
	
	
	public function simpandtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->insert_batch('tnilaisikapdtl',$data);
  		 
    }
	public function updatedtl($data)
	 {
		 $data = json_decode( $data, true );
		 $query = $this->db->update_batch('tnilaisikapdtl',$data,'idnilaisikapdtl');
  		 return  json_encode($query);
    }

	public function deletedtl1($id)
	{
		return $this->db->delete('tnilaisikapdtl', array('idnilaisikapdtl' => $id));
	}
	
	public function hapusnilaisikap($id)
	{
		return $this->db->delete('tnilaisikap', array('idnilaisikap' => $id));
	}
	
	public function editnilaisikap($id)
	{
		return $this->db->get_where('tnilaisikap',array('idnilaisikap'=>$id));
	}
	
	
	public function datapeserta()
    {
        $arr = array();
		
		 $query = $this->db->query("select  a.idpeserta,a.nik,a.nama,b.kelas,a.idkelas from tpeserta as a left join tkelas as b on a.idkelas = b.idkelas" );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	public function datasikap()
    {
        $arr = array();
		
		 $query = $this->db->query("select  a.idsikapdtl as idsikap,b.sikap as kategori,a.sikapdtl as sikap,a.bobot from tsikapdtl as a left join tsikap as b on a.idsikap = b.idsikap" );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	
	
	public function get_filterdata($field)
    {
        $arr = array();

		$query = $this->db->query("SELECT * from tnilaisikap as b   where b.nmnilaisikap like '" . $field . "%' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function urlnumber()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
	
	
	public function datanilaisikap()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT b.kdakundtl as kode,b.akundtl as akun,a.idakun,b.`satuan` FROM takun_nilaisikap AS a LEFT JOIN takundtl AS b ON a.`idakun` = b.`idakundtl` where flag='1'" );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	
		public function getjson()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT  column_name, column_type,column_comment FROM database_schema WHERE table_name =  'tnilaisikap' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }

	
	public function mgetjsonshow($id)
    {
        $arr = array();


		$query = $this->db->query("SELECT a.idnilaisikap,a.tgl,b.nmkaryawan,c.nama,d.kelas,e.sikapdtl,a.bobot,a.`idkelas`,a.idsikap,a.idpeserta,a.idpengajar FROM 
tnilaisikap AS a LEFT JOIN tkaryawan AS b ON a.idpengajar = b.idkaryawan LEFT JOIN tpeserta AS c ON a.idpeserta = c.idpeserta
 LEFT JOIN tkelas AS d ON a.idkelas = d.idkelas LEFT JOIN tsikapdtl AS e ON a.idsikap = e.idsikapdtl  where a.idnilaisikap = '$id'");	
        
		foreach($query->result_object() as $rows )
        {
		foreach ($query->list_fields() as $field)
			{
				$arr[$field] =$rows->$field ;
			}	   	
       }

        return  json_encode($arr);

    }
	
	public function get_datapopup($field)
    {
       $arr = array();
		
	$query = $this->db->query("SELECT a.idnilaisikap ,a.nik,a.nmnilaisikap  FROM  tnilaisikap AS a  where (a.nmnilaisikap like '" . $field . "%' or a.nik like '" . $field . "%')   limit 1000 ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function url()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
}
