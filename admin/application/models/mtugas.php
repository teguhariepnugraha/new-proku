<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mtugas extends CI_Model {

	
	public function tampil($field)
	 {
		return  $this->db->query("SELECT * from ttugas as a left join tkaryawan as b ON a.idkaryawan = b.idkaryawan where ( b.nmkaryawan like '" . $field . "%') limit 1000 ");
  
    }
	public function hapustugas($id)
	{
	
		return $this->db->delete('ttugas', array('idtugas' => $id));
	}
	
	public function edittugas($id)
	{
		return $this->db->get_where('ttugas',array('idtugas'=>$id));
	}
	
	public function updatedtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->update_batch('ttugasdtl',$data,'idtugasdtl');
  		 
    }
	public function idtugas($kdtugas)
	 {		 
		 $arr = array();
		
		$query = $this->db->query("SELECT count(*) as jml,idtugas from ttugas where kdtugas = '" . $kdtugas . "' ");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);

  		 
    }
	
	
	public function tugasdtl($idtugas)
	 {
  		$arr = array();

		$query = $this->db->query("SELECT a.idtugasdtl,a.idtugas,a.tgl_mulai,a.tgl_beres,a.file_tugas,a.file_hasil_tugas,c.idkaryawan,c.nik,c.nmkaryawan,'checked' as chk
FROM ttugasdtl AS a LEFT JOIN tkaryawan as c on a.idkaryawan = c.idkaryawan  where ( a.idtugas= '" . $idtugas . "' ) ");

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	

	public function get_filterdata($field)
    {
        $arr = array();

		$query = $this->db->query("SELECT * from ttugas as b   where b.depatemen like '" . $field . "%' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
		public function getjson()
    {
        $arr = array();
		
		 $query = $this->db->query("SELECT  column_name, column_type,column_comment FROM database_schema WHERE table_name =  'ttugas' " );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	
	public function get_datapopup1($mtahun,$idmgroupkerja)
    {
       $arr = array();
		
		$query = $this->db->query("SELECT idtugas,kdtugas from ttugas  limit 1000");
		
		
        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  "{\"data\":" .json_encode($arr). "}";
    }
	
	public function mgetjsonshow($id)
    {
        $arr = array();


		$query = $this->db->query("SELECT * from ttugas as a where a.idtugas = '$id'");	
        
		foreach($query->result_object() as $rows )
        {
		foreach ($query->list_fields() as $field)
			{
				$arr[$field] =$rows->$field ;
			}	   	
       }

        return  json_encode($arr);

    }
	
	public function datakaryawan()
    {
        $arr = array();
		
		 $query = $this->db->query("select idkaryawan,nik,nmkaryawan from tkaryawan" );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
        }
        return  json_encode($arr);
    }
	public function simpandtl($data)
	 {
		 $data = json_decode( $data, true );
		 $this->db->insert_batch('ttugasdtl',$data);
  		 
    }
	
	
	public function urlnumber()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	public function url()
    {
        $arr = array();
		$link=decrypt_url($_GET['link']);
		$query = $this->db->query($link );

        foreach($query->result_object() as $rows )
        {
            $arr[] = $rows;
			
        }
        return  json_encode($arr);
    }
	
	
}
